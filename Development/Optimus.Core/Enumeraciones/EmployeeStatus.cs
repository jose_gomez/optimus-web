﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Core.Enumeraciones
{
    public enum EmployeeStatus
    {
        ACTIVO = 31,
        INACTIVO = 32,
        LIQUIDADO = 39,
        PENDIENTE = 35
    }
}
