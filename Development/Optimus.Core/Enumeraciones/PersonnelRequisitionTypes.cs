﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Core.Enumeraciones
{
    public enum PersonnelRequisitionTypes
    {
        PRODUCTION = 1,
        ADMINISTRATIVE = 2
    }
}
