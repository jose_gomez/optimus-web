﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Optimus.Core.WebMessagesHelper
{
    public class MessagesHelperWb
    {
        public static string GetHtmlForAlertMessage( List<string> errores)
        {
            string error = string.Empty;
            foreach (string item in errores)
            {
                error += "<li>"
                    + item
                    + "</li>";
            }
            if (!string.IsNullOrWhiteSpace(error))
            {
                return "<a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" +
                                                   "<ul>" + error + "</ul>";
            }

            return string.Empty;
        }
    }
}
