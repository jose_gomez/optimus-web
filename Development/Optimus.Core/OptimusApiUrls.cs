﻿namespace Optimus.Core
{
    public class OptimusApiUrls
    {

        #region Public Fields

        public const string AccionesEmpleados_POST = PREFIX_AccionesEmpleados + "/Post";
        public const string AccionesEmpleados_GET_Pendientes = PREFIX_AccionesEmpleados + "/Get/Pendientes";
        public const string AccionesEmpleados_GET_Pendientes_FECHA = PREFIX_AccionesEmpleados + "/Get/Pendientes/{fecha}";
        public const string Candidatos_GET_BY_ID = PREFIX_Candidatos + "/Get/{id}";
        public const string Empleados_DELETE = PREFIX_Empleados + "/Delete/{id}";
        public const string Empleados_GET_AccionesEmpleado = PREFIX_Empleados + "Get/{id}/AccionesEmpleado";
        public const string Empleados_GET_BY_ID = PREFIX_Empleados + "/Get/{id}";
        public const string Empleados_GET_BY_POSITION_ID = PREFIX_Empleados + "/Get/Posicion/{id}";
        public const string Empleados_GET_Posicion = PREFIX_Empleados + "/Get/{id}/Posicion";
        public const string Empleados_POST = PREFIX_Empleados + "/Post";
        public const string Empleados_PUT = PREFIX_Empleados + "/Put/{id}";
        public const string InformacionesBasicasEmpleados_GET = PREFIX_InformacionesBasicasEmpleados + "/Get";
        public const string InformacionesBasicasEmpleados_GET_ACTIVOS = PREFIX_InformacionesBasicasEmpleados + "/Get/Activos";
        public const string InformacionesBasicasEmpleados_GET_ACTIVOS_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Activos/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_BY_ID = PREFIX_InformacionesBasicasEmpleados + "/Get/{id}";
        public const string InformacionesBasicasEmpleados_GET_INACTIVOS = PREFIX_InformacionesBasicasEmpleados + "/Get/Inactivos";
        public const string InformacionesBasicasEmpleados_GET_INACTIVOS_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Inactivos/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_LIQUIDADOS = PREFIX_InformacionesBasicasEmpleados + "/Get/Liquidados";
        public const string InformacionesBasicasEmpleados_GET_LIQUIDADOS_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Liquidados/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_PENDIENTES = PREFIX_InformacionesBasicasEmpleados + "/Get/Pendientes";
        public const string InformacionesBasicasEmpleados_GET_PENDIENTES_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Pendientes/{filtro}";
        public const string Jornadas_GET = PREFIX_Jornadas + "/Get";
        public const string Jornadas_GET_BY_ID = PREFIX_Jornadas + "/Get/{id}";
        public const string Posiciones_DELETE = PREFIX_Posiciones + "/Delete/{id}";
        public const string Posiciones_GET = PREFIX_Posiciones + "/Get";
        public const string Posiciones_GET_ACTIVE = PREFIX_Posiciones + "/Get/Estado/Activo";
        public const string Posiciones_GET_BY_ID = PREFIX_Posiciones + "/Get/{id}";
        public const string Posiciones_GET_BY_STATUS = PREFIX_Posiciones + "/Get/Estado/{id}";
        public const string Posiciones_GET_INACTIVE = PREFIX_Posiciones + "/Get/Estado/Inactivo";
        public const string Posiciones_POST = PREFIX_Posiciones + "/Post";
        public const string Posiciones_PUT = PREFIX_Posiciones + "/Put/{id}";
        public const string PREFIX_AccionesEmpleados = "Optimus/AccionesEmpleados";
        public const string PREFIX_Candidatos = "Optimus/Candidatos";
        public const string PREFIX_Empleados = "Optimus/Empleados";
        public const string PREFIX_InformacionesBasicasEmpleados = "Optimus/InformacionesBasicasEmpleados";
        public const string PREFIX_Jornadas = "Optimus/Jornadas";
        public const string PREFIX_Posiciones = "Optimus/Posiciones";
        public const string PREFIX_PropuestasCompensacionesBeneficios = "Optimus/PropuestasCompensacionesBeneficios";
        public const string PropuestasCompensacionesBeneficios_GET_BY_CANDIDATE = PREFIX_PropuestasCompensacionesBeneficios + "/Get/Candidato/{id}";
        public const string PropuestasCompensacionesBeneficios_GET_BY_ID = PREFIX_PropuestasCompensacionesBeneficios + "/Get/{id}";
        public const string PropuestasCompensacionesBeneficios_POST = PREFIX_PropuestasCompensacionesBeneficios + "/Post";
        public const string PropuestasCompensacionesBeneficios_PUT = PREFIX_PropuestasCompensacionesBeneficios + "/Put/{id}";

        #endregion Public Fields

    }
}