﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Core
{
    public class OptimusMessages
    {
        public const string ERROR_MESSAGE = "Ha ocurrido un error. Por favor contactar a IT.";
        public const string PERSONA_YA_ES_EMPLEADO = "Esta persona ya es un empleado de la empresa.";
    }
}
