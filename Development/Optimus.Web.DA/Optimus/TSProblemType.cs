//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Optimus.Web.DA.Optimus
{
    using System;
    using System.Collections.Generic;
    
    public partial class TSProblemType
    {
        public TSProblemType()
        {
            this.TSTickets = new HashSet<TSTicket>();
        }
    
        public int ProblemTypeID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public string Name { get; set; }
    
        public virtual HRDepartment HRDepartments { get; set; }
        public virtual ICollection<TSTicket> TSTickets { get; set; }
    }
}
