//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Optimus.Web.DA.Optimus
{
    using System;
    using System.Collections.Generic;
    
    public partial class VWTicket
    {
        public Nullable<System.DateTime> SubmitDate { get; set; }
        public int TicketNumber { get; set; }
        public string Department { get; set; }
        public string Status { get; set; }
        public string Technician { get; set; }
        public Nullable<int> SubmitUser { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}
