﻿using Optimus.Web.WebForms.GuiHelpers;
using Optimus.Web.WebForms.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                this.Form.DefaultButton = ((Button)IdLogin.FindControl("btnLogin")).UniqueID;
                var a = Request.QueryString["ReturnUrl"];
            }
            if (this.Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Default.aspx");
            }
           
        }
        protected void IniciarSesion(object sender,EventArgs e)
        {
            CustomMembership a = new CustomMembership();
            string user = "";
            string pass;
            bool access = false;

            if (!this.Page.User.Identity.IsAuthenticated)
            {
                user = ((TextBox)IdLogin.FindControl("txtUsername")).Text;
                pass = ((TextBox)IdLogin.FindControl("txtPassword")).Text;
                access = a.ValidateUser(user, pass);
            }
            if (access)
            {
                FormsAuthentication.SetAuthCookie(user,true);
                FormsAuthentication.RedirectFromLoginPage(user, true);
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Error!", "Usuario o contraseña incorrecto");
            }
        }
        protected void SalirSesion(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
    }
}