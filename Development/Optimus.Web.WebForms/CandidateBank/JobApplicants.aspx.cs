﻿using iTextSharp.text.pdf;
using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CandidateBank
{
    public partial class JobApplicants : System.Web.UI.Page
    {
        private ServiceContainer container;
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
       ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            container = new  ServiceContainer();           
            candidatosRepeater.DataSource = container.getSolicitudesServices().GetCandidatesAll(); 
            candidatosRepeater.DataBind();
            if (!IsPostBack)
            {
                //BeneficiosCheckBox.DataSource = container.GetPositionsService().GetAllCompensationBenefit().Find(x=>x.CompensationID == 5 || x.CompensationID == 6);
                //BeneficiosCheckBox.DataTextField = "Name";
                //BeneficiosCheckBox.DataValueField = "CompensationID";
                //BeneficiosCheckBox.DataBind();

                PosicionDropDownList.DataSource = container.GetPositionsService().GetPositionRequsition();
                PosicionDropDownList.DataTextField = "PositionName";
                PosicionDropDownList.DataValueField = "PositionID";
                PosicionDropDownList.DataBind();

                deparmentDropDownDown.DataSource = container.GetUnitOfWork().dept.GetAll();
                deparmentDropDownDown.DataTextField = "Name";
                deparmentDropDownDown.DataValueField = "DepartmentID";
                deparmentDropDownDown.DataBind();


                PosicionFiltroDropDown.DataSource = container.GetPositionsService().GetPositionRequsition();
                PosicionFiltroDropDown.DataTextField = "PositionName";
                PosicionFiltroDropDown.DataValueField = "PositionID";
                PosicionFiltroDropDown.DataBind();
                PosicionFiltroDropDown.Items.Insert(0, new ListItem("<Seleccione una Posición>", "-1"));



                DropBoxEstudios.DataSource = container.GetStudies().GetAllStudies();
                DropBoxEstudios.DataTextField = "Name";
                DropBoxEstudios.DataValueField = "StudiesID";
                DropBoxEstudios.DataBind();
                DropBoxEstudios.Items.Insert(0, new ListItem("<Seleccione una Posición>", "-1"));

                estadoDropboxList.DataSource = container.getSolicitudesServices().GetCandidateStates();
                estadoDropboxList.DataTextField = "State";
                estadoDropboxList.DataValueField = "CandidateStateID";
                estadoDropboxList.DataBind();

            }
        }
        protected void filtro(object sender,EventArgs e)
        {
            string cedula = CedulaTextBox.Text == string.Empty?"-1":CedulaTextBox.Text;
            string sexo = SexoRadioButton.SelectedItem.Value;
            int posicion = int.Parse(PosicionFiltroDropDown.SelectedItem.Value);
            int area = int.Parse(CategoriaPosicion.SelectedItem.Value);
            int estudio = int.Parse(DropBoxEstudios.SelectedItem.Value);
            container = new ServiceContainer();
            candidatosRepeater.DataSource = container.getSolicitudesServices().GetCandidates(cedula,sexo,posicion,area,estudio);
            candidatosRepeater.DataBind();
        }

        protected void Redirect(object sender, CommandEventArgs e)
        {
        //  Response.Redirect("http://drl-it-testing/ReportServer/Pages/ReportViewer.aspx?/SolicitudesByCedula&cedula=" + e.CommandArgument.ToString());
            Response.Redirect("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/SolicitudesByCedula&cedula=" + e.CommandArgument.ToString());
        }
        protected void Contratar(object sender,CommandEventArgs e)
        {

        }
        protected void Curriculum(object sender, CommandEventArgs e)
        {
            container = new ServiceContainer();
            var cv = container.getSolicitudesServices().GetCandidateGUIDCurriculum(e.CommandArgument.ToString());
            if (cv != null)
            {
                // string FilePath = "C:\\Repositories\\Optimus-Web\\Development\\Optimus.Web.WebForms\\petition\\PDFFile\\" + cv.guid + ".pdf";


                string FilePath = "C:\\inetpub\\wwwroot\\Optimus\\Documents\\Cvs\\" + cv.guid+".pdf";//C:\inetpub\wwwroot\Documents\Cvs


                WebClient User = new WebClient();
                Byte[] FileBuffer = User.DownloadData(FilePath);
                if (FileBuffer != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", FileBuffer.Length.ToString());
                    Response.BinaryWrite(FileBuffer);
                }
            }
            else
            {
                
            }

        }
        protected void GuardarCurriculum(object sender, EventArgs e)
        {
            if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
            {
               
                container = new ServiceContainer();
                string gui = Guid.NewGuid().ToString();
                // string SaveLocation = "C:\\Repositories\\Optimus-Web\\Development\\Optimus.Web.WebForms\\petition\\PDFFile\\" + gui + ".pdf";

                string SaveLocation = "C:\\inetpub\\wwwroot\\Optimus\\Documents\\Cvs\\" + gui + ".pdf";

             

                CandidateGUIDCurriculum cv = new CandidateGUIDCurriculum();
                cv.Cedula = hiddencedula.Value;
                cv.guid = gui;
                cv.Create_dt = DateTime.Now;
                container.getSolicitudesServices().Save(cv);

                try
                {
                    FileUpload1.PostedFile.SaveAs(SaveLocation);
                    Response.Write("El archivo se ha cargado.");
                }
                catch (Exception ex)
                {
                    Response.Write("Error : " + ex.Message);

                }
            }

        }
        protected void  PDFCreating(object sender,EventArgs e)
        {
            //Stream file = new FileStream("C:\\Repositories\\Optimus-Web\\Development\\Optimus.Web.WebForms\\petition\\PDFFile\\FormularioNuevo", FileMode.Create);
            //FillPDF("", file);
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Formulario.pdf");
         correo(FillPDF(Server.MapPath("//petition//PDF//PropuestaSalarial.pdf")));
        }
        public Byte[] FillPDF(string template)
        {
            PdfReader reader = new PdfReader(template);

            string Nombre = container.getSolicitudesServices().GetCandidates(hiddencedula.Value).FullName;

          

            using (MemoryStream ms = new MemoryStream())
            {
                using (PdfStamper stamp = new PdfStamper(reader, ms, '\0', true))
                {
                    
                    stamp.AcroFields.SetField("Fecha", String.Format("{0} de {1} del año {2}", DateTime.Now.Date.Day.ToString(), DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-MX")),DateTime.Now.Year.ToString()));
                    stamp.AcroFields.SetField("Nombre", Nombre);
                    stamp.AcroFields.SetField("Cedula", hiddencedula.Value);
                    stamp.AcroFields.SetField("Posicion", PosicionDropDownList.SelectedItem.Text); 
                    stamp.AcroFields.SetField("SeReporta","");
                    stamp.AcroFields.SetField("Salario", SalarioTextBox.Text);
                    stamp.AcroFields.SetField("FlotaTelefonica", BeneficiosCheckBox.Items.FindByValue("5").Selected ? "○Flota telefonica" : "");
                    stamp.AcroFields.SetField("Incentivo Trimestral", BeneficiosCheckBox.Items.FindByValue("6").Selected ? "○Incentivo Trimestral" : "");
                    //stamp.FormFlattening = true;
                    stamp.Close();
                }
                return ms.ToArray();
            }
        }

        public void correo(Byte[]  bytes)
        {
            Candidates can = new Candidates();
            can = container.getSolicitudesServices().GetCandidates(hiddencedula.Value);
            MailMessage mm = new MailMessage();
            mm.Subject = "Propuesta salarial.";
            mm.Body = "Saludos.<br>Nos dirigimos a usted cordialmente ofertandole nuestra propuesta salarial, adjunto las informaciones de lugar.Quedamos en espera de su confirmación y posible fecha de inicio.";
            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Propuesta.pdf"));
            mm.IsBodyHtml = true;
            MailAddress ma = new MailAddress(can.Email);
            mm.To.Add(ma);
            mm.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailSenderAddress"]);
            var client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailSenderAddress"],
                    System.Configuration.ConfigurationManager.AppSettings["MailPassword"]);
            client.EnableSsl = true;
            client.Send(mm);
        }

       protected void enviarContrato(object sender,EventArgs e)
        {
            Candidates can = new Candidates();
            can = container.getSolicitudesServices().GetCandidates(hiddencedula.Value);
            MailMessage mm = new MailMessage();
            mm.Subject = "Contrato";
            mm.Body = "Saludos!";
            mm.Attachments.Add(new Attachment(Server.MapPath("//petition//PDF//Contrato.pdf")));
            mm.IsBodyHtml = true;
            MailAddress ma = new MailAddress(can.Email);
            mm.To.Add(ma);
            mm.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailSenderAddress"]);
            var client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailSenderAddress"],
                    System.Configuration.ConfigurationManager.AppSettings["MailPassword"]);
            client.EnableSsl = true;
            client.Send(mm);

        }

        protected void ActualizarCandidato(object sender,EventArgs e)
        {

            HRCandidate can = new HRCandidate();
            can = ContextSQL.HRCandidates.FirstOrDefault(x=>x.Cedula == hiddencedula.Value);
          //  Candidates can = container.getSolicitudesServices().GetCandidates(hiddencedula.Value);
            can.CandidateStateID = int.Parse(estadoDropboxList.SelectedItem.Value);
            can.interviewed = Boolentrevisto.Checked;
            ContextSQL.SubmitChanges();

            Response.Redirect(Request.RawUrl);
        }

        protected void ExportarCandidato(object sender, EventArgs e)
        {
            int dept = int.Parse(deparmentDropDownDown.SelectedItem.Value);
            string cedula = hiddencedula.Value;
            int candidateID = ContextSQL.HRCandidates.FirstOrDefault(x=>x.Cedula==cedula).CandidateID;
            container.getSolicitudesServices().ExportCandidate(candidateID,dept);
        }

    }
}