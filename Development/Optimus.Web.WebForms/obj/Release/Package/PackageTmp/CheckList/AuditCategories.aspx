﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditCategories.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.AuditCategories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script>
        <%
            var container = new Optimus.Web.BC.ServiceContainer();
        %>

        function cargarGridBySelect()
        {
            <%
                if (!string.IsNullOrEmpty(cbAuditType.Value))
                {

                    dgvCategory.AutoGenerateColumns = false;
                    var audtCat = container.GetAuditCategoriesService();
                    var consCat = audtCat.GetCategoryByControlID(Convert.ToInt16(cbAuditType.Value));
                    dgvCategory.DataSource = consCat;
                    dgvCategory.DataBind();
                }
            %>
            __doPostBack('cbAuditType')
        }

    </script>

    <div id="MessageAlertCategory" runat="server">

    </div>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">CATEGORIAS </a>
                        </h4>

                    </div>
                </div>
            </div>
        </div>
        <div id="collapse1" class="panel-collapse collapse in">

            <div class="panel-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">REGISTRO DE CATEGORIAS
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="main row">

                            <div class="col-sm-12 col-md-6">
                                <label class="control-label col-sm-12" for="Select2">Auditoria</label>
                                <select id="cbAuditType" name="cbAuditType" runat="server" class="form-control" onchange="cargarGridBySelect();" >
                                    <option></option>
                                        
                                </select>
                            </div>

                            <div class="col-sm-12 col-md-6">
                                <label class="control-label col-sm-12" for="txtCateg">Categoría</label>
                                <asp:TextBox class="form-control" ID="txtCateg" runat="server" OnTextChanged="txtCateg_TextChanged" placeholder="Categoria" TextMode="MultiLine" Style="text-transform: uppercase"></asp:TextBox>
                            </div>

                        </div>
                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <asp:Button  ID="btnRegistrar" runat="server" Text="Guardar" OnClick="btnRegistrar_Click" class="btn btn-primary" />
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">BUSQUEDA DE CATEGORIAS 
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div style="overflow: scroll;">
                            <asp:GridView ID="dgvCategory" runat="server" EmptyDataText="No existen Prueba." ShowHeaderWhenEmpty="false" Width="100%" ShowHeader="true" EnableModelValidation="False" OnRowCreated="dgvCategory_RowCreated" class="table table-striped" OnSelectedIndexChanged="dgvCategory_SelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField HeaderText="Categoria" ShowHeader="false" DataField="Categoria" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="AuditCategories.aspx?idCat=<%# Eval("ID") %>">Editar</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="alert-info">
                                        <strong>Información!</strong> No se encontro ningun registro.
                                    </div>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC00" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</asp:Content>
