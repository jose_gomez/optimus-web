﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Optimus.Web.WebForms._Default" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .sliderimage {
            width: 100%;
            height: 480px;
        }

        .headline {
            color: white;
        }

        .customCaption {
            background: rgba(0, 0, 0, 0.75) none repeat scroll 0px 0px;
            right: 0%;
            left: 0%;
            padding-top: 0%;
            padding-bottom: 0%;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('.carousel').carousel({
                interval: 5000
            });
        });
    </script>
    <div>
        <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/slider/slider1.jpg" class="sliderimage" alt="">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                            <h1 class="headline">Bienvenido a DRL Manufacturing</h1>
                            <p class="lead">
                                Nuestro compromiso como organización está enfocado en la calidad de los productos que ofrecemos, y en el servicio que brindamos a nuestros clientes internos y externos.
                        Servicio que solo puede ser obtenido gracias a el compromiso de nuestro personal, individuos trabajadores y comprometidos con la satisfacción completa de nuestros cliente y 
                        en mejorar los procesos de la organización, demostrando día a día en su trabajo valores como motivación, vocación y pasión por nuestro trabajo.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slider/slider2.jpg" class="sliderimage" alt="banner1">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                            <h1 class="headline">Misión.</h1>
                            <p class="lead">
                                Ser una compañía que genere beneficios a sus accionistas, colaboradores, clientes y a la comunidad donde operamos, así como seguir nuestros valores
                        empresariales dentro del cumplimiento del marco legal, ético, técnico y de los más altos estándares en materia de gestión de nuestros recursos humanos, calidad, seguridad, salud,
                        ambiente y de responsabilidad social.
                            </p>

                        </div>
                    </div>
                </div>

                <div class="item">
                    <img src="images/slider/slider3.jpg" class="sliderimage" alt="banner2">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                            <h1 class="headline">Visión.</h1>
                            <p class="lead">
                                Convertirnos en una empresa destacada y reconocida por las buenas prácticas técnicas y operacionales en las actividades de fabricación de joyería en grandes volúmenes para exportación en el sector de zonas francas de la Republica Dominicana.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slider/slider4.jpg" class="sliderimage" alt="banner2">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                            <h1 class="headline">Nuestros valores.</h1>

                            <ul>

                                <li>Honestidad</li>
                                <li>Creatividad / innovación</li>
                                <li>transparencia</li>
                                <li>compromiso</li>
                                <li>trabajo en equipo</li>
                                <li>responsabilidad
                                </li>
                                <li>eficacia/eficiencia</li>
                                <li>liderazgo
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slider/slider5.jpg" class="sliderimage" alt="banner2">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                            <h1 class="headline">Líderes del sector manufacturero de joyería en el Caribe.</h1>
                            <p class="lead">
                                Gracias al compromiso ejemplar de nuestros trabajadores, somos capaces de entregar productos con la mayor calidad mientras reducimos costos.
                                Esto nos hace la mejor opción para nuestros clientes.
                            </p>

                        </div>
                    </div>
                </div>
                <div class="item">
                    <img src="images/slider/slider6.jpg" class="sliderimage" alt="banner2">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                            <h1 class="headline">Mejoras continuas.</h1>
                            <p class="lead">Nuestros procesos de mejoras continuas nos permiten reaccionar de forma eficiente ante el siempre cambiante entorno global.</p>

                        </div>
                    </div>
                </div>


            </div>

            <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
        </div>
    </div>

    <div>
        <h1>Nuestros Valores:</h1>
        <ul>
            <li>
                <h2>Honestidad</h2>
                <p>
                    Actuamos de cara a la verdad, con rectitud y en consonancia de lo que 

            decimos y hacemos en cualquier actividad frente a los colaboradores, 

                proveedores, suplidores y clientes.
                </p>
            </li>
            <li>
                <h2>Creatividad/Innovación</h2>
                <p>
                    Tenemos la iniciativa e ingenio para mejorar nuestros productos, procesos,

                    operaciones y optimizar el desempeño de nuestra gente.
                </p>
            </li>
            <li>
                <h2>Transparencía</h2>
                <p>
                    Guiamos el accionar de la empresa y nuestros colaboradores dentro del

                marco de la ética, confianza y el deber de hacer rendición de cuentas 

                sobre los recursos de la compañía asignados.
                </p>
            </li>
            <li>
                <h2>Compromiso</h2>
                <p>
                    Realizamos nuestra labores con la mejor actitud de servicio y en forma

                    segura, poniendo al máximo nuestra capacidad para entregar resultados que 

                    agreguen valor a nuestros clientes y colaboradores. Fomentamos la 

                    actualización de los conocimientos de nuestros colaboradores para alcanzar 

                    mejores resultados que deriven en mayor satisfacción de los clientes y un 

                    mejor desempeño de estos.
                </p>
            </li>
            <li>
                <h2>Trabajo en equipo</h2>
                <p>
                    Unimos nuestro esfuerzos y compartimos un mismo propósito, conocido

                    por todos, para entregar resultados de valor para nuestra organización y 

                    clientes. Trabajamos con entusiasmo, manteniendo una comunicación 

                    efectiva, aportando diferentes puntos de vista, que permiten engrandecer 

                    las ideas y aportes de todos respetando la diversidad.
                </p>
            </li>
            <li>
                <h2>Responsabilidad</h2>
                <p>
                    Asumimos los diferentes retos y escenarios que se nos presentan con

                dedicación identificando las oportunidades y somos conscientes de los deberes 

                y las consecuencias de nuestros actos. Adoptamos sentido de pertenencia 

                optimizando los recursos y minimizando los riesgos en nuestro actuar.
                </p>
            </li>
            <li>
                <h2>Eficacía/Eficiencía</h2>
                <p>
                    Ejecutamos nuestras labores conforme a los estándares establecidos justo a 

                tiempo , aprovechando adecuadamente los recursos disponibles.
                </p>
            </li>
            <li>
                <h2>Liderazgo</h2>
                <p>
                    Dirigimos nuestro equipo de trabajo logrando que trabajen con entusiasmo 

conquistando las metas y los objetivos planteados.
                </p>
            </li>
        </ul>
    </div>
   <%-- <div class="container">
        <div class="row" style="text-align: center; vertical-align: middle">
            <a class="col-xs-2" href="#myCarousel1" role="button" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <div id="myCarousel1" class="carousel slide col-xs-8" data-ride="carousel">
                <!-- Indicators -->
                <ol id="CaruselIndicadores" runat="server" class="carousel-indicators">
                    <li data-target="#myCarousel1" data-slide-to="0"></li>
                    <li data-target="#myCarousel1" data-slide-to="1"></li>
                    <li data-target="#myCarousel1" data-slide-to="2"></li>
                    <li data-target="#myCarousel1" data-slide-to="3"></li>
                </ol>

                <!-- Wrapper for slides -->
                <div class="carousel-inner col-xs-8" role="listbox">
                    <div class="item active">
                        <div class="row">
                            <div class="col-xs-6">
                                <div class="thumbnail">
                                    <img src="..." alt="...">
                                    <div class="caption">
                                        <h3>Thumbnail label 09</h3>
                                        <p>...</p>
                                        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6">
                                <div class="thumbnail">
                                    <div class="caption">
                                        <h3>Thumbnail label 10</h3>
                                        <p>...</p>
                                        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="..." alt="...">
                                    <div class="caption">
                                        <h3>Thumbnail label 11</h3>
                                        <p>...</p>
                                        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="..." alt="...">
                                    <div class="caption">
                                        <h3>Thumbnail label 12</h3>
                                        <p>...</p>
                                        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="item">
                        <div class="row">
                            <div class="col-sm-6 col-md-4">
                                <div class="thumbnail">
                                    <img src="..." alt="...">
                                    <div class="caption">
                                        <h3>Thumbnail label 13</h3>
                                        <p>...</p>
                                        <p><a href="#" class="btn btn-primary" role="button">Button</a> <a href="#" class="btn btn-default" role="button">Button</a></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xs-2" style="height: 100%; vertical-align: middle; display: block;">
                <a href="#myCarousel1" role="button" data-slide="next" style="vertical-align: middle; height: 100%">
                    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>

        </div>
    </div>--%>

</asp:Content>
