﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Intranet.aspx.cs" Inherits="Optimus.Web.WebForms.Intranet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        @media screen and (min-width: 992px) {

            #menu {
                position: fixed;
            }

            #MenuScrollspy {
                display: block;
            }
        }

        @media screen and (max-width: 991px) {

            #menu {
                position: inherit;
            }

            #MenuScrollspy {
                display: none;
            }
        }
    </style>
    <body data-spy="scroll" data-target="#myScrollspy" data-offset="20">
        <div class="container">
            <div class="col-sm-12 col-md-3">
                <div id="menu">
                    <div id="MenuScrollspy" class="panel">
                        <nav id="myScrollspy">
                            <ul class="nav nav-pills nav-stacked">
                                <li class="active"><a href="#section1">Recursos Humanos</a></li>
                                <li><a href="#section2">Seguridad e Higiene</a></li>
                                <li><a href="#section3">Reportes</a></li>
                                <li><a href="#section4">Sistemas</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div id="Favoritos" class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Star Filled-40.png" class="img-rounded" alt="Favoritos">
                            Mas usados.
                        </div>
                        <div class="panel-body" style="font-size: medium;">
                            <ul>
                                <li><a runat="server" href="~/CheckList/Audit.aspx">Auditoria.</a></li>
                                <li><a runat="server" href="/tickets/tickets.aspx">Tickets.</a></li>
                                <li><a runat="server" href="~/HHRR/RequisicionPersonal.aspx">Requerir personal.</a></li>
                                <li><a runat="server" href="/petition/Solicitudes.aspx">Solicitar empleo.</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-9">
                <div id="section1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Conference-40.png" class="img-rounded" alt="Recursos Humanos">
                            Recursos Humanos.
                        </div>
                        <div class="panel-body" style="font-size: medium;">
                            <div class="contanier">
                                <div class="row">
                                    <div class=" col-sm-12 col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Principal.
                                            </div>
                                            <div class="panel-body" style="font-size: medium;">
                                                <ul>
                                                    <li><a runat="server" href="~/HHRR/ListaRequisicionesPersonal.aspx">Requisiciónes.</a></li>
                                                    <li><a runat="server" href="~/HHRR/ListaPuesto.aspx">Puestos.</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-12 col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Configuración.
                                            </div>
                                            <div class="panel-body" style="font-size: medium;">
                                                <ul>
                                                    <li><a runat="server" href="~/HHRR/AdministrarCompetencias.aspx">Competencias.</a></li>
                                                    <li><a runat="server" href="~/HHRR/AdministrarNivelesSalarial.aspx">Niveles salariales.</a></li>
                                                    <li><a runat="server" href="~/HHRR/AdministrarCompensacionesBeneficios.aspx">Compensaciones y beneficios.</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-sm-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Reportes.
                                            </div>
                                            <div class="panel-body" style="font-size: medium;">
                                                <ul>
                                                    <li>
                                                        <asp:HyperLink ID="LinkPerfilCompetenciasLaborales" runat="server">Perfil de competencias laborales.</asp:HyperLink></li>
                                                    <li>
                                                        <asp:HyperLink ID="LinkSolicitudesByCedula" runat="server">Solicitud laboral por cédula.</asp:HyperLink></li>
                                                    <li>
                                                        <asp:HyperLink ID="LinkSolicitudesMasivoByRangoFecha" runat="server">Solicitud laboral por rango de fecha.</asp:HyperLink></li>
                                                    <li>
                                                        <asp:HyperLink ID="LinkListadosSolicitudes" runat="server">Solicitud laboral por rango de fecha y posición.</asp:HyperLink></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="section2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Biohazard-40.png" class="img-rounded" alt="Seguridad e Higiene">
                            Seguridad e Higiene.
                        </div>
                        <div class="panel-body" style="font-size: medium;">
                            <div class="contanier">
                                <div class="row">
                                    <div class=" col-sm-12 col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Principal.
                                            </div>
                                            <div class="panel-body" style="font-size: medium;">
                                                <ul>
                                                    <li><a runat="server" href="~/CheckList/Audit.aspx">Auditoria.</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" col-sm-12 col-md-6">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Configuración.
                                            </div>
                                            <div class="panel-body" style="font-size: medium;">
                                                <ul>
                                                    <li><a runat="server" href="~/CheckList/SettingAudit.aspx">Configuración.</a></li>
                                                    <li><a runat="server" href="~/CheckList/AuditCategories.aspx">Mantenimiento de categorías.</a></li>
                                                    <li><a runat="server" href="~/CheckList/AuditIndicator.aspx">Mantenimiento de indicadores.</a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class=" col-sm-12 col-md-12">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                Reportes.
                                            </div>
                                            <div class="panel-body" style="font-size: medium;">
                                                <ul>
                                                    <li>
                                                        <asp:HyperLink ID="LinkReporteGeneralAuditoria" runat="server">Reporte general de auditoria.</asp:HyperLink></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="section3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Document-40.png" class="img-rounded" alt="Reportes">
                            Reportes.
                        </div>
                        <div class="panel-body" style="font-size: medium;">
                            <ul>
                                <li>
                                    <asp:HyperLink ID="LinkPendienteParcializarPorSemanas" runat="server">Pendiente parcializar por semanas.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkProcesadoEnCadena" runat="server">Procesado en cadena.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkProcesadoPorSemanas" runat="server">Procesado por semanas.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkTrabajosAbiertos" runat="server">Trabajos abiertos.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkEficiencia" runat="server">Eficiencia.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkPagoDeProduccion" runat="server">Pago de producción.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkDetalleTrabajos" runat="server">Detalle de trabajos.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkTrabajosAbiertosNoParcializadosPorSemana" runat="server">Trabajos abiertos no parcializados por semana.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkTrabajosDetenidos" runat="server">Trabajos detenidos.</asp:HyperLink></li>
                                <li>
                                    <asp:HyperLink ID="LinkTrackingV2" runat="server">Tracking V2.</asp:HyperLink></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div id="section4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Virtual Machine 2-40.png" class="img-rounded" alt="Sistemas.">
                            Sistemas.
                        </div>
                        <div class="panel-body" style="font-size: medium;">
                            <ul>
                                <li><a runat="server" href="/tickets/Maintenance/Maintenance.aspx">Asignar encargado de departamento.</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</asp:Content>
