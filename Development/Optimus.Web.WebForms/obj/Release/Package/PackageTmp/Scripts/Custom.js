﻿
$(document).ready(function () {
    $('#sideMenu').BootSideMenu();
    $('.datepicker').datepicker();
});

function preventLettersOnKeyPress(event) {
    if (event.keyCode == 46 || event.keyCode == 8) {

    }
    else {
        if (event.keyCode > 95 && event.keyCode < 106) {

            return;
        }
        if ((event.keyCode < 48 || event.keyCode > 57)) {
            event.preventDefault();
        }
    }

}

function renderValidationMessages(errors) {
    $('#bs').empty();
    $('#bs').append("<div id='BootstrapMessage'></div>");
    element  = $('#BootstrapMessage');
    if (element) {
        $(element).append("<a href=\"#\" class=\"close\" data-dismiss=\"alert\">&times;</a>" +
            "<ul>");
        errors.forEach(function showValidationMessages(item, index, error) {
            $(element).append("<li>" + item + "</li>");
        });
        $(element).append("</ul>");
        $(element).addClass( "alert alert-success" );
    }
    
}



function showModalMessage(header, message) {
    $('#modalHeader').text(header);
    $('#modalMsg').text(message);
    $('#messageModal').modal();
}