﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tickets.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.Tickets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        @media only screen and (max-width:1000px) {
            .tab-content {
                text-align: center;
                max-width: 760px;
            }
        }

        @media only screen and (min-width:800px) {
            #txtTitulo, #txtDescription {
                min-width: 320px;
            }
        }

        #guardar {
            margin-top: 1%;
        }
    </style>

    <script>

        $(document).ready(function () {
            getDepartmentHead();
            setTicketStatus();
            setPorcentStatus();
            initSample();
        });

        function setPorcentStatus() {
            $('.progress-bar').each(function () {
                var status = $(this).text();
                if (status.trim() == "100%") {
                    $(this).addClass('progress-bar-success');
                }
            });

        }

        function GuardarTickets() {
            var comment = CKEDITOR.instances.editor.getData();
            __doPostBack('GuardarTickets', comment)

        }
        function setTicketStatus() {
            $('.submitted').each(function () {
                var estatus = $(this).text();
                switch (estatus) {
                    case "Sometido": {
                        $(this).addClass('btn btn-default');
                    } break;
                    case "Cancelled": {
                        $(this).addClass('btn btn-danger');
                    } break;
                    case "Approved": {
                        $(this).addClass('btn btn-success');
                    } break;
                    default: {
                        $(this).addClass('btn btn-default');
                    } break;
                }

            });
        }

        function getDepartmentHead() {
            $('#imgModal').modal();
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetDepartmentHead',
                data: '{Department:\'' + $('#MainContent_drpDepartamento option:selected').text() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#empdetails').empty();
                    if (data.d) {

                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                        $('#empdetails').append("<img alt='Encargado' id='imgPic' />");

                        document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                    }
                    else {
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'> N/A </small> </h5>");
                    }
                    $('#imgModal').modal('hide');
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }


        $(function () {
            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').click();
            }
        });


    </script>
    <div id="bs">
    </div>
    <div id="Tabs" role="tabpanel">
        <div class="row">
            <div id="ticketscontainer">
                <h1>Tickets Dashboard</h1>
                <label id="lblEtiqueta"></label>
                <ul class="nav nav-pills nav-justified" id="myTab">
                    <li><a data-toggle="tab" role="tab" href="#someter">Someter Ticket</a></li>
                    <li class="active"><a data-toggle="tab" role="tab" href="#mistickets">Tickets Enviados</a></li>
                    <li><a data-toggle="tab" role="tab" href="#pendientes">Tickets sin Asignación</a></li>
                    <li><a data-toggle="tab" role="tab" href="#Asignados">Tickets asignados a mi </a></li>
                    <li><a data-toggle="tab" role="tab" href="#Cerrados">Tickets Cerrados</a></li>
                    <li><a data-toggle="tab" role="tab" href="#Todos">Todos los Tickets</a></li>
                </ul>
            </div>
            <asp:HiddenField ID="TabName" runat="server" Value="someter" />
            <div class="tab-content">
                <div id="someter" role="tabpanel" class="tab-pane fade">
                    <h3>Someter ticket</h3>
                    <div id="deptcol" class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Departamento:" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="drpDepartamento" onchange="return getDepartmentHead()" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Título / Solicitud" Font-Bold="true"></asp:Label>
                                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtTitulo" CssClass="form-control" placeholder="Ingrese una solicitud"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Descripción" Font-Bold="true"></asp:Label>
                                <%--<asp:TextBox runat="server" ClientIDMode="Static" ID="txtDescription" Style="min-width: 90%" CssClass="form-control" placeholder="Ingrese una descripción" TextMode="MultiLine"></asp:TextBox>--%>
                                <div class="adjoined-bottom">
                                    <div class="grid-container">
                                        <div class="grid-width-100">
                                            <div id="editor">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="guardar">
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnGuardar" Text="Guardar" class="btn btn-primary" ClientIDMode="Static" onsubmit="return validate()" OnClientClick="GuardarTickets()" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="empdetails">
                        </div>
                    </div>
                </div>
                <div id="mistickets" role="tabpanel" class="tab-pane fade in active">
                    <h3>Tickets Enviados</h3>
                    <div class="container">
                        <div class="row">
                            <asp:Label Text="Filtrar por Estado:" runat="server" Font-Bold="true"></asp:Label>
                            <asp:DropDownList runat="server" ID="FiltroMisTickets" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="CambioFiltro">
                                <asp:ListItem Value="0">Todos</asp:ListItem>
                                <asp:ListItem Value="1" Selected="true">Sin Terminar</asp:ListItem>
                                <asp:ListItem Value="2">Terminado</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ListView ID="lstTickets" runat="server" OnPagePropertiesChanging="lstTickets_PagePropertiesChanging">
                                <LayoutTemplate>
                                    <div class="table-responsive">
                                        <table class="table  table-stripe">
                                            <thead>
                                                <th>Fecha</th>
                                                <th>Titulo</th>
                                                <th>#Ticket</th>
                                                <th>Departamento</th>
                                                <th>Porcentaje</th>
                                                <th>Estado</th>
                                                <th>Asignado a:</th>
                                            </thead>
                                            <tbody class="myTable">
                                                <tr runat="server" id="itemPlaceholder" />
                                            </tbody>
                                        </table>
                                    </div>
                                    <asp:DataPager ID="TicketsPager" runat="server" PagedControlID="lstTickets" PageSize="20">
                                        <Fields>
                                            <asp:NumericPagerField ButtonCount="10"
                                                PreviousPageText="<--"
                                                NextPageText="-->" />
                                        </Fields>
                                    </asp:DataPager>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr runat="server">
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                        <td>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="<%#Eval("PorcentProgress ")%>" aria-valuemin="0" aria-valuemax="100" style='width: <%#Eval("PorcentProgress")+"%" %>'>
                                                    <%#Eval("PorcentProgress")+"%"%>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Responsible") %>'></asp:Label></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                <div id="pendientes" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets Pendientes de Asignación</h3>
                    <div class="row">
                        <asp:ListView ID="lstTicketsPendientes" ClientIDMode="Static" runat="server" OnPagePropertiesChanging="lstTicketsPendientes_PagePropertiesChanging">
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Auto Asignar</th>
                                        <th>Departamento</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="PendientesPager" runat="server" PagedControlID="lstTicketsPendientes" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>

                            <ItemTemplate>
                                <tr runat="server">
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label>

                                    </td>
                                    <td><asp:Button Text="Tomar" CssClass="btn btn-primary" runat="server"  CommandName='<%# Eval("TicketNumber") %>'  OnCommand ="AutoAsignar"/></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div id="Asignados" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets asignados a mi</h3>
                    <div class="row">
                        <asp:ListView ID="lstTicketsAsignados" ClientIDMode="Static" OnPagePropertiesChanging="lstTicketsAsignados_PagePropertiesChanging" runat="server">
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Departamento</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="AsignadosPager" runat="server" PagedControlID="lstTicketsAsignados" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr runat="server">
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'> </asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div id="Cerrados" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets Cerrados</h3>
                    <div class="row">
                        <asp:ListView ID="lstTicketsCerrados" ClientIDMode="Static" runat="server" OnPagePropertiesChanging="lstTicketsCerrados_PagePropertiesChanging">
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Departamento</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="CerradosPager" runat="server" PagedControlID="lstTicketsCerrados" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr runat="server">
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'> </asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div id="Todos" role="tabpanel" class="tab-pane fade">
                    <h3>Todos los Tickets</h3>
                    <div class="container">
                        <div class="row">
                            <asp:Label Text="Filtrar por Estado:" runat="server" Font-Bold="true"></asp:Label>
                            <asp:DropDownList runat="server" ID="FiltroTodos" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="CambioFiltroTodos">
                                <asp:ListItem Value="0">Todos</asp:ListItem>
                                <asp:ListItem Value="1">Tickest enviados del departamento</asp:ListItem>
                                <asp:ListItem Value="2">Tickest recibidos del departamento</asp:ListItem>
                                <asp:ListItem Value="3">Todos mis Tickets</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ListView ID="lstTodosTickets" runat="server" OnPagePropertiesChanging="lstTodosTickets_PagePropertiesChanging">
                                <LayoutTemplate>
                                    <div class="table-responsive">
                                        <table class="table  table-stripe">
                                            <thead>
                                                <th>Fecha</th>
                                                <th>Titulo</th>
                                                <th>#Ticket</th>
                                                <th>Departamento</th>
                                                <th>Porcentaje</th>
                                                <th>Estado</th>
                                            </thead>
                                            <tbody class="myTable">
                                                <tr runat="server" id="itemPlaceholder" />
                                            </tbody>
                                        </table>
                                    </div>
                                    <asp:DataPager ID="TodosPager" runat="server" PagedControlID="lstTodosTickets" PageSize="20">
                                        <Fields>
                                            <asp:NumericPagerField ButtonCount="10"
                                                PreviousPageText="<--"
                                                NextPageText="-->" />
                                        </Fields>
                                    </asp:DataPager>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr runat="server">
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                        <td>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="<%#Eval("PorcentProgress ")%>" aria-valuemin="0" aria-valuemax="100" style='width: <%#Eval("PorcentProgress")+"%" %>'>
                                                    <%#Eval("PorcentProgress")+"%"%>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="imgModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Loading...</h1>
                </div>
                <div class="modal-body">
                    <div class="progress progress-striped active">
                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
