﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TicketsDetalle.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.TicketsDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <script>
        $(document).ready(function () {
            initSample();
            setPorcentStatus();
            ReadOnlyComment();

        });
    </script>
    <script type="text/javascript">

        function setPorcentStatus() {
            $('.progress-bar').each(function () {
                var status = $(this).text();
                if (status.trim() == "100%") {
                    $(this).addClass('progress-bar-success');
                }
            });
        }
        function ReadOnlyComment() {
            if ($('#MainContent_lblEstado').text() == 'Validate' || $('#MainContent_lblEstado').text() == 'Cancelled') {
                CKEDITOR.config.readOnly = true;
            }

        }
        function MostrarAlerta() {
            $('#myModal').modal('show');
        }
        function GuardarComentario() {
            var comment = CKEDITOR.instances.editor.getData();
            __doPostBack('GuardarComment', comment)

        }
    </script>
    <div class="container">
        <br />
        <asp:HiddenField runat="server" ID="HDFieldUserID" />
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="lblTitulo" Font-Size="X-Large" Font-Bold="true"></asp:Label>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-6">
                        <label>No Tickets:</label>
                        <asp:Label runat="server" ID="lblTickets"></asp:Label>
                    </div>
                    <div class="col-md-6">
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Creado por:</label>
                        <asp:Label runat="server" ID="lblCreadoPor"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <label>Fecha Creación:</label>
                        <asp:Label runat="server" ID="lblFecha"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Departamento Asignado:</label>
                        <asp:Label runat="server" ID="lblDeptAsignado"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <label>Departamento adjudico:</label>
                        <asp:Label runat="server" ID="lblDeptAjudico"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Estado: </label>
                        <asp:Label runat="server" ID="lblEstado"></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                 <a href="#cambioEstado" id="cambioEstadoA" runat="server" data-toggle="collapse">
                     <i class="glyphicon glyphicon-asterisk"></i>Cambiar Estado</a>
                        <div id="cambioEstado" class="collapse">
                            <span style="font-weight: bold">Cambiar estado:</span>
                            <asp:DropDownList runat="server" ID="dropEstadosTickets" OnSelectedIndexChanged="ActualizarEstado" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Usuarios Asignados: </label>
                        <asp:Label runat="server" ID="lblUsernameAssig"></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                <a href="#cambioUsuario" id="cambioUsuarioA" runat="server" data-toggle="modal" data-target="#ModalSeleccionUsuario">
                    <i class="glyphicon glyphicon-asterisk"></i>Asignar nuevo Usuario</a>
                    </div>
                    <div class="col-md-6">
                        <div style="float: left">
                            <label>Porcentaje: </label>
                        </div>
                        <div class="progress" style="width: 100px; float: left; padding-right: 1px">
                            <div runat="server" id="divProgreso" class="progress-bar progress-bar-striped active" role="progressbar"
                                aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <%--Codigo para la parte de asignar varios usuarios a un tickets--%>
                        <asp:Repeater runat="server" ID="repetar1" OnItemDataBound="repeater_ItemDataBound">
                            <ItemTemplate>
                                <span class="tag label label-pill label-primary">
                                    <span style="font-size: x-small"><%#Eval("Responsible")%> </span>
                                    <asp:LinkButton ID="ResponsibleLink" CssClass="resLink" runat="server" OnCommand="EliminarResponsible" CommandName='<%#Eval("EmployeesIDResponsible")%>' CommandArgument='<%#Eval("TicketNumber")%>'><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></asp:LinkButton>
                                </span>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="cambioPorcentaje">
                            <asp:RadioButtonList ID="PorcentajeRadioButtonList" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="GuardarPorcentaje" AutoPostBack="true">
                                <asp:ListItem Value="25">25%</asp:ListItem>
                                <asp:ListItem Value="50">50%</asp:ListItem>
                                <asp:ListItem Value="75">75%</asp:ListItem>
                                <asp:ListItem Value="100">100%</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <asp:Button runat="server" ID="ValidateButton" OnClick="ActualizarValidate" />
                    </div>
                    <div class="col-md-6">
                        <a runat="server" href="/tickets/tickets.aspx">Volver a Tickets</a>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-4">
                        <label>Descripción:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p runat="server" id="PDescripcion">
                        </p>
                    </div>
                </div>
                <br />
                <br />
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" role="tab" href="#ComentariosTab"><i class="glyphicon glyphicon-comment"></i>
                            <label style="font: bold">Comentarios: </label>
                        </a></li>
                        <li><a data-toggle="tab" role="tab" href="#HistorialTab">Historial</a></li>
                    </ul>
                </div>
                <div>
                    <div class="tab-content">
                        <div id="ComentariosTab" role="tabpanel" class="tab-pane fade in active">
                            <br />
                            <div class="col-md-12">
                                <asp:Repeater runat="server" ID="CommentRepeater">
                                    <ItemTemplate>
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <asp:Label runat="server" Text='<%#Eval("UserName")%>' Font-Bold="true"></asp:Label>
                                                            </div>
                                                            <div style="float: right">
                                                                <asp:Label runat="server" Text='<%#Eval("Datetime")%>' Font-Bold="true"></asp:Label>
                                                            </div>
                                                            <hr />
                                                        </div>
                                                        <div>
                                                            <p runat="server" id="PCometarios">
                                                                <%# Eval("Comentarios")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="row" id="comment" runat="server">
                                <div class="col-md-12">
                                    <div class="adjoined-bottom">
                                        <div class="grid-container">
                                            <div class="grid-width-100">
                                                <div id="editor">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" style="padding-top: 10px">
                                    <asp:Button runat="server" Text="Agregar comentario" CssClass="btn btn-primary" OnClientClick="GuardarComentario()" />
                                </div>
                            </div>
                        </div>
                        <div id="HistorialTab" role="tabpanel" class="tab-pane fade">
                            <br />
                            <asp:Repeater runat="server" ID="HistorialRepeater">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <asp:Label runat="server" Text='<%#Eval("UserName")%>' Font-Bold="true"></asp:Label>
                                                        </div>
                                                        <div style="float: right">
                                                            <asp:Label runat="server" Text='<%#Eval("Datetime")%>' Font-Bold="true"></asp:Label>
                                                        </div>
                                                        <hr />
                                                    </div>
                                                    <div>
                                                        <p runat="server" id="PCometarios">
                                                            <%# Eval("Comentarios")%>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
                <%-- <div class="row">
                    <div class="col-md-12">
                        <asp:Repeater runat="server" ID="CommentRepeater">
                            <ItemTemplate>
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="row">
                                                    <div class="col-md-6">
                                                        <asp:Label runat="server" Text='<%#Eval("UserName")%>' Font-Bold="true"></asp:Label>
                                                    </div>
                                                    <div style="float: right">
                                                        <asp:Label runat="server" Text='<%#Eval("Datetime")%>' Font-Bold="true"></asp:Label>
                                                    </div>
                                                    <hr />
                                                </div>
                                                <div>
                                                    <p runat="server" id="PCometarios">
                                                        <%# Eval("Comentarios")%>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>--%>
            </div>
        </div>
    </div>
    <%--Codigo para la parte de asignar varios usuarios a un tickets--%>
    <div id="ModalSeleccionUsuario" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Asignación de usuarios </h4>
                </div>
                <div class="modal-body">
                    <asp:ListBox runat="server" Style="min-width: 100%" ID="ListUsuariosDepartamento" SelectionMode="Multiple"></asp:ListBox>
                    <p class="text-warning"><small>Mantenga presionada la tecla control para seleccionar varios usuarios</small></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <asp:Button runat="server" class="btn btn-primary" Text="Guardar" OnClick="ActualizarResponsable" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
