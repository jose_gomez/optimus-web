﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarCompensacionesBeneficios.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarCompensacionesBeneficios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Compensaciones y beneficios</h3>
        </div>
        <div class="panel-body">
            <div class="panel">
                <asp:DropDownList ID="ddlCompensacionesBeneficios" runat="server" CssClass="form-control col-xs-12" AutoPostBack="True" OnSelectedIndexChanged="ddlCompensacionesBeneficios_SelectedIndexChanged"></asp:DropDownList><asp:Button ID="btnNew" CssClass="btn btn-primary" runat="server" Text="Nuevo" OnClick="btnNew_Click" />
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">

                    <h4 class="panel-title">Compensación o beneficio:
                        <asp:Label ID="lblCompensacionBeneficioID" runat="server" Text=""></asp:Label></h4>

                </div>
                <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="col-xs-12">Compensación o beneficio:</h4>
                        </div>
                        <div class="col-xs-12">
                            <asp:TextBox ID="txtCompensacionBeneficio" runat="server" CssClass="form-control col-xs-12" MaxLength="50"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <h4 class="col-xs-12">Porcentaje de incremento:</h4>
                            <div class="checkbox">
                                <asp:CheckBox ID="chckPorcentaje" runat="server" AutoPostBack="True" OnCheckedChanged="chkPorcentaje_CheckedChanged" />
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <asp:TextBox ID="txtPorcentaje" runat="server" CssClass="form-control col-xs-12" TextMode="Number" Enabled="False"></asp:TextBox>
                        </div>
                    </div>

                </div>
                <div class="panel-footer" style="vertical-align: middle; text-align: right">
                    <asp:Button ID="btnSaveCompensacionBeneficio" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSaveCompensacionBeneficio_Click" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
