﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalleRequisicionPersonal.aspx.cs" Inherits="Optimus.Web.WebForms.Requisicion_de_personal.DetalleRequisicionPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Requisicion de personal
            </h3>
        </div>
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Información general.
                </div>
                <div class="panel-body">
                    <div class="row col-xs-12 col-sm-6">
                        <div class="col-xs-12 col-md-6">
                            <b>Puesto:</b>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <i>
                                <asp:Label ID="lblPuestos" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12 col-sm-6">
                        <div class="col-xs-12 col-md-6">
                            <b>Cantidad de empleados requeridos:</b>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <i>
                                <asp:Label ID="lblCantidadEmpleados" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12 col-sm-6">
                        <div class="col-xs-12 col-md-6">
                            <b>Supervisado por:</b>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <i>
                                <asp:Label ID="lblSupervisor" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12 col-sm-6">
                        <div class="col-xs-12 col-md-6">
                            <b>Departamento:</b>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <i>
                                <asp:Label ID="lblDepartamentos" runat="server" /></i>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-body">
                <div class="row col-xs-12">
                <div class="col-xs-12 col-md-4">
                    <b>Motivo:</b>
                </div>
                <div class="col-xs-12 col-md-8">
                    <i>
                        <asp:Label ID="lblMotivo" runat="server" /></i>
                </div>
            </div>
            </div>
            
        </div>
    </div>
</asp:Content>
