﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarNivelesSalarial.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarNivelesSalarial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Niveles salariales</h3>
        </div>
        <div class="panel-body">
            <div class="panel">
                <asp:DropDownList ID="ddlNiveles" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlNiveles_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList><asp:Button ID="btnNew" CssClass="btn btn-primary" runat="server" Text="Nuevo" OnClick="btnNew_Click" />
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h4 id="title" runat="server" class="panel-title">Numero:
                        <asp:Label ID="lblNumero" runat="server" Text=""></asp:Label></h4>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlNiveles" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnNew" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel5" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row col-lg-12">
                                <h4 class="col-xs-12">Nivel:</h4>
                                <asp:TextBox ID="txtName" CssClass="form-control col-xs-12" runat="server" MaxLength="50" Width="100%"></asp:TextBox>
                                <h4 class="col-xs-12">Descripción:</h4>
                                <asp:TextBox ID="txtDescripcion" CssClass="form-control col-xs-12" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                <h4 class="col-xs-12">Secuencia:</h4>
                                <asp:TextBox ID="txtSecuencia" CssClass="form-control col-xs-12" runat="server" TextMode="Number" Width="100%"></asp:TextBox>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlNiveles" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="btnNew" EventName="Click" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="panel-footer" style="vertical-align: middle; text-align: right">
                    <asp:Button ID="btnSaveCompetencia" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSaveCompetencia_Click" />
                </div>
            </div>
        </div>
        <div class="panel-footer">
        </div>
    </div>
</asp:Content>
