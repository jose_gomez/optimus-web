﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarCompetencias.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarCompetencias" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Competencias</h3>
        </div>
        <div class="panel-body">

            <div class="panel">
                <h4>Categorías:</h4>
                <asp:DropDownList ID="ddlCategoriasLista" runat="server" CssClass="form-control col-xs-12" AutoPostBack="True" OnSelectedIndexChanged="ddlCategoriasLista_SelectedIndexChanged"></asp:DropDownList><asp:Button ID="btnNew" CssClass="btn btn-primary" runat="server" Text="Nuevo" OnClick="btnNew_Click" />
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel5" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h4 class="panel-title">Categoría:
                        <asp:Label ID="lblCategoriaID" runat="server" Text=""></asp:Label></h4>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCategoriasLista" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="col-xs-12">Categoría:</h4>
                                </div>
                                <div class="col-xs-12">
                                    <asp:TextBox ID="txtCategoria" runat="server" CssClass="form-control col-xs-12" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCategoriasLista" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="panel-footer" style="vertical-align: middle; text-align: right">
                    <asp:Button ID="btnSaveCategoria" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSaveCategoria_Click" />
                </div>
            </div>

            <div class="panel">
                <h4>Competencias:</h4>
                <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:DropDownList ID="ddlCompetencias" runat="server" CssClass="form-control col-xs-12" AutoPostBack="True" OnSelectedIndexChanged="ddlCompetencias_SelectedIndexChanged"></asp:DropDownList>
                    </ContentTemplate>
                    <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="ddlCategoriasLista" EventName="SelectedIndexChanged" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:Button ID="btnNewComp" CssClass="btn btn-primary" runat="server" Text="Nuevo" OnClick="btnNewComp_Click" />
            </div>


            <div class="panel panel-default">
                <div class="panel-heading">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel4" UpdateMode="Conditional">
                        <ContentTemplate>
                            <h4 class="panel-title">Competencia:
                                <asp:Label ID="lblCompetenciaID" runat="server" Text=""></asp:Label></h4>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCategoriasLista" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlCompetencias" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="panel-body">
                    <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                        <ContentTemplate>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="col-xs-12">Competencia:</h4>
                                </div>
                                <div class="col-xs-12">
                                    <asp:TextBox ID="txtCompetencia" runat="server" CssClass="form-control col-xs-12" MaxLength="150"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-xs-12">
                                    <h4 class="col-xs-12">Descripción:</h4>
                                </div>
                                <div class="col-xs-12">
                                    <asp:TextBox ID="txtDescripcion" runat="server" CssClass="form-control col-xs-12" TextMode="MultiLine"></asp:TextBox>
                                </div>
                            </div>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="ddlCategoriasLista" EventName="SelectedIndexChanged" />
                            <asp:AsyncPostBackTrigger ControlID="ddlCompetencias" EventName="SelectedIndexChanged" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
                <div class="panel-footer" style="vertical-align: middle; text-align: right">
                    <asp:Button ID="btnSaveCompetencia" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSaveCompetencia_Click" />
                </div>
            </div>

        </div>
        <div class="panel-footer">
        </div>
    </div>
</asp:Content>
