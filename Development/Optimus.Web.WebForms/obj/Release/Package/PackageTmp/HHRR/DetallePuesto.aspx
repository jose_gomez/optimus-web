﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetallePuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.DetallePuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Puesto
            </h3>
        </div>
        <div class="panel-body">
            <div class="col-xs-12">
                <h4>Nombre de la posición:</h4>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="lblPuestoName" Text="" runat="server" />
            </div>
            <div class="col-xs-12">
                <h4>Departamento:</h4>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="lblDepartamento" Text="" runat="server" />
            </div>
            <div class="col-xs-12">
                <h4>Superior:</h4>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="lblSuperior" Text="" runat="server" />
            </div>
            <div id="divSub" runat="server" class="col-xs-12">
                
            </div>
            <div class="col-xs-12">
                <h4>Descripción:</h4>
            </div>
            <div class="col-xs-12">
                <asp:Label ID="lblDescripcion" Text="" runat="server" />
            </div>
        </div>
        <div class="panel-footer">
            <asp:HyperLink ID="HyperLink1" runat="server">Editar</asp:HyperLink>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Análisis de puesto</h3>
        </div>
        <div class="panel-body">
            <span id="spanprueba" runat="server">
            </span>
        </div>
        <div class="panel-footer">
            <asp:HyperLink ID="HyperLink3" runat="server">Editar</asp:HyperLink>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Competencias</h3>
        </div>
        <div class="panel-body">
            <span id="span1" runat="server"></span>
        </div>
        <div class="panel-footer">
            <asp:HyperLink ID="HyperLink2" runat="server">Editar</asp:HyperLink>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Matriz salarial</h3>
        </div>
        <div class="panel-body">
            <span id="span2" runat="server"></span>
            <asp:GridView ID="gvMatriz" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
                <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                <Columns>
                    <asp:BoundField DataField="MarcoGeneral" HeaderText="Marco general" />
                    <asp:BoundField DataField="D" HeaderText="D">
                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="C" HeaderText="C">
                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"/>
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PROMEDIO" HeaderText="PROMEDIO">
                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="B" HeaderText="B">
                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"/>
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                    <asp:BoundField DataField="A" HeaderText="A">
                    <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle"/>
                    <ItemStyle HorizontalAlign="Right" />
                    </asp:BoundField>
                </Columns>
                <EditRowStyle BackColor="#999999" />
                <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
                <SortedAscendingCellStyle BackColor="#E9E7E2" />
                <SortedAscendingHeaderStyle BackColor="#506C8C" />
                <SortedDescendingCellStyle BackColor="#FFFDF8" />
                <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
            </asp:GridView>
        </div>
        <div class="panel-footer">
            <asp:HyperLink ID="HyperLink4" runat="server">Editar</asp:HyperLink>
        </div>
    </div>
</asp:Content>
