﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.ListaPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Lista de puestos</h3>
        </div>
        <div class="panel-body">
            <asp:GridView ID="gvPuestos" CssClass="table table-hover table-condensed medium-top-margin col-sx-12" runat="server" AutoGenerateColumns="False" BorderStyle="None">
                <Columns>
                    <asp:TemplateField HeaderText="Nombre">
                        <ItemTemplate>
                            <a href='/HHRR/DetallePuesto?ID=<%# Eval("PositionID") %>'><%# Eval("PositionName") %></a>
                        </ItemTemplate>
                        <ControlStyle BorderStyle="None" />
                        <HeaderStyle Font-Size="Large" HorizontalAlign="Justify" VerticalAlign="Middle" BorderStyle="None" />
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="panel-footer">
            <asp:HyperLink ID="HyperLink1" runat="server">Registrar nuevo puesto</asp:HyperLink>
        </div>
    </div>
</asp:Content>
