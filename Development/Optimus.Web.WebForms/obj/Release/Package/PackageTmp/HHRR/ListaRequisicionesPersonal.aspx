﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaRequisicionesPersonal.aspx.cs" Inherits="Optimus.Web.WebForms.Requisicion_de_personal.ListaRequisicionesPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .scrolling-table-container {
            height: 378px;
            overflow-y: scroll;
            overflow-x: hidden;
        }
    </style>

    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Requisiciones de personal</h3>
        </div>
        <div class="panel-body">
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Requisiciones para nuevos puestos. <span id="CantidadNuevoPuesto" runat="server" class="badge">5</span></a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion1">
                                <asp:GridView ID="gvNuevos" CssClass="table table-hover table-condensed small-top-margin col-sx-12" runat="server" AutoGenerateColumns="False">
                                    <Columns>
                                        <asp:TemplateField HeaderText="ID">
                                            <ItemTemplate>
                                                <a href='/HHRR/DetalleRequisicionPersonal?ID=<%# Eval("PersonalRequisitionID") %>'><%# Eval("PersonalRequisitionID") %></a>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="PositionName" HeaderText="Puesto" />
                                        <asp:BoundField DataField="QuantityEmployees" HeaderText="Cantidad de empleados" />
                                        <asp:BoundField DataField="HRDepartment.Name" HeaderText="Departamento" />
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:LinkButton ID="lbttnCrearPuesto" runat="server">Crear Puesto</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Requisiciones para puestos existentes. <span id="CantidadPuestosExistentes" runat="server" class="badge">5</span></a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                            <div class="panel-group" id="accordion2">
                                <div class="panel panel-success">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse21">Aprobados <span id="ExistentesAprobados" runat="server" class="badge">5</span></a>
                                        </h4>
                                    </div>
                                    <div id="collapse21" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <asp:GridView ID="gvExistentesAprobados" CssClass="table table-hover table-condensed small-top-margin col-sx-12" runat="server" AutoGenerateColumns="False" OnRowDataBound="gvExistentesAprobados_RowDataBound">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="Seleccionar">
                                                        <ItemTemplate>
                                                            <a href='/HHRR/DetallePuesto?ID=<%# Eval("PositionID") %>'><%# Eval("PositionName") %></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="QuantityEmployees" HeaderText="Cantidad de empleados" />
                                                    <asp:BoundField DataField="HRDepartment.Name" HeaderText="Departamento" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <a href='/HHRR/DetalleRequisicionPersonal?ID=<%# Eval("PersonalRequisitionID") %>'>Ver detalles</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:HyperLink ID="CantidatosLink" runat="server">Ver candidatos</asp:HyperLink>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <a href='/petition/Solicitudes'>LLenar solicitud</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                                <div class="panel panel-warning">
                                    <div class="panel-heading">
                                        <h4 class="panel-title">
                                            <a data-toggle="collapse" data-parent="#accordion2" href="#collapse22">Pendientes <span id="ExistentesPendientes" runat="server" class="badge">5</span></a>
                                        </h4>
                                    </div>
                                    <div id="collapse22" class="panel-collapse collapse">
                                        <div class="panel-body">
                                            <asp:GridView ID="gvExistentesPendientes" CssClass="table table-hover table-condensed small-top-margin col-sx-12" runat="server" AutoGenerateColumns="False">
                                                <Columns>
                                                    <asp:TemplateField HeaderText="ID">
                                                        <ItemTemplate>
                                                            <a href='/HHRR/DetalleRequisicionPersonal?ID=<%# Eval("PersonalRequisitionID") %>'><%# Eval("PersonalRequisitionID") %></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Seleccionar">
                                                        <ItemTemplate>
                                                            <a href='/HHRR/DetallePuesto?ID=<%# Eval("PositionID") %>'><%# Eval("PositionName") %></a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="QuantityEmployees" HeaderText="Cantidad de empleados" />
                                                    <asp:BoundField DataField="HRDepartment.Name" HeaderText="Departamento" />
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <a href='/HHRR/DetalleRequisicionPersonal?ID=<%# Eval("PersonalRequisitionID") %>'>Ver candidatos</a>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                            </asp:GridView>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
