﻿using iTextSharp.text.pdf;
using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarPropuesta : System.Web.UI.Page
    {

        #region Private Fields

        private const string URL_DETALLE_CANDIDATO = "~/HHRR/DetalleCandidato.aspx?ID={0}";
        private const string URL_PARAMETER_CANDIDATE_ID = "CandidateID";
        private const string URL_PARAMETER_WAGE_PROPOSAL_ID = "WageProposalID";

        #endregion Private Fields

        #region Public Methods

        public void SendMail(Byte[] bytes)
        {
            var candidate = CandidateDataHelper.Get(Core.OptimusApiUrls.Candidatos_GET_BY_ID.Replace("{id}", ObtenerCandidateID().ToString()));
            MailMessage mm = new MailMessage();
            mm.Subject = "Propuesta salarial.";
            mm.Body = "Saludos.<br>Nos dirigimos a usted cordialmente ofertando nuestra propuesta salarial, adjunto las informaciones de lugar.Quedamos en espera de su confirmación y posible fecha de inicio.";
            mm.Attachments.Add(new Attachment(new MemoryStream(bytes), "Propuesta.pdf"));
            mm.IsBodyHtml = true;
            //MailAddress ma = new MailAddress(candidate.Email);
            MailAddress ma = new MailAddress("carlos.dominguez@richlinegroup.com"); //HACK: carlos dominguez cambiar correo.
            mm.To.Add(ma);
            mm.From = new MailAddress(System.Configuration.ConfigurationManager.AppSettings["MailSenderAddress"]);
            var client = new SmtpClient();
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["MailSenderAddress"],
                    System.Configuration.ConfigurationManager.AppSettings["MailPassword"]);
            client.EnableSsl = true;
            client.Send(mm);
        }

        #endregion Public Methods

        #region Protected Methods

        protected void AprobarPropuesta()
        {
            List<string> validaciones = new List<string>();
            try
            {
                string wageProposalID = ObtenerWageProposalID().ToString();
                DTO.PropuestaCompensacionBeneficio wageProposal = WageProposalDataHelper.Get(Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_GET_BY_ID.Replace("{id}", wageProposalID));
                if (wageProposal != null)
                {
                    wageProposal.StatusID = (int)Core.Enumeraciones.WageProposalStatus.APROBADO;
                    WageProposalDataHelper.Save(wageProposal);
                    Response.Redirect(string.Format(URL_DETALLE_CANDIDATO, ObtenerCandidateID()));
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void bttnAceptar_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DTO.PropuestaCompensacionBeneficio wageProposal = new DTO.PropuestaCompensacionBeneficio
                {
                    PositionID = int.Parse(ddlPositions.SelectedValue.ToString()),
                    Proposal = double.Parse(txtProposal.Value),
                    CandidateID = ObtenerCandidateID(),
                    StatusID = (int)Core.Enumeraciones.WageProposalStatus.PENDIENTE,
                    UserID = 2
                };
                var candidate = CandidateDataHelper.Get(Core.OptimusApiUrls.Candidatos_GET_BY_ID.Replace("{id}", ObtenerCandidateID().ToString()));
                var wageProposalCreated = WageProposalDataHelper.Save(wageProposal);
                var pdfFile = FillPDF(Server.MapPath("//petition//PDF//PropuestaSalarial.pdf"), wageProposalCreated, candidate);
                SendProposal(pdfFile);
                Response.Redirect(string.Format(URL_DETALLE_CANDIDATO, ObtenerCandidateID()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void bttnAprobar_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                string wageProposalID = ObtenerWageProposalID().ToString();
                DTO.PropuestaCompensacionBeneficio wageProposal = WageProposalDataHelper.Get(Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_GET_BY_ID.Replace("{id}", wageProposalID));
                if (wageProposal != null)
                {
                    wageProposal.StatusID = (int)Core.Enumeraciones.WageProposalStatus.APROBADO;
                    WageProposalDataHelper.Save(wageProposal);
                    Response.Redirect(string.Format(URL_DETALLE_CANDIDATO, ObtenerCandidateID()));
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void bttnCancelar_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(string.Format(URL_DETALLE_CANDIDATO, ObtenerCandidateID()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void bttnRechazar_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                string wageProposalID = ObtenerWageProposalID().ToString();
                DTO.PropuestaCompensacionBeneficio wageProposal = WageProposalDataHelper.Get(Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_GET_BY_ID.Replace("{id}", wageProposalID));
                if (wageProposal != null)
                {
                    wageProposal.StatusID = (int)Core.Enumeraciones.WageProposalStatus.RECHAZADO;
                    WageProposalDataHelper.Save(wageProposal);
                    Response.Redirect(string.Format(URL_DETALLE_CANDIDATO, ObtenerCandidateID()));
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    string candidateID = ObtenerCandidateID().ToString();
                    string wageProposalID = ObtenerWageProposalID().ToString();
                    ddlPositions.SetPositions();
                    DTO.PropuestaCompensacionBeneficio wageProposal = WageProposalDataHelper.Get(Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_GET_BY_ID.Replace("{id}", wageProposalID));
                    if (wageProposal == null)
                    {
                        ddlPositions.SelectedIndex = 0;
                        txtProposal.Value = string.Empty;
                        ddlPositions.Enabled = true;
                        txtProposal.Disabled = false;
                        bttnA.Visible = false;
                        bttnRechazar.Visible = false;
                        bttnAceptar.Visible = true;
                    }
                    else
                    {
                        ddlPositions.SelectedValue = wageProposal.PositionID.ToString();
                        txtProposal.Value = wageProposal.Proposal.ToString();
                        ddlPositions.Enabled = false;
                        txtProposal.Disabled = true;
                        bttnA.Visible = true;
                        bttnRechazar.Visible = true;
                        bttnAceptar.Visible = false;
                    }
                }
                else
                {
                    ClientScript.GetPostBackEventReference(this, string.Empty);
                    if (Request.Form["__EVENTTARGET"] == "Aprobar")
                    {
                        AprobarPropuesta();
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private Byte[] FillPDF(string template, DTO.PropuestaCompensacionBeneficio wageProposal, DTO.Candidate candidate)
        {
            PdfReader reader = new PdfReader(template);
            string Nombre = candidate.FullName;
            using (MemoryStream ms = new MemoryStream())
            {
                using (PdfStamper stamp = new PdfStamper(reader, ms, '\0', true))
                {
                    stamp.AcroFields.SetField("Fecha", String.Format("{0} de {1} del año {2}", DateTime.Now.Date.Day.ToString(), DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-DO")), DateTime.Now.Year.ToString()));
                    stamp.AcroFields.SetField("Nombre", Nombre);
                    stamp.AcroFields.SetField("Cedula", candidate.Cedula);
                    stamp.AcroFields.SetField("Posicion", wageProposal.Position.PositionName);
                    int superiorID = wageProposal.Position.SuperiorPositionID ?? 0;
                    string supervisor = PositionDataHelper.Get(Core.OptimusApiUrls.Posiciones_GET_BY_ID.Replace("{id}", superiorID.ToString())).PositionName;
                    stamp.AcroFields.SetField("SeReporta", supervisor);
                    stamp.AcroFields.SetField("Salario", wageProposal.Proposal.ToString());
                    //stamp.AcroFields.SetField("FlotaTelefonica", BeneficiosCheckBox.Items.FindByValue("5").Selected ? "○Flota telefonica" : "");
                    //stamp.AcroFields.SetField("Incentivo Trimestral", BeneficiosCheckBox.Items.FindByValue("6").Selected ? "○Incentivo Trimestral" : "");
                    //stamp.FormFlattening = true;
                    stamp.Close();
                }
                return ms.ToArray();
            }
        }

        private int ObtenerCandidateID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER_CANDIDATE_ID].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return id;
        }

        private int ObtenerWageProposalID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER_WAGE_PROPOSAL_ID].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return id;
        }

        private void SendProposal(Byte[] pdfFile)
        {
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=Formulario.pdf");
            SendMail(pdfFile);
        }

        #endregion Private Methods

    }
}