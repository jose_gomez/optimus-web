﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalleRequisicionPersonal.aspx.cs" Inherits="Optimus.Web.WebForms.Requisicion_de_personal.DetalleRequisicionPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Requisición de personal
            </h3>
        </div>
        <div class="panel-body">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Información general.
                </div>
                <div class="panel-body">
                    <div class="row col-xs-12">
                        <div class="col-xs-12 col-md-6">
                            <b>Puesto:</b>
                            <i>
                                <asp:Label ID="lblPuestos" runat="server" /></i>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <b>Cantidad de empleados requeridos:</b>
                            <i>
                                <asp:Label ID="lblCantidadEmpleados" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12">
                        <div class="col-xs-12 col-md-6">
                            <b>Supervisado por:</b>
                            <i>
                                <asp:Label ID="lblSupervisor" runat="server" /></i>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <b>Departamento:</b>
                            <i>
                                <asp:Label ID="lblDepartamentos" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12">
                        <div class="col-xs-12">
                            <b>Motivo:</b> <i>
                                <asp:Label ID="lblMotivo" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12">
                        <div class="col-xs-12 col-md-6">
                            <b>Tiempo de vinculación requerido:</b>
                            <i>
                                <asp:Label ID="lblTiempoRequerido" runat="server" /></i>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <b>Jornada laboral:</b>
                            <i>
                                <asp:Label ID="lblJornadaLaboral" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12">
                        <div class="col-xs-12">
                            <b>Horario:</b><i>
                                <asp:Label ID="lblHorario" runat="server" /></i>
                        </div>
                    </div>
                    <div class="row col-xs-12" style="text-align: right">
                        <b>Usuario que requirió el personal:</b>
                        <i>
                            <asp:Label ID="lblUser" runat="server" /></i>
                    </div>
                </div>
                <div class="panel-footer">
                    <asp:Button ID="bttnAprovacion" runat="server" Text="Aprobar" CssClass="btn btn-primary" OnClick="bttnAprovacion_Click" />
                </div>
            </div>
            <div id="Pasos" runat="server">
            </div>
            <div class="col-xs-12">
                <ul class="pager">
                    <li class="previous"><a href="/HHRR/ListaRequisicionesPersonal.aspx">Atrás</a></li>
                </ul>
            </div>
        </div>
    </div>
</asp:Content>
