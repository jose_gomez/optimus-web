﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Optimus.Web.DA;
using Optimus.Web.WebForms.DataHelpers;

namespace Optimus.Web.WebForms
{
    public partial class RequisicionPersonal : System.Web.UI.Page
    {
        BC.ServiceContainer ServCont = new BC.ServiceContainer();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    lblFechaActual.Text = DateTime.Now.ToLongDateString();
                    List<HRPosition> listaPosiciones = ServCont.GetPositionsService().GetAllPositions();

                    ddlPuestos.DataSource = listaPosiciones;
                    ddlPuestos.DataValueField = "PositionID";
                    ddlPuestos.DataTextField = "PositionName";
                    ddlPuestos.DataBind();

                    txtNoEmpleados.Value = "1";

                    ddlDepartamento.SetDepartments();

                    ddlPuestos1.DataSource = ServCont.GetPositionsService().GetAllPositions();
                    ddlPuestos1.DataValueField = "PositionID";
                    ddlPuestos1.DataTextField = "PositionName";
                    ddlPuestos1.DataBind();



                    radioOtro.Checked = true;

                    RadioButtonList2.SelectedIndex = 0;

                    List<VwTurnosJornada> listaJornadas = ServCont.GetPersonnelRequisitionService().GetAllJornadas();
                    listaJornadas = listaJornadas.GroupBy(a => a.AutoID).Select(a => a.First()).ToList();

                    ddlJornadas.DataSource = listaJornadas;
                    ddlJornadas.DataValueField = "AutoId";
                    ddlJornadas.DataTextField = "Jornada";
                    ddlJornadas.DataBind();

                    ObtenerHorario();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
            
        }
        public enum MessageType { Success, Error, Info, Warning };
        protected void ShowMessage(string Message, MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }
        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                
                if (string.IsNullOrWhiteSpace(txtPuestoRequerido.Text)) validaciones.Add("Debe indicar el nombre de el puesto.");
                
                if (string.IsNullOrWhiteSpace(txtNoEmpleados.Value)||int.Parse(txtNoEmpleados.Value)<=0) validaciones.Add("Debe indicar la cantidad de empleados que solicita.");
                if((radioReemplazo.Checked && string.IsNullOrWhiteSpace(txtReplazaA.Text)) || (radioRetiro.Checked && string.IsNullOrWhiteSpace(txtReplazaA.Text))) validaciones.Add("Debe indicar el empleado que se remplazara.");
                if(!radioNuevo.Checked && !radioReemplazo.Checked && !radioRetiro.Checked && (radioOtro.Checked && string.IsNullOrWhiteSpace(txtCual.Value))) validaciones.Add("Debe indicar el motivo.");
                DateTime fecha;
                if (!DateTime.TryParse(txtFechaInicioLabor.Text, out fecha)) validaciones.Add("Debe indicar la fecha de inicio de labores.");

                if (validaciones.Count > 0) throw new Optimus.Core.Exceptions.ValidationException("Error");

                HRPersonnelRequisition pr = new HRPersonnelRequisition();
                pr.PersonalRequisitionID = 0;

                ListItem puesto = ddlPuestos.Items.FindByText(txtPuestoRequerido.Text.Trim());

                if (puesto != null)
                {
                    pr.PositionID = int.Parse(puesto.Value);
                    pr.IsNew = false;
                }
                else
                {
                    pr.IsNew = true;
                }

                pr.PositionName = txtPuestoRequerido.Text.Trim();

                pr.QuantityEmployees = int.Parse(txtNoEmpleados.Value);

                pr.SuperiorPositionID = int.Parse(ddlPuestos1.SelectedValue);

                pr.DepartmentID = int.Parse(ddlDepartamento.SelectedValue);

                if (radioOtro.Checked)
                {
                    pr.Reason = txtCual.Value;
                }
                else if(radioNuevo.Checked)
                {
                    pr.Reason = radioNuevo.Value;
                }
                else if (radioReemplazo.Checked)
                {
                    pr.Reason = radioReemplazo.Value;
                }
                else if (radioRetiro.Checked)
                {
                    pr.Reason = radioRetiro.Value;
                }

                pr.Replace = txtReplazaA.Text;

                pr.StatusID = 24;

                pr.Contract = RadioButtonList1.SelectedItem.Text;

                pr.Workday = RadioButtonList2.SelectedValue;

                pr.WorkingHours = ddlJornadas.SelectedItem.Text;

                pr.StartDate = DateTime.Parse(txtFechaInicioLabor.Text);

                pr.User = User.Identity.Name;

                int id = ServCont.GetPersonnelRequisitionService().SaveReturnID(pr);

                List<string> mensajes = new List<string>();
                mensajes.Add("Registro guardado.");

                string guid = ServCont.GetUnitOfWork().TSTickets.SubmitTicket("REQUISICION DE PERSONAL", string.Format(
                    "Se genero una requision de personal para el puesto {0} para {1} empleado(s) en el departamento de {2}. Para mas detalles <a href='/HHRR/DetalleRequisicionPersonal?ID={3}'>Aquí</a>"
                    , txtPuestoRequerido.Text
                    , txtNoEmpleados.Value
                    , ddlDepartamento.Items[ddlDepartamento.SelectedIndex].Text
                    ,id.ToString())
                    ,ServCont.GetUnitOfWork().User.Find(x=> x.UserName== User.Identity.Name).FirstOrDefault().UserID
                    ,25
                    ,2
                    ,true
                    ,false);

                pr.guid = new Guid(guid);

                pr.PersonalRequisitionID = id;

                ServCont.GetPersonnelRequisitionService().Save(pr);

                mensajes.Add(string.Format("Se ha generado un tique: {0}",guid));

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);

                BootstrapMessage.Attributes["class"] = "alert alert-success";
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlJornadas_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ObtenerHorario();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }
        private void ObtenerHorario()
        {
            List<VwTurnosJornada> listaJornadas = ServCont.GetPersonnelRequisitionService().GetAllJornadas();
            listaJornadas = (from a in listaJornadas
                             where a.AutoID == int.Parse(ddlJornadas.SelectedValue.ToString())
                             select a).ToList();
            Label1.Text = "";
            foreach (VwTurnosJornada item in listaJornadas)
            {
                Label1.Text +=
                    string.Format("{0}  {1} ", item.DiaSemana, item.Turno);
            }
        }
        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                //ObtenerPosicionesPorDepartamento();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }
        private void ObtenerPosicionesPorDepartamento() {
            ddlPuestos1.DataSource = ServCont.GetPositionsService().GetPositionsByDeparmentID(int.Parse(ddlDepartamento.SelectedValue));
            ddlPuestos1.DataValueField = "PositionID";
            ddlPuestos1.DataTextField = "PositionName";
            ddlPuestos1.DataBind();
        }

        protected void txtReplazaA_TextChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Label2.Text = ServCont.GetUnitOfWork().Employees.GetEmployeeById(int.Parse(txtReplazaA.Text)).FullName;
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }

        }

        protected void ddlPuestos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                HRPosition posicionBuscada = ServCont.GetPositionsService().GetPositionsByID(int.Parse(ddlPuestos.SelectedValue));
                if (posicionBuscada != null)
                {
                    txtPuestoRequerido.Text = posicionBuscada.PositionName;
                    HRDeparmentsPosition deparment = ServCont.GetPositionsService().GetDepartmentPosition(posicionBuscada.PositionID);
                    if (deparment != null)
                    {
                        ddlDepartamento.SelectedValue = deparment.DeparmentID.ToString();
                        ddlDepartamento_SelectedIndexChanged(this, new EventArgs());

                        HROrganizationChart superior = ServCont.GetPositionsService().GetOrganizationChart(posicionBuscada.PositionID);
                        if (superior != null)
                        {
                            ddlPuestos1.SelectedValue = superior.SuperiorPositionID.ToString();
                        }
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }
    }
}