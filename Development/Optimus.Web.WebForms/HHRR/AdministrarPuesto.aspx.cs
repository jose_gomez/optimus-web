﻿using Optimus.Web.DA;
using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string DEPARTMENT_ID = "DepartmentId";
        private const string DEPARTMENT_NAME = "Name";
        private const string LINK_DETALLES_PUESTO = "~/HHRR/DetallePuesto?ID={0}";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string POSITION_ID = "PositionID";
        private const string POSITION_NAME = "PositionName";
        private const string URL_PARAMETER = "ID";

        private HRPosition Position;
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format(LINK_DETALLES_PUESTO, ObtenerID().ToString()));
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Position = ServCont.GetPositionService().GetPositionByID(ObtenerID());
                Position.OrganizationLevelID = int.Parse(ddlOrganizationLevels.SelectedValue);
                Position.DepartmentID = int.Parse(ddlDepartments.SelectedValue);
                Position.PositionName = txtName.Text;
                Position.Description = txtDescription.Text;
                Position.SuperiorPositionID = int.Parse(ddlSuperiorPositions.SelectedValue);
                ServCont.GetPositionService().Save(Position);
                Response.Redirect(string.Format(LINK_DETALLES_PUESTO, ObtenerID().ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlOrganizationLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                SetSuperiorPositions();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    SetPositionCategories();
                    SetOrganizationLevels();
                    SetDepartments();
                    SetSuperiorPositions();
                    ObtenerPosicion();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        private void ObtenerPosicion()
        {
            Position = ServCont.GetPositionsService().GetPositionsByID(ObtenerID());
            if (Position != null)
            {
                title.InnerHtml = Position.PositionName;
                ddlCategories.SelectedValue = Position.PositionCategoryID.ToString();
                ddlOrganizationLevels.SelectedValue = Position.OrganizationLevelID.ToString();
                ddlDepartments.SelectedValue = Position.DepartmentID.ToString();
                ddlSuperiorPositions.SelectedValue = Position.SuperiorPositionID.ToString();
                txtName.Text = Position.PositionName;
                txtDescription.Text = Position.Description;
            }
        }

        private void SetDepartments()
        {
            ddlDepartments.SetDepartments();
        }

        private void SetOrganizationLevels()
        {
            ddlOrganizationLevels.SetOrganizationLevels();
        }

        private void SetPositionCategories()
        {
            ddlCategories.SetPositionCategories();
        }

        private void SetSuperiorPositions()
        {
            if (!string.IsNullOrWhiteSpace(ddlOrganizationLevels.SelectedValue))
            {
                var niveles = ServCont.GetOrganizationLevelService().GetSuperiorOrganizationLevels(int.Parse(ddlOrganizationLevels.SelectedItem.Value));
                ddlSuperiorPositions.DataSource = ServCont.GetPositionService().GetPositionsByLevels(niveles);
                ddlSuperiorPositions.DataValueField = POSITION_ID;
                ddlSuperiorPositions.DataTextField = POSITION_NAME;
                ddlSuperiorPositions.DataBind();
            }
        }

        #endregion Private Methods

    }
}