﻿using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarCompensacionesBeneficios : System.Web.UI.Page
    {
        private const string MSG_INGRESAR_NOMBRE = "Ingresar el nombre.";
        private const string MSG_INGRESAR_PORCENTAJE = "Ingresar un porcentaje valido.";
        private const string MSG_REGISTRO_GUARDADO = "Registro guardado.";
        private const string COMPENSATION_ID = "CompensationID";
        private const string COMPENSATION_NAME = "Name";

        #region Private Fields

        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods
        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Intranet.aspx");
        }
        protected void btnNew_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                lblCompensacionBeneficioID.Text = "0";
                txtCompensacionBeneficio.Text = string.Empty;
                txtPorcentaje.Text = "0";
                txtPorcentaje.Enabled =
                    chckPorcentaje.Checked = false;
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void btnSaveCompensacionBeneficio_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                //Verifica que el texto el nombre no este vació
                if (string.IsNullOrWhiteSpace(txtCompensacionBeneficio.Text))
                {
                    throw new Core.Exceptions.ValidationException(MSG_INGRESAR_NOMBRE);
                }
                //Verifica si incrementa y que el porcentaje pasado sea un porcentaje valido.
                int result;
                if (chckPorcentaje.Checked && !int.TryParse(txtPorcentaje.Text, out result))
                {
                    throw new Core.Exceptions.ValidationException(MSG_INGRESAR_PORCENTAJE);
                }

                DA.HRCompensationBenefit compensacion = new DA.HRCompensationBenefit();

                compensacion.CompensationID = int.Parse(lblCompensacionBeneficioID.Text);
                compensacion.Name = txtCompensacionBeneficio.Text;
                compensacion.IsIncremental = chckPorcentaje.Checked;

                if (chckPorcentaje.Checked)
                {
                    compensacion.Percentage = int.Parse(txtPorcentaje.Text);
                }
                else
                {
                    compensacion.Percentage = 0;
                }

                ServCont.GetPositionsService().Save(compensacion);

                #region Mensaje de exito de la operacion

                List<string> mensajes = new List<string>();
                mensajes.Add(MSG_REGISTRO_GUARDADO);

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);

                BootstrapMessage.Attributes["class"] = "alert alert-success";

                #endregion Mensaje de exito de la operacion

                CargarCompensacionesBeneficios();

                if (!string.IsNullOrWhiteSpace(ddlCompensacionesBeneficios.SelectedValue))
                {
                    DA.HRCompensationBenefit compensacion1 = ServCont.GetPositionsService().GetCompensationBenefitByID(int.Parse(ddlCompensacionesBeneficios.SelectedValue));
                    if (compensacion1 != null)
                    {
                        lblCompensacionBeneficioID.Text = string.Format(" {0}", compensacion1.CompensationID.ToString());
                        txtCompensacionBeneficio.Text = compensacion1.Name;
                        chckPorcentaje.Checked = compensacion.IsIncremental == null ? false : (bool)compensacion.IsIncremental;
                        txtPorcentaje.Text = compensacion.Percentage == null ? "0" : compensacion.Percentage.ToString();
                        txtPorcentaje.Enabled = chckPorcentaje.Checked;
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void chkPorcentaje_CheckedChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                txtPorcentaje.Enabled = chckPorcentaje.Checked;
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void ddlCompensacionesBeneficios_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!string.IsNullOrWhiteSpace(ddlCompensacionesBeneficios.SelectedValue))
                {
                    DA.HRCompensationBenefit compensacion = ServCont.GetPositionsService().GetCompensationBenefitByID(int.Parse(ddlCompensacionesBeneficios.SelectedValue));
                    if (compensacion != null)
                    {
                        lblCompensacionBeneficioID.Text = string.Format(" {0}", compensacion.CompensationID.ToString());
                        txtCompensacionBeneficio.Text = compensacion.Name;
                        chckPorcentaje.Checked = compensacion.IsIncremental == null ? false : (bool)compensacion.IsIncremental;
                        txtPorcentaje.Text = compensacion.Percentage == null ? "0" : compensacion.Percentage.ToString();
                        txtPorcentaje.Enabled = chckPorcentaje.Checked;

                        BootstrapMessage.InnerHtml = "";

                        BootstrapMessage.Attributes["class"] = "";
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    CargarCompensacionesBeneficios();
                    if (!string.IsNullOrWhiteSpace(ddlCompensacionesBeneficios.SelectedValue))
                    {
                        DA.HRCompensationBenefit compensacion = ServCont.GetPositionsService().GetCompensationBenefitByID(int.Parse(ddlCompensacionesBeneficios.SelectedValue));
                        if (compensacion != null)
                        {
                            lblCompensacionBeneficioID.Text = string.Format(" {0}", compensacion.CompensationID.ToString());
                            txtCompensacionBeneficio.Text = compensacion.Name;
                            chckPorcentaje.Checked = compensacion.IsIncremental == null ? false : (bool)compensacion.IsIncremental;
                            txtPorcentaje.Text = compensacion.Percentage == null ? "0" : compensacion.Percentage.ToString();
                            txtPorcentaje.Enabled = chckPorcentaje.Checked;
                        }
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        
        private void CargarCompensacionesBeneficios()
        {
            ddlCompensacionesBeneficios.DataSource = ServCont.GetPositionsService().GetAllCompensationBenefitForEdit();
            ddlCompensacionesBeneficios.DataTextField = COMPENSATION_NAME;
            ddlCompensacionesBeneficios.DataValueField = COMPENSATION_ID;
            ddlCompensacionesBeneficios.DataBind();
        }
        private void ShowAlertMessage(Exception ex)
        {
            List<string> errores = new List<string>();
            errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            BootstrapMessage.Attributes["class"] = "alert alert-danger";
        }

        private void ShowWarningMessage(List<string> validaciones, Core.Exceptions.ValidationException ex)
        {
            List<string> errores = new List<string>();
            if (validaciones.Count <= 0)
            {
                errores.Add("<b>" + ex.Message + "</b>");
            }
            else
            {
                errores = validaciones;
            }
            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            BootstrapMessage.Attributes["class"] = "alert alert-warning";
        }
        #endregion Private Methods

    }
}