﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetalleCandidato.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.DetalleCandidato" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Candidato
            </h3>
        </div>
        <div class="panel-body">
            <h4>Cédula:
            </h4>
            <asp:Label ID="lblCedula" runat="server" Text=""></asp:Label>
            <h4>Nombre:
            </h4>
            <asp:Label ID="lblNombre" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <ul class="pager">
        <li class="next">
            <asp:LinkButton ID="linkBttnReporte" runat="server" OnClick="linkBttnReporte_Click">Imprimir</asp:LinkButton>
    </ul>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Propuestas
            </h3>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs">
                <li class="active"><a data-toggle="tab" href="#pendientes">Pendientes</a></li>
                <li><a data-toggle="tab" href="#aprobados">Aprobadas</a></li>
                <li><a data-toggle="tab" href="#rechazados">Rechazadas</a></li>
            </ul>
            <div class="tab-content">
                <div id="pendientes" class="tab-pane fade in active">
                    <asp:ListView ID="lviewPendientes" runat="server" ItemType="Optimus.DTO.WageProposal">
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <td>No hay propuestas pendientes.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <EmptyItemTemplate>
                            <td />
                        </EmptyItemTemplate>
                        <GroupTemplate>
                            <tr id="itemPlaceholderContainer" runat="server">
                                <td id="itemPlaceholder" runat="server"></td>
                            </tr>
                        </GroupTemplate>
                        <ItemTemplate>
                            <td runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <p><%#: Item.Position.PositionName %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: String.Format("{0:c}", Item.Proposal) %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: Item.CreateDate %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: Enum.GetName(typeof(Optimus.Core.Enumeraciones.WageProposalStatus),Item.StatusID) %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <ul class="pager">
                                                <li class="next"><a href='<%#: string.Format("AdministrarPropuesta.aspx?CandidateID={0}&WageProposalID={1}",Item.CandidateID,Item.WageProposalID) %>'>Editar</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                                <tr id="groupPlaceholder"></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                    </asp:ListView>
                </div>
                <div id="aprobados" class="tab-pane fade">
                    <asp:ListView ID="lviewAprobados" runat="server" ItemType="Optimus.DTO.WageProposal">
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <td>No hay propuestas aprobadas.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <EmptyItemTemplate>
                            <td />
                        </EmptyItemTemplate>
                        <GroupTemplate>
                            <tr id="itemPlaceholderContainer" runat="server">
                                <td id="itemPlaceholder" runat="server"></td>
                            </tr>
                        </GroupTemplate>
                        <ItemTemplate>
                            <td runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <p><%#: Item.Position.PositionName %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: String.Format("{0:c}", Item.Proposal) %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: Item.CreateDate %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: Enum.GetName(typeof(Optimus.Core.Enumeraciones.WageProposalStatus),Item.StatusID) %></p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                                <tr id="groupPlaceholder"></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                    </asp:ListView>
                </div>
                <div id="rechazados" class="tab-pane fade">
                    <asp:ListView ID="lviewRechazados" runat="server" ItemType="Optimus.DTO.WageProposal">
                        <EmptyDataTemplate>
                            <table>
                                <tr>
                                    <td>No hay propuestas rechazadas.</td>
                                </tr>
                            </table>
                        </EmptyDataTemplate>
                        <EmptyItemTemplate>
                            <td />
                        </EmptyItemTemplate>
                        <GroupTemplate>
                            <tr id="itemPlaceholderContainer" runat="server">
                                <td id="itemPlaceholder" runat="server"></td>
                            </tr>
                        </GroupTemplate>
                        <ItemTemplate>
                            <td runat="server">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-lg-12">
                                            <p><%#: Item.Position.PositionName %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: String.Format("{0:c}", Item.Proposal) %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: Item.CreateDate %></p>
                                        </div>
                                        <div class="col-lg-3">
                                            <p><%#: Enum.GetName(typeof(Optimus.Core.Enumeraciones.WageProposalStatus),Item.StatusID) %></p>
                                        </div>
                                    </div>
                                </div>
                            </td>
                        </ItemTemplate>
                        <LayoutTemplate>
                            <table style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td>
                                            <table id="groupPlaceholderContainer" runat="server" style="width: 100%">
                                                <tr id="groupPlaceholder"></tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr></tr>
                                </tbody>
                            </table>
                        </LayoutTemplate>
                    </asp:ListView>
                </div>
            </div>
        </div>
    </div>
    <div class="panel">
        <asp:Button ID="bttnNuevo" runat="server" Text="Nuevo" CssClass="btn btn-primary" OnClick="Button1_Click" />
    </div>
</asp:Content>
