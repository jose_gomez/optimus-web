﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class ListaPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_REGISTRAR_PUESTO = "~/HHRR/RegistrarPuesto.aspx";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnNewComp_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(LINK_REGISTRAR_PUESTO);
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    LoadPositions();
                    HyperLink2.NavigateUrl = "~/HHRR/OrgChartImg.aspx?ID=53";
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void LoadPositions()
        {
            gvPuestos.DataSource = PositionDataHelper.GetList(Core.OptimusApiUrls.Posiciones_GET);
            gvPuestos.DataBind();
        }

        #endregion Private Methods

    }
}