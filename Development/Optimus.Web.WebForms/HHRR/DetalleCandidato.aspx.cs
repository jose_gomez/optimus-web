﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class DetalleCandidato : System.Web.UI.Page
    {

        #region Private Fields

        private const string URL_PARAMETER_CANDIDATE_ID = "ID";

        #endregion Private Fields

        #region Protected Methods

        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                Response.Redirect(string.Format("~/HHRR/AdministrarPropuesta?CandidateID={0}", ObtenerCandidateID()));
            }
            catch (Exception)
            {
                throw;
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    var candidate = DataHelpers.CandidateDataHelper.Get(Core.OptimusApiUrls.Candidatos_GET_BY_ID.Replace("{id}", ObtenerCandidateID().ToString()));
                    if (candidate != null)
                    {
                        lblCedula.Text = candidate.Cedula;
                        lblNombre.Text = string.Format("{0} {1}", candidate.FirstName, candidate.LastName);
                    }
                    ObtenerPropuestas();
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private int ObtenerCandidateID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER_CANDIDATE_ID].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                //Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        private void ObtenerPropuestas()
        {
            var propuestas = DataHelpers.WageProposalDataHelper.GetList(
                            Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_GET_BY_CANDIDATE.Replace("{id}", ObtenerCandidateID().ToString()));
            if (propuestas != null && propuestas.Count > 0)
            {
                ObtenerPropuestasPendientes(propuestas);
                ObtenerPropuestasAprobadas(propuestas);
                ObtenerPropuestasRechazadas(propuestas);
                SePuedeCrearUnaNueva();
            }
        }

        private void ObtenerPropuestasAprobadas(List<DTO.PropuestaCompensacionBeneficio> propuestas)
        {
            lviewAprobados.DataSource = (from a in propuestas
                                         where a.StatusID == (int)Core.Enumeraciones.WageProposalStatus.APROBADO
                                         select a).ToList();
            lviewAprobados.DataBind();
        }

        private void ObtenerPropuestasPendientes(List<DTO.PropuestaCompensacionBeneficio> propuestas)
        {
            lviewPendientes.DataSource = (from a in propuestas
                                          where a.StatusID == (int)Core.Enumeraciones.WageProposalStatus.PENDIENTE
                                          select a).ToList();
            lviewPendientes.DataBind();
        }
        private void SePuedeCrearUnaNueva()
        {
            bttnNuevo.Enabled = lviewAprobados.Items.Count == 0 && lviewPendientes.Items.Count == 0;
        }
        private void ObtenerPropuestasRechazadas(List<DTO.PropuestaCompensacionBeneficio> propuestas)
        {
            lviewRechazados.DataSource = (from a in propuestas
                                          where a.StatusID == (int)Core.Enumeraciones.WageProposalStatus.RECHAZADO
                                          select a).ToList();
            lviewRechazados.DataBind();
        }

        #endregion Private Methods

        protected void linkBttnReporte_Click(object sender, EventArgs e)
        {
            Response.Redirect(string.Format("{0}{1}&cedula={2}", System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString(), System.Configuration.ConfigurationManager.AppSettings["ReportSolicitudesByCedula"].ToString(), lblCedula.Text));
        }
    }
}