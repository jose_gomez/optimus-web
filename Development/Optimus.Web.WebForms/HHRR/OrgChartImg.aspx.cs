﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OrgChartGenerator;
using System.Drawing.Imaging;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class OrgChartImg : System.Web.UI.Page
    {
        private const string URL_PARAMETER = "ID";
        BC.ServiceContainer ServCont = new BC.ServiceContainer();
        protected void Page_Load(object sender, EventArgs e)
        {
            // Change the response headers to output a JPEG image.
            this.Response.Clear();
            this.Response.ContentType = "image/jpeg";
            //Build the image 
            //In real life this can be done using parameters
            OrgChartGenerator.OrgChart myOrgChart;
            OrgChartGenerator.OrgData.OrgDetailsDataTable myOrgData =
              new OrgChartGenerator.OrgData.OrgDetailsDataTable();
            string directorGeneralID=ObtenerID().ToString();
            bool isInOrgChart = false;
            foreach (var position in ServCont.GetPositionService().GetOrgChart())
            {
                myOrgData.AddOrgDetailsRow(position.PositionID.ToString(),
                    "",
                    position.SuperiorPosition == null ? string.Empty : position.SuperiorPosition.ToString(),
                    position.PositionName);
                if (position.PositionID == int.Parse(directorGeneralID))
                {
                    isInOrgChart = true;
                }
            }

            //instantiate the object
            myOrgChart = new OrgChartGenerator.OrgChart(myOrgData);
            myOrgChart.FontSize = 7;
            myOrgChart.HorizontalSpace = 3;
            myOrgChart.VerticalSpace = 45;
            System.Drawing.Image OC =
               System.Drawing.Image.FromStream(myOrgChart.GenerateOrgChart(
                   isInOrgChart ? directorGeneralID : "53",
                  System.Drawing.Imaging.ImageFormat.Bmp));

            // Write the image to the response stream in JPEG format.
            OC.Save(this.Response.OutputStream, ImageFormat.Jpeg);
            OC.Dispose();
        }
        private int ObtenerID()
        {
            int id = 0;
            try
            {
                if (Request.QueryString[URL_PARAMETER]!=null)
                {
                    id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
                }
                else
                {
                    id = 53;
                }
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return id;
        }

    }
}