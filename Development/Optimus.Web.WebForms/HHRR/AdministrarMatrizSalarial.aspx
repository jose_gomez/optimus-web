﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarMatrizSalarial.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarMatrizSalarial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Competencias del puesto</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Nombre de la posición:</h4>
                </div>
                <div class="col-xs-12">
                    <asp:Label ID="lblPuestoName" Text="Nombre del puesto" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
            <h4 class="col-xs-12">Nivel:
                <asp:Label ID="lbNivel" runat="server" Text=""></asp:Label></h4>
            <h4 class="col-xs-12">Compensación o beneficio:</h4>
            <asp:DropDownList ID="ddlCompensacionesBeneficios" runat="server" CssClass="form-control col-xs-12" OnSelectedIndexChanged="ddlCompensacionesBeneficios_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            <asp:Label ID="lblPorcentaje" runat="server" Text="" CssClass="col-xs-12"></asp:Label>
            <asp:TextBox ID="txtCantidad" runat="server" CssClass="form-control col-xs-12" ></asp:TextBox>
        </div>
        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSave_Click" />
            <asp:Button ID="Button2" CssClass="btn btn-default" runat="server" Text="Cancelar" OnClick="Button2_Click"  />
        </div>
    </div>
</asp:Content>
