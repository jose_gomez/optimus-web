﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class RegistrarPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string POSITION_ID = "PositionID";
        private const string POSITION_NAME = "PositionName";
        private const int POSITION_STATUS_ACTIVE = 43;
        private DA.HRPosition Position = new DA.HRPosition();
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Position.OrganizationLevelID = int.Parse(ddlOrganizationLevels.SelectedValue);
                Position.DepartmentID = int.Parse(ddlDepartments.SelectedValue);
                Position.PositionName = txtName.Text;
                Position.Description = txtDescription.Text;
                Position.Status = POSITION_STATUS_ACTIVE;
                Position.SuperiorPositionID = int.Parse(ddlSuperiorPositions.SelectedValue);
                ServCont.GetPositionService().Save(Position);
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void ddlDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                SetPositionName();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void ddlOrganizationLevels_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                SetPositionName();
                SetSuperiorPositions();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    SetPositionCategories();
                    SetOrganizationLevels();
                    SetDepartments();
                    SetSuperiorPositions();
                    SetPositionName();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void SetDepartments()
        {
            ddlDepartments.SetDepartments();
        }

        private void SetOrganizationLevels()
        {
            ddlOrganizationLevels.SetOrganizationLevels();
        }

        private void SetPositionCategories()
        {
            ddlCategories.SetPositionCategories();
        }

        private void SetPositionName()
        {
            txtName.Text = string.Format("{0} {1}", ddlOrganizationLevels.SelectedItem.Text, ddlDepartments.SelectedItem.Text);
        }

        private void SetSuperiorPositions()
        {
            if (!string.IsNullOrWhiteSpace(ddlOrganizationLevels.SelectedValue))
            {
                var niveles = ServCont.GetOrganizationLevelService().GetSuperiorOrganizationLevels(int.Parse(ddlOrganizationLevels.SelectedItem.Value));
                ddlSuperiorPositions.DataSource = ServCont.GetPositionService().GetPositionsByLevels(niveles);
                ddlSuperiorPositions.DataValueField = POSITION_ID;
                ddlSuperiorPositions.DataTextField = POSITION_NAME;
                ddlSuperiorPositions.DataBind();
            }
        }

        private void ShowAlertMessage(Exception ex)
        {
            List<string> errores = new List<string>();
            errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            BootstrapMessage.Attributes["class"] = "alert alert-danger";
        }

        private void ShowWarningMessage(List<string> validaciones, Core.Exceptions.ValidationException ex)
        {
            List<string> errores = new List<string>();
            if (validaciones.Count <= 0)
            {
                errores.Add("<b>" + ex.Message + "</b>");
            }
            else
            {
                errores = validaciones;
            }
            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            BootstrapMessage.Attributes["class"] = "alert alert-warning";
        }

        #endregion Private Methods

    }
}