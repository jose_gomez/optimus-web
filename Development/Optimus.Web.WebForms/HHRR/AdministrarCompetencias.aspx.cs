﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarCompetencias : System.Web.UI.Page
    {
        #region Private Fields

        private const string COMPETENCE_CATEGORY_ID = "CompetenceCategoryID";
        private const string COMPETENCE_CATEGORY_NAME = "Name";
        private const string COMPETENCE_ID = "CompetenceID";
        private const string COMPETENCE_NAME = "Name";

        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Intranet.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                lblCategoriaID.Text = "0";
                txtCategoria.Text = string.Empty;
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void btnNewComp_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                lblCompetenciaID.Text = "0";
                txtCompetencia.Text = string.Empty;
                txtDescripcion.Text = string.Empty;
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void btnSaveCategoria_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DA.HRCompetencesCategory a = new DA.HRCompetencesCategory
                {
                    CompetenceCategoryID = int.Parse(lblCategoriaID.Text),
                    Name = txtCategoria.Text
                };

                ServCont.GetPositionsService().Save(a);

                CargarCategorias();
                ObtenerCategoria();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void btnSaveCompetencia_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DA.HRCompetence a = new DA.HRCompetence
                {
                    CompetenceID = int.Parse(lblCompetenciaID.Text),
                    Name = txtCompetencia.Text,
                    Description = txtDescripcion.Text,
                    CompetenceCategoryID = int.Parse(ddlCategoriasLista.SelectedValue)
                };

                ServCont.GetPositionsService().Save(a);

                CargarCompetencias();
                ObtenerCompetencia();

                List<string> mensajes = new List<string>();
                mensajes.Add("Registro guardado.");

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);

                BootstrapMessage.Attributes["class"] = "alert alert-success";
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlCategoriasLista_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ObtenerCategoria();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlCompetencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ObtenerCompetencia();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    CargarCategorias();
                    ObtenerCategoria();
                    CargarCompetencias();
                    ObtenerCompetencia();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void CargarCategorias()
        {
            ddlCategoriasLista.DataSource = ServCont.GetPositionsService().GetAllCompetencesCategory();
            ddlCategoriasLista.DataTextField = COMPETENCE_CATEGORY_NAME;
            ddlCategoriasLista.DataValueField = COMPETENCE_CATEGORY_ID;
            ddlCategoriasLista.DataBind();
        }

        private void CargarCompetencias()
        {
            List<DA.HRCompetence> competencias = ServCont.GetPositionsService().GetCompetenceByCategoryID(int.Parse(ddlCategoriasLista.SelectedValue));
            ddlCompetencias.DataSource = competencias;
            ddlCompetencias.DataTextField = COMPETENCE_NAME;
            ddlCompetencias.DataValueField = COMPETENCE_ID;
            ddlCompetencias.DataBind();
        }

        private void ObtenerCategoria()
        {
            if (!string.IsNullOrWhiteSpace(ddlCategoriasLista.SelectedValue))
            {
                var cate = ServCont.GetPositionsService().GetCompetencesCategoryBYID(int.Parse(ddlCategoriasLista.SelectedValue));
                if (cate != null)
                {
                    lblCategoriaID.Text = cate.CompetenceCategoryID.ToString();
                    txtCategoria.Text = cate.Name;

                    CargarCompetencias();
                    ObtenerCompetencia();
                }
            }
            else
            {
                lblCategoriaID.Text = "0";
                txtCategoria.Text = string.Empty;
            }
        }

        private void ObtenerCompetencia()
        {
            if (!string.IsNullOrWhiteSpace(ddlCompetencias.SelectedValue.ToString()))
            {
                DA.HRCompetence comp = ServCont.GetPositionsService().GetCompetenceByID(int.Parse(ddlCompetencias.SelectedValue));
                if (comp != null)
                {
                    lblCompetenciaID.Text = comp.CompetenceID.ToString();
                    txtCompetencia.Text = comp.Name;
                    txtDescripcion.Text = comp.Description;
                }
            }
            else
            {
                lblCompetenciaID.Text = "0";
                txtCompetencia.Text = string.Empty;
                txtDescripcion.Text = string.Empty;
            }
        }

        #endregion Private Methods

    }
}