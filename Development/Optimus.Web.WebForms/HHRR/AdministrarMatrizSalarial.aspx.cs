﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarMatrizSalarial : System.Web.UI.Page
    {
        #region Private Fields

        private const string URL_PARAMETER = "ID";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string LINK_DETALLE_PUESTO = "~/HHRR/DetallePuesto?ID={0}";
        private const string PORCENTAJE_INCREMENTO = "Porcentaje de incremento: {0}%";
        private const string MSG_CANTIDAD_INVALIDA = "Debe ingresar una cantidad validad.";
        private const string COMPENSATION_ID = "CompensationID";
        private const string COMPENSATION_NAME = "Name";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    DA.HRPosition po = ServCont.GetPositionsService().GetPositionsByID(ObtenerID());
                    lblPuestoName.Text = po.PositionName;
                    lbNivel.Text = ServCont.GetPositionsService().GetBasicSalaryLevel().Name;
                    ddlCompensacionesBeneficios.DataSource = ServCont.GetPositionsService().GetAllCompensationBenefitForEdit();
                    ddlCompensacionesBeneficios.DataValueField = COMPENSATION_ID;
                    ddlCompensacionesBeneficios.DataTextField = COMPENSATION_NAME;
                    ddlCompensacionesBeneficios.DataBind();
                    ObtenerCompensacion();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        private void ObtenerCompensacion()
        {
            DA.HRCompensationBenefit compBen = ServCont.GetPositionsService().GetCompensationBenefitByID(int.Parse(ddlCompensacionesBeneficios.SelectedValue));
            if (compBen != null)
            {
                lblPorcentaje.Text = string.Format(PORCENTAJE_INCREMENTO, compBen.Percentage.ToString());
                DA.HRSalaryDetail salaryDetail = ServCont.GetPositionsService().GetSalaryDetailBy(ObtenerID(), compBen.CompensationID, ServCont.GetPositionsService().GetBasicSalaryLevel().SalaryLevelID);
                if (salaryDetail != null) txtCantidad.Text = salaryDetail.Quantity.ToString();
            }
        }

        protected void ddlCompensacionesBeneficios_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ObtenerCompensacion();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                int id = ObtenerID();
                double cantidad = 0;
                if (!double.TryParse(txtCantidad.Text, out cantidad))
                {
                    throw new Core.Exceptions.ValidationException(MSG_CANTIDAD_INVALIDA);
                }
                DA.HRSalaryDetail detalle = new DA.HRSalaryDetail();
                detalle.PositionID = id;
                detalle.CompensationID = int.Parse(ddlCompensacionesBeneficios.SelectedValue);
                detalle.SalaryLevelsID = ServCont.GetPositionsService().GetBasicSalaryLevel().SalaryLevelID;
                detalle.Quantity = cantidad;
                detalle.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;

                ServCont.GetPositionsService().Save(detalle);

                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, id.ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, ObtenerID().ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }
    }
}