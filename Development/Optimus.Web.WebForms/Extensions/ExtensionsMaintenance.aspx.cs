﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Extensions
{
    public partial class ExtensionsMaintenance : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();

            if (!IsPostBack)
            {
                var ext = worker.Extensions.GetExtensiones();
                ExtencionGridView.DataSource = ext;
                ExtencionGridView.DataBind();

                var dep = container.GetDepartmentService().GetAllDepartments();
                DepartementoDropDownList.DataTextField = "Name";
                DepartementoDropDownList.DataValueField = "DepartmentId";
                DepartementoDropDownList.DataSource = dep;
                DepartementoDropDownList.DataBind();

            
                DeparmentDropDown.DataTextField = "Name";
                DeparmentDropDown.DataValueField = "DepartmentId";
                DeparmentDropDown.DataSource = dep;
                DeparmentDropDown.DataBind();
            }

        }
        protected void DepartamentoFiltro(object sender, EventArgs e)
        {
            var ext = worker.Extensions.GetExtensiones(int.Parse(DepartementoDropDownList.SelectedItem.Value));
            ExtencionGridView.DataSource = ext;
            ExtencionGridView.DataBind();
        }


        protected void NombreFiltro(object sender, EventArgs e)
        {
            var ext = worker.Extensions.GetExtensiones(nombreTextBox.Text);
            
            ExtencionGridView.DataSource = ext;
            ExtencionGridView.DataBind();

        }


        protected void GuardarExtesion(object sender, EventArgs e)
        {

            int empID = 0;
            if (IDExtensionHidden.Value != string.Empty)
            {
                 empID = int.Parse(IDExtensionHidden.Value);
            }
            SYEmployeeExtensions Emp = worker.Extensions.Find(x =>  x.EmployeeExtensionsID == empID).FirstOrDefault();
            if (Emp == null)
            {
                Emp = new SYEmployeeExtensions();
                worker.Extensions.Add(Emp);
            }
            if (int.TryParse(empleadoTextBox.Text,out empID))
            {
                Emp.EmployeeID = int.Parse(empleadoTextBox.Text);
            }else
            {
                Emp.EmployeeID = null;
            }
            if (int.TryParse(ExtensionTextBox.Text,out empID))
            {
                Emp.Extensions = int.Parse(ExtensionTextBox.Text);
            }else
            {
                Emp.Extensions = null;
            }
            
            Emp.DepartmentId = int.Parse(DeparmentDropDown.SelectedItem.Value);
            Emp.Flota = FlotaTextBox.Text;
            Emp.Etiqueta = EtiquetaTextBox.Text;
            worker.Complete();
            Response.Redirect(Request.RawUrl);

        }

        protected void ExtencionGridView_RowDeleting(object sender, GridViewDeleteEventArgs e)
        {
            Label  EmployeeExtensionsID = (Label)ExtencionGridView.Rows[e.RowIndex].FindControl("EmployeeExtensionsLabel");          
            SYEmployeeExtensions Emp = new SYEmployeeExtensions();
            int id = int.Parse(EmployeeExtensionsID.Text);
            Emp = worker.Extensions.Find(x => x.EmployeeExtensionsID == id).FirstOrDefault();
            worker.Extensions.Remove(Emp);
            worker.Complete();
            Response.Redirect(Request.RawUrl);

        }
    }
}