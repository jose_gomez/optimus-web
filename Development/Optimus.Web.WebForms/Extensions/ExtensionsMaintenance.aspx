﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExtensionsMaintenance.aspx.cs" Inherits="Optimus.Web.WebForms.Extensions.ExtensionsMaintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .NoExt{
            display:none;
        }
    </style>
   <script>
       jQuery(function ($) {
           $("#ExtensionTextBox").mask("999");
           $("#FlotaTextBox").mask("(999) 999-9999");
           $("#empleadoTextBox").mask("9999");
       });
       function OpenModal(id) {
           if (id!= 0) {
               getExtension(id);
           } else {
               $('#ExtensionTextBox').val('');
               $('#empleadoTextBox').val('');
               $('#FlotaTextBox').val('');
               $('#EtiquetaTextBox').val('');
           }
           
           $('#AgregarExt').modal('show');
       }
       
       function getExtension(id) {

           $.ajax({
               type: 'Post',
               url: '/WebServices/OptimusWebServices.asmx/GetExtensionByID',
               data: '{id:\'' + id + '\'}',
               contentType: "application/json; charset=utf-8",
               dataType: "json",
               success: function (data) {
                   if (data.d) {
                       $('#MainContent_IDExtensionHidden').attr("value", data.d.EmployeeExtensionsID.toString());

                       $('#ExtensionTextBox').val(data.d.ext.toString());
                       if (data.d.EmployeesID != null)
                       {
                           $('#empleadoTextBox').val(data.d.EmployeesID);
                       }
                       if (data.d.DeparmentID != null) {
                           $('#MainContent_DeparmentDropDown').val(data.d.DeparmentID.toString());
                       }
                       if (data.d.Flota != null) {
                           $('#FlotaTextBox').val(data.d.Flota.toString());
                       }
                       if (data.d.Etiqueta != null) {
                           $('#EtiquetaTextBox').val(data.d.Etiqueta.toString());
                       }
                   }
               }
           });
       }
   </script>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <asp:Label runat="server" Text="Buscar:" Font-Bold="true"></asp:Label>
                <asp:TextBox runat="server" ID="nombreTextBox" OnTextChanged="NombreFiltro" AutoPostBack="true" ToolTip="Buscar por Nombre,Extensión o Etiqueta"></asp:TextBox>
                <asp:Label runat="server" Text="Buscar por Departamento:" Font-Bold="true"></asp:Label>
                <asp:DropDownList runat="server" ID="DepartementoDropDownList" OnSelectedIndexChanged="DepartamentoFiltro" AutoPostBack="true"></asp:DropDownList>
                <button type="button" id="AccionesButton"  onclick="OpenModal(0)" class="btn btn-default">Agregar</button>
                <br />
                <br />
                <br />
                <asp:GridView runat="server" AutoGenerateColumns="false" ID="ExtencionGridView"     GridLines="none"
                            CellSpacing="-1"
                            CssClass="table table-stripe" AllowSorting="true" OnRowDeleting="ExtencionGridView_RowDeleting">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                              
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" ID="EmployeeExtensionsLabel" CssClass="NoExt" Text='<%#Eval("EmployeeExtensionsID")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Extensión"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("ext")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                       
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Empleado"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("fullName")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                <asp:Label runat="server" Text="Departamento"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("Departamento")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                          <asp:TemplateField>
                           <HeaderTemplate>
                               <asp:Label runat="server" Text="Etiqueta"></asp:Label>
                           </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("Etiqueta")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                           <asp:TemplateField ControlStyle-Width="100px">
                                 <HeaderTemplate>
                                <asp:Label runat="server" Text="Flota"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("Flota")%>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                           <a href="#" id="medde" onclick="OpenModal('<%#Eval("EmployeeExtensionsID")%>')" >Editar</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:ButtonField ItemStyle-Width="50px" ItemStyle-HorizontalAlign="Center" ButtonType="Image" ImageUrl="~/images/Icon/DeleteRed.png" CommandName="Delete" />
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div id="AgregarExt" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Agregar Extensión</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <asp:HiddenField runat="server" ID="IDExtensionHidden" />
                        <asp:Label runat="server" Text="Extensión:" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="ExtensionTextBox" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Número de empleado:" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="empleadoTextBox" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Número de flota:" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="FlotaTextBox" CssClass="form-control"></asp:TextBox>
                    </div>
                       <div class="form-group">
                        <asp:Label runat="server" Text="Etiqueta:" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="EtiquetaTextBox" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Departamento:" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="DeparmentDropDown" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <asp:Button runat="server" class="btn btn-default" Text="Aceptar" OnClick="GuardarExtesion" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
