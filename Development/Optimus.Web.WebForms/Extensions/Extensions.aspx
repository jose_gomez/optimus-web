﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Extensions.aspx.cs" Inherits="Optimus.Web.WebForms.Extensions.Extensions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Extensiones Telefónicas</h3>
        </div>
        <div class="panel-body">      
            <asp:Label runat="server" Text="Buscar:" Font-Bold="true"></asp:Label>
            <asp:TextBox runat="server" ID="nombreTextBox" OnTextChanged="NombreFiltro" AutoPostBack="true" ToolTip="Buscar por Nombre,Extensión o Etiqueta"></asp:TextBox>
            <asp:Label runat="server" Text="Buscar por Departamento:" Font-Bold="true"></asp:Label>
            <asp:DropDownList runat="server" ID="DepartementoDropDownList" AutoPostBack="true" OnSelectedIndexChanged="DepartamentoFiltro"></asp:DropDownList>
            <br />
            <br />
            <br />
            <asp:GridView runat="server" AutoGenerateColumns="false" ID="ExtencionGridView" 
                 GridLines="none"
                            CellSpacing="-1"
                            CssClass="table table-stripe" >
               
                <Columns>
                    <asp:BoundField ItemStyle-Width="50px" HeaderText="Extensión" DataField="ext"/>
                    <asp:BoundField ItemStyle-Width="350px" HeaderText="Empleado" DataField="fullName" />
                    <asp:BoundField ItemStyle-Width="250px" HeaderText="Departamento" DataField="Departamento" />
                    <asp:BoundField ItemStyle-Width="250px" HeaderText="Etiqueta" DataField="Etiqueta" />
                    <asp:BoundField ItemStyle-Width="200px" HeaderText="Flota" DataField="Flota" /> 
                </Columns>
                    
                    
            </asp:GridView>
        </div>
    </div>
</asp:Content>
