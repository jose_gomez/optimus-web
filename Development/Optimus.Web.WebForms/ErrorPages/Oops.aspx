﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Oops.aspx.cs" Inherits="Optimus.Web.WebForms.ErrorPages.Oops" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="panel-group">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h2 runat="server" id="TituloH2">Se ha detectado un error!</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">                  
                        <asp:Label runat="server" ID="errorLabel" Text="Se ha detectado un error  en la operación" Font-Size="Medium"></asp:Label>
                        <p class="text-warning"><small>Comuníquese con en el departamento de sistemas</small></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:LinkButton runat="server" Text="Ir a pagina principal" PostBackUrl="~/Default.aspx"></asp:LinkButton>
                    </div>
                </div>
        
            </div>

        </div>
    </div>

</asp:Content>
