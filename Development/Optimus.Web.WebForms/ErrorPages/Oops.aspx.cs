﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.ErrorPages
{
    public partial class Oops : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
          
            string Exec = Request.QueryString["exec"];
            string Denied = Request.QueryString["Denied"];
            if (Exec != null)
            {
                errorLabel.Text = DesEncriptar(Exec);
                if (Denied != null)
                {
                    if (Convert.ToBoolean(Denied))
                    {
                        TituloH2.InnerText = "Acceso denegado!";
                    }
                }
            }                      
        }

        public static string DesEncriptar(string msgr)
        {
            string result = string.Empty;
            byte[] decryted = Convert.FromBase64String(msgr);
          
            result = System.Text.Encoding.Unicode.GetString(decryted);
            return result;
        }
    }
}