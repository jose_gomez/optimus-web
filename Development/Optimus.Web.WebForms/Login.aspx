﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Optimus.Web.WebForms.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style type="text/css">
        /* enable absolute positioning */
        .inner-addon {
            position: relative;
        }

            /* style icon */
            .inner-addon .glyphicon {
                position: absolute;
                padding: 10px;
                pointer-events: none;
            }

        /* align icon */
        .left-addon .glyphicon {
            left: 0px;
        }

        .right-addon .glyphicon {
            right: 0px;
        }

        /* add padding  */
        .left-addon input {
            padding-left: 30px;
        }

        .right-addon input {
            padding-right: 30px;
        }
    </style>
    <asp:LoginView runat="server" ID="IdLogin">
        <AnonymousTemplate>
            <div class="panel-group">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4>Iniciar Sesión</h4>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <asp:Label runat="server" Text="Usuario:" Font-Bold="true"></asp:Label>
                        <div class="inner-addon left-addon">
                            <i class="glyphicon glyphicon-user"></i>
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtUsername"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtUsername" ErrorMessage="*Ingrese un nombre de usuario" ForeColor="Red">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Contraseña:" Font-Bold="true"></asp:Label>
                        <div class="inner-addon left-addon">
                            <i class="glyphicon glyphicon-lock"></i>
                            <asp:TextBox runat="server" CssClass="form-control" ID="txtPassword" TextMode="Password"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="txtPassword" ErrorMessage="*Ingrese una contraseña" ForeColor="Red">
                            </asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="form-group">
                        <asp:Button ClientIDMode="Static" ID="btnLogin" runat="server" Text="Ingresar" CssClass="btn btn-primary" OnClick="IniciarSesion" />
                    </div>
                </div>
            </div>
        </div>
        </AnonymousTemplate>
        <LoggedInTemplate>
            <div class="panel-group">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Bienvenido!</h4>
                    </div>
                    <div class="panel-body">
                        <h3>Ya esta Loggeado</h3>
                        <asp:Button ID="btnLogOut1" runat="server" Text="Salir" class="btn btn-primary" OnClick="SalirSesion" />
                    </div>
                </div>
            </div>
        </LoggedInTemplate>
    </asp:LoginView>
        

</asp:Content>
