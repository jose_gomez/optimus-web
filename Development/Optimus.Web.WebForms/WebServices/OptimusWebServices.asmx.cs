﻿using Newtonsoft.Json;
using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web.Script.Services;
using System.Web.Services;

namespace Optimus.Web.WebForms.WebServices
{
    /// <summary>
    /// Summary description for OptimusWebServices
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
    [System.Web.Script.Services.ScriptService]
    public class OptimusWebServices : System.Web.Services.WebService
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        private OptimusEntities entity;

        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
        ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public OptimusWebServices()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }

        [WebMethod]
        public Employees GetEmployeeById(int employeeId)
        {
            var emp = worker.Employees.GetEmployeeById(employeeId);
            if (emp.Picture != null)
            {
                var img = GUIHelpers.ImagesHelper.byteArrayToImage(emp.Picture);
                emp.PictureBase64 = ImageToBase64(img, ImageFormat.Jpeg);
            }
            return emp;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<DataPoint> GetStatistics(string dept)
        {
            List<DataPoint> points = new List<DataPoint>();
            var department= container.GetDepartmentService().GetAllDepartments().Where(d => d.Name.ToLower() == dept.ToLower()).FirstOrDefault();
            int deptId = department.DepartmentId;
            var ticketsStatistics = worker.TSTickets.GetPercentagesOfTicketsStatusesByDepartment(deptId);
            var properties = ticketsStatistics.GetType().GetProperties();
            int xAxis = 0;
            foreach(var p in properties)
            {
                if(p.Name == "TotalTicketsCount")
                {
                    continue;
                }
                DataPoint point = new DataPoint();
                point.x = xAxis;
                xAxis++;
                point.label = p.Name;
                point.y = (double)p.GetValue(ticketsStatistics);
                points.Add(point);
            }
            return points;
        }
        private List<DataPoint> GetTicketsLeaderBoard(int employeeId)
        {
            var employee = worker.Employees.Find(x => x.EmployeeId == employeeId).FirstOrDefault();
            if (employee != null)
            {
                int departmentId = employee.EmployeeId;
                var dataPoints = worker.TSTickets.GetAllTimeTicketsLeaders(departmentId);
                return dataPoints;
            }
            return null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public double GetAvgResponsetime(string dept)
        {
            return worker.TSTickets.GetAverageResponseTime(dept);
        }

        [WebMethod(MessageName = "GetAllTimeLeaderBoard")]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<DataPoint> GetAllTimeTicketsLeaderBoard1()
        {
            JsonSerializerSettings _jsonSetting = new JsonSerializerSettings() { NullValueHandling = NullValueHandling.Ignore };
            var user = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault();
            if (user != null)
            {
                if (user.EmployeeID != null)
                {
                    return GetTicketsLeaderBoard(user.EmployeeID.Value);
                }
            }
            return null;
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<DataPoint> GetAllTimeTicketsLeaderBoard(string dept)
        {
            var department = container.GetDepartmentService().GetAllDepartments().Where(d => d.Name == dept).FirstOrDefault();
            if(department != null)
            {
                return worker.TSTickets.GetAllTimeTicketsLeaders(department.DepartmentId, "Validate", false);
            }
            return null;
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public List<JQTreeNodeData> GetTicketHierarchy(int ticketId)
        {
            return worker.TSTickets.GetTicketsHierarchyForJQTree(ticketId);
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat=ResponseFormat.Json)]
        public Employees GetTopPerformer(string dept)
        {
            var department = container.GetDepartmentService().GetAllDepartments().Where(d => d.Name == dept).FirstOrDefault();
            if (department != null)
            {
                var top =  worker.TSTickets.GetAllTimeTicketsLeaders(department.DepartmentId, "Validate", false).FirstOrDefault();
                var employee = worker.Employees.Find(x => x.EmployeeId == top.EmployeeId).FirstOrDefault();

                employee.Picture = new BC.Repositories.API.Employee().GetEmployeeByID(employee.EmployeeId).Picture;
                Employees emp = new Employees();
                emp.FirstName = employee.FirstName;
                emp.LastName = employee.LastName;
                if(employee.Picture != null)
                {
                    var img = GUIHelpers.ImagesHelper.byteArrayToImage(employee.Picture);
                    emp.PictureBase64 = ImageToBase64(img, ImageFormat.Jpeg);
                }
                return emp;
            }
            return null;
        }

        [WebMethod]
        public Candidates GetCandidates(string cedula)
        {
            var candidate = (from p in ContextSQL.HRCandidates
                             where p.Cedula == cedula
                             select new Candidates
                             {
                                 PostulatePositionID = (int)p.PostulatePositionID,
                                 FirstName = p.FirstName,
                                 LastName = p.LastName,
                                 Streets = p.Streets,
                                 HouseNumber = p.HouseNumber,
                                 Sector = p.Sector,
                                 location = p.location,
                                 Sex = Convert.ToString(p.Sex),
                                 NationalityID = p.NationalityID,
                                 PhoneNumber = p.PhoneNumber,
                                 MovilNumber = p.MovilNumber,
                                 Email = p.Email,
                                 Age = (int)p.Age,
                                 Cedula = p.Cedula,
                                 BirthDate = p.BirthDate,
                                 alias = p.Alias,
                                 EmergencyNumber = p.EmergencyNumber,
                                 EmergencyContactName = p.EmergencyContactName,
                                 Size = p.Size,
                                 Religion = p.Religion,
                                 CivilStatus = p.CivilStatus,
                                 Allergies = (bool)p.allergies,
                                 AllergiesType = p.AllergiesType,
                                 suffering = (bool)p.suffering,
                                 sufferingType = p.sufferingType,
                                 AvailableOverTime = (bool)p.AvailableOvertime,
                                 AvailableWeekend = (bool)p.AvailableWeekend,
                                 LivesWith = p.LivesWith,
                                 SpouseName = p.SpouseName,
                                 SpouseActivity = p.SpouseActivity,
                                 MotherName = p.MotherName,
                                 FatherName = p.FatherName,
                                 DependentChildren = (int)p.DependentChildren,
                                 DependentPeople = (int)p.DependentPeople,
                                 StudiesID = (int)p.StudiesID,
                                 ComputerStudies = (bool)p.ComputerStudies,
                                 IsStudent = p.IsStudent,
                                 DaysWeek = p.DaysWeek,
                                 Schedule = p.Schedule,
                                 ProgramsKnow = p.Programsknow,
                                 WhosRecommended = p.WhosRecommended,
                                 FamilyOrFriend = (bool)p.FamilyOrFriend,
                                 FamilyOrFriendName = p.FamilyOrFriendName,
                                 FamilyOrFriendPuesto = p.FamilyOrFriendPuesto,
                                 FamilyOrFriendRelationship = p.FamilyOrFriendRelationship,
                                 PositionCategoryID = (int)p.PositionCategoryID,
                                 Interviewed = (bool)p.interviewed,
                                 CandidateStateID = (int)p.CandidateStateID,
                                 Picture64Bit = container.getSolicitudesServices().GetPictureCandidate(p.Cedula).PictureCadidate,
                                 PosicionName = container.getSolicitudesServices().GetCandidateStatesByID((int)p.CandidateStateID).State,
                             }).FirstOrDefault();

            return candidate;
        }

        [WebMethod]
        public Employees GetDepartmentHead(string Department)
        {
            var emp = worker.DepartmentHeads.GetDepartmentHead(Department);
            if (emp != null)
            {
                if (emp.Picture != null)
                {
                    var img = GUIHelpers.ImagesHelper.byteArrayToImage(emp.Picture);
                    emp.PictureBase64 = ImageToBase64(img, ImageFormat.Jpeg);
                }

                return emp;
            }
            return null;
        }
        [WebMethod]
        public List<ProblemType> getProblemByDept(string Department)
        {           
          return  worker.TSTickets.getProblemTypeByDeparment(Department);
        }

        public string ImageToBase64(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

        [WebMethod]
        public EmployeeExtension GetExtensionByID(int id)
        {
            return worker.Extensions.GetExtensionByID(id);
        }
    }
}