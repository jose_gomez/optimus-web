﻿using Optimus.Web.BC.Models;
using Optimus.Web.BC.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Security
{
    public partial class RecoverPassword : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BC.ServiceContainer ServCont = new BC.ServiceContainer();
            BC.Services.UnitOfWork worker = ServCont.GetUnitOfWork();
            if (Request.QueryString["UserID"] != null)
            {
                string userName = Request.QueryString["UserId"].ToString();
                var user = worker.User.Find(x => x.UserName == userName).FirstOrDefault();
                if(user!= null)
                {
                    if(user.Email != null)
                    {
                        List<MailAddress> destinataries = new List<MailAddress>() { new MailAddress(user.Email) };
                        var guid = Guid.NewGuid().ToString();
                        string encryptedGuid = EncryptionUtility.Encrypt(guid, user.UserID.ToString());
                        var resetService = ServCont.GetResetPassHistory();
                        var userToSave = new User();
                        userToSave.userName = user.UserName;
                        userToSave.userID = user.UserID;
                        resetService.Save(userToSave, guid);
                        string body = string.Format("Saludos, Para recuperar su clave de optimus web por favor hacer click en el siguiente enlace:"+
                                            "<a href = '{0}/security/FrmResetPass.aspx?UserId={1}&Code={2}' > Recuperación de claves Optimus </a> " +
                                             " Este enlace tiene un codigo que expirara en 30 minutos. Por favor recupere su contraseña antes de este tiempo",
                                          Request.Url.Scheme + "://" + Request.Url.Authority + Request.ApplicationPath.TrimEnd('/') + "/",
                                         user.UserName, Server.UrlEncode(encryptedGuid));
                        DRL.Utilities.Mail.Mailer.SendMail("Recuperación de contraseña Optimus", destinataries,
                                                body, true);
                    }
                    else
                    {
                        errorLabel.Text = "Su usuario no tiene correo electronico registrado. ";
                        smn.InnerText = "Comuníquese con en el departamento de sistemas";
                    }
                }
                else
                {
                    errorLabel.Text = "No se ha encontrado su usuario o el mismo no existe. ";
                    smn.InnerText = "Comuníquese con en el departamento de sistemas";
                }
            }
        }
    }
}