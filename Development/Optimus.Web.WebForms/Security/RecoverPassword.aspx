﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RecoverPassword.aspx.cs" Inherits="Optimus.Web.WebForms.Security.RecoverPassword" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    
    <div class="panel-group">
        <div class="panel panel-danger">
            <div class="panel-heading">
                <h2 runat="server" id="TituloH2">Recuparación de contraseña.</h2>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-12">                  
                        <asp:Label runat="server" ID="errorLabel" Text="Se ha enviado un correo con un link de recuperación a su correo electronico" 
                            Font-Size="Medium"></asp:Label>
                        <p  class="text-warning"><small id="smn" runat="server" ></small></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:LinkButton runat="server" Text="Ir a pagina principal" PostBackUrl="~/Default.aspx"></asp:LinkButton>
                    </div>
                </div>
        
            </div>

        </div>
    </div>
</asp:Content>
