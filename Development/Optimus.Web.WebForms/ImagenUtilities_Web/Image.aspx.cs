﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace ImageUtilities_Web
{
    public partial class Image : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public static System.Web.UI.WebControls.Image byteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn.Length == 0)
                return null;
            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            System.Web.UI.WebControls.Image img = byteArrayIn != null ? (System.Web.UI.WebControls.Image)converter.ConvertFrom(byteArrayIn) : null;

            return img;
        }

    }
}