﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TicketsSon.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.TicketsSon" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="container">                    
                 <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Departamento:" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="DropDownList1" CssClass="form-control"  runat="server"></asp:DropDownList>
                            </div>
                        </div>
                          <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Prioridad:" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Título / Solicitud" Font-Bold="true"></asp:Label>
                                <asp:TextBox runat="server" ClientIDMode="Static" ID="TextBox1" CssClass="form-control" placeholder="Ingrese una solicitud"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Descripción" Font-Bold="true"></asp:Label>
                                <%--<asp:TextBox runat="server" ClientIDMode="Static" ID="txtDescription" Style="min-width: 90%" CssClass="form-control" placeholder="Ingrese una descripción" TextMode="MultiLine"></asp:TextBox>--%>
                                <div class="adjoined-bottom">
                                    <div class="grid-container">
                                        <div class="grid-width-100">
                                            <div <%--id="editor"--%> class="editor">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button runat="server" class="btn btn-default" Text="Aceptar" OnClick="ActualizarPriodidad" />
                </div>
            </div>
</asp:Content>
