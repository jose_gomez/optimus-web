﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="TicketsDetalle.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.TicketsDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .modal-header-danger {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #d9534f;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }

        .modal-header-info {
            color: #fff;
            padding: 9px 15px;
            border-bottom: 1px solid #eee;
            background-color: #5bc0de;
            -webkit-border-top-left-radius: 5px;
            -webkit-border-top-right-radius: 5px;
            -moz-border-radius-topleft: 5px;
            -moz-border-radius-topright: 5px;
            border-top-left-radius: 5px;
            border-top-right-radius: 5px;
        }
    </style>


    <script>
        $(document).ready(function () {
            initSample();
            setPorcentStatus();
            ReadOnlyComment();
            VisibiliteElement();
            setPriodity();
            getTicketHierarchy();
            //  Prueba();
        });
    </script>


    <script>
        function Prueba() { CKEDITOR.replace('editor1'); CKEDITOR.replace('editor2'); }

        function getTicketHierarchy() {
            var ticketNumber = $('#MainContent_lblTickets').text();
            $.ajax({
                url: '/WebServices/OptimusWebServices.asmx/GetTicketHierarchy',
                type: 'Post',
                data: '{ticketId:' + ticketNumber + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d) {

                        $('#jqtree').tree({
                            data: result.d,
                            autoOpen: true,
                            closedIcon: $('<i class="fa fa-arrow-circle-right"></i>'),
                            openedIcon: $('<i class="fa fa-arrow-circle-down"></i>'),
                            onCreateLi: function (node, $li) {
                                $li.find('.jqtree-element').append(
                                    "<a href='/tickets/TicketsDetalle?ID=" +  node.id +  "' class='edit'>"+node.name+"</a><p>" + node.progress + "%</p>"
                                    );
                            }
                        });
                        var x = jqtree.getElementsByTagName('span');
                        $(x).hide();
                    }
                }
            });
        }
        function setProblemHidden() {
            var a = $('#MainContent_drpProblem option:selected').val();
            $('#MainContent_HiddenProblem').attr("value", a);
            return false;


        }

    </script>
    <script>
        function getproblem() {
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/getProblemByDept',
                data: '{Department:\'' + $('#MainContent_DeparmentDropDown option:selected').text() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    $('#MainContent_drpProblem').empty().append('<option selected="selected" value="0">Seleccione</option>');
                    $.each(data.d, function () {
                        $('#MainContent_drpProblem').append($("<option></option>").val(this['ProblemTypeID']).html(this['Name']));
                    });
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }
            });

        }
    </script>

    <script type="text/javascript">

        function setPriodity() {
            var priodidad = $("#MainContent_PriodidadLabel").text();

            switch (priodidad.trim()) {

                case "Baja": {

                    $("#MainContent_PriodidadLabel").css({ 'color': 'orange' });
                } break;

                case "Normal": {
                    $("#MainContent_PriodidadLabel").css({ 'color': 'green' });


                } break;
                case "Alta": {
                    $("#MainContent_PriodidadLabel").css({ 'color': 'red' });
                } break;
            }


        }
        function setPorcentStatus() {
            $('.progress-bar').each(function () {
                var status = $(this).text();
                if (status.trim() == "100%") {
                    $(this).addClass('progress-bar-success');
                }
            });

        }
        function ReadOnlyComment() {
            if ($('#MainContent_lblEstado').text() == 'Validate' || $('#MainContent_lblEstado').text() == 'Cancelled') {
                CKEDITOR.config.readOnly = true;
            }
        }
        function VisibiliteElement() {
            $('#CancelacionAlert').hide();
            $('#ValidadoAlert').hide();
            if ($('#MainContent_lblEstado').text() == 'Cancelled') {
                $('#CancelarButton').text('Cancelado');
                $('#CancelarButton').attr('disabled', 'disabled');
                $('#CancelacionAlert').slideDown();
            } else if ($('#MainContent_lblEstado').text() == 'Validate') {
                $('#ValidadoAlert').slideDown();
            }
            else {

                $('#CancelacionAlert').hide();
                $('#ValidadoAlert').hide();
            }

        }
        function MostrarAlerta() {
            $('#myModal').modal('show');
        }
        function GuardarComentario() {
            var comment = CKEDITOR.instances.editor.getData();
            __doPostBack('GuardarComment', comment)

        }

        function GuardarHijoComentario() {
            var comment = CKEDITOR.instances.editor2.getData();
            __doPostBack('GuardarCommentHijo', comment)
        }

    </script>
    <div class="container">
        <br />
        <asp:HiddenField runat="server" ID="HDFieldUserID" />
        <asp:HiddenField runat="server" ID="PadreHidden" />

        <div id="CancelacionAlert" class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Ticket Cancelado!</strong> Este Ticket esta cancelado.No se puede realizar mas cambios
        </div>
        <div id="ValidadoAlert" class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
            <strong>Ticket Validado!</strong> Este Ticket esta validado y cerrado.No se puede realizar mas cambios
        </div>
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="lblTitulo" Font-Size="X-Large" Font-Bold="true"></asp:Label>
                    </div>
                </div>
                <br />
                <div class="row">
                    <div class="col-md-6">
                        <label>No Tickets:</label>
                        <asp:Label runat="server" ID="lblTickets"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <label>Prioridad:</label>
                        <asp:Label runat="server" ID="PriodidadLabel"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Creado por:</label>
                        <asp:Label runat="server" ID="lblCreadoPor"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <label>Fecha Creación:</label>
                        <asp:Label runat="server" ID="lblFecha"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Departamento Asignado:</label>
                        <asp:Label runat="server" ID="lblDeptAsignado"></asp:Label>
                    </div>
                    <div class="col-md-6">
                        <label>Departamento adjudico:</label>
                        <asp:Label runat="server" ID="lblDeptAjudico"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <label>Estado: </label>
                        <asp:Label runat="server" ID="lblEstado"></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                 <a href="#cambioEstado" id="cambioEstadoA" runat="server" data-toggle="collapse">
                     <i class="glyphicon glyphicon-asterisk"></i>Cambiar Estado</a>
                        <div id="cambioEstado" class="collapse">
                            <span style="font-weight: bold">Cambiar estado:</span>
                            <asp:DropDownList runat="server" ID="dropEstadosTickets" OnSelectedIndexChanged="ActualizarEstado" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label>Usuarios Asignados: </label>
                        <asp:Label runat="server" ID="lblUsernameAssig"></asp:Label>
                        &nbsp;&nbsp;&nbsp;
                <a href="#cambioUsuario" id="cambioUsuarioA" runat="server" data-toggle="modal" data-target="#ModalSeleccionUsuario">
                    <i class="glyphicon glyphicon-asterisk"></i>Asignar nuevo Usuario</a>
                    </div>
                    <div class="col-md-6">
                        <div style="float: left">
                            <label>Porcentaje: </label>
                        </div>
                        <div class="progress" style="width: 100px; float: left; padding-right: 1px">
                            <div runat="server" id="divProgreso" class="progress-bar progress-bar-striped active" role="progressbar"
                                aria-valuenow="" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <%--Codigo para la parte de asignar varios usuarios a un tickets--%>
                        <asp:Repeater runat="server" ID="repetar1" OnItemDataBound="repeater_ItemDataBound">
                            <ItemTemplate>
                                <span class="tag label label-pill label-primary">
                                    <span style="font-size: x-small"><%#Eval("Responsible")%> </span>
                                    <asp:LinkButton ID="ResponsibleLink" CssClass="resLink" runat="server"
                                        OnCommand="EliminarResponsible" CommandName='<%#Eval("EmployeesIDResponsible")%>' CommandArgument='<%#Eval("TicketNumber")%>'><i class="remove glyphicon glyphicon-remove-sign glyphicon-white"></i></asp:LinkButton>
                                </span>
                            </ItemTemplate>
                        </asp:Repeater>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div id="cambioPorcentaje">
                            <asp:RadioButtonList ID="PorcentajeRadioButtonList" runat="server" RepeatDirection="Horizontal" OnSelectedIndexChanged="GuardarPorcentaje" AutoPostBack="true">
                                <asp:ListItem Value="25">25%</asp:ListItem>
                                <asp:ListItem Value="50">50%</asp:ListItem>
                                <asp:ListItem Value="75">75%</asp:ListItem>
                                <asp:ListItem Value="100">100%</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <asp:Button runat="server" ID="ValidateButton" OnClick="ActualizarValidate" />
                    </div>
                    <div class="col-md-6">
                        <a runat="server" href="/tickets/tickets.aspx" class="glyphicon glyphicon-arrow-left">Volver a Tickets</a>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <asp:LinkButton runat="server" Text="Ir a Tickets padre" ID="IrTicketPadreLink" Visible="false" OnClick="IrTicketPadre"></asp:LinkButton>
                        <div id="jqtree">
                            hello
                        </div>
                        <!--jqtree goes here -->
                    </div>
                    <div class="col-md-6">
                        <div runat="server" id="CancelarTicketsDiv">
                            <button type="button" id="AccionesButton" class="btn btn-sm" data-toggle="modal" data-target="#AccionesTickets">Acciones</button>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="row">
                    <div class="col-md-4">
                        <label>Descripción:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p runat="server" id="PDescripcion">
                        </p>
                    </div>
                </div>
                <br />
                <br />
                <div class="row">
                    <ul class="nav nav-tabs">
                        <li class="active"><a data-toggle="tab" role="tab" href="#ComentariosTab"><i class="glyphicon glyphicon-comment"></i>
                            <label style="font: bold">Comentarios: </label>
                        </a></li>
                        <li><a data-toggle="tab" role="tab" href="#HistorialTab">Historial</a></li>
                    </ul>
                </div>
                <div>
                    <div class="tab-content">
                        <div id="ComentariosTab" role="tabpanel" class="tab-pane fade in active">
                            <br />
                            <div class="col-md-12">
                                <asp:Repeater runat="server" ID="CommentRepeater">
                                    <ItemTemplate>
                                        <div class="panel panel-default">
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-md-12">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <asp:Label runat="server" Text='<%#Eval("UserName")%>' Font-Bold="true"></asp:Label>
                                                            </div>
                                                            <div style="float: right">
                                                                <asp:Label runat="server" Text='<%#Eval("Datetime")%>' Font-Bold="true"></asp:Label>
                                                            </div>
                                                            <hr />
                                                        </div>
                                                        <div>
                                                            <p runat="server" id="PCometarios">
                                                                <%# Eval("Comentarios")%>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </ItemTemplate>
                                </asp:Repeater>
                            </div>
                            <div class="row" id="comment" runat="server">
                                <div class="col-md-12">
                                    <div class="adjoined-bottom">
                                        <div class="grid-container">
                                            <div class="grid-width-100">
                                                <div id="editor" class="editor">
                                                </div>

                                                <%--<textarea name="editor1" id="editor1" rows="10" cols="80"></textarea>--%>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-4" style="padding-top: 10px">
                                    <asp:Button runat="server" Text="Agregar comentario" CssClass="btn btn-primary" OnClientClick="GuardarComentario()" />
                                </div>
                            </div>
                        </div>
                        <div id="HistorialTab" role="tabpanel" class="tab-pane fade">
                            <br />
                            <asp:Repeater runat="server" ID="HistorialRepeater">
                                <ItemTemplate>
                                    <div class="panel panel-default">
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <asp:Label runat="server" Text='<%#Eval("UserName")%>' Font-Bold="true"></asp:Label>
                                                        </div>
                                                        <div style="float: right">
                                                            <asp:Label runat="server" Text='<%#Eval("Datetime")%>' Font-Bold="true"></asp:Label>
                                                        </div>
                                                        <hr />
                                                    </div>
                                                    <div>
                                                        <p runat="server" id="PCometarios">
                                                            <%# Eval("Comentarios")%>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </ItemTemplate>
                            </asp:Repeater>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="ModalSeleccionUsuario" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Asignación de usuarios </h4>
                </div>
                <div class="modal-body">
                    <asp:ListBox runat="server" Style="min-width: 100%" ID="ListUsuariosDepartamento" SelectionMode="Multiple"></asp:ListBox>
                    <p class="text-warning"><small>Mantenga presionada la tecla control para seleccionar varios usuarios</small></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    <asp:Button runat="server" class="btn btn-primary" Text="Guardar" OnClick="ActualizarResponsable" />
                </div>
            </div>
        </div>
    </div>
    <div id="AccionesTickets" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Acciones del Tickets </h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <div id="CambiarPrioridadDiv" runat ="server">
                        <label>Cambiar Prioridad: </label>
                        <asp:DropDownList runat="server" CssClass="form-control" ID="PriodidadDropDownList"></asp:DropDownList>
                            </div>
                    </div>
                    <div class="form-group">
                        <div id="CancelarDiv" runat ="server">
                        <label>Cancelar Tickets:</label>
                        <button type="button" id="CancelarButton" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#CancelarTickets">Cancelar Tickets</button>
                            </div>
                    </div>
                    <div class="form-group">
                        <label>Crear Tickets Hijo: </label>
                        <button type="button" id="TicketsHijoButton" class="btn btn-default btn-sm" data-toggle="modal" data-target="#TicketsHijo">Crear Tickets Hijo</button>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button runat="server" class="btn btn-default" Text="Aceptar" OnClick="ActualizarPriodidad" />
                </div>
            </div>
        </div>
    </div>
    <div id="CancelarTickets" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Cancelar Tickets </h4>
                </div>
                <div class="modal-body">
                    <p class="text-warning">Seguro que quiere cancelar el Tickets?</p>
                    <p class="text-warning"><small>Recuerde que los Tickets cerrados no se pueden reabrir</small></p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button runat="server" class="btn btn-danger btn-sm" Text="Cancelar" OnClick="CancelarTickets" />
                </div>
            </div>
        </div>
    </div>
    <div id="TicketsHijo" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Someter ticket Hijo </h4>
                </div>
                <div class="modal-body">

                    <div class="form-group">
                        <asp:Label runat="server" Text="Departamento:" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="DeparmentDropDown" CssClass="form-control" onchange="return getproblem()"   runat="server"></asp:DropDownList>
                    </div>
                     <div class="form-group">
                                <asp:Label runat="server" Text="Categoría:" Font-Bold="true"></asp:Label>
<%--                                <asp:DropDownList ID="drpProblem" CssClass="form-control" onchange="return setProblemHidden()" runat="server"></asp:DropDownList>--%>
                         <select ID="MainContent_drpProblem" Class="form-control" onchange="return setProblemHidden()">

                         </select>
                                <asp:HiddenField ID="HiddenProblem" runat="server" />
                            </div>
                    <div class="form-group">
                        <asp:Label runat="server" Text="Prioridad:" Font-Bold="true"></asp:Label>
                        <asp:DropDownList ID="PrioridadDropDownTicketHijo" CssClass="form-control" runat="server"></asp:DropDownList>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" Text="Título / Solicitud" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="txtTitulo" CssClass="form-control" placeholder="Ingrese una solicitud"></asp:TextBox>
                    </div>

                    <div class="form-group">
                        <asp:Label runat="server" Text="Descripción" Font-Bold="true"></asp:Label>
                        <%--<asp:TextBox runat="server" ClientIDMode="Static" ID="txtDescription" Style="min-width: 90%" CssClass="form-control" placeholder="Ingrese una descripción" TextMode="MultiLine"></asp:TextBox>--%>
                        <div class="adjoined-bottom">
                            <div class="grid-container">
                                <div class="grid-width-100">
                                    <%-- <textarea name="editor1" id="editor2" rows="10" cols="80"></textarea>--%>
                                    <div id="editor2"></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <asp:Button runat="server" ID="btnGuardar2" class="btn btn-default" ClientIDMode="Static" Text="Aceptar" onsubmit="return validate()" OnClientClick="GuardarHijoComentario()" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
