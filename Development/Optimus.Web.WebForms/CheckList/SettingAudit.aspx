﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SettingAudit.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.SettingAudit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        input, select, textarea {
            max-width: none;
        }
    </style>
    <script>
        function cambiarCombo() {
            var myValor = document.getElementById('<%=cbAuditFormat.ClientID%>').value;

            if (myValor == "LITERAL") {
                document.getElementById('<%=txtStartValue.ClientID%>').disabled = true;
                document.getElementById('<%=txtEndValue.ClientID%>').disabled = true;
                document.getElementById('<%=cbEvaluationType.ClientID%>').disabled = true;
            }
            if (myValor == "NUMERICO") {
                document.getElementById('<%=txtStartValue.ClientID%>').disabled = false;
                document.getElementById('<%=txtEndValue.ClientID%>').disabled = false;
                document.getElementById('<%=cbEvaluationType.ClientID%>').disabled = false;
            }
        }
    </script>

    <div id="BootstrapMessage" runat="server" >
    </div>

    <div class="panel panel-default">
        <!--HEADER-->
        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">CONFIGURACIÓN DE LAS AUDITORIAS </a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <!--BODY-->
        <div id="collapse2" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">Control =
                            <asp:Label ID="lbControlID" runat="server" Text="0"></asp:Label>
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div>
                            <label class="control-label col-sm-12" for="Select2">Auditoria</label>
                            <select id="cbAudit" name="cbAudit" runat="server" class="form-control">
                                <option value="0">SELECCIONE </option>
                            </select><br />
                            <div class="panel-footer" style="vertical-align: middle; text-align: right">
                                <asp:Button ID="btnEditAudit" runat="server" Text="Editar Auditoria" CssClass="btn btn-primary" OnClick="btnEditAudit_Click" />
                                <asp:Button ID="btnNewAudit" runat="server" Text="Nueva Auditoria" CssClass="btn btn-primary" OnClick="btnNewAudit_Click" />
                            </div>

                            <hr />
                        </div>
                        <div class="main row">

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtAuditName">Nombre de la Auditoria</label>
                                    <input type="text" class="form-control" id="txtAuditName" runat="server" placeholder="Nombre de la Auditoria" style="text-transform: uppercase" />
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="Select1">Formato de la Auditoria</label>
                                    <select id="cbAuditFormat" name="cbAuditFormat" runat="server" class="form-control" onchange="cambiarCombo();">
                                        <option value="0">SELECCIONE </option>
                                        <option value="LITERAL">SI-NO-N/A </option>
                                        <option value="NUMERICO">RANGO DE NÚMEROS </option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtMaxFhoto">Máximo de Foto</label>
                                    <input class="form-control" id="txtMaxFhoto" runat="server" placeholder="Max Fhoto" type="number" />
                                </div>
                            </div>

                            <div class="col-sm-6 col-lg-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtMaxFhoto">Tipo de Evaluación</label>
                                    <asp:DropDownList ID="cbEvaluationType" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtStartValue">Desde</label>
                                    <input class="form-control" id="txtStartValue" runat="server" placeholder="Valor de Inicio" type="number" />
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtEndValue">Hasta</label>
                                    <input class="form-control" id="txtEndValue" runat="server" placeholder="Valor Final" type="number" />
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtInstrucciones">Instrucciones</label>
                                    <asp:TextBox class="form-control" ID="txtInstrucciones" runat="server" placeholder="Instrucciones de uso..." TextMode="MultiLine" Height="100"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-sm-12 col-lg-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtDescripcionAudit">Descripción de la Auditoria</label>
                                    <asp:TextBox class="form-control" ID="txtDescripcionAudit" runat="server" placeholder="Descripción detallada de la auditoria..." TextMode="MultiLine" Height="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <asp:Button ID="btnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                    </div>
                </div>

            </div>
        </div>



    </div>

    <div class="panel panel-info">
        <div class="panel-heading">
            <h4 class="panel-title">PERSONAS A ALERTAR
            </h4>
        </div>
        <div class="panel-body">

            <div style="overflow: scroll; height: auto;">
                <asp:GridView ID="dgvAlertas" runat="server" EmptyDataText="No existen Alertas." ShowHeaderWhenEmpty="false" ShowHeader="true" EnableModelValidation="False" class="table table-striped">
                    <Columns>
                        <asp:BoundField HeaderText="Emails" ShowHeader="false" DataField="Email" />
                    </Columns>
                    <EmptyDataTemplate>
                        <div class="alert-info">
                            <strong>Información!</strong> No se encontro ningun registro.
                        </div>
                    </EmptyDataTemplate>
                    <SelectedRowStyle BackColor="#FFCC00" />
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>
