﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class SettingAudit : System.Web.UI.Page
    {
        public int controlID = 0;
        ServiceContainer container;
        UnitOfWork worker;

        public SettingAudit()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarControlType();
                CargarEvaluationType();

                //var audControl = container.GetAuditoryControlService().GetAllAuditControl();
                //cbAudit.DataSource = audControl;
                //cbAudit.DataValueField = "ControlID";
                //cbAudit.DataTextField = "NameAudit";
                //cbAudit.DataBind();

                btnGuardar.Enabled = false;
                txtAuditName.Disabled = true;

            }
        }

        public void CargarEvaluationType()
        {
            var consEvalType = container.GetAuditoryControlService().GetAllEvaluationType();
            consEvalType.Insert(0, new SHAuditEvalutionType { Name = "SELECCIONE", AuditEvalutionTypeID = 0 });
            cbEvaluationType.DataSource = consEvalType;
            cbEvaluationType.DataValueField = "AuditEvalutionTypeID";
            cbEvaluationType.DataTextField = "Name";
            cbEvaluationType.DataBind();
        }
        public void CargarControlType()
        {
            var audControl = container.GetAuditoryControlService().GetAllAuditControl();
            audControl.Insert(0, new SHAuditControl { NameAudit = "SELECCIONE", ControlID = 0 });
            cbAudit.DataSource = audControl;
            cbAudit.DataValueField = "ControlID";
            cbAudit.DataTextField = "NameAudit";
            cbAudit.DataBind();
        }

        public void Limpiar()
        {
            txtDescripcionAudit.Text = txtInstrucciones.Text = txtMaxFhoto.Value = txtStartValue.Value = txtEndValue.Value = string.Empty;
            txtAuditName.Value = "";
            cbAuditFormat.Value = "0";
            cbEvaluationType.SelectedValue = "0";
            lbControlID.Text = "0";
        }
        public void CargarEmail()
        {
            dgvAlertas.AutoGenerateColumns = false;
            var controlService = container.GetAuditoryControlService().GetEmailsByAuditControlID(txtAuditName.Value);
            dgvAlertas.DataSource = controlService;
            dgvAlertas.DataBind();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                List<string> mensajes = new List<string>(); 
                BootstrapMessage.Attributes["class"] = "alert alert-danger";
                var consNameControl = container.GetAuditoryControlService().GetAuditControlByName(txtAuditName.Value.Trim());

                if (string.IsNullOrEmpty(txtAuditName.Value.Trim()))
                {
                    mensajes.Add("Ingresar el nombre de la auditoria");
                }
                if (cbAuditFormat.Value == "0")
                {
                    mensajes.Add("Debe de seleccionar el formato de la auditoria");
                }
                if (string.IsNullOrEmpty(txtMaxFhoto.Value.Trim()))
                {
                    mensajes.Add("Debe de ingresar la cantidad máxima de fotos");
                }

                if(mensajes.Count != 0)
                {
                    BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);
                }
                else
                {

                    if (int.Parse(lbControlID.Text) == 0 && consNameControl.Count != 0)
                    {
                        BootstrapMessage.InnerHtml = "No se pueden registrar varias auditorias con el mismo nombre.";
                        return;
                    }

                    SHAuditControl auditControl = new SHAuditControl();

                    auditControl.ControlID = int.Parse(lbControlID.Text);
                    auditControl.NameAudit = txtAuditName.Value.ToUpper().Trim();
                    auditControl.DescriptionAudit = txtDescripcionAudit.Text;
                    auditControl.Answer_type = cbAuditFormat.Value;
                    auditControl.Instrucciones = Server.HtmlEncode(txtInstrucciones.Text.Trim());

                    if (cbAuditFormat.Value == "NUMERICO")
                    {
                        List<string> mensajes2 = new List<string>();
                        if (string.IsNullOrEmpty(txtStartValue.Value.Trim()))
                            mensajes2.Add("Ingresar el número de inicio");
                        if (string.IsNullOrEmpty(txtEndValue.Value.Trim()))
                            mensajes2.Add("Ingresar el número final");
                        if (int.Parse(cbEvaluationType.SelectedValue) == 0)
                            mensajes2.Add("Seleccione el tipo de Evaluación a aplicar");
                        if (mensajes2.Count != 0)
                        {
                            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes2);
                        }
                        else
                        {
                            if (int.Parse(txtStartValue.Value) > int.Parse(txtEndValue.Value))
                            {
                                BootstrapMessage.InnerHtml = "El valor de inicio no puede ser mayor que el valor final";
                            }
                            else
                            {
                                auditControl.valorization_from = int.Parse(txtStartValue.Value);
                                auditControl.valorization_to = int.Parse(txtEndValue.Value);
                                auditControl.AuditEvalutionTypeID = int.Parse(cbEvaluationType.SelectedValue);
                            }
                        }
                    }

                    auditControl.Images_number = int.Parse(txtMaxFhoto.Value);
                    auditControl.Users = User.Identity.Name;

                    var auditControlService = container.GetAuditoryControlService();
                    var consAuditControl = auditControlService.SaveAuditControlByID(auditControl);

                    if (consAuditControl.Count > 0)
                    {
                        if(int.Parse(lbControlID.Text) == 0)
                        {
                            var alertServices = container.GetAlertService();
                            SYAlertsType model = new SYAlertsType();
                            model.AlertTypes = txtAuditName.Value.Trim();
                            alertServices.SaveAlertType(model);
                        }

                        BootstrapMessage.Attributes["class"] = "alert alert-success";
                        BootstrapMessage.InnerHtml = "Registro Exitoso";
                        // Activar Controles
                        btnGuardar.Enabled = false;
                        txtAuditName.Disabled = true;
                        Limpiar();
                    }
                    else
                    {
                        BootstrapMessage.Attributes["class"] = "alert alert-danger";
                        BootstrapMessage.InnerHtml = "No se guardo ningún registro";
                    }
                }
            }
            catch (Exception ex)
            {
                AlertHelpers.ShowAlertMessage(this, "Exception", ex.Message);
            }
        }

        protected void btnEditAudit_Click(object sender, EventArgs e)
        {
            if (int.Parse(cbAudit.Value) == 0)
            {
                BootstrapMessage.Attributes["class"] = "alert alert-danger";
                BootstrapMessage.InnerHtml = "Seleccione una auditoria";
            }
            else
            {
                var contService = container.GetAuditoryControlService();
                var consControl = contService.GetAuditControlByID(int.Parse(cbAudit.Value)).FirstOrDefault();

                lbControlID.Text = consControl.ControlID.ToString();
                txtAuditName.Value = consControl.NameAudit;
                txtDescripcionAudit.Text = consControl.DescriptionAudit;
                cbAuditFormat.Value = consControl.Answer_type;                
                txtInstrucciones.Text = Server.HtmlDecode(consControl.Instrucciones);
                txtMaxFhoto.Value = consControl.Images_number.ToString();
                txtStartValue.Value = consControl.valorization_from.ToString();
                txtEndValue.Value = consControl.valorization_to.ToString();
                if (!string.IsNullOrEmpty(consControl.AuditEvalutionTypeID.ToString()))
                    cbEvaluationType.SelectedValue = consControl.AuditEvalutionTypeID.ToString();
                else
                    cbEvaluationType.SelectedValue = "0";
                //
                dgvAlertas.AutoGenerateColumns = false;
                var controlService = container.GetAuditoryControlService().GetEmailsByAuditControlID(txtAuditName.Value);
                dgvAlertas.DataSource = controlService;
                dgvAlertas.DataBind();
                // Activar Controles
                btnGuardar.Enabled = true;
                txtAuditName.Disabled = true;
            }
        }

        protected void btnNewAudit_Click(object sender, EventArgs e)
        {
            Limpiar();
            btnGuardar.Enabled = true;
            txtAuditName.Disabled = false;
        }
    }
}