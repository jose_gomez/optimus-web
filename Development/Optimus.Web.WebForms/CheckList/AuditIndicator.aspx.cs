﻿using Optimus.Web.BC;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class AuditIndicator : System.Web.UI.Page
    {
        ServiceContainer container;
        public AuditIndicator()
        {
            InitializeCulture();
            container = new ServiceContainer();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarAut();
                CargarCat();

                string idInd = Request.QueryString["idIndic"];

                if (string.IsNullOrEmpty(idInd))
                {
                    Response.Redirect("AuditIndicator.aspx?idIndic=0");
                }

                if (int.Parse(idInd) != 0)
                {
                    var indicService = container.GetAuditIndicatorService();
                    var consIndic = indicService.GetIndicatorByID(int.Parse(idInd));
                    txtIndicator.Text = consIndic.FirstOrDefault().AuditIndicator;
                    cbCategory.Value = consIndic.FirstOrDefault().AuditCategory.ToString();
                }
            }
        }

        public void CargarInd()
        {
            dgvIndicator.AutoGenerateColumns = false;
            var audtInt = container.GetAuditIndicatorService();
            var consInd = audtInt.GetAllIndicator().Where(x => x.IdControl == int.Parse(cbAuditType.SelectedValue)).ToList();
            dgvIndicator.DataSource = consInd;
            dgvIndicator.DataBind();
        }

        public void CargarAut()
        {
            var audControl = container.GetAuditoryControlService().GetAllAuditControl();
            audControl.Insert(0, new SHAuditControl { NameAudit = "SELECCIONE", ControlID = 0 });
            cbAuditType.DataSource = audControl;
            cbAuditType.DataValueField = "ControlID";
            cbAuditType.DataTextField = "NameAudit";
            cbAuditType.DataBind();
            
        }

        public void CargarCat()
        {
            var audtCat = container.GetAuditCategoriesService();
            var consCat = audtCat.GetAllCategory();

            if (int.Parse(cbAuditType.SelectedValue) != 0)
            {
                consCat = audtCat.GetAllCategory().Where(x => x.ControlID == int.Parse(cbAuditType.SelectedValue)).ToList();
            }

            consCat.Insert(0, new SHAuditCategory { Categoria = "SELECCIONE", ID = 0 });
            cbCategory.DataSource = consCat;
            cbCategory.DataValueField = "ID";
            cbCategory.DataTextField = "Categoria";
            cbCategory.DataBind();
        }

        protected void btnSaveIndicator_Click(object sender, EventArgs e)
        {
            try
            {
                BootstrapMessage.Attributes["class"] = "alert alert-danger";
                List<string> mensajes = new List<string>();

                if (string.IsNullOrEmpty(txtIndicator.Text))
                {
                    mensajes.Add("Ingrese un Indicador");
                }
                if (int.Parse(cbCategory.Value) == 0)
                {
                    mensajes.Add("Debe de cargar las categorías");
                }

                if (mensajes.Count != 0)
                {
                    BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);
                }
                else
                {
                    SHAuditIndicator tabInd = new SHAuditIndicator();
                    var audService = container.GetAuditIndicatorService();

                    tabInd.ID = int.Parse(Request.QueryString["idIndic"]);
                    tabInd.AuditIndicator = txtIndicator.Text.Trim();
                    tabInd.AuditCategory = Convert.ToInt32(cbCategory.Value);
                    tabInd.User = User.Identity.Name;

                    audService.SaveByID(tabInd);

                    if (int.Parse(Request.QueryString["idIndic"]) != 0)
                        Response.Redirect("AuditIndicator.aspx?idIndic=0");

                    mensajes.Add("Indicador Registrado");
                    BootstrapMessage.Attributes["class"] = "alert alert-success";
                    BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);

                    txtIndicator.Text = string.Empty;
                    CargarInd();
                }
            }
            catch (Exception ex)
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", ex.Message);
            }
        }

        protected void selIndicator_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            txtIndicator.Text = Server.HtmlDecode(grdrow.Cells[1].Text);
            cbCategory.Value = grdrow.Cells[2].Text;
        }

        protected void cbAuditType_SelectedIndexChanged(object sender, EventArgs e)
        {
            CargarCat();
            CargarInd();
        }
    }
}