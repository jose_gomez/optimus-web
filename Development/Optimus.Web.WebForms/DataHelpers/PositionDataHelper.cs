﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class PositionDataHelper
    {

        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.Posicion Get(string path)
        {
            DTO.Posicion position = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                position = DTO.Factory.PositionFactory.Deserialize(data);
            }
            return position;
        }

        public static List<DTO.Posicion> GetList(string path)
        {
            List<DTO.Posicion> positions = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                positions = DTO.Factory.PositionFactory.DeserializeList(data);
            }
            return positions;
        }

        public static void Save(DTO.Posicion position)
        {
        }

        #endregion Public Methods

    }
}