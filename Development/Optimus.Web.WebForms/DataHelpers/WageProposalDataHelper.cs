﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class WageProposalDataHelper
    {

        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.PropuestaCompensacionBeneficio Get(string path)
        {
            DTO.PropuestaCompensacionBeneficio wageProposal = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                wageProposal = DTO.Factory.WageProposalFactory.Deserialize(data);
            }
            return wageProposal;
        }

        public static List<DTO.PropuestaCompensacionBeneficio> GetList(string path)
        {
            List<DTO.PropuestaCompensacionBeneficio> wageProposals = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                wageProposals = DTO.Factory.WageProposalFactory.DeserializeList(data);
            }
            return wageProposals;
        }

        public static DTO.PropuestaCompensacionBeneficio Save(DTO.PropuestaCompensacionBeneficio wageProposal)
        {
            if (wageProposal.WageProposalID == 0)
            {
                client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
                var a = DTO.Factory.WageProposalFactory.Serialize(wageProposal);
                HttpResponseMessage response = Task.Run(() => client.PostAsync(Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_POST, new StringContent(a, System.Text.Encoding.Unicode, "application/json"))).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                    return wageProposal = DTO.Factory.WageProposalFactory.Deserialize(data);
                }
                else
                {
                    throw new System.Exception(Core.OptimusMessages.ERROR_MESSAGE);
                }
            }
            else
            {
                client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
                var a = DTO.Factory.WageProposalFactory.Serialize(wageProposal);
                HttpResponseMessage response = Task.Run(() => client.PutAsync(Core.OptimusApiUrls.PropuestasCompensacionesBeneficios_PUT.Replace("{id}", wageProposal.WageProposalID.ToString()), new StringContent(a, System.Text.Encoding.Unicode, "application/json"))).Result;
                if (response.IsSuccessStatusCode)
                {
                    string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                    return wageProposal = DTO.Factory.WageProposalFactory.Deserialize(data);
                }
                else
                {
                    throw new System.Exception(Core.OptimusMessages.ERROR_MESSAGE);
                }
            }
        }

        #endregion Public Methods

    }
}