﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using System;
using System.Web.UI;

namespace Optimus.Web.WebForms.PermissionsPetition
{
    public partial class PermissionForm : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();

            if (!IsPostBack)
            {
                if (this.Page.User.Identity.IsAuthenticated)
                {
                    var user = worker.User.GetUserByUserName(Page.User.Identity.Name);
                    var Emp = worker.Employees.GetEmployeeById(user.EmployeeID);
                    NombreLabel.Text = Emp.FullName;
                    codigoTextBox.Text = Emp.EmployeeId.ToString();

                    var Perm = container.GetPermissionsEmployeesService().GetListPermissionPendingEmployeesBeEmployeeID(Emp.EmployeeId);
                    PermissionRepeatera.DataSource = Perm;
                    PermissionRepeatera.DataBind();
                    CantidadPermisosPendientes.InnerText = Perm.Count.ToString();

                    var Apro = container.GetPermissionsEmployeesService().GetListPermissionAprovedEmployeesBeEmployeeID(Emp.EmployeeId);
                    AprovadoListView.DataSource = Apro;
                    AprovadoListView.DataBind();
                    CantidadAprobados.InnerText = Apro.Count.ToString();

                }
                RazonDropDownList.DataSource = container.GetPermissionsEmployeesService().GetPermissionReason();
                RazonDropDownList.DataTextField = "PermissionReason";
                RazonDropDownList.DataValueField = "PermissionReasonID";
                RazonDropDownList.DataBind();
            }
        }

        protected void BuscarCodigo(object sender, EventArgs e)
        {
            if (codigoTextBox.Text != string.Empty)
            {
                var Emp = worker.Employees.GetEmployeeById(int.Parse(codigoTextBox.Text));
                NombreLabel.Text = Emp.FullName;
            }
        }

        protected void GuardarPermisos(object sender, EventArgs e)
        {
            PermissionsEmployees PermisosEmp = new PermissionsEmployees();
            PermisosEmp.EmployeeID = int.Parse(codigoTextBox.Text);
            PermisosEmp.DateSubmit = DateTime.Now;
            PermisosEmp.DateEfective = Convert.ToDateTime(FechaTextBox.Text);
            PermisosEmp.HourStart = int.Parse(HoursStartDropdown.SelectedItem.Value);
            PermisosEmp.HourEnd = int.Parse(HoursEndDropdown.SelectedItem.Value);
            PermisosEmp.PermissionReasonID = int.Parse(RazonDropDownList.SelectedItem.Value);
            PermisosEmp.StatusID = 33;
            PermisosEmp.Comment = comentarioTextBox.Text;
            container.GetPermissionsEmployeesService().savePermission(PermisosEmp);

            string permiso= string.Empty;      
            
            permiso += "<p><b>Fecha Solicitud:</b> "+ DateTime.Now+"               <b>Fecha Efectiva:</b>"+FechaTextBox.Text+"<br/>";
            permiso += "<b>Hora Salida:</b> " + HoursStartDropdown.SelectedItem.Text + " <b>hasta</b> " + HoursEndDropdown.SelectedItem.Text + "<br/>";
            permiso += "<b>Motivo:</b><br/>";
            permiso += comentarioTextBox.Text+"</p>";
           // CrearTickets(permiso);

            Response.Redirect(Request.RawUrl);

        }
        private void CrearTickets(string permiso)
        {
            var user = worker.User.GetUserByUserName(Page.User.Identity.Name);
            var Emp = worker.Employees.GetEmployeeById(user.EmployeeID);
            worker.TSTickets.SubmitTicket("Solicitud de permiso: "+Emp.FullName+"("+Emp.EmployeeId+")",permiso, user.userID,25, 2, true);
        }
    }
}