﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.PermissionsPetition
{
    public partial class ChekingEmployees : System.Web.UI.Page
    {

        private ServiceContainer container;
        private UnitOfWork worker;
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
        ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);


        protected void Page_Load(object sender, EventArgs e)
        {



            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            NumeroEmployeTextbox.Focus();
        }
       protected void Buscar(object sender,EventArgs e)
        {
            string num = NumeroEmployeTextbox.Text;
            int ID = int.Parse(num.Substring(0, num.Length - 1));
            var emp = worker.Employees.GetEmployeeById(ID);

            NumeroEmployeTextbox.Text = emp.EmployeeId.ToString();
            NumeroEmployeLabel.Text = emp.EmployeeId.ToString();
            NombreLabel.Text = emp.FullName;
            DepartamentoLabel.Text = worker.dept.Find(x => x.DepartmentId == emp.DepartmentID).FirstOrDefault().Name;
            var pos = ContextSQL.HRPositions.FirstOrDefault(x => x.PositionID == emp.PositionID);
           if (pos != null)
            {
                PosicionLabel.Text = pos.PositionName;

            }else
            {
                PosicionLabel.Text = "N/A";
            }

            if (emp != null)
            {
                if (emp.Picture != null)
                {
                    var img = GUIHelpers.ImagesHelper.byteArrayToImage(emp.Picture);
                    emp.PictureBase64 = "data: image / png; base64," + ImageToBase64(img, ImageFormat.Jpeg);
                }

            }
            ImageCan.Src = emp.PictureBase64;

            var Apro = container.GetPermissionsEmployeesService().GetListPermissionAprovedOnDateEmployeesBeEmployeeID(emp.EmployeeId).FirstOrDefault();

            if (Apro != null)
            {
                HoraPermisoLabel.Text = Apro.HourStart + ":00 Hasta " + Apro.HourEnd + " :00";
                EstadoPermisoLabel.Text = Apro.Status;
            }
         
            //AprovadoListView.DataSource = Apro;
            //AprovadoListView.DataBind();
            //CantidadAprobados.InnerText = Apro.Count.ToString();
            NumeroEmployeTextbox.Text = string.Empty;

            JornadaLabel.Text = container.GetPermissionsEmployeesService().GetJornadaByEmployeeID(emp.EmployeeId);
            HoraExtraLabel.Text = container.GetEmployeesExtraHoursListService().GetJornadaHorasExtrasByEmployeeID(emp.EmployeeId);

        }
    

        public string ImageToBase64(System.Drawing.Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }
    }
}