﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PermissionForm.aspx.cs" Inherits="Optimus.Web.WebForms.PermissionsPetition.PermissionForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .form-control {
            max-width: none;
        }
    </style>
     <style>
        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

            .bs-callout h4 {
                margin-top: 0;
                margin-bottom: 5px;
            }

            .bs-callout p:last-child {
                margin-bottom: 0;
            }

            .bs-callout code {
                border-radius: 3px;
            }

            .bs-callout + .bs-callout {
                margin-top: -5px;
            }

        .bs-callout-default {
            border-left-color: #777;
        }

            .bs-callout-default h4 {
                color: #777;
            }

        .bs-callout-primary {
            border-left-color: #428bca;
        }

            .bs-callout-primary h4 {
                color: #428bca;
            }

        .bs-callout-success {
            border-left-color: #5cb85c;
        }

            .bs-callout-success h4 {
                color: #5cb85c;
            }

        .bs-callout-danger {
            border-left-color: #d9534f;
        }

            .bs-callout-danger h4 {
                color: #d9534f;
            }

        .bs-callout-warning {
            border-left-color: #f0ad4e;
        }
         .bs-callout-warning h4 {
                color: #f0ad4e;
            }

        .bs-callout-info {
            border-left-color: #5bc0de;
        }

            .bs-callout-info h4 {
                color: #5bc0de;
            }
    </style>
    <div id="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Solicitud de permisos</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" Font-Bold="true" Text="Código:" ID="Label2"></asp:Label>
                        <asp:TextBox runat="server" ID="codigoTextBox" CssClass="form-control" OnTextChanged="BuscarCodigo" AutoPostBack="true"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="codigoTextBox" ForeColor="red" ErrorMessage="*Ingrese un codigo"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" Font-Bold="true" Text="Nombre:" ID="Label1"></asp:Label>
                        <asp:Label runat="server" Text="Nombre:" CssClass="form-control" ID="NombreLabel"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <asp:Label runat="server" Font-Bold="true" Text="Fecha Efectiva:" ID="fechaLabel"></asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control datepicker" ID="FechaTextBox" ClientIDMode="static"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="FechaTextBox" ForeColor="red" ErrorMessage="*Ingrese una fecha"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <asp:Label runat="server" Font-Bold="true" Text="Rango hora:"></asp:Label>
                            <asp:DropDownList runat="server" ID="HoursStartDropdown" CssClass="form-control">
                               
                                <asp:ListItem Value="6">6:00</asp:ListItem>
                                <asp:ListItem Value="7">7:00</asp:ListItem>
                                <asp:ListItem Value="8">8:00</asp:ListItem>
                                <asp:ListItem Value="9">9:00</asp:ListItem>
                                <asp:ListItem Value="10">10:00</asp:ListItem>
                                <asp:ListItem Value="11">11:00</asp:ListItem>
                                <asp:ListItem Value="12">12:00</asp:ListItem>
                                <asp:ListItem Value="13">13:00</asp:ListItem>
                                <asp:ListItem Value="14">14:00</asp:ListItem>
                                <asp:ListItem Value="15">15:00</asp:ListItem>
                                <asp:ListItem Value="16">16:00</asp:ListItem>
                                <asp:ListItem Value="17">17:00</asp:ListItem>
                                <asp:ListItem Value="18">18:00</asp:ListItem>
                                <asp:ListItem Value="19">19:00</asp:ListItem>
                                <asp:ListItem Value="20">20:00</asp:ListItem>
                                <asp:ListItem Value="21">21:00</asp:ListItem>
                                <asp:ListItem Value="22">22:00</asp:ListItem>
                                
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <span style="font-weight: bold">hasta:</span>
                        <asp:DropDownList runat="server" ID="HoursEndDropdown" CssClass="form-control">
                    
                            <asp:ListItem Value="6">6:00</asp:ListItem>
                            <asp:ListItem Value="7">7:00</asp:ListItem>
                            <asp:ListItem Value="8">8:00</asp:ListItem>
                            <asp:ListItem Value="9">9:00</asp:ListItem>
                            <asp:ListItem Value="10">10:00</asp:ListItem>
                            <asp:ListItem Value="11">11:00</asp:ListItem>
                            <asp:ListItem Value="12">12:00</asp:ListItem>
                            <asp:ListItem Value="13">13:00</asp:ListItem>
                            <asp:ListItem Value="14">14:00</asp:ListItem>
                            <asp:ListItem Value="15">15:00</asp:ListItem>
                            <asp:ListItem Value="16">16:00</asp:ListItem>
                            <asp:ListItem Value="17">17:00</asp:ListItem>
                            <asp:ListItem Value="18">18:00</asp:ListItem>
                            <asp:ListItem Value="19">19:00</asp:ListItem>
                            <asp:ListItem Value="20">20:00</asp:ListItem>
                            <asp:ListItem Value="21">21:00</asp:ListItem>
                            <asp:ListItem Value="22">22:00</asp:ListItem>
                       
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" Text="Razón" Font-Bold="true"></asp:Label>
                        <asp:DropDownList runat="server" ID="RazonDropDownList" CssClass="form-control"></asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label runat="server" Text="Comentarios" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="comentarioTextBox" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="padding-top:10px">
                        <asp:Button runat="server" ID="EnviarButton" CssClass="btn btn-primary" Text="Enviar Listado" OnClick="GuardarPermisos" />
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Listado de permisos</h3>
            </div>
            <div class="panel-body">
           
                <br />
                <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Listado de permisos sin aprobar. <span id="CantidadPermisosPendientes" runat="server" class="badge">0</span></a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                         
                            <asp:ListView runat="server" ID="PermissionRepeatera" >
                                <ItemTemplate>
                                    <div class="bs-callout bs-callout-default">
                                        <h4><%#Eval("FullName")%></h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Fecha Solicitud:</b><%#Eval("DateEfective")%>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Estado:</b><asp:Label runat="server" ID="StatusLabel" Text='<%#Eval("Status")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Desde:</b> <%#Eval("HourStart")%>:00 <b>hasta</b> <%#Eval("HourEnd")%>:00
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Razón:</b> <%#Eval("PermissionReason")%>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Motivo</label>
                                                <div>
                                                    <p>
                                                        <%#Eval("Comment")%>
                                                    <p />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
        
                                                <a  onClick="window.open(this.href, this.target, 'width=900,height=500');return false;"; target="popup" 
                                                 href='<%# string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>
                                                    <%--href='<%# string.Format("http://drl-it-testing//ReportServer/Pages/ReportViewer.aspx?/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>--%>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                    </div>
                 <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Listado de permisos aprobados. <span id="CantidadAprobados" runat="server" class="badge">0</span></a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">                       
                            <asp:ListView runat="server" ID="AprovadoListView" >
                                <ItemTemplate>
                                    <div class="bs-callout bs-callout-success">
                                        <h4 style="color:black"><%#Eval("FullName")%></h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Fecha Solicitud:</b><%#Eval("DateEfective")%>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Estado:</b><asp:Label runat="server" ID="StatusLabel" Text='<%#Eval("Status")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Desde:</b> <%#Eval("HourStart")%>:00 <b>hasta</b> <%#Eval("HourEnd")%>:00
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Razón:</b> <%#Eval("PermissionReason")%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Motivo</label>
                                                <div>
                                                    <p>
                                                        <%#Eval("Comment")%>
                                                    <p />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                  <a  onClick="window.open(this.href, this.target, 'width=900,height=500');return false;"; target="popup" 
                                                 href='<%# string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>
                                                   <%-- href='<%# string.Format("http://drl-it-testing//ReportServer/Pages/ReportViewer.aspx?/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>--%>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                    </div>
            </div>
        </div>
        
    </div>
</asp:Content>
