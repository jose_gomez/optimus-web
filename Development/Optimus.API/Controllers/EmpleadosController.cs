﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones para manejar los empleados en la empresa.
    /// </summary>
    [RoutePrefix("Optimus/Empleados")]
    public class EmpleadosController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.Employee _EmployeeRepo = new Web.BC.Repositories.API.Employee();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        ///
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete/{id}")]
        [ResponseType(typeof(List<DTO.Empleado>))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                _EmployeeRepo.Delete(id);
                return Ok();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(Core.OptimusMessages.ERROR_MESSAGE);
            }
        }

        /// <summary>
        /// Obtiene todo los empleados en el sistema.
        /// </summary>
        /// <returns>Listado de empleados.</returns>
        [HttpGet]
        [Route("Get")]
        [ResponseType(typeof(List<DTO.Empleado>))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Get()
        {
            return NotFound();
        }

        /// <summary>
        /// Obtiene la información de un empleado por su numero.
        /// </summary>
        /// <param name="id">Numero del empleado.</param>
        /// <returns>Empleado.</returns>
        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(DTO.Empleado))]
        public IHttpActionResult Get(int id)
        {
            var employee = _EmployeeRepo.GetEmployeeByID(id);
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        /// <summary>
        /// Obtiene las acciones hechas a un empleado.
        /// </summary>
        /// <param name="id">Numero de empleado.</param>
        /// <returns>Lista de acciones.</returns>
        [HttpGet]
        [Route("Get/{id}/AccionesEmpleado")]
        [ResponseType(typeof(List<DTO.AccionEmpleado>))]
        public IHttpActionResult GetAccionesEmpleados(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var result = _EmployeeRepo.GetEmployeeActions(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene los empleados activos en una posición especifica.
        /// </summary>
        /// <param name="id">Identificador de la posición.</param>
        /// <returns>Listado de empleados.</returns>
        [HttpGet]
        [Route("Get/Posicion/{id}")]
        [ResponseType(typeof(List<DTO.Empleado>))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult GetByPositionID(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var employee = _EmployeeRepo.GetEmployeesByPosition(id);
                if (employee == null)
                {
                    return NotFound();
                }
                return Ok(employee);
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene la posición a la que pertenece un empleado.
        /// </summary>
        /// <param name="id">Numero de empleado.</param>
        /// <returns>Posición</returns>
        [HttpGet]
        [Route("Get/{id}/Posicion")]
        [ResponseType(typeof(DTO.Posicion))]
        public IHttpActionResult GetEmployeePosition(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var result = _EmployeeRepo.GetEmployeePosition(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }
        /// <summary>
        /// Crea un nuevo empleado.
        /// </summary>
        /// <param name="empleado">Nuevo empleado.</param>
        /// <returns>Empleado creado.</returns>
        [HttpPost]
        [Route("Post")]
        [ResponseType(typeof(List<DTO.Empleado>))]
        public IHttpActionResult Post([FromBody]DTO.Empleado empleado)
        {
            try
            {
                if (empleado == null)
                {
                    return BadRequest();
                }
                _EmployeeRepo.Save(empleado);
                return Ok();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Actualiza datos de un empleado.
        /// </summary>
        /// <param name="id">Identificador de un empleado.</param>
        /// <param name="employee">Valores para actualizar.</param>
        /// <returns>Empleado actualizado.</returns>
        [HttpPut]
        [Route("Put/{id}")]
        [ResponseType(typeof(List<DTO.Empleado>))]
        public IHttpActionResult Put(int id, [FromBody]DTO.Empleado employee)
        {
            try
            {
                if (employee == null)
                {
                    return BadRequest();
                }
                var result = _EmployeeRepo.GetEmployeeByID(employee.EmployeeID);
                if (result == null)
                {
                    return NotFound();
                }
                _EmployeeRepo.Save(employee);
                return Ok();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods

    }
}