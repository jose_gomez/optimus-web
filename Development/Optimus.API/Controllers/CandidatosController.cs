﻿using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones correspondientes a los candidatos que aplican para un puesto en la empresa.
    /// </summary>
    [RoutePrefix("Optimus/Candidatos")]
    public class CandidatosController : ApiController
    {
        #region Private Fields

        private Web.BC.Repositories.API.Candidate _CandidaeRepo = new Web.BC.Repositories.API.Candidate();

        #endregion Private Fields

        #region Public Methods

        [HttpDelete]
        [Route("Delete/{id}")]
        [ResponseType(typeof(DTO.Candidate))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene todos los candidatos del sistema.
        /// </summary>
        /// <returns>Lista de candidatos.</returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Get()
        {
            try
            {
                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones de un candidato en especifico.
        /// </summary>
        /// <param name="id">Identificador del candidato.</param>
        /// <returns>Un candidato.</returns>
        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(DTO.Candidate))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var candidate = _CandidaeRepo.GetCandidateByID(id);
                if (candidate == null)
                {
                    return NotFound();
                }
                return Ok(candidate);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="candidate"></param>
        /// <returns></returns>
        [HttpPost]
        [Route("Post")]
        [ResponseType(typeof(DTO.Candidate))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Post([FromBody]DTO.Candidate candidate)
        {
            try
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        [Route("Put/{id}")]
        [ResponseType(typeof(DTO.Candidate))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Put(int id, [FromBody]DTO.Candidate value)
        {
            try
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods
    }
}