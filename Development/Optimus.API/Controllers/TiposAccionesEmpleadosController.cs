﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Tipos de acciones de empleados.
    /// </summary>
    [RoutePrefix("Optimus/TiposAccionesEmpleados")]
    public class TiposAccionesEmpleadosController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.TipoAccionEmpleado _TipoAccionRepo = new Web.BC.Repositories.API.TipoAccionEmpleado();
        private Web.BC.Repositories.API.AccionEmpleado _AccionRepo = new Web.BC.Repositories.API.AccionEmpleado();
        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Elimina un tipo de acción de empleado.
        /// </summary>
        /// <param name="id">Identificador de la acción.</param>
        /// <returns>Resultado.</returns>
        [HttpDelete]
        [Route("Delete/{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var result = _TipoAccionRepo.Get(id);
                if (result == null)
                {
                    return NotFound();
                }
                _TipoAccionRepo.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene todos los tipos de acciones de empleados.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        [ResponseType(typeof(List<DTO.TipoAccionEmpleado>))]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _TipoAccionRepo.Get();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene un tipo de acción de empleado.
        /// </summary>
        /// <param name="id">Identificador</param>
        /// <returns></returns>
        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(DTO.TipoAccionEmpleado))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _TipoAccionRepo.Get(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Crea una nuevo tipo de acción de empleado.
        /// </summary>
        /// <param name="value">Nuevo tipo de acción.</param>
        /// <returns>Tipo de acción creada.</returns>
        [HttpPost]
        [Route("Post")]
        [ResponseType(typeof(DTO.TipoAccionEmpleado))]
        public IHttpActionResult Post([FromBody]DTO.TipoAccionEmpleado value)
        {
            try
            {
                if (value == null)
                {
                    return NotFound();
                }
                var result = _TipoAccionRepo.Create(value);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Actualiza un tipo de acción de empleado.
        /// </summary>
        /// <param name="id">Identificador de tipo de acción.</param>
        /// <param name="value">Tipo de acción de empleado para actualizar.</param>
        /// <returns>Tipo de acción de empleado actualizada.</returns>
        [HttpPut]
        [Route("Put/{id}")]
        [ResponseType(typeof(DTO.TipoAccionEmpleado))]
        public IHttpActionResult Put(int id, [FromBody]DTO.TipoAccionEmpleado value)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                if (value == null)
                {
                    return BadRequest();
                }
                var result = _TipoAccionRepo.Get(id);
                if (result == null)
                {
                    NotFound();
                }
                result = _TipoAccionRepo.Update(value);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods

    }
}