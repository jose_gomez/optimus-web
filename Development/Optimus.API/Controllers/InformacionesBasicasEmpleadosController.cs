﻿using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones obtener las informaciones básicas de un empleado.
    /// </summary>
    [RoutePrefix("Optimus/InformacionesBasicasEmpleados")]
    public class InformacionesBasicasEmpleadosController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.InformacionBasicaEmpleado _Repo = new Web.BC.Repositories.API.InformacionBasicaEmpleado();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Obtiene las informaciones básicas de un empleado.
        /// </summary>
        /// <param name="id">Numero de empleado.</param>
        /// <returns>Información básica del empleado.</returns>
        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _Repo.Get(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados activos.
        /// </summary>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Activos")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetActivos()
        {
            try
            {
                var result = _Repo.GetActivos();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados activos.
        /// </summary>
        /// <param name="filtro">Filtro.</param>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Activos/{filtro}")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetActivos(string filtro)
        {
            try
            {
                var result = _Repo.GetActivos(filtro);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados inactivos.
        /// </summary>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Inactivos")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetInactivos()
        {
            try
            {
                var result = _Repo.GetInactivos();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados inactivos.
        /// </summary>
        /// <param name="filtro">Filtro.</param>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Inactivos/{filtro}")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetInactivos(string filtro)
        {
            try
            {
                var result = _Repo.GetInactivos(filtro);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados liquidación.
        /// </summary>
        /// <param name="filtro">Filtro.</param>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Liquidados/{filtro}")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetLiquidados(string filtro)
        {
            try
            {
                var result = _Repo.GetLiquidados(filtro);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados liquidados.
        /// </summary>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Liquidados")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetLiquidados()
        {
            try
            {
                var result = _Repo.GetLiquidados();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados pendientes.
        /// </summary>
        /// <param name="filtro">Filtro.</param>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Pendientes/{filtro}")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetPendientes(string filtro)
        {
            try
            {
                var result = _Repo.GetPendientes(filtro);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene las informaciones básicas de los empleados pendientes.
        /// </summary>
        /// <returns>Informaciones básicas de los empleados.</returns>
        [HttpGet]
        [Route("Get/Pendientes")]
        [ResponseType(typeof(DTO.InformacionBasicaEmpleado))]
        public IHttpActionResult GetPendientes()
        {
            try
            {
                var result = _Repo.GetPendientes();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods

    }
}