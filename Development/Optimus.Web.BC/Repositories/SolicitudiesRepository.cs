﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Drawing;

namespace Optimus.Web.BC.Repositories
{
    internal class SolicitudiesRepository : ISolicitudes
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
       ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public int Save(Candidates candidates)
        {
            HRCandidate candidate = (from p in ContextSQL.HRCandidates
                                     where p.Cedula == candidates.Cedula
                                     select p
                                     ).FirstOrDefault();
            if (candidate == null)
            {
                candidate = new HRCandidate();
                candidate.PostulatePositionID = candidates.PostulatePositionID;
                candidate.FirstName = candidates.FirstName;
                candidate.LastName = candidates.LastName;
                candidate.Streets = candidates.Streets;
                candidate.HouseNumber = candidates.HouseNumber;
                candidate.Sector = candidates.Sector;
                candidate.location = candidates.location;
                candidate.Sex = Convert.ToChar(candidates.Sex);
                candidate.NationalityID = candidates.NationalityID;
                candidate.PhoneNumber = candidates.PhoneNumber;
                candidate.MovilNumber = candidates.MovilNumber;
                candidate.Email = candidates.Email;
                candidate.Age = candidates.Age;
                candidate.Cedula = candidates.Cedula;
                candidate.BirthDate = candidates.BirthDate;
                candidate.Alias = candidate.Alias;
                candidate.EmergencyNumber = candidates.EmergencyNumber;
                candidate.EmergencyContactName = candidates.EmergencyContactName;
                candidate.Size = candidates.Size;
                candidate.Religion = candidates.Religion;
                candidate.CivilStatus = candidates.CivilStatus;
                candidate.allergies = candidates.Allergies;
                candidate.AllergiesType = candidates.AllergiesType;
                candidate.suffering = candidates.suffering;
                candidate.sufferingType = candidates.sufferingType;
                candidate.AvailableOvertime = candidates.AvailableOverTime;
                candidate.AvailableWeekend = candidates.AvailableWeekend;
                candidate.LivesWith = candidates.LivesWith;
                candidate.SpouseName = candidates.SpouseName;
                candidate.SpouseActivity = candidates.SpouseActivity;
                candidate.MotherName = candidates.MotherName;
                candidate.FatherName = candidates.FatherName;
                candidate.DependentChildren = candidates.DependentChildren;
                candidate.DependentPeople = candidates.DependentPeople;
                candidate.StudiesID = candidates.StudiesID;
                candidate.ComputerStudies = candidates.ComputerStudies;
                candidate.IsStudent = candidates.IsStudent;
                candidate.DaysWeek = candidates.DaysWeek;
                candidate.Schedule = candidates.Schedule;
                candidate.Programsknow = candidates.ProgramsKnow;
                candidate.WhosRecommended = candidates.WhosRecommended;
                candidate.FamilyOrFriend = candidates.FamilyOrFriend;
                candidate.FamilyOrFriendName = candidates.FamilyOrFriendName;
                candidate.FamilyOrFriendPuesto = candidates.FamilyOrFriendPuesto;
                candidate.FamilyOrFriendRelationship = candidates.FamilyOrFriendRelationship;
                candidate.PositionCategoryID = candidates.PositionCategoryID;
                candidate.interviewed = candidates.Interviewed;
                candidate.CandidateStateID = candidates.CandidateStateID;
                candidate.create_dt = DateTime.Now;

                ContextSQL.HRCandidates.InsertOnSubmit(candidate);
                ContextSQL.SubmitChanges();
            }
            else
            {
                candidate.PostulatePositionID = candidates.PostulatePositionID;
                candidate.FirstName = candidates.FirstName;
                candidate.LastName = candidates.LastName;
                candidate.Streets = candidates.Streets;
                candidate.HouseNumber = candidates.HouseNumber;
                candidate.Sector = candidates.Sector;
                candidate.location = candidates.location;
                candidate.Sex = Convert.ToChar(candidates.Sex);
                candidate.NationalityID = candidates.NationalityID;
                candidate.PhoneNumber = candidates.PhoneNumber;
                candidate.MovilNumber = candidates.MovilNumber;
                candidate.Email = candidates.Email;
                candidate.Age = candidates.Age;
                candidate.Cedula = candidates.Cedula;
                candidate.BirthDate = candidates.BirthDate;
                candidate.Alias = candidate.Alias;
                candidate.EmergencyNumber = candidates.EmergencyNumber;
                candidate.EmergencyContactName = candidates.EmergencyContactName;
                candidate.Size = candidates.Size;
                candidate.Religion = candidates.Religion;
                candidate.CivilStatus = candidates.CivilStatus;
                candidate.allergies = candidates.Allergies;
                candidate.AllergiesType = candidates.AllergiesType;
                candidate.suffering = candidates.suffering;
                candidate.sufferingType = candidates.sufferingType;
                candidate.AvailableOvertime = candidates.AvailableOverTime;
                candidate.AvailableWeekend = candidates.AvailableWeekend;
                candidate.LivesWith = candidates.LivesWith;
                candidate.SpouseName = candidates.SpouseName;
                candidate.SpouseActivity = candidates.SpouseActivity;
                candidate.MotherName = candidates.MotherName;
                candidate.FatherName = candidates.FatherName;
                candidate.DependentChildren = candidates.DependentChildren;
                candidate.DependentPeople = candidates.DependentPeople;
                candidate.StudiesID = candidates.StudiesID;
                candidate.ComputerStudies = candidates.ComputerStudies;
                candidate.IsStudent = candidates.IsStudent;
                candidate.DaysWeek = candidates.DaysWeek;
                candidate.Schedule = candidates.Schedule;
                candidate.Programsknow = candidates.ProgramsKnow;
                candidate.WhosRecommended = candidates.WhosRecommended;
                candidate.FamilyOrFriend = candidates.FamilyOrFriend;
                candidate.FamilyOrFriendName = candidates.FamilyOrFriendName;
                candidate.FamilyOrFriendPuesto = candidates.FamilyOrFriendPuesto;
                candidate.FamilyOrFriendRelationship = candidates.FamilyOrFriendRelationship;
                candidate.PositionCategoryID = candidates.PositionCategoryID;
                candidate.interviewed = candidates.Interviewed;
                candidate.CandidateStateID = candidates.CandidateStateID;
                candidate.interviewed = candidates.Interviewed;
                candidate.CandidateStateID = candidates.CandidateStateID;
                ContextSQL.SubmitChanges();

                return candidate.CandidateID;
            }

            return candidate.CandidateID;
        }

        public void Save(List<CandidateBrothers> CanBro)
        {
            HRCandidateBrother CandidateBrother;
            foreach (CandidateBrothers item in CanBro)
            {
                CandidateBrother = new HRCandidateBrother();
                CandidateBrother.CandidateID = item.CandidateID;
                CandidateBrother.Names = item.Names;
                ContextSQL.HRCandidateBrothers.InsertOnSubmit(CandidateBrother);
                ContextSQL.SubmitChanges();
            }
        }

        public void Save(List<CandidateExperience> Exp)
        {
            HRCandidateExperience CandExp;
            foreach (CandidateExperience item in Exp)
            {
                CandExp = new HRCandidateExperience();
                CandExp.CandidateID = item.CandidateID;
                CandExp.Company = item.Company;
                CandExp.WorkPosition = item.WorkPosition;
                CandExp.PerformedTask = item.PerfomedTask;
                CandExp.DateFrom = item.DateFrom;
                CandExp.DateTo = item.DateTo;
                CandExp.Salary = item.Salary;
                CandExp.ReasonOut = item.ReasonOut;
                CandExp.PhoneNumberCompany = item.PhoneNumberCompany;
                ContextSQL.HRCandidateExperiences.InsertOnSubmit(CandExp);
                ContextSQL.SubmitChanges();
            }
        }

        public void Save(List<CandidateTitle> Title)
        {
            HRCandidateTitle Titles;
            foreach (CandidateTitle item in Title)
            {
                Titles = new HRCandidateTitle();
                Titles.CandidateID = item.CandidateID;
                Titles.Titles = item.Titles;
                ContextSQL.HRCandidateTitles.InsertOnSubmit(Titles);
                ContextSQL.SubmitChanges();
            }
        }

        public void Save(List<CandidateLanguages> CandLang)
        {
            HRCandidateLanguage CandidatosLenguajes;
            foreach (CandidateLanguages item in CandLang)
            {
                CandidatosLenguajes = new HRCandidateLanguage();
                CandidatosLenguajes.CandidateID = item.CandidateID;
                CandidatosLenguajes.LanguageID = item.LanguagesID;
                CandidatosLenguajes.LevellanguageID = item.LevelID;
                ContextSQL.HRCandidateLanguages.InsertOnSubmit(CandidatosLenguajes);
                ContextSQL.SubmitChanges();
            }
        }

        public void Save(List<CandidateSkills> CandSkills)
        {
            HRCandidateSkill CandidatosSkill;
            foreach (CandidateSkills item in CandSkills)
            {
                CandidatosSkill = new HRCandidateSkill();
                CandidatosSkill.CandidateID = item.CandidateID;
                CandidatosSkill.SkillID = item.SkillID;
                ContextSQL.HRCandidateSkills.InsertOnSubmit(CandidatosSkill);
                ContextSQL.SubmitChanges();
            }
        }

        public void Save(List<CandidateReferences> CandReferences)
        {
            HRCandidateReference CandidatoReferencias;

            foreach (CandidateReferences item in CandReferences)
            {
                CandidatoReferencias = new HRCandidateReference();
                CandidatoReferencias.CandidateID = item.CandidateID;
                CandidatoReferencias.Name = item.Name;
                CandidatoReferencias.PhoneNumber = item.PhoneNumber;
                CandidatoReferencias.Profession = item.Profession;
                ContextSQL.HRCandidateReferences.InsertOnSubmit(CandidatoReferencias);
                ContextSQL.SubmitChanges();
            }
        }

        public void SavePicture(CandidatePictures Pics)
        {
            HRCandidatePicture Picture = (from p in ContextSQL.HRCandidatePictures
                                          where p.CandidateID == Pics.CandidateID
                                          select p
                                          ).FirstOrDefault();
            if (Picture == null)
            {
                Picture = new HRCandidatePicture();
                Picture.CandidateID = Pics.CandidateID;
                Picture.CandidatePicture = Pics.PictureCadidate;
                Picture.CandidateCedulaFrontPicture = Pics.PictureCadidateCedulaFrontPicture;
                Picture.CandidateCedulaBackPicture = Pics.PictureCadidateCedulaBackPicture;
                ContextSQL.HRCandidatePictures.InsertOnSubmit(Picture);
                ContextSQL.SubmitChanges();
            }
            else
            {
                Picture.CandidateID = Pics.CandidateID;                
                if (Pics.PictureCadidate.Length > 3)
                {
                    Picture.CandidatePicture = Pics.PictureCadidate;
                }
                if (Pics.PictureCadidateCedulaFrontPicture.Length > 3)
                {
                    Picture.CandidateCedulaFrontPicture = Pics.PictureCadidateCedulaFrontPicture;
                }
                if (Pics.PictureCadidateCedulaBackPicture != null)
                {
                    Picture.CandidateCedulaBackPicture = Pics.PictureCadidateCedulaBackPicture;
                }       
                ContextSQL.SubmitChanges();
            }
           
            
        }
        public void Save(CandidateGUIDCurriculum GuidCurriculum)
        {
            HRCandidateGUIDCurriculum HGC = new HRCandidateGUIDCurriculum();
            HGC.Cedula = GuidCurriculum.Cedula;
            HGC.Guid =   GuidCurriculum.guid;
            HGC.Create_dt = GuidCurriculum.Create_dt;
            ContextSQL.HRCandidateGUIDCurriculums.InsertOnSubmit(HGC);
            ContextSQL.SubmitChanges();

        }
        public CandidatePictures GetPictureCandidate(string cedula)
        {
            Byte[] array;
        CandidatePictures candPic = (from p in ContextSQL.HRCandidatePictures
                           join h in ContextSQL.HRCandidates on p.CandidateID equals h.CandidateID
                           where h.Cedula == cedula
                           select new CandidatePictures
                           {
                               PictureCadidate = p.CandidatePicture.ToArray()
                           }).FirstOrDefault();
            if (candPic == null)
            {
                array =  new Byte[0];
                candPic = new CandidatePictures();
                candPic.PictureCadidate = array;
                return candPic;
            }else
            {
                return candPic;
            }

                 
        }
        public CandidateGUIDCurriculum GetCandidateGUIDCurriculum(string cedula)
        {
            var candCvs = (from p in ContextSQL.HRCandidateGUIDCurriculums
                           where p.Cedula == cedula
                           select new CandidateGUIDCurriculum
                           {

                      Cedula = p.Cedula,
                                guid = p.Guid
                               
                           }).FirstOrDefault(); 
            return candCvs;
            
        }
        public List<Candidates> GetCandidatesAll()
        {
            var query = (from p in ContextSQL.HRCandidates
                         where p.CandidateStateID != 5
                        
                         select new Candidates
                         {
                            CandidateID = p.CandidateID,
                             PostulatePositionID = (int)p.PostulatePositionID,
                             FirstName = p.FirstName,
                             LastName = p.LastName,
                             Streets = p.Streets,
                             HouseNumber = p.HouseNumber,
                             Sector = p.Sector,
                             location = p.location,
                             Sex = Convert.ToString(p.Sex),
                             NationalityID = p.NationalityID,
                             PhoneNumber = p.PhoneNumber,
                             MovilNumber = p.MovilNumber,
                             Email = p.Email,
                             Age = (int)p.Age,
                             Cedula = p.Cedula,
                             BirthDate = p.BirthDate,
                             alias = p.Alias,
                             EmergencyNumber = p.EmergencyNumber,
                             EmergencyContactName = p.EmergencyContactName,
                             Size = p.Size,
                             Religion = p.Religion,
                             CivilStatus = p.CivilStatus,
                             Allergies = (bool)p.allergies,
                             AllergiesType = p.AllergiesType,
                             suffering = (bool)p.suffering,
                             sufferingType = p.sufferingType,
                             AvailableOverTime = (bool)p.AvailableOvertime,
                             AvailableWeekend = (bool)p.AvailableWeekend,
                             LivesWith = p.LivesWith,
                             SpouseName = p.SpouseName,
                             SpouseActivity = p.SpouseActivity,
                             MotherName = p.MotherName,
                             FatherName = p.FatherName,
                             DependentChildren = (int)p.DependentChildren,
                             DependentPeople = (int)p.DependentPeople,
                             StudiesID = (int)p.StudiesID,
                             ComputerStudies = (bool)p.ComputerStudies,
                             IsStudent = p.IsStudent,
                             DaysWeek = p.DaysWeek,
                             Schedule = p.Schedule,
                             ProgramsKnow = p.Programsknow,
                             WhosRecommended = p.WhosRecommended,
                             FamilyOrFriend = (bool)p.FamilyOrFriend,
                             FamilyOrFriendName = p.FamilyOrFriendName,
                             FamilyOrFriendPuesto = p.FamilyOrFriendPuesto,
                             FamilyOrFriendRelationship = p.FamilyOrFriendRelationship,
                             PositionCategoryID = (int)p.PositionCategoryID,
                             Picture64Bit = GetPictureCandidate(p.Cedula).PictureCadidate,
                             Interviewed = (bool)p.interviewed,
                             CandidateStateID = (int)p.CandidateStateID,


                         }).ToList();
            return query;
        }

        public List<Candidates> GetCandidates(string cedula,string sexo,int posicion,int area,int studio)
        {
            var query = (from p in ContextSQL.HRCandidates
                         where (p.Cedula == cedula || cedula == "-1") && (p.Sex == char.Parse(sexo) || sexo == "0") && (p.PostulatePositionID == posicion || posicion == -1) &&
                               (p.PositionCategoryID == area || area == -1) && (p.StudiesID == studio || studio == -1) && p.CandidateStateID != 5
                         select new Candidates
                         {
                            CandidateID = p.CandidateID,
                             PostulatePositionID = (int)p.PostulatePositionID,
                             FirstName = p.FirstName,
                             LastName = p.LastName,
                             Streets = p.Streets,
                             HouseNumber = p.HouseNumber,
                             Sector = p.Sector,
                             location = p.location,
                             Sex = Convert.ToString(p.Sex),
                             NationalityID = p.NationalityID,
                             PhoneNumber = p.PhoneNumber,
                             MovilNumber = p.MovilNumber,
                             Email = p.Email,
                             Age = (int)p.Age,
                             Cedula = p.Cedula,
                             BirthDate = p.BirthDate,
                             alias = p.Alias,
                             EmergencyNumber = p.EmergencyNumber,
                             EmergencyContactName = p.EmergencyContactName,
                             Size = p.Size,
                             Religion = p.Religion,
                             CivilStatus = p.CivilStatus,
                             Allergies = (bool)p.allergies,
                             AllergiesType = p.AllergiesType,
                             suffering = (bool)p.suffering,
                             sufferingType = p.sufferingType,
                             AvailableOverTime = (bool)p.AvailableOvertime,
                             AvailableWeekend = (bool)p.AvailableWeekend,
                             LivesWith = p.LivesWith,
                             SpouseName = p.SpouseName,
                             SpouseActivity = p.SpouseActivity,
                             MotherName = p.MotherName,
                             FatherName = p.FatherName,
                             DependentChildren = (int)p.DependentChildren,
                             DependentPeople = (int)p.DependentPeople,
                             StudiesID = (int)p.StudiesID,
                             ComputerStudies = (bool)p.ComputerStudies,
                             IsStudent = p.IsStudent,
                             DaysWeek = p.DaysWeek,
                             Schedule = p.Schedule,
                             ProgramsKnow = p.Programsknow,
                             WhosRecommended = p.WhosRecommended,
                             FamilyOrFriend = (bool)p.FamilyOrFriend,
                             FamilyOrFriendName = p.FamilyOrFriendName,
                             FamilyOrFriendPuesto = p.FamilyOrFriendPuesto,
                             FamilyOrFriendRelationship = p.FamilyOrFriendRelationship,
                             PositionCategoryID = (int)p.PositionCategoryID,
                             Picture64Bit = GetPictureCandidate(p.Cedula).PictureCadidate,
                             Interviewed = (bool)p.interviewed,
                             CandidateStateID = (int)p.CandidateStateID     
                         }).ToList();
            return query;
        }
        public Candidates GetCandidates(string cedula)
        {
            var candidate = (from p in ContextSQL.HRCandidates
                             where p.Cedula == cedula
                             select new Candidates
                             {
                                 CandidateID = p.CandidateID,
                                 PostulatePositionID = (int)p.PostulatePositionID,
                                 FirstName = p.FirstName,
                                 LastName = p.LastName,
                                 Streets = p.Streets,
                                 HouseNumber = p.HouseNumber,
                                 Sector = p.Sector,
                                 location = p.location,
                                 Sex = Convert.ToString(p.Sex),
                                 NationalityID = p.NationalityID,
                                 PhoneNumber = p.PhoneNumber,
                                 MovilNumber = p.MovilNumber,
                                 Email = p.Email,
                                 Age = (int)p.Age,
                                 Cedula = p.Cedula,
                                 BirthDate = p.BirthDate,
                                 alias = p.Alias,
                                 EmergencyNumber = p.EmergencyNumber,
                                 EmergencyContactName = p.EmergencyContactName,
                                 Size = p.Size,
                                 Religion = p.Religion,
                                 CivilStatus = p.CivilStatus,
                                 Allergies = (bool)p.allergies,
                                 AllergiesType = p.AllergiesType,
                                 suffering = (bool)p.suffering,
                                 sufferingType = p.sufferingType,
                                 AvailableOverTime = (bool)p.AvailableOvertime,
                                 AvailableWeekend = (bool)p.AvailableWeekend,
                                 LivesWith = p.LivesWith,
                                 SpouseName = p.SpouseName,
                                 SpouseActivity = p.SpouseActivity,
                                 MotherName = p.MotherName,
                                 FatherName = p.FatherName,
                                 DependentChildren = (int)p.DependentChildren,
                                 DependentPeople = (int)p.DependentPeople,
                                 StudiesID = (int)p.StudiesID,
                                 ComputerStudies = (bool)p.ComputerStudies,
                                 IsStudent = p.IsStudent,
                                 DaysWeek = p.DaysWeek,
                                 Schedule = p.Schedule,
                                 ProgramsKnow = p.Programsknow,
                                 WhosRecommended = p.WhosRecommended,
                                 FamilyOrFriend = (bool)p.FamilyOrFriend,
                                 FamilyOrFriendName = p.FamilyOrFriendName,
                                 FamilyOrFriendPuesto = p.FamilyOrFriendPuesto,
                                 FamilyOrFriendRelationship = p.FamilyOrFriendRelationship,
                                 PositionCategoryID = (int)p.PositionCategoryID,
                                 Interviewed = (bool)p.interviewed,
                                 CandidateStateID = (int)p.CandidateStateID
                             }).FirstOrDefault();

            return candidate;
        }

        public List<CandidateState> GetCandidateStates()
        {
            var query = (from p in ContextSQL.HRcandidateStates
                         select new CandidateState
                         {
                             CandidateStateID = p.CandidateStateID,
                             State = p.State
                         }
                         ).ToList();
            return query;       

        }
        public CandidateState GetCandidateStatesByID(int ID)
        {
            var query = (from p in ContextSQL.HRcandidateStates
                         where p.CandidateStateID == ID
                         select new CandidateState
                         {
                             CandidateStateID = p.CandidateStateID,
                             State = p.State
                         }
                         ).FirstOrDefault();
            return query;

        }
        public void ExportCandidate(int candidateID,int deparment)
        {
            ContextSQL.SPExportToPayrollEmployee(candidateID, deparment);

        }

        public List<HRPositionCategory> GetPositionCategory()
        {
            var query = (from p in ContextSQL.HRPositionCategories
                       select p).ToList();
            return query;
        }
    }
}