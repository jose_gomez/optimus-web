﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
  internal class LanguagesRepository:ILanguages
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
       ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public List<Languages> GetAllLanguages()
        {
            var query = (from p in ContextSQL.SyLanguages
                         select new Languages
                         {
                             LanguageID = p.LanguageID,
                             Name = p.Name
                         }).ToList();

            return query;

        }

        public List<LanguagesLevel> GetAllLanguagesLevel()
        {
            var query = (from p in ContextSQL.HRLevellanguages
                         select new LanguagesLevel
                         {
                             LevellanguageID = p.LevellanguageID,
                             Level = p.Level
                         }
                         ).ToList();
            return query;
        }
    }
}
