﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
    internal class Position : Contracts.Position
    {

        #region Private Fields

        private const string POSITION_EXIST = "Esta posición ya existe.";
        private const string POSITION_NAME_EMPTY = "Debe especificar el nombre de la posición.";
        private const string POSITION_SUPERIOR_ID_EMPTY = "Debe especificar la posición superior.";

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRPosition> GetAllPositions()
        {
            var result = (from a in Context.HRPositions
                          select a).OrderBy(x => x.PositionName).ToList();
            return result;
        }

        public List<OrgChartModel> GetOrgChart()
        {
            var result = (from a in Context.HRPositions
                          where a.SuperiorPositionID != null
                          select new Models.OrgChartModel
                          {
                              PositionID = a.PositionID,
                              PositionName = a.PositionName,
                              SuperiorPosition = a.SuperiorPositionID == a.PositionID ? new int?() : a.SuperiorPositionID
                          }).ToList();
            return result;
        }

        public List<OrgChartModel> GetOrgChartByPosicion(int PosicionId)
        {
            var result = (from a in Context.HRPositions
                          where a.SuperiorPositionID != null
                          select new Models.OrgChartModel
                          {
                              PositionID = a.PositionID,
                              PositionName = a.PositionName,
                              SuperiorPosition = a.SuperiorPositionID == a.PositionID ? new int?() : a.SuperiorPositionID
                          }).ToList();
            return result;
        }

        public HRPosition GetPositionByID(int positionID)
        {
            var result = (from a in Context.HRPositions
                          where a.PositionID == positionID
                          select a).FirstOrDefault();
            return result;
        }

        public List<HRPosition> GetPositionsByLevels(List<HROrganizationLevel> niveles)
        {
            List<HRPosition> posiciones = new List<HRPosition>();
            var results = new List<HRPosition>();
            foreach (HROrganizationLevel nivel in niveles)
            {
                results = (from a in Context.HRPositions
                           where a.OrganizationLevelID == nivel.OrganizationLevelID
                           select a).ToList();
                foreach (var posicion in results)
                {
                    posiciones.Add(posicion);
                }
            }
            return posiciones;
        }

        public void Save(object objeto)
        {
            HRPosition position = (HRPosition)objeto;
            Validate(position);
            InsertOrUpdateInDatabase(position);
        }

        #endregion Public Methods

        #region Private Methods

        private void Exist(HRPosition position)
        {
            var result = (from a in Context.HRPositions
                          where a.DepartmentID == position.DepartmentID &&
                          a.OrganizationLevelID == position.OrganizationLevelID &&
                          a.PositionName == position.PositionName.Trim().ToUpper() &&
                          a.PositionID == 0
                          select a).FirstOrDefault();
            if (result != null)
            {
                throw new Core.Exceptions.ValidationException(POSITION_EXIST);
            }
        }

        private void InsertOrUpdateInDatabase(HRPosition position)
        {
            var result = (from a in Context.HRPositions
                          where a.PositionID == position.PositionID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new HRPosition();
                Context.HRPositions.InsertOnSubmit(result);
            }
            result.OrganizationLevelID = position.OrganizationLevelID;
            result.DepartmentID = position.DepartmentID;
            result.PositionName = position.PositionName.Trim().ToUpper();
            result.Status = position.Status;
            result.Description = position.Description;
            result.PositionCategoryID = position.PositionCategoryID;
            result.SuperiorPositionID = position.SuperiorPositionID;
            Context.SubmitChanges();
        }

        private void RequiredField(HRPosition position)
        {
            if (string.IsNullOrWhiteSpace(position.PositionName))
            {
                throw new Core.Exceptions.ValidationException(POSITION_NAME_EMPTY);
            }
            if (position.SuperiorPositionID == null || position.SuperiorPositionID == 0)
            {
                throw new Core.Exceptions.ValidationException(POSITION_SUPERIOR_ID_EMPTY);
            }
        }

        private void Validate(HRPosition position)
        {
            RequiredField(position);
            Exist(position);
        }

        #endregion Private Methods

    }
}