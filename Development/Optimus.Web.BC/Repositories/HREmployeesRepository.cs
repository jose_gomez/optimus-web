﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Optimus.Web.BC.Repositories
{
  public  class HREmployeesRepository : Repository<HREmployee>,IHREmployees
    {
        public HREmployeesRepository(OptimusEntities context) : base(context)
        {
           
        }


        private byte[] GetPicture(int employeeId)
        {

            var pic =this.Find(e => e.EmployeeId == employeeId).FirstOrDefault();
            if (pic != null)
            {
                var picture = Context.SYGUIDImages.Where(c => c.GUID == pic.PictureGUID).FirstOrDefault();
                return picture != null ? picture.Picture.ToArray() : null;
            }
            return null;
        }

        public Employees GetEmployeeById(int id)
        {
            return this.Find(x => x.EmployeeId == id).Select(e => new Employees()
            {
                 EmployeeId = e.EmployeeId,
                 FirstName = e.FirstName,
                 LastName = e.LastName,
                 Cedula = e.Cedula,
                 Picture =  GetPicture(id),
                 DepartmentID = Convert.ToInt32(e.DepartmentID),
                 PositionID = (int)e.PositionID
                 

                 
            }).FirstOrDefault();
        }
        public List<Employees> GetEmployeeByDeparment(int dept)
        {
            var query = (from p in Context.HREmployees
                         where p.DepartmentID == dept
                         select new Employees
                         {
                             EmployeeId = p.EmployeeId,
                             Cedula = p.Cedula,
                             FirstName = p.FirstName,
                             LastName = p.LastName,
                             DOB = (DateTime)p.DOB,
                             DepartmentID = (int)p.DepartmentID

                         }
                         ).ToList();
            return query;
        }
        public int GetCountOfActiveEmployees()
        {
            return this.Find(x => x.Status == "A").Count();
        }

        public List<Employees> GetEmployeeBirhtDayToday()
        {
            var query = (from p in Context.HREmployees
                         join h in Context.HRDepartments on p.DepartmentID equals h.DepartmentId
                         where (((DateTime)p.DOB).Day == DateTime.Now.Day) && (((DateTime)p.DOB).Month == DateTime.Now.Month) && p.Status == "A"
                         select new Employees
                         {
                             EmployeeId = p.EmployeeId,
                             FirstName = p.FirstName,
                             LastName = p.LastName,
                             Departamento = h.Name,
                         }).ToList();

            foreach (Employees emp in query)
            {
                emp.PictureBase64 = "data:image/jpg;base64,"+PictureBase64ByID(emp.EmployeeId);
            }
            return query;
        }
        public List<Employees> GetEmployeeBirhtDayMonth()
        {
            var query = (from p in Context.HREmployees
                         join h in Context.HRDepartments on p.DepartmentID equals h.DepartmentId
                         where (((DateTime)p.DOB).Month == DateTime.Now.Month) && p.Status == "A"
                         orderby ((DateTime)p.DOB).Day ascending
                         select new Employees
                         {
                             EmployeeId = p.EmployeeId,
                             FirstName = p.FirstName,
                             LastName = p.LastName,
                             Departamento = h.Name,
                             DOB = (DateTime)p.DOB

                         }).ToList();
            return query;
        }
        private string PictureBase64ByID(int id)
        {
            
            if (GetPicture(id)!= null)
            {
              if (byteArrayToImage(GetPicture(id)) != null)
                {

                    if (ImageToBase64(byteArrayToImage(GetPicture(id)),ImageFormat.Jpeg)!=null  || ImageToBase64(byteArrayToImage(GetPicture(id)), ImageFormat.Jpeg) != string.Empty)
                    {
                        return ImageToBase64(byteArrayToImage(GetPicture(id)), ImageFormat.Jpeg);
                    }else
                    {
                        return string.Empty;
                    }
                }else
                {
                    return string.Empty;
                }
            }
            else
            {
                return string.Empty;
            }

            
        }
        private static Image byteArrayToImage(byte[] byteArrayIn)
        {

            if (byteArrayIn.Length == 0)
                return null;
            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            Image img = byteArrayIn != null ? (Image)converter.ConvertFrom(byteArrayIn) : null;

            return img;
        }
        private string ImageToBase64(Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

    }
}
