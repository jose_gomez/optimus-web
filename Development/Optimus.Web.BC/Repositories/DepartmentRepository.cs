﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using System.Configuration;

using Optimus.Web.DA.Optimus;

namespace Optimus.Web.BC.Repositories
{
  internal class DepartmentRepository:Repository<HRDepartment>,InDepartment
    {
        OptimusEntities co;

        public DepartmentRepository(OptimusEntities context) : base(context)
        {
            this.co = context;
        }

        public Department GetDepartmentsByUsername(string username)
        {
            SYUserRepository userR = new SYUserRepository(this.co);
            HREmployeesRepository emplR = new HREmployeesRepository(this.co);

            User user = userR.GetUserByUserName(username);
            Employees em = emplR.GetEmployeeById(user.EmployeeID);
           
            if (em == null)  throw new Optimus.Core.Exceptions.ValidationException("Necesita un usuario valido,contacte con el departamento de sistema");
  

            return this.Find(x => x.DepartmentId == em.DepartmentID).Select(e => new Department()
            {
                DepartmenId = e.DepartmentId,
                Name = e.Name,
                CompanyID = Convert.ToInt32(e.CompanyID),
                Account = e.Account,
                Status = (bool) e.Status,
                DivisionID = Convert.ToInt32(e.DivisionID),
            }).FirstOrDefault();
        }

        public Department GetDepartmentsByUser(int users) {

            SYUserRepository userR = new SYUserRepository(this.co);
            HREmployeesRepository emplR = new HREmployeesRepository(this.co);

            var usuario = userR.SingleOrDefault(x => x.UserID == users);
            Employees em = emplR.GetEmployeeById((int)usuario.EmployeeID);

            return this.Find(x => x.DepartmentId == em.DepartmentID).Select(e => new Department()
            {
                DepartmenId = e.DepartmentId,
                Name = e.Name,
                CompanyID = Convert.ToInt32(e.CompanyID),
                Account = e.Account,
                Status = (bool)e.Status,
                DivisionID = Convert.ToInt32(e.DivisionID),
            }).FirstOrDefault();

        }
    }
}
