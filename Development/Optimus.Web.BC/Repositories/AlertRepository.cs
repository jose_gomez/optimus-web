﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Repositories
{
    internal class AlertRepository : Contracts.AlertRepository
    {
        public void SaveAlertType(SYAlertsType model)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

            SYAlertsType consAlertType = context.SYAlertsTypes.Where(tab => tab.AlertTypesID == model.AlertTypesID).FirstOrDefault();

            if (consAlertType == null)
            {
                consAlertType = new SYAlertsType();
                consAlertType.create_dt = DateTime.Now;
                context.SYAlertsTypes.InsertOnSubmit(consAlertType);
            }

            consAlertType.AlertTypes = model.AlertTypes;

            context.SubmitChanges();
        }
    }
}
