﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class TipoAccionEmpleado : Contracts.API.TipoAccionEmpleado
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public DTO.TipoAccionEmpleado Create(DTO.TipoAccionEmpleado tipoAccion)
        {
            var result = Get(tipoAccion.ID);
            if (result == null)
            {
                var nuevoTipoAccion = new DA.HRActionsType();
                nuevoTipoAccion.ActionTypeID = tipoAccion.ID;
                nuevoTipoAccion.Name = tipoAccion.Nombre;
                Context.HRActionsTypes.InsertOnSubmit(nuevoTipoAccion);
                Context.SubmitChanges();
                result = Get(nuevoTipoAccion.ActionTypeID);
            }
            return result;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<DTO.TipoAccionEmpleado> Get()
        {
            var result = (from a in Context.HRActionsTypes
                          select new DTO.TipoAccionEmpleado
                          {
                              ID = a.ActionTypeID,
                              Nombre = a.Name
                          }).ToList();
            return result;
        }

        public DTO.TipoAccionEmpleado Get(int id)
        {
            var result = (from a in Context.HRActionsTypes
                          where a.ActionTypeID == id
                          select new DTO.TipoAccionEmpleado
                          {
                              ID = a.ActionTypeID,
                              Nombre = a.Name
                          }).FirstOrDefault();
            return result;
        }

        public DTO.TipoAccionEmpleado Update(DTO.TipoAccionEmpleado tipoAccion)
        {
            DTO.TipoAccionEmpleado result = null;
            var tipoAccionActualizar = (from a in Context.HRActionsTypes
                                        where a.ActionTypeID == tipoAccion.ID
                                        select a).FirstOrDefault();
            if (tipoAccionActualizar != null)
            {
                tipoAccionActualizar.ActionTypeID = tipoAccion.ID;
                tipoAccionActualizar.Name = tipoAccion.Nombre;
                Context.SubmitChanges();
                result = Get(tipoAccionActualizar.ActionTypeID);
            }
            return result;
        }

        #endregion Public Methods

    }
}