﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class WageProposal : Contracts.API.WageProposal
    {

        #region Private Fields

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }
        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        private Position PositionRepo = new Position();

        #endregion Private Fields

        #region Public Methods

        public DTO.PropuestaCompensacionBeneficio Create(DTO.PropuestaCompensacionBeneficio wageProposal)
        {
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == wageProposal.WageProposalID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new HRWageProposal();
                result.CandidateID = wageProposal.CandidateID;
                result.Proposal = wageProposal.Proposal;
                result.PositionID = wageProposal.PositionID;
                result.StatusID = (int)Core.Enumeraciones.WageProposalStatus.PENDIENTE;
                result.UserID = wageProposal.UserID;
                result.Create_dt = DateTime.Now;
                result.Modify_dt = DateTime.Now;
                Context.HRWageProposals.InsertOnSubmit(result);
                Context.SubmitChanges();
                return GetWageProposalsByID(result.WageProposalID);
            }
            return null;
        }

        public void Create(object obj)
        {
            DTO.PropuestaCompensacionBeneficio wageProposal = (DTO.PropuestaCompensacionBeneficio)obj;
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == wageProposal.WageProposalID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new HRWageProposal();
                result.CandidateID = wageProposal.CandidateID;
                result.Proposal = wageProposal.Proposal;
                result.PositionID = wageProposal.PositionID;
                result.StatusID = (int)Core.Enumeraciones.WageProposalStatus.PENDIENTE;
                result.UserID = wageProposal.UserID;
                result.Create_dt = DateTime.Now;
                result.Modify_dt = DateTime.Now;
                Context.HRWageProposals.InsertOnSubmit(result);
                Context.SubmitChanges();
            }
        }

        public void Delete(object obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.PropuestaCompensacionBeneficio> GetWageProposalsByCandidate(int id)
        {
            var wageProposals = (from a in Context.HRWageProposals
                                 where a.CandidateID == id
                                 select new DTO.PropuestaCompensacionBeneficio
                                 {
                                     WageProposalID = a.WageProposalID,
                                     CandidateID = a.CandidateID ?? 0,
                                     Proposal = a.Proposal ?? 0,
                                     PositionID = a.PositionID ?? 0,
                                     Position = PositionRepo.GetPositionByID(a.PositionID ?? 0),
                                     CreateDate = a.Create_dt ?? DateTime.Now,
                                     StatusID = a.StatusID ?? 0,
                                     ModifyDate = a.Modify_dt ?? DateTime.Now,
                                     UserID = a.UserID ?? 0
                                 }).OrderByDescending(x => x.CreateDate).ToList();
            return wageProposals;
        }

        public DTO.PropuestaCompensacionBeneficio GetWageProposalsByID(int id)
        {
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == id
                          select new DTO.PropuestaCompensacionBeneficio
                          {
                              WageProposalID = a.WageProposalID,
                              CandidateID = a.CandidateID ?? 0,
                              Proposal = a.Proposal ?? 0,
                              PositionID = a.PositionID ?? 0,
                              Position = PositionRepo.GetPositionByID(a.PositionID ?? 0),
                              CreateDate = a.Create_dt ?? DateTime.Now,
                              StatusID = a.StatusID ?? 0,
                              ModifyDate = a.Modify_dt ?? DateTime.Now,
                              UserID = a.UserID ?? 0
                          }).FirstOrDefault();
            return result;
        }

        public DTO.PropuestaCompensacionBeneficio Save(DTO.PropuestaCompensacionBeneficio wageProposal)
        {
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == wageProposal.WageProposalID
                          select a).FirstOrDefault();
            if (result == null) { return Create(wageProposal); } else { return Update(wageProposal); }
        }

        public void Save(object obj)
        {
            DTO.PropuestaCompensacionBeneficio wageProposal = (DTO.PropuestaCompensacionBeneficio)obj;
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == wageProposal.WageProposalID
                          select a).FirstOrDefault();
            if (result == null) { Create(wageProposal); } else { Update(wageProposal); }
        }

        public DTO.PropuestaCompensacionBeneficio Update(DTO.PropuestaCompensacionBeneficio wageProposal)
        {
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == wageProposal.WageProposalID
                          select a).FirstOrDefault();
            if (result != null)
            {
                result.StatusID = wageProposal.StatusID;
                result.UserID = wageProposal.UserID;
                result.Modify_dt = DateTime.Now;
                Context.SubmitChanges();
                return GetWageProposalsByID(result.WageProposalID);
            }
            return null;
        }

        public void Update(object obj)
        {
            DTO.PropuestaCompensacionBeneficio wageProposal = (DTO.PropuestaCompensacionBeneficio)obj;
            var result = (from a in Context.HRWageProposals
                          where a.WageProposalID == wageProposal.WageProposalID
                          select a).FirstOrDefault();
            if (result != null)
            {
                result.StatusID = wageProposal.StatusID;
                result.UserID = wageProposal.UserID;
                result.Modify_dt = DateTime.Now;
                Context.SubmitChanges();
            }
        }

        #endregion Public Methods

    }
}