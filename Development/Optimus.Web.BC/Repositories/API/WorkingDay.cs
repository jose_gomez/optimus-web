﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.DTO;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Repositories.API
{
    public class WorkingDay : Contracts.API.WorkingDay
    {
        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }
        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);
        /// <summary>
        /// Obtiene todas las jornadas de trabajo.
        /// </summary>
        /// <returns>Listado de jornadas.</returns>
        public List<DTO.Jornada> Get()
        {
            var result = (from a in Context.HRJornadas
                          select new DTO.Jornada
                          {
                              ID = a.ID,
                              Name = a.Jornada,
                              UserID = a.UserId,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }
        /// <summary>
        /// Obtiene una jornada de trabajo.
        /// </summary>
        /// <param name="id">Identificador de la jornada.</param>
        /// <returns>Jornada de trabajo.</returns>
        public DTO.Jornada Get(int id)
        {
            var result = (from a in Context.HRJornadas
                          where a.ID == id
                          select new DTO.Jornada
                          {
                              ID = a.ID,
                              Name = a.Jornada,
                              UserID = (int)a.UserId,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).FirstOrDefault();
            return result;
        }
    }
}
