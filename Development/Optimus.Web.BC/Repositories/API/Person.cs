﻿using Optimus.Web.DA;
using System;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Person : Contracts.API.Person
    {

        #region Private Fields

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }
        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Methods

        public void Create(object obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(object obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Persona GetPersonByID(int id)
        {
            var person = (from a in Context.HRPeoples
                          where a.PersonID == id
                          select new DTO.Persona
                          {
                              PersonID = a.PersonID,
                              FirstName = a.FirstName,
                              LastName = a.LastName,
                              PictureGUID = a.PictureGUID,
                              Cedula = a.Cedula,
                              BirthDate = a.BirthDate ?? DateTime.Now
                          }).FirstOrDefault();
            return person;
        }

        public void Save(object obj)
        {
            throw new NotImplementedException();
        }

        public void Update(object obj)
        {
            throw new NotImplementedException();
        }

        #endregion Public Methods

    }
}