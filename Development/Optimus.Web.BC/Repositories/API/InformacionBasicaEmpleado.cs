﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class InformacionBasicaEmpleado : Contracts.API.InformacionBasicaEmpleado
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        private Position PositionRepo = new Position();

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.InformacionBasicaEmpleado> Get()
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public DTO.InformacionBasicaEmpleado Get(int id)
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where a.EmployeeId == id
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetActivos()
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.ACTIVO
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetActivos(string filtro)
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.ACTIVO) &&
                          (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID((a.PositionID == null ? 0 : (int)a.PositionID)).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetInactivos()
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.INACTIVO
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetInactivos(string filtro)
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.INACTIVO) &&
                          (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetLiquidados()
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.LIQUIDADO
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetLiquidados(string filtro)
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.LIQUIDADO) &&
                          (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetPendientes()
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.PENDIENTE
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetPendientes(string filtro)
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.PENDIENTE) &&
                          (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                              PositionName = PositionRepo.GetPositionByID(a.PositionID == null ? 0 : (int)a.PositionID).PositionName
                          }).ToList();
            return result;
        }

        #endregion Public Methods

    }
}