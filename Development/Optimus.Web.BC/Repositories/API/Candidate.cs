﻿using Optimus.Web.DA;
using System;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Candidate : Contracts.API.Candidate
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public DTO.Candidate GetCandidateByID(int id)
        {
            var candidate = (from p in Context.HRCandidates
                             where p.CandidateID == id
                             select new DTO.Candidate
                             {
                                 PostulatePositionID = (int)p.PostulatePositionID,
                                 FirstName = p.FirstName,
                                 LastName = p.LastName,
                                 Streets = p.Streets,
                                 HouseNumber = p.HouseNumber,
                                 Sector = p.Sector,
                                 location = p.location,
                                 Sex = Convert.ToString(p.Sex),
                                 NationalityID = p.NationalityID,
                                 PhoneNumber = p.PhoneNumber,
                                 MovilNumber = p.MovilNumber,
                                 Email = p.Email,
                                 Age = (int)p.Age,
                                 Cedula = p.Cedula,
                                 BirthDate = p.BirthDate,
                                 alias = p.Alias,
                                 EmergencyNumber = p.EmergencyNumber,
                                 EmergencyContactName = p.EmergencyContactName,
                                 Size = p.Size,
                                 Religion = p.Religion,
                                 CivilStatus = p.CivilStatus,
                                 Allergies = (bool)p.allergies,
                                 AllergiesType = p.AllergiesType,
                                 suffering = (bool)p.suffering,
                                 sufferingType = p.sufferingType,
                                 AvailableOverTime = (bool)p.AvailableOvertime,
                                 AvailableWeekend = (bool)p.AvailableWeekend,
                                 LivesWith = p.LivesWith,
                                 SpouseName = p.SpouseName,
                                 SpouseActivity = p.SpouseActivity,
                                 MotherName = p.MotherName,
                                 FatherName = p.FatherName,
                                 DependentChildren = (int)p.DependentChildren,
                                 DependentPeople = (int)p.DependentPeople,
                                 StudiesID = (int)p.StudiesID,
                                 ComputerStudies = (bool)p.ComputerStudies,
                                 IsStudent = p.IsStudent,
                                 DaysWeek = p.DaysWeek,
                                 Schedule = p.Schedule,
                                 ProgramsKnow = p.Programsknow,
                                 WhosRecommended = p.WhosRecommended,
                                 FamilyOrFriend = (bool)p.FamilyOrFriend,
                                 FamilyOrFriendName = p.FamilyOrFriendName,
                                 FamilyOrFriendPuesto = p.FamilyOrFriendPuesto,
                                 FamilyOrFriendRelationship = p.FamilyOrFriendRelationship,
                                 PositionCategoryID = (int)p.PositionCategoryID,
                                 Interviewed = (bool)p.interviewed,
                                 CandidateStateID = (int)p.CandidateStateID
                             }
                                   ).FirstOrDefault();

            return candidate;
        }

        #endregion Public Methods

    }
}