﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Repositories;
using Optimus.Web.DA.Optimus;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Optimus.Web.BC.Services
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly OptimusEntities _optimusContext;

        public UnitOfWork()
        {
            this._optimusContext = new OptimusEntities("name=OptimusEntities");
            this.Employees = new HREmployeesRepository(this._optimusContext);
            this.DepartmentHeads = new TSDepartmentHeadsRepository(this._optimusContext);
            this.TSTickets = new TSTicketRepository(_optimusContext);
            /*trinidad*/
            this.User = new SYUserRepository(this._optimusContext);
            this.dept = new DepartmentRepository(this._optimusContext);
            this.TicketState = new StateRepository(this._optimusContext);
            this.comment = new CommentTicketsRepository(this._optimusContext);
            this.ResponsibleTickets = new ResponsiblesTicketsRepository(_optimusContext);
            this.UserSystemPermissions = new UserSystemPermissionsRepository(this._optimusContext);
            this.LogTickets = new LogTicketsRepository(this._optimusContext);
            this.Extensions = new EmployeeExtensionsRepository(this._optimusContext);
            
            
                      
          
        }


        public IHREmployees Employees { get; private set; }
        public ITSDepartmentHeads DepartmentHeads { get; private set; }
        public ITSTicket TSTickets { get; private set; }
        public ISYUsers User { get; set;}
        public InDepartment dept { get; set; }
        public ITicketState TicketState { get; set; } 
        public ICommentTicket comment { get; set; }
        public IResponsiblesTicket ResponsibleTickets { get; set; }
        public IUserSystemPermissions UserSystemPermissions { get; set; }
        public ILogTickets LogTickets { get; set; }
        public IExtensions Extensions { get; set; }


        public int Complete()
        {
            return this._optimusContext.SaveChanges();
        }

        public void Dispose()
        {
            _optimusContext.Dispose();
        }
    }
}
