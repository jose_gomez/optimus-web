﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class ResetPassHistoryService : Contracts.ResetPassHistory
    {
        private Contracts.ResetPassHistory _Repo;
        public ResetPassHistoryService(Contracts.ResetPassHistory repo)
        {
            this._Repo = repo;
        }
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public SYResetPassHistory GetResetPassHistoryByID(string id)
        {
            return this._Repo.GetResetPassHistoryByID(id);
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }

        public void Save(User user, string code)
        {
            this._Repo.Save(user, code);
        }
    }
}
