﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Services
{
    public class LanguagesServices : ILanguages
    {
        private ILanguages _LanguagesRepo;
        public LanguagesServices(ILanguages repo)
        {
            this._LanguagesRepo = repo;
        }
        public List<Languages> GetAllLanguages()
        {
            return this._LanguagesRepo.GetAllLanguages();
        }

        public List<LanguagesLevel> GetAllLanguagesLevel()
        {
            return this._LanguagesRepo.GetAllLanguagesLevel();
        }
    }
}
