﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class PositionService : Contracts.Position
    {
        private Contracts.Position _PositionRepo;
        public PositionService(Contracts.Position positionRepo)
        {
            this._PositionRepo = positionRepo;
        }
        public void Delete(object objeto)
        {
            this._PositionRepo.Delete(objeto);
        }

        public List<HRPosition> GetAllPositions()
        {
            return this._PositionRepo.GetAllPositions();
        }

        public List<OrgChartModel> GetOrgChart()
        {
            return this._PositionRepo.GetOrgChart();
        }

        public List<OrgChartModel> GetOrgChartByPosicion(int PosicionId)
        {
            throw new NotImplementedException();
        }

        public HRPosition GetPositionByID(int positionID)
        {
            return this._PositionRepo.GetPositionByID(positionID);
        }

        public List<HRPosition> GetPositionsByLevels(List<HROrganizationLevel> niveles)
        {
            return this._PositionRepo.GetPositionsByLevels(niveles);
        }

        public void Save(object objeto)
        {
            this._PositionRepo.Save(objeto);
        }
    }
}
