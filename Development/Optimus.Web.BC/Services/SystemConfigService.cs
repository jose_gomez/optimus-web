﻿using Optimus.Web.BC.Contracts;
using System;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class SystemConfigService : ISystemConfig
    {
        #region Private Fields

        private ISystemConfig _RepoSystemConfig;

        #endregion Private Fields

        #region Public Constructors

        public SystemConfigService(ISystemConfig repoSystemConfig)
        {
            this._RepoSystemConfig = repoSystemConfig;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public SYSystemsConfiguration GetSysTemConfigByName(string name)
        {
            return this._RepoSystemConfig.GetSysTemConfigByName(name);
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }

        #endregion Public Methods
    }
}