﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Services
{
    public class PersonnelRequisitonsService : IPersonnelRequisitions
    {
        private IPersonnelRequisitions _PersonnelRequisitionRepo;

        public PersonnelRequisitonsService(IPersonnelRequisitions repo)
        {
            this._PersonnelRequisitionRepo = repo;
        }

        public void Save(HRPersonnelRequisition requisition)
        {
            this._PersonnelRequisitionRepo.Save(requisition);
        }

        public List<VwTurnosJornada> GetAllJornadas()
        {
            return this._PersonnelRequisitionRepo.GetAllJornadas();
        }

        public List<HRPersonnelRequisition> GetAllOpenedRequisitions()
        {
            return this._PersonnelRequisitionRepo.GetAllOpenedRequisitions();
        }

        public void Save(object objeto)
        {
            this._PersonnelRequisitionRepo.Save(objeto);
        }

        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public int SaveReturnID(HRPersonnelRequisition requisicion)
        {
            return this._PersonnelRequisitionRepo.SaveReturnID(requisicion);
        }

        public HRPersonnelRequisition GetByID(int id)
        {
            return this._PersonnelRequisitionRepo.GetByID(id);
        }

        public List<HRPersonnelRequisition> GetAllNewOpenedRequisitions()
        {
            return this._PersonnelRequisitionRepo.GetAllNewOpenedRequisitions();
        }

        public List<HRPersonnelRequisition> GetAllOldOpenedRequisitions()
        {
            return this._PersonnelRequisitionRepo.GetAllOldOpenedRequisitions();
        }

        public void Approve(HRRequisitionsApprobation aprobacion)
        {
            this._PersonnelRequisitionRepo.Approve(aprobacion);
        }
    }
}
