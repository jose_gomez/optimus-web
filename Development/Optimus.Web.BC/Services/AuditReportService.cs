﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Services
{
    public class AuditReportService
    {
        IAuditoryReport _repository;
        public AuditReportService(IAuditoryReport rep)
        {
            this._repository = rep;
        }

        public List<SHAudit> GetAuditEPPByDetDate(int deparment, DateTime date)
        {
            return this._repository.GetAuditEPPByDetDate(deparment, date);
        }
    }
}
