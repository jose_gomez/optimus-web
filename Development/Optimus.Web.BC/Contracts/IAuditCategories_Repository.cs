﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface IAuditCategories_Repository
    {
        void SaveCategoryByID(SHAuditCategory tabCat);
        List<SHAuditCategory> GetAllCategory();
        List<SHAuditCategory> GetCategoryByID(int idCat);
        List<SHAuditCategory> GetCategoryByName(string nameCategory);
        List<SHAuditCategory> GetCategoryByControlID(int controlID);
        int CountIndicatorByCategory(int auditType);
        int CountIndicatorByAudit(int auditMasterID);
    }
}
