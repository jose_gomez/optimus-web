﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface TipoAccionEmpleado
    {

        #region Public Methods

        DTO.TipoAccionEmpleado Create(DTO.TipoAccionEmpleado tipoAccion);

        void Delete(int id);

        List<DTO.TipoAccionEmpleado> Get();

        DTO.TipoAccionEmpleado Get(int id);

        DTO.TipoAccionEmpleado Update(DTO.TipoAccionEmpleado tipoAccion);

        #endregion Public Methods

    }
}