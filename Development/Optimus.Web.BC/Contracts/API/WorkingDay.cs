﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts.API
{
    public interface WorkingDay
    {
        List<DTO.Jornada> Get();
        DTO.Jornada Get(int id);
    }
}
