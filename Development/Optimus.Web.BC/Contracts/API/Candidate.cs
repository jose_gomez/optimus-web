﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts.API
{
   public interface Candidate
    {
        DTO.Candidate GetCandidateByID(int id);
    }
}
