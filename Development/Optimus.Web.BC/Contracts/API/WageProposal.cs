﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface WageProposal : CRUD
    {

        #region Public Methods

        DTO.PropuestaCompensacionBeneficio Create(DTO.PropuestaCompensacionBeneficio wageProposal);

        List<DTO.PropuestaCompensacionBeneficio> GetWageProposalsByCandidate(int id);

        DTO.PropuestaCompensacionBeneficio GetWageProposalsByID(int id);

        DTO.PropuestaCompensacionBeneficio Save(DTO.PropuestaCompensacionBeneficio wageProposal);

        DTO.PropuestaCompensacionBeneficio Update(DTO.PropuestaCompensacionBeneficio wageProposal);

        #endregion Public Methods
    }
}