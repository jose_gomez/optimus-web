﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Employee : CRUD
    {

        #region Public Methods

        List<DTO.AccionEmpleado> GetEmployeeActions(int employeeID);

        DTO.Empleado GetEmployeeByID(int id);

        DTO.Posicion GetEmployeePosition(int employeeID);

        List<DTO.Empleado> GetEmployeesByPosition(int positionID);

        #endregion Public Methods
    }
}