﻿
using Optimus.Web.BC.Models;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
   public interface InDepartment: IRepository<HRDepartment>
    {
        Department GetDepartmentsByUsername(string username);
    }
}
