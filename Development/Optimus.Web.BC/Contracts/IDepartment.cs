﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using Optimus.Web.BC.Contracts;

namespace Optimus.Web.BC.Contracts
{
    public interface IDepartment : ICRUD
    {
        List<HRDepartment> GetAllDepartments();
        HRDepartment GetBy(int id);
        
    }
}
