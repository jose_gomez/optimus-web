﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface IAuditIndicator
    {
        void SaveByID(SHAuditIndicator indicator);
        List<AuditIndicator> GetAllIndicator();
        List<VwAuditIndicatorOrder> GetAllIndicatorByCategoryID(int idCat);
        List<SHAuditIndicator> GetIndicatorByID(int idIndic);
        List<Models.AuditIndicator> GetAllIndicatorByControlID(int controlID);
    }
}
