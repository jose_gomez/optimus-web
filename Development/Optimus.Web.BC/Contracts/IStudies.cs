﻿using Optimus.Web.BC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
  public  interface IStudies
    {
        List<Studies> GetAllStudies();
    }
}
