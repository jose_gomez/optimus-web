﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Optimus.Web.BC.Contracts
{
    public interface IUnitOfWork : IDisposable
    {
      
        int Complete();
    }
}
