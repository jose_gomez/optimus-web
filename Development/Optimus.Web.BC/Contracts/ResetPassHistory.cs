﻿using Optimus.Web.BC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface ResetPassHistory : ICRUD
    {
        DA.SYResetPassHistory GetResetPassHistoryByID(string id);
        void Save(User user, string code);
    }
}
