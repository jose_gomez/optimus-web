﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    [Serializable]
    public class TicketStatistics
    {
        private double validatedTicketsPercentage;
        public double Validated
        {
            get
            {
                return ReturnZeroIfValueIsNaN(validatedTicketsPercentage);
            }
            set { validatedTicketsPercentage = value; }
        }
        private double assignedTicketsPercentage;
        public double Assigned
        {
            get
            {
                return ReturnZeroIfValueIsNaN(assignedTicketsPercentage);
            }
            set
            {
                assignedTicketsPercentage = value;
            }
        }
        private double unassignedTicketsPercentage;
        public double Pending
        {
            get
            {
                return ReturnZeroIfValueIsNaN(unassignedTicketsPercentage);
            }
            set { unassignedTicketsPercentage = value; }
        }
        private double cancelledTicketsPercentage;
        public double Cancelled
        {
            get
            {
                return ReturnZeroIfValueIsNaN(cancelledTicketsPercentage);
            }
            set { cancelledTicketsPercentage = value; }
        }
        private double ticketsDonePercentage;
        public double Done
        {
            get
            {
                return ReturnZeroIfValueIsNaN(ticketsDonePercentage);
            }
            set { ticketsDonePercentage = value; }
        }

        private double approvedTicketsPercentage;
        public double Approved
        {
            get
            {
                return ReturnZeroIfValueIsNaN(approvedTicketsPercentage);
            }
            set { approvedTicketsPercentage = value; }
        }

        public double TotalTicketsCount { get; set; }
        private double ticketsInProcess;
        public double Working
        {
            get
            {
                return ReturnZeroIfValueIsNaN(ticketsInProcess);
            }
            set
            {
                ticketsInProcess = value;
            }
        }

        private double ReturnZeroIfValueIsNaN(double value)
        {
            if (value != double.NaN)
                return value;
            else
                return 0;
        }
    }
}
