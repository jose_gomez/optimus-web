﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public class ResponsiblesTicket
    {
        public int TicketsResponsibleID { get; set; }
        public int TcketsID { get; set; }
        public int Responsible { get; set; }
        public bool Assigned { get; set; }
        public DateTime AssignDate { get; set; }
    }
}
