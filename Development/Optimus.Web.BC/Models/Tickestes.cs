﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
 public class Tickestes
    {
        public int TicketNumber { get; set; }
        public Nullable<System.DateTime> SubmitDate { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public string Department { get; set; }
        public int IdDepartment { get; set; }
        public string Status { get; set; }
        public string SubmitUserName { get; set; }
        public int PorcentProgress { get; set; }
        public string Responsible { get; set; }
        public int EmployeesIDResponsible { get; set; }
        public string DepartamentAdjudico { get; set; }
        public Boolean? validated { get; set; }
        public int StatusID { get; set; }
        public bool Requisition { get; set; }
        public int DeparmentIdSubmit { get; set; }
        public int PriorityID { get; set; }
        public string Priority { get; set; }
        public int? FatherTickets { get; set; }
        public bool Approved { get; set; }
        public int? TipoProblemaID { get; set; }
        public string TipoProblema { get; set; }
    }
}
