﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public  class CommentTicket
    {
        public int CommentTicketsID  { get; set; }
        public int TicketsID  { get; set; }
        public int  UserID { get; set; }
        public string Comentarios { get; set; }
        public System.DateTime Datetime {get;set;}
        public string UserName { get; set; }


    }
}
