﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class Audit
    {
        
        public int AuditMasterID { get; set; }
        public DateTime? AuditDate { get; set; }
        public int DepartmentId { get; set; }
        public string DepartmentName { get; set; }
        public int SupervisorId { get; set; }
        public string SupervisorName { get; set; }
        public int AnalystId { get; set; }
        public string AnalistName { get; set; }
        public int ControlID { get; set; }
        public string AuditName { get; set; }
    }
}
