﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models.SHAudit
{
    public class AuditEPPModels
    {
        public string departamento { get; set; }

        public string epp { get; set; }

        public int condicion { get; set; }

        public int uso { get; set; }

        public string observaciones { get; set; }
    }
}
