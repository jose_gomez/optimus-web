﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class ModelBase
    {

        List<string> errors = new List<string>();

        public string ErrorMEssages
        {
            get
            {
                StringBuilder builder = new StringBuilder();
                foreach (var error in this.errors)
                {
                    builder.Append(error + "\n");
                }
                return builder.ToString();
            }
        }
        public bool IsValid()
        {
            var validationContext = new ValidationContext(this, null, null);
            List<ValidationResult> results = new List<ValidationResult>();
            if (Validator.TryValidateObject(this, validationContext, results, true))
            {
                return true;
                //Instead of a Redirect here, you need to do something WinForms to display the main form or something like a Dialog Close.
                //return RedirectToAction("Index");
            }
            else {
                foreach (var error in results)
                {
                    errors.Add(error.ErrorMessage);
                }
                return false;
            }
        }

    }
}
