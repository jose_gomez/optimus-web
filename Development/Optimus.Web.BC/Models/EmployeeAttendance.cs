﻿using Optimus.Web.BC;
using Optimus.Web.BC.Repositories;
using Optimus.Web.BC.Services;
using Optimus.Web.DA.Payroll;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public  class EmployeeAttendance : Employees
    {
        public int TotalEmployeesInAttendance { get; set; }
        public double AttendancePercentage { get; set; }
        public DateTime Date { get; set; }
        public int TotalEmployees { get; set; }
        PayrollContextDataContext payrollContext;
        ServiceContainer container;
        UnitOfWork worker;
        public EmployeeAttendance()
        {
            payrollContext = new PayrollContextDataContext(ConfigurationManager.ConnectionStrings["Payroll"].ConnectionString);
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }
        public void CalculateAttendance(DateTime date)
        {
            TotalEmployees = worker.Employees.GetCountOfActiveEmployees();
            date = DateTime.Parse(date.ToShortDateString());
            TotalEmployeesInAttendance = payrollContext.HistorialdePonches.Where(x => x.Fecha == date).Select(x => x.Numero_empleado).Distinct().Count();
            AttendancePercentage = Math.Round(((double)TotalEmployeesInAttendance / (double)TotalEmployees) * 100, 2);
        }
    }
}
