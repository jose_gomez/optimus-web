﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public  class ProblemType
    {
        public int ProblemTypeID { get; set; }
        public int DepartmentID { get; set; }
        public string Name { get; set; }
    }
}
