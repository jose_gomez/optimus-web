﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class PersonFactory
    {
        #region Public Methods

        public static List<Persona> DeserializeList(string data)
        {
            List<Persona> persons = null;
            persons = JsonConvert.DeserializeObject<List<Persona>>(data);
            return persons;
        }

        public static Persona Deserialize(string data)
        {
            Persona person = null;
            person = JsonConvert.DeserializeObject<Persona>(data);
            return person;
        }

        public static string Serialize(Persona person)
        {
            string data = JsonConvert.SerializeObject(person);
            return data;
        }

        #endregion Public Methods
    }
}
