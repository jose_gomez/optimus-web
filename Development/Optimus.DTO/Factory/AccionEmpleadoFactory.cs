﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class AccionEmpleadoFactory
    {
        #region Public Methods

        public static AccionEmpleado Deserialize(string data)
        {
            AccionEmpleado accion = null;
            accion = JsonConvert.DeserializeObject<AccionEmpleado>(data);
            return accion;
        }

        public static List<AccionEmpleado> DeserializeList(string data)
        {
            List<AccionEmpleado> acciones = null;
            acciones = JsonConvert.DeserializeObject<List<AccionEmpleado>>(data);
            return acciones;
        }
        public static string Serialize(AccionEmpleado accion)
        {
            string data = JsonConvert.SerializeObject(accion);
            return data;
        }

        #endregion Public Methods
    }
}