﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class PositionFactory
    {
        #region Public Methods

        public static List<Posicion> DeserializeList(string data)
        {
            List<Posicion> positions = null;
            positions = JsonConvert.DeserializeObject<List<Posicion>>(data);
            return positions;
        }

        public static Posicion Deserialize(string data)
        {
            Posicion position = null;
            position = JsonConvert.DeserializeObject<Posicion>(data);
            return position;
        }

        public static string Serialize(Posicion position)
        {
            string data = JsonConvert.SerializeObject(position);
            return data;
        }

        #endregion Public Methods
    }
}