﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class EmployeeFactory
    {

        #region Public Methods

        public static Empleado Deserialize(string data)
        {
            Empleado employee = null;
            employee = JsonConvert.DeserializeObject<Empleado>(data);
            return employee;
        }

        public static List<Empleado> DeserializeList(string data)
        {
            List<Empleado> employees = null;
            employees = JsonConvert.DeserializeObject<List<Empleado>>(data);
            return employees;
        }
        public static string Serialize(Empleado employee)
        {
            string data = JsonConvert.SerializeObject(employee);
            return data;
        }

        #endregion Public Methods

    }
}