﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class TipoAccionEmpleadoFactory
    {
        #region Public Methods

        public static TipoAccionEmpleado Deserialize(string data)
        {
            TipoAccionEmpleado tipoAccion = null;
            tipoAccion = JsonConvert.DeserializeObject<TipoAccionEmpleado>(data);
            return tipoAccion;
        }

        public static List<TipoAccionEmpleado> DeserializeList(string data)
        {
            List<TipoAccionEmpleado> tiposAcciones = null;
            tiposAcciones = JsonConvert.DeserializeObject<List<TipoAccionEmpleado>>(data);
            return tiposAcciones;
        }
        public static string Serialize(TipoAccionEmpleado tipoAccion)
        {
            string data = JsonConvert.SerializeObject(tipoAccion);
            return data;
        }

        #endregion Public Methods
    }
}