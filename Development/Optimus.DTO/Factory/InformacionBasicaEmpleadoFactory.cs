﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class InformacionBasicaEmpleadoFactory
    {
        #region Public Methods

        public static InformacionBasicaEmpleado Deserialize(string data)
        {
            InformacionBasicaEmpleado result = null;
            result = JsonConvert.DeserializeObject<InformacionBasicaEmpleado>(data);
            return result;
        }

        public static List<InformacionBasicaEmpleado> DeserializeList(string data)
        {
            List<InformacionBasicaEmpleado> result = null;
            result = JsonConvert.DeserializeObject<List<InformacionBasicaEmpleado>>(data);
            return result;
        }
        public static string Serialize(InformacionBasicaEmpleado informacionBasica)
        {
            string data = JsonConvert.SerializeObject(informacionBasica);
            return data;
        }

        #endregion Public Methods
    }
}
