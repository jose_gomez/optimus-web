﻿using System;

namespace Optimus.DTO
{
    /// <summary>
    /// Persona
    /// </summary>
    public class Persona
    {
        #region Public Properties

        /// <summary>
        /// Fecha de nacimiento.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Lugar de nacimiento.
        /// </summary>
        public string BirthPlace { get; set; }

        /// <summary>
        /// Cédula.
        /// </summary>
        public string Cedula { get; set; }

        /// <summary>
        /// Ciudad.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Tipo de documento personal.
        /// </summary>
        public int? DocumentTypeID { get; set; }

        /// <summary>
        /// Nombres.
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Genero.
        /// </summary>
        public char? Gender { get; set; }

        /// <summary>
        /// Numero de casa.
        /// </summary>
        public string HomeNum { get; set; }

        /// <summary>
        /// Apellidos.
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Estado civil.
        /// </summary>
        public int? MaritalStatusID { get; set; }

        /// <summary>
        /// Teléfono móvil.
        /// </summary>
        public string Movil { get; set; }

        /// <summary>
        /// Nacionalidad.
        /// </summary>
        public int? NacionalityID { get; set; }

        /// <summary>
        /// Identificador.
        /// </summary>
        public int PersonID { get; set; }
        /// <summary>
        /// Identificador de la imagen.
        /// </summary>
        public string PictureGUID { get; set; }
        /// <summary>
        /// Sector.
        /// </summary>
        public string Sector { get; set; }

        /// <summary>
        /// Calle.
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Teléfono local.
        /// </summary>
        public string Telephone { get; set; }

        #endregion Public Properties
    }
}