﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    /// <summary>
    /// Nacionalidad.
    /// </summary>
    public class Nacionalidad
    {
        /// <summary>
        /// Identificador.
        /// </summary>
        public int NationalityID { get; set; }
        /// <summary>
        /// Nombre de la nacionalidad.
        /// </summary>
        public string Name { get; set; }
    }
}
