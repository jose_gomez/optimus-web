﻿namespace Optimus.DTO
{
    /// <summary>
    /// Tipos de acciones que se realizan a un empleado.
    /// </summary>
    public class TipoAccionEmpleado
    {

        #region Public Properties

        /// <summary>
        /// Identificador.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Nombre de la acción.
        /// </summary>
        public string Nombre { get; set; }

        #endregion Public Properties

    }
}