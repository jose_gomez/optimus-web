﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    /// <summary>
    /// Estado civil.
    /// </summary>
    public class EstadoCivil
    {
        /// <summary>
        /// Identificador.
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Nombre.
        /// </summary>
        public string Name { get; set; }
    }
}
