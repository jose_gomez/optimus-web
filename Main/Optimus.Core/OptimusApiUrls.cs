﻿namespace Optimus.Core
{
    public class OptimusApiUrls
    {
        #region Public Fields

        public const string AccionesEmpleados_GET_Pendientes = PREFIX_AccionesEmpleados + "/Get/Pendientes";
        public const string AccionesEmpleados_GET_Pendientes_FECHA = PREFIX_AccionesEmpleados + "/Get/Pendientes/{fecha}";
        public const string AccionesEmpleados_POST = PREFIX_AccionesEmpleados + "/Post";
        public const string Candidatos_GET_BY_ID = PREFIX_Candidatos + "/Get/{id}";
        public const string CategoriasCompetencias_GET = PREFIX_CategoriasCompetencias + "/Get";
        public const string CategoriasCompetencias_GET_BY_ID = PREFIX_CategoriasCompetencias + "/Get/{id}";
        public const string CategoriasCompetencias_GET_Competencias = PREFIX_CategoriasCompetencias + "/Get/{id}/Competencias";
        public const string Competencias_GET = PREFIX_Competencias + "/Get";
        public const string Competencias_GET_BY_ID = PREFIX_Competencias + "/Get/{id}";
        public const string Departamentos_GET = PREFIX_Departamentos + "/Get";
        public const string Departamentos_GET_POSICIONES_ACTIVE = PREFIX_Departamentos + "/Get/{id}/Posiciones/Activas";
        public const string Departamentos_GET_POSICIONES_INACTIVE = PREFIX_Departamentos + "/Get/{id}/Posiciones/Inactivas";
        public const string Empleados_DELETE = PREFIX_Empleados + "/Delete/{id}";
        public const string Empleados_GET_AccionesEmpleado = PREFIX_Empleados + "Get/{id}/AccionesEmpleado";
        public const string Empleados_GET_BY_ID = PREFIX_Empleados + "/Get/{id}";
        public const string Empleados_GET_BY_POSITION_ID = PREFIX_Empleados + "/Get/Posicion/{id}";
        public const string Empleados_GET_Posicion = PREFIX_Empleados + "/Get/{id}/Posicion";
        public const string Empleados_POST = PREFIX_Empleados + "/Post";
        public const string Empleados_PUT = PREFIX_Empleados + "/Put/{id}";
        public const string InformacionesBasicasEmpleados_GET = PREFIX_InformacionesBasicasEmpleados + "/Get";
        public const string InformacionesBasicasEmpleados_GET_ACTIVOS = PREFIX_InformacionesBasicasEmpleados + "/Get/Activos";
        public const string InformacionesBasicasEmpleados_GET_ACTIVOS_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Activos/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Filtro/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_BY_ID = PREFIX_InformacionesBasicasEmpleados + "/Get/{id}";
        public const string InformacionesBasicasEmpleados_GET_INACTIVOS = PREFIX_InformacionesBasicasEmpleados + "/Get/Inactivos";
        public const string InformacionesBasicasEmpleados_GET_INACTIVOS_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Inactivos/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_LIQUIDADOS = PREFIX_InformacionesBasicasEmpleados + "/Get/Liquidados";
        public const string InformacionesBasicasEmpleados_GET_LIQUIDADOS_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Liquidados/{filtro}";
        public const string InformacionesBasicasEmpleados_GET_PENDIENTES = PREFIX_InformacionesBasicasEmpleados + "/Get/Pendientes";
        public const string InformacionesBasicasEmpleados_GET_PENDIENTES_BY_FILTRO = PREFIX_InformacionesBasicasEmpleados + "/Get/Pendientes/{filtro}";
        public const string Jornadas_GET = PREFIX_Jornadas + "/Get";
        public const string Jornadas_GET_BY_ID = PREFIX_Jornadas + "/Get/{id}";
        public const string Modulos_GET = PREFIX_Modulos + "/Get";
        public const string Modulos_GET_BY_ID = PREFIX_Modulos + "/Get/{id}";
        public const string Modulos_GET_MODULOS = PREFIX_Modulos + "/Get/Modulos";
        public const string Posiciones_DELETE = PREFIX_Posiciones + "/Delete/{id}";
        public const string Posiciones_GET = PREFIX_Posiciones + "/Get";
        public const string Posiciones_GET_ACTIVE = PREFIX_Posiciones + "/Get/Estado/Activo";
        public const string Posiciones_GET_BY_ID = PREFIX_Posiciones + "/Get/{id}";
        public const string Posiciones_GET_BY_STATUS = PREFIX_Posiciones + "/Get/Estado/{id}";
        public const string Posiciones_GET_Departamento = PREFIX_Posiciones + "/Get/{id}/Departamento";
        public const string Posiciones_GET_INACTIVE = PREFIX_Posiciones + "/Get/Estado/Inactivo";
        public const string Posiciones_GET_NivelCompetencia = PREFIX_Posiciones + "/Get/{idPosicion}/Competencia/{idCompetencia}/Nivel";
        public const string Posiciones_POST = PREFIX_Posiciones + "/Post";
        public const string Posiciones_PUT = PREFIX_Posiciones + "/Put/{id}";
        public const string PREFIX_AccionesEmpleados = "Optimus/AccionesEmpleados";
        public const string PREFIX_Candidatos = "Optimus/Candidatos";
        public const string PREFIX_CategoriasCompetencias = "Optimus/CategoriasCompetencias";
        public const string PREFIX_Competencias = "Optimus/Competencias";
        public const string PREFIX_Departamentos = "Optimus/Departamentos";
        public const string PREFIX_Empleados = "Optimus/Empleados";
        public const string PREFIX_InformacionesBasicasEmpleados = "Optimus/InformacionesBasicasEmpleados";
        public const string PREFIX_Jornadas = "Optimus/Jornadas";
        public const string PREFIX_Modulos = "Optimus/Modulos";
        public const string PREFIX_Posiciones = "Optimus/Posiciones";
        public const string PREFIX_PropuestasCompensacionesBeneficios = "Optimus/PropuestasCompensacionesBeneficios";
        public const string PREFIX_TiposReportes = "Optimus/TiposReportes";
        public const string PropuestasCompensacionesBeneficios_GET_BY_CANDIDATE = PREFIX_PropuestasCompensacionesBeneficios + "/Get/Candidato/{id}";
        public const string PropuestasCompensacionesBeneficios_GET_BY_ID = PREFIX_PropuestasCompensacionesBeneficios + "/Get/{id}";
        public const string PropuestasCompensacionesBeneficios_POST = PREFIX_PropuestasCompensacionesBeneficios + "/Post";
        public const string PropuestasCompensacionesBeneficios_PUT = PREFIX_PropuestasCompensacionesBeneficios + "/Put/{id}";
        public const string TiposReportes_GET = PREFIX_TiposReportes + "/Get";
        public const string TiposReportes_GET_BY_ID = PREFIX_TiposReportes + "/Get/{id}";

        #endregion Public Fields
    }
}