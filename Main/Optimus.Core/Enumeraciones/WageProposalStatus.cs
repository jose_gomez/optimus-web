﻿namespace Optimus.Core.Enumeraciones
{
    public enum WageProposalStatus
    {
        APROBADO = 27,
        RECHAZADO = 26,
        PENDIENTE = 35
    }
}