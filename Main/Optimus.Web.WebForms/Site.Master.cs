﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.BC.Utilities;
using Optimus.Web.WebForms.GuiHelpers;
using Optimus.Web.WebForms.Security;
using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class SiteMaster : MasterPage
    {
        #region Private Fields

        private const string HTML_URL_TEMPLATE = "<a target=\"_blank\" href=\"NotifyPage.aspx?guid={0}\"> Mas...</a>";
        private ServiceContainer container;
        private UnitOfWork worker;

        #endregion Private Fields

        #region Protected Methods

        protected void IniciarSesion(object sender, EventArgs e)
        {
            CustomMembership a = new CustomMembership();
            string user = string.Empty;
            string pass = string.Empty;
            bool access = false;

            if (!this.Page.User.Identity.IsAuthenticated)
            {
                user = ((TextBox)IdLogin.FindControl("txtUserName")).Text;
                pass = ((TextBox)IdLogin.FindControl("txtPassword")).Text;
                access = a.ValidateUser(user, pass);
            }
            if (access)
            {
                FormsAuthentication.RedirectFromLoginPage(user, true);
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Error!", "Usuario o contraseña incorrecto");
            }
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            var guid = Guid.NewGuid().ToString();
            var user = container.GetUnitOfWork().User.GetUserByUserName(Page.User.Identity.Name);
            string encryptedGuid = EncryptionUtility.Encrypt(guid, user.userID.ToString());
            var resetService = container.GetResetPassHistory();
            resetService.Save(user, guid);
            Response.Redirect(string.Format("/security/FrmResetPass.aspx?UserId={0}&Code={1}", user.userName, Server.UrlEncode(encryptedGuid)));
        }

        protected void Page_Init(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            string path = Request.FilePath;
            if (!this.Page.User.Identity.IsAuthenticated && !NotPermissionPages.notPermissionPages.Contains(path.ToUpper()))
            {
                Response.Redirect("~/Login.aspx", true);
            }
            else if (this.Page.User.Identity.IsAuthenticated && !NotPermissionPages.notPermissionPages.Contains(path.ToUpper()))
            {
                bool HaveAcces = false;
                var urlsPermittedForUser = worker.UserSystemPermissions.GetSystemPermissionByUserID((worker.User.GetUserByUserName(this.Page.User.Identity.Name)).userID);

                if (urlsPermittedForUser.Any(c => c.ConstantName.ToLower() == path.ToLower()))
                {
                    HaveAcces = true;
                }
                if (!HaveAcces) DRL.Utilities.ErrorPage.ErrorPages.RedirectToErrorPage("Usted no tiene permisos suficientes para acceder a esta pagina", true);
            }
            if (this.Page.User.Identity.IsAuthenticated)
            {
                MostrarNotificaciones();
            }
            SetAlertHours();

        }

        protected void SalirSesion(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        #endregion Protected Methods

        #region Private Methods

        private void MostrarNotificacion(DA.SYNotifycation notificacion, string tipoNotificacion, string funtionTemplate, string htmlURL)
        {
            string scriptName = tipoNotificacion + notificacion.NotifycationsID.ToString();
            ScriptManager.RegisterStartupScript(this.Page,
                this.GetType(),
                scriptName,
                string.Format(funtionTemplate, notificacion.Title, htmlURL), true);
        }

        private void MostrarNotificaciones()
        {
            NotificacionesGlobales();
            NotificacionesPersonales();
        }

        private void MostrarNotificacionGlobal(DA.SYNotifycation notificacion)
        {
            const string NOTIFICATION_GLOBAL = "notGlobal";
            string funtionTemplate = "";
            string htmlURL = "";
            switch (notificacion.NotificationTypeID)
            {
                case 1:
                    funtionTemplate = "SuccessNotify('{0}', '{1}');";
                    htmlURL = string.Format(HTML_URL_TEMPLATE, notificacion.NotifycationsID);
                    break;

                case 2:
                    funtionTemplate = "WarningNotify('{0}', '{1}');";
                    htmlURL = string.Format(HTML_URL_TEMPLATE, notificacion.NotifycationsID);
                    break;

                case 3:
                    funtionTemplate = "ErrorNotify('{0}', '{1}');";
                    htmlURL = string.Format(HTML_URL_TEMPLATE, notificacion.NotifycationsID);
                    break;

                default:
                    return;
            }
            MostrarNotificacion(notificacion, NOTIFICATION_GLOBAL, funtionTemplate, htmlURL);
        }

        private void MostrarNotificacionPersonal(DA.SYNotifycation notificacion)
        {
            const string NOTIFICACION_PERSONAL = "notPersonal";
            string funtionTemplate = "";
            string htmlURL = "";
            switch (notificacion.NotificationTypeID)
            {
                case 1:
                    funtionTemplate = "SuccessNotify('{0}', '{1}');";
                    htmlURL = string.Format(HTML_URL_TEMPLATE, notificacion.NotifycationsID);
                    break;

                case 2:
                    funtionTemplate = "WarningNotify('{0}', '{1}');";
                    htmlURL = string.Format(HTML_URL_TEMPLATE, notificacion.NotifycationsID);
                    break;

                case 3:
                    funtionTemplate = "ErrorNotify('{0}', '{1}');";
                    htmlURL = string.Format(HTML_URL_TEMPLATE, notificacion.NotifycationsID);
                    break;

                default:
                    return;
            }
            MostrarNotificacion(notificacion, NOTIFICACION_PERSONAL, funtionTemplate, htmlURL);
        }

        private void NotificacionesGlobales()
        {
            var globalNotifications = container.GetNotificationsService().GetGlobalNotifications();
            foreach (var item in globalNotifications)
            {
                MostrarNotificacionGlobal(item);
            }
        }

        private void NotificacionesPersonales()
        {
            var personalNotifications = container.GetNotificationsService().GetUserNotifications((worker.User.GetUserByUserName(this.Page.User.Identity.Name)).userID);
            foreach (var item in personalNotifications)
            {
                MostrarNotificacionPersonal(item);
            }
        }

        private void SetAlertHours()
        {
            AlertDiv.Visible = false;
            string path = Request.FilePath;
            string Login = "/LOGIN";
            string Solicitud = "/PETITION/SOLICITUDES";
            int horaInt = Convert.ToInt32(DateTime.Now.ToString("HHmm"));
            if (horaInt >= 1100 && horaInt <= 1400)
            {
                if (path.ToUpper() == Login || path.ToUpper() == Solicitud)
                {
                    AlertDiv.Visible = false;
                }
                else
                {
                    AlertDiv.Visible = true;
                }
            }
        }

        #endregion Private Methods
    }
}