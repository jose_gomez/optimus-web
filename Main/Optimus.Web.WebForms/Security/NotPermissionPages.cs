﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Web;

namespace Optimus.Web.WebForms.Security
{
    public class NotPermissionPages
    {
        public static readonly ReadOnlyCollection<string> notPermissionPages = new ReadOnlyCollection<string>
            (new string[] {
                "/DEFAULT",
                "/ABOUT",
                "/CONTACT",
                "/LOGIN",
                "/ERRORPAGES/OOPS",
                "/DEFAULT.ASPX",
                "/PRUEBA",
                "/PRUEBA.ASPX",
                "/INTRANET.ASPX",
                "/Intranet.aspx",
                "/INTRANET",
                "/SECURITY/FRMRESETPASS",
                "/EXTENSIONS/EXTENSIONS",
                "/SECURITY/RECOVERPASSWORD",
                "/PERMISSIONSPETITION/CHEKINGEMPLOYEES",
                "/PETITION/SOLICITUDES",
                "/NOTIFYPAGE"
            });
    }
}