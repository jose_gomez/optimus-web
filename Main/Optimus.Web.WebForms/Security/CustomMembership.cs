﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using DRL.Utilities;
using DRL.Utilities.Encryption;
using Optimus.Web.BC.Repositories;
using Optimus.Web.BC.Services;
using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using System.Web.UI;

namespace Optimus.Web.WebForms.Security
{
    public class CustomMembership : MembershipProvider
    {
        public override string ApplicationName
        {
            get
            {
                throw new NotImplementedException();
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordReset
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool EnablePasswordRetrieval
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MaxInvalidPasswordAttempts
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredNonAlphanumericCharacters
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int MinRequiredPasswordLength
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override int PasswordAttemptWindow
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override MembershipPasswordFormat PasswordFormat
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override string PasswordStrengthRegularExpression
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresQuestionAndAnswer
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool RequiresUniqueEmail
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        public override bool ChangePassword(string username, string oldPassword, string newPassword)
        {
            ServiceContainer container = new ServiceContainer();
            UnitOfWork worker = container.GetUnitOfWork();
            User usuario = worker.User.GetUserByUserName(username);
            var user = worker.User.Find(x => x.UserName == username).FirstOrDefault();
            if(user!= null)
            {
                user.password = newPassword;
                user.LastPasswordChange = DateTime.Now;
                worker.Complete();
                return true;
            }
            return false;

        }

        public override bool ChangePasswordQuestionAndAnswer(string username, string password, string newPasswordQuestion, string newPasswordAnswer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser CreateUser(string username, string password, string email, string passwordQuestion, string passwordAnswer, bool isApproved, object providerUserKey, out MembershipCreateStatus status)
        {
            throw new NotImplementedException();
        }

        public override bool DeleteUser(string username, bool deleteAllRelatedData)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByEmail(string emailToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection FindUsersByName(string usernameToMatch, int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override MembershipUserCollection GetAllUsers(int pageIndex, int pageSize, out int totalRecords)
        {
            throw new NotImplementedException();
        }

        public override int GetNumberOfUsersOnline()
        {
            throw new NotImplementedException();
        }

        public override string GetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(string username, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override MembershipUser GetUser(object providerUserKey, bool userIsOnline)
        {
            throw new NotImplementedException();
        }

        public override string GetUserNameByEmail(string email)
        {
            throw new NotImplementedException();
        }

        public override string ResetPassword(string username, string answer)
        {
            throw new NotImplementedException();
        }

        public override bool UnlockUser(string userName)
        {
            throw new NotImplementedException();
        }

        public override void UpdateUser(MembershipUser user)
        {
            throw new NotImplementedException();
        }

        public override bool ValidateUser(string username, string password)
        {
            ServiceContainer container = new ServiceContainer();
            UnitOfWork worker = container.GetUnitOfWork();
            User usuario = worker.User.GetUserByUserName(username);
            var user = worker.User.Find(x => x.UserName == username).FirstOrDefault();
            if (usuario != null)
            {
                if (CheckPassword(password, usuario.password) && usuario.isEnable)
                {
                    user.LastLogin = DateTime.Now;
                    worker.Complete();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            throw new NotImplementedException();
        }

        private bool CheckPassword(string password, string dbpassword)
        {
            string pass1 = EncryptionUtility.HashPassword256(password);
            string pass2 = dbpassword;
            if (pass1 == pass2)
            {
                return true;
            }
            return false;
        }
    }
}
