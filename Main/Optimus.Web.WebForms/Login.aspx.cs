﻿using Optimus.Web.WebForms.GuiHelpers;
using Optimus.Web.WebForms.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.Security;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class Login : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Session["UserName"] = "";
                Session["Counter"] = 0;
            }
            
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                this.Form.DefaultButton = ((Button)IdLogin.FindControl("btnLogin")).UniqueID;
                var a = Request.QueryString["ReturnUrl"];
            }
            if (this.Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Default.aspx");
            }
        }

        protected void IniciarSesion(object sender, EventArgs e)
        {
            CustomMembership a = new CustomMembership();
            string userName = "";
            string pass;
            bool access = false;

            if (!this.Page.User.Identity.IsAuthenticated)
            {
                userName = ((TextBox)IdLogin.FindControl("txtUsername")).Text;
                pass = ((TextBox)IdLogin.FindControl("txtPassword")).Text;
                if (UsuarioExiste(userName))
                {
                    if (UsuarioActivo(userName))
                    {
                        ValidarCambio(userName);
                        access = a.ValidateUser(userName, pass);
                        if (access)
                        {
                            FormsAuthentication.SetAuthCookie(userName, true);
                            FormsAuthentication.RedirectFromLoginPage(userName, true);
                        }
                        else
                        {
                            if ((string)Session["UserName"] == userName)
                            {
                                Session["Counter"] = (int)Session["Counter"] + 1;
                            }
                            else
                            {
                                Session["UserName"] = userName;
                                Session["Counter"] = 0;
                            }

                            MostrarMensajeError(string.Format("Usuario o contraseña incorrecto, tiene disponible {0} intentos.", (3 - (int)Session["Counter"]).ToString()));
                        }

                        if ((int)Session["Counter"] >= 3)
                        {
                            BC.ServiceContainer container = new BC.ServiceContainer();
                            var usersService = container.GetUnitOfWork();
                            var user = usersService.User.Find(x => x.UserName == userName).FirstOrDefault();
                            user.IsEnabled = false;
                            usersService.Complete();

                            MostrarMensajeError("Usuario bloqueado.");

                            MailAddress address = new MailAddress("IT.Department@Richlinegroup.com");
                            DRL.Utilities.Mail.Mailer.SendMail("Usuario bloqueado", new List<MailAddress> { address },
                                string.Format("El usuario {0} ha sido bloqueado por intentar iniciar sesión varias veces.", user.UserName), false);
                            Session["Counter"] = 0;
                        }
                    }
                    else
                    {
                        MostrarMensajeError("Este usuario no se encuentra activo.");
                    }
                }
                else
                {
                    MostrarMensajeError("Usuario no existe.");
                }
                //log.CreateDate = DateTime.Now;
                //log.Method = "Intento Sesión fallido";
                //log.Message = string.Format("El usuario {0} ha intentado iniciar sesión sin exito.", user.Username);
                //log.AuditFields.OSUser = user.Username;
            }
        }
        protected void MostrarMensajeError(string mensaje)
        {
            List<string> errores = new List<string>();
            errores.Add(string.Format("<b>Error!</b> {0}",mensaje));
            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);
            BootstrapMessage.Attributes["class"] = "alert alert-danger";
        }
        protected void ValidarCambio(string username)
        {
            BC.ServiceContainer container = new BC.ServiceContainer();
            var usersService = container.GetUnitOfWork();
            var configService = container.GetSystemConfigService();
            var configs = configService.GetSysTemConfigByName("PASSWORDDURATION");
            var user = usersService.User.GetUserByUserName(username);
            int days = int.Parse(configs.SysConfigValue);
            if (user.LastPaswordChange.AddDays(days) < DateTime.Now)
            {
                Response.Redirect("~/Security/RecoverPassword?UserID={id}".Replace("{id}", user.userName));
            }
        }

        protected bool UsuarioExiste(string username)
        {
            BC.ServiceContainer container = new BC.ServiceContainer();
            var usersService = container.GetUnitOfWork();
            var user = usersService.User.GetUserByUserName(username);
            return user != null;
        }

        protected bool UsuarioActivo(string username)
        {
            BC.ServiceContainer container = new BC.ServiceContainer();
            var usersService = container.GetUnitOfWork();
            var user = usersService.User.GetUserByUserName(username);
            if (user != null)
            {
                return user.isEnable;
            }
            return false;
        }

        protected void SalirSesion(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }

        protected void LinkButton1_Click(object sender, EventArgs e)
        {
            var userName = ((TextBox)IdLogin.FindControl("txtUsername")).Text;
            if (UsuarioExiste(userName))
            {
                Response.Redirect("~/Security/RecoverPassword?UserID={id}".Replace("{id}", userName));
            }
            if (string.IsNullOrWhiteSpace(userName))
            {
                MostrarMensajeError("Indicar el nombre de usuario.");
            }
            else
            {
                MostrarMensajeError("Usuario no existe.");
            }
        }
    }
}