﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditSearch.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.AuditSearch" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            Criterios de Busqueda
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-body">
            BODY
        </div>

        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            PIE
        </div>

    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            Resultados
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-body">
            <div style="overflow: scroll; height: auto;">
                <asp:GridView ID="dgvAuditList" runat="server" class="table table-striped">
                    <Columns>
                        <asp:BoundField HeaderText="Item" ShowHeader="false" DataField="AuditMasterID"/>
                        <asp:BoundField HeaderText="Departamento" ShowHeader="false" DataField="DepartmentName"/>
                        <asp:BoundField HeaderText="Supervisor" ShowHeader="false" DataField="SupervisorName"/>
                        <asp:BoundField HeaderText="Analista" ShowHeader="false" DataField="AnalistName"/>
                        <asp:BoundField HeaderText="Fecha" ShowHeader="false" DataField="AuditDate"/>
                        <asp:BoundField HeaderText="Auditoria" ShowHeader="false" DataField="AuditName"/>
                    </Columns>
                </asp:GridView>
            </div>
        </div>

        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            Registros = (<asp:Label ID="lbQtyReg" runat="server" Text=""></asp:Label>)
        </div>

    </div>

    
</asp:Content>
