﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Audit.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.Audit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        input, select, textarea {
            max-width: none;
        }

        .estilo {
            border-radius: 5px;
            width: 100%;
            height: 30px;
        }
    </style>

    <script>
        $(document).ready(
            function () {
                $('.FUplAddImg').each(function () {
                    this.onchange = function () {
                        var filesize = this.files[0].size;
                        if (filesize > 1000000) {
                            alert("Elija un archivo menor a 1 MB");
                            (this).value = "";
                        }
                    }
                });
            }
        );
    </script>

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">DATOS DE LA AUDITORIA </a>
                        </h4>

                    </div>
                </div>
            </div>
        </div>

        <div id="collapse1" class="panel-collapse collapse in">
            <div class="panel-body">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">DATOS
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="main row">


                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="Select1">Departamento</label>
                                    <select id="cbDepartment" class="form-control" name="cbDepartment" runat="server">
                                        <option></option>
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtSupervisor">Número del Supervisor</label>
                                    <asp:TextBox CssClass="form-control" ID="txtSupervisor" runat="server" OnTextChanged="txtSupervisor_TextChanged" placeholder="Supervisor" AutoPostBack="true"></asp:TextBox>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtAnalyst">Número del Analista</label>
                                    <asp:TextBox CssClass="form-control" ID="txtAnalyst" runat="server" OnTextChanged="txtAnalyst_TextChanged" placeholder="Analista" AutoPostBack="true"></asp:TextBox>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtFechaAudit">Fecha</label>
                                    <asp:TextBox CssClass="form-control datepicker" ID="txtFechaAudit" runat="server"></asp:TextBox>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtSupervisorName">Nombre del Supervisor</label>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel1" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox CssClass="form-control" ID="txtSupervisorName" runat="server" placeholder="Nombre del Supervisor"></asp:TextBox>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="txtSupervisor" EventName="TextChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="txtAnalistaName">Nombre del Analista</label>
                                    <asp:UpdatePanel runat="server" ID="UpdatePanel2" UpdateMode="Conditional">
                                        <ContentTemplate>
                                            <asp:TextBox CssClass="form-control" ID="txtAnalistaName" runat="server" placeholder="Nombre del Analista"></asp:TextBox>
                                        </ContentTemplate>

                                        <Triggers>
                                            <asp:AsyncPostBackTrigger ControlID="txtAnalyst" EventName="TextChanged" />
                                        </Triggers>
                                    </asp:UpdatePanel>
                                </div>
                            </div>

                            <div class="col-lg-12 col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-sm-12" for="Select2">Auditoria</label>
                                    <select id="cbAuditType" name="cbAuditType" runat="server" class="form-control">
                                        <option></option>
                                    </select>
                                </div>
                            </div>


                        </div>
                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <asp:Button ID="btnIniciarAudit" runat="server" Text="Iniciar Auditoria" class="btn btn-primary" OnClick="btnIniciarAudit_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">CATEGORIAS</a></h4>
                    </div>
                </div>
            </div>
        </div>

        <div id="collapse3" class="panel-collapse collapse in">
            <div class="panel-body">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">SELECCIONAR LA CATEGORIA
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div style="overflow: scroll; height: auto;">
                            <asp:GridView AutoGenerateColumns="false" Width="100%" runat="server" ID="dgvListCalegory" class="table table-striped" OnRowCreated="dgvListCalegory_RowCreated">
                                <Columns>

                                    <asp:BoundField HeaderText="ID" ShowHeader="false" DataField="CategoryID" />
                                    <asp:BoundField HeaderText="Categoria" ShowHeader="false" DataField="Category" />

                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:Button ID="btnSelCategory" runat="server" Text="Iniciar" class="btn btn-default" OnClick="btnSelCategory_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse4">AUDITORIA </a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div id="collapse4" class="panel-collapse collapse in">
            <div class="panel-body">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">INSTRUCCIONES
                        </h4>
                    </div>
                    <div class="panel-body">
                        <asp:Label ID="lbInstrucion" runat="server" Text=""></asp:Label>
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <asp:Label ID="lbAuditType" runat="server" Text=""></asp:Label>
                            &nbsp;( CATEGORIA NÚMERO =
                            <asp:Label ID="lbIdCategory" runat="server" Text="0"></asp:Label>
                            )
                        </h4>
                    </div>
                    <div class="panel-body" id="PruebaGrid">

                        <div style="overflow: scroll; height: auto;">
                            <asp:GridView AutoGenerateColumns="false" Width="100%" runat="server" ID="GridView1" class="table table-striped">
                                <Columns>
                                    <asp:BoundField HeaderText="NO" ShowHeader="false" DataField="Numero" />
                                    <asp:BoundField HeaderText="ELEMENTOS" ShowHeader="false" DataField="AuditIndicator" />
                                    <asp:BoundField HeaderText="ID" ShowHeader="false" DataField="IndicatorID" />

                                    <asp:TemplateField HeaderText="SI">
                                        <ItemTemplate>
                                            <asp:RadioButton type="radio" GroupName='<%# Eval("IndicatorID") %>' ID="radioSI" value="SI" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="NO">
                                        <ItemTemplate>
                                            <asp:RadioButton type="radio" GroupName='<%# Eval("IndicatorID") %>' ID="radioNO" value="NO" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="N/A">
                                        <ItemTemplate>
                                            <asp:RadioButton type="radio" GroupName='<%# Eval("IndicatorID") %>' ID="radioNA" value="N/A" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Observaciones">
                                        <ItemTemplate>
                                            <div class="form-group">
                                                <asp:TextBox ID="txtObservaciones" runat="server" placeholder="Observaciones" TextMode="MultiLine" Height="50"></asp:TextBox>
                                            </div>                                            
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:FileUpload class="FUplAddImg" ID="FUplAddImg" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </div>

                        <div style="overflow: scroll; height: auto;">
                            <asp:GridView AutoGenerateColumns="false" Width="100%" runat="server" ID="GridViewEPP" class="table table-striped">
                                <Columns>
                                    <asp:BoundField HeaderText="NO" ShowHeader="false" DataField="Numero" />
                                    <asp:BoundField HeaderText="ELEMENTOS" ShowHeader="false" DataField="AuditIndicator" />
                                    <asp:BoundField HeaderText="ID" ShowHeader="false" DataField="IndicatorID" />

                                    <asp:TemplateField HeaderText="Condición">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="cbRange" Font-Names="cbRange" runat="server">
                                                <asp:ListItem Value="">Seleccionar</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Uso">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="cbRangeUso" Font-Names="cbRangeUso" runat="server">
                                                <asp:ListItem Value="">Seleccionar</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Observaciones">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObservaciones" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:FileUpload class="FUplAddImg" ID="FUplAddImg" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div style="overflow: scroll; height: auto;">
                            <asp:GridView AutoGenerateColumns="false" Width="100%" runat="server" ID="dgvUsoEvaluation" class="table table-striped">
                                <Columns>
                                    <asp:BoundField HeaderText="NO" ShowHeader="false" DataField="Numero" />
                                    <asp:BoundField HeaderText="ELEMENTOS" ShowHeader="false" DataField="AuditIndicator" />
                                    <asp:BoundField HeaderText="ID" ShowHeader="false" DataField="IndicatorID" />

                                    <asp:TemplateField HeaderText="Uso">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="cbRangeUso" Font-Names="cbRangeUso" runat="server">
                                                <asp:ListItem Value="">Seleccionar</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Observaciones">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObservaciones" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:FileUpload class="FUplAddImg" ID="FUplAddImg" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                        <div style="overflow: scroll; height: auto;">
                            <asp:GridView AutoGenerateColumns="false" Width="100%" runat="server" ID="dgvConditionEvaluation" class="table table-striped">
                                <Columns>
                                    <asp:BoundField HeaderText="NO" ShowHeader="false" DataField="Numero" />
                                    <asp:BoundField HeaderText="ELEMENTOS" ShowHeader="false" DataField="AuditIndicator" />
                                    <asp:BoundField HeaderText="ID" ShowHeader="false" DataField="IndicatorID" />

                                    <asp:TemplateField HeaderText="Condición">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="cbRange" Font-Names="cbRange" runat="server">
                                                <asp:ListItem Value="">Seleccionar</asp:ListItem>
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="Observaciones">
                                        <ItemTemplate>
                                            <asp:TextBox ID="txtObservaciones" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="">
                                        <ItemTemplate>
                                            <asp:FileUpload class="FUplAddImg" ID="FUplAddImg" runat="server" />
                                        </ItemTemplate>
                                    </asp:TemplateField>

                                </Columns>
                            </asp:GridView>
                        </div>

                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <asp:Button ID="btnGuardarAudit" runat="server" Text="Guardar Auditoria" class="btn btn-primary" OnClick="btnGuardarAudit_Click" />
                        <asp:Button ID="btnNextFin" runat="server" Text="Finalizar Auditoria" class="btn btn-primary" OnClick="btnNextFin_Click" />
                    </div>

                </div>

                <div class="panel-footer" style="vertical-align: middle; text-align: right">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
