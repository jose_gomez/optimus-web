﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ProjectDetails.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.ProjectDetails" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" ID="lblTitulo" Font-Size="XX-Large" Font-Bold="true"></asp:Label>
                    </div>
                </div>
                <br />
                <hr />
                <div class="row">
                    <div class="col-md-4">
                        <label style="font-size:large">Departamento:</label>
                        <asp:Label runat="server" ID="DepartamentoLabel"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <label style="font-size:large">Fecha estimada:</label>
                        <asp:Label runat="server" ID="FechaEstimadaLabel"></asp:Label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <label style="font-size:medium">Descripción:</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p runat="server" id="PDescripcion">
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">    
                        <label style="font-size:large">Objetivos/Proyectos:</label><br />                   
                        <asp:ListView runat="server" ID="ListView" >
                            <ItemTemplate>        
                                <span class="glyphicon glyphicon-asterisk" aria-hidden="true"></span>
                                <asp:LinkButton runat="server" Text='<%# Eval("Title")%>' PostBackUrl='<%# String.Format("~/Tickets/ProjectDetails.aspx?ID={0}",Eval("ProjectObjetivesID")) %>'></asp:LinkButton>
                                <br />
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <br />
                <br />
            </div>
        </div>
</asp:Content>
