﻿<%@ Page Title="" Language="C#" EnableEventValidation="false" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tickets.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.Tickets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        @media only screen and (max-width:1000px) {
            .tab-content {
                text-align: center;
                max-width: 760px;
            }
        }

        @media only screen and (min-width:800px) {
            #txtTitulo, #txtDescription {
                min-width: 320px;
            }
        }

        #guardar {
            margin-top: 1%;
        }   

        #statisticsPanel {
            text-align: center;
            clear: both;
            margin-left: 10%;
        }

  #wrap {
    float: left;
    position: relative;
    left: 50%;
}

#chartContainer {
    float: left;
    position: relative;
    left: -50%;
}
    </style>
    <script>

        function setKnob() {
            $(".dial").knob({
                readOnly: true
            });
        }
        function setTicketPercentage() {
            try {
                $.each($(".dial"), function (index, value) {
                    this.value = this.value + '%';
                    this.style.fontSize = '15px';
                });
            }
            catch (e) {
                console.log(e);
            }
        }

    </script>
    <script>

        $(document).ready(function () {
            getDepartmentHead();
            setTicketStatus();
            setPorcentStatus();
            initSample();
            setKnob();
            setTicketPercentage();
            getChartData();
            setPriorityColor();
            setMannager();
            $(document).tooltip({
                content: function () {
                    return $(this).prop('title');
                },
                position: {
                    my: "center bottom-40", // the "anchor point" in the tooltip element
                    at: "center top", // the position of that anchor point relative to selected element
                }
            });
            $('#CheckboxAsoc').click(function () {
                
              // $("#collapseOne").toggle();

                //if ($('#CheckboxAsoc').attr('checked')) {
                //    $("#collapseOne").show();
                //} else {
                //    $("#collapseOne").hide();
                //}

                if (document.getElementById('CheckboxAsoc').checked) {
                    $("#collapseOne").collapse('show');
                 
                    $("#MainContent_ProjectHidden").attr("value", "true");
                } else {
                    $("#collapseOne").collapse('hide');
                    $("#MainContent_ProjectHidden").attr("value", "false");
                }
                
            });
           
        });

        function getChartData() {
            $('#imgModal').modal();
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetAllTimeTicketsLeaderBoard1',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#empdetails').empty();
                    if (data.d) {

                        loadChartData(data.d);
                    }
                    else {

                    }
                    $('#imgModal').modal('hide');   
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }
        function loadChartData(dataPoint) {
            var chart = new CanvasJS.Chart("chartContainer",
                {
                    theme: "theme2",
                    title: {
                        text: "Ranking de tickets validados"
                    },
                    width: 900,
                    height:380,
                    data: [

                    {
                        type: "column",
                        dataPoints: dataPoint
                    }
                    ]
                });

            chart.render();
        }
        function setPorcentStatus() {
            $('.progress-bar').each(function () {
                var status = $(this).text();
                if (status.trim() == "100%") {
                    $(this).addClass('progress-bar-success');
                }
            });

        }
        function GuardarTickets() {
            var comment = CKEDITOR.instances.editor.getData();
            __doPostBack('GuardarTickets', comment)

        }
        function setTicketStatus() {
            $('.submitted').each(function () {
                var estatus = $(this).text();
                switch (estatus) {
                    case "Sometido": {
                        $(this).addClass('btn btn-default');
                    } break;
                    case "Cancelled": {
                        $(this).addClass('btn btn-danger');
                    } break;
                    case "Validate": {
                        $(this).addClass('btn btn-success');
                    } break;
                    case "in process": {
                        $(this).addClass('btn btn-info');
                    } break;
                    case "Done": {
                        $(this).addClass('btn btn-primary');
                    } break;
                    default: {
                        $(this).addClass('btn btn-default');
                    } break;
                }
            });
        }
        function ModalCompletalPerfil() {
            $('#CompletarUsuario').modal({ backdrop: 'static', keyboard: false });

        }
        function getDepartmentHead() {

       
            $('#imgModal').modal();
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetDepartmentHead',
                data: '{Department:\'' + $('#MainContent_drpDepartamento option:selected').text() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#empdetails').empty();
                    if (data.d) {

                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                        $('#empdetails').append("<img alt='Encargado' id='imgPic' />");

                        document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                    }
                    else {
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'> N/A </small> </h5>");
                    }
                    $('#imgModal').modal('hide');
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }
            });
            getproblem();
        }

        function getproblem() {
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/getProblemByDept',
                data: '{Department:\'' + $('#MainContent_drpDepartamento option:selected').text() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#MainContent_drpProblem').empty().append('<option selected="selected" value="0">Seleccione</option>');
                    $.each(data.d, function () {
                        $('#MainContent_drpProblem').append($("<option></option>").val(this['ProblemTypeID']).html(this['Name']));
                    });
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }
            });
        }
        function setProblemHidden() {
            var a = $('#MainContent_drpProblem option:selected').val();
            $('#MainContent_HiddenProblem').attr("value", a);
        }
        $(function () {
            // for bootstrap 3 use 'shown.bs.tab', for bootstrap 2 use 'shown' in the next line
            $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
                // save the latest tab; use cookies if you like 'em better:
                localStorage.setItem('lastTab', $(this).attr('href'));
            });

            // go to the latest tab, if it exists:
            var lastTab = localStorage.getItem('lastTab');
            if (lastTab) {
                $('[href="' + lastTab + '"]').click();
            }
        });
        function setPriorityColor() {
            $('.prioridadLabel').each(function () {
                var priority = $(this).text();
                switch (priority.trim()) {

                    case "Baja": {

                        $(this).css({ 'color': 'orange' });
                    } break;

                    case "Normal": {
                        $(this).css({ 'color': 'green' });
                    } break;
                    case "Alta": {
                        $(this).css({ 'color': 'red' });
                    } break;
                }
            });
        }
        function setMannager() {
            if ($('#MainContent_isMngHidden').val() == 'True') {
                $("#CheckboxAsoc").removeAttr("disabled");
            } else {
                $('#CheckboxAsoc').attr("disabled", true);
            }
        }
    </script>
    <div id="bs">
    </div>
    <div class="panel panel-default">
        <asp:HiddenField runat="server" ID="isMngHidden" />
        <div class="panel-heading">
            <h4 class="panel-title">
                <a class="" id="anchrestadisticas" data-toggle="collapse" data-parent="#accordion" href="#estadisticas" aria-expanded="true">Estadisticas</a>
            </h4>
        </div>

        <div id="estadisticas" styl class="panel-collapse collapsed collapse in" aria-expanded="true">
            <div class="panel-body">
                <div role="tabpanel">
                    <div class="row">
                        <div id="statisticsPills">
                            <ul class="nav nav-pills nav-justified">
                                <li class="active"><a data-toggle="tab" role="tab" href="#statisticsPanel">Estadisticas</a></li>
                                <li><a data-toggle="tab" role="tab"  href="#rendimiento">Rendimiento</a></li>
                            </ul>
                        </div>
                        <div class="tab-content">
                            <div class=" row tab-pane fade in active" id="statisticsPanel">
                                <div class="col-md-2">
                                    <div class="row">
                                        Tickets Validados 
                                    </div>
                                    <div class="row" style="pointer-events: none">
                                        <input type="text" readonly="readonly" data-readonly="true" id="validatedTicketsPercentage" runat="server" data-fgcolor="green" value="0" data-min="0" data-max="100" data-width="100" data-height="150" class="dial">
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        Tickets pendientes
                                    </div>
                                    <div class="row" style="pointer-events: none">
                                        <input type="text" data-readonly="true" id="pendingTicketsPercentage" runat="server" value="0" data-width="100" data-height="150" class="dial">
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        Tickets Abiertos 
                                    </div>
                                    <div class="row" style="pointer-events: none">
                                        <input type="text" data-readonly="true" id="ticketsAbiertos" data-fgcolor="#3399ff" runat="server" value="0" data-width="100" data-height="150" class="dial">
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        Tickets Listos
                                    </div>
                                    <div class="row" style="pointer-events: none">
                                        <input type="text" data-readonly="true" id="ticketsListos" data-fgcolor="#0066ff" runat="server" value="0" data-width="100" data-height="150" class="dial">
                                    </div>

                                </div>
                                <div class="col-md-2">
                                    <div class="row">
                                        Tickets Cancelados 
                                    </div>
                                    <div class="row" style="pointer-events: none">
                                        <input type="text" data-readonly="true" id="ticketsCancelados" data-fgcolor="#ff0000" runat="server" value="0" data-width="100" data-height="150" class="dial">
                                    </div>

                                </div>
                            </div>
                            <div class="row tab-pane fade" style="height: 360px;"  id="rendimiento">
                                <div id="wrap">
                                         <div id="chartContainer" style="width:1000px;margin-left:1%" ></div>
                                </div>
                           
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <asp:TextBox ID="text" runat="server" ClientIDMode="Static" value="button" Style="display: none;"></asp:TextBox>
    <asp:Button ID="button" runat="server" ClientIDMode="Static"
        Style="display: none;" />

    <div id="Tabs" role="tabpanel">
        <div class="row">
            <div id="ticketscontainer">
                <h1>Tickets Dashboard</h1>
                <label id="lblEtiqueta"></label>
                <ul class="nav nav-pills nav-justified" id="myTab">
                    <li><a data-toggle="tab" role="tab" href="#someter">Someter Ticket</a></li>
                    <li class="active"><a data-toggle="tab" role="tab" href="#mistickets">Tickets Enviados</a></li>
                    <li><a data-toggle="tab" role="tab" href="#pendientes">Tickets sin Asignación</a></li>
                    <li><a data-toggle="tab" role="tab" href="#Asignados">Tickets asignados a mi </a></li>
                    <li><a data-toggle="tab" role="tab" href="#pendientesAprobacion">Tickets pendientes de aprobación</a></li>
                    <li><a data-toggle="tab" role="tab" href="#Cerrados">Tickets Cerrados</a></li>
                    <li><a data-toggle="tab" role="tab" href="#Todos">Todos los Tickets</a></li>
                </ul>
            </div>
            <asp:HiddenField ID="TabName" runat="server" Value="someter" />
            <div class="tab-content">
                <div id="someter" role="tabpanel" class="tab-pane fade">
                    <h3>Someter ticket</h3>
                    <div id="deptcol" class="col-md-6">
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Departamento:" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="drpDepartamento" CssClass="form-control" onchange="return getDepartmentHead()"  runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Categoría:" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="drpProblem" CssClass="form-control" onchange="return setProblemHidden()" runat="server"></asp:DropDownList>
                                <asp:HiddenField ID="HiddenProblem" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Prioridad:" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="PriodidadDropDown" CssClass="form-control" runat="server"></asp:DropDownList>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input id="CheckboxAsoc"  type="checkbox" /><label>Asociar a un proyecto</label>
                                <asp:HiddenField ID="ProjectHidden" runat="server" />
                            </div>
                        </div>                        
                        <div id="collapseOne" class="collapse">
                        <div class="row">
                            <div class="form-group">
                               <asp:Label runat="server" Text="Proyecto" Font-Bold="true"></asp:Label>
                                <asp:DropDownList ID="ProyectoLabel" CssClass="form-control" runat="server"></asp:DropDownList>
                                </div>
                                
                            </div>
                                
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Título / Solicitud" Font-Bold="true"></asp:Label>
                                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtTitulo" CssClass="form-control" placeholder="Ingrese una solicitud"></asp:TextBox>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Text="Descripción" Font-Bold="true"></asp:Label>
                                <%--<asp:TextBox runat="server" ClientIDMode="Static" ID="txtDescription" Style="min-width: 90%" CssClass="form-control" placeholder="Ingrese una descripción" TextMode="MultiLine"></asp:TextBox>--%>
                                <div class="adjoined-bottom">
                                    <div class="grid-container">
                                        <div class="grid-width-100">
                                            <div id="editor">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row" id="guardar">
                            <div class="form-group">
                                <asp:Button runat="server" ID="btnGuardar" Text="Someter Tickets" class="btn btn-primary" ClientIDMode="Static" onsubmit="return validate()" OnClientClick="GuardarTickets()" />
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="empdetails">
                        </div>
                    </div>
                </div>
                <div id="mistickets" role="tabpanel" class="tab-pane fade in active">
                    <h3>Tickets Enviados</h3>
                    <div class="container">

                        <div class="row">
                            <asp:Label Text="Filtrar por Estado:" runat="server" Font-Bold="true"></asp:Label>
                            <asp:DropDownList runat="server" ID="FiltroMisTickets" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="CambioFiltro">
                                <asp:ListItem Value="0">Todos</asp:ListItem>
                                <asp:ListItem Value="1" Selected="true">Sin Terminar</asp:ListItem>
                                <asp:ListItem Value="2">Terminado</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ListView ID="lstTickets" runat="server" OnPagePropertiesChanging="lstTickets_PagePropertiesChanging">
                                <LayoutTemplate>
                                    <div class="table-responsive">
                                        <table  class="table  table-striped">
                                            <thead>
                                                <th>Fecha</th>
                                                <th>Titulo</th>
                                                <th>#Ticket</th>
                                                <th>Categoría</th>
                                                <th>Departamento</th>
                                                <th>Porcentaje</th>
                                                <th>Estado</th>
                                                <th>Asignado a:</th>
                                            </thead>
                                            <tbody class="myTable">
                                                <tr runat="server" id="itemPlaceholder" />
                                            </tbody>
                                        </table>
                                    </div>
                                    <asp:DataPager ID="TicketsPager" runat="server" PagedControlID="lstTickets" PageSize="20">
                                        <Fields>
                                            <asp:NumericPagerField ButtonCount="10"
                                                PreviousPageText="<--"
                                                NextPageText="-->" />
                                        </Fields>
                                    </asp:DataPager>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr runat="server">
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                        <td>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                           <td>
                                        <asp:Label runat="server" Text='<%# Eval("TipoProblema") %>'></asp:Label>
                                    </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="<%#Eval("PorcentProgress ")%>" aria-valuemin="0" aria-valuemax="100" style='width: <%#Eval("PorcentProgress")+"%" %>'>
                                                    <%#Eval("PorcentProgress")+"%"%>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Responsible") %>'></asp:Label></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                <div id="pendientes" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets Pendientes de Asignación</h3>
                    <div class="row">
                        <asp:ListView ID="lstTicketsPendientes" ClientIDMode="Static" runat="server" OnPagePropertiesChanging="lstTicketsPendientes_PagePropertiesChanging">
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Categoría</th>
                                        <th>Prioridad</th>
                                        <th>Departamento</th>
                                        <th>Auto Asignar</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="PendientesPager" runat="server" PagedControlID="lstTicketsPendientes" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr runat="server" title='<%# "<div ><p style=\"font-weight:bold\">Submitted by:<p>" + Eval("SubmitUserName").ToString()  +"</div> </br> <div  >"+ HttpUtility.HtmlDecode(Eval("Description").ToString()) + "</div>" %>'>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server"  Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label>
                                    </td>
                                     <td>
                                        <asp:Label runat="server" Text='<%# Eval("TipoProblema") %>'></asp:Label>
                                    </td>
                                   
                                    <td>
                                        <asp:Label runat="server" CssClass="prioridadLabel" Text='<%# Eval("Priority")%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Button Text="Tomar" CssClass="btn btn-primary" Visible='<%# !bool.Parse(Eval("Requisition").ToString()) %>' runat="server" CommandName='<%# Eval("TicketNumber") %>' OnCommand="AutoAsignar" />
                                    </td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div id="Asignados" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets asignados a mi</h3>
                    <div class="row">
                        <asp:ListView ID="lstTicketsAsignados" ClientIDMode="Static" OnPagePropertiesChanging="lstTicketsAsignados_PagePropertiesChanging" runat="server">
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Categoría</th>
                                        <th>Departamento</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="AsignadosPager" runat="server" PagedControlID="lstTicketsAsignados" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr runat="server">
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'> </asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                       <td>
                                        <asp:Label runat="server" Text='<%# Eval("TipoProblema") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>

                 <div id="pendientesAprobacion" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets para aprobar</h3>
                    <div class="row">
                        <asp:ListView ID="ToprovedListview" ClientIDMode="Static" runat="server" >
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Categoría</th>
                                        <th>Prioridad</th>
                                        <th>Departamento</th>                                   
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="PendientesPager" runat="server" PagedControlID="lstTicketsPendientes" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server"  Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label>
                                    </td>
                                     <td>
                                        <asp:Label runat="server" Text='<%# Eval("TipoProblema") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" CssClass="prioridadLabel" Text='<%# Eval("Priority")%>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Button Text="Tomar" CssClass="btn btn-primary" Visible='<%# !bool.Parse(Eval("Requisition").ToString()) %>' runat="server" CommandName='<%# Eval("TicketNumber") %>' OnCommand="AutoAsignar" />
                                    </td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div id="Cerrados" role="tabpanel" class="tab-pane fade">
                    <h3>Tickets Cerrados</h3>
                    <div class="row">
                        <asp:ListView ID="lstTicketsCerrados" ClientIDMode="Static" runat="server" OnPagePropertiesChanging="lstTicketsCerrados_PagePropertiesChanging">
                            <LayoutTemplate>
                                <table class="table table-striped">
                                    <thead>
                                        <th>Fecha</th>
                                        <th>Titulo</th>
                                        <th>#Ticket</th>
                                        <th>Categoría</th>
                                        <th>Departamento</th>
                                        <th>Estado</th>
                                    </thead>
                                    <tbody>
                                        <tr runat="server" id="itemPlaceholder" />
                                    </tbody>
                                </table>
                                <asp:DataPager ID="CerradosPager" runat="server" PagedControlID="lstTicketsCerrados" PageSize="20">
                                    <Fields>
                                        <asp:NumericPagerField ButtonCount="10"
                                            PreviousPageText="<--"
                                            NextPageText="-->" />
                                    </Fields>
                                </asp:DataPager>
                            </LayoutTemplate>
                            <ItemTemplate>
                                <tr runat="server">
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'> </asp:Label></td>
                                    <td>
                                        <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                       <td>
                                        <asp:Label runat="server" Text='<%# Eval("TipoProblema") %>'></asp:Label>
                                    </td>
                                    <td>
                                        <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                    <td>
                                        <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                </tr>
                            </ItemTemplate>
                        </asp:ListView>
                    </div>
                </div>
                <div id="Todos" role="tabpanel" class="tab-pane fade">
                    <h3>Todos los Tickets</h3>
                    <div class="container">
                        <div class="row">
                            <div class="row">
                                <div class="col-md-4">
                                    <asp:Label Text="Filtrar por Estado:" runat="server" Font-Bold="true"></asp:Label>
                                    <asp:DropDownList runat="server" ID="FiltroTodos" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="CambioFiltroTodos">
                                        <asp:ListItem Value="0">Todos</asp:ListItem>
                                        <asp:ListItem Value="1">Tickest enviados del departamento</asp:ListItem>
                                        <asp:ListItem Value="2">Tickest recibidos del departamento</asp:ListItem>
                                        <asp:ListItem Value="3">Todos mis Tickets</asp:ListItem>
                                        <asp:ListItem Value="4">Todos los Tickets sin terminar</asp:ListItem>
                                        <asp:ListItem Value="5">Todos terminados no validados</asp:ListItem>
                                        <asp:ListItem Value="6">Todos los Tickets Cancelados</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-md-4">                             
                                    <asp:TextBox runat="server" ID="BuscarID" Width="80px"  CssClass="form-control col-md-2" MaxLength="5"></asp:TextBox>
                                      <button type="button" class="btn btn-default btn-sm" runat="server" onserverclick="BuscarByID">
                                        <span class="glyphicon glyphicon-search"></span>Buscar por ID
                                    </button>
                                </div>
                                <div class="col-md-4">
                                      <asp:TextBox runat="server" ID="KeyWordTextBox"  CssClass="form-control col-md-2"></asp:TextBox>
                                      <button type="button" class="btn btn-default btn-sm" runat="server" onserverclick="BuscarPalabraClave">
                                        <span class="glyphicon glyphicon-search"></span>Buscar 
                                    </button>
                                </div>
                                </div>
                            </div>
                            <asp:ListView ID="lstTodosTickets" runat="server" OnPagePropertiesChanging="lstTodosTickets_PagePropertiesChanging">
                                <LayoutTemplate>
                                    <div class="table-responsive">
                                        <table class="table  table-striped">
                                            <thead>
                                                <th>Fecha</th>
                                                <th>Titulo</th>
                                                <th>#Ticket</th>
                                                <th>Categoría</th>
                                                <th>Departamento</th>
                                                <th>Porcentaje</th>
                                                <th>Estado</th>
                                            </thead>
                                            <tbody class="myTable">
                                                <tr runat="server" id="itemPlaceholder" />
                                            </tbody>
                                        </table>
                                    </div>
                                    <asp:DataPager ID="TodosPager" runat="server" PagedControlID="lstTodosTickets" PageSize="20">
                                        <Fields>
                                            <asp:NumericPagerField ButtonCount="10"
                                                PreviousPageText="<--"
                                                NextPageText="-->" />
                                        </Fields>
                                    </asp:DataPager>
                                </LayoutTemplate>
                                <ItemTemplate>
                                    <tr runat="server">
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("SubmitDate","{0:dd/M/yyyy}") %>'></asp:Label></td>
                                        <td>
                                            <asp:LinkButton runat="server" Text='<%# Eval("Title") %>' PostBackUrl='<%# String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", Eval("TicketNumber")) %>'></asp:LinkButton></td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("TicketNumber") %>'></asp:Label></td>
                                           <td>
                                        <asp:Label runat="server" Text='<%# Eval("TipoProblema") %>'></asp:Label>
                                    </td>
                                        <td>
                                            <asp:Label runat="server" Text='<%# Eval("Department") %>'></asp:Label></td>
                                        <td>
                                            <div class="progress">
                                                <div class="progress-bar progress-bar-striped active" role="progressbar"
                                                    aria-valuenow="<%#Eval("PorcentProgress ")%>" aria-valuemin="0" aria-valuemax="100" style='width: <%#Eval("PorcentProgress")+"%" %>'>
                                                    <%#Eval("PorcentProgress")+"%"%>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <asp:Label class="submitted" runat="server" Text='<%# Eval("Status") %>'></asp:Label></td>
                                    </tr>
                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
   
    <div class="modal fade" id="imgModal" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Loading...</h1>
                </div>
                <div class="modal-body">
                    <div class="progress progress-striped active">
                        <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
                    </div>
                </div>
                <div class="modal-footer">
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" id="CompletarUsuario">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Complete su perfil de usuario</h4>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Numero de empleado:</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="EmployeeIDTextBox"></asp:TextBox>                    
                    </div>
                    <div class="form-group">
                        <label for="recipient-name" class="control-label">Correo:</label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="CorreoTextBox"></asp:TextBox>
                        <p class="text-warning"><small>Debe completar su perfil de usuario para usar el sistema de tickets correctamente</small></p>
                    </div>
                </div>
                <div class="modal-footer">
                    <asp:Button runat="server" class="btn btn-primary" Text="Completar" OnClick="CompletarPerfil" />
                </div>
            </div>
        </div>

    </div>
    <div style="display: none">
        <div id="editor2" style="display: none"></div>
    </div>
    <script src="/Scripts/canvasjs.min.js"></script>
</asp:Content>
