﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarAnalisisPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarAnalisisPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Puesto</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Nombre de la posición:</h4>
                </div>
                <div class="col-xs-12">
                    <asp:Label ID="lblPuestoName" Text="Nombre del puesto" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
            <h4 class="col-xs-12">Función:</h4>
            <asp:TextBox ID="txtFuncion" CssClass="form-control col-xs-12" runat="server" AutoPostBack="True" Width="100%" OnTextChanged="TextBox1_TextChanged"></asp:TextBox>
            <h4 class="col-xs-12">Que hace?</h4>
            <asp:TextBox ID="txtQueHace" CssClass="form-control col-xs-12" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            <h4 class="col-xs-12">Como lo hace?:</h4>
            <asp:TextBox ID="txtComoHace" CssClass="form-control col-xs-12" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            <h4 class="col-xs-12">Para que lo hace?</h4>
            <asp:TextBox ID="txtParaHace" CssClass="form-control col-xs-12" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
            <h4 class="col-xs-12">Que tiempo se toma para hacerlo?</h4>
            <asp:TextBox ID="txtQueTiempo" CssClass="form-control col-xs-12" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
        </div>
        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSave_Click" />
            <asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnDelete_Click" />
            <asp:Button ID="btnCancel" CssClass="btn btn-default" runat="server" Text="Cancelar" OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
