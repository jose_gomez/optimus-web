﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarCompetenciasPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarCompetenciasPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Competencias del puesto</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Nombre de la posición:</h4>
                </div>
                <div class="col-xs-12">
                    <asp:Label ID="lblPuestoName" Text="Nombre del puesto" runat="server" />
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="col-xs-12">
                    <h4>Categorías:</h4>
                </div>
                <div class="col-xs-12">
                    <asp:DropDownList ID="DropDownList1" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h4>Competencias:</h4>
                </div>
                <div class="col-xs-12">
                    <asp:DropDownList ID="DropDownList2" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList2_SelectedIndexChanged"></asp:DropDownList>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-12">
                    <h4>Nivel:</h4>
                </div>
                <div class="col-xs-12">
                    <asp:TextBox ID="TextBox3" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
        </div>
        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            <asp:Button ID="Button1" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="Button1_Click" />
            <asp:Button ID="Button3" CssClass="btn btn-danger" runat="server" Text="Eliminar" OnClick="Button3_Click" />
            <asp:Button ID="Button2" CssClass="btn btn-default" runat="server" Text="Cancelar" OnClick="Button2_Click" />
        </div>
    </div>
</asp:Content>
