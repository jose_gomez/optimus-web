﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Capacitaciones.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.Capacitaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/cstm-bootstrap-tree.css" rel="stylesheet" />
    <script src="../Scripts/cstm-bootstrap-tree.js"></script>
    <div class="panel panel-default">

        <div class="panel-heading">
            <h2>Capacitaciones</h2>
        </div>

        <div class="panel-body">
            <h3>En Proceso</h3>
            <div id="Div1" runat="server"></div>
            <h3>Próximas capacitaciones agendadas</h3>
            <div id="Div2" runat="server"></div>
        </div>

    </div>
</asp:Content>
