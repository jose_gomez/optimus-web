﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RegistrarPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.RegistrarPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 id="title" runat="server">Puesto</h3>
        </div>
        <div class="panel-body">
            <h4 class="col-xs-12">Categoría:</h4>
            <asp:DropDownList ID="ddlCategories" runat="server" CssClass="form-control col-xs-12"></asp:DropDownList>
            <h4 class="col-xs-12">Nivel:</h4>
            <asp:DropDownList ID="ddlOrganizationLevels" runat="server" CssClass="form-control col-xs-12" OnSelectedIndexChanged="ddlOrganizationLevels_SelectedIndexChanged" AutoPostBack="True"></asp:DropDownList>
            <h4 class="col-xs-12">Departamento:</h4>
            <asp:DropDownList ID="ddlDepartments" runat="server" CssClass="form-control col-xs-12" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartments_SelectedIndexChanged"></asp:DropDownList>
            <h4 class="col-xs-12">Superior:</h4>
            <asp:DropDownList ID="ddlSuperiorPositions" runat="server" CssClass="form-control col-xs-12"></asp:DropDownList>
            <h4 class="col-xs-12">Nombre:</h4>
            <asp:TextBox ID="txtName" CssClass="form-control col-xs-12" runat="server"></asp:TextBox>
            <h4 class="col-xs-12">Descripción:</h4>
            <asp:TextBox ID="txtDescription" CssClass="form-control col-xs-12" runat="server" TextMode="MultiLine"></asp:TextBox>
        </div>
        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSave_Click" />
            <asp:Button ID="btnCancel" CssClass="btn btn-default" runat="server" Text="Cancelar" OnClick="btnCancel_Click" />
        </div>
    </div>
</asp:Content>
