﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarPropuesta.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarPropuesta" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-xs-12 {
            max-width: 100%;
        }
    </style>
    <script type="text/javascript">
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode != 46)
                return false;
            return true;
        }
    </script>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Propuesta
            </h3>
        </div>
        <div class="panel-body">
            <h4 class="col-xs-12">Posición disponible:</h4>
            <asp:DropDownList ID="ddlPositions" runat="server" CssClass="form-control col-xs-12"></asp:DropDownList>
            <h4 class="col-xs-12">Propuesta:</h4>
            <input id="txtProposal" type="text" onkeypress="return isNumberKey(event)" runat="server" class="form-control col-xs-12" />
        </div>
        <div class="panel-footer">
            <div class="btn-group" style="text-align: right; vertical-align: middle;">
                <button id="bttnA" type="button" runat="server" class="btn btn-success" data-toggle="modal" data-target="#myModal">Aprobar</button>
                <asp:Button ID="bttnRechazar" runat="server" Text="Rechazar" CssClass="btn btn-danger" OnClick="bttnRechazar_Click" />
                <asp:Button ID="bttnAceptar" runat="server" Text="Aceptar" CssClass="btn btn-primary" OnClick="bttnAceptar_Click" />
                <asp:Button ID="bttnCancelar" runat="server" Text="Cancelar" CssClass="btn btn-default" OnClick="bttnCancelar_Click" />
            </div>
        </div>
    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Aprobación de propuesta</h4>
                </div>
                <div class="modal-body">
                    <p>Fecha de entrada del empleado.</p>
                    <asp:TextBox runat="server" class="form-control datepicker" ID="txtFechaInicioLabor" />
                </div>
                <div class="modal-footer">
                    <div class="btn-group" style="text-align: right; vertical-align: middle;">
                        <button id="Button2" type="button" runat="server" class="btn btn-success" data-dismiss="modal" onclick="javascript:__doPostBack('Aprobar','')">Aprobar</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>