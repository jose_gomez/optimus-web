﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Intranet.aspx.cs" Inherits="Optimus.Web.WebForms.Intranet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        @media screen and (min-width: 992px) {

            #menu {
                position: fixed;
            }

            #MenuScrollspy {
                display: block;
            }
        }

        @media screen and (max-width: 991px) {

            #menu {
                position: inherit;
            }

            #MenuScrollspy {
                display: none;
            }
        }
    </style>
    <body>
        <div class="container">
            <div class="col-sm-12 col-md-3">
                <div id="menu">
                    <div id="MenuScrollspy" class="panel">
                        <nav id="myScrollspy">
                            <ul class="nav nav-pills nav-stacked">
                                <li><a href="#section1">Recursos Humanos</a></li>
                                <li><a href="#section2">Seguridad e Higiene</a></li>
                                <li><a href="#section3">Reportes</a></li>
                                <li><a href="#section3">JTS</a></li>
                                <li><a href="#section5">Sistemas</a></li>
                            </ul>
                        </nav>
                    </div>
                    <div id="Favoritos" class="panel panel-default">
                        <div class="panel-heading">
                            Acceso rápido
                        </div>
                        <div class="panel-body" style="font-size: medium;">
                            <ul>
                                <li><a href="/tickets/tickets.aspx">Tickets</a></li>
                                <li><a href="CheckList/Audit.aspx">Auditoria</a></li>
                                <li><a runat="server" href="/AccessPetition/AccessPetition.aspx">Solicitud de acceso</a></li>
                                <li><a runat="server" href="/tickets/Dashboard.aspx">Dashboard de estadisticas</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12 col-md-9">
                <div id="section1">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Conference-40.png" class="img-rounded" alt="Recursos Humanos">
                            Recursos Humanos.
                        </div>
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Servicios-40.png" class="img-rounded" alt="Servicios" />
                                    Servicios
                                </div>
                                <div class="panel-body list-group">
                                    <a href="PermissionsPetition/PermissionForm.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Solicitud de permiso</a>
                                    <a href="PermissionsPetition/ComplaintForm.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Formulario de reclamación</a>
                                    <a href="ExtraHours/ExtraHours.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Listado de horas extras y cena / almuerzo</a>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Enviar%20currículum-40.png" class="img-rounded" alt="Reclutamiento y selección" />
                                    Reclutamiento y selección
                                </div>
                                <div class="panel-body list-group">
                                    <a href="HHRR/JobApplicants.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Banco de candidatos</a>
                                    <a href="HHRR/RequisicionPersonal.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Requisición de personal</a>
                                    <a href="HHRR/ListaRequisicionesPersonal.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Requisiciones de personal</a>
                                    <asp:LinkButton ID="lnkSolicitar" OnClick="lnkSolicitar_Click" runat="server" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Solicitud de empleo</asp:LinkButton>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Formación-40.png" class="img-rounded" alt="Capacitación y entrenamiento" />
                                    Capacitación y entrenamiento
                                </div>
                                <div class="panel-body list-group">
                                    <a href="HHRR/Capacitaciones.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Capacitaciones</a>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Apoyo-40.png" class="img-rounded" alt="Administración" />
                                    Administración
                                </div>
                                <div class="panel-body list-group">
                                    <a href="PermissionsPetition/PermissionsPetitionAprobation.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Aprobación de permisos</a>
                                    <a href="ExtraHours/ExtraHoursManager.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Aprobación Listado horas Extra</a>
                                    <a href="HHRR/AdministrarCompensacionesBeneficios.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Compensaciones y beneficios</a>
                                    <a href="HHRR/AdministrarCompetencias.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Competencias</a>
                                    <a href="HHRR/AdministrarNivelesSalarial.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Niveles salariales</a>
                                    <a href="Tickets/ProjectAndObjective.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Objetivos y proyectos</a>
                                    <a href="HHRR/ListaPuesto.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Puestos</a>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Documentos-40.png" class="img-rounded" alt="Reportes" />
                                    Reportes
                                </div>
                                <div class="panel-body list-group">
                                    <asp:HyperLink ID="LinkReporteCumpleano" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Listado cumpleaños por mes</asp:HyperLink>
                                    <asp:HyperLink ID="LinkPerfilCompetenciasLaborales" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Perfil de competencias laborales</asp:HyperLink>
                                    <asp:HyperLink ID="LinkCalificacion" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Resultados de evaluaciones</asp:HyperLink>
                                    <asp:HyperLink ID="LinkSolicitudesByCedula" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Solicitud laboral por cédula</asp:HyperLink>
                                    <asp:HyperLink ID="LinkSolicitudesMasivoByRangoFecha" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Solicitud laboral por rango de fecha</asp:HyperLink>
                                    <asp:HyperLink ID="LinkListadosSolicitudes" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Solicitud laboral por rango de fecha y posición</asp:HyperLink>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="section2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Biohazard-40.png" class="img-rounded" alt="Seguridad e Higiene" />
                            Seguridad e Higiene
                        </div>
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Examen-40.png" class="img-rounded" alt="Auditorias" />
                                    Auditoria
                                </div>
                                <div class="panel-body list-group">
                                    <a href="CheckList/Audit.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Auditorias</a>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Apoyo-40.png" class="img-rounded" alt="Administración" />
                                    Administración
                                </div>
                                <div class="panel-body list-group">
                                    <a href="CheckList/SettingAudit.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Administración de auditorias</a>
                                    <a href="CheckList/AuditCategories.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Administración de categorías</a>
                                    <a href="CheckList/AuditIndicator.aspx" class="list-group-item list-group-item-action">
                                        <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Administración de indicadores</a>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <img src="images/Icon/Documentos-40.png" class="img-rounded" alt="Reportes" />
                                    Reportes
                                </div>
                                <div class="panel-body list-group">
                                    <asp:LinkButton runat="server" OnClick="RedirectAuditFecha" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reporte auditorias por fechas</asp:LinkButton>
                                    <asp:HyperLink ID="LinkReporteGeneralAuditoria" runat="server" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reporte general de auditoria</asp:HyperLink>
                                    <asp:LinkButton runat="server" OnClick="RedirectAudit" class="list-group-item list-group-item-action">
                                                    <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reporte semanal de auditorias</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="section3">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Document-40.png" class="img-rounded" alt="Reportes">
                            Reportes
                        </div>
                        <div class="panel-body list-group">
                            <asp:HyperLink ID="LinkDetalleTrabajos" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Detalle de trabajos</asp:HyperLink>
                            <asp:HyperLink ID="LinkEficiencia" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Eficiencia</asp:HyperLink>

                            <asp:HyperLink ID="LinkPendienteParcializarPorSemanas" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Pendiente parcializar por semanas</asp:HyperLink>
                            <asp:HyperLink ID="LinkProcesadoEnCadena" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Procesado en cadena</asp:HyperLink>
                            <asp:HyperLink ID="LinkProcesadoPorSemanas" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Procesado por semanas</asp:HyperLink>
                            <asp:HyperLink ID="LinkPagoDeProduccion" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Pago de producción</asp:HyperLink>

                            <asp:HyperLink ID="LinkHorasExtraDepartment" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reporte horas Extra por departamento</asp:HyperLink>
                            <asp:HyperLink ID="LinkHorasExtraTotalHorasPorSemana" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reporte de total de Horas Extras por Semana</asp:HyperLink>
                            <asp:LinkButton runat="server" OnClick="Redirect" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reportes Tickets</asp:LinkButton>
                            <asp:LinkButton runat="server" OnClick="RedirectTotalCena" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Reporte Total cenas</asp:LinkButton>

                            <asp:HyperLink ID="LinkTrabajosAbiertos" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Trabajos abiertos</asp:HyperLink>
                            <asp:HyperLink ID="LinkTrabajosAbiertosNoParcializadosPorSemana" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Trabajos abiertos no parcializados por semana</asp:HyperLink>
                            <asp:HyperLink ID="LinkTrabajosDetenidos" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Trabajos detenidos</asp:HyperLink>
                            <asp:HyperLink ID="LinkTrackingV2" runat="server" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Tracking V2</asp:HyperLink>
                        </div>
                    </div>
                </div>
                <div id="section4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/JTS.png" class="img-rounded" alt="Sistemas.">
                            JTS
                        </div>
                        <div class="panel-body list-group">
                            <a href="http://drl-sevr-web:8000/Inventory" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Control de Inventario</a>
                        </div>
                    </div>
                </div>
                <div id="section5">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <img src="images/Icon/Virtual Machine 2-40.png" class="img-rounded" alt="Sistemas.">
                            Sistemas
                        </div>
                        <div class="panel-body list-group">
                            <a runat="server" href="/tickets/Maintenance/Maintenance.aspx" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Asignar encargado de departamento</a>
                            <a runat="server" href="/Tickets/ConfigTickets.aspx" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Configuración Tickets</a>
                            <a runat="server" href="/Extensions/ExtensionsMaintenance.aspx" class="list-group-item list-group-item-action">
                                <img src="images/Icon/Un%20círculo%20de%20puntos-16.png" />&nbsp;Asignar extensiones</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</asp:Content>
