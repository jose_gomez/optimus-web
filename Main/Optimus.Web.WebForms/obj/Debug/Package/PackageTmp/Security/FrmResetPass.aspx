﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FrmResetPass.aspx.cs" Inherits="Optimus.Web.WebForms.Security.FrmResetPass" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div runat="server" id="divMessage" visible="false" class="panel-body">
        <div class="row">
            <div class="col-md-12">
                <asp:Label runat="server" ID="errorLabel" Text="Se ha detectado un error  en la operación" Font-Size="Medium"></asp:Label>
                <p class="text-warning"><small>Comuníquese con en el departamento de sistemas</small></p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <asp:LinkButton runat="server" Text="Ir a pagina principal" PostBackUrl="~/Default.aspx"></asp:LinkButton>
            </div>
        </div>
    </div>
    <div runat="server" id="frmPass" style="margin-top:5%" >
        <div class="form-group">
            <label for="Newpwd">Nueva contraseña:</label>
            <input type="password" class="form-control" id="Newpwd" runat="server">
        </div>
        <div class="form-group">
            <label for="Confirmpwd">Confirmar contraseña:</label>
            <input type="password" class="form-control" id="Confirmpwd" runat="server">
        </div>
        <asp:Button ID="Button1" runat="server" OnClick="Button1_Click" Text="Cambiar password" CssClass="btn btn-default" />
          <asp:LinkButton runat="server" Text="Iniciar Sesión" Visible="false" PostBackUrl="~/Default.aspx"></asp:LinkButton>
    </div>
</asp:Content>
