﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExtraHours.aspx.cs" Inherits="Optimus.Web.WebForms.ExtraHours.ExtraHours" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .botonFlecha {
            position: relative;
            top: 200px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('#ValidadoAlert').hide();
            $(".datepicker").datepicker({
                dateFormat: 'dd/mm/yy'
            });
            //      $(".datepicker").datepicker().datepicker('setDate', 'today');
            $('#ValidadoAlert').slideDown();
        });
        function AllCheck() {
            //$('#ExtraTable').find("input[type = 'checkbox']").each(function () {
            $('#ExtraTable').find(".divCheck").each(function () {
                $(this).find("input[type = 'checkbox']").each(function () {
                    $(this)[0].checked = !($(this)[0].checked);
                });
            });

        }
        function AllCheck2() {
            //$('#ExtraTable').find("input[type = 'checkbox']").each(function () {
            $('#ExtraTable').find(".divSelAll").each(function () {
                $(this).find("input[type = 'checkbox']").each(function () {
                    $(this)[0].checked = !($(this)[0].checked);
                });
            });

        }
        function AllCheck1() {
            //$('#ExtraTable').find("input[type = 'checkbox']").each(function () {
            $('#TableEmployee').find(".divCheck").each(function () {
                $(this).find("input[type = 'checkbox']").each(function () {
                    $(this)[0].checked = !($(this)[0].checked);
                });
            });

        }
        function AlertaFecha() {

            if ($('#MainContent_EstateHidden').val() == "True") {
                // $('#ValidadoAlert').slideDown();
            }

        }


    </script>
    <div id="container">

        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="conditional">
            <ContentTemplate>
                <div runat="server" id="ValidadoAlert" class="alert alert-warning">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                   <h2><asp:Label runat="server" ID="lblTag"></asp:Label></h2> 
                    <h2><asp:Label runat="server" ID="lblTime" Font-Bold="true"></asp:Label></h2>
                </div>
                <asp:Timer ID="Timer1" runat="server" OnTick="GetTime" Interval="60000" />
            </ContentTemplate>
        </asp:UpdatePanel>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Asignación Horas Extras y cena/almuerzo</h3>
            </div>
            <div class="panel-body">
                <asp:HiddenField runat="server" ID="EstateHidden" />
                <div class="row">
                    <div class="col-md-4">
                        <div>
                            <asp:Label runat="server" Font-Bold="true" Text="Fecha:" ID="fechaLabel"></asp:Label>
                            <asp:TextBox runat="server" CssClass="datepicker form-control" ID="FechaTextBox" ClientIDMode="static" OnTextChanged="cambioFecha" AutoPostBack="true"></asp:TextBox>
                            <asp:LinkButton runat="server" OnClick="Redirect">Ver Detalles</asp:LinkButton>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div>
                               
                            <asp:Label runat="server" Font-Bold="true" Text="Departamento:"></asp:Label>
                            <asp:DropDownList runat="server" ID="deparmetDropdownList" CssClass="form-control" OnSelectedIndexChanged="cambiarDepartment" AutoPostBack="true"></asp:DropDownList>
                        </div>
                    </div>
                    <asp:UpdatePanel runat="server" ID="upDateHoras" UpdateMode="conditional">
                        <ContentTemplate>
                            <div class="col-md-2">
                                <asp:Label runat="server" Font-Bold="true" Text="Rango hora:"></asp:Label>
                                <asp:DropDownList runat="server" ID="HoursStartDropdown" CssClass="form-control" OnSelectedIndexChanged="cambioHora" AutoPostBack="true">
   
                
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-2">
                                <span style="font-weight: bold">hasta:</span>
                                <asp:DropDownList runat="server" ID="HoursEndDropdown" CssClass="form-control" OnSelectedIndexChanged="cambioHora" AutoPostBack="true">
                     
                    
                                </asp:DropDownList>
                            </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>

                </div>
                <div class="row">
                    <div style="position: relative; top: 15px; left: 40px;">
                        <asp:UpdatePanel runat="server" ID="upPanel">
                            <ContentTemplate>
                                <div class="col-md-5">
                                    <h4>Empleados sin Horas Extra en fecha seleccionada</h4>
                                    <div class="panel panel-default" style="overflow: scroll">
                                        <div class="panel-body" style="width: 450px; height: 400px;">
                                            <label style="font-size: xx-small">
                                                <input id="Checkbox1" title="Seleccionar todo" type="checkbox" onchange="AllCheck1()" />
                                                Seleccionar todos</label>
                                            <asp:ListView runat="server" ID="EmployeesdeparmetListView">
                                                <LayoutTemplate>
                                                    <div class="table-responsive">
                                                        <table id="TableEmployee" class="table  table-stripe">
                                                            <thead>
                                                                <th>Seleccionar</th>
                                                                <th>Código</th>
                                                                <th>Empleado</th>
                                                            </thead>
                                                            <tbody class="myTable">
                                                                <tr runat="server" id="itemPlaceholder" />
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr runat="server">
                                                        <td>
                                                            <div class="divCheck">
                                                                <asp:CheckBox runat="server" ID="seleccionCheckBox" />
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Text='<%#Eval("EmployeeId")%>' ID="codigoLabel"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Text='<%#Eval("FullName")%>' ID="nombreLabel"></asp:Label>
                                                        </td>

                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-1">
                                    <asp:Button runat="server" CssClass="btn btn-default botonFlecha col-md-2" Text="<" OnClick="EliminarLista" ID="EliminarButton" />
                                    <asp:Button runat="server" CssClass="btn btn-default botonFlecha col-md-1" ID="AgregarButton" OnClick="AgregarLista" Text=">" />
                                </div>
                                <div class="col-md-5">
                                    <h4>Empleados con Horas Extra </h4><span class="badge" style="float:left" id="countEmpBadge" runat="server">12</span>
                                    <div class="panel panel-default" style="overflow: scroll">
                                        <div class="panel-body " style="width: 450px; height: 400px;">
                                            <label style="font-size: xx-small">
                                                <input id="Checkbox1" title="Seleccionar todo" type="checkbox" onchange="AllCheck2()" />
                                                Seleccionar todos </label>
                                            <label style="font-size: xx-small">
                                                <input id="Checkbox1" title="Seleccionar todo" type="checkbox" onchange="AllCheck()" />
                                                Seleccionar todos comida</label>
                                            <asp:ListView runat="server" ID="EmployeesExtraListView">
                                                <LayoutTemplate>
                                                    <div class="table-responsive">
                                                        <table id="ExtraTable" class="table  table-stripe">
                                                            <thead>
                                                                <th>Seleccionar</th>
                                                                <th>Código</th>
                                                                <th>Empleado</th>
                                                                <th>Comida</th>
                                                            </thead>
                                                            <tbody class="myTable">
                                                                <tr runat="server" id="itemPlaceholder" />
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </LayoutTemplate>
                                                <ItemTemplate>
                                                    <tr runat="server">
                                                        <td>
                                                            <div class="divSelAll">
                                                                <asp:CheckBox runat="server" ID="seleccionCheckBox" />
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Text='<%#Eval("EmployeeId")%>' ID="codigoLabel"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <asp:Label runat="server" Text='<%#Eval("FirstName")%>' ID="nombreLabel"></asp:Label>
                                                        </td>
                                                        <td>
                                                            <div class="divCheck">
                                                                <asp:CheckBox runat="server" ID="ComidaCheckBox" ClientIDMode="static" CssClass="comidaCheckBox" Checked='<%#Eval("Comida")%>' />
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </ItemTemplate>
                                            </asp:ListView>
                                        </div>
                                    </div>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </div>
                </div>
                <div class="row" style="position: relative; left: 40px; top: 5px;">
                    <asp:Button runat="server" ID="EnviarButton" CssClass="btn btn-primary" Text="Enviar Listado" OnClick="guardarLista" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
