﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CrearNotificacion.aspx.cs" Inherits="Optimus.Web.WebForms.Sistema.CrearNotificacion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>

        $(document).ready(function () {
            initSample()
        });

        function GuardarTickets() {
            var comment = CKEDITOR.instances.editor.getData();
            __doPostBack('GuardarTickets', comment)
        }
    </script>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div id="headDiv" runat="server" class="panel-heading"></div>
        <div id="bodyDiv" runat="server" class="panel-body">
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" Enabled="false" RepeatDirection="Horizontal">
                <asp:ListItem Value="1" Selected="True">Todos los usuarios.</asp:ListItem>
                <asp:ListItem Value="2">Usuarios específicos.</asp:ListItem>
            </asp:RadioButtonList>
            <div class="container col-lg-6">
                <div class="row">
                    <asp:Label ID="Label1" runat="server" Text="Titulo:" CssClass="col-lg-12"></asp:Label>
                    <asp:TextBox ID="txtTitulo" runat="server" MaxLength="75" CssClass="form-control col-lg-12"></asp:TextBox>
                </div>
            </div>
            <div class="container col-lg-6">
                <div class="row">
                    <asp:Label ID="Label2" runat="server" Text="Tipo de notificación:" CssClass="col-lg-12"></asp:Label>
                    <asp:DropDownList ID="ddlTiposNotificaciones" runat="server" CssClass="form-control col-lg-12">
                        <asp:ListItem Value="1">VERDE</asp:ListItem>
                        <asp:ListItem Value="2">AMARILLO</asp:ListItem>
                        <asp:ListItem Value="3">ROJO</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="container col-lg-12">
                <div class="row">
                    <div id="editor"></div>
                    <div style="display: none">
                        <div id="editor2" style="display: none"></div>
                    </div>
                </div>
            </div>
            <div class="container col-lg-12">
                <div class="row">
                    <input id="Button1" class="btn btn-primary" type="button" value="Crear" onclick="GuardarTickets();" />
                </div>
            </div>
            
        </div>
    </div>
</asp:Content>
