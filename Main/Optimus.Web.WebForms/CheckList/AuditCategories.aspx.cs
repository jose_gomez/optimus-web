﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class AuditCategories : System.Web.UI.Page
    {
        public int idCat = 0;
        public int idIndicator = 0;
        ServiceContainer container;
        UnitOfWork worker;
        public AuditCategories()
        {
            InitializeCulture();
            container = new ServiceContainer();
            worker = new UnitOfWork();
        }

        public enum MessageType { Success, Error, Info, Warning };
        protected void ShowMessage(string Message, MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }

        public void CargarCat()
        {
            dgvCategory.AutoGenerateColumns = false;
            var audtCat = container.GetAuditCategoriesService();
            var consCat = audtCat.GetAllCategory();
            dgvCategory.DataSource = consCat;
            dgvCategory.DataBind();
        }


        public void CargarAut()
        {
            var audControl = container.GetAuditoryControlService().GetAllAuditControl();
            cbAuditType.DataSource = audControl;
            cbAuditType.DataValueField = "ControlID";
            cbAuditType.DataTextField = "NameAudit";
            cbAuditType.DataBind();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                CargarAut();
                //
                CargarCat();

                string id = Request.QueryString["idCat"];

                if(string.IsNullOrEmpty(id))
                {
                    Response.Redirect("AuditCategories.aspx?idCat=0");
                }

                if(int.Parse(id) != 0)
                {
                    var catService = container.GetAuditCategoriesService();
                    var consCat = catService.GetCategoryByID(int.Parse(id));
                    txtCateg.Text = consCat.FirstOrDefault().Category;
                    cbAuditType.Value = consCat.FirstOrDefault().ControlID.ToString();
                }                
            }
        }
        
        protected void btnRegistrar_Click(object sender, EventArgs e)
        {
            try
            {
                if(!string.IsNullOrEmpty(txtCateg.Text))
                {
                    SHAuditCategory tabCat = new SHAuditCategory();
                    var audService = container.GetAuditCategoriesService();
                    
                    User usuario = worker.User.GetUserByUserName(User.Identity.Name);

                    tabCat.CategoryID = int.Parse(Request.QueryString["idCat"]); //Convert.ToInt32(lbIdCat.Text);
                    tabCat.Category = txtCateg.Text.ToUpper().Trim();
                    tabCat.ControlID = Convert.ToInt32(cbAuditType.Value);
                    tabCat.UserID = usuario.userID;

                    audService.SaveCategoryByID(tabCat);

                    AlertHelpers.ShowAlertMessage(this, "Registro", "Categoría Registrada");
                    
                    CargarAut();
                    txtCateg.Text = string.Empty;
                }
                else
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Ingrese la Categoría");
                }
            }
            catch(Exception ex)
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", ex.Message);
            }
        }

        protected void txtCateg_TextChanged(object sender, EventArgs e)
        {
            
        }

        protected void dgvCategory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='underline';";
                e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';";
                e.Row.ToolTip = "Click to select row";
            }
        }

        protected void txtIndicator_TextChanged(object sender, EventArgs e)
        {

        }

        

        protected void dgvIndicator_RowCreated(object sender, GridViewRowEventArgs e)
        {
            e.Row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='underline';";
            e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';";
            e.Row.ToolTip = "Click to select row";
        }

        
        protected void selCategory_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;

            var catService = container.GetAuditCategoriesService();
            var consCat = catService.GetCategoryByName(Server.HtmlDecode(grdrow.Cells[0].Text));
            txtCateg.Text = consCat.FirstOrDefault().Category;
            cbAuditType.Value = consCat.FirstOrDefault().ControlID.ToString();

            AlertHelpers.ShowAlertMessage(this, "Bien", "Categoria Registrada");

        }

        protected void selCategoryBtnLink_Click(object sender, EventArgs e)
        {
            AlertHelpers.ShowAlertMessage(this, "Bien", "Categoria Probada");
        }

        protected void dgvCategory_SelectedIndexChanged(object sender, EventArgs e)
        {
            AlertHelpers.ShowAlertMessage(this, "Bien", "Categoria Probada");
        }

        protected void btnCargarControl_Click(object sender, EventArgs e)
        {
            CargarAut();
        }
    }
}