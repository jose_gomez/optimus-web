﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditIndicator.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.AuditIndicator" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        input, select, textarea {
            max-width: none;
        }
    </style>

    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left; top: -5px; left: 4px;">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">INDICADORES </a>
                        </h4>
                    </div>

                </div>
            </div>
        </div>

        <div id="collapse2" class="panel-collapse collapse in">
            <div class="panel-body">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">REGISTRO DE INDICADORES
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="row">

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="txtIndicator">Auditoria</label>
                                    <asp:DropDownList class="form-control" ID="cbAuditType" runat="server" OnSelectedIndexChanged="cbAuditType_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                </div>
                            </div>

                            <div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group">
                                    <label class="control-label" for="cbCategory">Categoría</label>
                                    <select id="cbCategory" name="cbCategory" runat="server" class="form-control">
                                        <option></option>

                                    </select>
                                </div>
                            </div>

                            <div class="col-sm-12 col-md-12 col-sm-12">
                                <div class="form-group">
                                    <label id="lbIndic" runat="server" class="control-label" for="txtIndicator">Indicador</label>
                                    <asp:TextBox class="form-control" ID="txtIndicator" runat="server" placeholder="Indicador" TextMode="MultiLine" Height="100"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div id="InactiveIndicator" class="collapse">
                                <div class="col-sm-10 col-md-10 col-sm-10">

                                    <div class="form-group">
                                        <label class="control-label" for="txtIndicator">¿Por que desea eliminar este registro?</label>
                                        <asp:TextBox class="form-control" ID="txtInactivationReason" runat="server" placeholder="Motivo de eliminación" TextMode="MultiLine" Height="80"></asp:TextBox>
                                    </div>
                                </div>

                                <div class="col-sm-2 col-md-2 col-sm-2">
                                    <div class="form-group">
                                        <asp:Button ID="btnInactiveIndicator" CssClass="btn btn-danger" OnClick="btnInactiveIndicator_Click" runat="server" Text="Eliminar" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <button type="button" id="btnDeleteInd" runat="server" disabled="disabled" class="btn btn-info" data-toggle="collapse" data-target="#InactiveIndicator">Eliminar</button>
                        <asp:Button ID="btnSaveIndicator" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSaveIndicator_Click" />
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">BUSQUEDA DE INDICADORES
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div style="overflow: scroll;">
                            <asp:GridView ID="dgvIndicator" runat="server" EmptyDataText="No existen Indicadores." ShowHeaderWhenEmpty="false" Width="100%" ShowHeader="true" EnableModelValidation="False" class="table table-striped">
                                <Columns>
                                    <asp:BoundField HeaderText="IdControl" ShowHeader="false" DataField="IDControl" Visible="false" />
                                    <asp:BoundField HeaderText="Auditoria" ShowHeader="false" DataField="ControlName" />
                                    <asp:BoundField HeaderText="Indicador" ShowHeader="false" DataField="Indicator" />
                                    <asp:BoundField HeaderText="Categoria" ShowHeader="false" DataField="Category" />

                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="AuditIndicator.aspx?idIndic=<%# Eval("IdIndicator") %>">Editar</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="alert-info">
                                        <strong>Información!</strong> No se encontró ningún registro.
                                    </div>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC00" />
                            </asp:GridView>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</asp:Content>
