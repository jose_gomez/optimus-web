﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Extensions
{
    public partial class Extensions : System.Web.UI.Page
    {

        private ServiceContainer container;
        private UnitOfWork worker;

        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();


            var ext = worker.Extensions.GetExtensiones();
            ExtencionGridView.DataSource = ext;
            ExtencionGridView.DataBind();
            if (!IsPostBack)
            {
                var dep = container.GetDepartmentService().GetAllDepartments();
                DepartementoDropDownList.DataTextField = "Name";
                DepartementoDropDownList.DataValueField = "DepartmentId";
                DepartementoDropDownList.DataSource = dep;
                DepartementoDropDownList.DataBind();
            }
           

        }
        protected void DepartamentoFiltro(object sender,EventArgs e)
        {
            var ext = worker.Extensions.GetExtensiones(int.Parse(DepartementoDropDownList.SelectedItem.Value));
            ExtencionGridView.DataSource = ext;
            ExtencionGridView.DataBind();
        }
        

        protected void NombreFiltro(object sender,EventArgs e)
        {
            var ext = worker.Extensions.GetExtensiones(nombreTextBox.Text);
            ExtencionGridView.DataSource = ext;
            ExtencionGridView.DataBind();

        }
    }
}