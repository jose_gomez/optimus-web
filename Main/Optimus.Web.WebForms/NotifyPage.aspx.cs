﻿using Optimus.Web.BC;
using System;

namespace Optimus.Web.WebForms
{
    public partial class NotifyPage : System.Web.UI.Page
    {

        #region Private Fields

        private ServiceContainer container = new ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            DA.SYNotifycation notificacion = container.GetNotificationsService().Get(ObtenerID());
            if (notificacion != null)
            {
                headDiv.InnerText = notificacion.Title;
                bodyDiv.InnerHtml = notificacion.Description;
                int userId = container.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;
                if (notificacion.UserID != null && notificacion.UserID == userId)
                {
                    container.GetNotificationsService().Delete(notificacion.NotifycationsID);
                }
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private Guid ObtenerID()
        {
            Guid id = new Guid();
            try
            {
                id = new Guid(Request.QueryString["GUID"].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return id;
        }

        #endregion Private Methods

    }
}