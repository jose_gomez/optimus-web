﻿using Optimus.Web.BC;
using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;
using System.Web.UI;

namespace Optimus.Web.WebForms.Sistema
{
    public partial class CrearNotificacion : System.Web.UI.Page
    {

        #region Private Fields

        private ServiceContainer container = new ServiceContainer();
        private string Descripcion;

        #endregion Private Fields

        #region Protected Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (IsPostBack)
                {
                    if (Page.Request.Params["__EVENTTARGET"] == "GuardarTickets")
                    {
                        Descripcion = Page.Request.Params["__EVENTARGUMENT"].ToString();
                        GuardarNotificacion();
                    }
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void GuardarNotificacion()
        {
            Validar();
            if (RadioButtonList1.SelectedValue == "1")
            {
                DA.SYNotifycation notificacion = new DA.SYNotifycation();
                notificacion.Title = txtTitulo.Text;
                notificacion.Description = Descripcion;
                notificacion.NotificationTypeID = int.Parse(ddlTiposNotificaciones.SelectedValue);
                notificacion.UserID = null;
                container.GetNotificationsService().Save(notificacion);
            }
        }

        private void Validar()
        {
            if (string.IsNullOrWhiteSpace(txtTitulo.Text))
            {
                throw new Core.Exceptions.ValidationException("Indicar el titulo de la notificación.");
            }
            if (string.IsNullOrWhiteSpace(Descripcion))
            {
                throw new Core.Exceptions.ValidationException("Ingresar en mensaje de la notificación.");
            }
        }

        #endregion Private Methods

    }
}