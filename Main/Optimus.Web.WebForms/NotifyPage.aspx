﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NotifyPage.aspx.cs" Inherits="Optimus.Web.WebForms.NotifyPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">
        <div id="headDiv" runat="server" class="panel-heading"></div>
        <div id="bodyDiv" runat="server" class="panel-body"></div>
    </div>
</asp:Content>