﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ComplaintForm.aspx.cs" Inherits="Optimus.Web.WebForms.PermissionsPetition.ComplaintForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        function getEmployee() {
        
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetEmployeeById',
                data: '{employeeId:\'' + $('#CodigoTextBox').val() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#MainContent_nombreLabel').text(data.d.FullName);
                },
                error: function (e) {
                 
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }
    </script>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Formulario de Reclamación</h3>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="form-group">
                    <div class="col-md-6">
                        <asp:Label runat="server" Text="Código:" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" ClientIDMode="Static" ID="CodigoTextBox" CssClass="form-control" placeholder="Ingrese una código" onblur="getEmployee()"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="CodigoTextBox" ErrorMessage="*Debe completar todos los campos" ForeColor="red" ></asp:RequiredFieldValidator>
                    </div>
                    <div class="col-md-6">
                        <asp:Label runat="server" Text="Nombre:" Font-Bold="true"></asp:Label>
                        <asp:Label runat="server" ID="nombreLabel"></asp:Label>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <asp:Label runat="server" Text="Tipo Reclamación:" Font-Bold="true"></asp:Label>
                <asp:DropDownList ID="ReclamacionDropDown" CssClass="form-control" runat="server"></asp:DropDownList>
            </div>
            <div class="form-group">
                <asp:Label runat="server" Text="Descripción" Font-Bold="true"></asp:Label>
                <asp:TextBox runat="server" ClientIDMode="Static" ID="txtDescription" Style="min-width: 90%" CssClass="form-control" placeholder="Ingrese una descripción" TextMode="MultiLine"></asp:TextBox>
                <asp:RequiredFieldValidator runat="server" ControlToValidate="txtDescription" ErrorMessage="*Debe completar todos los campos" ForeColor="red" ></asp:RequiredFieldValidator>
            </div>
            <div class="form-group">
                <asp:Button runat="server" class="btn btn-primary" Text="Enviar reclamación" OnClick="EnviarTicketReclamacion" />
            </div>
        </div>
    </div>
</asp:Content>
