﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.PermissionsPetition
{
    public partial class PermissionsPetitionAprobation : System.Web.UI.Page
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        private ServiceContainer container;
        private UnitOfWork worker;
        Boolean AvalibleButton = false;
        Boolean HHRRUserAproved = false;
        SYAprobationsRequest Mang = null;

        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            var user = worker.User.GetUserByUserName(Page.User.Identity.Name);
            var emp = worker.Employees.GetEmployeeById(user.EmployeeID);
            HHRRUserAproved = container.GetPermissionsEmployeesService().AprovedHHRRUser(emp.EmployeeId);
            Mang = ContextSQL.SYAprobationsRequests.FirstOrDefault(x => x.UserID == user.userID && x.DepartmentID == emp.DepartmentID);

            AvalibleButtonAprovar();
            if (!IsPostBack)
            {
                var Perm = container.GetPermissionsEmployeesService().GetListPermissionPending(user.userName);
                PermissionRepeatera.DataSource = Perm;
                PermissionRepeatera.DataBind();
                CantidadPermisosPendientes.InnerText = Perm.Count.ToString();

                var Apro = container.GetPermissionsEmployeesService().GetListPermissionAproved(user.userName);
                AprovadoListView.DataSource = Apro;
                AprovadoListView.DataBind();
                CantidadAprobados.InnerText = Apro.Count.ToString();
            }
        }

        private void AvalibleButtonAprovar()
        {
            if (Mang != null || HHRRUserAproved)
            {
                AvalibleButton = true;
            }
        }
       protected void DataBound(object sender,ListViewItemEventArgs e)
        {
           ((LinkButton)e.Item.FindControl("AprobarButton")).Visible = AvalibleButton;
            string a = ((Label)e.Item.FindControl("StatusLabel")).Text;
            if (a == "APPROVED" || (!HHRRUserAproved && a == "PRE-APROVED"))
            {
                ((LinkButton)e.Item.FindControl("AprobarButton")).Enabled = false;
                ((LinkButton)e.Item.FindControl("AprobarButton")).Text = "Aprobada";
            }
        }

        protected void DataBoundAProved(object sender, ListViewItemEventArgs e)
        {
            ((LinkButton)e.Item.FindControl("AprobarButton")).Visible = AvalibleButton;
            string a = ((Label)e.Item.FindControl("StatusLabel")).Text;
            if (a == "APPROVED" || (!HHRRUserAproved && a == "PRE-APROVED"))
            {
                ((LinkButton)e.Item.FindControl("AprobarButton")).Enabled = false;
                ((LinkButton)e.Item.FindControl("AprobarButton")).Text = "Aprobada";
            }
        }

        protected void Aprobar(object sender, CommandEventArgs e)
        {
            int ID = int.Parse(e.CommandArgument.ToString());
            container.GetPermissionsEmployeesService().AprovedOrPreAprovedPermission(ID, HHRRUserAproved);
            Response.Redirect(Request.RawUrl);
        }
        protected void Denegar(object sender, CommandEventArgs e)
        {
            int ID = int.Parse(e.CommandArgument.ToString());
            HRPermissionsEmployee Permi = ContextSQL.HRPermissionsEmployees.FirstOrDefault(x => x.PermissionsEmployeesID == ID);
            Permi.StatusID = 26;
            ContextSQL.SubmitChanges();
        }
        protected void BuscarByID(object sender,EventArgs e)
        {
            if (!string.IsNullOrEmpty(EmployeeIDTextBox.Text))
            {
                int Emp = int.Parse(EmployeeIDTextBox.Text);
                var Perm = container.GetPermissionsEmployeesService().GetListPermissionPendingEmployeesBeEmployeeID(Emp);
                PermissionRepeatera.DataSource = Perm;
                PermissionRepeatera.DataBind();
                CantidadPermisosPendientes.InnerText = Perm.Count.ToString();
                var Apro = container.GetPermissionsEmployeesService().GetListPermissionAprovedEmployeesBeEmployeeID(Emp);
                AprovadoListView.DataSource = Apro;
                AprovadoListView.DataBind();
                CantidadAprobados.InnerText = Apro.Count.ToString();
                var Cancel = container.GetPermissionsEmployeesService().GetListPermissionCanceledEmployeesBeEmployeeID(Emp);
                CancelarListView.DataSource = Cancel;
                CancelarListView.DataBind();
                DenegadosBadge.InnerText = Cancel.Count.ToString();
            }

        }
    }
}