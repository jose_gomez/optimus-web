﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ChekingEmployees.aspx.cs" Inherits="Optimus.Web.WebForms.PermissionsPetition.ChekingEmployees" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

            .bs-callout h4 {
                margin-top: 0;
                margin-bottom: 5px;
            }

            .bs-callout p:last-child {
                margin-bottom: 0;
            }

            .bs-callout code {
                border-radius: 3px;
            }

            .bs-callout + .bs-callout {
                margin-top: -5px;
            }

        .bs-callout-default {
            border-left-color: #777;
        }

            .bs-callout-default h4 {
                color: #777;
            }

        .bs-callout-primary {
            border-left-color: #428bca;
        }

            .bs-callout-primary h4 {
                color: #428bca;
            }

        .bs-callout-success {
            border-left-color: #5cb85c;
        }

            .bs-callout-success h4 {
                color: #5cb85c;
            }

        .bs-callout-danger {
            border-left-color: #d9534f;
        }

            .bs-callout-danger h4 {
                color: #d9534f;
            }

        .bs-callout-warning {
            border-left-color: #f0ad4e;
        }
         .bs-callout-warning h4 {
                color: #f0ad4e;
            }

        .bs-callout-info {
            border-left-color: #5bc0de;
        }

            .bs-callout-info h4 {
                color: #5bc0de;
            }
            .bs-callout{
                font-size:large;
                padding:10px;
            }
  
    </style>
    <div class="container">
        <asp:TextBox runat="server" Width="0px" Height="0px" ID="NumeroEmployeTextbox" OnTextChanged="Buscar" AutoPostBack="true" BorderStyle="None"></asp:TextBox>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Información Empleado</h3>
            </div>
            <div class="panel-body">

                <div class="row">
                    <div class="col-md-4">
                        <img runat="server" width="360" height="350" id="ImageCan" />
                    </div>
                    <div class="col-md-8">
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server"  CssClass="form-label" Font-Bold="true" Text="Codigo:" Font-Size="35px"></asp:Label>  
                                <asp:Label runat="server"  CssClass="form-label" Font-Bold="true" ID="NumeroEmployeLabel"  Font-Size="35px"></asp:Label>                         
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Font-Size="35px" CssClass="form-label" Font-Bold="true" Text="Nombre:"></asp:Label>
                                <asp:Label runat="server" Font-Size="35px" CssClass="form-label" ID="NombreLabel" ></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Font-Size="35px" CssClass="form-label" Font-Bold="true" Text="Departamento:"></asp:Label>
                                <asp:Label runat="server" Font-Size="35px" CssClass="form-label" ID="DepartamentoLabel" ></asp:Label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <asp:Label runat="server" Font-Size="35px" CssClass="form-label" Font-Bold="true" Text="Posición:"></asp:Label>
                                <asp:Label runat="server" Font-Size="35px" CssClass="form-label" ID="PosicionLabel"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr />
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="form-label" Font-Bold="true" Text="Jornada:" Font-Size="40px"></asp:Label>
                                <asp:Label runat="server" ID="JornadaLabel" Font-Size="40px" CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                           <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="form-label" Font-Bold="true" Text="Hora Extra:" Font-Size="40px"></asp:Label>
                                <asp:Label runat="server" ID="HoraExtraLabel" Font-Size="40px" CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr />
                        <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="form-label" Font-Bold="true" Text="Estado permiso:" Font-Size="35px"></asp:Label>
                                <asp:Label runat="server" ID="EstadoPermisoLabel" Font-Size="35px"  ForeColor="Green" CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                           <div class="col-md-6">
                            <div class="form-group">
                                <asp:Label runat="server" CssClass="form-label" Font-Bold="true" Text="Hora Permiso:" Font-Size="35px"></asp:Label>
                                <asp:Label runat="server" ID="HoraPermisoLabel" Font-Size="35px" CssClass="form-label"></asp:Label>
                            </div>
                        </div>
                    </div>
                </div>
            
                <div class="row">
                    <div class="col-md-12">
                        <hr />
               
                    </div>
                </div>
            </div>
        </div>
    </div>
       <div id="Textbox" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-info">
             
                </div>
                <div class="modal-body">
                    </div>
                </div>
            </div>
           </div>
    
</asp:Content>
