﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarIndicadoresPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarIndicadoresPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2>Administración de indicadores</h2>
        </div>
        <div class="panel-body form">
            <div class="form-group">
                <label>Puesto:</label>
                <label id="LblNombrePuesto" runat="server">Nombre del puesto</label>
            </div>
            <div class="form-group">
                <label for="DDLIndicadores">Indicador:</label>
                <asp:DropDownList ID="DDLIndicadores" runat="server" CssClass="form-control" OnSelectedIndexChanged="DDLIndicadores_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </div>
            <div class="form-group">
                <label for="Porcentaje">Porcentaje:</label>
                <input type="number" class="form-control" id="Porcentaje" runat="server" min="1" max="100" aria-valuemin="1" aria-valuemax="100">
            </div>
            <asp:Button ID="BttnGuardar" runat="server" Text="Guardar" CssClass="btn btn-primary" OnClick="BttnGuardar_Click" />
            <asp:Button ID="btnDelete" CssClass="btn btn-danger" runat="server" Text="Eliminar" OnClick="btnDelete_Click" />
            <asp:Button ID="Button1" CssClass="btn btn-default" runat="server" Text="Cancelar" OnClick="Button1_Click" />
        </div>
    </div>
</asp:Content>