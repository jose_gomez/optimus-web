﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AnalisisPuesto : System.Web.UI.Page
    {
        #region Private Fields

        private const string URL_PARAMETER_IDANALISIS = "IDAnalisis";
        private const string URL_PARAMETER_IDPOSICION = "IDPosicion";
        private const string URL_PARAMETER_IDREQUISICION = "IDRequisicion";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void LinkButtonCancelar_Click(object sender, EventArgs e)
        {
            Response.RedirectPermanent(ObtenerReturn());
        }

        protected void LinkButtonEliminar_Click(object sender, EventArgs e)
        {
            var analisis = ServCont.GetAnalisisPosicion().Get(ObtenerIDAnalisis());
            if (analisis != null)
            {
                ServCont.GetAnalisisPosicionDetalle().Delete(int.Parse(HiddenFieldDetalleID.Value));
                Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", analisis.ID, ObtenerReturn()));
            }
        }

        protected void LinkButtonGuardar_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                var analisis = ServCont.GetAnalisisPosicion().Get(ObtenerIDAnalisis());
                if (analisis != null)
                {
                    analisis.PositionName = txtPositionName.Text;
                    analisis.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;
                    analisis = ServCont.GetAnalisisPosicion().Save(analisis);
                    GuardarDetalle(analisis.ID);
                    Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", analisis.ID, ObtenerReturn()));
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void LinkButtonPublicar_Click(object sender, EventArgs e)
        {
            var analisis = ServCont.GetAnalisisPosicion().Get(ObtenerIDAnalisis());
            if (analisis != null)
            {
                analisis.StatusID = 33; //Sumitted
                analisis.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;
                analisis = ServCont.GetAnalisisPosicion().Save(analisis);
                Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", analisis.ID, ObtenerReturn()));
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    if (ObtenerIDRequision() > 0)
                    {
                        var analisis = ServCont.GetAnalisisPosicion().GetByRequisicion(ObtenerIDRequision());
                        if (analisis == null)
                        {
                            DTO.AnalisisPosicion nuevoAnalisis = new DTO.AnalisisPosicion();
                            nuevoAnalisis.RequisitionID = ObtenerIDRequision();
                            nuevoAnalisis.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(Page.User.Identity.Name).userID;
                            nuevoAnalisis.StatusID = 35; //pendiente
                            nuevoAnalisis.PositionName = "POSICION SIN NOMBRE";
                            nuevoAnalisis = ServCont.GetAnalisisPosicion().Save(nuevoAnalisis);
                            Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", nuevoAnalisis.ID, ObtenerReturn()));
                        }
                        else
                        {
                            Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", analisis.ID, ObtenerReturn()));
                        }
                    }
                    else if (ObtenerIDPosicion() > 0)
                    {
                        var analisis = ServCont.GetAnalisisPosicion().GetByPosition(ObtenerIDPosicion());
                        if (analisis == null)
                        {
                            DTO.AnalisisPosicion nuevoAnalisis = new DTO.AnalisisPosicion();
                            nuevoAnalisis.PositionID = ObtenerIDPosicion();
                            nuevoAnalisis.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(Page.User.Identity.Name).userID;
                            nuevoAnalisis.StatusID = 35; //pendiente
                            nuevoAnalisis.PositionName = ServCont.GetPositionService().GetPositionByID(ObtenerIDPosicion()).PositionName;
                            nuevoAnalisis = ServCont.GetAnalisisPosicion().Save(nuevoAnalisis);
                            Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", nuevoAnalisis.ID, ObtenerReturn()));
                        }
                        else
                        {
                            Response.Redirect(string.Format("~/HHRR/AnalisisPuesto?IDAnalisis={0}&Return={1}", analisis.ID, ObtenerReturn()));
                        }
                    }
                    else if (ObtenerIDAnalisis() > 0 && ObtenerIDDetalle() > 0)
                    {
                        var analisis = ServCont.GetAnalisisPosicion().Get(ObtenerIDAnalisis());
                        if (analisis != null)
                        {
                            LinkButtonGuardar.Enabled = true;
                            LinkButtonPublicar.Enabled = (analisis.StatusID == 35 || analisis.StatusID == null); //pendiente
                            txtPositionName.Text = analisis.PositionName;
                            txtPositionName.ReadOnly = analisis.PositionID != null;
                            Repeater1.DataSource = ServCont.GetAnalisisPosicion().GetDetalles(analisis.ID);
                            Repeater1.DataBind();
                        }

                        var detalle = ServCont.GetAnalisisPosicionDetalle().Get(ObtenerIDDetalle());
                        if (detalle != null)
                        {
                            LinkButtonEliminar.Enabled = true;
                            HiddenFieldDetalleID.Value = detalle.ID.ToString();
                            txtFuntion.Text = detalle.Funtion;
                            txtWhat.Text = detalle.What;
                            txtHow.Text = detalle.How;
                            txtWhy.Text = detalle.Why;
                            txtTime.Text = detalle.Time;
                        }
                    }
                    else if (ObtenerIDAnalisis() > 0)
                    {
                        var analisis = ServCont.GetAnalisisPosicion().Get(ObtenerIDAnalisis());
                        if (analisis != null)
                        {
                            LinkButtonGuardar.Enabled = true;
                            LinkButtonPublicar.Enabled = (analisis.StatusID == 35 || analisis.StatusID == null); //pendiente
                            txtPositionName.Text = analisis.PositionName;
                            txtPositionName.ReadOnly = analisis.PositionID != null;
                            foreach (var item in ServCont.GetAnalisisPosicion().GetDetalles(analisis.ID))
                            {
                            }
                            Repeater1.DataSource = ServCont.GetAnalisisPosicion().GetDetalles(analisis.ID);
                            Repeater1.DataBind();
                        }
                    }
                    else
                    {
                        LinkButtonGuardar.Enabled = false;
                        LinkButtonPublicar.Enabled = false;
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void GuardarDetalle(int analisisID)
        {
            DTO.AnalisisPosicionDetalle detalle = new DTO.AnalisisPosicionDetalle();
            detalle.ID = int.Parse(HiddenFieldDetalleID.Value);
            detalle.AnalisisID = analisisID;
            detalle.Funtion = txtFuntion.Text;
            detalle.What = txtWhat.Text;
            detalle.How = txtHow.Text;
            detalle.Why = txtWhy.Text;
            detalle.Time = txtTime.Text;
            ServCont.GetAnalisisPosicionDetalle().Save(detalle);
        }

        private int ObtenerIDAnalisis()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER_IDANALISIS].ToString());
            }
            catch (Exception)
            {
            }
            return id;
        }

        private int ObtenerIDDetalle()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString["IDDetalle"].ToString());
            }
            catch (Exception)
            {
            }
            return id;
        }

        private int ObtenerIDPosicion()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER_IDPOSICION].ToString());
            }
            catch (Exception)
            {
            }
            return id;
        }

        private int ObtenerIDRequision()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER_IDREQUISICION].ToString());
            }
            catch (Exception)
            {
            }
            return id;
        }

        private string ObtenerReturn()
        {
            string i = string.Empty;
            try
            {
                i = Request.QueryString["Return"].ToString();
            }
            catch (Exception)
            {
            }
            return i;
        }

        #endregion Private Methods

        protected void LinkButtonImprimir_Click(object sender, EventArgs e)
        {
            Response.RedirectPermanent(string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/AnalisisPuesto&positionAnalisis={0}&rc:Parameters=False", ObtenerIDAnalisis().ToString()));
        }
    }
}