﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class DetallePuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_ADMINISTRAR_ANALISIS_PUESTO = "~/HHRR/AdministrarAnalisisPuesto.aspx?ID={0}";
        private const string LINK_ADMINISTRAR_COMPETENCIAS_PUESTO = "~/HHRR/AdministrarCompetenciasPuesto.aspx?ID={0}";
        private const string LINK_ADMINISTRAR_MATRIZ_SALARIAL = "~/HHRR/AdministrarMatrizSalarial.aspx?ID={0}";
        private const string LINK_ADMINISTRAR_PUESTO = "~/HHRR/AdministrarPuesto.aspx?ID={0}";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string SIN_DEPARTAMENTO = "Sin departamento.";
        private const string SIN_SUPERIOR = "Sin superior asignado";
        private const string TOTAL = "TOTAL";
        private const string URL_PARAMETER = "ID";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    ObtenerInformacionBasicaPosicion();
                    ObtenerInformacionBasicaDepartamento();
                    ObtenerInformacionBasicaPosicionSuperior();

                    var analisis = ServCont.GetAnalisisPosicion().GetByPosition(ObtenerID());
                    if (analisis != null)
                    {
                        AnalisisDetalles.DataSource = ServCont.GetAnalisisPosicionDetalle().GetByAnalisis(analisis.ID);
                        AnalisisDetalles.DataBind();
                    }
                    

                    var indicadores = ServCont.GetIndicadorDesempenioPosicion().GetByPosition(ObtenerID());
                    if (indicadores!=null)
                    {
                        Repeater1.DataSource = indicadores;
                        Repeater1.DataBind();
                    }
                    Repeater2.DataSource = CategoriaCompetenciaDataHelper.GetList(Core.OptimusApiUrls.CategoriasCompetencias_GET);
                    Repeater2.DataBind();
                    Repeater4.DataSource = ServCont.GetPositionsService().GetAllSalaryLevelOrderBySequence();
                    Repeater4.DataBind();

                    GetMatrizSalarial();
                    SetLinks();
                    OrgChartImg.Src = string.Format("OrgChartImg.aspx?ID={0}", ObtenerID().ToString());
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private string CrearAnalisis(int id)
        {
            string html = string.Empty;
            DA.HRPositionAnalisy analisis = ServCont.GetPositionsService().GetPositionsAnalisyBYPositionID(id);

            if (analisis != null)
            {
                List<DA.HRPositionAnalisysDetail> detalles = ServCont.GetPositionsService().GetPositionAnalisyDeatailByAnalisyID(analisis.PositionAnalysisID);
                html = "<ul>";
                foreach (DA.HRPositionAnalisysDetail item in detalles)
                {
                    html += string.Format("<li><b>{0}</b><ul>", item.Funtion);
                    html += string.Format("<li><b>Que hace?</b> {0}</li>", item.WhatDo);
                    html += string.Format("<li><b>Como lo hace?</b> {0}</li>", item.HowDo);
                    html += string.Format("<li><b>Para que lo hace?</b> {0}</li>", item.WhyDo);
                    html += string.Format("<li><b>Que tiempo se toma para hacerlo?</b> {0}</li></ul></ li>", item.TimeDo);
                }
                html += "</ ul >";
            }

            return html;
        }

        private void GetMatrizSalarial()
        {
            List<DA.VWMatrizSalarial> matriz = ServCont.GetPositionsService().GetMatrizSalarialByPositionID(ObtenerID());
            DA.VWMatrizSalarial matr = new DA.VWMatrizSalarial { D = 0, C = 0, PROMEDIO = 0, B = 0, A = 0 };
            foreach (DA.VWMatrizSalarial item in matriz)
            {
                matr.MarcoGeneral = TOTAL;
                matr.D += item.D;
                matr.C += item.C;
                matr.PROMEDIO += item.PROMEDIO;
                matr.B += item.B;
                matr.A += item.A;
            }
            matriz.Add(matr);

            gvMatriz.DataSource = matriz;
            gvMatriz.DataBind();
        }

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        private void ObtenerInformacionBasicaDepartamento()
        {
            var departamento = DepartamentoDataHelper.Get(Core.OptimusApiUrls.Posiciones_GET_Departamento.Replace("{id}", 
                ObtenerID().ToString()));
            if (departamento == null)
            {
                lblDepartamento.Text = SIN_DEPARTAMENTO;
            }
            else
            {
                lblDepartamento.Text = departamento.Name;
            }
        }

        private void ObtenerInformacionBasicaPosicion()
        {
            var posicion = PositionDataHelper.Get(Core.OptimusApiUrls.Posiciones_GET_BY_ID.Replace("{id}", 
                ObtenerID().ToString()));
            lblPuestoName.Text = posicion.PositionName;
            lblDescripcion.Text = posicion.Description;
        }

        private void ObtenerInformacionBasicaPosicionSuperior()
        {
            var posicion = PositionDataHelper.Get(Core.OptimusApiUrls.Posiciones_GET_BY_ID.Replace("{id}", 
                ObtenerID().ToString()));
            var superior = PositionDataHelper.Get(Core.OptimusApiUrls.Posiciones_GET_BY_ID.Replace("{id}", 
                (posicion.SuperiorPositionID == null ? "0" : ((int)posicion.SuperiorPositionID).ToString())));
            if (superior == null)
            {
                lblSuperior.Text = SIN_SUPERIOR;
            }
            else
            {
                lblSuperior.Text = superior.PositionName;
            }
        }

        private void SetLinks()
        {
            HyperLink1.NavigateUrl = string.Format(LINK_ADMINISTRAR_PUESTO, ObtenerID().ToString());
            HyperLink2.NavigateUrl = string.Format(LINK_ADMINISTRAR_COMPETENCIAS_PUESTO, ObtenerID().ToString());
            HyperLink3.NavigateUrl = string.Format("~/HHRR/AnalisisPuesto?IDPosicion={0}&Return={1}", ObtenerID().ToString(), Request.Url);
            HyperLink4.NavigateUrl = string.Format(LINK_ADMINISTRAR_MATRIZ_SALARIAL, ObtenerID().ToString());
            HyperLink5.NavigateUrl = string.Format("~/HHRR/OrgChartImg.aspx?ID={0}", ObtenerID().ToString());
            HyperLink6.NavigateUrl = string.Format("~/HHRR/AdministrarIndicadoresPuesto.aspx?ID={0}", ObtenerID().ToString());
        }

        #endregion Private Methods

        protected void Repeater2_ItemDataBound(object sender, System.Web.UI.WebControls.RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var a = ((DTO.CategoriaCompetencia)e.Item.DataItem).CompetenceCategoryID;
                var competencias = ServCont.GetCompetenciaService().GetByCategoriaPosicion(ObtenerID(),a);
                if (competencias != null)
                {
                    (e.Item.FindControl("Repeater3") as Repeater).DataSource = competencias;
                    (e.Item.FindControl("Repeater3") as Repeater).DataBind();
                }
            }
            
        }
    }
}