﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarCompetenciasPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_DETALLE_PUESTO = "~/HHRR/DetallePuesto?ID={0}";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string MSG_ESPECIFICAR_NIVEL = "Debe especificar el nivel.";
        private const string URL_PARAMETER = "ID";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                int salida = 0;
                if (!int.TryParse(TextBox3.Text, out salida))
                {
                    validaciones.Add(MSG_ESPECIFICAR_NIVEL);
                    throw new Core.Exceptions.ValidationException("Validación");
                }
                int id = ObtenerID();

                DA.HRPositionCompetence rel = new DA.HRPositionCompetence
                {
                    PositionID = id,
                    CompetenceID = int.Parse(DropDownList2.SelectedValue),
                    Level = float.Parse(TextBox3.Text)
                };

                ServCont.GetPositionsService().Save(rel);

                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, id.ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Button2_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, ObtenerID().ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Button3_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                int id = ObtenerID();
                int idCompetencia = int.Parse(DropDownList2.SelectedValue);
                DA.HRPositionCompetence rel = new DA.HRPositionCompetence
                {
                    CompetenceID = idCompetencia,
                    PositionID = id
                };
                ServCont.GetPositionsService().Delete(rel);
                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, id.ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void DropDownList1_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DropDownList2.SetCompetencesByCategory(int.Parse(DropDownList1.SelectedValue));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void DropDownList2_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                BC.Models.Competencia comp = ServCont.GetPositionsService().GetCompetenceBy(int.Parse(DropDownList2.SelectedValue), ObtenerID());
                if (comp != null) TextBox3.Text = comp.Nivel.ToString();
                else { TextBox3.Text = ""; }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    int id = ObtenerID();
                    DA.HRPosition po = ServCont.GetPositionsService().GetPositionsByID(id);
                    lblPuestoName.Text = po.PositionName;
                    DropDownList1.SetAllCompetenceCategory();
                    DropDownList2.SetCompetencesByCategory(int.Parse(DropDownList1.SelectedValue));
                    BC.Models.Competencia comp = ServCont.GetPositionsService().GetCompetenceBy(int.Parse(DropDownList2.SelectedValue), id);
                    if (comp != null) TextBox3.Text = comp.Nivel.ToString();
                    else { TextBox3.Text = ""; }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        #endregion Private Methods

    }
}