﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AdministrarIndicadoresPosicion.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AdministrarIndicadoresPosicion" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
        <div class="panel panel-default">
            <div class="panel-heading">Panel Heading</div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="email">Email address:</label>
                    <input type="email" class="form-control" id="email">
                </div>
                <div class="form-group">
                    <label for="pwd">Password:</label>
                    <input type="password" class="form-control" id="pwd">
                </div>
                <div class="checkbox">
                    <label>
                        <input type="checkbox">
                        Remember me</label>
                </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </div>
        </div>
</asp:Content>
