﻿using Optimus.Web.BC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class Capacitaciones : System.Web.UI.Page
    {
        private ServiceContainer container = new ServiceContainer();
        protected void Page_Load(object sender, EventArgs e)
        {
            CargarEnProceso();
            CargarPendientes();
        }

        private void CargarPendientes()
        {
            StringBuilder listadoCapacitaciones = new StringBuilder();
            listadoCapacitaciones.Append("<ul class=\"treeview\">");
            var cursos = container.GetCurso().GetCAPPendientes();
            foreach (var item in cursos)
            {
                listadoCapacitaciones.AppendFormat("<li >&nbsp;{0}", item.Title);
                var sesiones = container.GetSesion().GetSesionesCAPPendientesByCurso(item.CourseID);
                listadoCapacitaciones.AppendFormat("&nbsp;<span class=\"badge\">{0}</span>", sesiones.Count.ToString());
                listadoCapacitaciones.Append("<ul>");
                foreach (var sesion in sesiones)
                {
                    listadoCapacitaciones.AppendFormat("<li >&nbsp;<a href=\"CapacitacionDetalle?ID={0}\">{1} Inicia:{2} </a></li>",sesion.SessionID.ToString(), sesion.SessionCode, ((DateTime)sesion.StartDate).ToShortDateString());
                }
                listadoCapacitaciones.Append("</ul>");
                listadoCapacitaciones.AppendFormat("</li>");
            }
            listadoCapacitaciones.Append("</ul>");
            Div2.InnerHtml = listadoCapacitaciones.ToString();
        }

        private void CargarEnProceso()
        {
            StringBuilder listadoCapacitaciones = new StringBuilder();
            listadoCapacitaciones.Append("<ul class=\"treeview\">");
            var cursos = container.GetCurso().GetCAPEnProceso();
            foreach (var item in cursos)
            {
                listadoCapacitaciones.AppendFormat("<li >&nbsp;{0}", item.Title);
                var sesiones = container.GetSesion().GetSesionesCAPEnProcesoByCurso(item.CourseID);
                listadoCapacitaciones.AppendFormat("&nbsp;<span class=\"badge\">{0}</span>", sesiones.Count.ToString());
                listadoCapacitaciones.Append("<ul>");
                foreach (var sesion in sesiones)
                {
                    listadoCapacitaciones.AppendFormat("<li >&nbsp;<a href=\"CapacitacionDetalle?ID={0}\">{1} Inicia:{2} </a></li>", sesion.SessionID.ToString(), sesion.SessionCode, ((DateTime)sesion.StartDate).ToShortDateString());
                }
                listadoCapacitaciones.Append("</ul>");
                listadoCapacitaciones.AppendFormat("</li>");
            }
            listadoCapacitaciones.Append("</ul>");
            Div1.InnerHtml = listadoCapacitaciones.ToString();
        }
    }
}