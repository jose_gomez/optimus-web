﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CapacitacionDetalle.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.CapacitacionDetalle" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/ctsm-bootstrap-user.css" rel="stylesheet" />
    <script src="../Scripts/ctsm-bootstrap-user.js"></script>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h2 class="panel-title">Participantes</h2>
        </div>
        <div class="panel-body">
            <div class=" col-lg-12">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <div class="row user-row">
                            <div class="col-xs-3 col-sm-2 col-md-1 col-lg-1">
                                <img src="../images/Icon/Usuario%20de%20género%20neutro-40.png" class="img-circle" />
                            </div>
                            <div class="col-xs-8 col-sm-9 col-md-10 col-lg-10">
                                <strong><%# Eval("FirstName") %> <%# Eval("LastName") %></strong><br>
                                <span class="text-muted">No. Empleado: <%# Eval("EmployeeID") %></span>
                            </div>
                            <div class="col-xs-1 col-sm-1 col-md-1 col-lg-1 dropdown-user" data-for=".<%# Eval("EmployeeID") %>">
                                <i class="glyphicon glyphicon-chevron-down text-muted"></i>
                            </div>
                        </div>
                        <div class="row user-infos <%# Eval("EmployeeID") %>">
                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10 col-xs-offset-0 col-sm-offset-0 col-md-offset-1 col-lg-offset-1">
                                <div class="panel panel-primary">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Información del empleado</h3>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-md-3 col-lg-3 hidden-xs hidden-sm">
                                                <img src="../images/Icon/Usuario%20de%20género%20neutro-80.png" class="img-circle" />
                                            </div>
                                            <div class="col-xs-2 col-sm-2 hidden-md hidden-lg">
                                                <img src="../images/Icon/Usuario%20de%20género%20neutro-40.png" class="img-circle" />
                                            </div>
                                            <div class="col-xs-10 col-sm-10 hidden-md hidden-lg">
                                                <strong><%# Eval("FirstName") %> <%# Eval("LastName") %></strong><br>
                                                <dl>
                                                    <dt>Posición:</dt>
                                                    <dd><%# Eval("PositionName") %></dd>
                                                    <dt>Departamento:</dt>
                                                    <dd><%# Eval("DepartmentName") %></dd>
                                                </dl>
                                            </div>
                                            <div class=" col-md-9 col-lg-9 hidden-xs hidden-sm">
                                                <strong><%# Eval("FirstName") %> <%# Eval("LastName") %></strong><br>
                                                <table class="table table-user-information">
                                                    <tbody>
                                                        <tr>
                                                            <td>Posición:</td>
                                                            <td><%# Eval("PositionName") %></td>
                                                        </tr>
                                                        <tr>
                                                            <td>Departamento:</td>
                                                            <td><%# Eval("DepartmentName") %></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    <%--Agregar botones en caso de necesitarse no BORRAR--%>
                                    <%--<div class="panel-footer">
                            <button class="btn btn-sm btn-primary" type="button"
                                data-toggle="tooltip"
                                data-original-title="Send message to user">
                                <i class="glyphicon glyphicon-envelope"></i>
                            </button>
                            <span class="pull-right">
                                <button class="btn btn-sm btn-warning" type="button"
                                    data-toggle="tooltip"
                                    data-original-title="Edit this user">
                                    <i class="glyphicon glyphicon-edit"></i>
                                </button>
                                <button class="btn btn-sm btn-danger" type="button"
                                    data-toggle="tooltip"
                                    data-original-title="Remove this user">
                                    <i class="glyphicon glyphicon-remove"></i>
                                </button>
                            </span>
                        </div>--%>
                                </div>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>

            </div>
        </div>
    </div>

</asp:Content>
