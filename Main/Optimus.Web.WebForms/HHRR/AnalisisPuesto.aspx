﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AnalisisPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.AnalisisPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script src="../Scripts/bootstrap-customfilteredlist.min.js"></script>
    <link href="../Content/bootstrap-customfilteredlist.min.css" rel="stylesheet" />
    <link href="../Content/bootstrap-custombtn.min.css" rel="stylesheet" />
    <style>
        .form-control {
            max-width: 100%;
        }
    </style>
    <legend>Análisis de puesto</legend>
    <div id="BootstrapMessage" runat="server">
    </div>
    <asp:HiddenField ID="HiddenFieldDetalleID" runat="server" Value="0"/>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="textinput">Nombre de la posición</label>
        <div class="col-sm-12 col-md-8">
            <asp:TextBox ID="txtPositionName" runat="server" CssClass="form-control input-md"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="textinput">Nombre de la función</label>
        <div class="col-sm-12 col-md-8">
            <asp:TextBox ID="txtFuntion" runat="server" CssClass="form-control input-md"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="textinput">Que hace?</label>
        <div class="col-sm-12 col-md-8">
            <asp:TextBox ID="txtWhat" runat="server" CssClass="form-control input-md"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="textinput">Como se hace?</label>
        <div class="col-sm-12 col-md-8">
            <asp:TextBox ID="txtHow" runat="server" CssClass="form-control input-md"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="textinput">Para que lo hace?</label>
        <div class="col-sm-12 col-md-8">
            <asp:TextBox ID="txtWhy" runat="server" CssClass="form-control input-md"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="textinput">Tiempo que emplea para hacerlo</label>
        <div class="col-sm-12 col-md-8">
            <asp:TextBox ID="txtTime" runat="server" CssClass="form-control input-md"></asp:TextBox>
        </div>
    </div>
    <div class="form-group">
        <label class="col-sm-12 col-md-4 control-label" for="submitButton"></label>
        <div class="col-sm-12 col-md-8">
            <asp:LinkButton ID="LinkButtonGuardar" runat="server" CssClass="btn btn-labeled btn-success" OnClick="LinkButtonGuardar_Click" Enabled="false"><span class="btn-label"><i class="glyphicon glyphicon-plus"></i></span>Guardar</asp:LinkButton>
            <asp:LinkButton ID="LinkButtonEliminar" runat="server" CssClass="btn btn-labeled btn-danger" OnClick="LinkButtonEliminar_Click" Enabled="false"><span class="btn-label"><i class="glyphicon glyphicon-minus"></i></span>Eliminar</asp:LinkButton>
            <asp:LinkButton ID="LinkButtonPublicar" runat="server" CssClass="btn btn-labeled btn-primary" OnClick="LinkButtonPublicar_Click" Enabled="false"><span class="btn-label"><i class="glyphicon glyphicon-open"></i></span>Publicar</asp:LinkButton>
            <asp:LinkButton ID="LinkButtonImprimir" runat="server" CssClass="btn btn-labeled btn-primary" OnClick="LinkButtonImprimir_Click" Enabled="true"><span class="btn-label"><i class="glyphicon glyphicon-print"></i></span>Imprimir</asp:LinkButton>
            <asp:LinkButton ID="LinkButtonCancelar" runat="server" CssClass="btn btn-labeled btn-default" OnClick="LinkButtonCancelar_Click"><span class="btn-label"><i class="glyphicon glyphicon-remove"></i></span>Cancel</asp:LinkButton>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <h2>Lista de funciones</h2>
            <div class="col-lg-12">
                <input type="search" class="form-control" id="input-search" placeholder="Buscar funcion...">
            </div>
            <div class="searchable-container">
                <asp:Repeater ID="Repeater1" runat="server">
                    <ItemTemplate>
                        <div class="items col-xs-12 col-sm-6 col-md-6 col-lg-6 clearfix">
                            <div class="info-block block-info clearfix">
                                <div class="square-box pull-left">
                                    <span class="glyphicon glyphicon-folder-open glyphicon-lg"></span>
                                </div>
                                <h5>Detalle de función</h5>
                                <h4><b><%# Eval("Funtion") %></b></h4>
                                <p><b>Que hace?</b> <%# Eval("What") %></p>
                                <p><b>Como se hace?</b> <%# Eval("How") %></p>
                                <p><b>Para que lo hace?</b> <%# Eval("Why") %></p>
                                <p><b>Tiempo que emplea para hacerlo:</b> <%# Eval("Time") %></p>
                                <a href="AnalisisPuesto?IDAnalisis=<%# Eval("AnalisisID") %>&IDDetalle=<%# Eval("ID") %>" target="_blank">Editar</a>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
    </div>
</asp:Content>
