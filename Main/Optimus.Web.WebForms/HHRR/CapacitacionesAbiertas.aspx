﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="CapacitacionesAbiertas.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.CapacitacionesAbiertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="treeview" id="treeview5">
        <ul class="list-group">
            <li class="list-group-item node-treeview5 node-selected" style="color: #FFFFFF; background-color: #428bca;" data-nodeid="0"><span class="icon expand-icon glyphicon glyphicon-chevron-right"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>Parent 1</li>
            <li class="list-group-item node-treeview5" style="color: undefined; background-color: undefined;" data-nodeid="5"><span class="icon glyphicon"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>Parent 2</li>
            <li class="list-group-item node-treeview5" style="color: undefined; background-color: undefined;" data-nodeid="6"><span class="icon glyphicon"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>Parent 3</li>
            <li class="list-group-item node-treeview5" style="color: undefined; background-color: undefined;" data-nodeid="7"><span class="icon glyphicon"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>Parent 4</li>
            <li class="list-group-item node-treeview5" style="color: undefined; background-color: undefined;" data-nodeid="8"><span class="icon glyphicon"></span><span class="icon node-icon glyphicon glyphicon-bookmark"></span>Parent 5</li>
        </ul>
    </div>
</asp:Content>
