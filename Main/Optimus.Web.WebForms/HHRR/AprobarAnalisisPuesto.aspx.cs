﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AprobarAnalisisPuesto : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            FrameReport.Src = string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/AnalisisPuesto&positionAnalisis={0}&rc:Parameters=False",ObtenerIDAnalisis().ToString());
        }
        private int ObtenerIDAnalisis()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString["IDAnalisis"].ToString());
            }
            catch (Exception)
            {
            }
            return id;
        }
    }
}