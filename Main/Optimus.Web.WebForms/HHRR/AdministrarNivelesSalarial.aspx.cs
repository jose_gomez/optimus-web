﻿using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarNivelesSalarial : System.Web.UI.Page
    {
        #region Private Fields

        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Intranet.aspx");
        }

        protected void btnNew_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                lblNumero.Text = "0";
                txtName.Text = string.Empty;
                txtDescripcion.Text = string.Empty;
                txtSecuencia.Text = string.Empty;
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                List<string> errores = new List<string>();
                if (validaciones.Count <= 0)
                {
                    errores.Add("<b>" + ex.Message + "</b>");
                }
                else
                {
                    errores = validaciones;
                }
                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-warning";
            }
            catch (Exception ex)
            {
                List<string> errores = new List<string>();
                errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);
            }
        }

        protected void btnSaveCompetencia_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DA.HRSalaryLevel lvl = new DA.HRSalaryLevel();

                lvl.SalaryLevelID = int.Parse(lblNumero.Text);
                lvl.Name = txtName.Text;
                lvl.Descripcion = txtDescripcion.Text;
                lvl.LevelSequence = int.Parse(txtSecuencia.Text);
                lvl.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;

                ServCont.GetPositionsService().Save(lvl);

                ddlNiveles.DataSource = ServCont.GetPositionsService().GetAllSalaryLevels();
                ddlNiveles.DataTextField = "Name";
                ddlNiveles.DataValueField = "SalaryLevelID";
                ddlNiveles.DataBind();

                DA.HRSalaryLevel lvl1 = ServCont.GetPositionsService().GetSalaryLevelByID(int.Parse(ddlNiveles.SelectedValue));
                if (lvl1 != null)
                {
                    lblNumero.Text = string.Format(" {0}", lvl1.SalaryLevelID.ToString());
                    txtName.Text = lvl1.Name;
                    txtDescripcion.Text = lvl1.Descripcion;
                    txtSecuencia.Text = lvl1.LevelSequence.ToString();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                List<string> errores = new List<string>();
                if (validaciones.Count <= 0)
                {
                    errores.Add("<b>" + ex.Message + "</b>");
                }
                else
                {
                    errores = validaciones;
                }
                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-warning";
            }
            catch (Exception ex)
            {
                List<string> errores = new List<string>();
                errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);
            }
        }

        protected void ddlNiveles_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DA.HRSalaryLevel lvl = ServCont.GetPositionsService().GetSalaryLevelByID(int.Parse(ddlNiveles.SelectedValue));
                if (lvl != null)
                {
                    lblNumero.Text = string.Format(" {0}", lvl.SalaryLevelID.ToString());
                    txtName.Text = lvl.Name;
                    txtDescripcion.Text = lvl.Descripcion;
                    txtSecuencia.Text = lvl.LevelSequence.ToString();
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                List<string> errores = new List<string>();
                if (validaciones.Count <= 0)
                {
                    errores.Add("<b>" + ex.Message + "</b>");
                }
                else
                {
                    errores = validaciones;
                }
                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-warning";
            }
            catch (Exception ex)
            {
                List<string> errores = new List<string>();
                errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-danger";
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    ddlNiveles.DataSource = ServCont.GetPositionsService().GetAllSalaryLevels();
                    ddlNiveles.DataTextField = "Name";
                    ddlNiveles.DataValueField = "SalaryLevelID";
                    ddlNiveles.DataBind();

                    DA.HRSalaryLevel lvl = ServCont.GetPositionsService().GetSalaryLevelByID(int.Parse(ddlNiveles.SelectedValue));
                    if (lvl != null)
                    {
                        lblNumero.Text = string.Format(" {0}", lvl.SalaryLevelID.ToString());
                        txtName.Text = lvl.Name;
                        txtDescripcion.Text = lvl.Descripcion;
                        txtSecuencia.Text = lvl.LevelSequence.ToString();
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                List<string> errores = new List<string>();
                if (validaciones.Count <= 0)
                {
                    errores.Add("<b>" + ex.Message + "</b>");
                }
                else
                {
                    errores = validaciones;
                }
                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-warning";
            }
            catch (Exception ex)
            {
                List<string> errores = new List<string>();
                errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-danger";
            }
        }

        #endregion Protected Methods
    }
}