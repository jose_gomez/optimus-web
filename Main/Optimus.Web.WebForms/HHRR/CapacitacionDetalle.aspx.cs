﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class CapacitacionDetalle : System.Web.UI.Page
    {
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();
        protected void Page_Load(object sender, EventArgs e)
        {
            var participantes = ServCont.GetSesion().GetParticipantes(ObtenerID());
            Repeater1.DataSource = participantes;
            Repeater1.DataBind();
        }
        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString["ID"].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect("~/HHRR/Capacitaciones");
            }
            return id;
        }
    }
}