﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarIndicadoresPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_DETALLE_PUESTO = "~/HHRR/DetallePuesto?ID={0}";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string URL_PARAMETER = "ID";
        private DA.HRPosition Posicion;
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ServCont.GetIndicadorDesempenioPosicion().Delete(ObtenerID(), int.Parse(DDLIndicadores.SelectedValue.ToString()));
                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, ObtenerID().ToString()));
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void BttnGuardar_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                double porcentaje;
                if (!double.TryParse(Porcentaje.Value, out porcentaje) || (porcentaje < 0 && porcentaje > 100))
                {
                    throw new Core.Exceptions.ValidationException("Indicar el porcentaje.");
                }
                int id = ObtenerID();
                Posicion = ServCont.GetPositionsService().GetPositionsByID(id);
                DTO.IndicadorDesempenioPosicion indicador = new DTO.IndicadorDesempenioPosicion();
                indicador.PositionID = Posicion.PositionID;
                indicador.IndicatorID = int.Parse(DDLIndicadores.SelectedValue.ToString());
                indicador.Percentage = double.Parse(Porcentaje.Value);
                ServCont.GetIndicadorDesempenioPosicion().Save(indicador);
                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, ObtenerID().ToString()));
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(string.Format(LINK_DETALLE_PUESTO, ObtenerID().ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void DDLIndicadores_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                BuscarPorcentaje();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    int id = ObtenerID();
                    Posicion = ServCont.GetPositionsService().GetPositionsByID(id);
                    LblNombrePuesto.InnerText = Posicion.PositionName;
                    var indicadores = ServCont.GetIndicadorDesempenio().Get();
                    DDLIndicadores.DataSource = indicadores;
                    DDLIndicadores.DataTextField = "Name";
                    DDLIndicadores.DataValueField = "IndicatorID";
                    DDLIndicadores.DataBind();
                    BuscarPorcentaje();
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void BuscarPorcentaje()
        {
            int indicadorID = int.Parse(DDLIndicadores.SelectedValue.ToString());
            DTO.IndicadorDesempenioPosicion indicador = ServCont.GetIndicadorDesempenioPosicion().Get(ObtenerID(), indicadorID);
            if (indicador != null)
            {
                Porcentaje.Value = indicador.Percentage.ToString();
            }
            else
            {
                Porcentaje.Value = "0";
            }
        }

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        #endregion Private Methods

    }
}