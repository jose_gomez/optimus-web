﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Requisicion_de_personal
{
    public partial class ListaRequisicionesPersonal : System.Web.UI.Page
    {
        BC.ServiceContainer ServCont = new BC.ServiceContainer();

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    List<DA.HRPersonnelRequisition> listaRequisicionesAbiertas = ServCont.GetPersonnelRequisitionService().GetAllOpenedRequisitions().OrderBy(x => x.PositionName).ToList();

                    CantidadNuevoPuesto.InnerText = (from a in listaRequisicionesAbiertas
                                                     where a.IsNew == true
                                                     select a).OrderBy(x => x.PositionName).ToList().Count.ToString();

                    gvNuevos.AutoGenerateColumns = false;
                    gvNuevos.DataSource = (from a in listaRequisicionesAbiertas
                                           where a.IsNew == true && (a.StatusID == 27 || a.StatusID == 24)
                                           select a).OrderBy(x => x.PositionName).ToList();

                    gvNuevos.DataBind();

                    CantidadPuestosExistentes.InnerText = (from a in listaRequisicionesAbiertas
                                                           where a.IsNew == false
                                                           select a).OrderBy(x => x.PositionName).ToList().Count.ToString();

                    gvExistentesAprobados.AutoGenerateColumns = false;
                    gvExistentesAprobados.DataSource = (from a in listaRequisicionesAbiertas
                                                        where a.IsNew == false && a.StatusID == 27
                                                        select a).OrderBy(x => x.PositionName).ToList();

                    gvExistentesAprobados.DataBind();

                    ExistentesAprobados.InnerText = (from a in listaRequisicionesAbiertas
                                                     where a.IsNew == false && a.StatusID == 27
                                                     select a).OrderBy(x => x.PositionName).ToList().Count.ToString();

                    gvExistentesPendientes.AutoGenerateColumns = false;
                    gvExistentesPendientes.DataSource = (from a in listaRequisicionesAbiertas
                                                         where a.IsNew == false && a.StatusID == 24
                                                         select a).OrderBy(x => x.PositionName).ToList();
                    gvExistentesPendientes.DataBind();

                    ExistentesPendientes.InnerText = (from a in listaRequisicionesAbiertas
                                                      where a.IsNew == false && a.StatusID == 24
                                                      select a).OrderBy(x => x.PositionName).ToList().Count.ToString();

                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                List<string> errores = new List<string>();
                if (validaciones.Count <= 0)
                {
                    errores.Add("<b>" + ex.Message + "</b>");
                }
                else
                {
                    errores = validaciones;
                }
                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-warning";
            }
            catch (Exception ex)
            {
                List<string> errores = new List<string>();
                errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

                BootstrapMessage.Attributes["class"] = "alert alert-danger";
            }
        }

        protected void gvExistentesAprobados_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //if (e.Row.RowType == DataControlRowType.DataRow)
            //{
            //    HyperLink hl = (HyperLink)e.Row.FindControl("CantidatosLink");

            //    DateTime dateNow = DateTime.Today;
            //    DateTime dateBefore = new DateTime(dateNow.Year,dateNow.Month-1,dateNow.Day==31?30 :dateNow.Day);
                
            //    hl.NavigateUrl = string.Format("{0}&fecha1={1}&fecha2={2}&position={3}", System.Configuration.ConfigurationManager.AppSettings["ReportListadosSolicitudes"].ToString(),  dateBefore.ToString(), dateNow.ToString(), DataBinder.Eval(e.Row.DataItem, "PositionID"));

            //}
        }
    }
}