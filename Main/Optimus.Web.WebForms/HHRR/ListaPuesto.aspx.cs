﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class ListaPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_REGISTRAR_PUESTO = "~/HHRR/RegistrarPuesto.aspx";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnNewComp_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(LINK_REGISTRAR_PUESTO);
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            gvPuestos.DataSource = PositionDataHelper.GetList(Core.OptimusApiUrls.Departamentos_GET_POSICIONES_ACTIVE.Replace("{id}", "20"));
            gvPuestos.DataBind();
        }

        protected void ddlDepartamentosActivos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                gvPuestos.DataSource = PositionDataHelper.GetList(Core.OptimusApiUrls.Departamentos_GET_POSICIONES_ACTIVE.Replace("{id}", ddlDepartamentosActivos.SelectedValue));
                gvPuestos.DataBind();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlDepartamentosInactivos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                grdInactivos.DataSource = PositionDataHelper.GetList(Core.OptimusApiUrls.Departamentos_GET_POSICIONES_INACTIVE.Replace("{id}", ddlDepartamentosActivos.SelectedValue));
                grdInactivos.DataBind();
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    LoadDepartments();
                    LoadPositions();
                    HyperLink2.NavigateUrl = "~/HHRR/OrgChartImg.aspx?ID=53";
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void LoadDepartments()
        {
            ddlDepartamentosActivos.DataSource = DepartamentoDataHelper.GetList(Core.OptimusApiUrls.Departamentos_GET);
            ddlDepartamentosActivos.DataTextField = "Name";
            ddlDepartamentosActivos.DataValueField = "DepartmentID";
            ddlDepartamentosActivos.DataBind();
            ddlDepartamentosInactivos.DataSource = DepartamentoDataHelper.GetList(Core.OptimusApiUrls.Departamentos_GET);
            ddlDepartamentosInactivos.DataTextField = "Name";
            ddlDepartamentosInactivos.DataValueField = "DepartmentID";
            ddlDepartamentosInactivos.DataBind();
        }

        private void LoadPositions()
        {
            gvPuestos.DataSource = PositionDataHelper.GetList(Core.OptimusApiUrls.Posiciones_GET_ACTIVE);
            gvPuestos.DataBind();
            grdInactivos.DataSource = PositionDataHelper.GetList(Core.OptimusApiUrls.Posiciones_GET_INACTIVE);
            grdInactivos.DataBind();
        }

        #endregion Private Methods

    }
}