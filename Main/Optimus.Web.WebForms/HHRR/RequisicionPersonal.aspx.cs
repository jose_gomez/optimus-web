﻿using Optimus.Web.DA;
using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class RequisicionPersonal : System.Web.UI.Page
    {
        #region Private Fields

        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Public Enums

        public enum MessageType { Success, Error, Info, Warning };

        #endregion Public Enums

        #region Protected Methods

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Validar(validaciones);
                HRPersonnelRequisition pr = new HRPersonnelRequisition();
                pr.PersonalRequisitionID = 0;
                pr.IsNew = radioButtonPNuevo.Checked;
                pr.PositionName = radioButtonPNuevo.Checked ? "NUEVA POSICION" : selectPuestos.SelectedItem.Text;
                pr.QuantityEmployees = int.Parse(txtNoEmpleados.Text);
                pr.SuperiorPositionID = int.Parse(ddlPuestos1.SelectedValue);
                pr.DepartmentID = int.Parse(ddlDepartamento.SelectedValue);
                pr.Reason = txtCual.Value;
                pr.Replace = txtReplazaA.Text;
                pr.StatusID = (bool)pr.IsNew ? 45 : 24;
                pr.Contract = RadioButtonList1.SelectedItem.Text;
                pr.Workday = RadioButtonList2.SelectedValue;
                pr.WorkingHours = ddlJornadas.SelectedItem.Text;
                pr.StartDate = DateTime.Parse(txtFechaInicioLabor.Text);
                pr.User = User.Identity.Name;
                pr.Description = txtDescripcion.Text;
                int id = ServCont.GetPersonnelRequisitionService().SaveReturnID(pr);
                List<string> mensajes = new List<string>();
                mensajes.Add("Registro guardado.");
                string guid = string.Empty;
                if ((bool)pr.IsNew)
                {
                    guid = GenerarTickNuevaPosicion(pr, id);
                }
                else
                {
                    guid = GenerarTickPosicionExistente(pr, id);
                }
                pr.guid = new Guid(guid);
                pr.PersonalRequisitionID = id;
                ServCont.GetPersonnelRequisitionService().Save(pr);
                mensajes.Add(string.Format("Se ha generado un tique: {0}", guid));
                BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(mensajes);
                BootstrapMessage.Attributes["class"] = "alert alert-success";
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        private string GenerarTickPosicionExistente(HRPersonnelRequisition pr, int id)
        {
            return ServCont.GetUnitOfWork().TSTickets.SubmitTicket("REQUISICION DE PERSONAL", string.Format(
                                "Se genero una requision de personal para el puesto {0} para {1} empleado(s) en el departamento de {2}. Para mas detalles <a href='/HHRR/DetalleRequisicionPersonal?ID={3}' target=\"_blank\">Aquí</a>"
                                , pr.PositionName
                                , pr.QuantityEmployees
                                , ddlDepartamento.Items[ddlDepartamento.SelectedIndex].Text
                                , id.ToString())
                                , ServCont.GetUnitOfWork().User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID
                                , 25
                                , 2
                                , true
                                , false);
        }

        private string GenerarTickNuevaPosicion(HRPersonnelRequisition pr, int id)
        {
            string guid = ServCont.GetUnitOfWork().TSTickets.SubmitTicket("REQUISICION DE PERSONAL (NUEVA POSICION)", string.Format(
                                "Se genero una requision de personal para una {0} para {1} empleado(s) en el departamento de {2}. Para mas detalles <a href='/HHRR/DetalleRequisicionPersonal?ID={3}' target=\"_blank\" >Aquí</a>"
                                , pr.PositionName
                                , pr.QuantityEmployees
                                , ddlDepartamento.Items[ddlDepartamento.SelectedIndex].Text
                                , id.ToString())
                                , ServCont.GetUnitOfWork().User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID
                                , 25
                                , 2
                                , true
                                , false);
            int tickID = ServCont.GetUnitOfWork().TSTickets.getTicketIDByGUID(new Guid(guid));
            ServCont.GetUnitOfWork().TSTickets.SubmitTicket("ANALISIS DE PUESTO", string.Format(
                                "Se genero una requision de personal para una {0} para {1} empleado(s) en el departamento de {2}. Se debe crear un <a href='/HHRR/AnalisisPuesto?IDRequisicion={3}' target=\"_blank\" >ANALISIS DE PUESTO</a>"
                                , pr.PositionName
                                , pr.QuantityEmployees
                                , ddlDepartamento.Items[ddlDepartamento.SelectedIndex].Text
                                , id.ToString())
                                , ServCont.GetUnitOfWork().User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID
                                , 25
                                , 2
                                , false
                                , false
                                ,4
                                ,-99
                                ,tickID );
            return guid;
        }

        private void Validar(List<string> validaciones)
        {
            if (string.IsNullOrWhiteSpace(txtNoEmpleados.Text) || int.Parse(txtNoEmpleados.Text) <= 0) validaciones.Add("Debe indicar la cantidad de empleados que solicita.");
            if ((RadioButton2.Checked && string.IsNullOrWhiteSpace(txtReplazaA.Text)) || (RadioButton1.Checked && string.IsNullOrWhiteSpace(txtReplazaA.Text))) validaciones.Add("Debe indicar el empleado que se remplazara.");
            if (!radioButtonPNuevo.Checked && !RadioButton1.Checked && !RadioButton2.Checked && (RadioButton4.Checked && string.IsNullOrWhiteSpace(txtCual.Value))) validaciones.Add("Debe indicar el motivo.");
            DateTime fecha;
            if (!DateTime.TryParse(txtFechaInicioLabor.Text, out fecha)) validaciones.Add("Debe indicar la fecha de inicio de labores.");
            if (validaciones.Count > 0) throw new Optimus.Core.Exceptions.ValidationException("Error");
        }

        protected void ddlDepartamento_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ObtenerPosicionesPorDepartamento();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlJornadas_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                ObtenerHorario();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void ddlPuestos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                HRPosition posicionBuscada = ServCont.GetPositionsService().GetPositionsByID(int.Parse(selectPuestos.SelectedValue));
                if (posicionBuscada != null)
                {
                    HRDeparmentsPosition deparment = ServCont.GetPositionsService().GetDepartmentPosition(posicionBuscada.PositionID);
                    if (deparment != null)
                    {
                        ddlDepartamento.SelectedValue = deparment.DeparmentID.ToString();
                        ddlDepartamento_SelectedIndexChanged(this, new EventArgs());

                        HROrganizationChart superior = ServCont.GetPositionsService().GetOrganizationChart(posicionBuscada.PositionID);
                        if (superior != null)
                        {
                            ddlPuestos1.SelectedValue = superior.SuperiorPositionID.ToString();
                        }
                    }
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    radioButtonPExistente.Checked = true;
                    PrepararParaPuestoExistente();
                    List<HRPosition> listaPosiciones = ServCont.GetPositionsService().GetAllPositions();
                    selectPuestos.DataSource = listaPosiciones;
                    selectPuestos.DataValueField = "PositionID";
                    selectPuestos.DataTextField = "PositionName";
                    selectPuestos.DataBind();
                    txtNoEmpleados.Text = "1";
                    ddlDepartamento.SetDepartments();
                    ddlPuestos1.DataSource = ServCont.GetPositionsService().GetAllPositions();
                    ddlPuestos1.DataValueField = "PositionID";
                    ddlPuestos1.DataTextField = "PositionName";
                    ddlPuestos1.DataBind();
                    RadioButtonList2.SelectedIndex = 0;
                    List<VwTurnosJornada> listaJornadas = ServCont.GetPersonnelRequisitionService().GetAllJornadas();
                    listaJornadas = listaJornadas.GroupBy(a => a.AutoID).Select(a => a.First()).ToList();
                    ddlJornadas.DataSource = listaJornadas;
                    ddlJornadas.DataValueField = "AutoId";
                    ddlJornadas.DataTextField = "Jornada";
                    ddlJornadas.DataBind();
                    ObtenerHorario();
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (RadioButton1.Checked)
                {
                    txtReplazaA.Enabled = true;
                    txtCual.Value = RadioButton1.Text;
                    txtCual.Disabled = true;
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (RadioButton2.Checked)
                {
                    txtReplazaA.Enabled = true;
                    txtCual.Value = RadioButton2.Text;
                    txtCual.Disabled = true;
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void RadioButton4_CheckedChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (RadioButton4.Checked)
                {
                    txtReplazaA.Enabled = true;
                    txtCual.Value = RadioButton4.Text;
                    txtCual.Disabled = false;
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void radioButtonPExistente_CheckedChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (radioButtonPExistente.Checked)
                {
                    PrepararParaPuestoExistente();
                }
                else
                {
                    PrepararParaPuestoNuevo();
                }
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        private void PrepararParaPuestoNuevo()
        {
            selectPuestos.SelectedIndex = 0;
            selectPuestos.Enabled = false;
            ddlDepartamento.Enabled = true;
            ddlPuestos1.Enabled = true;
            RadioButton1.Enabled = false;
            RadioButton2.Enabled = false;
            RadioButton4.Enabled = false;
            txtReplazaA.Text = string.Empty;
            txtReplazaA.Enabled = false;
            txtCual.Value = radioButtonPNuevo.Text;
            txtCual.Disabled = true;
        }

        private void PrepararParaPuestoExistente()
        {
            selectPuestos.Enabled = true;
            ddlDepartamento.SelectedIndex = 0;
            ddlDepartamento.Enabled = false;
            ddlPuestos1.SelectedIndex = 0;
            ddlPuestos1.Enabled = false;
            RadioButton1.Enabled = true;
            RadioButton2.Enabled = true;
            RadioButton4.Enabled = true;
            txtReplazaA.Enabled = true;
            RadioButton4.Checked = true;
            txtCual.Disabled = false;
        }

        protected void ShowMessage(string Message, MessageType type)
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), System.Guid.NewGuid().ToString(), "ShowMessage('" + Message + "','" + type + "');", true);
        }

        protected void txtReplazaA_TextChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Label2.Text = ServCont.GetUnitOfWork().Employees.GetEmployeeById(int.Parse(txtReplazaA.Text)).FullName;
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void ObtenerHorario()
        {
            List<VwTurnosJornada> listaJornadas = ServCont.GetPersonnelRequisitionService().GetAllJornadas();
            listaJornadas = (from a in listaJornadas
                             where a.AutoID == int.Parse(ddlJornadas.SelectedValue.ToString())
                             select a).ToList();
            Label1.Text = "";
            foreach (VwTurnosJornada item in listaJornadas)
            {
                Label1.Text +=
                    string.Format("{0}  {1} ", item.DiaSemana, item.Turno);
            }
        }

        private void ObtenerPosicionesPorDepartamento()
        {
            ddlPuestos1.DataSource = ServCont.GetPositionsService().GetPositionsByDeparmentID(int.Parse(ddlDepartamento.SelectedValue));
            ddlPuestos1.DataValueField = "PositionID";
            ddlPuestos1.DataTextField = "PositionName";
            ddlPuestos1.DataBind();
        }

        #endregion Private Methods

        protected void popiRadios_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}