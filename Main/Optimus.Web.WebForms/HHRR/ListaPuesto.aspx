﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.ListaPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <%--<script>
  $( function() {
    $( "#dialog-message" ).dialog({
      modal: true,
      buttons: {
        Ok: function() {
          $( this ).dialog( "close" );
        }
      }
    });
  } );
  </script>--%>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Organigrama</h3>
        </div>
        <div class="panel-body">
            <img src="OrgChartImg.aspx" style="width: 100%" />
        </div>
        <div class="panel-footer">
            <ul class="pager">
                <li class="next">
                    <asp:HyperLink ID="HyperLink2" runat="server">Ver Organigrama</asp:HyperLink></li>
            </ul>
        </div>
    </div>
    <div class="panel">
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#activosContent">Activos</a></li>
            <li><a data-toggle="tab" href="#inactivosContent">Inactivos</a></li>
        </ul>
        <div class="tab-content">
            <div id="activosContent" class="tab-pane fade in active">
                <h3>Posiciones activas</h3>
                <asp:DropDownList ID="ddlDepartamentosActivos" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlDepartamentosActivos_SelectedIndexChanged"></asp:DropDownList>
                <asp:GridView ID="gvPuestos" CssClass="table table-hover table-condensed medium-top-margin col-sx-12" runat="server" AutoGenerateColumns="False" BorderStyle="None">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <a href='/HHRR/DetallePuesto?ID=<%# Eval("PositionID") %>'><%# Eval("PositionName") %></a>
                            </ItemTemplate>
                            <ControlStyle BorderStyle="None" />
                            <HeaderStyle Font-Size="Large" HorizontalAlign="Justify" VerticalAlign="Middle" BorderStyle="None" />
                            <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
            <div id="inactivosContent" class="tab-pane fade in">
                <h3>Posiciones inactivas</h3>
                <asp:DropDownList ID="ddlDepartamentosInactivos" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddlDepartamentosInactivos_SelectedIndexChanged"></asp:DropDownList>
                <asp:GridView ID="grdInactivos" CssClass="table table-hover table-condensed medium-top-margin col-sx-12" runat="server" AutoGenerateColumns="False" BorderStyle="None">
                    <Columns>
                        <asp:TemplateField HeaderText="Nombre">
                            <ItemTemplate>
                                <a href='/HHRR/DetallePuesto?ID=<%# Eval("PositionID") %>'><%# Eval("PositionName") %></a>
                            </ItemTemplate>
                            <ControlStyle BorderStyle="None" />
                            <HeaderStyle Font-Size="Large" HorizontalAlign="Justify" VerticalAlign="Middle" BorderStyle="None" />
                            <ItemStyle BorderStyle="None" />
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    <div class="panel-footer" style="text-align: right">
        <asp:Button ID="btnNewComp" CssClass="btn btn-primary" runat="server" Text="Nuevo" OnClick="btnNewComp_Click" />
    </div>

    <%--<div id="dialog-message" title="Download complete">
  <p>
    <span class="ui-icon ui-icon-circle-check" style="float:left"></span>
    Your files have downloaded successfully into the My Downloads folder.
  </p>
  <p>
    Currently using <b>36% of your storage space</b>.
  </p>
</div>--%>
</asp:Content>