﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequisicionPersonal.aspx.cs" EnableEventValidation="false" Inherits="Optimus.Web.WebForms.RequisicionPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <link href="../Content/bootstrap-select.min.css" rel="stylesheet" />
    <script src="../Scripts/bootstrap-select.min.js"></script>
    <style type="text/css">
        .espaciocompleto {
            max-width: 100%;
            width: 100%;
        }
    </style>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title">Requisición de personal</h3>
        </div>
        <div class="panel-body">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">Instrucciones
                    </h3>
                </div>
                <div class="panel-body">
                    <p>
                        <b>Señores Gerentes:</b> Recuerden al realizar su requisición de personal con un <b>MINIMO 10 DIAS DE ANTICIPACIÓN</b>, a la fecha en que debe ser cubierta la plaza solicitada, de manera que el Departamento de Gestión Humana pueda realizar el procedimiento de reclutamiento, selección y contratación según lo establecido.
                    </p>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Datos de la requisición</h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <asp:RadioButton ID="radioButtonPExistente" runat="server" Text=" Puesto existente" GroupName="tipoRequi" Checked="true" OnCheckedChanged="radioButtonPExistente_CheckedChanged" AutoPostBack="true" />
                        <asp:RadioButton ID="radioButtonPNuevo" runat="server" Text=" Puesto nuevo" GroupName="tipoRequi" OnCheckedChanged="radioButtonPExistente_CheckedChanged" AutoPostBack="true" />
                    </div>
                    <div class="form-group">
                        <label for="selectPuestos">Puesto:</label>
                        <asp:DropDownList ID="selectPuestos" runat="server" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlPuestos_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="txtNoEmpleados">Cantidad de empleados requeridos:</label>
                        <asp:TextBox ID="txtNoEmpleados" runat="server" CssClass="form-control espaciocompleto" TextMode="Number"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="ddlDepartamento">Área:</label>
                        <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="selectpicker form-control" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="ddlPuestos1">Dependencia:</label>
                        <asp:DropDownList ID="ddlPuestos1" runat="server" CssClass="selectpicker form-control"></asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Motivo de la requisición
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <asp:RadioButton ID="RadioButton1" runat="server" GroupName="radioMotivo" OnCheckedChanged="RadioButton1_CheckedChanged" AutoPostBack="true" Text=" Retiro / Renuncia empleado" />
                        <asp:RadioButton ID="RadioButton2" runat="server" GroupName="radioMotivo" OnCheckedChanged="RadioButton2_CheckedChanged" AutoPostBack="true" Text=" Reemplazo por maternidad / incapacidad" />
                        <asp:RadioButton ID="RadioButton4" runat="server" GroupName="radioMotivo" OnCheckedChanged="RadioButton4_CheckedChanged" AutoPostBack="true" Text=" Otro" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Otro motivo:</label>
                        <input type="text" class="form-control espaciocompleto" id="txtCual" runat="server" placeholder="Motivo" maxlength="250">
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="txtReplazaA">Código del empleado a remplazar:</label>
                        <asp:TextBox ID="txtReplazaA" runat="server" CssClass="form-control espaciocompleto" OnTextChanged="txtReplazaA_TextChanged" AutoPostBack="True"></asp:TextBox>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="Label2" Text="" runat="server" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="txtReplazaA" EventName="TextChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Tiempo de vinculación requerido:</label>
                        <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                            <asp:ListItem Selected="True">Indefinido.</asp:ListItem>
                            <asp:ListItem>Temporal.</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Jornada laboral:</label>
                        <asp:RadioButtonList ID="RadioButtonList2" runat="server">
                            <asp:ListItem Selected="True">Tiempo completo.</asp:ListItem>
                            <asp:ListItem>Medio tiempo.</asp:ListItem>
                        </asp:RadioButtonList>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Horario laboral requerido:</label>
                        <asp:DropDownList ID="ddlJornadas" CssClass="selectpicker form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlJornadas_SelectedIndexChanged"></asp:DropDownList>
                        <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
                            <ContentTemplate>
                                <asp:Label ID="Label1" Text="" runat="server" />
                            </ContentTemplate>
                            <Triggers>
                                <asp:AsyncPostBackTrigger ControlID="ddlJornadas" EventName="SelectedIndexChanged" />
                            </Triggers>
                        </asp:UpdatePanel>
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fecha de inicio de labores:</label>
                        <asp:TextBox runat="server" class="form-control datepicker espaciocompleto" ID="txtFechaInicioLabor" />
                    </div>
                    <div class="form-group">
                        <label class="control-label">Fecha de inicio de labores:</label>
                        <asp:TextBox runat="server" class="form-control datepicker espaciocompleto" ID="txtDescripcion" MaxLength="500" />
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSave_Click" />
        </div>

    </div>

</asp:Content>
