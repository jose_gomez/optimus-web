﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DetallePuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.DetallePuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <legend>
        <h2><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Descripción de puesto</h2>
    </legend>
    <div class="col-xs-12">
        <h3>Nombre de la posición:</h3>
    </div>
    <div class="col-xs-12">
        <asp:Label ID="lblPuestoName" Text="" runat="server" />
    </div>
    <div class="col-xs-12">
        <h3>Departamento:</h3>
    </div>
    <div class="col-xs-12">
        <asp:Label ID="lblDepartamento" Text="" runat="server" />
    </div>
    <div class="col-xs-12">
        <h3>Superior:</h3>
    </div>
    <div class="col-xs-12">
        <asp:Label ID="lblSuperior" Text="" runat="server" />
    </div>
    <div class="col-xs-12">
        <h3>Descripción:</h3>
    </div>
    <div class="col-xs-12">
        <asp:Label ID="lblDescripcion" Text="" runat="server" />
    </div>
    <ul class="pager">
        <li class="previous"><a href="/HHRR/ListaPuesto.aspx">Atrás</a></li>
        <li class="next">
            <asp:HyperLink ID="HyperLink1" runat="server">Editar</asp:HyperLink></li>
    </ul>
    <legend>
        <h2><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Ubicación del puesto dentro de la estructura organizacional</h2>
    </legend>
    <img id="OrgChartImg" style="width: 100%" runat="server" />
    <ul class="pager">
        <li class="previous"><a href="/HHRR/ListaPuesto.aspx">Atrás</a></li>
        <li class="next">
            <asp:HyperLink ID="HyperLink5" runat="server">Ver Organigrama</asp:HyperLink></li>
    </ul>
    <legend>
        <h2><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Responsabilidades y tareas especificas del puesto</h2>
    </legend>
    <asp:Repeater ID="AnalisisDetalles" runat="server">
        <ItemTemplate>
            <p>
                <%# Eval("What") %>
            </p>
        </ItemTemplate>
    </asp:Repeater>
    <ul class="pager">
        <li class="previous"><a href="/HHRR/ListaPuesto.aspx">Atrás</a></li>
        <li class="next">
            <asp:HyperLink ID="HyperLink3" runat="server">Editar</asp:HyperLink></li>
    </ul>
    <legend>
        <h2><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Indicador de desempeño para el puesto</h2>
    </legend>
    <asp:Repeater ID="Repeater1" runat="server">
        <ItemTemplate>
            <h4 style="vertical-align:middle"><%#Eval("Name") %>&nbsp;<span class="badge"><%#Eval("Percentage") %> %</span></h4>
            <p><%#Eval("Description") %></p>
        </ItemTemplate>
    </asp:Repeater>
    <ul class="pager">
        <li class="previous"><a href="/HHRR/ListaPuesto.aspx">Atrás</a></li>
        <li class="next">
            <asp:HyperLink ID="HyperLink6" runat="server">Editar</asp:HyperLink></li>
    </ul>
    <legend>
        <h2><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Compensación y beneficios para el puesto</h2>
    </legend>
    <asp:Repeater ID="Repeater4" runat="server">
        <ItemTemplate>
            <h4><%# Eval("Name") %></h4>
            <p><%# Eval("Descripcion") %></p>
        </ItemTemplate>
    </asp:Repeater>
    <asp:GridView ID="gvMatriz" runat="server" Width="100%" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
        <Columns>
            <asp:BoundField DataField="MarcoGeneral" HeaderText="Marco general" />
            <asp:BoundField DataField="D" HeaderText="D">
                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="C" HeaderText="C">
                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="PROMEDIO" HeaderText="PROMEDIO">
                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="B" HeaderText="B">
                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
            <asp:BoundField DataField="A" HeaderText="A">
                <HeaderStyle Font-Size="Medium" HorizontalAlign="Center" VerticalAlign="Middle" />
                <ItemStyle HorizontalAlign="Right" />
            </asp:BoundField>
        </Columns>
        <EditRowStyle BackColor="#999999" />
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <SortedAscendingCellStyle BackColor="#E9E7E2" />
        <SortedAscendingHeaderStyle BackColor="#506C8C" />
        <SortedDescendingCellStyle BackColor="#FFFDF8" />
        <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
    </asp:GridView>
    <ul class="pager">
        <li class="previous"><a href="/HHRR/ListaPuesto.aspx">Atrás</a></li>
        <li class="next">
            <asp:HyperLink ID="HyperLink4" runat="server">Editar</asp:HyperLink></li>
    </ul>
    <legend>
        <h2><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;Competencias</h2>
    </legend>
    <asp:Repeater ID="Repeater2" runat="server" OnItemDataBound="Repeater2_ItemDataBound">
        <ItemTemplate>
            <h3><%#Eval("Name") %></h3>
            <asp:Repeater ID="Repeater3" runat="server">
                <ItemTemplate>
                    <h4><%#Eval("Name") %>&nbsp;<span class="badge"><%#Eval("Level") %>/10</span></h4>
                    <p><%#Eval("Description") %></p>
                </ItemTemplate>
            </asp:Repeater>
        </ItemTemplate>
    </asp:Repeater>
    <ul class="pager">
        <li class="previous"><a href="/HHRR/ListaPuesto.aspx">Atrás</a></li>
        <li class="next">
            <asp:HyperLink ID="HyperLink2" runat="server">Editar</asp:HyperLink></li>
    </ul>
</asp:Content>
