﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CandidateBank
{
    public partial class FileUpLoadCandidate : System.Web.UI.Page
    {
        private ServiceContainer container;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack){
                divContent.Visible = false;            
            }
           
        }
        protected void Buscar(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            Candidates candidate = new Candidates();
            candidate =  container.getSolicitudesServices().GetCandidates(CedulaTextBox.Text);
            nombresLabel.Text = candidate.FirstName + " " + candidate.LastName;
            SexoLabel.Text = candidate.Sex == "M" ? "Masculino" : "Femenino";
            CorreoLabel.Text = candidate.Email;
            TelefonoLabel.Text = candidate.PhoneNumber;
            CelularLabel.Text = candidate.MovilNumber;
            CedulaTextBox.Text = candidate.Cedula;

            var pic = container.getSolicitudesServices().GetPictureCandidate(CedulaTextBox.Text);
            if (pic != null)
            {
                var img = GUIHelpers.ImagesHelper.byteArrayToImage(pic.PictureCadidate);

                ImageCan.Src = "data:image/jpg;base64," + ImageToBase64(img, ImageFormat.Jpeg);
            }
            
            divContent.Visible = true;


        }
        protected void Redirect(object sender,EventArgs e)
        {
            Response.Redirect("http://drl-it-testing/ReportServer/Pages/ReportViewer.aspx?/SolicitudesByCedula&cedula=" + CedulaTextBox.Text);
        }
        protected void Curriculum(object sender,EventArgs e)
        {
            container = new ServiceContainer();
            var   cv = container.getSolicitudesServices().GetCandidateGUIDCurriculum(CedulaTextBox.Text);
            if (cv != null)
            {
                string FilePath = "C:\\Repositories\\Optimus-Web\\Development\\Optimus.Web.WebForms\\petition\\PDFFile\\" + cv.guid + ".pdf"; ////  Server.MapPath("PDFFile") + "\\" + cv.guid + ".pdf";
                WebClient User = new WebClient();
                Byte[] FileBuffer = User.DownloadData(FilePath);
                if (FileBuffer != null)
                {
                    Response.ContentType = "application/pdf";
                    Response.AddHeader("content-length", FileBuffer.Length.ToString());
                    Response.BinaryWrite(FileBuffer);
                }
            }else
            {
              
            }
           
            }
        protected void Guardar(object sender, EventArgs e)
        {
            if ((FileUpload1.PostedFile != null) && (FileUpload1.PostedFile.ContentLength > 0))
            {
                //  string fn = System.IO.Path.GetFileName(ArchivoFileUpload.PostedFile.FileName);
                container = new ServiceContainer();
                string gui = Guid.NewGuid().ToString();
                string SaveLocation = "C:\\Repositories\\Optimus-Web\\Development\\Optimus.Web.WebForms\\petition\\PDFFile\\" + gui + ".pdf";

                    //Server.MapPath("PDFFile") + "\\" + gui + ".pdf";

                CandidateGUIDCurriculum cv = new CandidateGUIDCurriculum();
                cv.Cedula = CedulaTextBox.Text;
                cv.guid = gui;
                cv.Create_dt = DateTime.Now;
                container.getSolicitudesServices().Save(cv);
                // string SaveLocation = "c:/ineput/Document/CVs/" + fn;
                // Server.MapPath("PDFFile") + "\\" + fn;
                //string a = Server.MapPath(".");
                //string b = Server.MapPath("..");
                //string c = Server.MapPath("~");
                //string d = Server.MapPath("/");
                try
                {
                    FileUpload1.PostedFile.SaveAs(SaveLocation);
                    Response.Write("El archivo se ha cargado.");
                }
                catch (Exception ex)
                {
                    Response.Write("Error : " + ex.Message);

                }
            }

        }

        public string ImageToBase64(System.Drawing.Image image, ImageFormat format)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Convert Image to byte[]
                image.Save(ms, format);
                byte[] imageBytes = ms.ToArray();

                // Convert byte[] to Base64 String
                string base64String = Convert.ToBase64String(imageBytes);
                return base64String;
            }
        }

    }
}