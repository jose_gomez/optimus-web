﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class Intranet : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!this.Page.User.Identity.IsAuthenticated)
            {
                Response.Redirect("~/Default.aspx");
            }
            if (!IsPostBack)
            {
                LinkListadosSolicitudes.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportListadosSolicitudes"].ToString();
                LinkPerfilCompetenciasLaborales.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportPerfilCompetenciasLaborales"].ToString();
                LinkSolicitudesByCedula.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportSolicitudesByCedula"].ToString();
                LinkSolicitudesMasivoByRangoFecha.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportSolicitudesMasivoByRangoFecha"].ToString();
                LinkReporteGeneralAuditoria.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportReporteGeneralAuditoria"].ToString();
                LinkPendienteParcializarPorSemanas.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportPendienteParcializarPorSemanas"].ToString();
                LinkProcesadoEnCadena.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportProcesadoEnCadena"].ToString();
                LinkProcesadoPorSemanas.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportProcesadoPorSemanas"].ToString();
                LinkTrabajosAbiertos.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportTrabajosAbiertos"].ToString();
                LinkEficiencia.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportEficiencia"].ToString();
                LinkPagoDeProduccion.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportPagoDeProduccion"].ToString();
                LinkDetalleTrabajos.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportDetalleTrabajos"].ToString();
                LinkTrabajosAbiertosNoParcializadosPorSemana.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportTrabajosAbiertosNoParcializadosPorSemana"].ToString();
                LinkTrabajosDetenidos.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportTrabajosDetenidos"].ToString();
                LinkTrackingV2.NavigateUrl= System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportTrackingV2"].ToString();
                LinkHorasExtraDepartment.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString()+
                    System.Configuration.ConfigurationManager.AppSettings["ReportHorasExtrasPorDepartamento"].ToString();
                LinkHorasExtraTotalHorasPorSemana.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportHorasExtrasTotalHorasPorSemana"].ToString();
                LinkCalificacion.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() +
                    System.Configuration.ConfigurationManager.AppSettings["ReportCalificaciones"].ToString();
                LinkReporteCumpleano.NavigateUrl = System.Configuration.ConfigurationManager.AppSettings["ReportServer"].ToString() + 
                    System.Configuration.ConfigurationManager.AppSettings["ReportCumpleaños"].ToString();


            }
        }

        protected void lnkSolicitar_Click(object sender, EventArgs e)
        {
            Server.Transfer("/petition/Solicitudes.aspx");
        }
        protected void Redirect(object sender, EventArgs e)
        {
            Response.Redirect("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/Sistemas/ReporteTickets");
                }
        protected void RedirectAudit(object sender, EventArgs e)
        {
            Response.Redirect("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/Auditoria/AuditoriaGraficos");
        }
        protected void RedirectAuditFecha(object sender, EventArgs e)
        {
            Response.Redirect("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/Auditoria/AuditoriaGraficosRangoFecha");
        }
        protected void RedirectTotalCena(object sender,EventArgs e)
        {
            Response.Redirect("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/ReporteTotalComida");
        }
    }
   
}