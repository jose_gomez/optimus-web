﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ExtraHoursManager.aspx.cs" Inherits="Optimus.Web.WebForms.ExtraHours.ExtraHoursManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>


    </script>
    <div id="container">
      
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Listado de horas extras</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <b>Buscar por Fecha: </b><asp:TextBox runat="server" ID="FechaBuscarTextBox" CssClass="datepicker" OnTextChanged="cambiarFecha" AutoPostBack="true" ></asp:TextBox>
                </div>
                <br />
                <div class="row">
                    <asp:Repeater runat="server" ID="ListadoHoraRepeter" OnItemDataBound="ListadoHoraRepeter_ItemDataBound"  >
                        <ItemTemplate>
                            <div class="panel panel-default col-md-10" style="position:relative;left:10px;padding:5px">
                                <div class="panel-body">
                                    <div class="col-md-1"></div>
                                    <div class="col-md-10">
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" Font-Bold="true" Text="Fecha:"></asp:Label>
                                                <asp:Label runat="server" ID="FechaLabel"  Text='<%#Eval("Fecha")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" Font-Bold="true" Text="Estado:"></asp:Label>
                                                <asp:Label runat="server" ID="StatusLabel"  Text='<%#Eval("StatusName")%>'></asp:Label>
                                            </div>
                                             <div class="col-md-4">
                                                <asp:LinkButton runat="server"  Text="Aprobar" CommandName='<%#Eval("Fecha")%>' OnCommand="Aprobar" id="AprobarButton" CommandArgument='<%#Eval("Fecha")%>'></asp:LinkButton>                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <asp:Label runat="server" Font-Bold="true" Text="Total Empleado:"></asp:Label>
                                                <asp:Label runat="server" Text='<%#Eval("NoEmpleado")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:Label runat="server" Font-Bold="true" Text="Total Departamento:"></asp:Label>
                                                <asp:Label runat="server" Text='<%#Eval("NoDepartamento")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <%--string URL = string.Format("http://" + "drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/Requsiciones/RequisicionCompra&requisition={0}&rc:Parameters=False&rs:Format=PDF", requisitionID);--%> 
                                               <a  onClick="window.open(this.href, this.target, 'width=900,height=500');return false;"; target="popup" 
                                                 href='<%# string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/ListadoHorasExtra&Fecha={0}",Eval("Fecha"))%>' >ver detalles</a>
                                                    <%-- href='<%# string.Format("http://drl-it-testing//ReportServer/Pages/ReportViewer.aspx?/ListadoHorasExtra&Fecha={0}",Eval("Fecha"))%>' >ver detalles</a>--%>
                                            </div>
                                           
                                        </div>
                                        <div class="row">
                                                <div class="col-md-4">
                                                   <asp:Label runat="server" Font-Bold="true" Text="Total Comida:"></asp:Label>
                                                   <asp:Label runat="server" Text='<%#Eval("CantidadComida")%>'></asp:Label>
                                                </div>
                                            <div class="col-md-4">                                                 
                                                   <asp:CheckBox runat="server"  Text="Enviar correo:" ID="EnviarCorreoCheckbox" TextAlign="Left" />
                                                </div>
                                            </div>
                                    </div>
                                    <div class="col-md-1"></div>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>       
                </div>
            </div>
        </div>
    </div>
</asp:Content>
