﻿<%@ Page Title="Información General" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="Optimus.Web.WebForms.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %>.</h2>
    <h3>DRL Manufacturing, S.A.</h3>
    <p>
        Nuestro compromiso como organización está enfocado en la calidad de los productos que ofrecemos, y en el servicio que brindamos a nuestros clientes internos y externos.
        Servicio que solo puede ser obtenido gracias a el compromiso de nuestro personal, individuos trabajadores y comprometidos con la satisfacción completa de nuestros cliente y 
        en mejorar los procesos de la organización, demostrando día a día en su trabajo valores como motivación, vocación y pasión por nuestro trabajo.
    </p>
    <h4>Misión</h4>
    <p>
        Ser una compañía que genere beneficios a sus accionistas, colaboradores, clientes y a la comunidad donde operamos así como seguir nuestros valores empresariales dentro del 
        cumplimiento del marco legal, ético, técnico y de los mas altos estándares en materia de gestión de nuestros recursos humanos, calidad, seguridad, salud, 
        ambiente y de responsabilidad social.
    </p>
    <h4>Visión</h4>
    <p>
        Convertirnos en una empresa destaca y reconocida por las buenas practicas técnicas y operacionales en las actividades de fabricación de joyería en grandes volúmenes para 
        exportación en el sector de Zonas Francas de la República Dominicana.
    </p>
    <h4>Valores</h4>

    <ul>
        <li>
            <h5>Honestidad </h5>
            <p>
                Actuamos de cara a la verdad, con rectitud y en consonancia de lo que decimos y hacemos en cualquier actividad frente a los colaboradores,
                proveedores, suplidores y clientes.
            </p>
        </li>
        <li>
            <h5>Creatividad / Innovación</h5>
            <p>
                Tenemos la iniciativa e ingenio para mejorar nuestros productos, procesos, operaciones y optimizar el desempeño de nuestra gente.
            </p>
        </li>
        <li>
            <h5>Transparencia</h5>
            <p>
                Guiamos el accionar de la empresa y nuestros colaboradores dentro del marco de ética, confianza y el deber de hacer rendición de cuentas
                sobre los recursos de la compañía asignados.
            </p>
        </li>
        <li>
            <h5>Compromiso</h5>
            <p>
                Realizamos nuestra labores con la mejor actitud de servicio y en forma segura, poniendo al máximo nuestra capacidad para entregar resultados que agreguen
                 valor a nuestros clientes y colaboradores. Fomentamos la actualización de los conocimientos de nuestros colaboradores para
                alcanzar mejores resultados que deriven en mayor satisfacción de los clientes y un mejor desempeño de estos.
            </p>
        </li>
        <li>
            <h5>Trabajo en equipo</h5>
            <p>
                Unimos nuestro esfuerzos y compartimos un mismo propósito, conocido por todos, para entregar resultados de valor para nuestra organización y clientes. 
                trabajamos con entusiasmo, manteniendo una comunicación efectiva, aportando diferentes puntos de vista, que permiten engrandecer las ideas y aportes de 
                todos respetando la diversidad.
            </p>
        </li>
        <li>
            <h5>Responsabilidad</h5>
            <p>
                Asumimos los diferentes retos y escenario que se nos presentan con dedicación identificando las oportunidades y somos consientes de los deberes y 
                las consecuencias de nuestros actos. adoptamos sentido de pertenencia optimizando los recursos y minimizando los riesgos en nuestro actuar.
            </p>
        </li>
        <li>
            <h5>Eficacia / Eficiencia</h5>
            <p>
                Ejecutamos nuestras labores conforme a los estándares establecidos justo a tiempo, aprovechando adecuadamente los recursos disponibles.
            </p>
        </li>
        <li>
            <h5>Liderazgo</h5>
            <p>
                Dirigimos nuestro equipo de trabajo logrando que trabajen con entusiasmo conquistando las metas y los objetivos planteados.
            </p>
        </li>
    </ul>

</asp:Content>
