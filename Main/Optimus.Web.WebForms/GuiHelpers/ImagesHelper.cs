﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;

namespace Optimus.Web.WebForms.GUIHelpers
{
   public class ImagesHelper
    {

        public static byte[] ImageToByteArray(Image img)
        {
            byte[] byteArray = new byte[0];
            using (MemoryStream stream = new MemoryStream())
            {
                try
                {
                    img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                }
                catch (Exception ex)
                {
                    string MENSAJE = ex.Message;
                }
               
                stream.Close();

                byteArray = stream.ToArray();
            }
            return byteArray;
        }

        public static Bitmap ResizeImage(Image image, int width, int height)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
                graphics.SmoothingMode = SmoothingMode.HighQuality;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.TileFlipXY);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn.Length == 0)
                return null;
            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            Image img = byteArrayIn!= null? (Image)converter.ConvertFrom(byteArrayIn): null;

            return img;
        }

        public static byte[] GetBytesOfImage(Image img)
        {
            ImageConverter converter = new ImageConverter();
            return (byte[])converter.ConvertTo(img, typeof(byte[]));
        }

        public static void SaveImage(byte[] byteArrayIn, string path, bool isTemp)
        {
            byte[] imagen = byteArrayIn;

            Bitmap bmap = (Bitmap)GUIHelpers.ImagesHelper.byteArrayToImage(imagen);
            string pathString = path;//string pathString = Server.MapPath(path);
            bmap.Save(pathString);
        }

    }
}
