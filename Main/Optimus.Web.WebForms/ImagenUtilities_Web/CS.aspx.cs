﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Drawing;

public partial class CS : System.Web.UI.Page
{
    byte[] returnValue;
    string returnUrl;
    protected void Upload(object sender, EventArgs e)
    {
        string imagen = Request.Form["theHidden"];
        byte[] bytes = Convert.FromBase64String(imagen.Split(',')[1]);
        returnValue = bytes;
        returnUrl = imagen;
    }

    public string CropString(Bitmap thebitmap)
    {
        return this.returnUrl;
    }
    public byte[] CropImage(Bitmap theBitmap)
    {
        return this.returnValue;
    }


}