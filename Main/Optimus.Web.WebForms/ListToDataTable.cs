﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Optimus.Web.WebForms
{
    public static class ListToDataTable
    {
        public static DataTable ToDataTable<TResult>(this IEnumerable<TResult> value) where TResult : class
        {
            List<PropertyInfo> pList = new List<PropertyInfo>();

            Type type = typeof(TResult);
            DataTable dt = new DataTable();

            Array.ForEach<PropertyInfo>(type.GetProperties(), p => { pList.Add(p); dt.Columns.Add(p.Name, p.PropertyType); });
            foreach (var item in value)
            {

                DataRow row = dt.NewRow();

                pList.ForEach(p => row[p.Name] = p.GetValue(item, null));

                dt.Rows.Add(row);
            }
            return dt;
        }
    }
}