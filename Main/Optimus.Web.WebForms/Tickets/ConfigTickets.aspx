﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfigTickets.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.AddCategoryTickets" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $(document).ready(function () {
            $('#anchrdeptheads').click();
            document.querySelector('form').onkeypress = checkEnter;
            if (preventLettersOnKeyPress) {
                document.getElementById("txtcodEncargado").addEventListener("keydown", preventLettersOnKeyPress2);
            }

            getDepartmentHead();
        });

        function validateNewDeptHead() {
            if ($('#txtcodEncargado').val() == undefined || $('#txtcodEncargado').val() == "") {
                alert('Por favor digite un codigo de encargado');
                return false;
            }
        }
        function preventLettersOnKeyPress2(event) {
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 13) {
                if (event.keyCode == 13) {
                    getEmployeeDetails();
                    return false;
                }
            }
            else {
                if (event.keyCode > 95 && event.keyCode < 106) {

                    return;
                }
                if ((event.keyCode < 48 || event.keyCode > 57)) {
                    event.preventDefault();
                }
            }

        }



        function checkEnter(e) {
            e = e || event;
            var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
            return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
        }

        function getDepartmentHead() {
            $('#imgModal').modal();
            $.ajax({

                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetDepartmentHead',
                data: '{Department:\'' + $('#MainContent_drpDepartamento option:selected').text() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#empdetails').empty();
                    if (data.d) {

                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                        $('#empdetails').append("<img alt='Encargado' id='imgPic' />");

                        document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                    }
                    else {
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'> N/A </small> </h5>");
                    }
                    $('#imgModal').modal('hide');
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }

        function getEmployeeDetails() {
            $('#imgModal').modal();
            $.ajax({

                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetEmployeeById',
                data: '{employeeId:' + $('#txtcodEncargado').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data) {
                        $('#empdetails').empty();
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                        $('#empdetails').append("<img alt='Encargado' id='imgPic' />");

                        document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                        $('#imgModal').modal('hide');
                    }
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }

    </script>
    <style>
        @media only screen and (min-width:800px) {
            input[type=text] {
                min-width: 279px;
            }
        }

        #main {
            min-height: 300px;
            padding-top: 5%;
        }

        #departmentHeads {
            padding: 50px;
        }
    </style>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Configuración de Tickets</h3>
            </div>
            <div class="panel-body">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Categorías</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="DepartmentDrop" OnSelectedIndexChanged="DepartmentDrop_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="AccionesButton" class="btn btn-sm" data-toggle="modal" data-target="#AnadirCategoria">Añadir categoría</button>
                                        <button type="button" id="EliminarButton" class="btn btn-sm btn-danger" data-toggle="modal" data-target="#EliminarCategoria">Eliminar</button>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <asp:ListBox runat="server" CssClass="form-control" ID="CategorylistBox"></asp:ListBox>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>Reasignar Tickets</h4>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label runat="server" Font-Bold="true" CssClass="form-label" Text="No.Tickets"></asp:Label>
                                        <asp:TextBox runat="server" CssClass="form-control" ID="NoTickets" OnTextChanged="BuscarTicket" AutoPostBack="true"></asp:TextBox>
                                    </div>
                                </div>
                            </div>
                            <br />
                            <br />
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-8">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label3" Font-Size="Large" Text="Titulo:"></asp:Label>
                                        <asp:Label runat="server" CssClass="form-label" ID="tituloLabel" Font-Size="Large"></asp:Label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label4" Font-Size="Large" Text="Categoría:"></asp:Label>
                                        <asp:Label runat="server" CssClass="form-label" ID="categoriaLabel" Font-Size="Large"></asp:Label>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label5" Font-Size="Large" Text="cambiar a "></asp:Label>
                                    </div>
                                    <div class="col-md-4">                                       
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="CategoriaDropbox" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                               <div class="row">
                                <div class="col-md-6">
                                    <asp:Button runat="server" ID="Button2" Text="Cambiar" CssClass="btn btn-md" OnClick="CambiarCategoria" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label6" Font-Size="Large" Text="Proyecto:"></asp:Label>
                                        <asp:Label runat="server" CssClass="form-label" ID="ProyectoLabel" Font-Size="Large"></asp:Label>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label8" Font-Size="Large" Text="cambiar a "></asp:Label>
                                    </div>
                                    <div class="col-md-4">                                       
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="ProyectoDropDownList" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                               <div class="row">
                                <div class="col-md-6">
                                    <asp:Button runat="server" ID="Button3" Text="Cambiar" CssClass="btn btn-md" OnClick="CambiarProyecto" />
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-4">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label1" Font-Size="Large" Text="Departamento:"></asp:Label>
                                        <asp:Label runat="server" CssClass="form-label" ID="departamentoLabel" Font-Size="Large"></asp:Label>
                                    </div>
                                    <div class="col-md-2">
                                        <asp:Label runat="server" CssClass="form-label" Font-Bold="true" ID="Label2" Font-Size="Large" Text="Mover a"></asp:Label>
                                    </div>
                                    <div class="col-md-4">
                                        <asp:DropDownList runat="server" CssClass="form-control" ID="DepartmentDrop2" AutoPostBack="true"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <asp:Button runat="server" ID="Button1" Text="Cambiar" CssClass="btn btn-md" OnClick="CambiarDepartamento" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a class="collapsed" id="anchrdeptheads" data-toggle="collapse" data-parent="#accordion" href="#departmentHeads">Encargados de departamento</a>
                        </h4>
                    </div>
                    <div style="height: 0px;" id="departmentHeads" class="panel-collapse collapsed">
                        <div class="panel-body">
                            Departamento:
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <asp:DropDownList ID="drpDepartamento" onchange="return getDepartmentHead()" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                            Encargado o Coordinador:
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <asp:TextBox runat="server" ClientIDMode="Static" onChange="return getEmployeeDetails" ID="txtcodEncargado" placeholder="Introduzca un codigo de empleado"></asp:TextBox>
                        </div>
                    </div>
                            <div class="row">
                                <div class="col-sm-4 col-md-4">
                                    <asp:CheckBox ID="EncargadoCheckBox" Text="Encargado" runat="server" />
                                </div>
                                <div class="col-sm-4 col-md-4" id="empdetails">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-4 col-md-4">
                                    <asp:Button class="btn btn-success" runat="server" Text="Guardar" OnClientClick="return validateNewDeptHead();" ID="btnGuardar" OnClick="btnGuardar_Click" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="AnadirCategoria" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Añadir Categoria</h4>
                </div>
                <div class="modal-body">
                    <asp:TextBox runat="server" CssClass="form-control" ID="CategoriaTextBox"></asp:TextBox>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button runat="server" class="btn btn-primary btn-sm" Text="Añadir" OnClick="AnadirCategoria" />
                </div>
            </div>
        </div>
    </div>
    <div id="EliminarCategoria" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header modal-header-danger">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Eliminar Categoría </h4>
                </div>
                <div class="modal-body">
                    <p class="text-warning">Seguro que quiere Eliminar la categoría?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                    <asp:Button runat="server" class="btn btn-danger btn-sm" Text="Eliminar" OnClick="EliminarCategorias" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
