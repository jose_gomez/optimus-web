﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class ProjectDetails : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
       ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            int id = int.Parse(Request.QueryString["ID"]);

            var project = (from p in ContextSQL.TSProjectObjetives
                           join t in ContextSQL.HRDepartments on p.DepartmentID equals t.DepartmentId
                           where p.ProjectObjetivesID == id
                           select  new{ p.Title,p.Description,t.Name,p.EstimatedDate }).FirstOrDefault();

            lblTitulo.Text = project.Title;
            PDescripcion.InnerHtml = project.Description;
            DepartamentoLabel.Text = project.Name;
            FechaEstimadaLabel.Text = project.EstimatedDate.ToString();

            ListView.DataSource = worker.TSTickets.GetObjetivesEspecific(id);
            ListView.DataBind();
        }
    }
}