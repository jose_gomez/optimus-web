﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class AddCategoryTickets : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
       ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            if (!IsPostBack)
            {
                DepartmentDrop.DataSource = worker.dept.GetAll().OrderBy(x=>x.Name);
                DepartmentDrop.DataTextField = "Name";
                DepartmentDrop.DataValueField = "DepartmentID";
                DepartmentDrop.DataBind();
                DepartmentDrop2.DataSource = worker.dept.GetAll().OrderBy(x => x.Name);
                DepartmentDrop2.DataTextField = "Name";
                DepartmentDrop2.DataValueField = "DepartmentID";
                DepartmentDrop2.DataBind();
                drpDepartamento.DataSource = worker.dept.GetAll().OrderBy(x => x.Name);
                drpDepartamento.DataTextField = "Name";
                drpDepartamento.DataValueField = "DepartmentId";
                drpDepartamento.DataBind();
            }
        }

        protected void DepartmentDrop_SelectedIndexChanged(object sender, EventArgs e)
        {
            CategorylistBox.DataSource = worker.TSTickets.getProblemTypeByDeparment(DepartmentDrop.SelectedItem.Text);
            CategorylistBox.DataTextField = "Name";
            CategorylistBox.DataValueField = "ProblemTypeID";
            CategorylistBox.DataBind();
        }
        protected void AnadirCategoria(object sender,EventArgs e)
        {
            int dept = int.Parse(DepartmentDrop.SelectedItem.Value);
            worker.TSTickets.InsertCategoryByDepartement(dept, CategoriaTextBox.Text);
            AlertHelpers.ShowAlertMessage(this.Page,"Exito","Categoria guardara correctamente");
            Response.Redirect(Request.RawUrl);
        }
        protected void EliminarCategorias(object sender, EventArgs e)
        {
            int tr = 0;
            if (int.TryParse(CategorylistBox.SelectedItem.Value, out tr))
            {
                int CatId = int.Parse(CategorylistBox.SelectedItem.Value);
                DA.TSProblemType Probtype = new DA.TSProblemType();
                Probtype = ContextSQL.TSProblemTypes.FirstOrDefault(x => x.ProblemTypeID == CatId);
                if (Probtype != null)
                {
                    ContextSQL.TSProblemTypes.DeleteOnSubmit(Probtype);
                    ContextSQL.SubmitChanges();
                }

                Response.Redirect(Request.RawUrl);
            }
        }
        protected void BuscarTicket(object sender, EventArgs e)
        {
            int o = 0;
            if (int.TryParse(NoTickets.Text,out o))
            {
                int id = int.Parse(NoTickets.Text);
                var ticket = worker.TSTickets.GetTicketsDetailByIdTickets(id).FirstOrDefault();
                if (ticket != null)
                {
                    tituloLabel.Text = ticket.Title;
                    departamentoLabel.Text = ticket.Department;
                    categoriaLabel.Text = ticket.TipoProblema;
                    ProyectoLabel.Text = ticket.ProjectObjetives;
                    CategoriaDropbox.DataSource =  worker.TSTickets.getProblemTypeByDeparment(ticket.Department);
                    CategoriaDropbox.DataTextField = "Name";
                    CategoriaDropbox.DataValueField  = "ProblemTypeID";
                    CategoriaDropbox.DataBind();

                    ProyectoDropDownList.DataSource = worker.TSTickets.GetObjectivesEspecificByDepartment(ticket.IdDepartment);
                    ProyectoDropDownList.DataTextField = "Title";
                    ProyectoDropDownList.DataValueField = "ProjectObjetivesID";
                    ProyectoDropDownList.DataBind();
                }
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Error!", "solo números");
            }
        }
        protected void CambiarDepartamento(object sender, EventArgs e)
        {
            TSTicket ticket = new  TSTicket();

            int o = 0;
            if (int.TryParse(NoTickets.Text,out o))
            {
                int id = int.Parse(NoTickets.Text);
                ticket = worker.TSTickets.Find(x => x.ID == id).FirstOrDefault();
                if (ticket != null)
                {
                    ticket.DepartmentId = int.Parse(DepartmentDrop2.SelectedItem.Value);
                    worker.Complete();
                }
            }          
        }
        protected void CambiarProyecto(object sender, EventArgs e)
        {
            TSTicket ticket = new TSTicket();

            int o = 0;
            if (int.TryParse(NoTickets.Text, out o))
            {
                int id = int.Parse(NoTickets.Text);
                ticket = worker.TSTickets.Find(x => x.ID == id).FirstOrDefault();
                if (ticket != null)
                {
                    ticket.ProjectObjetivesID = int.Parse(ProyectoDropDownList.SelectedItem.Value);
                    worker.Complete();
                }
            }
        }
        protected void CambiarCategoria(object sender, EventArgs e)
        {
            TSTicket ticket = new TSTicket();

            int o = 0;
            if (int.TryParse(NoTickets.Text, out o))
            {
                int id = int.Parse(NoTickets.Text);
                ticket = worker.TSTickets.Find(x => x.ID == id).FirstOrDefault();
                if (ticket != null)
                {
                    ticket.ProblemTypeID = int.Parse(CategoriaDropbox.SelectedItem.Value);
                    worker.Complete();
                }
            }
        }
        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtcodEncargado.Text))
            {
                if (worker == null)
                {
                    container = new ServiceContainer();
                    worker = container.GetUnitOfWork();
                }
                int codigo = 0;
                if (int.TryParse(txtcodEncargado.Text, out codigo))
                {
                    int departmentId = int.Parse(this.drpDepartamento.SelectedValue);
                    DA.Optimus.TSDepartmentHead deptHead = worker.DepartmentHeads.Find(x => x.DepartmentId == departmentId && x.TicketManagerId == codigo).FirstOrDefault();
                    if (deptHead == null)
                    {
                        deptHead = new DA.Optimus.TSDepartmentHead();
                        worker.DepartmentHeads.Add(deptHead);
                    }
                    deptHead.DepartmentId = departmentId;
                    deptHead.TicketManagerId = codigo;
                    deptHead.CreateDate = DateTime.Now;
                    deptHead.Users = User.Identity.Name;
                    deptHead.Manager = EncargadoCheckBox.Checked;
                    if (EncargadoCheckBox.Checked)
                    {
                        DA.Optimus.TSDepartmentHead deptHeadManager = worker.DepartmentHeads.Find(x => x.DepartmentId == departmentId && x.Manager == true).FirstOrDefault();
                        if (deptHeadManager != null)
                        {
                            deptHeadManager.Manager = false;
                            worker.Complete();
                        }
                    }
                    worker.Complete();
                    Response.Redirect(Request.RawUrl);
                    AlertHelpers.ShowAlertMessage(this, "Guardar Encargado", "Registro Guardado");
                }
                else
                {

                }
            }
        }
    }
}