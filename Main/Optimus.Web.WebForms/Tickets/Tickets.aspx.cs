﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA.Optimus;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class Tickets : System.Web.UI.Page
    {
        #region Private Fields

        private ServiceContainer container;
        private UnitOfWork worker;

        #endregion Private Fields

        #region Public Constructors

        public Tickets()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }

        #endregion Public Constructors

        #region Protected Methods

        protected void AutoAsignar(Object sender, CommandEventArgs e)
        {
            int ticketNumber = int.Parse(e.CommandName);
            TicketsDetalle td = new TicketsDetalle();
            if (ticketNumber > 0)
            {
                var ticket = worker.TSTickets.Find(x => x.ID == ticketNumber).FirstOrDefault();
                if (ticket != null)
                {
                    var user = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault();
                    if (user != null && user.EmployeeID != null)
                    {
                        var employee = worker.Employees.Find(x => x.EmployeeId == user.EmployeeID).FirstOrDefault();
                        int employeeId = user.EmployeeID.Value;
                        TSTicketsResponsible Responsible = new TSTicketsResponsible();
                        Responsible.TicketsID = ticket.ID;
                        Responsible.Responsible = employeeId;
                        Responsible.Assigned = true;
                        Responsible.AssignDate = DateTime.Now;
                        worker.ResponsibleTickets.Add(Responsible);
                        ticket.AssignDate = DateTime.Now;
                        ticket.TicketState = 3;
                        CargarGridTodos(employee.DepartmentID.Value, int.Parse(FiltroTodos.SelectedItem.Value), user.UserID, employee.EmployeeId, bool.Parse(isMngHidden.Value));
                        worker.Complete();

                        td.guardarLog(ticket.ID, "Nueva asignación de usuario: <b>" + user.UserName + "</b> por <b>" + User.Identity.Name + "</b>");
                        td.EnviarEmailChanges(ticket.ID, " Sistema de Tickets: Actualización de Responsable", "Nueva asignación de usuario: <b>" + user.UserName + "</b> por <b>" + User.Identity.Name + "</b>");
                        Response.Redirect(Request.RawUrl);

                        //var ticketsPendientes = worker.TSTickets.GetTicketsPendingByDepartment((worker.dept.GetDepartmentsByUsername(this.Page.User.Identity.Name)).DepartmenId);
                        //this.lstTicketsPendientes.DataSource = ((List<Tickestes>)ticketsPendientes).
                        //Select(TickestPendientes => new {TickestPendientes.TicketNumber, TickestPendientes.SubmitDate, TickestPendientes.Department, TickestPendientes.Title, TickestPendientes.Status }).ToList();
                        //this.lstTicketsPendientes.DataBind();
                        AlertHelpers.ShowAlertMessage(this, "Ticket actualizado", "Ticket auto asignado");
                    }
                    else
                    {
                        if (user.EmployeeID == null)
                        {
                            AlertHelpers.ShowAlertMessage(this, "Usuario no vinculado", "Su usuario no esta vinculado a sus detalles de empleado. Por favor contactar a IT.");
                        }
                    }
                }
            }
        }

        protected void BuscarByID(object sender, EventArgs e)
        {
            int id = 0;
            int.TryParse(BuscarID.Text, out id);
            TSTicket tickets = new TSTicket();
            tickets = worker.TSTickets.Find(x => x.ID == id).FirstOrDefault();
            if (tickets != null)
            {
                Response.Redirect(String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", id));
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Tickets no encontrado", "El Tickets no fue encontrado");
            }
        }

        protected void BuscarPalabraClave(object sender, EventArgs e)
        {
            //lstTodosTickets.DataSource = worker.TSTickets.GetTicketsByKEyWord(KeyWordTextBox.Text);
            //lstTodosTickets.DataBind();
            BindTodos();
        }

        protected void CambioFiltro(object sender, EventArgs e)
        {
            CargarGridSubmit(worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID, int.Parse(FiltroMisTickets.SelectedItem.Value));
        }

        protected void CambioFiltroTodos(object sender, EventArgs e)
        {
            KeyWordTextBox.Text = string.Empty;
            BindTodos();
        }

        protected void CompletarPerfil(object sender, EventArgs e)
        {
            if (EmployeeIDTextBox.Text == string.Empty || CorreoTextBox.Text == string.Empty)
            {
                AlertHelpers.ShowAlertMessage(this.Page, "", "Debe completar todos los campos");
                Response.Redirect(Request.RawUrl);
            }
            else
            {
                SYUsers user = worker.User.Find(x => x.UserName == this.User.Identity.Name).FirstOrDefault();
                user.EmployeeID = int.Parse(EmployeeIDTextBox.Text);
                user.Email = CorreoTextBox.Text;
                worker.Complete();
                Response.Redirect(Request.RawUrl);
            }
        }

        protected void lstTickets_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            ((DataPager)lstTickets.FindControl("TicketsPager")).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.DataBind();
        }

        protected void lstTicketsAsignados_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            ((DataPager)lstTicketsAsignados.FindControl("AsignadosPager")).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.DataBind();
        }

        protected void lstTicketsCerrados_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            ((DataPager)lstTicketsCerrados.FindControl("CerradosPager")).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.DataBind();
        }

        protected void lstTicketsPendientes_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            ((DataPager)lstTicketsPendientes.FindControl("PendientesPager")).SetPageProperties(e.StartRowIndex, e.MaximumRows, false);
            this.DataBind();
        }

        protected void lstTodosTickets_PagePropertiesChanging(object sender, PagePropertiesChangingEventArgs e)
        {
            ((DataPager)lstTodosTickets.FindControl("TodosPager")).SetPageProperties(e.StartRowIndex, e.MaximumRows,false);
         this.DataBind();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.Request.Params["__EVENTTARGET"] == "GuardarTickets")
            {
                string dat = Page.Request.Params["__EVENTARGUMENT"].ToString();
                guardarTickets(dat);
            }
            if (Page.Request.Params["__EVENTTARGET"] == "DrpDepartment")
            {
                string dat = Page.Request.Params["__EVENTARGUMENT"].ToString();
            }
            if (this.IsPostBack)
            {
                TabName.Value = Request.Form[TabName.UniqueID];
            }
            if (!IsPostBack)
            {
                var departmentService = container.GetDepartmentService();
                var departments = departmentService.GetAllDepartments();

                ProjectHidden.Value = "false";

                drpDepartamento.DataTextField = "Name";
                drpDepartamento.DataValueField = "DepartmentId";
                drpDepartamento.DataSource = departments;
                drpDepartamento.DataBind();

                PriodidadDropDown.DataSource = worker.TSTickets.GetTicketsPriority();
                PriodidadDropDown.DataTextField = "Priority";
                PriodidadDropDown.DataValueField = "TicketsPriorityID";
                PriodidadDropDown.DataBind();

                ProyectoLabel.DataSource = worker.TSTickets.GetObjectivesEspecificByDepartment(worker.dept.GetDepartmentsByUsername(this.User.Identity.Name).DepartmenId);
                ProyectoLabel.DataTextField = "Title";
                ProyectoLabel.DataValueField = "ProjectObjetivesID";
                ProyectoLabel.DataBind();
            }

                if (this.Page.User.Identity.IsAuthenticated)
                {

                    var user = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault();
                    if (user.EmployeeID == null || user.Email == null)
                    {
                        if (!IsPostBack)
                        {
                            CorreoTextBox.Text = user.Email;
                            EmployeeIDTextBox.Text = user.EmployeeID.ToString();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Pop", "ModalCompletalPerfil();", true);
                        }
                    }
                    else
                    {
                        int dept;
                        string deptN;
                        int employeeId = user.EmployeeID != null ? user.EmployeeID.Value : 0;

                        dept = worker.dept.GetDepartmentsByUsername(this.User.Identity.Name).DepartmenId;
                        deptN = worker.dept.GetDepartmentsByUsername(this.User.Identity.Name).Name;

                        var ticketsStatistics = worker.TSTickets.GetPercentagesOfTicketsStatusesByDepartment(dept);
                        if (ticketsStatistics != null)
                        {
                            validatedTicketsPercentage.Value = ticketsStatistics.Validated.ToString();
                            pendingTicketsPercentage.Value = ticketsStatistics.Pending.ToString();
                            ticketsAbiertos.Value = (ticketsStatistics.Assigned + ticketsStatistics.Pending + ticketsStatistics.Done +
                                ticketsStatistics.Approved + ticketsStatistics.Working).ToString();
                            ticketsListos.Value = ticketsStatistics.Done.ToString();
                            ticketsCancelados.Value = ticketsStatistics.Cancelled.ToString();
                        }

                        bool isMng;
                        int MngID = 0;

                        isMng = worker.DepartmentHeads.IsManagerByUser(this.Page.User.Identity.Name);
                        isMngHidden.Value = isMng.ToString();
                        if (isMng)
                        {
                            MngID = (int)worker.User.Find(x => x.UserName == this.Page.User.Identity.Name).FirstOrDefault().EmployeeID;
                        }
                        try
                        {
                            var tickets = worker.TSTickets.GetTicketsBySubmitUser(user.UserID, 1);
                            this.lstTickets.DataSource = tickets;
                            this.lstTickets.DataBind();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    try
                    {
                        var TicketsParaAproved = worker.TSTickets.GetTicketsPendingAprovedByEmployeeID((int)user.EmployeeID);
                        this.ToprovedListview.DataSource = TicketsParaAproved;
                        this.ToprovedListview.DataBind();

                    }
                    catch (Exception)
                    {

                        throw;
                    }

                        try
                        {
                            var ticketsPendientes = worker.TSTickets.GetTicketsPendingByDepartment(dept, isMng, MngID);
                            this.lstTicketsPendientes.DataSource = ((List<Tickestes>)ticketsPendientes).
                            Select(TickestPendientes => new
                            {
                                TickestPendientes.TicketNumber,
                                TickestPendientes.SubmitDate,
                                TickestPendientes.Description,
                                TickestPendientes.Department,
                                TickestPendientes.Title,
                                TickestPendientes.Status,
                                TickestPendientes.SubmitUserName,
                                TickestPendientes.Priority,
                                TickestPendientes.Requisition,
                                TickestPendientes.TipoProblema
                            }).ToList();
                            this.lstTicketsPendientes.DataBind();
                        }
                        catch (Optimus.Core.Exceptions.ValidationException ex)
                        {
                            DRL.Utilities.ErrorPage.ErrorPages.RedirectToErrorPage(ex.Message);
                        }
                        try
                        {
                            var ticketsAsignados = worker.TSTickets.GetTicketsAssingByUser(this.Page.User.Identity.Name);
                            this.lstTicketsAsignados.DataSource = ((List<Tickestes>)ticketsAsignados).
                                Select(TicketsAsignados => new { TicketsAsignados.TicketNumber, TicketsAsignados.SubmitDate, TicketsAsignados.Department, TicketsAsignados.Title, TicketsAsignados.Status, TicketsAsignados.Priority, TicketsAsignados.TipoProblema }).ToList();
                            this.lstTicketsAsignados.DataBind();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        try
                        {
                            var ticketsCerrados = worker.TSTickets.GetTicketsClosedByDepartment(dept, isMng, MngID);
                            this.lstTicketsCerrados.DataSource = ((List<Tickestes>)ticketsCerrados).
                                Select(TicketsCerrados => new { TicketsCerrados.TicketNumber, TicketsCerrados.SubmitDate, TicketsCerrados.Department, TicketsCerrados.Title, TicketsCerrados.Status, TicketsCerrados.TipoProblema }).ToList();
                            this.lstTicketsCerrados.DataBind();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                        try
                        {

                            BindTodos();
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }
          //  }
              
        }

        protected void Sorting(object sender, ListViewSortEventArgs e)
        {
        }

        #endregion Protected Methods

        #region Private Methods

        private void BindTodos()
        {
            int dept;
            var user = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault();
            dept = worker.dept.GetDepartmentsByUsername(this.User.Identity.Name).DepartmenId;
            CargarGridTodos(dept, int.Parse(FiltroTodos.SelectedItem.Value), user.UserID, (int)user.EmployeeID, bool.Parse(isMngHidden.Value));
        }

        private void CargarGridSubmit(int user, int filtro)
        {
            try
            {
                var tickets = worker.TSTickets.GetTicketsBySubmitUser(user, filtro);
                this.lstTickets.DataSource = tickets;
                this.lstTickets.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void CargarGridTodos(int dept, int filtro, int IdUser, int Responsible, bool isMng)
        {
            try
            {
                var tickets = worker.TSTickets.GetAllTicketsByDepartment(dept, filtro, IdUser, Responsible, bool.Parse(isMngHidden.Value),KeyWordTextBox.Text);
                lstTodosTickets.DataSource = tickets;
                lstTodosTickets.DataBind();
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void EnviarCorreo(int dept, string Subject, string body)
        {
            var Emails = worker.DepartmentHeads.GetEmailsDeparmentHead(dept);
            string CorreoUsuarioSubmit = worker.User.SingleOrDefault(x => x.UserName == this.User.Identity.Name).Email;
            List<MailAddress> correoList = new List<MailAddress>();
            if (CorreoUsuarioSubmit != string.Empty)
            {
                MailAddress MailCorreoSubmit = new MailAddress(CorreoUsuarioSubmit);
                correoList.Add(MailCorreoSubmit);
            }
            foreach (var item in Emails)
            {
                MailAddress mail = new MailAddress(item.Email);
                correoList.Add(mail);
            }
            if (correoList != null)
            {
                DRL.Utilities.Mail.Mailer.SendMail(Subject, correoList, body, true);
            }
        }

        private void guardarTickets(string Descripcion)
        {
            int probID = 0;
            if (string.IsNullOrEmpty(txtTitulo.Text) || string.IsNullOrEmpty(Descripcion))
            {
                AlertHelpers.ShowAlertMessage(this, "Datos Faltantes", "Por favor llenar tanto titulo como descripción");
                return;
            }
            TSTicket ticket = new TSTicket();
            ticket.Referral = Guid.NewGuid().ToString();
            ticket.Title = txtTitulo.Text;
            ticket.Description = Descripcion;
            ticket.DepartmentId = int.Parse(drpDepartamento.SelectedItem.Value);
            ticket.SubmitDate = DateTime.Now;
            var user = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault();
            ticket.SubmitUser = user.UserID;
            ticket.TicketState = 4;
            ticket.PorcentProgress = 0;
            ticket.validated = false;
            ticket.TicketsPriorityID = int.Parse(PriodidadDropDown.SelectedItem.Value);
            ticket.DeparmentIdSubmit = (worker.dept.GetDepartmentsByUsername(this.Page.User.Identity.Name)).DepartmenId;
            ticket.Requisition = false;
            ticket.Approved = false;
            if (int.TryParse(HiddenProblem.Value, out probID))
            {
                ticket.ProblemTypeID = int.Parse(HiddenProblem.Value);
            }
            if (bool.Parse(ProjectHidden.Value))
            {
                if (int.TryParse(ProyectoLabel.SelectedItem.Value,out probID))
                {
                    ticket.ProjectObjetivesID = int.Parse(ProyectoLabel.SelectedItem.Value);
                }
            }
            
            worker.TSTickets.Add(ticket);
            worker.Complete();
            TicketsDetalle td = new TicketsDetalle();
            td.guardarLog(ticket.ID, "Creación del Tickets No." + ticket.ID);

            //var Enc = worker.DepartmentHeads.GetDepartmentHead(drpDepartamento.SelectedItem.Text);

            //string CorreoEncargado = string.Empty;
            //if (Enc != null)
            //{
            //    CorreoEncargado = worker.User.SingleOrDefault(x => x.EmployeeID == Enc.EmployeeId).Email;
            //}
            //string CorreoUsuarioSubmit = worker.User.SingleOrDefault(x => x.UserName == this.User.Identity.Name).Email;
            //List <MailAddress> correo = new List<MailAddress>();
            //if (CorreoEncargado != string.Empty)
            //{
            //    MailAddress MailCorreoEncargado = new MailAddress(CorreoEncargado);
            //    correo.Add(MailCorreoEncargado);
            //}
            //if ( CorreoUsuarioSubmit != string.Empty)
            //{
            //    MailAddress MailCorreoSubmit = new MailAddress(CorreoUsuarioSubmit);
            //    correo.Add(MailCorreoSubmit);
            //}
            //if (correo != null)
            //{
            //    DRL.Utilities.Mail.Mailer.SendMail("Nuevo Tickets", correo, "Nuevo Tickets <br>" + txtTitulo.Text, true);
            // }

            EnviarCorreo(int.Parse(drpDepartamento.SelectedItem.Value), "Nuevo Tickets No:" + ticket.ID, "Nuevo Tickets <br>" + txtTitulo.Text);
            AlertHelpers.ShowAlertMessage(this, "Ticket sometido", "Su ticket ha sido enviado. Puede revisar el estado en el tab 'Mis Tickets'");
            Response.Redirect(Request.RawUrl);
        }
        private void setProblem(string dept)
        {
            drpProblem.DataSource = worker.TSTickets.getProblemTypeByDeparment(dept);
            drpProblem.DataTextField = "Name";
            drpProblem.DataValueField = "ProblemTypeID";
            drpProblem.DataBind();
        }

        #endregion Private Methods
    }
}