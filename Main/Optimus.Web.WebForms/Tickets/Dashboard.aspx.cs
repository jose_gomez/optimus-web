﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Optimus.Web.BC;
using Optimus.Web.BC.Services;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class Dashboard : System.Web.UI.Page
    {
        ServiceContainer container;
        UnitOfWork worker;
        public Dashboard()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var depts = container.GetDepartmentService().GetAllDepartments();
            DeptsDDL.DataSource = depts;
            DeptsDDL.DataTextField = "Name";
            DeptsDDL.DataValueField = "DepartmentId";
            DeptsDDL.DataBind();
        }
    }
}