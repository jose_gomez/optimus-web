﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA.Optimus;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class Maintenance : System.Web.UI.Page
    {
        ServiceContainer container;
        UnitOfWork worker;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                container = new ServiceContainer();
                UnitOfWork worker = container.GetUnitOfWork();
                var departmentService = container.GetDepartmentService();
                var departments = departmentService.GetAllDepartments();
                drpDepartamento.DataTextField = "Name";
                drpDepartamento.DataValueField = "DepartmentId";
                drpDepartamento.DataSource = departments;
                drpDepartamento.DataBind();
            }
          
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(txtcodEncargado.Text))
            {
                if(worker == null)
                {
                    container = new ServiceContainer();
                    worker = container.GetUnitOfWork();
                }
                int codigo = 0;
                if (int.TryParse(txtcodEncargado.Text, out codigo))
                {
                    int departmentId = int.Parse(this.drpDepartamento.SelectedValue);

                    TSDepartmentHead deptHead = worker.DepartmentHeads.Find(x => x.DepartmentId == departmentId && x.TicketManagerId == codigo).FirstOrDefault();
                    if (deptHead == null)
                    {
                        deptHead = new TSDepartmentHead();
                        worker.DepartmentHeads.Add(deptHead);
                    }
                    deptHead.DepartmentId = departmentId;
                    deptHead.TicketManagerId = codigo;
                    deptHead.CreateDate = DateTime.Now;
                    deptHead.Users = User.Identity.Name;
                    deptHead.Manager = EncargadoCheckBox.Checked;
                    if (EncargadoCheckBox.Checked)
                    {
                        TSDepartmentHead deptHeadManager = worker.DepartmentHeads.Find(x => x.DepartmentId == departmentId && x.Manager == true).FirstOrDefault();
                        if (deptHeadManager != null)
                        {
                            deptHeadManager.Manager = false;
                            worker.Complete();
                        }
                        
                    }
                    worker.Complete();
                    AlertHelpers.ShowAlertMessage(this, "Guardar Encargado", "Registro Guardado");
                }
                else
                {

                }
            }
        }
    }
}