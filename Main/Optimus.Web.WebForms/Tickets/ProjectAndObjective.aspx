﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="ProjectAndObjective.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.ProjectAndObjective" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script type="text/javascript">
        $(document).ready(function () {
            initSample();
            $("#MainContent_ProjectHidden").attr("value", "1");
        })
        $(document).ready(function () {
            $("input[name='inlineRadioOptions']").click(function () {
                var checkedValue = $("input[name='inlineRadioOptions']:checked").val();
                console.log(checkedValue);
                if (checkedValue == "1") {
                    $("#collapseOne").collapse('hide');
                    $("#MainContent_ProjectHidden").attr("value", "1");

                } else if (checkedValue == "2") {
                    $("#collapseOne").collapse('show');
                    $("#MainContent_ProjectHidden").attr("value", "2");
                    $("#MainContent_ObjetiveLabel").text("Seleccion Objetivo general");
                    getObjetives(1);

                } else if (checkedValue == "3") {
                    $("#collapseOne").collapse('show');
                    $("#MainContent_ProjectHidden").attr("value", "3");
                    $("#MainContent_ObjetiveLabel").text("Seleccion Objetivo especifico");
                    getObjetives(2);
                } else {
                    console.log("Oops.");
                }
            });
        });
        function GuardarTickets() {
            var checkedValue = $("input[name='inlineRadioOptions']:checked").val();
            if ((checkedValue == "2" || checkedValue == "3") && $('#MainContent_projectoDropdownList option:selected').text() == "-1") {
                alarm("Debe seleccionar un Objetivo o proyecto");
            } else {
                var comment = CKEDITOR.instances.editor.getData();
                __doPostBack('GuardarTickets', comment);
            }
       
        }
       

        function setRestrictDate()
        {
            var year = $('#MainContent_YearDropDownList option:selected').text()
            var Min = new Date(year, 0, 1);
            var Max = new Date(year, 11, 31);
            $('#FechaTextBox').datepicker('option', { minDate: new Date(Min), maxDate: new Date(Max) });
        }
        function getObjetives(Types) {
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetObjetives',
                data: '{Types:\'' + Types + '\',year:' + $('#MainContent_YearDropDownList option:selected').text() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#MainContent_projectoDropdownList').empty().append('<option selected="selected" value="0">Seleccione</option>');
                    $.each(data.d, function () {
                        $('#MainContent_projectoDropdownList').append($("<option></option>").val(this['ProjectObjetivesID']).html(this['Title']));
                    });
                },
                error: function (e) {
                   
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }
            });

        }

    </script>
    <div id="Tabs" role="tabpanel">
        <div id="ticketscontainer">
            <ul class="nav nav-pills nav-justified" id="myTab">
                <li><a data-toggle="tab" role="tab" href="#CrearProject">Crear proyecto</a></li>
                <li class="active"><a data-toggle="tab" role="tab" href="#ProjectObjective">Projectos y Objetivos</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div id="CrearProject" role="tabpanel" class="tab-pane fade">
                <div id="someter">
                    <h2>Project and Objetives</h2>
                    <div id="deptcol">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label runat="server" Text="Título" Font-Bold="true"></asp:Label>
                                    <asp:TextBox runat="server" ClientIDMode="Static" ID="txtTitulo" CssClass="form-control" placeholder="Ingrese una solicitud"></asp:TextBox>
                                </div>
                            </div>
                        </div>
                          <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <asp:Label runat="server" Text="Año:" Font-Bold="true"></asp:Label>
                                    <asp:DropDownList ID="YearDropDownList" CssClass="form-control" runat="server" onchange="return setRestrictDate()"></asp:DropDownList>
                                </div>
                                <div class="col-md-6">
                                    <asp:Label runat="server" Text="Fecha Estimada:" Font-Bold="true"></asp:Label>
                                    <asp:TextBox runat="server" CssClass="form-control datepicker" ID="FechaTextBox" ClientIDMode="static"></asp:TextBox>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioOptions" id="inlineRadio1" value="1" checked>
                                        Objetivo General
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioOptions" id="inlineRadio2" value="2">
                                        Objetivo Especifico
                                    </label>
                                    <label class="radio-inline">
                                        <input type="radio" name="inlineRadioOptions" id="inlineRadio3" value="3">
                                        Proyecto
                                    </label>
                                    <asp:HiddenField runat="server" ID="ProjectHidden" />
                                </div>
                            </div>
                        </div>
                        <div id="collapseOne" class="collapse">
                            <div class="row">
                                <div class="form-group">
                                    <div class="col-md-6">
                                        <asp:Label runat="server" ID="ObjetiveLabel" Text="Objetivo:" Font-Bold="true"></asp:Label>
                                        <asp:DropDownList ID="projectoDropdownList" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                    <div class="col-md-6">
                                        <asp:Label runat="server" Text="Departamento:" Font-Bold="true"></asp:Label>
                                        <asp:DropDownList ID="deparmetDropdownList" CssClass="form-control" runat="server"></asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <asp:Label runat="server" Text="Descripción" Font-Bold="true"></asp:Label>
                                    <div class="adjoined-bottom">
                                        <div class="grid-container">
                                            <div class="grid-width-100">
                                                <div id="editor">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br />
                        <div class="row" id="guardar">
                            <div class="form-group">
                                <div class="col-md-12">
                                   <%--<asp:Button runat="server" ID="btnGuardar" Text="Crear" class="btn btn-primary" ClientIDMode="Static" OnClientClick="return GuardarTickets()" />--%>
                                    <button ID="btnGuardar"  class="btn btn-primary"  onclick="GuardarTickets()">Crear</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div id="empdetails">
                        </div>
                    </div>
                    <div style="display: none">
                        <div id="editor2" style="display: none"></div>
                    </div>
                </div>
            </div>
            <div id="ProjectObjective" role="tabpanel" class="tab-pane fade">
                <asp:ListView ID="lstProject" ClientIDMode="Static" runat="server">
                    <LayoutTemplate>
                        <table class="table table-striped">
                            <thead>
                                <th>Número de project</th>
                                <th>Titulo</th>
                                <th>Estado</th>
                                <th>Año</th>
                                <th>Tipo</th>
                                <th>Porcentaje</th>
                            </thead>
                            <tbody>
                                <tr runat="server" id="itemPlaceholder" />
                            </tbody>
                        </table>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <tr>
                            <td>
                                <asp:Label runat="server" Text='<%# Eval("ProjectObjetivesID") %>'></asp:Label>
                            </td>
                            <td>
                                <asp:LinkButton runat="server" Text='<%# Eval("Title") %>'  PostBackUrl='<%# String.Format("~/Tickets/ProjectDetails.aspx?ID={0}",Eval("ProjectObjetivesID")) %>'></asp:LinkButton></td>
                            <td>
                            </td>                           
                            <td>
                                <asp:Label runat="server" Text='<%#Eval("Year")%>'></asp:Label>
                            </td>
                            <td>
                                <asp:Label runat="server" Text='<%#Eval("ProjectObjetives")%>'></asp:Label>
                            </td>
                            <td>
                                <div class="progress">
                                    <div class="progress-bar progress-bar-striped active" role="progressbar"
                                        aria-valuenow="<%#Eval("PorcentProgress ")%>" aria-valuemin="0" aria-valuemax="100" style='width: <%#Eval("PorcentProgress")+"%" %>'>
                                        <%#Eval("PorcentProgress")+"%"%>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </ItemTemplate>
                </asp:ListView>
            </div>
        </div>
    </div>
</asp:Content>
