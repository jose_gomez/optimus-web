﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class ProjectAndObjective : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
        ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            if (Page.Request.Params["__EVENTTARGET"] == "GuardarTickets")
            {
                string dat = Page.Request.Params["__EVENTARGUMENT"].ToString();
                Crear(dat);
            }
            if (!IsPostBack)
            {
                deparmetDropdownList.DataSource = container.GetDepartmentService().GetAllDepartments();
                deparmetDropdownList.DataTextField = "Name";
                deparmetDropdownList.DataValueField = "DepartmentID";
                deparmetDropdownList.DataBind();

                YearDropDownList.DataSource = years(2012);               
                YearDropDownList.DataBind();

                //projectoDropdownList.DataSource = worker.TSTickets.GetProjects();
                //projectoDropdownList.DataTextField = "Title";
                //projectoDropdownList.DataValueField = "ProjectObjetivesID";
                //projectoDropdownList.DataBind();

                lstProject.DataSource = worker.ProyectObjetive.GetObjetives();
                lstProject.DataBind();
            }
        }

        private List<int> years(int y)
        {
            List<int> year = new List<int>();
            for (int i = DateTime.Now.Year; i <=  DateTime.Now.Year+3; i++)
            {
                year.Add(i);
            }
            return year;
        }

        private void Crear(string Description)
        {
            TSProjectObjetives proy = new TSProjectObjetives();
            proy.Title = txtTitulo.Text;
            proy.EstimatedDate = Convert.ToDateTime(FechaTextBox.Text);
            if (projectoDropdownList.SelectedItem != null)
            {
                if (projectoDropdownList.SelectedItem.Value != "-1")
                {
                    if (ProjectHidden.Value == "2" || ProjectHidden.Value == "3")
                    {
                        proy.FatherProject = int.Parse(projectoDropdownList.SelectedItem.Value);
                    }              
                }
            }
            proy.PorcentProgress = 0;
            if (deparmetDropdownList.SelectedItem.Value != "-1")
            {
                proy.DepartmentID = int.Parse(deparmetDropdownList.SelectedItem.Value);                    
            }
            proy.Year = int.Parse(YearDropDownList.SelectedItem.Value);
            proy.Project = ProjectHidden.Value == "3"?true:false;
            proy.Description = Description;
            proy.StatusID = 24;
            proy.ProjectObjetivesTypesID = int.Parse(ProjectHidden.Value);
            proy.UserID = worker.User.GetUserByUserName(Page.User.Identity.Name).userID;
            proy.Create_dt = DateTime.Now;
            worker.ProyectObjetive.Add(proy);
            worker.Complete();
        }
    }
}