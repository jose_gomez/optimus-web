﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class DepartamentoDataHelper
    {
        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.Departamento Get(string path)
        {
            DTO.Departamento departamento = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                departamento = DTO.Factory.DepartamentoFactory.Deserialize(data);
            }
            return departamento;
        }

        public static List<DTO.Departamento> GetList(string path)
        {
            List<DTO.Departamento> departamentos = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                departamentos = DTO.Factory.DepartamentoFactory.DeserializeList(data);
            }
            return departamentos;
        }

        public static void Save(DTO.Departamento departamento)
        {
        }

        #endregion Public Methods
    }
}