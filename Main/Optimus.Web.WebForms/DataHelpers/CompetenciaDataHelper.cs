﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class CompetenciaDataHelper
    {

        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.Competencia Get(string path)
        {
            DTO.Competencia competencia = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                competencia = DTO.Factory.CompetenciaFactory.Deserialize(data);
            }
            return competencia;
        }

        public static List<DTO.Competencia> GetList(string path)
        {
            List<DTO.Competencia> competencias = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                competencias = DTO.Factory.CompetenciaFactory.DeserializeList(data);
            }
            return competencias;
        }

        public static void Save(DTO.Competencia competencia)
        {
        }

        #endregion Public Methods

    }
}