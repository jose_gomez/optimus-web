﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class CategoriaCompetenciaDataHelper
    {
        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.CategoriaCompetencia Get(string path)
        {
            DTO.CategoriaCompetencia categoria = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                categoria = DTO.Factory.CategoriaCompetenciaFactory.Deserialize(data);
            }
            return categoria;
        }

        public static List<DTO.CategoriaCompetencia> GetList(string path)
        {
            List<DTO.CategoriaCompetencia> categorias = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                categorias = DTO.Factory.CategoriaCompetenciaFactory.DeserializeList(data);
            }
            return categorias;
        }

        public static void Save(DTO.CategoriaCompetencia categoria)
        {
        }

        #endregion Public Methods
    }
}