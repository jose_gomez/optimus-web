﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class NivelCompetenciaDeseadoDataHelper
    {
        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.NivelCompetenciaDeseado Get(string path)
        {
            DTO.NivelCompetenciaDeseado nivel = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                nivel = DTO.Factory.NivelCompetenciaDeseadoFactory.Deserialize(data);
            }
            return nivel;
        }

        public static List<DTO.NivelCompetenciaDeseado> GetList(string path)
        {
            List<DTO.NivelCompetenciaDeseado> niveles = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                niveles = DTO.Factory.NivelCompetenciaDeseadoFactory.DeserializeList(data);
            }
            return niveles;
        }

        public static void Save(DTO.NivelCompetenciaDeseado departamento)
        {
        }

        #endregion Public Methods
    }
}