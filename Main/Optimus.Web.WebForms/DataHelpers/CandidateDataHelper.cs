﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class CandidateDataHelper
    {
        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.Candidate Get(string path)
        {
            DTO.Candidate candidate = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                candidate = DTO.Factory.CandidateFactory.Deserialize(data);
            }
            return candidate;
        }

        public static List<DTO.Candidate> GetList(string path)
        {
            List<DTO.Candidate> candidates = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                candidates = DTO.Factory.CandidateFactory.DeserializeList(data);
            }
            return candidates;
        }

        public static void Save(DTO.Candidate position)
        {
        }

        #endregion Public Methods
    }
}