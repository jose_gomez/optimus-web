﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Services
{
    public class StudiesServices : IStudies
    {
        private IStudies _StudiesRepo;

        public StudiesServices(IStudies repo)
        {
            this._StudiesRepo = repo;
        }
        public List<Studies> GetAllStudies()
        {
           return this._StudiesRepo.GetAllStudies();
        }
    }
}
