﻿using Optimus.DTO;
using System.Collections.Generic;

namespace Optimus.Web.BC.Services
{
    public class CursoService : Contracts.API.Curso
    {

        #region Private Fields

        private Contracts.API.Curso _Repo;

        #endregion Private Fields

        #region Public Constructors

        public CursoService(Contracts.API.Curso repo)
        {
            _Repo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public List<Curso> GetCAPEnProceso()
        {
            return _Repo.GetCAPEnProceso();
        }

        public List<Curso> GetCAPPendientes()
        {
            return _Repo.GetCAPPendientes();
        }

        public List<Curso> GetEnProceso()
        {
            return _Repo.GetEnProceso();
        }

        public List<Curso> GetPendientes()
        {
            return this._Repo.GetPendientes();
        }

        #endregion Public Methods

    }
}