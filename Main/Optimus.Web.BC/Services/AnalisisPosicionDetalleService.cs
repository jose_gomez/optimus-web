﻿using Optimus.DTO;
using System.Collections.Generic;

namespace Optimus.Web.BC.Services
{
    public class AnalisisPosicionDetalleService : Contracts.API.AnalisisPosicionDetalle
    {

        #region Private Fields

        private Contracts.API.AnalisisPosicionDetalle _Repo;

        #endregion Private Fields

        #region Public Constructors

        public AnalisisPosicionDetalleService(Contracts.API.AnalisisPosicionDetalle repo)
        {
            _Repo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Delete(int id)
        {
            _Repo.Delete(id);
        }

        public AnalisisPosicionDetalle Get(int id)
        {
            return _Repo.Get(id);
        }

        public List<AnalisisPosicionDetalle> GetByAnalisis(int analisisID)
        {
            return _Repo.GetByAnalisis(analisisID);
        }

        public AnalisisPosicionDetalle Save(AnalisisPosicionDetalle detalle)
        {
            return _Repo.Save(detalle);
        }

        #endregion Public Methods

    }
}