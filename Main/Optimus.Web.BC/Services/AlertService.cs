﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class AlertService : Contracts.AlertRepository
    {
        Contracts.AlertRepository _repository;
        public AlertService(Contracts.AlertRepository rep)
        {
            this._repository = rep;
        }
        public void SaveAlertType(SYAlertsType model)
        {
            this._repository.SaveAlertType(model);
        }
    }
}
