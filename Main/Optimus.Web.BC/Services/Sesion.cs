﻿using System;
using System.Collections.Generic;
using Optimus.DTO;

namespace Optimus.Web.BC.Services
{
    public class Sesion : Contracts.API.Sesion
    {

        #region Private Fields

        private Contracts.API.Sesion _Repo;

        #endregion Private Fields

        #region Public Constructors

        public Sesion(Contracts.API.Sesion repo)
        {
            _Repo = repo;
        }

        public List<InformacionBasicaEmpleado> GetParticipantes(int sesionID)
        {
            return this._Repo.GetParticipantes(sesionID);
        }

        #endregion Public Constructors

        #region Public Methods

        public List<DTO.Sesion> GetSesionesCAPEnProcesoByCurso(int cursoID)
        {
            return _Repo.GetSesionesCAPEnProcesoByCurso(cursoID);
        }

        public List<DTO.Sesion> GetSesionesCAPPendientesByCurso(int cursoID)
        {
            return _Repo.GetSesionesCAPPendientesByCurso(cursoID);
        }

        public List<DTO.Sesion> GetSesionesEnProcesoByCurso(int cursoID)
        {
            return _Repo.GetSesionesEnProcesoByCurso(cursoID);
        }

        public List<DTO.Sesion> GetSesionesPendientesByCurso(int cursoID)
        {
            return _Repo.GetSesionesPendientesByCurso(cursoID);
        }

        #endregion Public Methods

    }
}