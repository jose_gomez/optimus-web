﻿using System;
using System.Collections.Generic;
using Optimus.DTO;

namespace Optimus.Web.BC.Services
{
    public class AnalisisPosicionService : Contracts.API.AnalisisPosicion
    {
        #region Private Fields

        private Contracts.API.AnalisisPosicion _Repo;

        #endregion Private Fields

        #region Public Constructors

        public AnalisisPosicionService(Contracts.API.AnalisisPosicion repo)
        {
            _Repo = repo;
        }

        public AnalisisPosicion Get(int id)
        {
            return _Repo.Get(id);
        }

        public AnalisisPosicion GetByPosition(int positionID)
        {
            return _Repo.GetByPosition(positionID);
        }

        #endregion Public Constructors

        #region Public Methods

        public AnalisisPosicion GetByRequisicion(int requisicionID)
        {
            return _Repo.GetByRequisicion(requisicionID);
        }

        public List<AnalisisPosicionDetalle> GetDetalles(int id)
        {
            return _Repo.GetDetalles(id);
        }

        public AnalisisPosicion Save(AnalisisPosicion analisis)
        {
            return _Repo.Save(analisis);
        }

        #endregion Public Methods
    }
}