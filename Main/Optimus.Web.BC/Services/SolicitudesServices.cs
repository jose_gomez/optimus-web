﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class SolicitudesServices : ISolicitudes
    {
        private ISolicitudes _Solicitudes;

        public SolicitudesServices(ISolicitudes repo)
        {
            this._Solicitudes = repo;
        }

        public void ExportCandidate(int candidateID, int deparment)
        {
            this.ExportCandidate(candidateID,deparment);
        }

        public CandidateGUIDCurriculum GetCandidateGUIDCurriculum(string cedula)
        {
           return this.GetCandidateGUIDCurriculum(cedula);
        }

        public Candidates GetCandidates(string cedula)
        {
            return this.GetCandidates(cedula);
        }

        public List<Candidates> GetCandidates(string cedula, string sexo, int posicion, int area, int studio)
        {
            return this.GetCandidates(cedula,sexo,posicion,area,studio);
        }

        public List<Candidates> GetCandidatesAll()
        {
            return this.GetCandidatesAll();
        }

        public List<CandidateState> GetCandidateStates()
        {
            return this.GetCandidateStates();
        }

        public CandidateState GetCandidateStatesByID(int ID)
        {
            return this.GetCandidateStatesByID(ID);
        }

        public CandidatePictures GetPictureCandidate(string cedula)
        {
            return this.GetPictureCandidate(cedula);
        }

        public List<HRPositionCategory> GetPositionCategory()
        {
            return this.GetPositionCategory();
        }

        public void Save(List<CandidateLanguages> CandLang)
        {
            this._Solicitudes.Save(CandLang);
        }

        public void Save(List<CandidateReferences> CandReferences)
        {
            this._Solicitudes.Save(CandReferences);
        }

        public void Save(CandidateGUIDCurriculum GuidCurriculum)
        {
            this._Solicitudes.Save(GuidCurriculum);
        }

        public void Save(List<CandidateSkills> CandSkills)
        {
            this._Solicitudes.Save(CandSkills);
        }

        public void Save(List<CandidateTitle> Title)
        {
            this._Solicitudes.Save(Title);
        }

        public void Save(List<CandidateBrothers> CanBro)
        {
            this._Solicitudes.Save(CanBro);
        }

        public void Save(List<CandidateExperience> Exp)
        {
            this._Solicitudes.Save(Exp);
        }
        public int Save(Candidates candidates)
        {
          return  this._Solicitudes.Save(candidates);
        }

        public void SavePicture(CandidatePictures Pics)
        {
            this._Solicitudes.SavePicture(Pics);
        }
    }
}
