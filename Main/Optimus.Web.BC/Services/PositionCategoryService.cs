﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;

namespace Optimus.Web.BC.Services
{
    public class PositionCategoryService : Contracts.PositionCategory
    {
        #region Private Fields

        private Contracts.PositionCategory _PositionCategory;

        #endregion Private Fields

        #region Public Constructors

        public PositionCategoryService(Contracts.PositionCategory positionCategory)
        {
            this._PositionCategory = positionCategory;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Delete(object objeto)
        {
            this._PositionCategory.Delete(objeto);
        }

        public List<HRPositionCategory> GetAllPositionCategories()
        {
            return this._PositionCategory.GetAllPositionCategories();
        }

        public void Save(object objeto)
        {
            this._PositionCategory.Save(objeto);
        }

        #endregion Public Methods
    }
}