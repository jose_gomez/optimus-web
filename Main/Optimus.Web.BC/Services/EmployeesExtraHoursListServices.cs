﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
     public class EmployeesExtraHoursListServices : IEmployeesExtraHoursList
    {
        private IEmployeesExtraHoursList _EmployeesExtraHoursList;

        public EmployeesExtraHoursListServices(IEmployeesExtraHoursList repo)
        {
            this._EmployeesExtraHoursList = repo;
        }
        public int SavesList(EmployeesExtraHoursList EmpList)
        {
           return  this._EmployeesExtraHoursList.SavesList(EmpList);
        }
        public void SavesListDetails(List<EmployeesExtraHoursListDetails> EmpListDetails)
        {
            this._EmployeesExtraHoursList.SavesListDetails(EmpListDetails);
        }
        public List<Employees> GetEmployeeNotExtraHours(int dept, DateTime fecha)
        {
            return this._EmployeesExtraHoursList.GetEmployeeNotExtraHours(dept, fecha);
        } 
        public List<Employees> GetEmployeeHasExtraHours(int dept, DateTime fecha, int HourStar, int HourEnd)
        {
            return this._EmployeesExtraHoursList.GetEmployeeHasExtraHours(dept, fecha, HourStar, HourEnd);
        }
        public void DeleteEmpleyeeExtraHours(int dept, DateTime fecha, int HourStar, int HourEnd, List<EmployeesExtraHoursListDetails> EmpListDetailsList)
        {
             this._EmployeesExtraHoursList.DeleteEmpleyeeExtraHours(dept,fecha,HourStar,HourEnd,EmpListDetailsList);
        }

        public List<VWListExtraHour> GetExtraHourListAll()
        {
            return this._EmployeesExtraHoursList.GetExtraHourListAll();
        }
        public bool GetStateExtraHourListByDate(DateTime Date)
        {
            return this._EmployeesExtraHoursList.GetStateExtraHourListByDate(Date);
        }

        public bool AprovedList(int EmployeeID)
        {
            return this._EmployeesExtraHoursList.AprovedList(EmployeeID);
        }

        public  void ActualizarEstadoExtraHours(string date, int userID)
        {
            this._EmployeesExtraHoursList.ActualizarEstadoExtraHours(date,userID);
        }

        public List<VWListExtraHour> GetExtraHourListAll(string Date)
        {
            return this._EmployeesExtraHoursList.GetExtraHourListAll(Date);
        }

        public int GetCountEmployeeExtraHourByDateDepartment(DateTime date, int dept)
        {
            return this._EmployeesExtraHoursList.GetCountEmployeeExtraHourByDateDepartment(date, dept);
        }

        public string GetJornadaHorasExtrasByEmployeeID(int EmployeeID)
        {
            return this._EmployeesExtraHoursList.GetJornadaHorasExtrasByEmployeeID(EmployeeID);
        }
    }
}
