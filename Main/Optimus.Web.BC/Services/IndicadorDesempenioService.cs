﻿using Optimus.DTO;
using System.Collections.Generic;
using System;

namespace Optimus.Web.BC.Services
{
    internal class IndicadorDesempenioService : Contracts.API.IndicadorDesempenio
    {
        #region Private Fields

        private Contracts.API.IndicadorDesempenio _Repo;

        #endregion Private Fields

        #region Public Constructors

        public IndicadorDesempenioService(Contracts.API.IndicadorDesempenio repo)
        {
            _Repo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public List<IndicadorDesempenio> Get()
        {
            return this._Repo.Get();
        }

        public IndicadorDesempenio Get(int id)
        {
            return this._Repo.Get(id);
        }

        #endregion Public Methods
    }
}