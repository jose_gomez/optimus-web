﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class RequisitionsApprobationService : Contracts.RequisitionsApprobation
    {
        private Contracts.RequisitionsApprobation _Repo;
        public RequisitionsApprobationService(Contracts.RequisitionsApprobation repo)
        {
            this._Repo = repo;
        }
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRRequisitionsApprobation> GetRequisitionsApprobationByRequisitionID(int requisitionID)
        {
            return this._Repo.GetRequisitionsApprobationByRequisitionID(requisitionID);
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
