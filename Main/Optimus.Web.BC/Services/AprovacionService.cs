﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.DTO;

namespace Optimus.Web.BC.Services
{
    public class AprovacionService : Contracts.API.Aprovacion
    {
        private Contracts.API.Aprovacion _Repo;
        public AprovacionService(Contracts.API.Aprovacion repo)
        {
            _Repo = repo;
        }
        public List<Aprovacion> Get(int tipoRequisionID, int requisicionID)
        {
            return _Repo.Get(tipoRequisionID, requisicionID);
        }

        public List<Aprovacion> GetByTipoRequisicion(int tipoRequisionID)
        {
            return GetByTipoRequisicion(tipoRequisionID);
        }

        public List<Aprovacion> GetWorkFlow(int tipoRequisionID)
        {
            return GetWorkFlow(tipoRequisionID);
        }
    }
}
