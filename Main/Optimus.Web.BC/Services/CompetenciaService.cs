﻿using Optimus.DTO;
using System.Collections.Generic;

namespace Optimus.Web.BC.Services
{
    public class CompetenciaService : Contracts.API.Competencia
    {
        #region Private Fields

        private Contracts.API.Competencia _Repo;

        #endregion Private Fields

        #region Public Constructors

        public CompetenciaService(Contracts.API.Competencia repo)
        {
            _Repo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public List<Competencia> Get()
        {
            return _Repo.Get();
        }

        public Competencia Get(int id)
        {
            return _Repo.Get(id);
        }

        public List<Competencia> GetByCategoria(int id)
        {
            return _Repo.GetByCategoria(id);
        }

        public List<Competencia> GetByCategoriaPosicion(int posicion, int categoria)
        {
            return _Repo.GetByCategoriaPosicion(posicion, categoria);
        }

        #endregion Public Methods
    }
}