﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;

namespace Optimus.Web.BC.Services
{
    public class NotificationService : Contracts.Notification
    {

        #region Private Fields

        private Contracts.Notification _Repo;

        #endregion Private Fields

        #region Public Constructors

        public NotificationService(Contracts.Notification repo)
        {
            this._Repo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Delete(Guid guid)
        {
            this._Repo.Delete(guid);
        }

        public SYNotifycation Get(Guid guid)
        {
            return this._Repo.Get(guid);
        }

        public List<SYNotifycation> GetGlobalNotifications()
        {
            return this._Repo.GetGlobalNotifications();
        }

        public List<SYNotifycation> GetUserNotifications(int userID)
        {
            return this._Repo.GetUserNotifications(userID);
        }

        public void Save(SYNotifycation notificacion)
        {
            this._Repo.Save(notificacion);
        }

        #endregion Public Methods

    }
}