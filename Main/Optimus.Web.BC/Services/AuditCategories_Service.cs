﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Services
{
    public class AuditCategories_Service
    {
        IAuditCategories_Repository _repository;
        public AuditCategories_Service(IAuditCategories_Repository rep)
        {
            this._repository = rep;
        }

        public void SaveCategoryByID(SHAuditCategory tabCat)
        {
            this._repository.SaveCategoryByID(tabCat);
        }
        public List<SHAuditCategory> GetAllCategory()
        {
            return this._repository.GetAllCategory();
        }
        public List<SHAuditCategory> GetCategoryByName(string nameCategory)
        {
            return this._repository.GetCategoryByName(nameCategory);
        }
        public List<SHAuditCategory> GetCategoryByControlID(int controlID)
        {
            return this._repository.GetCategoryByControlID(controlID);
        }
        public List<SHAuditCategory> GetCategoryByID(int idCat)
        {
            return this._repository.GetCategoryByID(idCat);
        }
        public int CountIndicatorByCategory(int auditType)
        {
            return this._repository.CountIndicatorByCategory(auditType);
        }
        public int CountIndicatorByAudit(int auditMasterID)
        {
            return this._repository.CountIndicatorByAudit(auditMasterID);
        }

    }
}
