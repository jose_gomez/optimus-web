﻿using System;
using System.Collections.Generic;
using Optimus.DTO;

namespace Optimus.Web.BC.Services
{
    public class IndicadorDesempenioPosicionService : Contracts.API.IndicadorDesempenioPosicion
    {

        #region Private Fields

        private Contracts.API.IndicadorDesempenioPosicion _Repo;

        #endregion Private Fields

        #region Public Constructors

        public IndicadorDesempenioPosicionService(Contracts.API.IndicadorDesempenioPosicion repo)
        {
            _Repo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public IndicadorDesempenioPosicion Create(IndicadorDesempenioPosicion indicadorDesempenioPosicion)
        {
            return _Repo.Create(indicadorDesempenioPosicion);
        }

        public void Delete(int positionID, int indicatorID)
        {
            this._Repo.Delete(positionID, indicatorID);
        }

        public IndicadorDesempenioPosicion Get(int positionID, int indicatorID)
        {
            return this._Repo.Get(positionID, indicatorID);
        }

        public List<IndicadorDesempenioPosicion> GetByPosition(int positionID)
        {
            return this._Repo.GetByPosition(positionID);
        }

        public IndicadorDesempenioPosicion Save(IndicadorDesempenioPosicion indicadorDesempenioPosicion)
        {
            return _Repo.Save(indicadorDesempenioPosicion);
        }

        public IndicadorDesempenioPosicion Update(IndicadorDesempenioPosicion indicadorDesempenioPosicion)
        {
            return _Repo.Update(indicadorDesempenioPosicion);
        }

        #endregion Public Methods

    }
}