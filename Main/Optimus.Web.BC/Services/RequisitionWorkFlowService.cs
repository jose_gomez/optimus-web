﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class RequisitionWorkFlowService : Contracts.RequisitionWorkFlow
    {
        private Contracts.RequisitionWorkFlow _Repo;
        public RequisitionWorkFlowService( Contracts.RequisitionWorkFlow repo)
        {
            this._Repo = repo;
        }
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRRequistionWorkFlow> GetWorkFlowByType(int typeRequisitionID)
        {
            return this._Repo.GetWorkFlowByType(typeRequisitionID);
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
