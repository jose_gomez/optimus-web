﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts.API
{
    public interface IndicadorDesempenio
    {
        List<DTO.IndicadorDesempenio> Get();
        DTO.IndicadorDesempenio Get(int id);
        
    }
}
