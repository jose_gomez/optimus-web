﻿namespace Optimus.Web.BC.Contracts.API
{
    public interface NivelCompetenciaDeseado
    {
        #region Public Methods

        DTO.NivelCompetenciaDeseado Get(int positionID, int competenciaID);

        #endregion Public Methods
    }
}