﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface AnalisisPosicionDetalle
    {

        #region Public Methods

        void Delete(int id);

        DTO.AnalisisPosicionDetalle Get(int id);

        List<DTO.AnalisisPosicionDetalle> GetByAnalisis(int analisisID);

        DTO.AnalisisPosicionDetalle Save(DTO.AnalisisPosicionDetalle detalle);

        #endregion Public Methods
    }
}