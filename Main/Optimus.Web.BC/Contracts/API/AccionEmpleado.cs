﻿using System;
using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface AccionEmpleado
    {

        #region Public Methods

        DTO.AccionEmpleado Create(DTO.AccionEmpleado accion);

        void Delete(int id);

        List<DTO.AccionEmpleado> Get();

        DTO.AccionEmpleado Get(int id);

        List<DTO.AccionEmpleado> GetPendientes();

        List<DTO.AccionEmpleado> GetPendientes(DateTime fecha);

        DTO.AccionEmpleado Update(DTO.AccionEmpleado accion);

        #endregion Public Methods

    }
}