﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface TipoReporte
    {
        #region Public Methods

        List<DTO.TipoReporte> Get();

        DTO.TipoReporte Get(int id);

        #endregion Public Methods
    }
}