﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface IndicadorDesempenioPosicion
    {

        #region Public Methods

        DTO.IndicadorDesempenioPosicion Create(DTO.IndicadorDesempenioPosicion indicadorDesempenioPosicion);

        void Delete(int positionID, int indicatorID);

        DTO.IndicadorDesempenioPosicion Get(int positionID, int indicatorID);

        List<DTO.IndicadorDesempenioPosicion> GetByPosition(int positionID);

        DTO.IndicadorDesempenioPosicion Save(DTO.IndicadorDesempenioPosicion indicadorDesempenioPosicion);

        DTO.IndicadorDesempenioPosicion Update(DTO.IndicadorDesempenioPosicion indicadorDesempenioPosicion);

        #endregion Public Methods

    }
}