﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Curso
    {
        #region Public Methods

        List<DTO.Curso> GetEnProceso();

        List<DTO.Curso> GetPendientes();

        List<DTO.Curso> GetCAPEnProceso();

        List<DTO.Curso> GetCAPPendientes();

        #endregion Public Methods
    }
}