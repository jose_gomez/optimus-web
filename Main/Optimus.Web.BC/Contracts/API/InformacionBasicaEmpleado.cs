﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface InformacionBasicaEmpleado
    {

        #region Public Methods

        List<DTO.InformacionBasicaEmpleado> Get();

        List<DTO.InformacionBasicaEmpleado> Get(string filtro);

        DTO.InformacionBasicaEmpleado Get(int id);

        List<DTO.InformacionBasicaEmpleado> GetActivos();

        List<DTO.InformacionBasicaEmpleado> GetActivos(string filtro);

        List<DTO.InformacionBasicaEmpleado> GetInactivos();

        List<DTO.InformacionBasicaEmpleado> GetInactivos(string filtro);

        List<DTO.InformacionBasicaEmpleado> GetLiquidados();

        List<DTO.InformacionBasicaEmpleado> GetLiquidados(string filtro);

        List<DTO.InformacionBasicaEmpleado> GetPendientes();

        List<DTO.InformacionBasicaEmpleado> GetPendientes(string filtro);

        #endregion Public Methods

    }
}