﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Position : CRUD
    {
        #region Public Methods

        List<DTO.Posicion> GetAllPositions();

        DTO.Departamento GetDepartmentOfPosition(int id);

        DTO.Posicion GetPositionByID(int positionID);

        List<DTO.Posicion> GetPositionsByStatus(int status);

        #endregion Public Methods
    }
}