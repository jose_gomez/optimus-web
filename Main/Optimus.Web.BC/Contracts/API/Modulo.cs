﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Modulo
    {

        #region Public Methods

        List<DTO.Modulo> Get();

        DTO.Modulo Get(int id);

        List<DTO.Modulo> GetParentsModules();

        #endregion Public Methods

    }
}