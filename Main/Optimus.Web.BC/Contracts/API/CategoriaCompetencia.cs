﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface CategoriaCompetencia
    {
        #region Public Methods

        List<DTO.CategoriaCompetencia> Get();

        DTO.CategoriaCompetencia Get(int id);

        List<DTO.Competencia> GetCompetenciasOfCategoria(int id);
        #endregion Public Methods
    }
}