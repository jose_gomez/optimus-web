﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Competencia
    {

        #region Public Methods

        List<DTO.Competencia> Get();

        DTO.Competencia Get(int id);

        List<DTO.Competencia> GetByCategoria(int id);

        List<DTO.Competencia> GetByCategoriaPosicion(int posicion, int categoria);

        #endregion Public Methods

    }
}