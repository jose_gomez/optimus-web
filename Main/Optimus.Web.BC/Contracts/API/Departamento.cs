﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Departamento
    {
        #region Public Methods

        List<DTO.Departamento> Get();

        DTO.Departamento Get(int id);

        List<DTO.Posicion> GetPositionsActiveOfDepartment(int deparmentID);

        List<DTO.Posicion> GetPositionsInactiveOfDepartment(int deparmentID);

        #endregion Public Methods
    }
}