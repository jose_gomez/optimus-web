﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface AnalisisPosicion
    {

        #region Public Methods

        DTO.AnalisisPosicion Get(int id);

        DTO.AnalisisPosicion GetByPosition(int positionID);

        DTO.AnalisisPosicion GetByRequisicion(int requisicionID);
        List<DTO.AnalisisPosicionDetalle> GetDetalles(int id);

        DTO.AnalisisPosicion Save(DTO.AnalisisPosicion analisis);

        #endregion Public Methods
    }
}