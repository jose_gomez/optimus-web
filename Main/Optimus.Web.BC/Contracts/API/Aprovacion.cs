﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Aprovacion
    {
        List<DTO.Aprovacion> GetByTipoRequisicion(int tipoRequisionID);
        List<DTO.Aprovacion> GetWorkFlow(int tipoRequisionID);
        List<DTO.Aprovacion> Get(int tipoRequisionID, int requisicionID);
    }
}
