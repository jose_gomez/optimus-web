﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Sesion
    {
        #region Public Methods

        List<DTO.Sesion> GetSesionesEnProcesoByCurso(int cursoID);

        List<DTO.Sesion> GetSesionesPendientesByCurso(int cursoID);

        List<DTO.Sesion> GetSesionesCAPEnProcesoByCurso(int cursoID);

        List<DTO.Sesion> GetSesionesCAPPendientesByCurso(int cursoID);
        List<DTO.InformacionBasicaEmpleado> GetParticipantes(int sesionID);

        #endregion Public Methods
    }
}