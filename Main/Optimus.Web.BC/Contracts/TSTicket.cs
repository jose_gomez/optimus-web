﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Optimus.Web.DA.Optimus;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Contracts
{
  public interface ITSTicket :IRepository<TSTicket>
    {
        List<Tickestes> GetTicketsBySubmitUser(int submittedUserId, int filtro = 0);
        List<Tickestes> GetTicketsPendingByDepartment(int IdDept, bool isMng, int MngID = 0);
        List<Tickestes> GetTicketsAssingByUser(string username);
        List<Tickestes> GetTicketsDetailByIdTickets(int IdTickets);
        List<CommentTicket> GetCommentByTicketsID(int IDTickets);
        List<Tickestes> GetTicketsClosedByDepartment(int IdDept, bool isMng, int MngID = 0);
        List<Tickestes> GetResponsibles(int IDTickets);
        List<User> GetEmailOfTicketsByID(int IDTickets);
        int? getStateTicketsByGUID(Guid gui);
        int getTicketIDByGUID(Guid gui);
        List<CommentTicket> GetLogByTicketsID(int IDTickets);
        string SubmitTicket(string Title, string Description, int submitUser, int DepartmentId, int prioridad = 2, bool isRequition = false, bool Approved = false, int status = 4, int ProblemType = -99, int FatherTickets = 0);
        List<Tickestes> GetAllTicketsByUser(int IdUser, int Responsible);
        List<Employees> getLogInEmployees(string user);
        List<DA.Optimus.TSTicketsPriority> GetTicketsPriority();
        TicketStatistics GetPercentagesOfTicketsStatusesByDepartment(int departmentId);
        List<DataPoint> GetAllTimeTicketsLeaders(int employeeId);
        List<DataPoint> GetAllTimeTicketsLeaders(int employeeId, string status);
        string setApprovedTickets(string ticketID);
        List<Tickestes> GetTicketsByKEyWord(string palabra);
        List<ProblemType> getProblemTypeByDeparment(string dept);
        List<JQTreeNodeData> GetTicketsHierarchyForJQTree(int ticketId);
        List<DataPoint> GetAllTimeTicketsLeaders(int departmentId, string status, bool includeDeptHeadHierarchy);
        double GetAverageResponseTime(string dept);
        List<Tickestes> GetAllTicketsByDepartment(int dept, int filtro, int IdUser, int Responsible, bool isMng, string WordKey = "");
        void InsertCategoryByDepartement(int Dept, string Category);
        bool AllSonComplete(int TicketsID);
        List<TSProjectObjetives> GetProjects();
        List<TSProjectObjetives> GetObjetivesEspecific(int Objetives);
        List<TSProjectObjetives> GetObjectivesEspecificByDepartment(int DeptID);
        List<Tickestes> GetTicketsPendingAprovedByEmployeeID(int employee);
    }
}
