﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts
{
    public interface IEmployeesExtraHoursList
    {
        int SavesList(EmployeesExtraHoursList EmpList);

        void SavesListDetails(List<EmployeesExtraHoursListDetails> EmpListDetails);

        List<Employees> GetEmployeeNotExtraHours(int dept, DateTime fecha);

        List<Employees> GetEmployeeHasExtraHours(int dept, DateTime fecha, int HourStar, int HourEnd);

        void DeleteEmpleyeeExtraHours(int dept, DateTime fecha, int HourStar, int HourEnd, List<EmployeesExtraHoursListDetails> EmpListDetailsList);

        List<VWListExtraHour> GetExtraHourListAll();

        bool GetStateExtraHourListByDate(DateTime Date);

        bool AprovedList(int EmployeeID);

        void ActualizarEstadoExtraHours(string date, int userID);

        List<VWListExtraHour> GetExtraHourListAll(string Date);
        int GetCountEmployeeExtraHourByDateDepartment(DateTime date, int dept);
         string  GetJornadaHorasExtrasByEmployeeID(int EmployeeID);

    }
}