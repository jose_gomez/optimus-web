﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
   public interface IExtensions: IRepository<SYEmployeeExtensions>
    {
        List<EmployeeExtension> GetExtensiones();
        List<EmployeeExtension> GetExtensiones(string searchTerm, int pageIndex);
        List<EmployeeExtension> GetExtensiones(int Dept);
        List<EmployeeExtension> GetExtensiones(string nombre);
        EmployeeExtension GetExtensionByID(int id);
    }
}
