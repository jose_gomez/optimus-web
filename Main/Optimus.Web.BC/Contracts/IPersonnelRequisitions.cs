﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface IPersonnelRequisitions: ICRUD
    {
        List<DA.HRPersonnelRequisition> GetAllOpenedRequisitions();
        List<DA.HRPersonnelRequisition> GetAllNewOpenedRequisitions();
        List<DA.HRPersonnelRequisition> GetAllOldOpenedRequisitions();
        List<DA.VwTurnosJornada> GetAllJornadas();
        int SaveReturnID(DA.HRPersonnelRequisition requisicion);
        DA.HRPersonnelRequisition GetByID(int id);

        void Approve(DA.HRRequisitionsApprobation aprobacion);
    }
}
