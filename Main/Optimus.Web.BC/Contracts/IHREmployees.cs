﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
  public  interface IHREmployees :IRepository<HREmployee>
    {
        Employees GetEmployeeById(int id);
        int GetCountOfActiveEmployees();
        List<Employees> GetEmployeeBirhtDayToday();
        List<Employees> GetEmployeeBirhtDayMonth();
        List<Employees> GetEmployeeByDeparment(int dept);
    }
}
