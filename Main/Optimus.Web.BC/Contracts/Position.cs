﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface Position: ICRUD
    {
        DA.HRPosition GetPositionByID(int positionID);
        List<DA.HRPosition> GetAllPositions();
        List<DA.HRPosition> GetPositionsByLevels(List<DA.HROrganizationLevel> niveles);
        List<Models.OrgChartModel> GetOrgChart();
        List<Models.OrgChartModel> GetOrgChartByPosicion(int PosicionId);
    }
}
