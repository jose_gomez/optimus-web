﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface OrganizationLevel: ICRUD
    {
        List<DA.HROrganizationLevel> GetAllOrganizationLevels();
        List<DA.HROrganizationLevel> GetSuperiorOrganizationLevels(int organizationLevelID);
    }
}
