﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
   public interface ISolicitudes
    {
        int Save(Candidates candidates);
        void Save(List<CandidateBrothers> CanBro);
        void Save(List<CandidateExperience> Exp);
        void Save(List<CandidateTitle> Title);
        void Save(List<CandidateLanguages> CandLang);
        void Save(List<CandidateSkills> CandSkills);
        void Save(List<CandidateReferences> CandReferences);
        Candidates GetCandidates(string cedula);
        void SavePicture(CandidatePictures Pics);
        void Save(CandidateGUIDCurriculum GuidCurriculum);
        CandidatePictures GetPictureCandidate(string cedula);
        CandidateGUIDCurriculum GetCandidateGUIDCurriculum(string cedula);
        List<Candidates> GetCandidatesAll();
        List<Candidates> GetCandidates(string cedula, string sexo, int posicion, int area, int studio);
        List<CandidateState> GetCandidateStates();
        CandidateState GetCandidateStatesByID(int ID);
        void ExportCandidate(int candidateID, int deparment);
        List<HRPositionCategory> GetPositionCategory();
    }
}
