﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Contracts
{
    public interface AlertRepository
    {
        void SaveAlertType(SYAlertsType model);
    }
}
