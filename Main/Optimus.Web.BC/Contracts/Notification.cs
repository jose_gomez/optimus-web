﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface Notification
    {
        List<DA.SYNotifycation> GetGlobalNotifications();
        List<DA.SYNotifycation> GetUserNotifications(int userID);
        DA.SYNotifycation Get(Guid guid);
        void Delete(Guid guid);
        void Save(DA.SYNotifycation notificacion);
    }
}
