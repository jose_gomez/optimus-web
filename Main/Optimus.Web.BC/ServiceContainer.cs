﻿using Microsoft.Practices.Unity;
using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Repositories;
using Optimus.Web.BC.Services;

namespace Optimus.Web.BC
{
    public class ServiceContainer
    {
        private UnityContainer Container = new UnityContainer();

        public ServiceContainer()
        {
            Container.RegisterType<IPositions, RepoPositions>();
            Container.RegisterType<IDepartment, RepoDepartment>();
            Container.RegisterType<IAuditCategories_Repository, AuditCategories_Repository>();
            Container.RegisterType<IAuditIndicator, AuditIndicator>();
            Container.RegisterType<IPersonnelRequisitions, RepoPersonnelRequisitions>();
            Container.RegisterType<IAuditory, Auditory>();
            Container.RegisterType<IAuditControls, AuditControlsRepository>();
            Container.RegisterType<INationality, NationalityRepository>();
            Container.RegisterType<ILanguages, LanguagesRepository>();
            Container.RegisterType<IStudies, StudiesRepository>();
            Container.RegisterType<ISolicitudes, SolicitudiesRepository>();
            Container.RegisterType<ISystemConfig, RepoSystemConfig>();
            Container.RegisterType<Contracts.ResetPassHistory, Repositories.ResetPassHistory>();
            Container.RegisterType<Contracts.RequisitionWorkFlow, Repositories.RequisitionWorkFlow>();
            Container.RegisterType<Contracts.RequisitionsApprobation, Repositories.RequisitionsApprobation>();
            Container.RegisterType<Contracts.Position, Repositories.Position>();
            Container.RegisterType<Contracts.OrganizationLevel, Repositories.OrganizationLevel>();
            Container.RegisterType<Contracts.PositionCategory, Repositories.PositionCategory>();
            Container.RegisterType<Contracts.IEmployeesExtraHoursList, Repositories.EmployeesExtraHoursListRepository>();
            Container.RegisterType<Contracts.IPermissionsEmployees, Repositories.PermissionsEmployeesRepository>();
            Container.RegisterType<Contracts.AlertRepository, Repositories.AlertRepository>();
            Container.RegisterType<Contracts.Notification, Repositories.Notification>();
            Container.RegisterType<Contracts.API.IndicadorDesempenio, Repositories.API.IndicadorDesempenio>();
            Container.RegisterType<Contracts.API.IndicadorDesempenioPosicion, Repositories.API.IndicadorDesempenioPosicion>();
            Container.RegisterType<Contracts.API.Curso, Repositories.API.Curso>();
            Container.RegisterType<Contracts.API.Sesion, Repositories.API.Sesion>();
            Container.RegisterType<Contracts.API.AnalisisPosicion, Repositories.API.AnalisisPosicion>();
            Container.RegisterType<Contracts.API.AnalisisPosicionDetalle, Repositories.API.AnalisisPosicionDetalle>();
            Container.RegisterType<Contracts.API.Competencia, Repositories.API.Competencia>();
            Container.RegisterType<Contracts.API.Aprovacion, Repositories.API.Aprovacion>();
        }

        public AlertService GetAlertService()
        {
            return Container.Resolve<AlertService>();
        }

        public SystemConfigService GetSystemConfigService()
        {
            return Container.Resolve<SystemConfigService>();
        }

        public PositionsService GetPositionsService()
        {
            return Container.Resolve<PositionsService>();
        }

        public DepartmentService GetDepartmentService()
        {
            return Container.Resolve<DepartmentService>();
        }

        public AuditCategories_Service GetAuditCategoriesService()
        {
            return Container.Resolve<AuditCategories_Service>();
        }

        public AuditIndicatorService GetAuditIndicatorService()
        {
            return Container.Resolve<AuditIndicatorService>();
        }

        public PersonnelRequisitonsService GetPersonnelRequisitionService()
        {
            return Container.Resolve<PersonnelRequisitonsService>();
        }

        public UnitOfWork GetUnitOfWork()
        {
            return new UnitOfWork();
        }

        public AuditoryService GetAuditoryService()
        {
            return Container.Resolve<AuditoryService>();
        }

        public AuditControlsService GetAuditoryControlService()
        {
            return Container.Resolve<AuditControlsService>();
        }

        public INationality GetNationalityService()
        {
            return Container.Resolve<INationality>();
        }

        public ILanguages GetLanguagesServices()
        {
            return Container.Resolve<ILanguages>();
        }

        public IStudies GetStudies()
        {
            return Container.Resolve<IStudies>();
        }

        public ISolicitudes getSolicitudesServices()
        {
            return Container.Resolve<ISolicitudes>();
        }

        public ResetPassHistoryService GetResetPassHistory()
        {
            return Container.Resolve<ResetPassHistoryService>();
        }

        public RequisitionWorkFlowService GetRequisitionWorkFlowService()
        {
            return Container.Resolve<RequisitionWorkFlowService>();
        }

        public RequisitionsApprobationService GetRequisitionsApprobationService()
        {
            return Container.Resolve<RequisitionsApprobationService>();
        }

        public PositionService GetPositionService()
        {
            return Container.Resolve<PositionService>();
        }

        public OrganizationLevelService GetOrganizationLevelService()
        {
            return Container.Resolve<OrganizationLevelService>();
        }

        public PositionCategoryService GetPositionCategoryService()
        {
            return Container.Resolve<PositionCategoryService>();
        }

        public IEmployeesExtraHoursList GetEmployeesExtraHoursListService()
        {
            return Container.Resolve<EmployeesExtraHoursListServices>();
        }

        public IPermissionsEmployees GetPermissionsEmployeesService()
        {
            return Container.Resolve<PermissionsEmployesServices>();
        }

        public Contracts.Notification GetNotificationsService()
        {
            return Container.Resolve<Services.NotificationService>();
        }

        public Contracts.API.IndicadorDesempenio GetIndicadorDesempenio()
        {
            return Container.Resolve<Services.IndicadorDesempenioService>();
        }

        public Contracts.API.IndicadorDesempenioPosicion GetIndicadorDesempenioPosicion()
        {
            return Container.Resolve<Services.IndicadorDesempenioPosicionService>();
        }

        public Contracts.API.Curso GetCurso()
        {
            return Container.Resolve<Services.CursoService>();
        }

        public Contracts.API.Sesion GetSesion()
        {
            return Container.Resolve<Services.Sesion>();
        }
        public Contracts.API.AnalisisPosicion GetAnalisisPosicion()
        {
            return Container.Resolve<Services.AnalisisPosicionService>();
        }
        public Contracts.API.AnalisisPosicionDetalle GetAnalisisPosicionDetalle()
        {
            return Container.Resolve<Services.AnalisisPosicionDetalleService>();
        }
        public Contracts.API.Competencia GetCompetenciaService()
        {
            return Container.Resolve<Services.CompetenciaService>();
        }
        public Contracts.API.Aprovacion GetAprovacionService()
        {
            return Container.Resolve<Services.AprovacionService>();
        }
    }
}