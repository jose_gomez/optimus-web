﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
 public class CandidateState
    {
        public int CandidateStateID { get; set; }
        public string State { get; set; }
    }
}
