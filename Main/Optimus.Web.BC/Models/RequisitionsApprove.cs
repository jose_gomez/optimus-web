﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class RequisitionsApprove
    {
        public string PositionName { get; set; }
        public string UserName { get; set; }
        public string Status { get; set; }
    }
}
