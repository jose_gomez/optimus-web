﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class AuditIndicator
    {
        public int IdIndicator { get; set; }
        public string Indicator { get; set; }
        public int IdCategory { get; set; }
        public string Category { get; set; }
        public int IdControl { get; set; }
        public string ControlName { get; set; }
        public bool? Active { get; set; }

    }
}
