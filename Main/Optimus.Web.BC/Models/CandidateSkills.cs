﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
   public class CandidateSkills
    {
        public int CandidateID { get; set; }
        public int SkillID { get; set; }
    }
}
