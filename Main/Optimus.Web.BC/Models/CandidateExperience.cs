﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
 public class CandidateExperience
    {
        public int CandidateID { get; set; }
        public string Company { get; set; }
        public string WorkPosition { get; set; }
        public string PerfomedTask { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public decimal Salary { get; set; }
        public string ReasonOut { get; set; }
        public string PhoneNumberCompany { get; set; }
    }
}
