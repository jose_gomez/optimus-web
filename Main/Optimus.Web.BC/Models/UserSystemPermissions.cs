﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
   public class UserSystemPermissions
    {
        public int UserID { get; set; }
        public int ModuloID { get; set; }
        public Boolean CanRead { get; set; }
        public Boolean CanUpdate { get; set; }
        public Boolean CanDelete { get; set; }
        public Boolean CanCreate { get; set; }
        public string title { get; set; }
        public string ConstantName { get; set;}

    }
}
