﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public class CandidatePictures
    {
        public int CandidateID { get; set; }
        public byte[] PictureCadidate { get; set; }
        public string PictureBase64Candidate { get; set;}
        public byte[] PictureCadidateCedulaFrontPicture { get; set; }
        public string PictureBase64CandidateCedulaFrontPicture { get; set; }
        public byte[] PictureCadidateCedulaBackPicture { get; set; }
        public string PictureBase64CandidateCedulaBackPicture { get; set; }
        public DateTime create_dt { get; set; }
    }
}
