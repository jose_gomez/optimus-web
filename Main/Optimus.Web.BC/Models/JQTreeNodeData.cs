﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    [Serializable]
    public class JQTreeNodeData
    {
        public JQTreeNodeData()
        {
            this.children = new List<JQTreeNodeData>();
        }
        public string name { get; set; }
        public int? id { get; set; }
        public int? rootId { get; set; }
        public int? progress { get; set; }
        public List<JQTreeNodeData> children { get; set; }
    }
}
