﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public class ProjectObjective
    {
        public int ProjectObjetivesID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public DateTime EstimatedDate { get; set; }
        public int PorcentProgress { get; set; }
        public int? FatherProject { get; set; }
        public int Year { get; set; }
        public int? StatusID { get; set; }
        public string Status { get; set; }
        public int? DepartmentID { get; set; }
        public string Department { get; set;}
        public int? ProjectObjetivesTypesID { get; set; }
        public string ProjectObjetives { get; set; }
        public bool Project { get; set; }

    
    }
}
