﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
   public class TicketState
    {
        public int ID { get; set; }
        public string TSTicketState { get; set; }
    }
}
