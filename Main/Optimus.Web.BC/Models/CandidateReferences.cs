﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
   public class CandidateReferences
    {
        public int CandidateID { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Profession { get; set; }
    }
}
