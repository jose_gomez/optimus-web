﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
 internal class StudiesRepository:IStudies
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
       ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public List<Studies> GetAllStudies()
        {
            var query = (from p in ContextSQL.HRStudies
                         select new Studies
                         {
                             StudiesID = p.StudiesID,
                             Name = p.Name
                         }).ToList();
            return query;
        }
    }
}
