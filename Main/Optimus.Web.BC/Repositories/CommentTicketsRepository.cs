﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;

namespace Optimus.Web.BC.Repositories
{
    public class CommentTicketsRepository : Repository<CommentTickets>, ICommentTicket
    {
        public CommentTicketsRepository(OptimusEntities context) : base(context)
        {
        }

    }
}
