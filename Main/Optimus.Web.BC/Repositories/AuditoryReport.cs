﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using Optimus.Web.BC.Models.SHAudit;

namespace Optimus.Web.BC.Repositories
{
    internal class AuditoryReport : IAuditoryReport
    {
        DataClassesDataContext context = new DataClassesDataContext();

        public List<SHAudit> GetAuditEPPByDetDate(int deparment, DateTime date)
        {
            var select = (from a in context.SHAudits
                          join dep in context.HRDepartments on a.DepartmentId equals dep.DepartmentId
                          join ind in context.SHAuditIndicators on a.AuditIndicatorId equals ind.IndicatorID
                          where a.ControlID == 1
                          select a).ToList();

            return select;

            //var select = (from a in context.SHAudits
            //              join dep in context.HRDepartments on a.DepartmentId equals dep.DepartmentId
            //              join ind in context.SHAuditIndicators on a.AuditIndicatorId equals ind.ID
            //              where a.ControlID == 1
            //              select new AuditEPPModels
            //              {
            //                  departamento = dep.Name,
            //                  epp = ind.AuditIndicator,
            //                  condicion = Convert.ToInt16( a.Condicion),
            //                  uso = Convert.ToInt16(a.Uso),
            //                  observaciones = a.Observaciones
            //              }).ToList();

            //return select;
        }
    }
}
