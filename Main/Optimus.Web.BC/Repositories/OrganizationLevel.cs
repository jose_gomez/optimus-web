﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Repositories
{
    internal class OrganizationLevel : Contracts.OrganizationLevel
    {
        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HROrganizationLevel> GetAllOrganizationLevels()
        {
            var result = (from a in Context.HROrganizationLevels
                          select a).OrderBy(x => x.Name).ToList();
            return result;
        }

        public List<HROrganizationLevel> GetSuperiorOrganizationLevels(int organizationLevelID)
        {
            List<HROrganizationLevel> lista = new List<HROrganizationLevel>();
            HROrganizationLevel level = (from a in Context.HROrganizationLevels
                                         where a.OrganizationLevelID == organizationLevelID 
                                         select a).FirstOrDefault();
            if (level==null)
            {
                return lista;
            }
            level = (from a in Context.HROrganizationLevels
                     where a.OrganizationLevelID == level.OrganizationLevelSup
                     select a).FirstOrDefault();
            do
            {
                lista.Add(level);
                level = (from a in Context.HROrganizationLevels
                         where a.OrganizationLevelID == level.OrganizationLevelSup
                         select a).FirstOrDefault();
                
            } while (!lista.Contains(level));
            return lista;
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
