﻿using DRL.Utilities.Encryption;
using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;

using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    public class SYUserRepository : Repository<SYUsers>,ISYUsers
    {
        public SYUserRepository(OptimusEntities context) : base(context)
        {
           
        }
        public  User GetUserByUserName(string username)
        {
            Repositories.RepoSystemConfig configRepo = new RepoSystemConfig();
            var config = configRepo.GetSysTemConfigByName("PASSWORDDURATION");
            double days = double.Parse("-" + config.SysConfigValue);
            return this.Find(x => x.UserName == username).Select(e => new User()
            {
                userID = e.UserID,
                userName = e.UserName,
                fullName = e.FullName,
                password = e.password,
                EmployeeID = Convert.ToInt32(e.EmployeeID),
                isEnable = e.IsEnabled.GetValueOrDefault(false),
                LastPaswordChange = e.LastPasswordChange.GetValueOrDefault(DateTime.Now.Date.AddDays(days)),
            }).FirstOrDefault();
        }
    }
}

