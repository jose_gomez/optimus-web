﻿
using Optimus.Web.BC.Contracts;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    class ResponsiblesTicketsRepository : Repository<TSTicketsResponsible>,IResponsiblesTicket
    {
        public ResponsiblesTicketsRepository(OptimusEntities context) : base(context)
        {
        }
    }
}
