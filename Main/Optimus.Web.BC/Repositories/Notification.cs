﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
    public class Notification : Contracts.Notification
    {
        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Delete(Guid guid)
        {
            var result = (from a in Context.SYNotifycations
                          where a.NotifycationsID == guid
                          select a).FirstOrDefault();
            if (result != null)
            {
                Context.SYNotifycations.DeleteOnSubmit(result);
                Context.SubmitChanges();
            }
        }

        public SYNotifycation Get(Guid guid)
        {
            var result = (from a in Context.SYNotifycations
                          where a.NotifycationsID == guid
                          select a).FirstOrDefault();
            return result;
        }

        public List<SYNotifycation> GetGlobalNotifications()
        {
            var result = (from a in Context.SYNotifycations
                          where a.UserID == null
                          select a).ToList();
            return result;
        }

        public List<SYNotifycation> GetUserNotifications(int userID)
        {
            var result = (from a in Context.SYNotifycations
                          where a.UserID == userID
                          select a).ToList();
            return result;
        }

        public void Save(SYNotifycation notificacion)
        {
            var result = (from a in Context.SYNotifycations
                          where a.NotifycationsID == notificacion.NotifycationsID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new SYNotifycation();
                result.NotifycationsID = Guid.NewGuid();
                Context.SYNotifycations.InsertOnSubmit(result);
            }
            result.Title = notificacion.Title;
            result.Description = notificacion.Description;
            result.NotificationTypeID = notificacion.NotificationTypeID;
            result.UserID = notificacion.UserID;
            Context.SubmitChanges();
        }

        #endregion Public Methods
    }
}