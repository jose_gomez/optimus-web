﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;

namespace Optimus.Web.BC.Repositories
{
    internal class RequisitionsApprobation : Contracts.RequisitionsApprobation
    {
        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
   ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRRequisitionsApprobation> GetRequisitionsApprobationByRequisitionID(int requisitionID)
        {
            var result = (from a in Context.HRRequisitionsApprobations
                          where a.RequisitionID == requisitionID
                          select a).OrderBy(x => x.Create_dt).ToList();
            return result;
        }

        public List<HRPosition> GetApprovedPositionsByRequisitionID(int requisitionID)
        {
            List<HRPosition> listaPosiciones = new List<HRPosition>();
            RequisitionsApprobation requisitionApprvRepo = new RequisitionsApprobation();
            Services.UnitOfWork unitW = new Services.UnitOfWork();
            RepoPositions positionRepo = new RepoPositions();
            var aprobaciones = requisitionApprvRepo.GetRequisitionsApprobationByRequisitionID(requisitionID);
            foreach (var item in aprobaciones)
            {
               SYUsers user =  unitW.User.Find(x => x.UserID == item.UserID).FirstOrDefault();
                if (user != null)
                {
                    var empleado = unitW.Employees.Find(x => x.EmployeeId == user.EmployeeID).FirstOrDefault();
                    if (empleado != null)
                    {
                        listaPosiciones.Add(positionRepo.GetPositionsByID((int)empleado.PositionID));
                    }
                }
            }
            return listaPosiciones;
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
