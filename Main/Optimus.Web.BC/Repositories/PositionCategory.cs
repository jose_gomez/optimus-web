﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Repositories
{
    internal class PositionCategory : Contracts.PositionCategory
    {
        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRPositionCategory> GetAllPositionCategories()
        {
            var result = (from a in Context.HRPositionCategories
                          select a).ToList();
            return result;
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
