﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    class StateRepository : Repository<TSTicketState>, ITicketState
    {
        public StateRepository(OptimusEntities context) : base(context)
        {
        }
        public List<TSTicketState> GetAllTicketsState()
        {
            var a = (from p in Context.TSTicketStates
                     where p.ID != 4 || p.ID != 3 || p.ID != 1
                     select p
                
                ).ToList();

            return a;

        }
        public List<TSTicketState> GetTicketsStateToLimitUser()
        {
            var a = (from p in Context.TSTicketStates
                     where p.ID == 5 || p.ID == 7 
                     select p
                     ).ToList();
            return a;
        }
        public List<TSTicketState> GetTicketsStateToRequiquitionLimit()
        {

            var a = (from p in Context.TSTicketStates
                     where p.ID == 7 || p.ID == 6
                     select p
                     ).ToList();

            return a;
        }

        public List<TSTicketState> GetTicketsStateToRequiquition()
        {

            var a = (from p in Context.TSTicketStates
                     where p.ID == 7 || p.ID == 6 || p.ID ==2
                     select p
                     ).ToList();

            return a;
        }
        public List<TSTicketState> GetTicketStateToAccesPetition()
        {
            var a = (from p in Context.TSTicketStates
                     where p.ID == 1|| p.ID == 2 
                     select p
                    ).ToList();

            return a;
        }
    }
}
