﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Repositories
{
    internal class Auditory : IAuditory
    {
        DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public List<Audit> GetAllAudit()
        {
            var select = (from a in context.SHAuditMasters
                          join b in context.HRDepartments on a.DepartmentId equals b.DepartmentId
                          join c in context.HREmployees on a.SupervisorId equals c.EmployeeId
                          join d in context.HREmployees on a.AnalystId equals d.EmployeeId
                          join e in context.SHAuditControls on a.ControlID equals e.ControlID
                          select new Audit()
                          {
                              AuditMasterID = a.AuditMasterID,
                              DepartmentName = b.Name,
                              SupervisorName = c.FirstName +" "+ c.LastName,
                              AnalistName = d.FirstName +" "+ d.LastName,
                              AuditName = e.NameAudit,
                              AuditDate = a.AuditDate
                          }).ToList();
            return select;

        }

        public List<SHAuditMaster> GetAuditBy_DepartmentID_Date_ControlID(int departmentID, DateTime date, int controlID)
        {
            var select = (from a in context.SHAuditMasters where (a.DepartmentId == departmentID && a.ControlID == controlID && a.AuditDate == date) select a).ToList();
            return select;
        }

        public List<SHAuditMaster> GetAuditBy_DepartmentID_Date_IndicateID(SHAuditMaster auditory, SHAuditDetail detail)
        {
            var select = (from a in context.SHAuditMasters
                          join b in context.SHAuditDetails on a.AuditMasterID equals b.AuditMasterID
                          join c in context.SHAuditIndicators on b.AuditIndicatorId equals c.IndicatorID
                          where (a.DepartmentId == auditory.DepartmentId && a.AuditDate == auditory.AuditDate && c.CategoryID == detail.AuditIndicatorId)
                          select a).ToList();
            return select;
        }

        public List<SHAuditImagen> GetImageByAuditID(int auditID)
        {
            var select = (from a in context.SHAuditImagens where (a.AuditID == auditID) select a).ToList();
            return select;
        }

        public List<SHAuditImagen> GetImageByImgID(int imgID)
        {
            var select = (from a in context.SHAuditImagens where (a.ImgID == imgID) select a).ToList();
            return select;
        }

        public List<SHAuditMaster> SaveByID(SHAuditMaster auditory)
        {
            SHAuditMaster audit = context.SHAuditMasters.Where(tab => tab.AuditMasterID == auditory.AuditMasterID).FirstOrDefault();

            if (audit == null)
            {
                audit = new SHAuditMaster();
                audit.Create_dt = DateTime.Now;
                context.SHAuditMasters.InsertOnSubmit(audit);
            }

            audit.AuditDate = auditory.AuditDate;
            audit.DepartmentId = auditory.DepartmentId;
            audit.SupervisorId = auditory.SupervisorId;
            audit.AnalystId = auditory.AnalystId;
            audit.ControlID = auditory.ControlID;
            audit.Modify_dt = DateTime.Now;
            audit.UsersID = auditory.UsersID;

            context.SubmitChanges();

            var id = audit.AuditMasterID;

            var select = ( from a in context.SHAuditMasters where (a.AuditMasterID == id) select a ).ToList();

            return select;
        }

        public List<SHAuditDetail> SaveDetailByID(SHAuditDetail auditory)
        {
            SHAuditDetail auditDetail = context.SHAuditDetails.Where(tab => tab.AuditMasterID == auditory.AuditMasterID && tab.AuditIndicatorId == auditory.AuditIndicatorId).FirstOrDefault();// tab => tab.AuditDetailID == auditory.AuditDetailID).FirstOrDefault();

            if (auditDetail == null)
            {
                auditDetail = new SHAuditDetail();
                auditDetail.CreateDate = DateTime.Now;
                context.SHAuditDetails.InsertOnSubmit(auditDetail);
            }

            auditDetail.AuditMasterID = auditory.AuditMasterID;
            auditDetail.AuditIndicatorId = auditory.AuditIndicatorId;
            auditDetail.Aplica = auditory.Aplica;
            auditDetail.Condicion = auditory.Condicion;
            auditDetail.Uso = auditory.Uso;
            auditDetail.Observaciones = auditory.Observaciones;
            auditDetail.UsersID = auditory.UsersID;

            context.SubmitChanges();

            var id = auditDetail.AuditDetailID;

            var select = (from a in context.SHAuditDetails where (a.AuditMasterID == id) select a).ToList();

            return select;
        }

        public List<SHAuditImagen> SaveImageByID(SHAuditImagen img)
        {
            SHAuditImagen auditImage = context.SHAuditImagens.Where(tab => tab.ImgID == img.ImgID).FirstOrDefault();

            if(auditImage == null)
            {
                auditImage = new SHAuditImagen();
                auditImage.Create_dt = DateTime.Now;
                context.SHAuditImagens.InsertOnSubmit(auditImage);
            }

            auditImage.AuditID = img.AuditID;
            auditImage.AuditIndicatorID = img.AuditIndicatorID;
            auditImage.Images = img.Images;
            auditImage.UserID = img.UserID;

            context.SubmitChanges();

            var id = auditImage.ImgID;
            var select = (from a in context.SHAuditImagens where (a.ImgID == id) select a).ToList();
            return select;
        }
    }
}
