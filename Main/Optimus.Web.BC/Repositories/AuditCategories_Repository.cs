﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using System.Configuration;

namespace Optimus.Web.BC.Repositories
{
    internal class AuditCategories_Repository : IAuditCategories_Repository
    {
        DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public List<SHAuditCategory> GetAllCategory()
        {
            var select = (
                            from a in context.SHAuditCategories
                            select a
                         ).ToList();
            return select;
        }

        public List<SHAuditCategory> GetCategoryByControlID(int controlID)
        {
            var select = (
                            from a in context.SHAuditCategories
                            where a.ControlID == controlID
                            select a
                         ).ToList();
            return select;
        }

        public List<SHAuditCategory> GetCategoryByName(string categoryName)
        {
            var select = (
                            from a in context.SHAuditCategories
                            where (a.Category == categoryName)
                            select a
                         ).ToList();
            return select;
        }
        public List<SHAuditCategory> GetCategoryByID(int categoryID)
        {
            var select = (
                            from a in context.SHAuditCategories
                            where (a.CategoryID == categoryID)
                            select a
                         ).ToList();
            return select;
        }
        public void SaveCategoryByID(SHAuditCategory tabCat)
        {
            SHAuditCategory auditCat = context.SHAuditCategories.Where(tab => tab.CategoryID == tabCat.CategoryID).FirstOrDefault();

            if (auditCat == null)
            {
                auditCat = new SHAuditCategory();
                auditCat.Create_dt = DateTime.Now;
                context.SHAuditCategories.InsertOnSubmit(auditCat);
            }

            auditCat.Category = tabCat.Category;
            auditCat.ControlID = tabCat.ControlID;
            auditCat.UserID = tabCat.UserID;
            context.SubmitChanges();
        }
        public int CountIndicatorByCategory(int auditType)
        {
            var select = (from a in context.SHAuditCategories
                          join b in context.SHAuditIndicators on a.CategoryID equals b.CategoryID
                          where (a.ControlID == auditType && b.Active == true)
                          select a).Count();

            return select;
        }
        public int CountIndicatorByAudit(int auditMasterID)
        {

            var select = (from j in context.SHAuditMasters
                          join k in context.SHAuditDetails on j.AuditMasterID equals k.AuditMasterID
                          where j.AuditMasterID == auditMasterID
                          select j).Count();
            return select;
        }
    }
}

