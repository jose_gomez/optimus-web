﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    public class EmployeeExtensionsRepository : Repository<SYEmployeeExtensions>, IExtensions
    {
        public EmployeeExtensionsRepository(OptimusEntities context) : base(context)
        {

        }
        public List<EmployeeExtension> GetExtensiones()
        {
            int outy = 0;
            var query = (from p in Context.SYEmployeeExtensions
                         join t in Context.HREmployees on p.EmployeeID equals t.EmployeeId into n from nt in n.DefaultIfEmpty()
                         join k in Context.HRDepartments on p.DepartmentId equals k.DepartmentId into dept from Depa in  dept.DefaultIfEmpty()
                         orderby p.Extensions ascending
                         select new EmployeeExtension
                         {
                             EmployeeExtensionsID = p.EmployeeExtensionsID ,
                             EmployeesID = p.EmployeeID ?? outy,
                             ext = p.Extensions ?? outy,
                             fullName = nt.FirstName + " " + nt.LastName,
                             Departamento = Depa.Name,
                             Flota = p.Flota,
                             Etiqueta = p.Etiqueta
                         }).ToList();
            return query;
        }
        public List<EmployeeExtension> GetExtensiones(int Dept)
        {
            int outy = 0;
            var query = (from p in Context.SYEmployeeExtensions
                         join t in Context.HREmployees on p.EmployeeID equals t.EmployeeId into n
                         from nt in n.DefaultIfEmpty()
                         join k in Context.HRDepartments on p.DepartmentId equals k.DepartmentId into dept
                         from Depa in dept.DefaultIfEmpty()
                         where Depa.DepartmentId == Dept
                         orderby p.Extensions ascending
                         select new EmployeeExtension
                         {
                             EmployeeExtensionsID = p.EmployeeExtensionsID,
                             EmployeesID = p.EmployeeID ?? outy,
                             ext = p.Extensions ?? outy,
                             fullName = nt.FirstName + " " + nt.LastName,
                             Departamento = Depa.Name,
                             Flota = p.Flota,
                             Etiqueta = p.Etiqueta

                         }).ToList();
            return query;
        }
        public List<EmployeeExtension> GetExtensiones(string nombre)
        {
            int ext = 0;
            int outy = 0;
            int.TryParse(nombre,out ext);
            var query = (from p in Context.SYEmployeeExtensions
                         join t in Context.HREmployees on p.EmployeeID equals t.EmployeeId into n
                         from nt in n.DefaultIfEmpty()
                         join k in Context.HRDepartments on p.DepartmentId equals k.DepartmentId into dept
                         from Depa in dept.DefaultIfEmpty()
                         where  ((nt.FirstName+" "+nt.LastName).Contains(nombre))|| p.Extensions == ext  || p.Etiqueta.Contains(nombre)
                         orderby p.Extensions ascending
                         select new EmployeeExtension
                         {
                             EmployeeExtensionsID = p.EmployeeExtensionsID,
                             EmployeesID = p.EmployeeID ??outy,
                             ext = p.Extensions ?? outy,
                             fullName = nt.FirstName + " " + nt.LastName,
                             Departamento = Depa.Name,
                             Flota = p.Flota,
                             Etiqueta = p.Etiqueta
                         }).ToList();
            return query;
        }
        public List<EmployeeExtension> GetExtensiones(string searchTerm, int pageIndex)
        {
            var votesParam = new SqlParameter { ParameterName = "voteCount", Value = 0, Direction = ParameterDirection.Output };
            int PageSize = 10;
            var Extenciones = Context.Database.SqlQuery<EmployeeExtension>("GetEmployeeExtensions @SearchTerm, @PageIndex, @PageSize, @RecordCount", new SqlParameter("SearchTerm", searchTerm), new SqlParameter("PageIndex", pageIndex), new SqlParameter("PageSize", PageSize), new SqlParameter("PageSize", PageSize), votesParam).ToList();
            return Extenciones;
        }
        public EmployeeExtension GetExtensionByID(int id)
        {
            int outy = 0;
            var query = (from p in Context.SYEmployeeExtensions
                         where p.EmployeeExtensionsID == id
                         orderby p.Extensions ascending
                         select new EmployeeExtension
                         {
                             EmployeeExtensionsID = p.EmployeeExtensionsID,
                             EmployeesID = p.EmployeeID ?? outy,
                             ext = p.Extensions ?? outy,
                             Flota = p.Flota,
                             DeparmentID = p.DepartmentId ?? outy,
                             Etiqueta = p.Etiqueta
                             
                         }).FirstOrDefault();
            return query;
        }
    }
}
