﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
    internal class RepoPersonnelRequisitions : IPersonnelRequisitions
    {
        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Approve(HRRequisitionsApprobation aprobacion)
        {
            var aprobacionGuardar = (from a in Context.HRRequisitionsApprobations
                                     where a.RequisitionID == aprobacion.RequisitionID && a.UserID == aprobacion.UserID && a.TypeRequisitionID == aprobacion.TypeRequisitionID
                                     select a).FirstOrDefault();
            if (aprobacionGuardar == null)
            {
                Context.SPInsertHRRequisitionsApprobation(aprobacion.TypeRequisitionID, aprobacion.RequisitionID, aprobacion.StatusID, aprobacion.UserID);
                RequisitionWorkFlow requisitionWFRepo = new RequisitionWorkFlow();
                RequisitionsApprobation requisitionAppr = new RequisitionsApprobation();
                Services.UnitOfWork unitW = new Services.UnitOfWork();
                var workFLow = requisitionWFRepo.GetWorkFlowByType((int)aprobacion.TypeRequisitionID);
                var posicionesAprobaron = requisitionAppr.GetApprovedPositionsByRequisitionID((int)aprobacion.RequisitionID);
                bool aprobada = true;
                foreach (var item in workFLow)
                {
                    var aprobacionBuscada = (from a in posicionesAprobaron
                                             where a.PositionID == item.PositionID
                                             select a).FirstOrDefault();
                    if (aprobacionBuscada == null)
                    {
                        aprobada = false;
                    }
                }
                if (aprobada)
                {
                    unitW.TSTickets.setApprovedTickets(GetByID((int)aprobacion.RequisitionID).guid.ToString());
                }
            }
        }

        public void Delete(object objeto)
        {
            HRPersonnelRequisition requisicionRecibida = (HRPersonnelRequisition)objeto;

            var requisicionBuscada = (from a in Context.HRPersonnelRequisitions
                                      where a.PersonalRequisitionID == requisicionRecibida.PersonalRequisitionID
                                      select a).FirstOrDefault();

            if (requisicionBuscada != null)
            {
                Context.HRPersonnelRequisitions.DeleteOnSubmit(requisicionBuscada);
            }

            Context.SubmitChanges();
        }

        public List<VwTurnosJornada> GetAllJornadas()
        {
            var lista = (from a in Context.VwTurnosJornadas
                         select a).ToList();

            return lista;
        }

        public List<HRPersonnelRequisition> GetAllNewOpenedRequisitions()
        {
            var lista = (from a in Context.HRPersonnelRequisitions
                         where a.StatusID == 24 && a.IsNew == true
                         select a).ToList();

            return lista;
        }

        public List<HRPersonnelRequisition> GetAllOldOpenedRequisitions()
        {
            var lista = (from a in Context.HRPersonnelRequisitions
                         where a.StatusID == 24 && a.IsNew == false
                         select a).ToList();

            return lista;
        }

        /// <summary>
        /// Obtiene un listado de todas las requisición en estado 24 o OPEN
        /// </summary>
        /// <returns>Lista de requisiciones abiertas</returns>
        public List<HRPersonnelRequisition> GetAllOpenedRequisitions()
        {
            ActualizarRequisiones();
            var lista = (from a in Context.HRPersonnelRequisitions
                         where a.StatusID == 24 || a.StatusID == 27
                         select a).ToList();

            return lista;
        }

        /// <summary>
        /// Obtiene una requisición por su id
        /// </summary>
        /// <param name="id">identificador de la requisición</param>
        /// <returns>requisición de personal</returns>
        public HRPersonnelRequisition GetByID(int id)
        {
            var requisicion = (from a in Context.HRPersonnelRequisitions
                               where a.PersonalRequisitionID == id
                               select a).FirstOrDefault();
            return requisicion;
        }

        /// <summary>
        /// Crea o Actualiza una requisición
        /// </summary>
        /// <param name="objeto">HRPersonnelRequisition</param>
        public void Save(object objeto)
        {
            HRPersonnelRequisition requisicionRecibida = (HRPersonnelRequisition)objeto;

            //Verifica si la requisición existe
            var requisicionBuscada = (from a in Context.HRPersonnelRequisitions
                                      where a.PersonalRequisitionID == requisicionRecibida.PersonalRequisitionID
                                      select a).FirstOrDefault();

            if (requisicionBuscada == null)//Si la requisición no existe se crea una nueva
            {
                foreach (HRPersonnelRequisition item in GetAllOpenedRequisitions())
                {
                    //Se verifica que no aya una requisición para este puesto abierta tomando en cuenta el departamento.
                    if (item.PositionName.Trim() == requisicionRecibida.PositionName.Trim() && item.DepartmentID == requisicionRecibida.DepartmentID)
                        throw new Optimus.Core.Exceptions.ValidationException("Ya se encuentra abierta una solicitud para este puesto.");
                }

                requisicionBuscada = new HRPersonnelRequisition();
                Context.HRPersonnelRequisitions.InsertOnSubmit(requisicionBuscada);
                requisicionBuscada.CreateDate = DateTime.Now;
            }

            requisicionBuscada.Contract = requisicionRecibida.Contract;
            requisicionBuscada.DepartmentID = requisicionRecibida.DepartmentID;
            requisicionBuscada.IsNew = requisicionRecibida.IsNew;
            requisicionBuscada.ModifyDate = DateTime.Now;
            requisicionBuscada.PositionID = requisicionRecibida.PositionID;
            if (string.IsNullOrWhiteSpace(requisicionRecibida.PositionName.ToUpper())) throw new Optimus.Core.Exceptions.ValidationException("Necesita especificar el nombre de la posición.");
            requisicionBuscada.PositionName = requisicionRecibida.PositionName.ToUpper();
            requisicionBuscada.QuantityEmployees = requisicionRecibida.QuantityEmployees;
            requisicionBuscada.Reason = requisicionRecibida.Reason;
            requisicionBuscada.Replace = requisicionRecibida.Replace;
            requisicionBuscada.StartDate = requisicionRecibida.StartDate;
            requisicionBuscada.StatusID = requisicionRecibida.StatusID;
            requisicionBuscada.SuperiorPositionID = requisicionRecibida.SuperiorPositionID;
            requisicionBuscada.User = requisicionRecibida.User;
            requisicionBuscada.Workday = requisicionRecibida.Workday;
            requisicionBuscada.WorkingHours = requisicionRecibida.WorkingHours;
            requisicionBuscada.guid = requisicionRecibida.guid;
            Context.SubmitChanges();
        }

        /// <summary>
        /// Crea o Actualiza una requisición
        /// </summary>
        /// <param name="requisicion">HRPersonnelRequisition</param>
        /// <returns>Identificador</returns>
        public int SaveReturnID(HRPersonnelRequisition requisicionRecibida)
        {
            //Verifica si la requisición existe
            var requisicionBuscada = (from a in Context.HRPersonnelRequisitions
                                      where a.PersonalRequisitionID == requisicionRecibida.PersonalRequisitionID
                                      select a).FirstOrDefault();

            if (requisicionBuscada == null)//Si la requisición no existe se crea una nueva
            {
                foreach (HRPersonnelRequisition item in GetAllOpenedRequisitions())
                {
                    //Se verifica que no aya una requisición para este puesto abierta tomando en cuenta el departamento.
                    if ((item.PositionName.Trim() == requisicionRecibida.PositionName.Trim()&& item.PositionName.Trim() != "NUEVA POSICION") && item.DepartmentID == requisicionRecibida.DepartmentID)
                        throw new Optimus.Core.Exceptions.ValidationException("Ya se encuentra abierta una solicitud para este puesto.");
                }

                requisicionBuscada = new HRPersonnelRequisition();
                Context.HRPersonnelRequisitions.InsertOnSubmit(requisicionBuscada);
                requisicionBuscada.CreateDate = DateTime.Now;
            }

            requisicionBuscada.Contract = requisicionRecibida.Contract;
            requisicionBuscada.DepartmentID = requisicionRecibida.DepartmentID;
            requisicionBuscada.IsNew = requisicionRecibida.IsNew;
            requisicionBuscada.ModifyDate = DateTime.Now;
            requisicionBuscada.PositionID = requisicionRecibida.PositionID;
            if (string.IsNullOrWhiteSpace(requisicionRecibida.PositionName.ToUpper())) throw new Optimus.Core.Exceptions.ValidationException("Necesita especificar el nombre de la posición.");
            requisicionBuscada.PositionName = requisicionRecibida.PositionName.ToUpper();
            requisicionBuscada.QuantityEmployees = requisicionRecibida.QuantityEmployees;
            requisicionBuscada.Reason = requisicionRecibida.Reason;
            requisicionBuscada.Replace = requisicionRecibida.Replace;
            requisicionBuscada.StartDate = requisicionRecibida.StartDate;
            requisicionBuscada.StatusID = requisicionRecibida.StatusID;
            requisicionBuscada.SuperiorPositionID = requisicionRecibida.SuperiorPositionID;
            requisicionBuscada.User = requisicionRecibida.User;
            requisicionBuscada.Workday = requisicionRecibida.Workday;
            requisicionBuscada.WorkingHours = requisicionRecibida.WorkingHours;
            requisicionBuscada.guid = requisicionRecibida.guid;
            requisicionBuscada.Description = requisicionRecibida.Description;
            Context.SubmitChanges();

            return requisicionBuscada.PersonalRequisitionID;
        }

        #endregion Public Methods

        #region Private Methods

        private void ActualizarRequisiones()
        {
            TSTicketRepository tiquete = new TSTicketRepository(new DA.Optimus.OptimusEntities());
            var lista = (from a in Context.HRPersonnelRequisitions
                         where a.StatusID == 24 || a.StatusID == 27
                         select a).ToList();
            int? state = 0;
            foreach (var item in lista)
            {
                state = new TSTicketRepository(new DA.Optimus.OptimusEntities()).getStateTicketsByGUID((Guid)item.guid);
                switch (state)
                {
                    case null:
                        break;

                    case 2: //CANCELLED
                        item.StatusID = 26;
                        Save(item);
                        break;

                    case 3: //ASSIGNED
                        item.StatusID = 27;
                        Save(item);
                        break;

                    case 5: //DONE
                        item.StatusID = 25;
                        Save(item);
                        break;

                    case 6: //Validate
                        item.StatusID = 25;
                        Save(item);
                        break;

                    case 7: //INPROGRESS
                        item.StatusID = 24;
                        Save(item);
                        break;

                    case 4: //SUMITTED
                        item.StatusID = 24;
                        Save(item);
                        break;
                }
            }
        }

        #endregion Private Methods
    }
}