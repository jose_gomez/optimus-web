﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
   internal class PermissionsEmployeesRepository:IPermissionsEmployees
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
        ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        private ServiceContainer container;
        private UnitOfWork worker;

        public void savePermission(PermissionsEmployees EmpPerm)
        {
            HRPermissionsEmployee PerEmp = new HRPermissionsEmployee();
            PerEmp.EmployeeID = EmpPerm.EmployeeID;
            PerEmp.DateSubmit = EmpPerm.DateSubmit;
            PerEmp.DateEfective = EmpPerm.DateEfective;
            PerEmp.Comment = EmpPerm.Comment;
            PerEmp.HourStart = EmpPerm.HourStart;
            PerEmp.HourEnd = EmpPerm.HourEnd;
            PerEmp.PermissionReasonID = EmpPerm.PermissionReasonID;
            PerEmp.StatusID = EmpPerm.StatusID;
            PerEmp.Firma = EmpPerm.Firma;
            ContextSQL.HRPermissionsEmployees.InsertOnSubmit(PerEmp);
            ContextSQL.SubmitChanges();

        }
        public List<HRPermissionReason> GetPermissionReason()
        {
            var query = (from p in ContextSQL.HRPermissionReasons
                         select p
                         ).ToList();
            return query;
        } 
        public List<PermissionsEmployees> GetPermissionPending()
        {
            var query = (from p in ContextSQL.HRPermissionsEmployees
                         select new PermissionsEmployees
                         {
                             PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                             EmployeeID = (int)p.EmployeeID,
                             DateEfective = (DateTime)p.DateEfective,
                             DateSubmit = (DateTime)p.DateSubmit,
                             HourStart = (int)p.HourStart,
                             HourEnd =(int)p.HourEnd,
                             PermissionReasonID = (int)p.PermissionReasonID,
                             PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x=>x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                             Comment = p.Comment,
                             StatusID = (int)p.StatusID,
                             Status = ContextSQL.SYStatus.FirstOrDefault(x=>x.StatusID == p.StatusID).StatusName,
                          

                         }).ToList();

            return query;
        }
        public bool AprovedHHRRUser(int EmployeeID)
        {
            var emp = ContextSQL.HREmployees.FirstOrDefault(x => x.EmployeeId == EmployeeID);
            var query = (from p in ContextSQL.HRRequistionWorkFlows
                         where p.PositionID == emp.PositionID && p.TypeRequisitionID == 5
                         select p).FirstOrDefault();

            if (query != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public void AprovedOrPreAprovedPermission(int PermissionID,bool Aproved)
        {
            HRPermissionsEmployee PermEmp = ContextSQL.HRPermissionsEmployees.FirstOrDefault(x=>x.PermissionsEmployeesID == PermissionID);
            // PermEmp.StatusID = Aproved?27:50;
            PermEmp.StatusID = Aproved ? 27 : 36;
            ContextSQL.SubmitChanges();
        }

        public List<PermissionsEmployees> GetListPermissionPending(string user)
        {
            var users = ContextSQL.SYUsers.FirstOrDefault(x => x.UserName == user);
            var emp = ContextSQL.HREmployees.FirstOrDefault(x => x.EmployeeId == users.EmployeeID);
            List<PermissionsEmployees> PermisionEmployee;
            if (AprovedHHRRUser((int)users.EmployeeID))
            {
                PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     where p.StatusID == 36
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

                return PermisionEmployee;
            }else
            {
                PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     join u in ContextSQL.SYAprobationsRequests on t.DepartmentID equals u.DepartmentID
                                     where p.StatusID == 33 && u.UserID == users.UserID
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

                return PermisionEmployee;
            }
        }
        public List<PermissionsEmployees> GetListPermissionPendingEmployeesBeEmployeeID(int employeeID)
        {
            var users = ContextSQL.SYUsers.FirstOrDefault(x => x.EmployeeID == employeeID);
            var emp = ContextSQL.HREmployees.FirstOrDefault(x => x.EmployeeId == users.EmployeeID);
            List<PermissionsEmployees> PermisionEmployee;
            if (AprovedHHRRUser((int)users.EmployeeID))
            {
                PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     where p.EmployeeID == employeeID && p.StatusID == 36
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

                return PermisionEmployee;
            }
            else
            {
                PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     where p.EmployeeID == employeeID &&( p.StatusID == 33||p.StatusID == 36)
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

                return PermisionEmployee;

            }
                
        }
        public List<PermissionsEmployees> GetListPermissionAprovedEmployeesBeEmployeeID(int employeeID)
        {
            var PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     where p.EmployeeID == employeeID && p.StatusID == 27
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

            return PermisionEmployee;
        }
        public List<PermissionsEmployees> GetListPermissionAproved(string user)
        {
            var users = ContextSQL.SYUsers.FirstOrDefault(x => x.UserName == user);
            var emp = ContextSQL.HREmployees.FirstOrDefault(x => x.EmployeeId == users.EmployeeID);
            List<PermissionsEmployees> PermisionEmployee;
            if (AprovedHHRRUser((int)users.EmployeeID))
            {
                PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     where p.StatusID == 27
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

                return PermisionEmployee;
            }
            else
            {

                PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     join u in ContextSQL.SYAprobationsRequests on t.DepartmentID equals u.DepartmentID
                                     where (p.StatusID == 27 || p.StatusID == 36) && u.UserID == users.UserID
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

                return PermisionEmployee;

            }
        }

        public string GetJornadaByEmployeeID(int employeeID)
        {
            var Query = ContextSQL.SPGetJornadaByEmployeeID(employeeID).FirstOrDefault() ;
            return Query.Jornada;
        }
        public List<PermissionsEmployees> GetListPermissionAprovedOnDateEmployeesBeEmployeeID(int employeeID)
        {
            var PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     where p.EmployeeID == employeeID && p.StatusID == 27 && p.DateEfective == DateTime.Now.Date
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

            return PermisionEmployee;
        }
        public List<PermissionsEmployees> GetListPermissionCanceledEmployeesBeEmployeeID(int employeeID)
        {
            var PermisionEmployee = (from p in ContextSQL.HRPermissionsEmployees
                                     join t in ContextSQL.HREmployees on p.EmployeeID equals t.EmployeeId
                                     where p.EmployeeID == employeeID && p.StatusID == 26
                                     select new PermissionsEmployees
                                     {
                                         PermissionsEmployeesID = (int)p.PermissionsEmployeesID,
                                         EmployeeID = (int)p.EmployeeID,
                                         DateEfective = (DateTime)p.DateEfective,
                                         DateSubmit = (DateTime)p.DateSubmit,
                                         HourStart = (int)p.HourStart,
                                         HourEnd = (int)p.HourEnd,
                                         PermissionReasonID = (int)p.PermissionReasonID,
                                         PermissionReason = ContextSQL.HRPermissionReasons.FirstOrDefault(x => x.PermissionReasonID == p.PermissionReasonID).PermissionReason,
                                         Comment = p.Comment,
                                         StatusID = (int)p.StatusID,
                                         Status = ContextSQL.SYStatus.FirstOrDefault(x => x.StatusID == p.StatusID).StatusName,
                                     }).ToList();

            return PermisionEmployee;
        }
    }
}
