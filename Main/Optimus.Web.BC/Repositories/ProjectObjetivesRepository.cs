﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA.Optimus;
using System.Linq.Expressions;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Repositories
{
    public class ProjectObjetivesRepository : Repository<TSProjectObjetives>, IProjectObjetive
    {
        public ProjectObjetivesRepository(OptimusEntities context) : base(context)
        {
        }


        public List<ProjectObjective> GetObjetives()
        {
            var query = (from p in Context.TSProjectObjetives
                         join t in Context.TSProjectObjetivesTypes on p.ProjectObjetivesTypesID equals t.ProjectObjetivesTypesID
                         join l in Context.HRDepartments on p.DepartmentID equals l.DepartmentId

                         select new ProjectObjective
                         {
                             ProjectObjetivesID = p.ProjectObjetivesID,
                             Title = p.Title,
                             Description = p.Description,
                             EstimatedDate = (DateTime)p.EstimatedDate,
                             PorcentProgress = (int)p.PorcentProgress,
                             FatherProject = (int)p.FatherProject,
                             Year = (int)p.Year,
                             StatusID = (int)p.StatusID,
                             DepartmentID = (int)p.DepartmentID,
                             Department = l.Name,
                             ProjectObjetivesTypesID = (int)t.ProjectObjetivesTypesID,
                             ProjectObjetives = t.Name
                         }).ToList();

            return query;
        }
        
    }
}
