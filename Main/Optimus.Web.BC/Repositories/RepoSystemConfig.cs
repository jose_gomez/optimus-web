﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Repositories
{
    internal class RepoSystemConfig : ISystemConfig
    {
        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        public SYSystemsConfiguration GetSysTemConfigByName(string name)
        {
            var result = (from a in Context.SYSystemsConfigurations
                          where a.SysConfigName == name.Trim().ToUpper()
                          select a).FirstOrDefault();
            return result;
        }

        void ICRUD.Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        void ICRUD.Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
