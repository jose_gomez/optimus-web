﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class AnalisisPosicion : Contracts.API.AnalisisPosicion
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public DTO.AnalisisPosicion Get(int id)
        {
            var result = (from a in Context.HRPositionAnalisies
                          where a.PositionAnalysisID == id
                          select new DTO.AnalisisPosicion
                          {
                              ID = a.PositionAnalysisID,
                              PositionID = a.PositionID,
                              RequisitionID = a.RequisitionID,
                              StatusID = a.StatusID,
                              UserID = a.UserID,
                              PositionName = a.PositionName,
                              CreateDate = a.Create_dt,
                              ModifyDate = a.Modify_ft
                          }).FirstOrDefault();
            return result;
        }

        public DTO.AnalisisPosicion GetByPosition(int positionID)
        {
            var result = (from a in Context.HRPositionAnalisies
                          where a.PositionID == positionID
                          select new DTO.AnalisisPosicion
                          {
                              ID = a.PositionAnalysisID,
                              PositionID = a.PositionID,
                              RequisitionID = a.RequisitionID,
                              PositionName = a.PositionName,
                              StatusID = a.StatusID,
                              UserID = a.UserID,
                              CreateDate = a.Create_dt,
                              ModifyDate = a.Modify_ft
                          }).FirstOrDefault();
            return result;
        }

        public DTO.AnalisisPosicion GetByRequisicion(int requisicionID)
        {
            var result = (from a in Context.HRPositionAnalisies
                          where a.RequisitionID == requisicionID
                          select new DTO.AnalisisPosicion
                          {
                              ID = a.PositionAnalysisID,
                              PositionID = a.PositionID,
                              RequisitionID = a.RequisitionID,
                              PositionName = a.PositionName,
                              StatusID = a.StatusID,
                              UserID = a.UserID,
                              CreateDate = a.Create_dt,
                              ModifyDate = a.Modify_ft
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.AnalisisPosicionDetalle> GetDetalles(int id)
        {
            Repositories.API.AnalisisPosicionDetalle detalleRepo = new API.AnalisisPosicionDetalle();
            return detalleRepo.GetByAnalisis(id);
        }

        public DTO.AnalisisPosicion Save(DTO.AnalisisPosicion analisis)
        {
            var result = (from a in Context.HRPositionAnalisies
                          where a.PositionAnalysisID == analisis.ID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new DA.HRPositionAnalisy();
                result.Create_dt = DateTime.Now;
                Context.HRPositionAnalisies.InsertOnSubmit(result);
            }
            result.PositionID = analisis.PositionID;
            result.RequisitionID = analisis.RequisitionID;
            result.StatusID = analisis.StatusID;
            result.PositionName = analisis.PositionName;
            result.UserID = analisis.UserID;
            result.Modify_ft = DateTime.Now;
            Context.SubmitChanges();
            return new DTO.AnalisisPosicion
            {
                ID = result.PositionAnalysisID,
                PositionID = result.PositionID,
                RequisitionID = result.RequisitionID,
                PositionName = result.PositionName,
                StatusID = result.StatusID,
                UserID = result.UserID,
                CreateDate = result.Create_dt,
                ModifyDate = result.Modify_ft
            };
        }

        #endregion Public Methods

    }
}