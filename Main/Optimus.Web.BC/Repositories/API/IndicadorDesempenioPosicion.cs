﻿using Optimus.Web.DA;
using System.Linq;
using Optimus.DTO;
using System;
using System.Collections.Generic;

namespace Optimus.Web.BC.Repositories.API
{
    public class IndicadorDesempenioPosicion : Contracts.API.IndicadorDesempenioPosicion
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public DTO.IndicadorDesempenioPosicion Create(DTO.IndicadorDesempenioPosicion indicadorDesempenioPosicion)
        {
            var result = (from a in Context.HRPositionsIndicators
                          where a.IndicatorID == indicadorDesempenioPosicion.IndicatorID &&
                          a.PositionID == indicadorDesempenioPosicion.PositionID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new HRPositionsIndicator();
                result.IndicatorID = indicadorDesempenioPosicion.IndicatorID;
                result.PositionID = indicadorDesempenioPosicion.PositionID;
                result.Percentage = indicadorDesempenioPosicion.Percentage;
                Context.HRPositionsIndicators.InsertOnSubmit(result);
                Context.SubmitChanges();
            }
            DTO.IndicadorDesempenioPosicion porcentajeIndicador = new DTO.IndicadorDesempenioPosicion();
            porcentajeIndicador.IndicatorID = result.IndicatorID;
            porcentajeIndicador.PositionID = result.PositionID;
            porcentajeIndicador.Percentage = result.Percentage;
            return porcentajeIndicador;
        }

        public void Delete(int positionID, int indicatorID)
        {
            var result = (from a in Context.HRPositionsIndicators
                          where a.IndicatorID == indicatorID &&
                          a.PositionID == positionID
                          select a).FirstOrDefault();
            if (result!=null)
            {
                Context.HRPositionsIndicators.DeleteOnSubmit(result);
                Context.SubmitChanges();
            }
        }

        public DTO.IndicadorDesempenioPosicion Get(int positionID, int indicatorID)
        {
            IndicadorDesempenio indicatorRepo = new API.IndicadorDesempenio();
            var result = (from a in Context.HRPositionsIndicators
                          where a.IndicatorID == indicatorID &&
                          a.PositionID == positionID
                          select new DTO.IndicadorDesempenioPosicion
                          {
                              IndicatorID = a.IndicatorID,
                              PositionID = a.PositionID,
                              Percentage = a.Percentage
                          }).FirstOrDefault();
            if (result!=null)
            {
                var indicador = indicatorRepo.Get(result.IndicatorID);
                result.Description = indicador.Description;
                result.Name = indicador.Name;
            }
            return result;
        }

        public List<DTO.IndicadorDesempenioPosicion> GetByPosition(int positionID)
        {
            IndicadorDesempenio indicatorRepo = new API.IndicadorDesempenio();
            var result = (from a in Context.HRPositionsIndicators
                          where a.PositionID == positionID
                          select new DTO.IndicadorDesempenioPosicion
                          {
                              IndicatorID = a.IndicatorID,
                              PositionID = a.PositionID,
                              Percentage = a.Percentage
                          }).ToList();
            List<DTO.IndicadorDesempenioPosicion> newLista = new List<DTO.IndicadorDesempenioPosicion>();
            foreach (var item in result)
            {
                var indicador = indicatorRepo.Get(item.IndicatorID);
                item.Description = indicador.Description;
                item.Name = indicador.Name;
                newLista.Add(item);
            }
            
            return newLista;
        }

        public DTO.IndicadorDesempenioPosicion Save(DTO.IndicadorDesempenioPosicion indicadorDesempenioPosicion)
        {
            DTO.IndicadorDesempenioPosicion porcentajeIndicador = new DTO.IndicadorDesempenioPosicion();
            var result = (from a in Context.HRPositionsIndicators
                          where a.IndicatorID == indicadorDesempenioPosicion.IndicatorID &&
                          a.PositionID == indicadorDesempenioPosicion.PositionID
                          select a).FirstOrDefault();
            if (result == null)
            {
                porcentajeIndicador = Create(indicadorDesempenioPosicion);
            }
            else
            {
                porcentajeIndicador = Update(indicadorDesempenioPosicion);
            }
            return porcentajeIndicador;
        }

        public DTO.IndicadorDesempenioPosicion Update(DTO.IndicadorDesempenioPosicion indicadorDesempenioPosicion)
        {
            var result = (from a in Context.HRPositionsIndicators
                          where a.IndicatorID == indicadorDesempenioPosicion.IndicatorID &&
                          a.PositionID == indicadorDesempenioPosicion.PositionID
                          select a).FirstOrDefault();
            if (result != null)
            {
                result.Percentage = indicadorDesempenioPosicion.Percentage;
                Context.SubmitChanges();
            }
            DTO.IndicadorDesempenioPosicion porcentajeIndicador = new DTO.IndicadorDesempenioPosicion();
            porcentajeIndicador.IndicatorID = result.IndicatorID;
            porcentajeIndicador.PositionID = result.PositionID;
            porcentajeIndicador.Percentage = result.Percentage;
            return porcentajeIndicador;
        }

        #endregion Public Methods

    }
}