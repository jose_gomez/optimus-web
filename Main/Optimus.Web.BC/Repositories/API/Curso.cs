﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Curso : Contracts.API.Curso
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.Curso> GetCAPEnProceso()
        {
            var result = (from a in Context.HRCourses
                          join b in Context.HRSessions on a.CourseID equals b.CourseID
                          where ((DateTime)b.StartDate).Date <= DateTime.Now.Date && ((DateTime)b.EndDate).Date >= DateTime.Now.Date && a.TypeCourseID == 1
                          select new DTO.Curso
                          {
                              CourseID = a.CourseID,
                              Title = a.Title
                          }).Distinct().OrderBy(x => x.Title).ToList();
            return result;
        }

        public List<DTO.Curso> GetCAPPendientes()
        {
            var result = (from a in Context.HRCourses
                          join b in Context.HRSessions on a.CourseID equals b.CourseID
                          where ((DateTime)b.StartDate).Date > DateTime.Now.Date && a.TypeCourseID == 1
                          select new DTO.Curso
                          {
                              CourseID = a.CourseID,
                              Title = a.Title
                          }).Distinct().OrderBy(x => x.Title).ToList();
            return result;
        }

        public List<DTO.Curso> GetEnProceso()
        {
            var result = (from a in Context.HRCourses
                          join b in Context.HRSessions on a.CourseID equals b.CourseID
                          where ((DateTime)b.StartDate).Date <= DateTime.Now.Date && ((DateTime)b.EndDate).Date >= DateTime.Now.Date
                          select new DTO.Curso
                          {
                              CourseID = a.CourseID,
                              Title = a.Title
                          }).Distinct().OrderBy(x => x.Title).ToList();
            return result;
        }

        public List<DTO.Curso> GetPendientes()
        {
            var result = (from a in Context.HRCourses
                          join b in Context.HRSessions on a.CourseID equals b.CourseID
                          where ((DateTime)b.StartDate).Date > DateTime.Now.Date
                          select new DTO.Curso
                          {
                              CourseID = a.CourseID,
                              Title = a.Title
                          }).Distinct().OrderBy(x => x.Title).ToList();
            return result;
        }

        #endregion Public Methods

    }
}