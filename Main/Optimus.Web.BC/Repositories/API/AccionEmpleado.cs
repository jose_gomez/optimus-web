﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class AccionEmpleado : Contracts.API.AccionEmpleado
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public DTO.AccionEmpleado Create(DTO.AccionEmpleado accion)
        {
            var result = Get(accion.ID);
            if (result == null)
            {
                var nuevaAccion = new DA.HREmployeesAction();
                nuevaAccion.ActionTypeID = accion.ActionTypeID;
                nuevaAccion.EmployeeID = accion.EmployeeID;
                nuevaAccion.InitialValue = accion.InitialValue;
                nuevaAccion.FinalValue = accion.FinalValue;
                nuevaAccion.Execute_dt = accion.Execute_dt;
                nuevaAccion.StatusID = accion.StatusID;
                nuevaAccion.CreateUser = accion.CreateUserID;
                nuevaAccion.Create_dt = DateTime.Now;
                Context.HREmployeesActions.InsertOnSubmit(nuevaAccion);
                Context.SubmitChanges();
                result = Get(nuevaAccion.EmployeesActionID);
            }
            return result;
        }

        public void Delete(int id)
        {
            throw new NotImplementedException();
        }

        public List<DTO.AccionEmpleado> Get()
        {
            var result = (from a in Context.HREmployeesActions
                          select new DTO.AccionEmpleado
                          {
                              ID = a.EmployeesActionID,
                              ActionTypeID = a.ActionTypeID,
                              EmployeeID = a.EmployeeID,
                              InitialValue = a.InitialValue,
                              FinalValue = a.FinalValue,
                              Execute_dt = a.Execute_dt,
                              StatusID = a.StatusID,
                              CreateUserID = a.CreateUser,
                              ModifyUserID = a.ModifyUser,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).ToList();
            return result;
        }

        public DTO.AccionEmpleado Get(int id)
        {
            var result = (from a in Context.HREmployeesActions
                          where a.EmployeesActionID == id
                          select new DTO.AccionEmpleado
                          {
                              ID = a.EmployeesActionID,
                              ActionTypeID = a.ActionTypeID,
                              EmployeeID = a.EmployeeID,
                              InitialValue = a.InitialValue,
                              FinalValue = a.FinalValue,
                              Execute_dt = a.Execute_dt,
                              StatusID = a.StatusID,
                              CreateUserID = a.CreateUser,
                              ModifyUserID = a.ModifyUser,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.AccionEmpleado> GetPendientes()
        {
            var result = (from a in Context.HREmployeesActions
                          where a.StatusID == (int)Core.Enumeraciones.AccionesEmpleadosEstados.PENDIENTE
                          select new DTO.AccionEmpleado
                          {
                              ID = a.EmployeesActionID,
                              ActionTypeID = a.ActionTypeID,
                              EmployeeID = a.EmployeeID,
                              InitialValue = a.InitialValue,
                              FinalValue = a.FinalValue,
                              Execute_dt = a.Execute_dt,
                              StatusID = a.StatusID,
                              CreateUserID = a.CreateUser,
                              ModifyUserID = a.ModifyUser,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).ToList();
            return result;
        }

        public List<DTO.AccionEmpleado> GetPendientes(DateTime fecha)
        {
            var result = (from a in Context.HREmployeesActions
                          where a.StatusID == (int)Core.Enumeraciones.AccionesEmpleadosEstados.PENDIENTE &&
                          a.Execute_dt == fecha
                          select new DTO.AccionEmpleado
                          {
                              ID = a.EmployeesActionID,
                              ActionTypeID = a.ActionTypeID,
                              EmployeeID = a.EmployeeID,
                              InitialValue = a.InitialValue,
                              FinalValue = a.FinalValue,
                              Execute_dt = a.Execute_dt,
                              StatusID = a.StatusID,
                              CreateUserID = a.CreateUser,
                              ModifyUserID = a.ModifyUser,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).ToList();
            return result;
        }

        public DTO.AccionEmpleado Update(DTO.AccionEmpleado accion)
        {
            DTO.AccionEmpleado result = null;
            var accionActualizar = (from a in Context.HREmployeesActions
                                    where a.EmployeesActionID == accion.ID
                                    select a).FirstOrDefault();
            if (accionActualizar != null)
            {
                accionActualizar.FinalValue = accion.FinalValue;
                accionActualizar.Execute_dt = accion.Execute_dt;
                accionActualizar.StatusID = accion.StatusID;
                accionActualizar.ModifyUser = accion.ModifyUserID;
                accionActualizar.Modify_dt = DateTime.Now;
                Context.SubmitChanges();
                result = Get(accionActualizar.EmployeesActionID);
            }
            return result;
        }

        #endregion Public Methods

    }
}