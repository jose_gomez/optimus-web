﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class TipoReporte : Contracts.API.TipoReporte
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.TipoReporte> Get()
        {
            var result = (from a in Context.SyReportTypes
                          select new DTO.TipoReporte
                          {
                              ID = a.ReportTypeID,
                              Name = a.Name
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }

        public DTO.TipoReporte Get(int id)
        {
            var result = (from a in Context.SyReportTypes
                          where a.ReportTypeID == id
                          select new DTO.TipoReporte
                          {
                              ID = a.ReportTypeID,
                              Name = a.Name
                          }).OrderBy(x => x.Name).FirstOrDefault();
            return result;
        }

        #endregion Public Methods

    }
}