﻿using Optimus.DTO;
using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Departamento : Contracts.API.Departamento
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.Departamento> Get()
        {
            var result = (from a in Context.HRDepartments
                          select new DTO.Departamento
                          {
                              DepartmentID = a.DepartmentId,
                              Name = a.Name
                          }).OrderBy(x => x.Name).ToList();

            return result;
        }

        public DTO.Departamento Get(int id)
        {
            var result = (from a in Context.HRDepartments
                          where a.DepartmentId == id
                          select new DTO.Departamento
                          {
                              DepartmentID = a.DepartmentId,
                              Name = a.Name
                          }).FirstOrDefault();

            return result;
        }

        public List<Posicion> GetPositionsActiveOfDepartment(int departmentID)
        {
            Position positionRepo = new Position();
            var result = positionRepo.GetPositionsByDeparment(departmentID);
            var resultFiltered = (from a in result
                                  where a.Status == (int)Optimus.Core.Enumeraciones.PositionStatus.ACTIVO
                                  select a).ToList();
            return resultFiltered;
        }

        public List<Posicion> GetPositionsInactiveOfDepartment(int departmentID)
        {
            Position positionRepo = new Position();
            var result = positionRepo.GetPositionsByDeparment(departmentID);
            var resultFiltered = (from a in result
                                  where a.Status == (int)Optimus.Core.Enumeraciones.PositionStatus.INACTIVO
                                  select a).ToList();
            return resultFiltered;
        }

        #endregion Public Methods

    }
}