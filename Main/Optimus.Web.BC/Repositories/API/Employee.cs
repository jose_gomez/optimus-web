﻿using Optimus.DTO;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Employee : Contracts.API.Employee
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void Create(object obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(object obj)
        {
            throw new NotImplementedException();
        }

        public List<DTO.AccionEmpleado> GetEmployeeActions(int employeeID)
        {
            var result = (from a in Context.HREmployeesActions
                          where a.EmployeeID == employeeID
                          select new DTO.AccionEmpleado
                          {
                              ID = a.EmployeesActionID,
                              EmployeeID = a.EmployeeID,
                              ActionTypeID = a.ActionTypeID,
                              InitialValue = a.InitialValue,
                              FinalValue = a.FinalValue,
                              Execute_dt = a.Execute_dt,
                              StatusID = a.StatusID,
                              CreateUserID = a.CreateUser,
                              ModifyUserID = a.ModifyUser,
                              Create_dt = a.Create_dt,
                              Modify_dt = a.Modify_dt
                          }).ToList();
            return result;
        }

        public Empleado GetEmployeeByID(int id)
        {
            Position positionRepo = new Position();
            Person PersonRepo = new Person();
            var employee = (from a in Context.HREmployees
                            where a.EmployeeId == id
                            select new Empleado
                            {
                                EmployeeID = a.EmployeeId,
                                CompanyID = a.CompanyID,
                                Status = a.Status,
                                PositionID = a.PositionID ?? 0,
                                ProductionCompensation = a.ProductionCompensation,
                                BaseSalary = a.BaseSalary,
                                EntryDate = a.EntryDate,
                                PaymentFrequency = a.PaymentFrequency,
                                IdJornada = a.IdJornada,
                                PaidByTheHour = a.PaidByTheHour,
                                TransactionPayment = a.TransactionPayment,
                                FinancialEntityID = a.FinancialEntityID,
                                Account = a.Account,
                                ProofPayment = a.ProofPayment,
                                PayByPayroll = a.PayByPayroll,
                                SDSS = a.SDSS,
                                ARS = a.ARS,
                                ContractTypeID = a.ContractTypeID,
                                EmployeeTypeID = a.EmployeeTypeID,
                                SeguroDental = a.SeguroDental,
                                IncentivesPayment = a.IncentivesPayment,
                                PersonID = a.PersonID ?? 0,
                                UserID = a.UserID ?? 0,
                                Picture = GetPicture(a.PictureGUID),
                                CreateDate = a.Create_dt,
                                ModifyDate = a.ModifyDate,
                            }).FirstOrDefault();
            return employee;
        }

        public Posicion GetEmployeePosition(int employeeID)
        {
            Posicion result = null;
            var posicionID = (from a in Context.HREmployees
                              where a.EmployeeId == employeeID
                              select a.PositionID).FirstOrDefault();
            if (posicionID != null)
            {
                Position positionRepo = new Position();
                result = positionRepo.GetPositionByID((int)posicionID);
            }
            return result;
        }

        public List<Empleado> GetEmployeesByPosition(int positionID)
        {
            Position positionRepo = new Position();
            Person PersonRepo = new Person();
            var employees = (from a in Context.HREmployees
                             where a.PositionID == positionID
                             select new Empleado
                             {
                                 EmployeeID = a.EmployeeId,
                                 PositionID = a.PositionID ?? 0,
                                 PersonID = a.PersonID ?? 0,
                                 UserID = a.UserID ?? 0,
                                 CreateDate = a.Create_dt,
                                 ModifyDate = a.ModifyDate,
                             }).OrderBy(x => x.EmployeeID).ToList();
            return employees;
        }

        public void Save(object obj)
        {
            Empleado employee = (Empleado)obj;
            var employeeFinded = (from a in Context.HREmployees
                                  where a.EmployeeId == employee.EmployeeID
                                  select a).FirstOrDefault();
            if (employeeFinded == null)
            {
                Create(employee);
            }
            else
            {
                Update(employee);
            }
        }

        public void Update(object obj)
        {
            Empleado employee = (DTO.Empleado)obj;
            var employeeFinded = (from a in Context.HREmployees
                                  where a.EmployeeId == employee.EmployeeID
                                  select a).FirstOrDefault();
            if (employeeFinded != null)
            {
                employeeFinded.CompanyID = employee.CompanyID;
                employeeFinded.ProductionCompensation = employee.ProductionCompensation;
                employeeFinded.BaseSalary = employee.BaseSalary;
                employeeFinded.ModifyDate = DateTime.Now;
                employeeFinded.UserID = employee.UserID;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private byte[] GetPicture(string guid)
        {
            var pic = Context.SYGUIDImages.Where(x => x.GUID == guid).FirstOrDefault();
            if (pic != null)
            {
                return pic.Picture.ToArray();
            }
            return null;
        }

        private void validate(DTO.Empleado employee)
        {
            var employeeFinded = (from a in Context.HREmployees
                                  where a.PersonID == employee.PersonID && a.EmployeeId != employee.EmployeeID
                                  select a).FirstOrDefault();
            if (employeeFinded != null)
            {
                throw new Core.Exceptions.ValidationException(Core.OptimusMessages.PERSONA_YA_ES_EMPLEADO);
            }
        }

        #endregion Private Methods

    }
}