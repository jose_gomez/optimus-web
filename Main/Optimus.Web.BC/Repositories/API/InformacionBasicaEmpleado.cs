﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class InformacionBasicaEmpleado : Contracts.API.InformacionBasicaEmpleado
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        private Position PositionRepo = new Position();

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.InformacionBasicaEmpleado> Get()
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> Get(string filtro)
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public DTO.InformacionBasicaEmpleado Get(int id)
        {
            var result = (from a in Context.HREmployees
                          join b in Context.HRPeoples on a.PersonID equals b.PersonID
                          where a.EmployeeId == id
                          select new DTO.InformacionBasicaEmpleado
                          {
                              EmployeeID = a.EmployeeId,
                              PersonID = b.PersonID,
                              FirstName = b.FirstName,
                              LastName = b.LastName,
                              PositionID = a.PositionID == null ? 0 : a.PositionID,
                          }).FirstOrDefault();
            if (result != null)
            {
                var position = PositionRepo.GetPositionByID((int)result.PositionID);
                result.PositionName = position == null ? string.Empty : position.PositionName;
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetActivos()
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.ACTIVO || a.Status == "A")
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetActivos(string filtro)
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.ACTIVO || a.Status == "A") &&
                                  (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetInactivos()
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.INACTIVO || a.Status == "I")
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetInactivos(string filtro)
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.INACTIVO || a.Status == "I") &&
                                  (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetLiquidados()
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.LIQUIDADO || a.Status == "L")
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetLiquidados(string filtro)
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.LIQUIDADO || a.Status == "L") &&
                                  (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetPendientes()
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.PENDIENTE || a.Status == "P")
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        public List<DTO.InformacionBasicaEmpleado> GetPendientes(string filtro)
        {
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRPeoples on a.PersonID equals b.PersonID
                                  where (a.StatusID == (int)Core.Enumeraciones.EmployeeStatus.PENDIENTE || a.Status == "P") &&
                                  (a.FirstName.Contains(filtro) || a.LastName.Contains(filtro) || a.EmployeeId.ToString().Contains(filtro))
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      PersonID = b.PersonID,
                                      FirstName = b.FirstName,
                                      LastName = b.LastName,
                                      PositionID = a.PositionID == null ? 0 : a.PositionID,
                                  }).ToList();
            var result = new List<DTO.InformacionBasicaEmpleado>();
            foreach (var item in listaEmpleados)
            {
                var position = PositionRepo.GetPositionByID((int)item.PositionID);
                item.PositionName = position == null ? string.Empty : position.PositionName;
                result.Add(item);
            }
            return result;
        }

        #endregion Public Methods

    }
}