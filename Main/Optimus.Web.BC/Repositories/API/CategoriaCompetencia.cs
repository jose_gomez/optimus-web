﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using Optimus.DTO;

namespace Optimus.Web.BC.Repositories.API
{
    public class CategoriaCompetencia : Contracts.API.CategoriaCompetencia
    {
        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.CategoriaCompetencia> Get()
        {
            var result = (from a in Context.HRCompetencesCategories
                          select new DTO.CategoriaCompetencia
                          {
                              CompetenceCategoryID = a.CompetenceCategoryID,
                              Name = a.Name
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }

        public DTO.CategoriaCompetencia Get(int id)
        {
            var result = (from a in Context.HRCompetencesCategories
                          where a.CompetenceCategoryID == id
                          select new DTO.CategoriaCompetencia
                          {
                              CompetenceCategoryID = a.CompetenceCategoryID,
                              Name = a.Name
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.Competencia> GetCompetenciasOfCategoria(int id)
        {
            Repositories.API.Competencia repo = new Competencia();
            return repo.GetByCategoria(id);
        }

        #endregion Public Methods
    }
}