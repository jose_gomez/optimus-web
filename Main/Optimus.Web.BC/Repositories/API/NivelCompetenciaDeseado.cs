﻿using Optimus.Web.DA;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class NivelCompetenciaDeseado : Contracts.API.NivelCompetenciaDeseado
    {
        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public DTO.NivelCompetenciaDeseado Get(int positionID, int competenciaID)
        {
            var result = (from a in Context.HRPositionCompetences
                          where a.PositionID == positionID && a.CompetenceID == competenciaID
                          select new DTO.NivelCompetenciaDeseado
                          {
                              CompetenceID = a.CompetenceID,
                              PositionID = a.PositionID,
                              Level = a.Level
                          }).FirstOrDefault();
            if (result == null)
            {
                result = new DTO.NivelCompetenciaDeseado();
            }
            return result;
        }

        #endregion Public Methods
    }
}