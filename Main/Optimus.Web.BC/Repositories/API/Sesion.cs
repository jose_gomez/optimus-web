﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Sesion : Contracts.API.Sesion
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.InformacionBasicaEmpleado> GetParticipantes(int sesionID)
        {
            Repositories.API.Position posit = new Repositories.API.Position();
            Repositories.API.Departamento departamento = new Departamento();
            var listaEmpleados = (from a in Context.HREmployees
                                  join b in Context.HRListSessions on a.EmployeeId equals b.EmployeeID
                                  where b.SessionID == sesionID
                                  select new DTO.InformacionBasicaEmpleado
                                  {
                                      EmployeeID = a.EmployeeId,
                                      FirstName = a.FirstName,
                                      LastName = a.LastName,
                                      PositionID = a.PositionID,
                                      DepartmentID = a.DepartmentID
                                  }).OrderBy(x => x.FirstName).ToList();

            foreach (var item in listaEmpleados)
            {
                if (item.PositionID != null && item.PositionID != 0)
                {
                    item.PositionName = posit.GetPositionByID((int)item.PositionID).PositionName;
                }
                if (item.DepartmentID != null && item.DepartmentID != 0)
                {
                    item.DepartmentName = departamento.Get((int)item.DepartmentID).Name;
                }
            }
            return listaEmpleados;
        }

        public List<DTO.Sesion> GetSesionesCAPEnProcesoByCurso(int cursoID)
        {
            var result = (from a in Context.HRSessions
                          join b in Context.HRCourses on a.CourseID equals b.CourseID
                          where ((DateTime)a.StartDate).Date <= DateTime.Now.Date && ((DateTime)a.EndDate).Date >= DateTime.Now.Date && b.TypeCourseID == 1
                          && a.CourseID == cursoID
                          select new DTO.Sesion
                          {
                              SessionID = a.SessionID,
                              SessionCode = a.SessionCode,
                              StartDate = a.StartDate,
                              EndDate = a.EndDate
                          }).OrderBy(x => x.SessionCode).ToList();
            return result;
        }

        public List<DTO.Sesion> GetSesionesCAPPendientesByCurso(int cursoID)
        {
            var result = (from a in Context.HRSessions
                          join b in Context.HRCourses on a.CourseID equals b.CourseID
                          where ((DateTime)a.StartDate).Date > DateTime.Now.Date && b.TypeCourseID == 1
                          && a.CourseID == cursoID
                          select new DTO.Sesion
                          {
                              SessionID = a.SessionID,
                              SessionCode = a.SessionCode,
                              StartDate = a.StartDate,
                              EndDate = a.EndDate
                          }).OrderBy(x => x.SessionCode).ToList();
            return result;
        }

        public List<DTO.Sesion> GetSesionesEnProcesoByCurso(int cursoID)
        {
            var result = (from a in Context.HRSessions
                          where ((DateTime)a.StartDate).Date <= DateTime.Now.Date && ((DateTime)a.EndDate).Date >= DateTime.Now.Date
                          && a.CourseID == cursoID
                          select new DTO.Sesion
                          {
                              SessionID = a.SessionID,
                              SessionCode = a.SessionCode,
                              StartDate = a.StartDate,
                              EndDate = a.EndDate
                          }).OrderBy(x => x.SessionCode).ToList();
            return result;
        }

        public List<DTO.Sesion> GetSesionesPendientesByCurso(int cursoID)
        {
            var result = (from a in Context.HRSessions
                          where ((DateTime)a.StartDate).Date > DateTime.Now.Date
                          && a.CourseID == cursoID
                          select new DTO.Sesion
                          {
                              SessionID = a.SessionID,
                              SessionCode = a.SessionCode,
                              StartDate = a.StartDate,
                              EndDate = a.EndDate
                          }).OrderBy(x => x.SessionCode).ToList();
            return result;
        }

        #endregion Public Methods

    }
}