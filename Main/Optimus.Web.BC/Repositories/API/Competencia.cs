﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;
using Optimus.DTO;
using System;

namespace Optimus.Web.BC.Repositories.API
{
    public class Competencia : Contracts.API.Competencia
    {
        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.Competencia> Get()
        {
            var result = (from a in Context.HRCompetences
                          select new DTO.Competencia
                          {
                              CompetenceID = a.CompetenceID,
                              Name = a.Name,
                              Description = a.Description,
                              CompetenceCategoryID = a.CompetenceCategoryID
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }

        public DTO.Competencia Get(int id)
        {
            var result = (from a in Context.HRCompetences
                          where a.CompetenceID == id
                          select new DTO.Competencia
                          {
                              CompetenceID = a.CompetenceID,
                              Name = a.Name,
                              Description = a.Description,
                              CompetenceCategoryID = a.CompetenceCategoryID
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.Competencia> GetByCategoria(int id)
        {
            var result = (from a in Context.HRCompetences
                          where a.CompetenceCategoryID == id
                          select new DTO.Competencia
                          {
                              CompetenceID = a.CompetenceID,
                              Name = a.Name,
                              Description = a.Description,
                              CompetenceCategoryID = a.CompetenceCategoryID
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }

        public List<DTO.Competencia> GetByCategoriaPosicion(int posicion, int categoria)
        {
            NivelCompetenciaDeseado nivelRepo = new NivelCompetenciaDeseado();
            var result = (from a in Context.HRCompetences
                          where a.CompetenceCategoryID == categoria
                          select new DTO.Competencia
                          {
                              CompetenceID = a.CompetenceID,
                              Name = a.Name,
                              Description = a.Description,
                              CompetenceCategoryID = a.CompetenceCategoryID,
                              Level = nivelRepo.Get(posicion, a.CompetenceID).Level
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }

        #endregion Public Methods
    }
}