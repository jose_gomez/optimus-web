﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class AnalisisPosicionDetalle : Contracts.API.AnalisisPosicionDetalle
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public void Delete(int id)
        {
            var result = (from a in Context.HRPositionAnalisysDetails
                          where a.DetailsID == id
                          select a).FirstOrDefault();
            if (result != null)
            {
                Context.HRPositionAnalisysDetails.DeleteOnSubmit(result);
                Context.SubmitChanges();
            }
        }

        public DTO.AnalisisPosicionDetalle Get(int id)
        {
            var result = (from a in Context.HRPositionAnalisysDetails
                          where a.DetailsID == id
                          select new DTO.AnalisisPosicionDetalle
                          {
                              ID = a.DetailsID,
                              AnalisisID = a.PositionAnalisysID,
                              Funtion = a.Funtion,
                              What = a.WhatDo,
                              How = a.HowDo,
                              Why = a.WhyDo,
                              Time = a.TimeDo,
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.AnalisisPosicionDetalle> GetByAnalisis(int analisisID)
        {
            var result = (from a in Context.HRPositionAnalisysDetails
                          where a.PositionAnalisysID == analisisID
                          select new DTO.AnalisisPosicionDetalle
                          {
                              ID = a.DetailsID,
                              AnalisisID = a.PositionAnalisysID,
                              Funtion = a.Funtion,
                              What = a.WhatDo,
                              How = a.HowDo,
                              Why = a.WhyDo,
                              Time = a.TimeDo,
                          }).OrderBy(x => x.Funtion).ToList();
            return result;
        }

        public DTO.AnalisisPosicionDetalle Save(DTO.AnalisisPosicionDetalle detalle)
        {
            var result = (from a in Context.HRPositionAnalisysDetails
                          where a.DetailsID == detalle.ID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new HRPositionAnalisysDetail();
                Context.HRPositionAnalisysDetails.InsertOnSubmit(result);
            }
            result.PositionAnalisysID = detalle.AnalisisID;
            result.Funtion = detalle.Funtion;
            result.WhatDo = detalle.What;
            result.HowDo = detalle.How;
            result.WhyDo = detalle.Why;
            result.TimeDo = detalle.Time;
            Context.SubmitChanges();
            return new DTO.AnalisisPosicionDetalle
            {
                ID = result.DetailsID,
                AnalisisID = result.PositionAnalisysID,
                Funtion = result.Funtion,
                What = result.WhatDo,
                How = result.HowDo,
                Why = result.WhyDo,
                Time = result.TimeDo
            };
        }

        #endregion Public Methods

    }
}