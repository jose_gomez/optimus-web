﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class IndicadorDesempenio : Contracts.API.IndicadorDesempenio
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.IndicadorDesempenio> Get()
        {
            var result = (from a in Context.HRIndicators
                          select new DTO.IndicadorDesempenio
                          {
                              IndicatorID = a.IndicatorID,
                              Name = a.Name,
                              Description = a.Description
                          }).OrderBy(x => x.Name).ToList();
            return result;
        }

        public DTO.IndicadorDesempenio Get(int id)
        {
            var result = (from a in Context.HRIndicators
                          where a.IndicatorID == id
                          select new DTO.IndicadorDesempenio
                          {
                              IndicatorID = a.IndicatorID,
                              Name = a.Name,
                              Description = a.Description
                          }).FirstOrDefault();
            return result;
        }

        #endregion Public Methods

    }
}