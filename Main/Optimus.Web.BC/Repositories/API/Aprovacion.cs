﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Aprovacion : Contracts.API.Aprovacion
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.Aprovacion> Get(int tipoRequisionID, int requisicionID)
        {
            
            var result = (from a in Context.HRRequisitionsApprobations
                          where a.RequisitionID == requisicionID && a.TypeRequisitionID == tipoRequisionID
                          select new DTO.Aprovacion
                          {
                              RequestTypeID = a.TypeRequisitionID,
                              RequestID = a.RequisitionID,
                              StatusID = a.StatusID,
                              CreateDate = a.Create_dt,
                              UserID = a.UserID
                          }).ToList();
            return result;
        }

        public List<DTO.Aprovacion> GetByTipoRequisicion(int tipoRequisionID)
        {
            var result = (from a in Context.HRRequistionWorkFlows
                          where a.TypeRequisitionID == tipoRequisionID
                          select new DTO.Aprovacion
                          {
                              PosicionID = a.PositionID,
                              Secuencia = a.Sequence
                          }).OrderBy(x => x.Secuencia).ToList();
            return result;
        }

        public List<DTO.Aprovacion> GetWorkFlow(int tipoRequisionID)
        {
            var result = (from a in Context.HRRequistionWorkFlows
                          where a.TypeRequisitionID == tipoRequisionID
                          select new DTO.Aprovacion
                          {
                              PosicionID = a.PositionID,
                              Secuencia = a.Sequence
                          }).OrderBy(x => x.Secuencia).ToList();
            return result;
        }

        #endregion Public Methods

    }
}