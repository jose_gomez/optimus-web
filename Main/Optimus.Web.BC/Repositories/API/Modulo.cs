﻿using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Modulo : Contracts.API.Modulo
    {
        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(ConnString);

        #endregion Private Fields

        #region Public Properties

        public static string ConnString
        {
            get
            {
                string _ConnString = System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"] == null ?
    System.Configuration.ConfigurationManager.ConnectionStrings["OptimusDB"].ConnectionString :
    System.Configuration.ConfigurationManager.ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString;
                return _ConnString;
            }
        }

        #endregion Public Properties

        #region Public Methods

        public List<DTO.Modulo> Get()
        {
            var result = (from a in Context.SYModules
                          select new DTO.Modulo
                          {
                              ID = a.ModuleID,
                              Title = a.Title,
                              IsSubModule = (bool)a.IsSubModule,
                              ParentModuleID = a.ParentModuleID,
                              SystemID = a.SystemID
                          }).OrderBy(x => x.Title).ToList();
            return result;
        }

        public DTO.Modulo Get(int id)
        {
            var result = (from a in Context.SYModules
                          where a.ModuleID == id
                          select new DTO.Modulo
                          {
                              ID = a.ModuleID,
                              Title = a.Title,
                              IsSubModule = (bool)a.IsSubModule,
                              ParentModuleID = a.ParentModuleID,
                              SystemID = a.SystemID
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.Modulo> GetParentsModules()
        {
            var result = (from a in Context.SYModules
                          where a.IsSubModule == false
                          select new DTO.Modulo
                          {
                              ID = a.ModuleID,
                              Title = a.Title,
                              IsSubModule = (bool)a.IsSubModule,
                              ParentModuleID = a.ParentModuleID,
                              SystemID = a.SystemID
                          }).OrderBy(x => x.Title).ToList();
            return result;
        }

        #endregion Public Methods
    }
}