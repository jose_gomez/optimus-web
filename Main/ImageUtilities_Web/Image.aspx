﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CS.aspx.cs" Inherits="CS"  %>

<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta name="description" content="A basic demo of Cropper.">
  <meta name="keywords" content="HTML, CSS, JS, JavaScript, image cropping, cropper, cropperjs, cropper.js, front-end, web development">
  <meta name="author" content="Fengyuan Chen">
  <title>Cropper.js</title>
  <link rel="stylesheet" href="../scripts/assets/css/font-awesome.min.css">
  <link rel="stylesheet" href="../scripts/assets/css/bootstrap.min.css">
  <link rel="stylesheet" href="../scripts/dist/cropper.css">
  <link rel="stylesheet" href="/scripts/css/main.css">
</head>
<body>
<form id="form1" runat="server">
  <!--[if lt IE 9]>
  <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
  <![endif]-->

  <!-- Header -->
  <header class="navbar navbar-static-top docs-header" id="top">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-target="#navbar-collapse" data-toggle="collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
      </div>

    </div>
  </header>

  <!-- Jumbotron -->
  <div class="jumbotron docs-jumbotron">
    <div class="container">
      <h1>Cropper Image</h1>
    </div>
  </div>

  <!-- Content -->
  <div class="container">
    <div class="row">
      <div class="col-md-9">
        <!-- <h3 class="page-header">Demo:</h3> -->
        <div class="img-container">
		<script type="text/javascript" language="javascript">

		    function GetHref() {
		            $('#theHidden').val($('#download').attr('href'));     
		            return true;               
		    }
			$(window).load(function(){

				$(function() 
				{
					$('#file-input').change(function(e) 
					{
						addImage(e); 
					});

					function addImage(e)
					{
						var file = e.target.files[0],
						imageType = /image.*/;
    
						if (!file.type.match(imageType))
							return;
  
						var reader = new FileReader();
						reader.onload = fileOnload;
						reader.readAsDataURL(file);
					}	
  
				function fileOnload(e) 
				{
					var result=e.target.result;
					$('#imgSalida').attr("src",result);
				}
				});
		  });
		</script>
          <img src= result alt="Picture">
        </div>
      </div>
    </div>
    <div class="row" id="actions">
      <div class="col-md-9 docs-buttons">
        <!-- <h3 class="page-header">Toolbar:</h3> -->
        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="Rotate Left">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(-45)">
              <span class="fa fa-rotate-left"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="Rotate Right">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.rotate(45)">
              <span class="fa fa-rotate-right"></span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-flip="horizontal" data-method="scaleX" data-option="-1" title="Flip Horizontal">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scaleX(-1)">
              <span class="fa fa-arrows-h"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-flip="vertical" data-method="scaleY" data-option="-1" title="Flip Vertical">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.scaleY(-1)">
              <span class="fa fa-arrows-v"></span>
            </span>
          </button>
        </div>

        <div class="btn-group">
          <button type="button" class="btn btn-primary" data-method="crop" title="Crop">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.crop()">
              <span class="fa fa-check"></span>
            </span>
          </button>
          <button type="button" class="btn btn-primary" data-method="clear" title="Clear">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.clear()">
              <span class="fa fa-remove"></span>
            </span>
          </button>
        </div>
        <div class="btn-group">
          <label class="btn btn-primary btn-upload" for="inputImage" title="Upload image file">
            <input type="file" class="sr-only" id="inputImage" name="file" accept="image/*">
            <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
              <span class="fa fa-upload"></span>
            </span>
          </label>
          <button type="button" class="btn btn-primary" data-method="destroy" title="Destroy">
            <span class="docs-tooltip" data-toggle="tooltip" title="cropper.destroy()">
              <span class="fa fa-refresh"></span>
            </span>
          </button>
        </div>


		    <div class="btn-group btn-group-crop">
				<button id="salvar" type="button" class="btn btn-primary" data-method="getCroppedCanvas">
					<span class="docs-tooltip" data-toggle="tooltip" title="cropper.getCroppedCanvas()">
					Save
					</span>
				</button>
			</div>
        <!-- Show the cropped image in modal -->
        <div class="modal fade docs-cropped" id="getCroppedCanvasModal" role="dialog" aria-hidden="true" aria-labelledby="getCroppedCanvasTitle" tabindex="-1">
          <div class="modal-dialog">
            <div class="modal-content">
             
			 <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="getCroppedCanvasTitle">Cropped</h4>
             </div>             
			  <div class="modal-body"  id="canvas"></div>			  
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                  <script type="text/javascript">
                      $(function ()
                      {
                          $('#salvar').change(function () {
                              $('#Image1').hide();
                              var reader = new FileReader();
                              reader.onload = function (e) {
                                  $('#Image1').show();
                                  $('#Image1').attr("src", e.target.result);
                                  $('#Image1').Jcrop({
                                      onChange: SetCoordinates,
                                      onSelect: SetCoordinates
                                  });

                                  $('#download').click(function () {
                                      var x1 = $('#imgX1').val();
                                      var y1 = $('#imgY1').val();
                                      var width = $('#imgWidth').val();
                                      var height = $('#imgHeight').val();
                                      var canvas = $("#canvas")[0];
                                      var context = canvas.getContext('2d');
                                      var img = new Image();
                                      img.onload = function () {
                                          canvas.height = height;
                                          canvas.width = width;
                                          context.drawImage(img, x1, y1, width, height, 0, 0, width, height);
                                          $('#imgCropped').val(canvas.toDataURL());
                                          $('[id*=download]').show();
                                      };

                                      img.src = $('#Image1').attr("src");

                                  });
                              }
                              reader.readAsDataURL($(this)[0].files[0]);
                          });
                      });
                      function SetCoordinates(c) {
                          $('#imgX1').val(c.x);
                          $('#imgY1').val(c.y);
                          $('#imgWidth').val(c.w);
                          $('#imgHeight').val(c.h);
                          $('#btnCrop').show();
                      };
                  </script>
                <asp:HiddenField ID="theHidden" runat="server" Value="" />
                <a class="btn btn-primary" id="download" href="javascript:void(0);" download="cropped.jpg">Download</a>
                <asp:button type="button" class="btn btn-success" OnClientClick="return GetHref()" id="btnUpload" runat="server" text="Upload" onclick="Upload" />			            
              </div>
			   
		   </div>
          </div>
        </div><!-- /.modal --> 
<div id="status"></div>
		
		<input type="text" class="form-control" id="putData" placeholder="Get data to here or set data with this value">
	
      </div><!-- /.docs-buttons -->

      <div class="col-md-3 docs-toggles">
        <!-- <h3 class="page-header">Toggles:</h3> -->
        <div class="dropdown dropup docs-options">
          <ul class="dropdown-menu" role="menu" aria-labelledby="toggleOptions">
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="responsive" checked>
                responsive
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="restore" checked>
                restore
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="checkCrossOrigin" checked>
                checkCrossOrigin
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="checkOrientation" checked>
                checkOrientation
              </label>
            </li>

            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="modal" checked>
                modal
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="guides" checked>
                guides
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="center" checked>
                center
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="highlight" checked>
                highlight
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="background" checked>
                background
              </label>
            </li>

            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="autoCrop" checked>
                autoCrop
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="movable" checked>
                movable
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="rotatable" checked>
                rotatable
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="scalable" checked>
                scalable
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="zoomable" checked>
                zoomable
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="zoomOnTouch" checked>
                zoomOnTouch
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="zoomOnWheel" checked>
                zoomOnWheel
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="cropBoxMovable" checked>
                cropBoxMovable
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="cropBoxResizable" checked>
                cropBoxResizable
              </label>
            </li>
            <li role="presentation">
              <label class="checkbox-inline">
                <input type="checkbox" name="toggleDragModeOnDblclick" checked>
                toggleDragModeOnDblclick
              </label>
            </li>
          </ul>
        </div><!-- /.dropdown -->
      </div><!-- /.docs-toggles -->
    </div>
  </div>
  <!-- Footer -->
  <footer class="docs-footer">
    <div class="container">
      <p class="heart"></p>
    </div>
  </footer>
    <input type="hidden" name="imgX1" id="imgX1" />
    <input type="hidden" name="imgY1" id="imgY1" />
    <input type="hidden" name="imgWidth" id="imgWidth" />
    <input type="hidden" name="imgHeight" id="imgHeight" />
  <!-- Scripts -->
  <script src="../scripts/assets/js/jquery.min.js"></script>
  <script src="../scripts/assets/js/bootstrap.min.js"></script>
  <script src="../scripts/dist/cropper.js"></script>
  <script src="/scripts/js/main.js"></script>
</body>
</form>
</html>
