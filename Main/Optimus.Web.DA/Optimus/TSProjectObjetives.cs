//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Optimus.Web.DA.Optimus
{
    using System;
    using System.Collections.Generic;
    
    public partial class TSProjectObjetives
    {
        public TSProjectObjetives()
        {
            this.TSProjectObjetives1 = new HashSet<TSProjectObjetives>();
        }
    
        public int ProjectObjetivesID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public Nullable<System.DateTime> EstimatedDate { get; set; }
        public Nullable<int> PorcentProgress { get; set; }
        public Nullable<int> FatherProject { get; set; }
        public Nullable<int> Year { get; set; }
        public Nullable<int> StatusID { get; set; }
        public Nullable<int> DepartmentID { get; set; }
        public Nullable<int> ProjectObjetivesTypesID { get; set; }
        public Nullable<bool> Project { get; set; }
        public Nullable<System.DateTime> Create_dt { get; set; }
        public Nullable<System.DateTime> Modify_dt { get; set; }
        public Nullable<int> UserID { get; set; }
    
        public virtual SYUsers SYUsers { get; set; }
        public virtual ICollection<TSProjectObjetives> TSProjectObjetives1 { get; set; }
        public virtual TSProjectObjetives TSProjectObjetives2 { get; set; }
        public virtual TSProjectObjetivesTypes TSProjectObjetivesTypes { get; set; }
    }
}
