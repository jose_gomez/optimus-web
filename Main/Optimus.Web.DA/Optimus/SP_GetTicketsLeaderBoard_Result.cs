//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Optimus.Web.DA.Optimus
{
    using System;
    
    public partial class SP_GetTicketsLeaderBoard_Result
    {
        public Nullable<int> Tickets { get; set; }
        public Nullable<int> Responsible { get; set; }
        public string TicketState { get; set; }
        public string FullName { get; set; }
    }
}
