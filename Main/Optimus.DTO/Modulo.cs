﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    /// <summary>
    /// Modelo de un modulo
    /// </summary>
    public class Modulo
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Titulo
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Nombre de constante
        /// </summary>
        public string ConstantName { get; set; }
        /// <summary>
        /// Es un sub-modulo
        /// </summary>
        public bool IsSubModule { get; set; }
        /// <summary>
        /// Identificador modulo padre
        /// </summary>
        public int? ParentModuleID { get; set; }
        /// <summary>
        /// Identificador del sistema
        /// </summary>
        public int? SystemID { get; set; }
    }
}
