﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    /// <summary>
    /// Modelo del tipo de reporte
    /// </summary>
    public class TipoReporte
    {
        /// <summary>
        /// Identificador
        /// </summary>
        public int ID { get; set; }
        /// <summary>
        /// Nombre
        /// </summary>
        public String Name { get; set; }
    }
}
