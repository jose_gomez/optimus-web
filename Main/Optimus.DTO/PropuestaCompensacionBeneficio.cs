﻿using System;

namespace Optimus.DTO
{
    public class PropuestaCompensacionBeneficio
    {
        #region Public Properties

        public int CandidateID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime ModifyDate { get; set; }
        public Posicion Position { get; set; }
        public int PositionID { get; set; }
        public Double Proposal { get; set; }
        public int StatusID { get; set; }
        public int UserID { get; set; }
        public int WageProposalID { get; set; }

        #endregion Public Properties
    }
}