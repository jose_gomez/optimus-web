﻿using System;

namespace Optimus.DTO
{
    public class Curso
    {
        #region Public Properties

        public bool Active { get; set; }
        public int? AreaID { get; set; }
        public string CourdeCode { get; set; }
        public int CourseID { get; set; }
        public DateTime Create_dt { get; set; }
        public int? DepartmentID { get; set; }
        public string Description { get; set; }
        public DateTime Modify_dt { get; set; }
        public int? OperationID { get; set; }
        public string Title { get; set; }
        public string TypeCourseID { get; set; }
        public int UserID { get; set; }

        #endregion Public Properties
    }
}