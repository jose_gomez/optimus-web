﻿using System;

namespace Optimus.DTO
{
    /// <summary>
    /// Acción a realizar aun empleado.
    /// </summary>
    public class AccionEmpleado
    {

        #region Public Properties

        /// <summary>
        /// Identificador del tipo de acción.
        /// </summary>
        public int? ActionTypeID { get; set; }

        /// <summary>
        /// Fecha en la que se creo la acción.
        /// </summary>
        public DateTime? Create_dt { get; set; }

        /// <summary>
        /// Usuario que creo la acción.
        /// </summary>
        public int? CreateUserID { get; set; }

        /// <summary>
        /// Numero de empleado.
        /// </summary>
        public int? EmployeeID { get; set; }

        /// <summary>
        /// Fecha en la que se debe ejecutar la acción.
        /// </summary>
        public DateTime? Execute_dt { get; set; }

        /// <summary>
        /// Valor final luego de ejecutarse la acción.
        /// </summary>
        public string FinalValue { get; set; }

        /// <summary>
        /// Identificador.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Valor inicial antes de ejecutarse la acción.
        /// </summary>
        public string InitialValue { get; set; }

        /// <summary>
        /// Fecha en la que se modifico la acción.
        /// </summary>
        public DateTime? Modify_dt { get; set; }

        /// <summary>
        /// Usuario que modifico la acción.
        /// </summary>
        public int? ModifyUserID { get; set; }

        /// <summary>
        /// Estado de la acción. Pendiente = 35, Sometido = 33, Cancelado = 34
        /// </summary>
        public int? StatusID { get; set; }

        #endregion Public Properties

    }
}