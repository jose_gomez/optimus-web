﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class InformacionBasicaEmpleado
    {
        public int EmployeeID { get; set; }
        public int? PositionID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PersonID { get; set; }
        public string PositionName { get; set; }
        public int? DepartmentID { get; set; }
        public string DepartmentName { get; set; }
    }
}
