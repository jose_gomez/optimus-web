﻿using System;

namespace Optimus.DTO
{
    public class AnalisisPosicion
    {

        #region Public Properties

        public DateTime CreateDate { get; set; }
        public int ID { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int? PositionID { get; set; }
        public string PositionName { get; set; }
        public int? RequisitionID { get; set; }
        public int? StatusID { get; set; }
        public int UserID { get; set; }

        #endregion Public Properties
    }
}