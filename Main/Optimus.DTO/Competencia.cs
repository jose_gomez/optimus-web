﻿namespace Optimus.DTO
{
    public class Competencia
    {
        #region Public Properties

        public int CompetenceCategoryID { get; set; }
        public int CompetenceID { get; set; }
        public string Description { get; set; }
        public double Level { get; set; }
        public string Name { get; set; }

        #endregion Public Properties
    }
}