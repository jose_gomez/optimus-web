﻿namespace Optimus.DTO
{
    public class AnalisisPosicionDetalle
    {
        #region Public Properties

        public int AnalisisID { get; set; }
        public string Funtion { get; set; }
        public string How { get; set; }
        public int ID { get; set; }
        public string Time { get; set; }
        public string What { get; set; }
        public string Why { get; set; }

        #endregion Public Properties
    }
}