﻿using System;

namespace Optimus.DTO
{
    public class Empleado
    {

        #region Public Properties
        /// <summary>
        /// Salario base.
        /// </summary>
        public double? BaseSalary { get; set; }
        public int CompanyID { get; set; }
        public DateTime? CreateDate { get; set; }
        public int EmployeeID { get; set; }
        public DateTime? ModifyDate { get; set; }
        public bool? PaidByTheHour { get; set; }
        public int PersonID { get; set; }
        public int PositionID { get; set; }
        public bool? ProductionCompensation { get; set; }
        public int StatusID { get; set; }
        public int UserID { get; set; }
        public DateTime? EntryDate { get; set; }
        public string PaymentFrequency { get; set; }
        public int? IdJornada { get; set; }
        public bool PaidByHour { get; set; }
        public bool? TransactionPayment { get; set; }
        public int? FinancialEntityID { get; set; }
        public string  Account { get; set; }
        public bool? ProofPayment { get; set; }
        public bool? PayByPayroll { get; set; }
        public string SDSS { get; set; }
        public string ARS { get; set; }
        public int? ContractTypeID { get; set; }
        public int? EmployeeTypeID { get; set; }
        public double? SeguroDental { get; set; }
        public double? IncentivesPayment { get; set; }
        public byte[] Picture { get; set; }
        public string Status { get; set; }

        #endregion Public Properties
    }
}