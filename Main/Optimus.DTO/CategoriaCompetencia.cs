﻿namespace Optimus.DTO
{
    public class CategoriaCompetencia
    {
        #region Public Properties

        public int CompetenceCategoryID { get; set; }
        public string Name { get; set; }

        #endregion Public Properties
    }
}