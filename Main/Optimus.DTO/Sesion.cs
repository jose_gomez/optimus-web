﻿using System;

namespace Optimus.DTO
{
    public class Sesion
    {
        #region Public Properties

        public int? CourseID { get; set; }
        public DateTime? Create_dt { get; set; }
        public DateTime? EndDate { get; set; }
        public DateTime? Modify_dt { get; set; }
        public int? Number { get; set; }
        public string SessionCode { get; set; }
        public int SessionID { get; set; }
        public DateTime? StartDate { get; set; }
        public int? StatusID { get; set; }
        public int? UserID { get; set; }
        public int? Year { get; set; }

        #endregion Public Properties
    }
}