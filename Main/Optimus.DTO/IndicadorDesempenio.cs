﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class IndicadorDesempenio
    {
        public int IndicatorID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Percentage { get; set; }
    }
}
