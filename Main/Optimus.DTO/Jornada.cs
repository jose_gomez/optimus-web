﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Optimus.DTO
{
    public class Jornada
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public DateTime? Create_dt { get; set; }
        public DateTime? Modify_dt { get; set; }
        public int? UserID { get; set; }
    }
}
