﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class NivelCompetenciaDeseado
    {
        public int CompetenceID { get; set; }
        public int PositionID { get; set; }
        public double Level { get; set; }
    }
}
