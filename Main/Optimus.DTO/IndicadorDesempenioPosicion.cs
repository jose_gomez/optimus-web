﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class IndicadorDesempenioPosicion: DTO.IndicadorDesempenio
    {

        public int PositionID { get; set; }
        public double Percentage { get; set; }
        
    }
}
