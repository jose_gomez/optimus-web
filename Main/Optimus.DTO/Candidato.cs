﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
   public class Candidate
    {
        int CandidateID  {get;set;}
        public int PostulatePositionID { get; set;}
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName

        {
            get
            {
                return this.FirstName + ", " + this.LastName;

            }
        }
        public string Streets { get; set; }
        public string HouseNumber { get; set; }
        public string Sector { get; set; }
        public string location { get; set; }
        public string Sex { get; set; }
        public int NationalityID { get; set; }
        public string PhoneNumber { get; set; }
        public string MovilNumber { get; set; }
        public string Email { get; set; }
        public int Age { get; set; }
        public string Cedula { get; set; }
        public string alias { get; set; }
        public DateTime BirthDate { get; set; }
        public string EmergencyNumber { get; set; }
        public string EmergencyContactName { get; set; }
        public string Size { get; set; }
        public string Religion { get; set; }
        public int CivilStatus { get; set; }
        public bool Allergies { get; set; }       
        public string AllergiesType { get; set; }
        public bool suffering { get; set; }
        public string sufferingType { get; set; }
        public bool AvailableOverTime { get; set; }
        public bool AvailableWeekend { get; set; }
        public string LivesWith { get; set; }
        public string SpouseName { get; set; }
        public string SpouseActivity { get; set; }
        public string MotherName { get; set; }
        public string FatherName { get; set; }
        public int DependentChildren { get; set; }
        public int DependentPeople { get; set; }
        public int StudiesID { get; set; }
        public bool ComputerStudies { get; set; }
        public bool IsStudent { get; set; }
        public string DaysWeek { get; set; }
        public string Schedule { get; set; }
        public string ProgramsKnow { get; set; }
        public string WhosRecommended { get; set; }
        public bool FamilyOrFriend { get; set; }
        public string FamilyOrFriendName { get; set; }
        public string FamilyOrFriendPuesto { get; set; }
        public string FamilyOrFriendRelationship { get; set; }
        public DateTime create_dt { get; set; }
        public int PositionCategoryID { get; set; }
        public byte[] Picture64Bit { get; set; }
        //public string Picture64String
        //{
        //    get
        //    {
        //        return "data:image/jpg;base64,"+ImageToBase64(byteArrayToImage(this.Picture64Bit), ImageFormat.Jpeg);
        //    }
        //}
        public bool Interviewed { get; set; } 
        public int CandidateStateID { get; set; }
        public string PosicionName { get; set; }



        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            if (byteArrayIn.Length == 0 )
                return null;
            System.Drawing.ImageConverter converter = new System.Drawing.ImageConverter();
            Image img = byteArrayIn != null ? (Image)converter.ConvertFrom(byteArrayIn) : null;

            return img;
        }
        public string ImageToBase64(System.Drawing.Image image, ImageFormat format)
        {
            if (image  != null)
            {
                using (MemoryStream ms = new MemoryStream())
                {
                    // Convert Image to byte[]
                    image.Save(ms, format);
                    byte[] imageBytes = ms.ToArray();

                    // Convert byte[] to Base64 String
                    string base64String = Convert.ToBase64String(imageBytes);
                    return base64String;
                }
            }
            return string.Empty;
       
        }
        public static bool IsValidImage(byte[] bytes)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(bytes))
                    Image.FromStream(ms);
            }
            catch (ArgumentException)
            {
                return false;
            }
            return true;
        }

    }
}
