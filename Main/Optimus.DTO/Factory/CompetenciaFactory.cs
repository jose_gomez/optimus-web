﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class CompetenciaFactory
    {
        #region Public Methods

        public static List<Competencia> DeserializeList(string data)
        {
            List<Competencia> positions = null;
            positions = JsonConvert.DeserializeObject<List<Competencia>>(data);
            return positions;
        }

        public static Competencia Deserialize(string data)
        {
            Competencia position = null;
            position = JsonConvert.DeserializeObject<Competencia>(data);
            return position;
        }

        public static string Serialize(Competencia position)
        {
            string data = JsonConvert.SerializeObject(position);
            return data;
        }

        #endregion Public Methods
    }
}
