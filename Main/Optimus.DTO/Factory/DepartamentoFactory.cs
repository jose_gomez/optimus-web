﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class DepartamentoFactory
    {
        #region Public Methods

        public static List<Departamento> DeserializeList(string data)
        {
            List<Departamento> positions = null;
            positions = JsonConvert.DeserializeObject<List<Departamento>>(data);
            return positions;
        }

        public static Departamento Deserialize(string data)
        {
            Departamento position = null;
            position = JsonConvert.DeserializeObject<Departamento>(data);
            return position;
        }

        public static string Serialize(Departamento position)
        {
            string data = JsonConvert.SerializeObject(position);
            return data;
        }

        #endregion Public Methods
    }
}
