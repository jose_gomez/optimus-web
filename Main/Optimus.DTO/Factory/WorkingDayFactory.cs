﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class WorkingDayFactory
    {

        #region Public Methods

        public static Jornada Deserialize(string data)
        {
            Jornada workingDay = null;
            workingDay = JsonConvert.DeserializeObject<Jornada>(data);
            return workingDay;
        }

        public static List<Jornada> DeserializeList(string data)
        {
            List<Jornada> workingDay = null;
            workingDay = JsonConvert.DeserializeObject<List<Jornada>>(data);
            return workingDay;
        }

        public static string Serialize(Jornada workingDay)
        {
            string data = JsonConvert.SerializeObject(workingDay);
            return data;
        }

        #endregion Public Methods

    }
}