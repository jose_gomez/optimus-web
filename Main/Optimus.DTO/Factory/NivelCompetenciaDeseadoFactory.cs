﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class NivelCompetenciaDeseadoFactory
    {
        #region Public Methods

        public static NivelCompetenciaDeseado Deserialize(string data)
        {
            NivelCompetenciaDeseado nivel = null;
            nivel = JsonConvert.DeserializeObject<NivelCompetenciaDeseado>(data);
            return nivel;
        }

        public static List<NivelCompetenciaDeseado> DeserializeList(string data)
        {
            List<NivelCompetenciaDeseado> niveles = null;
            niveles = JsonConvert.DeserializeObject<List<NivelCompetenciaDeseado>>(data);
            return niveles;
        }

        public static string Serialize(NivelCompetenciaDeseado nivel)
        {
            string data = JsonConvert.SerializeObject(nivel);
            return data;
        }

        #endregion Public Methods
    }
}
