﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class CategoriaCompetenciaFactory
    {
        #region Public Methods

        public static List<CategoriaCompetencia> DeserializeList(string data)
        {
            List<CategoriaCompetencia> categorias = null;
            categorias = JsonConvert.DeserializeObject<List<CategoriaCompetencia>>(data);
            return categorias;
        }

        public static CategoriaCompetencia Deserialize(string data)
        {
            CategoriaCompetencia categoria = null;
            categoria = JsonConvert.DeserializeObject<CategoriaCompetencia>(data);
            return categoria;
        }

        public static string Serialize(CategoriaCompetencia categoria)
        {
            string data = JsonConvert.SerializeObject(categoria);
            return data;
        }

        #endregion Public Methods
    }
}
