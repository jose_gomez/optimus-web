﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class WageProposalFactory
    {

        #region Public Methods

        public static PropuestaCompensacionBeneficio Deserialize(string data)
        {
            PropuestaCompensacionBeneficio wageProposal = null;
            wageProposal = JsonConvert.DeserializeObject<PropuestaCompensacionBeneficio>(data);
            return wageProposal;
        }

        public static List<PropuestaCompensacionBeneficio> DeserializeList(string data)
        {
            List<PropuestaCompensacionBeneficio> wageProposals = null;
            wageProposals = JsonConvert.DeserializeObject<List<PropuestaCompensacionBeneficio>>(data);
            return wageProposals;
        }
        public static string Serialize(PropuestaCompensacionBeneficio wageProposal)
        {
            string data = JsonConvert.SerializeObject(wageProposal);
            return data;
        }

        #endregion Public Methods

    }
}