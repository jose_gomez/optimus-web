﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class ModuloFactory
    {
        #region Public Methods

        public static Modulo Deserialize(string data)
        {
            Modulo result = null;
            result = JsonConvert.DeserializeObject<Modulo>(data);
            return result;
        }

        public static List<Modulo> DeserializeList(string data)
        {
            List<Modulo> result = null;
            result = JsonConvert.DeserializeObject<List<Modulo>>(data);
            return result;
        }
        public static string Serialize(Modulo informacionBasica)
        {
            string data = JsonConvert.SerializeObject(informacionBasica);
            return data;
        }

        #endregion Public Methods
    }
}
