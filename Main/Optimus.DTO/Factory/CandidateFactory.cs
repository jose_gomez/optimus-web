﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class CandidateFactory
    {

        #region Public Methods

        public static Candidate Deserialize(string data)
        {
            Candidate candidate = null;
            candidate = JsonConvert.DeserializeObject<Candidate>(data);
            return candidate;
        }

        public static List<Candidate> DeserializeList(string data)
        {
            List<Candidate> candidates = null;
            candidates = JsonConvert.DeserializeObject<List<Candidate>>(data);
            return candidates;
        }

        public static string Serialize(Candidate candidate)
        {
            string data = JsonConvert.SerializeObject(candidate);
            return data;
        }

        #endregion Public Methods

    }
}