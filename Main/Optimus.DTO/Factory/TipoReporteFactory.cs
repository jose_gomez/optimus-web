﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class TipoReporteFactory
    {

        #region Public Methods

        public static TipoReporte Deserialize(string data)
        {
            TipoReporte tipoReporte = null;
            tipoReporte = JsonConvert.DeserializeObject<TipoReporte>(data);
            return tipoReporte;
        }

        public static List<TipoReporte> DeserializeList(string data)
        {
            List<TipoReporte> tiposReportes = null;
            tiposReportes = JsonConvert.DeserializeObject<List<TipoReporte>>(data);
            return tiposReportes;
        }

        public static string Serialize(TipoReporte tipoReporte)
        {
            string data = JsonConvert.SerializeObject(tipoReporte);
            return data;
        }

        #endregion Public Methods

    }
}