﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones para manejar las posiciones que se encuentra en la empresa.
    /// </summary>
    [RoutePrefix("Optimus/Posiciones")]
    public class PosicionesController : ApiController
    {
        #region Private Fields

        private Web.BC.Repositories.API.Position _PositionRepo = new Web.BC.Repositories.API.Position();

        #endregion Private Fields

        #region Public Methods

        [HttpDelete]
        [Route("Delete/{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var result = _PositionRepo.GetPositionByID(id);
                if (result == null)
                {
                    return NotFound();
                }
                _PositionRepo.Delete(id);
                return Ok();
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtener todas las posiciones.
        /// </summary>
        /// <returns>Listado de posiciones.</returns>
        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            var positions = _PositionRepo.GetAllPositions();
            if (positions == null || positions.Count == 0)
            {
                return NotFound();
            }
            return Ok(positions);
        }

        [HttpGet]
        [ResponseType(typeof(DTO.NivelCompetenciaDeseado))]
        [Route("Get/{idPosicion}/Competencia/{idCompetencia}/Nivel")]
        public IHttpActionResult Get(int idPosicion, int idCompetencia)
        {
            Web.BC.Repositories.API.NivelCompetenciaDeseado _Repository = new Web.BC.Repositories.API.NivelCompetenciaDeseado();
            var result = _Repository.Get(idPosicion, idCompetencia);
            return Ok(result);
        }

        /// <summary>
        /// Obtener una posición especifica.
        /// </summary>
        /// <param name="id">Identificador de una posición.</param>
        /// <returns>Posición.</returns>
        [HttpGet]
        [ResponseType(typeof(DTO.Posicion))]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var position = _PositionRepo.GetPositionByID(id);
                return Ok(position);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtener las posiciones activas.
        /// </summary>
        /// <returns>Listado de posiciones.</returns>
        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get/Estado/Activo")]
        public IHttpActionResult GetActive()
        {
            try
            {
                var positions = _PositionRepo.GetPositionsByStatus((int)Core.Enumeraciones.PositionStatus.ACTIVO);
                return Ok(positions);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtener las posiciones por su estado.
        /// </summary>
        /// <param name="id">Identificador del estado.</param>
        /// <returns>Listado de posiciones.</returns>
        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get/Estado/{id}")]
        public IHttpActionResult GetByStatus(int id)
        {
            try
            {
                var positions = _PositionRepo.GetPositionsByStatus(id);
                return Ok(positions);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [ResponseType(typeof(DTO.Departamento))]
        [Route("Get/{id}/Departamento")]
        public IHttpActionResult GetDepartmentOfPosition(int id)
        {
            try
            {
                var departamento = _PositionRepo.GetDepartmentOfPosition(id);
                return Ok(departamento);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtener las posiciones inactivas.
        /// </summary>
        /// <returns>Listado de posiciones.</returns>
        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get/Estado/Inactivo")]
        public IHttpActionResult GetInactive()
        {
            try
            {
                var positions = _PositionRepo.GetPositionsByStatus((int)Core.Enumeraciones.PositionStatus.INACTIVO);
                return Ok(positions);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Crear una nueva posición.
        /// </summary>
        /// <param name="position">Nueva posición.</param>
        /// <returns>Posición creada.</returns>
        [HttpPost]
        [ResponseType(typeof(DTO.Posicion))]
        [Route("Post")]
        public IHttpActionResult Post([FromBody]DTO.Posicion position)
        {
            try
            {
                if (position == null)
                {
                    return BadRequest();
                }
                _PositionRepo.Save(position);
                return Created("", position);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Actualizar una posición.
        /// </summary>
        /// <param name="id">Identificador de la posición.</param>
        /// <param name="posicion">Posición a actualizar.</param>
        /// <returns>Posición actualizada.</returns>
        [HttpPut]
        [ResponseType(typeof(DTO.Posicion))]
        [Route("Put/{id}")]
        public IHttpActionResult Put(int id, [FromBody]DTO.Posicion posicion)
        {
            try
            {
                if (posicion == null)
                {
                    return BadRequest();
                }
                var result = _PositionRepo.GetPositionByID(id);
                if (result == null)
                {
                    return NotFound();
                }
                _PositionRepo.Save(posicion);
                return Ok(posicion);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods
    }
}