﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    [RoutePrefix("Optimus/CategoriasCompetencias")]
    public class CategoriasCompetenciasController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.CategoriaCompetencia _Repo = new Web.BC.Repositories.API.CategoriaCompetencia();

        #endregion Private Fields

        #region Public Methods

        // DELETE: api/CategoriasCompetencias/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Delete(int id)
        {
        }

        // GET: api/CategoriasCompetencias
        [HttpGet]
        [ResponseType(typeof(List<DTO.CategoriaCompetencia>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _Repo.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // GET: api/CategoriasCompetencias/5
        [HttpGet]
        [ResponseType(typeof(DTO.CategoriaCompetencia))]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _Repo.Get(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<DTO.Competencia>))]
        [Route("Get/{id}/Competencias")]
        public IHttpActionResult GetCompetenciasOfCategoria(int id)
        {
            try
            {
                var result = _Repo.GetCompetenciasOfCategoria(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // POST: api/CategoriasCompetencias
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CategoriasCompetencias/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Put(int id, [FromBody]string value)
        {
        }

        #endregion Public Methods

    }
}