﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    [RoutePrefix("Optimus/Competencias")]
    public class CompetenciasController : ApiController
    {
        Web.BC.Repositories.API.Competencia _Repo = new Web.BC.Repositories.API.Competencia();
        // GET: api/Competencias
        [HttpGet]
        [ResponseType(typeof(List<DTO.Competencia>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _Repo.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // GET: api/Competencias/5
        [HttpGet]
        [ResponseType(typeof(DTO.Competencia))]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var result = _Repo.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // POST: api/Competencias
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Competencias/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Competencias/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Delete(int id)
        {
        }
    }
}
