﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones para manejar las propuestas salariales que se les 
    /// hacen a los candidatos que aplican para una posición en la empresa.
    /// </summary>
    [RoutePrefix("Optimus/PropuestasCompensacionesBeneficios")]
    public class PropuestasCompensacionesBeneficiosController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.WageProposal _WageProposalRepo = new Web.BC.Repositories.API.WageProposal();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                return NotFound();
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Get")]
        [ResponseType(typeof(List<DTO.PropuestaCompensacionBeneficio>))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Get()
        {
            try
            {
                return NotFound();
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene una propuesta salarial especifica.
        /// </summary>
        /// <param name="id">Identificador de la propuesta salarial.</param>
        /// <returns>Propuesta salarial requerida.</returns>
        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(DTO.PropuestaCompensacionBeneficio))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var wageProposal = _WageProposalRepo.GetWageProposalsByID(id);
                if (wageProposal == null)
                {
                    return NotFound();
                }
                return Ok(wageProposal);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene todas las propuestas salariales hechas a un candidato especifico.
        /// </summary>
        /// <param name="id">Identificador del candidato.</param>
        /// <returns>Propuestas salariales de un candidato.</returns>
        [HttpGet]
        [Route("Get/Candidato/{id}")]
        [ResponseType(typeof(List<DTO.PropuestaCompensacionBeneficio>))]
        public IHttpActionResult GetWageProposalsByCandidate(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var wageProposals = _WageProposalRepo.GetWageProposalsByCandidate(id);
                if (wageProposals == null)
                {
                    return NotFound();
                }
                return Ok(wageProposals);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Crea una propuesta salarial nueva.
        /// </summary>
        /// <param name="wageProposal">Nueva propuesta salarial.</param>
        /// <returns>Propuesta salarial creada.</returns>
        [HttpPost]
        [Route("Post")]
        [ResponseType(typeof(DTO.PropuestaCompensacionBeneficio))]
        public IHttpActionResult Post([FromBody]DTO.PropuestaCompensacionBeneficio wageProposal)
        {
            try
            {
                if (wageProposal == null)
                {
                    return BadRequest();
                }
                var wProposal = _WageProposalRepo.Save(wageProposal);
                if (wProposal == null)
                {
                    return NotFound();
                }
                return Ok(wProposal);
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Actualizar una propuesta salarial.
        /// </summary>
        /// <param name="id">Identificador de la propuesta salarial.</param>
        /// <param name="wageProposal">Propuesta salarial para actualizar.</param>
        /// <returns>Propuesta salarial actualizada.</returns>
        [HttpPut]
        [Route("Put/{id}")]
        [ResponseType(typeof(DTO.PropuestaCompensacionBeneficio))]
        public IHttpActionResult Put(int id, [FromBody]DTO.PropuestaCompensacionBeneficio wageProposal)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                if (wageProposal == null)
                {
                    return BadRequest();
                }
                var result = _WageProposalRepo.GetWageProposalsByID(id);
                if (result == null)
                {
                    return NotFound();
                }
                _WageProposalRepo.Save(wageProposal);
                return Ok();
            }
            catch (System.Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods

    }
}