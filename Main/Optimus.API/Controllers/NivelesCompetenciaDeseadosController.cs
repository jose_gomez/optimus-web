﻿using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    public class NivelesCompetenciaDeseadosController : ApiController
    {
        #region Public Methods

        // DELETE: api/NivelesCompetenciasDeseados/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Delete(int id)
        {
        }

        // GET: api/NivelesCompetenciasDeseados
        [ApiExplorerSettings(IgnoreApi = true)]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/NivelesCompetenciasDeseados/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/NivelesCompetenciasDeseados
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/NivelesCompetenciasDeseados/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Put(int id, [FromBody]string value)
        {
        }

        #endregion Public Methods
    }
}