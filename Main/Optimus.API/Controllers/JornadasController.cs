﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones para manejar las diferentes jornadas en las que laboran los empleados..
    /// </summary>
    [RoutePrefix("Optimus/Jornadas")]
    public class JornadasController : ApiController
    {
        #region Private Fields

        private Web.BC.Repositories.API.WorkingDay _Repository = new Web.BC.Repositories.API.WorkingDay();

        #endregion Private Fields

        #region Public Methods
        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [Route("Delete/{id}")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Delete(int id)
        {
        }

        /// <summary>
        /// Obtiene todas las jornadas.
        /// </summary>
        /// <returns>Listado de jornadas.</returns>
        [HttpGet]
        [ResponseType(typeof(List<DTO.Jornada>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _Repository.Get();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception ex)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene la jornada especificada.
        /// </summary>
        /// <param name="id">Identificador de la jornada.</param>
        /// <returns>Una jornada.</returns>
        [HttpGet]
        [ResponseType(typeof(DTO.Jornada))]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var result = _Repository.Get(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Crear una jornada.
        /// </summary>
        /// <param name="workingDay">Nueva jornada.</param>
        /// <returns>Jornada creada.</returns>
        [HttpPost]
        [ResponseType(typeof(DTO.Jornada))]
        [Route("Post")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Post([FromBody]DTO.Jornada workingDay)
        {
            try
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpPut]
        [ResponseType(typeof(DTO.Jornada))]
        [Route("Put")]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Put(int id, [FromBody]DTO.Jornada value)
        {
            try
            {
                return NotFound();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods
    }
}