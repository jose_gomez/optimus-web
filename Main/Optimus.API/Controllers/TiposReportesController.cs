﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones para manejar los tipos de reportes.
    /// </summary>
    [RoutePrefix("Optimus/TiposReportes")]
    public class TiposReportesController : ApiController
    {
        #region Private Fields

        private Web.BC.Repositories.API.TipoReporte _RepoTipoReporte = new Web.BC.Repositories.API.TipoReporte();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Método para obtener todos los tipos de reportes.
        /// </summary>
        /// <returns>Listado de todos los tipos de reporte.</returns>
        [HttpGet]
        [ResponseType(typeof(List<DTO.TipoReporte>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _RepoTipoReporte.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Metodo para obtener un tipo de reporte.
        /// </summary>
        /// <param name="id">Identificador del tipo de reporte.</param>
        /// <returns>Un tipo de reporte.</returns>
        [HttpGet]
        [ResponseType(typeof(DTO.TipoReporte))]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var result = _RepoTipoReporte.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods
    }
}