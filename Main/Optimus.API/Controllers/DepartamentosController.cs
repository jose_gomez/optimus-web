﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    [RoutePrefix("Optimus/Departamentos")]
    public class DepartamentosController : ApiController
    {

        #region Private Fields

        private Optimus.Web.BC.Repositories.API.Departamento _Repo = new Web.BC.Repositories.API.Departamento();

        #endregion Private Fields

        #region Public Methods

        // DELETE: api/Departamentos/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Delete(int id)
        {
        }

        // GET: api/Departamentos
        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _Repo.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // GET: api/Departamentos/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public string Get(int id)
        {
            return "value";
        }

        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get/{id}/Posiciones/Activas")]
        public IHttpActionResult GetPositionsActiveOfDepartment(int id)
        {
            try
            {
                var result = _Repo.GetPositionsActiveOfDepartment(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<DTO.Posicion>))]
        [Route("Get/{id}/Posiciones/Inactivas")]
        public IHttpActionResult GetPositionsInactiveOfDepartment(int id)
        {
            try
            {
                var result = _Repo.GetPositionsInactiveOfDepartment(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        // POST: api/Departamentos
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Departamentos/5
        [ApiExplorerSettings(IgnoreApi = true)]
        public void Put(int id, [FromBody]string value)
        {
        }

        #endregion Public Methods

    }
}