﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    [RoutePrefix("Optimus/Modulos")]
    public class ModulosController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.Modulo _RepoModulo = new Web.BC.Repositories.API.Modulo();

        #endregion Private Fields

        #region Public Methods

        [HttpGet]
        [ResponseType(typeof(List<DTO.Modulo>))]
        [Route("Get")]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _RepoModulo.Get();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [ResponseType(typeof(DTO.Modulo))]
        [Route("Get/{id}")]
        public IHttpActionResult Get(int id)
        {
            try
            {
                if (id == 0) return BadRequest();
                var result = _RepoModulo.Get(id);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        [HttpGet]
        [ResponseType(typeof(List<DTO.Modulo>))]
        [Route("Get/Modulos")]
        public IHttpActionResult GetModulos()
        {
            try
            {
                var result = _RepoModulo.GetParentsModules();
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods
    }
}