﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;

namespace Optimus.API.Controllers
{
    /// <summary>
    /// Conjunto de funciones para manejar las acciones que se realizan a un empleado.
    /// </summary>
    [RoutePrefix("Optimus/AccionesEmpleados")]
    public class AccionesEmpleadosController : ApiController
    {

        #region Private Fields

        private Optimus.Web.BC.Repositories.API.AccionEmpleado _Repo = new Optimus.Web.BC.Repositories.API.AccionEmpleado();

        #endregion Private Fields

        #region Public Methods

        /// <summary>
        /// Elimina una accion
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route("Delete/{id}")]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                _Repo.Delete(id);
                return Ok();
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene todas las acciones de los empleados.
        /// </summary>
        /// <returns>Lista de acciones.</returns>
        [HttpGet]
        [Route("Get")]
        [ResponseType(typeof(List<DTO.AccionEmpleado>))]
        public IHttpActionResult Get()
        {
            try
            {
                var result = _Repo.Get();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene una acción especifica.
        /// </summary>
        /// <param name="id">Identificador de la acción.</param>
        /// <returns>Acción</returns>
        [HttpGet]
        [Route("Get/{id}")]
        [ResponseType(typeof(DTO.AccionEmpleado))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                if (id == 0)
                {
                    return BadRequest();
                }
                var result = _Repo.Get(id);
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene todas las acciones de los empleados pendientes.
        /// </summary>
        /// <returns>Lista de acciones.</returns>
        [HttpGet]
        [Route("Get/Pendientes")]
        [ResponseType(typeof(List<DTO.AccionEmpleado>))]
        public IHttpActionResult GetPendientes()
        {
            try
            {
                var result = _Repo.GetPendientes();
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Obtiene todas las acciones de los empleados pendientes
        /// para un día en especifico.
        /// </summary>
        /// <param name="fecha">Fecha.</param>
        /// <returns>Lista de acciones pendientes.</returns>
        [HttpGet]
        [Route("Get/Pendientes/{fecha}")]
        [ResponseType(typeof(List<DTO.AccionEmpleado>))]
        public IHttpActionResult GetPendientes(string fecha)
        {
            try
            {
                var result = _Repo.GetPendientes(DateTime.Parse(fecha));
                if (result == null)
                {
                    return NotFound();
                }
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Crea una nueva acción.
        /// </summary>
        /// <param name="value">Nueva acción.</param>
        /// <returns>Acción creada.</returns>
        [HttpPost]
        [Route("Post")]
        [ResponseType(typeof(DTO.AccionEmpleado))]
        public IHttpActionResult Post([FromBody]DTO.AccionEmpleado value)
        {
            try
            {
                if (value == null)
                {
                    return BadRequest();
                }
                var result = _Repo.Create(value);
                if (result == null)
                {
                    return NotFound();
                }
                return Created(string.Format("Optimus/AccionesEmpleados/Get/{0}", result.ID), result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        /// <summary>
        /// Actualiza la acción de personal.
        /// </summary>
        /// <param name="id">Identificador de la acción.</param>
        /// <param name="value">Valores para actualizar.</param>
        /// /// <returns>Acción actualizada.</returns>
        [HttpPut]
        [Route("Put/{id}")]
        [ResponseType(typeof(DTO.AccionEmpleado))]
        public IHttpActionResult Put(int id, [FromBody]DTO.AccionEmpleado value)
        {
            try
            {
                if (id == 0 || value == null)
                {
                    return BadRequest();
                }
                var result = _Repo.Update(value);
                return Ok(result);
            }
            catch (Exception)
            {
                return InternalServerError();
            }
        }

        #endregion Public Methods

    }
}