﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class _Default : Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            AsistenciaLbl.Text = DateTime.Now.ToString("d/MM/yyyy", CultureInfo.CreateSpecificCulture("es-MX"));
            HeaderMesLabel.Text ="Cumpleaños en el mes de "+ DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("es-MX"));
            EmployeeAttendance attendance = new EmployeeAttendance();
            attendance.CalculateAttendance(DateTime.Now);
            KnobAsistencia.Value = attendance.AttendancePercentage.ToString();
            if (!IsPostBack)
            {
                EmployeeBirthDateRepeater.DataSource = worker.Employees.GetEmployeeBirhtDayToday();
                EmployeeBirthDateRepeater.DataBind();
                LLenarDatos();
            }
        }
        private void LLenarDatos()
        {
            BirthDaysgridView.DataSource = worker.Employees.GetEmployeeBirhtDayMonth();
            BirthDaysgridView.DataBind();
        }

        protected void BirthDaysgridView_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            BirthDaysgridView.PageIndex = e.NewPageIndex;
            LLenarDatos();

        }
    }
}