﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class AuditFoto : System.Web.UI.Page
    {
        ServiceContainer container;
        UnitOfWork worker;

        public AuditFoto()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var controlService = container.GetAuditoryControlService().GetAuditControlByID(Convert.ToInt32(Request.QueryString["controlID"]));
                int maxImg = Convert.ToInt32(controlService.FirstOrDefault().Images_number);

                lbQtyFoto.Text = maxImg.ToString();
                cargarImg();
            }
        }

        public void cargarImg()
        {
            var imgService = container.GetAuditoryService();
            var consImg = imgService.GetImageByAuditID(Convert.ToInt32(Request.QueryString["ID"])).ToList();

            dgvImages.AutoGenerateColumns = false;
            dgvImages.DataSource = consImg;
            dgvImages.DataBind();
        }
        protected void btnSaveImg_Click(object sender, EventArgs e)
        {
            try
            {
                var controlService = container.GetAuditoryControlService().GetAuditControlByID(Convert.ToInt32(Request.QueryString["controlID"]));
                int maxImg = Convert.ToInt32(controlService.FirstOrDefault().Images_number);

                var imgService = container.GetAuditoryService();
                var consQtyImg = imgService.GetImageByAuditID(Convert.ToInt32(Request.QueryString["ID"]));
                int QtyRegister = consQtyImg.Count;

                if(QtyRegister != maxImg)
                {
                    if ((flAuditImg.PostedFile != null) && (flAuditImg.PostedFile.ContentLength > 0))
                    {
                        HttpPostedFile ImgFile = flAuditImg.PostedFile;
                        
                        byte[] byteImage = new byte[flAuditImg.PostedFile.ContentLength];

                        ImgFile.InputStream.Read(byteImage, 0, flAuditImg.PostedFile.ContentLength);
                        //
                        User usuario = worker.User.GetUserByUserName(User.Identity.Name);
                        SHAuditImagen imgModel = new SHAuditImagen();
                        imgModel.ImgID = 0;
                        imgModel.AuditID = Convert.ToInt32(Request.QueryString["ID"]);
                        imgModel.AuditIndicatorID = Convert.ToInt32(Request.QueryString["ID"]);
                        imgModel.Images = byteImage;
                        imgModel.UserID = usuario.userID;

                        var consImg = imgService.SaveImageByID(imgModel);

                        if (consImg.Count != 0)
                        {
                            AlertHelpers.ShowAlertMessage(this, "Registro", "La imagen se guardo satisfactoriamente.");
                            cargarImg();
                        }
                        else
                        {
                            AlertHelpers.ShowAlertMessage(this, "Error", "No se pudo guardar la imagen");
                        }
                    }
                    else
                    {
                        AlertHelpers.ShowAlertMessage(this, "Validación", "No se selecciono ninguna imagen");
                    }
                }
                else
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Ha excedido el numero máximo de imagenes");
                }
            }
            catch (Exception ex)
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", "Ocurrió un error :" + ex.Message);
            }
        }

        protected void verImagenLink_Click(object sender, EventArgs e)
        {

            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            
            var imgService = container.GetAuditoryService();
            var consImg = imgService.GetImageByImgID(Convert.ToInt32(grdrow.Cells[0].Text)).ToList();

            byte[] byteImg = (byte[])consImg.FirstOrDefault().Images.ToArray();

            if (byteImg != null)
            {
                Response.ContentType = "image/jpeg";
                Response.Expires = 0;
                Response.Buffer = true;
                Response.Clear();
                Response.BinaryWrite(byteImg);
                Response.End();
            }
        }

        protected void btnPruebaImagen_Click(object sender, EventArgs e)
        {
            var consImg = container.GetAuditoryService().GetImageByImgID(14);

            byte[] imagen = consImg.FirstOrDefault().Images.ToArray();
            string path = Server.MapPath("/images/AuditImages/" + consImg.FirstOrDefault().ImgID + ".jpg");
            Image1.ImageUrl = "/images/AuditImages/" + consImg.FirstOrDefault().ImgID + ".jpg";
            GUIHelpers.ImagesHelper.SaveImage(imagen, path, false);
        }
    }
}