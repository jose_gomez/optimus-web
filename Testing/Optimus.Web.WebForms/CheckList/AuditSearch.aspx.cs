﻿using Optimus.Web.BC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class AuditSearch : System.Web.UI.Page
    {
        ServiceContainer container;
        //UnitOfWork worker;

        public AuditSearch()
        {
            container = new ServiceContainer();
            //worker = container.GetUnitOfWork();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            var consAudit = container.GetAuditoryService().GetAllAudit();
            this.dgvAuditList.AutoGenerateColumns = false;
            this.dgvAuditList.DataSource = consAudit;
            this.dgvAuditList.DataBind();

            lbQtyReg.Text = consAudit.Count().ToString();
        }
    }
}