﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditIndicator.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.AuditIndicator" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

<%--    <script>
        <%
            var container = new Optimus.Web.BC.ServiceContainer();
        %>
        function cargarGrilByCategory()
        {
            <%
                if (!string.IsNullOrEmpty(cbCatSearch.Value))
                {
                    dgvIndicator.AutoGenerateColumns = false;
                    var audtInd = container.GetAuditIndicatorService();
                    var consInd = audtInd.GetAllIndicatorByCategoryID(Convert.ToInt32(cbCatSearch.Value));
                    dgvIndicator.DataSource = consInd;
                    dgvIndicator.DataBind();
                }
            %>
            __doPostBack('aBuscar')
        }
        function cargarGridBySelectAudit()
        {
            <%
        if(!string.IsNullOrEmpty(cbAuditType.Value))
        {
            
            dgvIndicator.AutoGenerateColumns = false;
            var audtIndic = container.GetAuditCategoriesService();
            var consIndic = audtIndic.GetCategoryByControlID(Convert.ToInt16(cbAuditType.Value));
            cbCatSearch.DataSource = consIndic;
            cbCatSearch.DataValueField = "ID";
            cbCatSearch.DataTextField = "Categoria";
            cbCatSearch.DataBind();
        }
            %>
            __doPostBack('cbCategory')
        }
    </script>--%>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left; top: -5px; left: 4px;">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">INDICADORES </a>
                        </h4>
                    </div>
                    
                </div>
            </div>
        </div>

        <div id="collapse2" class="panel-collapse collapse in">
            <div class="panel-body">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">REGISTRO DE INDICADORES
                        </h4>
                    </div>
                    <div class="panel-body">
                        <div class="main row">

                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label col-sm-12 col-md-3" for="txtIndicator">Indicador</label>
                                    <asp:TextBox class="form-control" ID="txtIndicator" runat="server" placeholder="Indicador" TextMode="MultiLine" Height="100"></asp:TextBox>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <label class="control-label col-sm-12 col-md-3" for="cbCategory">Categoria</label>
                                    <select id="cbCategory" name="cbCategory" runat="server" class="form-control">
                                        <option></option>

                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <asp:Button  ID="btnSaveIndicator" runat="server" Text="Guardar" class="btn btn-primary" OnClick="btnSaveIndicator_Click" />
                    </div>
                </div>
            

            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">BUSQUEDA DE INDICADORES
                    </h4>
                </div>
                <div class="panel-body">
                    <div style="overflow: scroll;">
                            <asp:GridView ID="dgvIndicator" runat="server" EmptyDataText="No existen Indicadores." ShowHeaderWhenEmpty="false" Width="100%" ShowHeader="true" EnableModelValidation="False" OnRowCreated="dgvIndicator_RowCreated" class="table table-striped" OnSelectedIndexChanged="dgvIndicator_SelectedIndexChanged">
                                <Columns>
                                    <asp:BoundField HeaderText="Auditoria" ShowHeader="false" DataField="ControlName" />
                                    <asp:BoundField HeaderText="Indicador" ShowHeader="false" DataField="Indicator" />
                                    <asp:BoundField HeaderText="Categoria" ShowHeader="false" DataField="Category" />
                                    
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <a href="AuditIndicator.aspx?idIndic=<%# Eval("IdIndicator") %>">Editar</a>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EmptyDataTemplate>
                                    <div class="alert-info">
                                        <strong>Información!</strong> No se encontro ningun registro.
                                    </div>
                                </EmptyDataTemplate>
                                <SelectedRowStyle BackColor="#FFCC00" />
                            </asp:GridView>
                        </div>
                </div>
            </div>
            </div>
        </div>
    </div>

</asp:Content>
