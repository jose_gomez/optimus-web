﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using Optimus.Web.WebForms.GUIHelpers;
using System;
using System.Collections.Generic;
using System.Data.Linq;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class Audit : System.Web.UI.Page
    {
        private int AuditMasterID = 0;
        public string Url = "";
        ServiceContainer container;
        UnitOfWork worker;

        public Audit()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }

        public Employees GetEmployeeById(int employeeId)
        {
            var emp = worker.Employees.GetEmployeeById(employeeId);

            return emp;
        }

        public void FiltroCatAuditadas()
        {
            foreach (GridViewRow fila in dgvListCalegory.Rows)
            {
                var auditService = container.GetAuditoryService();

                SHAuditMaster auditModel = new SHAuditMaster();

                auditModel.AuditDate = Convert.ToDateTime(txtFechaAudit.Text);
                auditModel.DepartmentId = int.Parse(cbDepartment.Value);
                //
                SHAuditDetail auditDetailModel = new SHAuditDetail();
                auditDetailModel.AuditIndicatorId = int.Parse(fila.Cells[0].Text);
                //
                var consAudit = auditService.GetAuditBy_DepartmentID_Date_IndicateID(auditModel, auditDetailModel);

                if (consAudit.Count != 0)
                {
                    var sel = (Button)fila.Cells[2].FindControl("btnSelCategory");
                    sel.Enabled = false;
                    sel.Text = "Completada";
                    sel.BackColor = Color.Blue;
                    sel.ForeColor = Color.Black;

                }
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                txtFechaAudit.Text = DateTime.Now.Date.ToString("d");

                cbDepartment.DataSource = container.GetDepartmentService().GetAllDepartments();
                cbDepartment.DataValueField = "DepartmentId";
                cbDepartment.DataTextField = "Name";
                cbDepartment.DataBind();

                var audControl = container.GetAuditoryControlService().GetAllAuditControl();
                cbAuditType.DataSource = audControl;
                cbAuditType.DataValueField = "ControlID";
                cbAuditType.DataTextField = "NameAudit";
                cbAuditType.DataBind();

                btnGuardarAudit.Enabled = txtFechaAudit.Enabled = txtSupervisorName.Enabled = txtAnalistaName.Enabled = false;
            }
        }

        protected void txtSupervisor_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtSupervisor.Text))
            {
                txtSupervisorName.Text = string.Empty;
                return;
            }
            else
            {
                txtSupervisorName.Text = GetEmployeeById(int.Parse(txtSupervisor.Text)).FullName;
            }
        }

        protected void txtAnalyst_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtAnalyst.Text))
            {
                txtAnalistaName.Text = string.Empty;
                return;
            }
            else
            {
                txtAnalistaName.Text = GetEmployeeById(int.Parse(txtAnalyst.Text)).FullName;
            }
        }

        protected void dgvListCalegory_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onmouseover"] = "this.style.cursor='pointer';this.style.textDecoration='underline';";
                e.Row.Attributes["onmouseout"] = "this.style.textDecoration='none';";
            }
        }

        protected void btnIniciarAudit_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFechaAudit.Text))
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", "Seleccione la Fecha de la Auditoria");
            }
            else if (string.IsNullOrEmpty(txtSupervisor.Text))
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", "Ingrese el Supervisor");
            }
            else if (string.IsNullOrEmpty(txtAnalyst.Text))
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", "Ingrese el Analista");
            }
            else
            {
                dgvListCalegory.AutoGenerateColumns = false;
                var audtInt = container.GetAuditCategoriesService();
                var consInd = audtInt.GetCategoryByControlID(int.Parse(cbAuditType.Value));

                if (consInd.Count != 0)
                {
                    txtAnalyst.Enabled = txtSupervisor.Enabled = btnIniciarAudit.Enabled = txtFechaAudit.Enabled = txtSupervisorName.Enabled = txtAnalistaName.Enabled = false;
                    cbDepartment.Disabled = cbAuditType.Disabled = true;

                    dgvListCalegory.DataSource = consInd;
                    dgvListCalegory.DataBind();

                    FiltroCatAuditadas();
                }
                else
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Este tipo de auditoria no se a definido completamente");
                }

            }
        }

        public void SaveByNumerico()
        {
            // Validaciones
            foreach (GridViewRow fila in GridViewEPP.Rows)
            {
                var range = (DropDownList)fila.Cells[3].FindControl("cbRange");
                var rangeUso = (DropDownList)fila.Cells[4].FindControl("cbRangeUso");
                if (string.IsNullOrEmpty(range.SelectedItem.Value))
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Debe de Completar la Auditoria");
                    range.BackColor = System.Drawing.Color.Red;
                    return;
                }
                else
                {
                    range.BackColor = System.Drawing.Color.White;
                }
                if (string.IsNullOrEmpty(rangeUso.SelectedItem.Value))
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Debe de Completar la Auditoria");
                    rangeUso.BackColor = System.Drawing.Color.Red;
                    return;
                }
                else
                {
                    rangeUso.BackColor = System.Drawing.Color.White;
                }
            }
            ConsAudit();
            SaveAuditCategory(this.AuditMasterID);
            SaveAuditDetailNumeric();
            AlertHelpers.ShowAlertMessage(this, "Registro", "Auditoria Exitosa");
        }

        public void SaveByLiteral()
        {
            // Validaciones
            foreach (GridViewRow fila in GridView1.Rows)
            {
                var radioSi = (RadioButton)fila.Cells[3].FindControl("radioSI");
                var radioNo = (RadioButton)fila.Cells[4].FindControl("radioNO");
                var radioNa = (RadioButton)fila.Cells[5].FindControl("radioNA");
                if (radioSi.Checked == true || radioNo.Checked == true || radioNa.Checked == true)
                {
                    radioSi.BackColor = radioNo.BackColor = radioNa.BackColor = System.Drawing.Color.White;
                }
                else
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Debe de contestar todas las respuestas");
                    radioSi.BackColor = radioNo.BackColor = radioNa.BackColor = System.Drawing.Color.Red;
                    return;
                }
            }

            ConsAudit();
            SaveAuditCategory(this.AuditMasterID);
            SaveAuditDetailLiteral();

            GridView1.DataSource = null;
            GridView1.DataBind();
            btnGuardarAudit.Enabled = false;

            AlertHelpers.ShowAlertMessage(this, "Registro", "Auditoria Exitosa");
        }

        protected void btnGuardarAudit_Click(object sender, EventArgs e)
        {
            var answerType = container.GetAuditoryControlService().GetAllAuditControl().Where(tab => tab.ControlID == int.Parse(cbAuditType.Value)).FirstOrDefault().Answer_type;

            if (answerType == "LITERAL")
            {
                SaveByLiteral();
            }
            else if (answerType == "NUMERICO")
            {
                SaveByNumerico();
            }
            FiltroCatAuditadas();
        }

        protected void btnSelCategory_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtFechaAudit.Text))
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", "Seleccione la Fecha");
            }
            else
            {
                GridViewRow grdrow = (GridViewRow)((Button)sender).NamingContainer;
                lbIdCategory.Text = grdrow.Cells[0].Text;

                GridView1.AutoGenerateColumns = false;

                var auditService = container.GetAuditoryService();

                SHAuditMaster auditModel = new SHAuditMaster();

                auditModel.AuditDate = Convert.ToDateTime(txtFechaAudit.Text);
                auditModel.DepartmentId = int.Parse(cbDepartment.Value);
                //
                SHAuditDetail auditDetailModel = new SHAuditDetail();
                auditDetailModel.AuditIndicatorId = int.Parse(grdrow.Cells[0].Text);
                //
                var consAudit = auditService.GetAuditBy_DepartmentID_Date_IndicateID(auditModel, auditDetailModel);

                if (consAudit.Count != 0)
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Ya se realizo una Auditoria a este departamento y en esta Fecha");
                    txtAnalyst.Enabled = txtSupervisor.Enabled = btnIniciarAudit.Enabled = true;
                    cbDepartment.Disabled = cbAuditType.Disabled = false;
                }
                else
                {
                    var audtInt = container.GetAuditIndicatorService();
                    var consInd = audtInt.GetAllIndicatorByCategoryID(int.Parse(lbIdCategory.Text));

                    if (consInd.Count != 0)
                    {
                        var valor = cbAuditType.Value;

                        var answerType = container.GetAuditoryControlService().GetAllAuditControl().Where(tab => tab.ControlID == int.Parse(cbAuditType.Value)).FirstOrDefault().Answer_type;

                        var audService = container.GetAuditoryControlService();
                        var consSer = audService.GetAuditControlByID(int.Parse(valor));

                        lbAuditType.Text = consSer.FirstOrDefault().NameAudit;
                        if (answerType == "LITERAL")
                        {
                            lbInstrucion.Text = consSer.FirstOrDefault().Instrucciones;
                            GridView1.DataSource = consInd;
                            GridView1.DataBind();

                            btnGuardarAudit.Enabled = true;
                        }
                        else if (answerType == "NUMERICO")
                        {
                            btnGuardarAudit.Enabled = true;

                            lbInstrucion.Text = consSer.FirstOrDefault().Instrucciones;
                            GridViewEPP.DataSource = consInd;
                            GridViewEPP.DataBind();

                            int rangefrom = int.Parse(consSer.FirstOrDefault().valorization_from.ToString());
                            int rangeTo = int.Parse(consSer.FirstOrDefault().valorization_to.ToString());
                            foreach (GridViewRow fila in GridViewEPP.Rows)
                            {
                                var listCondiciones = (DropDownList)fila.Cells[3].FindControl("cbRange");
                                var listUso = (DropDownList)fila.Cells[4].FindControl("cbRangeUso");
                                for (rangefrom = int.Parse(consSer.FirstOrDefault().valorization_from.ToString()); rangefrom <= rangeTo; rangefrom++)
                                {
                                    listCondiciones.Items.Add(new ListItem(rangefrom.ToString(), rangefrom.ToString()));
                                    listUso.Items.Add(new ListItem(rangefrom.ToString(), rangefrom.ToString()));
                                }
                            }
                        }
                        else
                        {
                            AlertHelpers.ShowAlertMessage(this, "Validación", "Esta auditoria no esta completamente definida");
                        }
                    }
                    else
                    {
                        AlertHelpers.ShowAlertMessage(this, "Validación", "Esta auditoria no contiene ninguna pregunta");
                    }
                }
            }
        }

        protected void btnNextFin_Click(object sender, EventArgs e)
        {
            ConsAudit();

            int selectIndicatorByCategory = container.GetAuditCategoriesService().CountIndicatorByCategory(int.Parse(cbAuditType.Value));
            int selectIndicatorByAudit = container.GetAuditCategoriesService().CountIndicatorByAudit(this.AuditMasterID);

            if (selectIndicatorByAudit >= selectIndicatorByCategory)
            {
                SendMail();
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", "La auditoria no se a completado.");
            }
        }

        /// <summary>
        /// Se guarda el maestro de la auditoria.
        /// </summary>
        /// <param name="AuditMasterID"></param>
        public void SaveAuditCategory(int AuditMasterID)
        {
            var auditService = container.GetAuditoryService();
            SHAuditMaster tabAudit = new SHAuditMaster();
            User usuario = worker.User.GetUserByUserName(User.Identity.Name);

            tabAudit.AuditMasterID = AuditMasterID;
            tabAudit.AuditDate = Convert.ToDateTime(txtFechaAudit.Text);
            tabAudit.DepartmentId = int.Parse(cbDepartment.Value);
            tabAudit.SupervisorId = int.Parse(txtSupervisor.Text);
            tabAudit.AnalystId = int.Parse(txtAnalyst.Text);
            tabAudit.ControlID = int.Parse(cbAuditType.Value);
            tabAudit.UsersID = usuario.userID;
            var consAudit = auditService.SaveByID(tabAudit);
            this.AuditMasterID = consAudit.FirstOrDefault().AuditMasterID;
        }

        public void SaveAuditDetailNumeric()
        {
            User usuario = worker.User.GetUserByUserName(User.Identity.Name);

            SHAuditDetail tabAuditDetail = new SHAuditDetail();
            foreach (GridViewRow fila in GridViewEPP.Rows)
            {
                var auditService = container.GetAuditoryService();
                tabAuditDetail.AuditDetailID = 0;
                tabAuditDetail.AuditMasterID = this.AuditMasterID;
                tabAuditDetail.AuditIndicatorId = int.Parse(fila.Cells[2].Text.ToString());
                var range = (DropDownList)fila.Cells[3].FindControl("cbRange");
                tabAuditDetail.Condicion = int.Parse(range.SelectedItem.Value);
                //
                var rangeUso = (DropDownList)fila.Cells[4].FindControl("cbRangeUso");
                tabAuditDetail.Uso = int.Parse(rangeUso.SelectedItem.Value);
                //
                var observ = (TextBox)fila.Cells[5].FindControl("txtObservaciones");
                tabAuditDetail.Observaciones = observ.Text;
                tabAuditDetail.UsersID = usuario.userID;
                var consDetAudit = auditService.SaveDetailByID(tabAuditDetail);
                //
                var fileU = (FileUpload)fila.Cells[6].FindControl("FUplAddImg");
                if (fileU.PostedFile != null && (fileU.PostedFile.ContentLength > 0))
                {
                    SavePhoto(fileU, fila);
                }
                GridViewEPP.DataSource = null;
                GridViewEPP.DataBind();
                btnGuardarAudit.Enabled = false;
            }
        }
        /// <summary>
        /// Se guarda el detalle de la auditoria en cuestión según el tipo de pregunta.
        /// </summary>
        public void SaveAuditDetailLiteral()
        {
            foreach (GridViewRow fila in GridView1.Rows)
            {
                SHAuditDetail tabDetailAudit = new SHAuditDetail();
                User usuario = worker.User.GetUserByUserName(User.Identity.Name);
                var auditService = container.GetAuditoryService();

                tabDetailAudit.AuditMasterID = this.AuditMasterID;
                tabDetailAudit.AuditIndicatorId = int.Parse(fila.Cells[2].Text.ToString());

                var radioSI = (RadioButton)fila.Cells[3].FindControl("radioSI");
                var radioNO = fila.Cells[4].FindControl("radioNO") as RadioButton;
                var radioNA = fila.Cells[5].FindControl("radioNA") as RadioButton;
                if (radioSI.Checked == true)
                {
                    tabDetailAudit.Aplica = radioSI.Attributes["value"].ToString();
                }
                else if (radioNO.Checked)
                {
                    tabDetailAudit.Aplica = radioNO.Attributes["value"].ToString();
                }
                else if (radioNA.Checked)
                {
                    tabDetailAudit.Aplica = radioNA.Attributes["value"].ToString();
                }
                tabDetailAudit.UsersID = usuario.userID;
                var consDetAudit = auditService.SaveDetailByID(tabDetailAudit);
                //
                var fileU = (FileUpload)fila.Cells[6].FindControl("FUplAddImg");

                if (fileU.PostedFile != null && (fileU.PostedFile.ContentLength > 0))
                {
                    SavePhoto(fileU, fila);
                }
            }
        }

        public void SavePhoto(FileUpload fileU, GridViewRow fila)
        {
            var imgService = container.GetAuditoryService();

            HttpPostedFile ImgFile = fileU.PostedFile;

            byte[] byteImage = new byte[fileU.PostedFile.ContentLength];

            ImgFile.InputStream.Read(byteImage, 0, fileU.PostedFile.ContentLength);
            //
            System.Drawing.Image bmap = ImagesHelper.byteArrayToImage(byteImage);
            User usuarioLg = worker.User.GetUserByUserName(User.Identity.Name);
            SHAuditImagen imgModel = new SHAuditImagen();
            imgModel.ImgID = 0;
            imgModel.AuditID = this.AuditMasterID;
            imgModel.AuditIndicatorID = int.Parse(fila.Cells[2].Text.ToString());
            bmap = ImagesHelper.ResizeImage(bmap, 800, 600);
            imgModel.Images = ImagesHelper.ImageToByteArray(bmap);
            imgModel.UserID = usuarioLg.userID;

            var consImg = imgService.SaveImageByID(imgModel);
        }

        /// <summary>
        /// Este Método es para verificar cual es la auditoria en cuestión.
        /// </summary>
        public void ConsAudit()
        {
            var auditService = container.GetAuditoryService();
            var consultAudit = auditService.GetAuditBy_DepartmentID_Date_ControlID(int.Parse(cbDepartment.Value), Convert.ToDateTime(txtFechaAudit.Text), int.Parse(cbAuditType.Value));
            if (consultAudit.Count != 0)
            {
                this.AuditMasterID = consultAudit.FirstOrDefault().AuditMasterID;
            }
        }
        /// <summary>
        /// Este Método es para enviar mensajes de que se realizo la auditoria (Auditoria completada).
        /// </summary>
        public void SendMail()
        {
            var consControlService = container.GetAuditoryControlService().GetAuditControlByID(int.Parse(cbAuditType.Value));

            var controlService = container.GetAuditoryControlService().GetEmailsByAuditControlID(consControlService.FirstOrDefault().NameAudit);
            var consDesp = container.GetDepartmentService().GetBy(int.Parse(cbDepartment.Value)).Name;
            if (controlService.Count != 0)
            {
                foreach (var a in controlService)
                {
                    MailAddress address = new MailAddress(a.Email);
                    List<MailAddress> recipients = new List<MailAddress>() { address };
                    string subject = "Reporte de Auditoria";

                    Url = string.Format(container.GetSystemConfigService().GetSysTemConfigByName("EMAIL DE REPORTE GENARAL DE AUDITORIA").SysConfigValue, consControlService.FirstOrDefault().ControlID, txtFechaAudit.Text, cbDepartment.Value);

                    string body = string.Format("Se realizo la auditoria de : {0}, en el departamento de : {1}, para ver los resultados de click en el siguiente enlace {2}", consControlService.FirstOrDefault().NameAudit, consDesp, Url);
                    DRL.Utilities.Mail.Mailer.SendMail(subject, recipients, body, false); //
                }
                AlertHelpers.ShowAlertMessage(this, "Bien", "La auditoria se completo correctamente.");
            }
        }
    }
}