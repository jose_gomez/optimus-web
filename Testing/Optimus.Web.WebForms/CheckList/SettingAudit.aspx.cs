﻿using DRL.Utilities.Mail;
using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class SettingAudit : System.Web.UI.Page
    {
        public int controlID = 0;
        ServiceContainer container;
        UnitOfWork worker;

        public SettingAudit()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                var audControl = container.GetAuditoryControlService().GetAllAuditControl();
                cbAudit.DataSource = audControl;
                cbAudit.DataValueField = "ControlID";
                cbAudit.DataTextField = "NameAudit";
                cbAudit.DataBind();

                btnGuardar.Enabled = false;
                txtAuditName.Disabled = true;
                
            }
        }

        public void Limpiar()
        {
            txtInstrucciones.Text = txtMaxFhoto.Value = txtStartValue.Value = txtEndValue.Value = string.Empty;
            txtAuditName.Value = "";
            cbAuditFormat.Value = "0";
            lbControlID.Text = "0";
        }
        public void CargarEmail()
        {
            dgvAlertas.AutoGenerateColumns = false;
            var controlService = container.GetAuditoryControlService().GetEmailsByAuditControlID(txtAuditName.Value);
            dgvAlertas.DataSource = controlService;
            dgvAlertas.DataBind();
        }

        protected void btnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                var consNameControl = container.GetAuditoryControlService().GetAuditControlByName(txtAuditName.Value.Trim());

                if (string.IsNullOrEmpty(txtAuditName.Value.Trim()))
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Ingresar el nombre de la auditoria");
                }
                else if (cbAuditFormat.Value == "0")
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Debe de seleccionar el formato de la auditoria");
                }
                else if (string.IsNullOrEmpty(txtMaxFhoto.Value.Trim()))
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Debe de ingresar la cantidad maxima de fotos");
                }
                else
                {

                    if (int.Parse(lbControlID.Text) == 0 && consNameControl.Count != 0)
                    {
                        AlertHelpers.ShowAlertMessage(this, "Validación", "No se pueden registrar varias auditorias con el mismo nombre.");
                        return;
                    }

                    SHAuditControl auditControl = new SHAuditControl();

                    auditControl.ControlID = Convert.ToInt32(lbControlID.Text);
                    auditControl.NameAudit = txtAuditName.Value.ToUpper().Trim();
                    auditControl.DescriptionAudit = txtDescripcionAudit.Text;
                    auditControl.Answer_type = cbAuditFormat.Value;
                    auditControl.Instrucciones = Server.HtmlEncode(txtInstrucciones.Text.Trim());
                    if (cbAuditFormat.Value == "NUMERICO")
                    {
                        if (string.IsNullOrEmpty(txtStartValue.Value.Trim()))
                        {
                            AlertHelpers.ShowAlertMessage(this, "Validación", "Ingresar el número de inicio");
                        }
                        else if (string.IsNullOrEmpty(txtEndValue.Value.Trim()))
                        {
                            AlertHelpers.ShowAlertMessage(this, "Validación", "Ingresar el número final");
                        }
                        else if(Convert.ToInt32(txtStartValue.Value) > Convert.ToInt32(txtEndValue.Value))
                        {
                            AlertHelpers.ShowAlertMessage(this, "Validación", "El valor de inicio no puede ser mayor que el valor final");
                            return;
                        }
                        else
                        {
                            auditControl.valorization_from = Convert.ToInt32(txtStartValue.Value);
                            auditControl.valorization_to = Convert.ToInt32(txtEndValue.Value);
                        }
                    }
                    
                    auditControl.Images_number = Convert.ToInt32(txtMaxFhoto.Value);
                    auditControl.Users = User.Identity.Name;

                    var auditControlService = container.GetAuditoryControlService();
                    var consAuditControl = auditControlService.SaveAuditControlByID(auditControl);

                    if (consAuditControl.Count > 0)
                    {
                        AlertHelpers.ShowAlertMessage(this, "Bien", "Registro Exitoso");
                        // Activar Controles
                        btnGuardar.Enabled = false;
                        txtAuditName.Disabled = true;
                        Limpiar();
                    }
                    else
                    {
                        AlertHelpers.ShowAlertMessage(this, "Error", "No se registro nada");
                    }
                }

            }
            catch (Exception ex)
            {
                AlertHelpers.ShowAlertMessage(this, "Exception", ex.Message);
            }
        }

        protected void btnEditAudit_Click(object sender, EventArgs e)
        {
            var contService = container.GetAuditoryControlService();
            var consControl = contService.GetAuditControlByID(Convert.ToInt32(cbAudit.Value));

            lbControlID.Text = consControl.FirstOrDefault().ControlID.ToString();
            txtAuditName.Value = consControl.FirstOrDefault().NameAudit;
            txtDescripcionAudit.Text = consControl.FirstOrDefault().DescriptionAudit;
            cbAuditFormat.Value = consControl.FirstOrDefault().Answer_type;
            txtInstrucciones.Text = Server.HtmlDecode(consControl.FirstOrDefault().Instrucciones);
            txtMaxFhoto.Value = consControl.FirstOrDefault().Images_number.ToString();
            txtStartValue.Value = consControl.FirstOrDefault().valorization_from.ToString();
            txtEndValue.Value = consControl.FirstOrDefault().valorization_to.ToString();
            //
            dgvAlertas.AutoGenerateColumns = false;
            var controlService = container.GetAuditoryControlService().GetEmailsByAuditControlID(txtAuditName.Value);
            dgvAlertas.DataSource = controlService;
            dgvAlertas.DataBind();
            // Activar Controles
            btnGuardar.Enabled = true;
            txtAuditName.Disabled = true;
        }

        protected void btnNewAudit_Click(object sender, EventArgs e)
        {
            Limpiar();
            btnGuardar.Enabled = true;
            txtAuditName.Disabled = false;
        }
    }
}