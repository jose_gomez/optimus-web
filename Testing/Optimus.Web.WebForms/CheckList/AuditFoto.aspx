﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AuditFoto.aspx.cs" Inherits="Optimus.Web.WebForms.CheckList.AuditFoto" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div class="panel panel-default">

        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse5"> FOTOS</a>
                        </h4>
                    </div>
                </div>
            </div>
        </div>

        <div id="collapse5" class="panel-collapse collapse in">
            <div class="panel-body">
                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">CARGAR FOTO
                        </h4>
                    </div>
                    <div class="panel-body">
                         <p>Debe de tener en cuenta que el máximo de foto segun este tipo de auditoria es de =
                            <asp:Label ID="lbQtyFoto" runat="server" Text=""></asp:Label> fotos
                         </p>
                        
                    </div><asp:FileUpload ID="flAuditImg" runat="server"  AutoPostBack="true" AllowMultiple="false" />
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                        <asp:Button ID="btnSaveImg" runat="server" Text="Guardar Foto" Class="btn btn-primary" OnClick="btnSaveImg_Click" />
                    </div>
                </div>

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h4 class="panel-title">SELECCIONAR LA FOTO
                        </h4>
                    </div>
                    <div class="panel-body">
                        
                        <asp:GridView ID="dgvImages" runat="server" AutoGenerateColumns="false" Width="100%">
                            <Columns>
                                <asp:BoundField HeaderText="ID" ShowHeader="false" DataField="ImgID" />
                                <asp:TemplateField HeaderText="Imagen">
                                    <ItemTemplate>
                                        <asp:Image ID="Imagen" runat="server" ImageUrl='<%# Eval("ImgID") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                                <asp:TemplateField HeaderText="">
                                    <ItemTemplate>
                                        <asp:LinkButton runat="server" ID="verImagenLink" OnClick="verImagenLink_Click" class="btn btn-primary">Ver Imagen</asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>

                    </div>
                    <div class="panel-footer" style="vertical-align: middle; text-align: right">
                    </div>
                </div>
                
            </div>
            
        </div>
    </div>

    .............................
    <asp:Button ID="btnPruebaImagen" runat="server" Text="Ver esta Foto" OnClick="btnPruebaImagen_Click" />
    <br />
    <asp:Image ID="Image1" runat="server" />
    <a href="/images/AuditImages/temp.jpg">Ver Fotos</a>

</asp:Content>
