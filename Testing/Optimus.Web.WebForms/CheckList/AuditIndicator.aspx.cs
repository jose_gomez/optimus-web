﻿using Optimus.Web.BC;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.CheckList
{
    public partial class AuditIndicator : System.Web.UI.Page
    {
        ServiceContainer container;
        public AuditIndicator()
        {
            InitializeCulture();
            container = new ServiceContainer();
        }
        public void CargarInd()
        {
            dgvIndicator.AutoGenerateColumns = false;
            var audtInt = container.GetAuditIndicatorService();
            var consInd = audtInt.GetAllIndicator();
            dgvIndicator.DataSource = consInd;
            dgvIndicator.DataBind();
        }

        public void CargarAut()
        {
            //var audControl = container.GetAuditoryControlService().GetAllAuditControl();
            //cbAuditType.DataSource = audControl;
            //cbAuditType.DataValueField = "ControlID";
            //cbAuditType.DataTextField = "NameAudit";
            //cbAuditType.DataBind();
        }

        public void CargarCat()
        {
            var audtCat = container.GetAuditCategoriesService();
            var consCat = audtCat.GetAllCategory();
            
            cbCategory.DataSource = consCat;
            cbCategory.DataValueField = "ID";
            cbCategory.DataTextField = "Categoria";
            cbCategory.DataBind();
        }

        protected void btnSaveIndicator_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtIndicator.Text))
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Ingrese un Indicador");
                }
                else if (string.IsNullOrEmpty(cbCategory.Value))
                {
                    AlertHelpers.ShowAlertMessage(this, "Validación", "Debe de cargar las categorias");
                }
                else
                {
                    SHAuditIndicator tabInd = new SHAuditIndicator();
                    var audService = container.GetAuditIndicatorService();

                    tabInd.ID = int.Parse(Request.QueryString["idIndic"]);
                    tabInd.AuditIndicator = txtIndicator.Text.Trim();
                    tabInd.AuditCategory = Convert.ToInt32(cbCategory.Value);
                    tabInd.User = User.Identity.Name;

                    audService.SaveByID(tabInd);
                    AlertHelpers.ShowAlertMessage(this, "Registro", "Indicador Registrado");

                    CargarInd();
                    txtIndicator.Text = string.Empty;
                }
            }
            catch (Exception ex)
            {
                AlertHelpers.ShowAlertMessage(this, "Validación", ex.Message);
            }
        }

        protected void btnCargarListCat_Click(object sender, EventArgs e)
        {
            //var audtCat = container.GetAuditCategoriesService();
            //var consCat = audtCat.GetCategoryByControlID(Convert.ToInt32("1")); // cbAuditType.Value
            //cbCategory.DataValueField = "ID";
            //cbCategory.DataTextField = "Categoria";
            //cbCategory.DataSource = consCat;
            //cbCategory.DataBind();
        }

        protected void selIndicator_Click(object sender, EventArgs e)
        {
            GridViewRow grdrow = (GridViewRow)((LinkButton)sender).NamingContainer;
            txtIndicator.Text = Server.HtmlDecode(grdrow.Cells[1].Text);
            cbCategory.Value = grdrow.Cells[2].Text;
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                //
                CargarAut();
                CargarCat();
                CargarInd();
                //
                string idInd = Request.QueryString["idIndic"];

                if (string.IsNullOrEmpty(idInd))
                {
                    Response.Redirect("AuditIndicator.aspx?idIndic=0");
                }

                if (int.Parse(idInd) != 0)
                {
                    var indicService = container.GetAuditIndicatorService();
                    var consIndic = indicService.GetIndicatorByID(int.Parse(idInd));
                    txtIndicator.Text = consIndic.FirstOrDefault().AuditIndicator;
                    cbCategory.Value = consIndic.FirstOrDefault().AuditCategory.ToString();
                }
            }
        }

        protected void dgvIndicator_RowCreated(object sender, GridViewRowEventArgs e)
        {

        }

        protected void dgvIndicator_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        //protected void btnSearch_Click(object sender, EventArgs e)
        //{
        //    dgvIndicator.AutoGenerateColumns = false;
        //    var audtIndic = container.GetAuditIndicatorService();
        //    var consIndic = audtIndic.GetAllIndicatorByCategoryID(Convert.ToInt32(cbCatSearch.Value));
        //    dgvIndicator.DataSource = consIndic;
        //    dgvIndicator.DataBind();
        //}

        //protected void btnBuscarSearch_Click(object sender, EventArgs e)
        //{
        //    if (!string.IsNullOrEmpty(cbCatSearch.Value))
        //    {
        //        dgvIndicator.AutoGenerateColumns = false;
        //        var audtInd = container.GetAuditIndicatorService();
        //        var consInd = audtInd.GetAllIndicatorByCategoryID(Convert.ToInt32(cbCatSearch.Value));
        //        dgvIndicator.DataSource = consInd;
        //        dgvIndicator.DataBind();
        //    }
        //}
    }
}