﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.WebForms.GuiHelpers;
using Optimus.Web.WebForms.Security;
using System;
using System.Linq;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms
{
    public partial class SiteMaster : MasterPage
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        protected void Page_Init(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            string path = Request.FilePath;
            if (!this.Page.User.Identity.IsAuthenticated && !NotPermissionPages.notPermissionPages.Contains(path.ToUpper()))
            {
                Response.Redirect("~/Login.aspx", true);
            }
            else if (this.Page.User.Identity.IsAuthenticated && !NotPermissionPages.notPermissionPages.Contains(path.ToUpper()))
            {
                bool HaveAcces = false;
                if (path.ToUpper() == "/INTRANET")
                {
                    HaveAcces = true;
                }
                else
                {
                    var urlsPermittedForUser = worker.UserSystemPermissions.GetSystemPermissionByUserID((worker.User.GetUserByUserName(this.Page.User.Identity.Name)).userID);

                    if (urlsPermittedForUser.Any(c => c.ConstantName.ToLower() == path.ToLower()))
                    {
                        HaveAcces = true;
                    }
                }
                if (!HaveAcces) DRL.Utilities.ErrorPage.ErrorPages.RedirectToErrorPage("Usted no tiene permisos suficientes para acceder a esta pagina", true);
            }

        }

        protected void IniciarSesion(object sender, EventArgs e)
        {
            CustomMembership a = new CustomMembership();
            string user = string.Empty;
            string pass = string.Empty;
            bool access = false;

            if (!this.Page.User.Identity.IsAuthenticated)
            {
                user = ((TextBox)IdLogin.FindControl("txtUserName")).Text;
                pass = ((TextBox)IdLogin.FindControl("txtPassword")).Text;
                access = a.ValidateUser(user, pass);
            }
            if (access)
            {
                FormsAuthentication.RedirectFromLoginPage(user, true);
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Error!", "Usuario o contraseña incorrecto");
            }
        }

        protected void SalirSesion(object sender, EventArgs e)
        {
            FormsAuthentication.SignOut();
            Response.Redirect("~/Default.aspx");
        }
    }
}