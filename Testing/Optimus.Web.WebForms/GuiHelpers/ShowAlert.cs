﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;

namespace Optimus.Web.WebForms.GuiHelpers
{
    public class AlertHelpers
    {

        public static void ShowAlertMessage(Page sender, string header, string message)
        {
           sender.ClientScript.RegisterStartupScript(sender.GetType(), "ShowMsg", "<script>showModalMessage('" + header + "', '" +message +" ');</script> ");
        }
    }
}