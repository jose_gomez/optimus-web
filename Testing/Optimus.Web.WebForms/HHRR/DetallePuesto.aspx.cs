﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class DetallePuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_ADMINISTRAR_ANALISIS_PUESTO = "~/HHRR/AdministrarAnalisisPuesto.aspx?ID={0}";
        private const string LINK_ADMINISTRAR_COMPETENCIAS_PUESTO = "~/HHRR/AdministrarCompetenciasPuesto.aspx?ID={0}";
        private const string LINK_ADMINISTRAR_MATRIZ_SALARIAL = "~/HHRR/AdministrarMatrizSalarial.aspx?ID={0}";
        private const string LINK_ADMINISTRAR_PUESTO = "~/HHRR/AdministrarPuesto.aspx?ID={0}";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string SIN_DEPARTAMENTO = "Sin departamento.";
        private const string SIN_SUPERIOR = "Sin superior asignado";
        private const string TOTAL = "TOTAL";
        private const string URL_PARAMETER = "ID";
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    DA.HRPosition position = ServCont.GetPositionService().GetPositionByID(ObtenerID());
                    lblPuestoName.Text = position.PositionName;
                    lblDescripcion.Text = position.Description;
                    GetDepartment();
                    GetSuperior();
                    GetSubordinados();
                    GetMatrizSalarial();
                    SetLinks();
                    OrgChartImg.Src = string.Format("OrgChartImg.aspx?ID={0}", ObtenerID().ToString());
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private string CrearAnalisis(int id)
        {
            string html = string.Empty;
            DA.HRPositionAnalisy analisis = ServCont.GetPositionsService().GetPositionsAnalisyBYPositionID(id);

            if (analisis != null)
            {
                List<DA.HRPositionAnalisysDetail> detalles = ServCont.GetPositionsService().GetPositionAnalisyDeatailByAnalisyID(analisis.PositionAnalysisID);
                html = "<ul>";
                foreach (DA.HRPositionAnalisysDetail item in detalles)
                {
                    html += string.Format("<li><b>{0}</b><ul>", item.Funtion);
                    html += string.Format("<li><b>Que hace?</b> {0}</li>", item.WhatDo);
                    html += string.Format("<li><b>Como lo hace?</b> {0}</li>", item.HowDo);
                    html += string.Format("<li><b>Para que lo hace?</b> {0}</li>", item.WhyDo);
                    html += string.Format("<li><b>Que tiempo se toma para hacerlo?</b> {0}</li></ul></ li>", item.TimeDo);
                }
                html += "</ ul >";
            }

            return html;
        }

        private string CrearCompetencias(int id)
        {
            string html = string.Empty;
            List<DA.HRCompetencesCategory> cate = ServCont.GetPositionsService().GetCompetencesCategoryBYPosicionID(id);
            if (cate != null)
            {
                html = "<ul>";
                foreach (DA.HRCompetencesCategory categoy in cate)
                {
                    html += string.Format("<li><b>{0}</b>", categoy.Name);
                    List<BC.Models.Competencia> comp = ServCont.GetPositionsService().GetCompetenciasByCategoryByID(categoy.CompetenceCategoryID, id);

                    if (comp != null)
                    {
                        html += "<ul>";
                        foreach (BC.Models.Competencia item in comp)
                        {
                            html += string.Format("<li><b>{0}</b> {1}/10: {2}</li>", item.Nombre, item.Nivel.ToString(), item.Descripcion);
                        }
                        html += "</ul>";
                    }
                    html += "</li>";
                }
                html += "</ul>";
            }

            return html;
        }

        private string CrearNivelesMatriz()
        {
            string html = string.Empty;
            List<DA.HRSalaryLevel> lvl = ServCont.GetPositionsService().GetAllSalaryLevelOrderBySequence();
            if (lvl != null)
            {
                html = "<ul>";
                foreach (DA.HRSalaryLevel level in lvl)
                {
                    html += string.Format("<li><b>{0}: </b>{1}", level.Name, level.Descripcion);

                    html += "</li>";
                }
                html += "</ul>";
            }

            return html;
        }

        private void GetDepartment()
        {
            DA.HRDeparmentsPosition rela = ServCont.GetPositionsService().GetDepartmentPosition(ObtenerID());
            if (rela != null)
            {
                DA.HRDepartment de = ServCont.GetDepartmentService().GetBy((int)rela.DeparmentID);
                if (de != null)
                {
                    lblDepartamento.Text = de.Name;
                }
            }
            else
            {
                lblDepartamento.Text = SIN_DEPARTAMENTO;
            }
        }

        private void GetMatrizSalarial()
        {
            List<DA.VWMatrizSalarial> matriz = ServCont.GetPositionsService().GetMatrizSalarialByPositionID(ObtenerID());
            DA.VWMatrizSalarial matr = new DA.VWMatrizSalarial { D = 0, C = 0, PROMEDIO = 0, B = 0, A = 0 };
            foreach (DA.VWMatrizSalarial item in matriz)
            {
                matr.MarcoGeneral = TOTAL;
                matr.D += item.D;
                matr.C += item.C;
                matr.PROMEDIO += item.PROMEDIO;
                matr.B += item.B;
                matr.A += item.A;
            }
            matriz.Add(matr);

            gvMatriz.DataSource = matriz;
            gvMatriz.DataBind();

            spanprueba.InnerHtml = CrearAnalisis(ObtenerID());
            span1.InnerHtml = CrearCompetencias(ObtenerID());
            span2.InnerHtml = CrearNivelesMatriz();
        }

        private void GetSubordinados()
        {
            //List<DA.HRPosition> subors = ServCont.GetPositionsService().GetSubordinadosByPositionID(ObtenerID());
            //if (subors.Count > 0)
            //{
            //    //<h4>Subordinados:</h4>
            //    string html = "";

            //    foreach (DA.HRPosition subor in subors)
            //    {
            //        html += string.Format("{0}", string.IsNullOrWhiteSpace(html) ? subor.PositionName : ", " + subor.PositionName);
            //    }
            //    html = "<h4>Subordinados:</h4>" + html;
            //    divSub.InnerHtml = html;
            //}
        }

        private void GetSuperior()
        {
            DA.HRPosition position = ServCont.GetPositionService().GetPositionByID(ObtenerID());
            if (position != null)
            {
                DA.HRPosition superior = ServCont.GetPositionService().GetPositionByID((position.SuperiorPositionID) ?? 0);
                if (superior != null)
                {
                    lblSuperior.Text = superior.PositionName;
                }
                else
                {
                    lblSuperior.Text = SIN_SUPERIOR;
                }
            }
        }

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        private void SetLinks()
        {
            HyperLink1.NavigateUrl = string.Format(LINK_ADMINISTRAR_PUESTO, ObtenerID().ToString());
            HyperLink2.NavigateUrl = string.Format(LINK_ADMINISTRAR_COMPETENCIAS_PUESTO, ObtenerID().ToString());
            HyperLink3.NavigateUrl = string.Format(LINK_ADMINISTRAR_ANALISIS_PUESTO, ObtenerID().ToString());
            HyperLink4.NavigateUrl = string.Format(LINK_ADMINISTRAR_MATRIZ_SALARIAL, ObtenerID().ToString());
            HyperLink5.NavigateUrl = string.Format("~/HHRR/OrgChartImg.aspx?ID={0}", ObtenerID().ToString());
        }

        #endregion Private Methods

    }
}