﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ListaPuesto.aspx.cs" Inherits="Optimus.Web.WebForms.HHRR.ListaPuesto" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Organigrama</h3>
        </div>
        <div class="panel-body">
            <img src="OrgChartImg.aspx" style="width: 100%" />
        </div>
        <div class="panel-footer">
            <ul class="pager">
                <li class="next">
                    <asp:HyperLink ID="HyperLink2" runat="server">Ver Organigrama</asp:HyperLink></li>
            </ul>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3>Lista de puestos</h3>
        </div>
        <div class="panel-body">
            <asp:GridView ID="gvPuestos" CssClass="table table-hover table-condensed medium-top-margin col-sx-12" runat="server" AutoGenerateColumns="False" BorderStyle="None">
                <Columns>
                    <asp:TemplateField HeaderText="Nombre">
                        <ItemTemplate>
                            <a href='/HHRR/DetallePuesto?ID=<%# Eval("PositionID") %>'><%# Eval("PositionName") %></a>
                        </ItemTemplate>
                        <ControlStyle BorderStyle="None" />
                        <HeaderStyle Font-Size="Large" HorizontalAlign="Justify" VerticalAlign="Middle" BorderStyle="None" />
                        <ItemStyle BorderStyle="None" />
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
        </div>
        <div class="panel-footer" style="text-align: right">
            <asp:Button ID="btnNewComp" CssClass="btn btn-primary" runat="server" Text="Nuevo" OnClick="btnNewComp_Click" />
        </div>
    </div>
</asp:Content>
