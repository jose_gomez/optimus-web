﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="RequisicionPersonal.aspx.cs" EnableEventValidation="false" Inherits="Optimus.Web.WebForms.RequisicionPersonal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .col-sm-12 {
            max-width: 100%;
        }

        .col-md-9 {
            max-width: 60%;
        }
    </style>
    <script type="text/javascript">
        function NewPosition() {
            if (document.getElementById('<%=radioRetiro.ClientID%>').checked) {
                document.getElementById('<%=txtReplazaA.ClientID%>').disabled = false;
                document.getElementById('<%=txtCual.ClientID%>').disabled = true;
                document.getElementById('<%=txtCual.ClientID%>').value = document.getElementById('<%=radioRetiro.ClientID%>').value;
            }
            if (document.getElementById('<%=radioReemplazo.ClientID%>').checked) {
                document.getElementById('<%=txtReplazaA.ClientID%>').disabled = false;
                document.getElementById('<%=txtCual.ClientID%>').disabled = true;
                document.getElementById('<%=txtCual.ClientID%>').value = document.getElementById('<%=radioReemplazo.ClientID%>').value;
            }
            if (document.getElementById('<%=radioNuevo.ClientID%>').checked) {
                document.getElementById('<%=txtReplazaA.ClientID%>').disabled = true;
                document.getElementById('<%=txtReplazaA.ClientID%>').value = ''; 
                document.getElementById('<%=Label2.ClientID%>').value = '';
                document.getElementById('<%=txtCual.ClientID%>').disabled = true;
                document.getElementById('<%=txtCual.ClientID%>').value = document.getElementById('<%=radioNuevo.ClientID%>').value;
            }
            if (document.getElementById('<%=radioOtro.ClientID%>').checked) {
                document.getElementById('<%=txtReplazaA.ClientID%>').disabled = false;
                document.getElementById('<%=txtCual.ClientID%>').disabled = false;
                document.getElementById('<%=txtCual.ClientID%>').value = '';
            }
        }

        <%--function actdesatxtPuesto() {
            document.getElementById('<%=txtPuestoRequerido.ClientID%>').disabled = !document.getElementById('<%=txtPuestoRequerido.ClientID%>').disabled;
        }--%>

        function New() {
            var a = document.getElementById('<%=ddlPuestos.ClientID%>');
            var selectedText = a.options[a.selectedIndex].text;
            document.getElementById('<%=txtPuestoRequerido.ClientID%>').value = selectedText;
            document.getElementById('<%=txtPuestoRequerido.ClientID%>').disabled = false;
            $('#demo').collapse('hide');
        }
    </script>
    <div id="BootstrapMessage" runat="server">
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: left">
                        <h3>Requisición de personal</h3>
                    </div>
                    <div class="col-xs-12 col-md-6" style="vertical-align: middle; text-align: right">
                        <asp:Label ID="lblFechaActual" runat="server" Text="" Style="vertical-align: middle; text-align: right" Font-Bold="True" Font-Italic="True"></asp:Label>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-body">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h4 class="panel-title">Instrucciones
                    </h4>
                </div>
                <div class="panel-body">
                    <p>
                        <b>Señores Gerentes:</b> Recuerden al realizar su requisición de personal con un <b>MINIMO 10 DIAS DE ANTICIPACIÓN</b>, a la fecha en que debe ser cubierta la plaza solicitada, de manera que el Departamento de Gestión Humana pueda realizar el procedimiento de reclutamiento, selección y contratación según lo establecido.
                    </p>
                </div>
            </div>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Datos de la requisición</a>
                        </h4>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <label class="control-label col-sm-12 col-md-3" for="txtPuestoRequerido" runat="server">Puesto requerido:</label>
                                    <asp:TextBox ID="txtPuestoRequerido" runat="server" CssClass="form-control col-sm-12 col-md-9" AutoCompleteType="None" MaxLength="50" Enabled="False"></asp:TextBox>
                                    <button name="btnBuscar" type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo" >Buscar</button>
                                </div>
                                <div class="row">
                                    <div id="demo" class="collapse">
                                        <label class="control-label col-sm-12 col-md-3" for="ddlPuestos" runat="server"></label>
                                        <asp:DropDownList ID="ddlPuestos" runat="server" CssClass="form-control col-sm-12 col-md-9" AutoPostBack="true" OnSelectedIndexChanged="ddlPuestos_SelectedIndexChanged"></asp:DropDownList>
                                    </div>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-12 col-md-3" for="txtNoEmpleados">Cantidad de empleados requeridos:</label>
                                    <input type="number" runat="server" class="form-control" id="txtNoEmpleados" min="1" value="1">
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-12 col-md-3" for="Select2">Área:</label>
                                    <asp:DropDownList ID="ddlDepartamento" runat="server" CssClass="form-control" AutoPostBack="True" OnSelectedIndexChanged="ddlDepartamento_SelectedIndexChanged"></asp:DropDownList>
                                </div>
                                <div class="row">
                                    <label class="control-label col-sm-12 col-md-3" for="Select1">Dependencia:</label>
                                    <asp:DropDownList ID="ddlPuestos1" runat="server" CssClass="form-control"></asp:DropDownList>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Motivo de la requisición</a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="container">
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">

                                        <div class="radio">
                                            <label>
                                                <input type="radio" runat="server" id="radioRetiro" name="optradio" value="Retiro/ Renuncia empleado." onchange="NewPosition();">Retiro / Renuncia empleado.</label>
                                        </div>

                                        <div class="radio">
                                            <label>
                                                <input type="radio" runat="server" id="radioReemplazo" name="optradio" value="Reemplazo por maternidad / incapacidad." onchange="NewPosition();">Reemplazo por maternidad / incapacidad.</label>
                                        </div>

                                        <div class="radio">
                                            <label>
                                                <input type="radio" runat="server" id="radioNuevo" name="optradio" value="Nuevo puesto." onchange="NewPosition();">Nuevo puesto.</label>
                                        </div>

                                        <div class="radio">
                                            <label>
                                                <input type="radio" runat="server" id="radioOtro" name="optradio" value="Otro." onchange="NewPosition();">Otro.</label>
                                        </div>

                                    </div>
                                    <div class="col-sm-12 col-md-6">

                                        <div class="row">
                                            <div class="col-md-12">
                                                <label class="control-label">Código del empleado a remplazar:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <asp:TextBox ID="txtReplazaA" runat="server" CssClass="form-control" OnTextChanged="txtReplazaA_TextChanged" AutoPostBack="True"></asp:TextBox>
                                                <asp:UpdatePanel runat="server" ID="UpdatePanel3" UpdateMode="Conditional">
                                                    <ContentTemplate>
                                                        <asp:Label ID="Label2" Text="" runat="server" />
                                                    </ContentTemplate>

                                                    <Triggers>
                                                        <asp:AsyncPostBackTrigger ControlID="txtReplazaA" EventName="TextChanged" />
                                                    </Triggers>
                                                </asp:UpdatePanel>
                                            </div>
                                            <div class="col-md-12">
                                                <label class="control-label">Otro motivo:</label>
                                            </div>
                                            <div class="col-md-12">
                                                <input type="text" class="form-control" id="txtCual" runat="server" placeholder="Motivo" maxlength="250">
                                            </div>
                                        </div>

                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Tiempo de vinculación requerido:</label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" CssClass="radio">
                                            <asp:ListItem Selected="True">Indefinido.</asp:ListItem>
                                            <asp:ListItem>Temporal.</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <label class="control-label">Jornada laboral:</label>
                                    </div>
                                    <div class="col-sm-12 col-md-3">
                                        <asp:RadioButtonList ID="RadioButtonList2" runat="server" CssClass="radio">
                                            <asp:ListItem Selected="True">Tiempo completo.</asp:ListItem>
                                            <asp:ListItem>Medio tiempo.</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12 col-md-6">
                                        <div class="col-md-12">
                                            <label class="control-label">Horario laboral requerido:</label>
                                        </div>
                                        <div class="col-md-12">
                                            <asp:DropDownList ID="ddlJornadas" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlJornadas_SelectedIndexChanged"></asp:DropDownList>
                                            <asp:UpdatePanel runat="server" ID="UpdatePanel" UpdateMode="Conditional">
                                                <ContentTemplate>
                                                    <asp:Label ID="Label1" Text="" runat="server" />
                                                </ContentTemplate>

                                                <Triggers>
                                                    <asp:AsyncPostBackTrigger ControlID="ddlJornadas" EventName="SelectedIndexChanged" />
                                                </Triggers>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="col-sm-12 col-md-6">
                                        <div class="col-md-12">
                                            <label class="control-label">Fecha de inicio de labores:</label>

                                        </div>
                                        <div class="col-md-12">
                                            <asp:TextBox runat="server" class="form-control datepicker" ID="txtFechaInicioLabor" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel-footer" style="vertical-align: middle; text-align: right">
            <asp:Button ID="btnSave" CssClass="btn btn-primary" runat="server" Text="Guardar" OnClick="btnSave_Click" />
        </div>

    </div>

</asp:Content>
