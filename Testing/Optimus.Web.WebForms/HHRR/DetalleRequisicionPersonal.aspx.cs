﻿using Optimus.Web.DA;
using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Requisicion_de_personal
{
    public partial class DetalleRequisicionPersonal : System.Web.UI.Page
    {
        #region Private Fields

        private const string URL_PARAMETER = "ID";
        private int APPROVE = 1;
        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void bttnAprovacion_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                DA.HRRequisitionsApprobation aprobacion = new HRRequisitionsApprobation();
                DA.HRDepartment departamento = ServCont.GetDepartmentService().GetBy(ServCont.GetPersonnelRequisitionService().GetByID(ObtenerID()).DepartmentID);
                aprobacion.TypeRequisitionID = (bool)departamento.IsProduction ? 1 : 2;//TODO: Carlos.Dominguez Cambiar este tipo de requisición que se busque en el sistema.
                aprobacion.RequisitionID = ObtenerID();
                aprobacion.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;
                aprobacion.StatusID = APPROVE;
                ServCont.GetPersonnelRequisitionService().Approve(aprobacion);
                Response.Redirect(Request.RawUrl);
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    int id = ObtenerID();
                    DA.HRPersonnelRequisition requi = ServCont.GetPersonnelRequisitionService().GetByID(id);

                    lblPuestos.Text = requi.PositionName;
                    lblCantidadEmpleados.Text = requi.QuantityEmployees.ToString();
                    lblDepartamentos.Text = requi.HRDepartment.Name;
                    lblSupervisor.Text = ServCont.GetPositionsService().GetPositionsByID(requi.SuperiorPositionID).PositionName;
                    lblMotivo.Text = requi.Reason;
                    lblTiempoRequerido.Text = requi.Contract;
                    lblJornadaLaboral.Text = requi.Workday;
                    lblHorario.Text = requi.WorkingHours;
                    ObtenerHorario();
                    lblUser.Text = requi.User;

                    List<int> listaPosiciones = new List<int>();
                    var result2 = ServCont.GetRequisitionsApprobationService().GetRequisitionsApprobationByRequisitionID(id);
                    foreach (var item in result2)
                    {
                        DA.Optimus.SYUser d = ServCont.GetUnitOfWork().User.Find(x => x.UserID == item.UserID).FirstOrDefault();
                        if (d != null)
                        {
                            var em = ServCont.GetUnitOfWork().Employees.Find(x => x.EmployeeId == d.EmployeeID).FirstOrDefault();
                            if (em != null)
                            {
                                listaPosiciones.Add((int)em.PositionID);
                            }
                        }
                    }

                    DA.HRDepartment departamento = ServCont.GetDepartmentService().GetBy(ServCont.GetPersonnelRequisitionService().GetByID(int.Parse(Request.QueryString["ID"].ToString())).DepartmentID);
                    var result = ServCont.GetRequisitionWorkFlowService().GetWorkFlowByType((bool)departamento.IsProduction ? 1 : 2);

                    List<HRRequistionWorkFlow> result3 = new List<HRRequistionWorkFlow>();
                    Pasos.InnerHtml = "";
                    foreach (var item in result)
                    {
                        var posi = ServCont.GetPositionsService().GetPositionsByID((int)item.PositionID);

                        var validPosi = (from a in listaPosiciones
                                         where a == posi.PositionID
                                         select a).FirstOrDefault();

                        if (validPosi != 0)
                        {
                            Pasos.InnerHtml += "<div class=\"col-xs-12 col-md-4\">" + posi.PositionName + "</div>" +
                                "<div class=\"col-xs-12 col-md-8\"><font face=\"verdana\" color=\"green\"> APROBADA</font></div>";
                        }
                        else
                        {
                            Pasos.InnerHtml += "<div class=\"col-xs-12 col-md-4\">" + posi.PositionName + "</div>" +
                                "<div class=\"col-xs-12 col-md-8\"><font face=\"verdana\" color=\"blue\"> PENDIENTE...</font></div>";
                            result3.Add(item);
                        }
                    }

                    var d1 = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name);
                    if (d1 != null)
                    {
                        var em1 = ServCont.GetUnitOfWork().Employees.Find(x => x.EmployeeId == d1.EmployeeID).FirstOrDefault();
                        if (em1 != null)
                        {
                            bttnAprovacion.Enabled = (result3.Count > 0 && result3.First().PositionID == em1.PositionID);
                        }
                    }
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void ObtenerHorario()
        {
            List<VwTurnosJornada> listaJornadas = ServCont.GetPersonnelRequisitionService().GetAllJornadas();
            listaJornadas = (from a in listaJornadas
                             where a.Jornada == lblHorario.Text
                             select a).ToList();
            foreach (VwTurnosJornada item in listaJornadas)
            {
                lblHorario.Text +=
                    string.Format(" {0}  {1}", item.DiaSemana, item.Turno);
            }
        }

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
            }
            return id;
        }

        #endregion Private Methods
    }
}