﻿using Optimus.Web.WebForms.DataHelpers;
using System;
using System.Collections.Generic;

namespace Optimus.Web.WebForms.HHRR
{
    public partial class AdministrarAnalisisPuesto : System.Web.UI.Page
    {

        #region Private Fields

        private const string LINK_DETALLES_PUESTO = "~/HHRR/DetallePuesto?ID={0}";
        private const string LINK_LISTA_PUESTO = "~/HHRR/ListaPuesto";
        private const string URL_PARAMETER = "ID";

        private DA.HRPositionAnalisysDetail Detalle;

        private BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Protected Methods

        protected void btnCancel_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Response.Redirect(string.Format(LINK_LISTA_PUESTO, ObtenerID().ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void btnDelete_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Detalle = ServCont.GetPositionsService().GetPositionDetailBy(txtFuncion.Text, ObtenerID());
                if (Detalle != null) ServCont.GetPositionsService().Delete(Detalle);
                Response.Redirect(string.Format(LINK_DETALLES_PUESTO, ObtenerID().ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                int id = ObtenerID();
                DA.HRPositionAnalisy analisis = ServCont.GetPositionsService().GetPositionsAnalisyBYPositionID(id);

                if (analisis == null)
                {
                    analisis = new DA.HRPositionAnalisy();
                    analisis.PositionAnalysisID = 0;
                    analisis.PositionID = id;
                    analisis.UserID = ServCont.GetUnitOfWork().User.GetUserByUserName(User.Identity.Name).userID;
                    ServCont.GetPositionsService().Save(analisis);
                }
                Detalle = ServCont.GetPositionsService().GetPositionDetailBy(txtFuncion.Text, id);
                if (Detalle == null)
                {
                    Detalle = new DA.HRPositionAnalisysDetail();
                    Detalle.DetailsID = 0;
                    Detalle.PositionAnalisysID = ServCont.GetPositionsService().GetPositionsAnalisyBYPositionID(id).PositionAnalysisID;
                }

                Detalle.Funtion = txtFuncion.Text;
                Detalle.WhatDo = txtQueHace.Text;
                Detalle.HowDo = txtComoHace.Text;
                Detalle.WhyDo = txtParaHace.Text;
                Detalle.TimeDo = txtQueTiempo.Text;

                ServCont.GetPositionsService().Save(Detalle);

                Response.Redirect(string.Format(LINK_DETALLES_PUESTO, id.ToString()));
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                if (!IsPostBack)
                {
                    DA.HRPosition po = ServCont.GetPositionService().GetPositionByID(ObtenerID());
                    lblPuestoName.Text = po.PositionName;
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        protected void TextBox1_TextChanged(object sender, EventArgs e)
        {
            List<string> validaciones = new List<string>();
            try
            {
                Detalle = ServCont.GetPositionsService().GetPositionDetailBy(txtFuncion.Text, ObtenerID());
                if (Detalle != null)
                {
                    txtFuncion.Text = Detalle.Funtion;
                    txtComoHace.Text = Detalle.HowDo;
                    txtParaHace.Text = Detalle.WhyDo;
                    txtQueHace.Text = Detalle.WhatDo;
                    txtQueTiempo.Text = Detalle.TimeDo;
                    txtFuncion.Enabled = false;
                }
            }
            catch (Optimus.Core.Exceptions.ValidationException ex)
            {
                BootstrapMessage.ShowWarningMessage(validaciones, ex);
            }
            catch (Exception ex)
            {
                BootstrapMessage.ShowAlertMessage(ex);
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private int ObtenerID()
        {
            int id = 0;
            try
            {
                id = int.Parse(Request.QueryString[URL_PARAMETER].ToString());
            }
            catch (Exception ex)
            {
                string error = ex.Message;
                Response.Redirect(LINK_LISTA_PUESTO);
            }
            return id;
        }

        private void ShowAlertMessage(Exception ex)
        {
            List<string> errores = new List<string>();
            errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            BootstrapMessage.Attributes["class"] = "alert alert-danger";
        }

        private void ShowWarningMessage(List<string> validaciones, Core.Exceptions.ValidationException ex)
        {
            List<string> errores = new List<string>();
            if (validaciones.Count <= 0)
            {
                errores.Add("<b>" + ex.Message + "</b>");
            }
            else
            {
                errores = validaciones;
            }
            BootstrapMessage.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            BootstrapMessage.Attributes["class"] = "alert alert-warning";
        }

        #endregion Private Methods

    }
}