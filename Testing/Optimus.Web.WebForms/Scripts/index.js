(function() {
  ['primary', 'success', 'danger', 'warning', 'info'].forEach(function(type) {
    return avalon.define({
      $id: "range-" + type,
      value: Math.round(Math.random() * 100),
      setValue: function(e) {
        e.preventDefault();
        return this.value = this.value;
      }
    });
  });

}).call(this);