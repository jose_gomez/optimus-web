﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="JobApplicants.aspx.cs" Inherits="Optimus.Web.WebForms.CandidateBank.JobApplicants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }

        .auto-style2 {
            width: 121px;
        }

        .auto-style3 {
            width: 581px;
        }

        .auto-style4 {
            width: 581px;
            height: 42px;
        }

        .auto-style5 {
            height: 42px;
            text-align: right;
        }

        .auto-style6 {
            text-align: right;
        }

        .panel-body {
            background: oldlace;
            padding: 5px;
        }

        .panel {
            margin-bottom: 3px;
        }

        .entrevistacolor {
            color: green;
        }
    </style>
    <script>

        $(document).ready(function () {

            setPorcentStatus();
            $('#MainContent_Boolentrevisto').hide();
        });

        function setPorcentStatus() {
            $('.EntrevistaLabel').each(function () {
                var status = $(this).text();
                if (status.trim() == "Entrevistado") {
                    $(this).addClass('entrevistacolor');
                }
            });
        }
        $(function () {
            $('#MainContent_SalarioTextBox').maskMoney({ allowNegative: false, thousands: ',', decimal: '.', affixesStay: false });
            $('#contenido').find('.candidate').each(function () {
                var elem = '<div><a href="" data-toggle="modal" data-target="#ModalEnviarContrato">Enviar Propuesta</a></div><div><a href=""   data-toggle="modal" data-target="#ModalContrato" >Enviar Contrato</a></div><div><a href="" onclick="Modal(' + "'" + $(this).find('.HidennCandidate').text() + "'" + ');" data-toggle="modal" data-target="#ModalSubirArchivo">Subir Curriculum</a></div>';
                $(this).find('.meddelanden').popover({ animation: true, content: elem, html: true });
            })

        });

        function Modal(cedula) {
            
            $('#MainContent_hiddencedula').attr("value", cedula);

        }

        function getCandidate(cedula) {
            Modal(cedula);
            $.ajax({
                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetCandidates',
                data: '{cedula:\'' + cedula + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data.d) {
                        $('#NombreLabel').text(data.d.FullName);
                        $('#PosicionEntrevista').text(data.d.Cedula);
                        $('#EstadoLabel').text(data.d.PosicionName);
                        if (data.d.Interviewed) {
                            $('#EntrevistadoLabel').text("Si")
                            $('#MainContent_Boolentrevisto').hide();
                        } else {
                            $('#EntrevistadoLabel').text("No")
                            $('#MainContent_Boolentrevisto').show();

                        }

                    }
                }
            });

            MostrarAlerta();

        }
        function MostrarAlerta() {
            $('#ModalDetalle').modal('show');
        }
    </script>
    <div id="container">
        <div id="cabecera">
            <h2>Banco de candidatos</h2>
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-3">
                                <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Cédula:</asp:Label>
                                <asp:TextBox runat="server" CssClass="form-control" ID="CedulaTextBox"  MaxLength="50" TabIndex="4"></asp:TextBox>
                            </div>
                            <div class="col-md-3">
                      
                                <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Posición:</asp:Label>
                                <asp:DropDownList runat="server" ID="PosicionFiltroDropDown" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="filtro"></asp:DropDownList>
                            </div>
                            <div class="col-md-3">
                                <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Area de Postulación:</asp:Label>
                                <asp:DropDownList runat="server" ID="CategoriaPosicion" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="filtro">
                                    <asp:ListItem Value="-1">Seleccione</asp:ListItem>
                                    <asp:ListItem Value="1">Administrativo</asp:ListItem>
                                    <asp:ListItem Value="2">Técnico</asp:ListItem>
                                    <asp:ListItem Value="3">Operario</asp:ListItem>
                                    <asp:ListItem Value="4">Seguridad</asp:ListItem>
                                </asp:DropDownList>

                            </div>
                            <div class="col-md-3">
                                <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Estudios:</asp:Label>
                                <asp:DropDownList runat="server" CssClass="form-control" ID="DropBoxEstudios" AutoPostBack="true" OnSelectedIndexChanged="filtro">
                                </asp:DropDownList>
                            </div>

                        </div>
                        <div class="row">
                             <div class="col-md-3">
                        <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Sexo:</asp:Label>
                        <asp:RadioButtonList ID="SexoRadioButton" runat="server" RepeatDirection="Horizontal" TabIndex="5" AutoPostBack="true" OnSelectedIndexChanged="filtro">
                            <asp:ListItem Value="0" Selected="true">Todos</asp:ListItem>
                            <asp:ListItem Value="M">Masculino</asp:ListItem>
                            <asp:ListItem Value="F">Femenino</asp:ListItem>
                        </asp:RadioButtonList>
                                 </div>
                             <div class="col-md-3">
                        <button type="button" class="btn btn-default btn-sm" runat="server" onserverclick="filtro">
                            <span class="glyphicon glyphicon-search"></span>Buscar
                        </button>
                                 </div>
                            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="contenido" id="contenido">
            <asp:Repeater runat="server" ID="candidatosRepeater">
                <ItemTemplate>
                    <div id="candidate" class="candidate">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <table class="auto-style1">
                                    <tr>
                                        <td class="auto-style2" rowspan="5">
                                            <img runat="server" width="100" height="100" id="ImageCan" src='<%#Eval("Picture64String")%>' />
                                        </td>
                                        <td class="auto-style4">
                                            <asp:Label ID="NombreLabel" runat="server" Text='<%#Eval("FullName")%>' Font-Size="Large"></asp:Label>
                                        </td>
                                        <td class="auto-style5">
                                            <asp:LinkButton ID="LinkButton1" runat="server" CommandArgument='<%#Eval("Cedula")%>' OnCommand="Curriculum">Ver Curriculun</asp:LinkButton>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style3">
                                            <asp:Label ID="CedulaLabel" Font-Bold="true" runat="server" Text='<%#Eval("Cedula") %>'></asp:Label>
                                        </td>
                                        <td class="auto-style6">
                                            <asp:LinkButton ID="LinkButton2" runat="server" OnCommand="Redirect" CommandArgument='<%#Eval("Cedula")%>'>Ver Solicitud</asp:LinkButton>

                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style3">
                                            <asp:Label ID="FechaNacimientoLabel" Font-Bold="true" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                        </td>
                                        <td class="auto-style6">
                                            <%--<asp:LinkButton ID="LinkButton3"  OnClientClick="getCandidate('<%#Eval("Cedula")%>')" runat="server" >Detalles</asp:LinkButton>--%>
                                           <%-- <a href="#" id="medde" onclick="getCandidate('<%#Eval("Cedula")%>')">Detalle</a>--%>
                                      <asp:LinkButton runat="server" Text='Detalle' PostBackUrl='<%# String.Format("~/HHRR/DetalleCandidato?ID={0}", Eval("CandidateID")) %>'></asp:LinkButton></td>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style3">
                                            <asp:Label ID="EntrevistaLabel" CssClass="EntrevistaLabel" runat="server" Font-Bold="true" Text='<%#((bool)Eval("Interviewed")) ?"Entrevistado":"sin entrevistar"%>'></asp:Label>
                                        </td>
                                        <td class="auto-style6">
                                            <%--<asp:HiddenField ID="HidennCandidate" Class="HidennCandidate" runat="server" Value='<%#Eval("Cedula")%>' />--%>
                                            <label runat="server" style="display: none" class="HidennCandidate"><%#Eval("Cedula")%></label>
                                            <a href="#" id="meddelanden" class="accion meddelanden" onclick="Modal('<%#Eval("Cedula")%>')"
                                                data-title="Acciones" data-toggle="clickover" data-placement="top">
                                                <i class="glyphicon glyphicon-menu-hamburger"></i>Acción</a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="auto-style3">&nbsp;</td>
                                        <td>&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
        <div id="ModalEnviarContrato" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Envio de Propuesta</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Propuesta:</label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="SalarioTextBox"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Posición:</label>
                            <asp:DropDownList runat="server" ID="PosicionDropDownList"></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Incluir beneficios:</label>
                            <asp:CheckBoxList ID="BeneficiosCheckBox" runat="server">
                                <asp:ListItem Value="5">Flota telefonica</asp:ListItem>
                                <asp:ListItem Value="6">Incentivo Salarial</asp:ListItem>
                            </asp:CheckBoxList>
                            <asp:HiddenField runat="server" ID="hiddencorreo" />
                        </div>
                        <%--<p class="text-warning"><small>Mantenga presionada la tecla control para seleccionar varios usuarios</small></p>--%>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <asp:Button runat="server" class="btn btn-primary" Text="Enviar propuesta" OnClick="PDFCreating" />
                    </div>
                </div>
            </div>
        </div>
        <div id="ModalSubirArchivo" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Subir Curriculum</h4>
                    </div>
                    <div class="modal-body">
                        <asp:HiddenField runat="server" ID="hiddencedula" />
                        <input type="file" runat="server" id="FileUpload1" accept=".pdf" />
                        <asp:HiddenField runat="server" ID="ArchivoPDF" />
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <asp:Button runat="server" class="btn btn-primary" Text="Guardar" OnClick="GuardarCurriculum" />
                    </div>
                </div>
            </div>
        </div>
        <div id="ModalContrato" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Enviar Contrato</h4>
                    </div>
                    <div class="modal-body">
                        <p class="text-warning">Seguro que quiere enviar el contrato</p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <asp:Button runat="server" class="btn btn-primary" Text="Enviar" OnClick="enviarContrato" />
                    </div>
                </div>
            </div>
        </div>
        <div id="ModalDetalle" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Detalle  Candidato</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="recipient-name" class="control-label"></label>
                            <label id="NombreLabel" style="font-size: large"></label>

                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Entrevistado:</label>
                            <label style="color: forestgreen" id="EntrevistadoLabel"></label>
                        </div>
                        <div class="form-group">
                            <asp:CheckBox ID="Boolentrevisto" runat="server" Text="se entevisto" />
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Estado candidato:</label>
                            <label id="EstadoLabel" style="font-style: normal; font-weight: normal"></label>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Actualizar Estado:</label>
                            <asp:DropDownList runat="server" ID="estadoDropboxList" CssClass="form-control" ></asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Posición se entrevisto:</label>
                            <label id="PosicionEntrevista"></label>
                        </div>
                        <div class="form-group">
                            <label for="recipient-name" class="control-label">Departamento:</label>
                            <asp:DropDownList runat="server" CssClass="form-control"  ID="deparmentDropDownDown"></asp:DropDownList>
                        </div>
                           <div class="form-group">                           
                            <asp:Button runat="server" ID="contratarButton" class="btn btn-default" Text="Contratar" OnClick="ExportarCandidato" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                        <asp:Button runat="server" class="btn btn-primary" Text="Guardar" OnClick="ActualizarCandidato" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>