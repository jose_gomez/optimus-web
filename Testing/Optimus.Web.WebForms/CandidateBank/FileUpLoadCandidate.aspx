﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="FileUpLoadCandidate.aspx.cs" Inherits="Optimus.Web.WebForms.CandidateBank.FileUpLoadCandidate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        #image{
            border:1px;          
        }
  </style>
    <script>
        ///Imagen Uno
        $(function () {
            $('#FileUpload1').change(function () {  
                var reader = new FileReader();
                reader.onload = function (e) {                 
                    $('#MainContent_ArchivoPDF').attr("value", e.target.result);
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
        });
    </script>
    <div class="container">
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-6">
                                <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Cédula:</asp:Label>
                                <asp:TextBox runat="server" CssClass="form-control col-md-4" ID="CedulaTextBox" Width="60%" MaxLength="50" TabIndex="4"></asp:TextBox>
                                <button type="button" class="btn btn-default btn-sm" runat="server" onserverclick="Buscar">
                                    <span class="glyphicon glyphicon-search"></span>Buscar
                                </button>
                            </div>
                        </div>
                    </div>
                    <br />
                    <br />
                    <br />
                    <div class="row">
                        <div class="col-md-6">
                            <div id="image">
                                <img runat="server" id="ImageCan" width="250" height="250" />
                            </div>
                        </div>
                    </div>
                    <div runat="server" id="divContent">
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Nombres:</asp:Label>
                                    <asp:Label runat="server" CssClass="form-control-label" ID="nombresLabel"></asp:Label>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Sexo:</asp:Label>
                                    <asp:Label runat="server" ID="SexoLabel" CssClass="form-control-label"></asp:Label>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Correo:</asp:Label>
                                    <asp:Label runat="server" ID="CorreoLabel" CssClass="form-control-label"></asp:Label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Télefono:</asp:Label>
                                    <asp:Label runat="server" CssClass="form-control-label" ID="TelefonoLabel"></asp:Label>
                                </div>
                                <div class="form-group">
                                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Celular:</asp:Label>
                                    <asp:Label runat="server" CssClass="form-control-label" ID="CelularLabel"></asp:Label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <asp:LinkButton runat="server" Text="Ver Curriculum" OnClick="Curriculum"></asp:LinkButton >
                                    <asp:LinkButton runat="server" Text="Ver Solicitud" OnClick="Redirect"></asp:LinkButton>
                                    
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <a href="#SubirCurriculum" id="cambioPorcentajeLink" runat="server" data-toggle="collapse">
                                        <i class="glyphicon glyphicon-asterisk"></i>Subir Curriculum</a>
                                    <div id="SubirCurriculum" class="collapse">
                                        <input type="file" runat="server" id="FileUpload1" accept=".pdf" />
                                        <asp:HiddenField runat="server" ID="ArchivoPDF" />
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <asp:Button runat="server" ID="boton" OnClick="Guardar" Text="Guardar Cambios" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
