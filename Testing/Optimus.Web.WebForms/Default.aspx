﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Optimus.Web.WebForms._Default" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=12.0.0.0, Culture=neutral, PublicKeyToken=89845dcd8080cc91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .sliderimage {
            width: 100%;
            height: 480px;
        }

        .headline {
            color: white;
        }

        .customCaption {
            background: rgba(0, 0, 0, 0.75) none repeat scroll 0px 0px;
            right: 0%;
            left: 0%;
            padding-top: 0%;
            padding-bottom: 0%;
        }

        div .ButtomTran {
            text-align: center;
            vertical-align: middle;
            width: 90px;
            height: 90px;

            -webkit-transition: width 1s, height 1s;
            transition: width 1s, height 1s;
        }

            div .ButtomTran:hover {
                width: 100px;
                height: 100px;
            }
    </style>
    <script>
        function setKnob() {
            $(".dial").knob({
                readOnly: true
            });
        }
        function setTicketPercentage() {
            try {
                $.each($(".dial"), function (index, value) {
                    this.value = this.value + '%';
                    this.style.fontSize = '15px';
                });
            }
            catch (e) {
                console.log(e);
            }
        }
        $(document).ready(function () {
            $('.carousel').carousel({
                interval: 5000
            });
            setKnob();
            setTicketPercentage();
            $('#my-table').dynatable();
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
    <div class="col-lg-12">
        <div id="myCarousel" class="carousel slide">
            <div class="carousel-inner">
                <div class="item active">
                    <img src="images/slider/slider1V2.jpg" class="sliderimage" alt="">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                        </div>
                    </div>
                </div>

                <div class="item">
                    <img src="images/slider/slider4V2.jpg" class="sliderimage" alt="banner1">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                        </div>
                    </div>
                </div>

                <div class="item">
                    <img src="images/slider/slider7V2.jpg" class="sliderimage" alt="banner2">
                    <div class="container">
                        <div class="carousel-caption customCaption">
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">‹</a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">›</a>
        </div>
    </div>
    <div class="col-lg-12">
        <div class="panel" style="pointer-events: none">
            <div class="col-lg-6">
                <input type="text" data-readonly="true" id="KnobAsistencia" data-fgcolor="#0066ff" runat="server" value="0" data-width="100" data-height="100" class="dial">
                <label>Asistencia:</label>
                <asp:Label runat="server" Font-Bold="true" Font-Size="small" ID="AsistenciaLbl"></asp:Label>
            </div>
            <div class="col-lg-6" style="text-align:right; vertical-align:middle">
                <a href="https://es.wikipedia.org/wiki/C%C3%A1ncer_de_mama"><img src="images/2016-10-19.png" height="80px" width="80px" /></a>
            </div>
        </div>
    </div>
    <div class="panel col-xs-12 col-sm-12 col-md-2 col-lg-2">
        <div class="col-xs-4 col-sm-4 col-md-12">
            <div class="ButtomTran">
                <a href="/Extensions/Extensions.aspx" data-toggle="tooltip" title="Extensiones!!!">
                    <img src="/images/Icon/PhoneB.ico" class="img-rounded" alt="Pulpit Rock" style="width: 100%; height: 100%">
                </a>
            </div>
        </div>

        <div class="col-xs-4 col-sm-4 col-md-12">
            <div class="ButtomTran" data-toggle="tooltip" title="Correo!!!">
                <a target="_blank" href="https://mail.google.com/a/richlinegroup.com">
                    <img src="/images/Icon/CorreoG.png" alt="Cinque Terre" class="img-rounded" style="width: 100%; height: 100%">
                </a>
            </div>
        </div>

        <div class="col-xs-4 col-sm-4 col-md-12">
            <div class="ButtomTran" data-toggle="tooltip" title="Solicitud de empleo!!!">
                <a target="_blank" href="/petition/Solicitudes.aspx">
                    <img src="/images/Icon/SolicitudIco.png" alt="Cinque Terre" class="img-rounded" style="width: 100%; height: 100%">
                </a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
        <div id="Cumpleanos" class="panel panel-info">
            <div class="panel-heading">
                <h4>Cumpleaños del día de hoy</h4>
            </div>
            <div class="panel-body">
                <asp:Repeater runat="server" ID="EmployeeBirthDateRepeater">
                    <ItemTemplate>
                        <div class="col-xs-6 col-sm-4 col-md-3" style="text-align: center; height: 250px">
                            <%--<div class="thumbnail" style="width: 200px; max-height: 275px;">--%>
                            <img runat="server" width="100" height="100" id="ImageCan" src='<%#Eval("PictureBase64")%>' />
                            <%--<div class="caption">--%>
                            <h4><%#Eval("FullName")%></h4>
                            <p><%#Eval("Departamento")%></p>
                            <%--</div>--%>
                            <%-- </div>--%>
                        </div>
                    </ItemTemplate>
                </asp:Repeater>
            </div>
        </div>
        <div id="CumpleanosMes" class="panel panel-default">
            <div class="panel-heading">
                <asp:Label runat="server" ID="HeaderMesLabel" Font-Size="Large"></asp:Label>
            </div>
            <div class="panel-body">
                <asp:GridView runat="server" ID="BirthDaysgridView" AutoGenerateColumns="false" AllowPaging="true" PageSize="20"
                    GridLines="none"
                    CellSpacing="-1"
                    CssClass="table table-stripe table-bordered table-hover"
                    OnPageIndexChanging="BirthDaysgridView_PageIndexChanging">
                    <Columns>
                        <asp:TemplateField ItemStyle-HorizontalAlign="center">
                            <ItemTemplate>
                                <%# Container.DataItemIndex + 1 %>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField ItemStyle-HorizontalAlign="center">
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Día"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("DOB","{0:%d}") %>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Nombre"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("FullName")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                <asp:Label runat="server" Text="Departamento"></asp:Label>
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label runat="server" Text='<%#Eval("Departamento")%>'> </asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
</asp:Content>