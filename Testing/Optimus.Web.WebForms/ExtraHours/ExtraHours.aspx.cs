﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.ExtraHours
{
    public partial class ExtraHours : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        List<Employees> Emp = new List<Employees>();
        List<Employees> EmpE = new List<Employees>();
        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
           // deparmetDropdownList.Enabled = false;

            if (!IsPostBack)
            {
                FechaTextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");

                DisableControl(container.GetEmployeesExtraHoursListService().GetStateExtraHourListByDate(DateTime.Now));
                deparmetDropdownList.DataSource =container.GetDepartmentService().GetAllDepartments();
                deparmetDropdownList.DataTextField = "Name"; 
                deparmetDropdownList.DataValueField = "DepartmentID";
                deparmetDropdownList.DataBind();

                var dept = worker.dept.GetDepartmentsByUsername(Page.User.Identity.Name);
              //Emp = worker.Employees.GetEmployeeByDeparment(dept.DepartmenId);
                EmployeesdeparmetListView.DataSource = container.GetEmployeesExtraHoursListService().GetEmployeeNotExtraHours(dept.DepartmenId, DateTime.Now);
                EmployeesdeparmetListView.DataBind();

                EmployeesExtraListView.DataSource = EmpE;
                EmployeesExtraListView.DataBind();

                countEmpBadge.InnerText = container.GetEmployeesExtraHoursListService().GetCountEmployeeExtraHourByDateDepartment(GetdateTextBox(), dept.DepartmenId).ToString();

                deparmetDropdownList.SelectedValue = dept.DepartmenId.ToString();
            }
        }
        protected void AgregarLista(object sender, EventArgs e)
        {
            foreach (ListViewItem item in EmployeesExtraListView.Items)
            {
                EmpE.Add(new Employees { EmployeeId = int.Parse(((Label)item.FindControl("codigoLabel")).Text), FirstName = ((Label)item.FindControl("nombreLabel")).Text });
            }
            foreach (ListViewItem item in EmployeesdeparmetListView.Items)
            {          
                CheckBox Check = (CheckBox)item.FindControl("seleccionCheckBox");
                Boolean Comida = int.Parse(HoursEndDropdown.SelectedItem.Value)>= 20 ? true:false;
                if (Check.Checked)
                {
                    EmpE.Add(new Employees {EmployeeId = int.Parse(((Label)item.FindControl("codigoLabel")).Text),FirstName = ((Label)item.FindControl("nombreLabel")).Text,comida=Comida });
                }
                else
                {
                    Emp.Add(new Employees { EmployeeId = int.Parse(((Label)item.FindControl("codigoLabel")).Text), FirstName = ((Label)item.FindControl("nombreLabel")).Text });
                }
            }
            EmployeesExtraListView.DataSource = EmpE;
            EmployeesExtraListView.DataBind();

            EmployeesdeparmetListView.DataSource = Emp;
            EmployeesdeparmetListView.DataBind();
        }
        protected void EliminarLista(object sender,EventArgs e)
        {
            int HoraInt = int.Parse(HoursStartDropdown.SelectedItem.Value);
            int HoraEnd = int.Parse(HoursEndDropdown.SelectedItem.Value);
            List<EmployeesExtraHoursListDetails> EmpList = new List<EmployeesExtraHoursListDetails>();
            foreach (ListViewItem item in EmployeesExtraListView.Items)
            {
                CheckBox Check = (CheckBox)item.FindControl("seleccionCheckBox");
                if (Check.Checked)
                {
                    EmpList.Add(new EmployeesExtraHoursListDetails { EmployeeID = int.Parse(((Label)item.FindControl("codigoLabel")).Text)});
                }
            }
            container.GetEmployeesExtraHoursListService().DeleteEmpleyeeExtraHours(int.Parse(deparmetDropdownList.SelectedItem.Value), GetdateTextBox(), int.Parse(HoursStartDropdown.SelectedItem.Value), int.Parse(HoursEndDropdown.SelectedItem.Value), EmpList);

            bindEmployeeDepartment();
            bindEmployeeExtra();
          
        }
        protected void guardarLista(object sender,EventArgs e)
        {
            int HoraInt = int.Parse(HoursStartDropdown.SelectedItem.Value);
            int HoraEnd = int.Parse(HoursEndDropdown.SelectedItem.Value);

            if (HoraInt > HoraEnd)
            {
                AlertHelpers.ShowAlertMessage(this,"Error", "Debe la hora de inicio debe ser menor a la hora de salida");
            }
            else
            {
                EmployeesExtraHoursList ExtHour = new EmployeesExtraHoursList();
                string fecha = FechaTextBox.Text;
                int horaInt = int.Parse(HoursStartDropdown.SelectedItem.Value);
                int horaEnd = int.Parse(HoursEndDropdown.SelectedItem.Value);

                ExtHour.DepartmentID = int.Parse(deparmetDropdownList.SelectedItem.Value);
                ExtHour.DateSubmit = GetdateTextBox();
                ExtHour.HourStart = horaInt;
                ExtHour.HourEnd = horaEnd;
                ExtHour.StatusID = 33;
                ExtHour.UserID = (container.GetUnitOfWork().User.GetUserByUserName(Page.User.Identity.Name)).userID;
                int IDExtraHours = container.GetEmployeesExtraHoursListService().SavesList(ExtHour);

                List<EmployeesExtraHoursListDetails> ListExtHourDetails = new List<EmployeesExtraHoursListDetails>();
                foreach (ListViewItem item in EmployeesExtraListView.Items)
                {
                    EmployeesExtraHoursListDetails ExtHourDetails = new EmployeesExtraHoursListDetails();
                    ExtHourDetails.EmployeesExtraHoursListID = IDExtraHours;
                    ExtHourDetails.EmployeeID = int.Parse(((Label)item.FindControl("codigoLabel")).Text);
                    ExtHourDetails.Food = ((CheckBox)item.FindControl("ComidaCheckBox")).Checked;
                    ListExtHourDetails.Add(ExtHourDetails);
                }
                container.GetEmployeesExtraHoursListService().SavesListDetails(ListExtHourDetails);
                CrearTickets();
                AlertHelpers.ShowAlertMessage(this, "Exito", "Lista enviada correctamente");
            }
        }
        protected void cambioFecha(object sender,EventArgs e)
        {
            bindEmployeeDepartment();
            bindEmployeeExtra();
            DisableControl(container.GetEmployeesExtraHoursListService().GetStateExtraHourListByDate(GetdateTextBox()));
            var dept = worker.dept.GetDepartmentsByUsername(Page.User.Identity.Name);
            countEmpBadge.InnerText = container.GetEmployeesExtraHoursListService().GetCountEmployeeExtraHourByDateDepartment(GetdateTextBox(),dept.DepartmenId).ToString();
        }

        protected void cambioHora(object sender, EventArgs e)
        {
                bindEmployeeExtra();
                bindEmployeeDepartment();
        }

        protected void cambiarDepartment(object sender,EventArgs e)
        {
            bindEmployeeExtra();
            bindEmployeeDepartment();
        }

        private void bindEmployeeDepartment()
        {
            EmployeesdeparmetListView.DataSource = container.GetEmployeesExtraHoursListService().GetEmployeeNotExtraHours(int.Parse(deparmetDropdownList.SelectedItem.Value), GetdateTextBox());
            EmployeesdeparmetListView.DataBind();
        }

        private void bindEmployeeExtra()
        {
            int HoraInt = int.Parse(HoursStartDropdown.SelectedItem.Value);
            int HoraEnd = int.Parse(HoursEndDropdown.SelectedItem.Value);

            EmployeesExtraListView.DataSource = container.GetEmployeesExtraHoursListService().GetEmployeeHasExtraHours(int.Parse(deparmetDropdownList.SelectedItem.Value), GetdateTextBox(), HoraInt, HoraEnd);
            EmployeesExtraListView.DataBind();
        }

        private void CrearTickets()
        {
            string titulo = "Listado de Hora Extras a la Fecha: " +FechaTextBox.Text;
            var user = worker.User.GetUserByUserName(User.Identity.Name);
            var dept = worker.dept.GetDepartmentsByUsername(User.Identity.Name);

            var Ticket = worker.TSTickets.Find(x=>x.Title == titulo).FirstOrDefault();
            if (Ticket == null)
            {
                worker.TSTickets.SubmitTicket("Listado de Hora Extras a la Fecha: " +FechaTextBox.Text, "Listado de Horas extras a la fecha: " + FechaTextBox.Text + " <a href='"+"http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/ListadoHorasExtra&Fecha="+ (GetdateTextBox()).ToString("dd-MM-yyyy") + "'>Mas Detalles</a> ", user.userID, 102, 2, false);
            }        
        }
        private void DisableControl(bool Disable)
        {
            EnviarButton.Enabled = !Disable;
            EliminarButton.Enabled = !Disable;
            AgregarButton.Enabled = !Disable;
            EstateHidden.Value = Disable.ToString();         
        }
        protected void Redirect(object sender, EventArgs e)
        {
            if (FechaTextBox.Text != string.Empty)
            {
                string a = (GetdateTextBox()).ToString("dd-MM-yyyy");
                Response.Redirect("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/ListadoHorasExtra&Fecha=" + a);
            }
            else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Notificación", "No hay  fecha seleccionada");
            }
        }
        private DateTime GetdateTextBox()
        {
            return DateTime.ParseExact(FechaTextBox.Text, "dd/MM/yyyy", new CultureInfo("es-MX"));
        }
    }
}