﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.ExtraHours
{
    public partial class ExtraHoursManager : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        bool Aproved = true;
        protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            ListadoHoraRepeter.DataSource =  container.GetEmployeesExtraHoursListService().GetExtraHourListAll();
            ListadoHoraRepeter.DataBind();
        }

        protected void ListadoHoraRepeter_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var user = worker.User.Find(x => x.UserName == Page.User.Identity.Name).FirstOrDefault();
            Aproved = container.GetEmployeesExtraHoursListService().AprovedList((int)user.EmployeeID);
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem;
                var index = e.Item.ItemIndex;
                ((LinkButton)e.Item.FindControl("AprobarButton")).Visible = Aproved;
                string a = ((Label)e.Item.FindControl("StatusLabel")).Text;
                if (a=="APPROVED")
                {
                    ((LinkButton)e.Item.FindControl("AprobarButton")).Enabled = false;
                    ((LinkButton)e.Item.FindControl("AprobarButton")).Text = "Aprobada";
                    ((LinkButton)e.Item.FindControl("AprobarButton")).ForeColor = Color.Green;
                }
            }
        }
        protected void Aprobar(object sender, CommandEventArgs e)
        {
            string date = e.CommandArgument.ToString();
            container.GetEmployeesExtraHoursListService().ActualizarEstadoExtraHours(date);
            string body = "Saludos <br/>";
            body += "listado de todo el personal a trabajar horas extras a la Fecha: " + date;
            body += "http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/ListadoHorasExtra&Fecha=" + date+"<br/>";
            container.GetAuditoryControlService().SendEmailByAlertTypeID(6,"Listado de Horas Extras "+date,body);
            Response.Redirect(Request.RawUrl);
        }
        protected void cambiarFecha(object sender, EventArgs e)
        {
            ListadoHoraRepeter.DataSource = container.GetEmployeesExtraHoursListService().GetExtraHourListAll(FechaBuscarTextBox.Text);
            ListadoHoraRepeter.DataBind();
        }
    }
}