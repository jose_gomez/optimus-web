﻿using System;
using System.Collections.Generic;
using Controls = System.Web.UI.WebControls;
using HtmlControls = System.Web.UI.HtmlControls.HtmlGenericControl;

namespace Optimus.Web.WebForms.DataHelpers
{
    public static class DataExtentionsMethods
    {

        #region Private Fields

        private const string BOOTSTRAP_ALERT_CLASS = "alert alert-danger";
        private const string BOOTSTRAP_WARNING_CLASS = "alert alert-warning";
        private const string CLASS_ATTRIBUTE = "class";
        private const string COMPETENCE_CATEGORY_ID = "CompetenceCategoryID";
        private const string COMPETENCE_CATEGORY_NAME = "Name";
        private const string COMPETENCE_ID = "CompetenceID";
        private const string COMPETENCE_NAME = "Name";
        private const string DEPARTMENT_ID = "DepartmentId";
        private const string DEPARTMENT_NAME = "Name";
        private const string ORGANIZATION_LEVEL_ID = "OrganizationLevelID";
        private const string ORGANIZATION_LEVEL_NAME = "Name";
        private const string POSITION_CATEGORY_ID = "PositionCategoryID";
        private const string POSITION_CATEGORY_NAME = "PostionCategory";
        private static BC.ServiceContainer ServCont = new BC.ServiceContainer();

        #endregion Private Fields

        #region Public Methods

        public static void SetAllCompetenceCategory(this Controls.DropDownList dropDownList)
        {
            dropDownList.DataSource = ServCont.GetPositionsService().GetAllCompetencesCategory();
            dropDownList.DataTextField = COMPETENCE_CATEGORY_NAME;
            dropDownList.DataValueField = COMPETENCE_CATEGORY_ID;
            dropDownList.DataBind();
        }

        public static void SetCompetencesByCategory(this Controls.DropDownList dropDownList, int categoryID)
        {
            List<DA.HRCompetence> competencias = ServCont.GetPositionsService().GetCompetenceByCategoryID(categoryID);
            dropDownList.DataSource = competencias;
            dropDownList.DataTextField = COMPETENCE_NAME;
            dropDownList.DataValueField = COMPETENCE_ID;
            dropDownList.DataBind();
        }

        public static void SetDepartments(this Controls.DropDownList dropDownList)
        {
            dropDownList.DataSource = ServCont.GetDepartmentService().GetAllDepartments();
            dropDownList.DataValueField = DEPARTMENT_ID;
            dropDownList.DataTextField = DEPARTMENT_NAME;
            dropDownList.DataBind();
        }

        public static void SetOrganizationLevels(this Controls.DropDownList dropDownList)
        {
            dropDownList.DataSource = ServCont.GetOrganizationLevelService().GetAllOrganizationLevels();
            dropDownList.DataValueField = ORGANIZATION_LEVEL_ID;
            dropDownList.DataTextField = ORGANIZATION_LEVEL_NAME;
            dropDownList.DataBind();
        }

        public static void SetPositionCategories(this Controls.DropDownList dropDownList)
        {
            dropDownList.DataSource = ServCont.GetPositionCategoryService().GetAllPositionCategories();
            dropDownList.DataValueField = POSITION_CATEGORY_ID;
            dropDownList.DataTextField = POSITION_CATEGORY_NAME;
            dropDownList.DataBind();
        }

        public static void ShowAlertMessage(this HtmlControls messageControl, Exception ex)
        {
            List<string> errores = new List<string>();
            errores.Add("<b>" + ex.Message + "</b>" + ex.StackTrace);

            messageControl.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            messageControl.Attributes[CLASS_ATTRIBUTE] = BOOTSTRAP_ALERT_CLASS;
        }

        public static void ShowWarningMessage(this HtmlControls messageControl, List<string> validaciones, Exception ex)
        {
            List<string> errores = new List<string>();
            if (validaciones.Count <= 0)
            {
                errores.Add("<b>" + ex.Message + "</b>");
            }
            else
            {
                errores = validaciones;
            }
            messageControl.InnerHtml = Core.WebMessagesHelper.MessagesHelperWb.GetHtmlForAlertMessage(errores);

            messageControl.Attributes[CLASS_ATTRIBUTE] = BOOTSTRAP_WARNING_CLASS;
        }

        #endregion Public Methods

    }
}