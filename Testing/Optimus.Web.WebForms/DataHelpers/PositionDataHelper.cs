﻿using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Optimus.Web.WebForms.DataHelpers
{
    public class PositionDataHelper
    {
        #region Private Fields

        private static HttpClient client = new HttpClient();

        #endregion Private Fields

        #region Public Methods

        public static DTO.Position GetPosition(string path)
        {
            DTO.Position position = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                position = DTO.Factory.PositionFactory.Deserialize(data);
            }
            return position;
        }

        public static List<DTO.Position> GetPositions(string path)
        {
            List<DTO.Position> positions = null;
            client = Core.APIHelper.OptimusAPIHttpClient.GetClient();
            HttpResponseMessage response = Task.Run(() => client.GetAsync(path)).Result;
            if (response.IsSuccessStatusCode)
            {
                string data = Task.Run(() => response.Content.ReadAsStringAsync()).Result;
                positions = DTO.Factory.PositionFactory.DeserializeList(data);
            }
            return positions;
        }

        public static async Task Save(DTO.Position position)
        {

        }
        #endregion Public Methods
    }
}