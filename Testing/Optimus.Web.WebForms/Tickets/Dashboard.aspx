﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.Dashboard" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server" class="container">
    <style>

        #statusbd{
                margin-left: -13px;
        }
        body {
            background-color: #fafafa;
            font-size: 16px;
            line-height: 1.5;
        }

        h1, h2, h3, h4, h5, h6 {
            font-weight: 400;
        }

        #header {
            border-bottom: 5px solid #37474F;
            color: #37474F;
            margin-bottom: 1.5rem;
            padding: 1rem 0;
        }

        #revenue-tag {
            font-weight: inherit !important;
            border-radius: 0px !important;
        }

        .card {
            border: 0rem;
            border-radius: 0rem;
        }

        .card-header {
            background-color: #37474F;
            border-radius: 0 !important;
            color: white;
            margin-bottom: 0;
            padding: 1rem;
        }

        .card-block {
            border: 1px solid #cccccc;
        }

        .shadow {
            box-shadow: 0 6px 10px 0 rgba(0, 0, 0, 0.14), 0 1px 18px 0 rgba(0, 0, 0, 0.12), 0 3px 5px -1px rgba(0, 0, 0, 0.2);
        }

        #revenue-column-chart, #products-revenue-pie-chart, #orders-spline-chart {
            height: 300px;
            width: 100%;
        }
        	#sales-doughnut-chart-us, #sales-doughnut-chart-nl, #sales-doughnut-chart-de, #responsetime {
				height: 280px;
				margin-top: 1rem;
				width: 100%;
			}
        .revenue-column-chart, #products-revenue-pie-chart, #orders-spline-chart {
            height: 300px;
            width: 100%;
        }
    </style>
    <script type="text/javascript">

        $(document).ready(
            function(){
                getDepartmentHead();
            }
            );

        function getTopPerformer() {
            var department = DeptsDDL.options[DeptsDDL.selectedIndex].text;
            $.ajax({
                type:'Post',
                url: '/Webservices/OptimusWebServices.asmx/GetTopPerformer',
                data: '{dept:\'' + department + '\'}',
                contentType: 'application/json; charset=utf-8',
                DataType:'json',
                success: function (data) {
                    if (data.d) {
                        $('#topperformer').empty();
                        $('#topperformer').append("<h5>  Name:  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                        $('#topperformer').append("<img alt='Top performer' id='imgPerf' />");
                        document.getElementById("imgPerf").src = "data:image/png;base64," + data.d.PictureBase64;
                    }
                }
            });
        }
        function getDepartmentHead() {
            var department = DeptsDDL.options[DeptsDDL.selectedIndex].text;
            $.ajax(
                {
                    type: 'Post',
                    DataType: 'json',
                    contentType: 'application/json; charset=utf-8',
                    url: '/webservices/OptimusWebServices.asmx/GetDepartmentHead',
                    data: '{Department:\'' + department + '\'}',
                    success: function (data) {
                        if (data.d) {
                            $('#empdetails').empty();
                            $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                            $('#empdetails').append("<img alt='Encargado' id='imgPic' />");
                            document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                            showLeaderBoard();
                            showStatusBreakdown();
                            GetAverageResponseTime();
                            getTopPerformer();
                        }
                        else {
                            $('#empdetails').empty();
                            $('#empdetails').append("<h5>  Encargado  <small class='text-muted'> N/A </small> </h5>");
                        }

                    },
                    error: function (e) {
                        console.log(e);
                    }
                }
                );
        }

        function GetAverageResponseTime() {
            var department = DeptsDDL.options[DeptsDDL.selectedIndex].text;
            $.ajax({

                type: 'Post',
                url: '/Webservices/OptimusWebServices.asmx/GetAvgResponsetime',
                data: '{dept:\'' + department + '\'}',
                contentType: 'application/json; charset=utf-8',
                success: function (data) {
                    if (data) {
                        showAvgTime(data.d);
                    }
                }

            });
        }

        function showAvgTime(avg) {
            var filler = 100 - avg;
            var responsetime = new CanvasJS.Chart("responsetime", {
                animationEnabled: true,
                backgroundColor: "transparent",
                title: {
                    fontColor: "#848484",
                    fontSize: 70,
                    horizontalAlign: "center",
                    text: parseInt(avg) + ' days',
                    verticalAlign: "center"
                },
                toolTip: {
                    backgroundColor: "#ffffff",
                    borderThickness: 0,
                    cornerRadius: 0,
                    fontColor: "#424242"
                },
                data: [
                    {
                        type: "doughnut",
                        dataPoints: [
                            { y: 100, toolTipContent: "Response Time in days: <span>" + parseInt(avg)+ "</span>" }
                        ]
                    }
                ]
            });

            responsetime.render();
        }
        function showStatusBreakdown() {
            var department = DeptsDDL.options[DeptsDDL.selectedIndex].text;
            $.ajax(
                {
                    type:'Post',
                    url: '/Webservices/OptimusWebServices.asmx/Getstatistics',
                    data: '{dept:\'' + department + '\'}',
                    DataType: 'json',
                    contentType:'application/json; charset=utf-8',
                    success: function (data) {
                        if (data.d) {
                            loadChart(data.d, breakdownPie,'', 'pie', '%', 360);
                        }
                    }
                }
                );
        }

        function loadChart(dataPoints, element, header, type, suffix) {
            var chart = new CanvasJS.Chart(element,
                {
                    theme: "theme2",
                    axisX: {
                        labelAngle: 0,
                        valueFormatString: '"#,##0.##%"'
                    },
                    title: {
                        text: header
                    },
                    data: [

                    {
                        type: type,
                        dataPoints: dataPoints
                    }
                    ],
                    suffix: suffix
                });

            chart.render();
        }
        function showLeaderBoard() {
            var dept = DeptsDDL.options[DeptsDDL.selectedIndex].text;
            $.ajax(
                {
                    type: 'Post',
                    url: '/Webservices/OptimusWebServices.asmx/GetAllTimeTicketsLeaderBoard',
                    data: '{dept: \'' + dept + '\'}',
                    contentType: 'application/json; charset=utf-8',
                    DataType: 'json',
                    success: function (data) {
                        if (data.d) {
                            loadChart(data.d, leaderboard, dept + ' tickets ranking', 'column', '');
                        }
                        else {
                            $(leaderboard).empty();
                        }

                    }
                }
                );
        }
    </script>
    <h2 id="header">
        <strong>Dashboard</strong>
        <small><% Response.Write(DateTime.Now.ToShortDateString()); %></small>
    </h2>
    <div class="row">
        <div class="col-md-5">
            <small>Seleccione un departamento:</small>
            <asp:DropDownList ClientIDMode="Static" ID="DeptsDDL" onchange="return getDepartmentHead()" runat="server"></asp:DropDownList>
        </div>

        <div class="col-md-5">
            <div id="empdetails">
            </div>
        </div>

    </div>
    <div class="row">
        <div class="card-shadow">
            <h4 class="card-header">Tickets Ranking
            </h4>
            <div class="card-block">
                <div id="revenue-column-chart">
                    <div id="leaderboard" class="canvasjs-chart-container" >
                    </div>
            </div>
            </div>
        </div>
    </div>
    <div class="row" style="margin-top:9%">
        <div id="statusbd" class="col-md-4">
                  <div class="card-shadow">
            <h4 class="card-header">Status breakdown
            </h4>
            <div class="card-block">
                <div class="revenue-column-chart">
                    <div id="breakdownPie" class="canvasjs-chart-container" >

                    </div>
            </div>
            </div>
        </div>
        </div>
           <div id="avgresponse" class="col-md-4">
                  <div class="card-shadow">
            <h4 class="card-header">Average response time
            </h4>
            <div class="card-block">
                <div class="revenue-column-chart">
                    <div id="responsetime" class="canvasjs-chart-container" >

                    </div>
            </div>
            </div>
        </div>
        </div>

              <div id="topperformance" class="col-md-4">
                  <div class="card-shadow">
            <h4 class="card-header">Top performer
            </h4>
            <div class="card-block">
                <div class="revenue-column-chart">
                    <div id="topperformer" class="canvasjs-chart-container" >

                    </div>
            </div>
            </div>
        </div>
        </div>
    
    </div>
    <script src="/Scripts/canvasjs.min.js"></script>
</asp:Content>
