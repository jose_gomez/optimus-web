﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA.Optimus;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Tickets
{
    public partial class TicketsDetalle : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        private Boolean isMgr = false;
        private Boolean EnableLimit = false;
        private Boolean EnableOwn = false;
        private Boolean isMgrSubmit = false;
        private Boolean ValidatedVisibilite = false;
        private Boolean Validated = false;
        private Boolean isRequition = false;
        private Boolean DepartmentUsers = false;
        private Boolean Approved = false;
        private int status;
        private DRL.Utilities.Mail.Mailer mail = new DRL.Utilities.Mail.Mailer();

        public TicketsDetalle()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }
        protected void Page_Init(object sender, EventArgs e)
        {
            
        }

        protected void GuardarCommentHijo(object sender, EventArgs e)
        {

        }


        protected void Page_Load(object sender, EventArgs e)
        {
            dropEstadosTickets.Enabled = false;
            cambioEstadoA.Visible = false;

            string id = Request.QueryString["ID"];
            var DetalleTicketes = worker.TSTickets.GetTicketsDetailByIdTickets(Convert.ToInt32(id));
            var Reposibles = worker.ResponsibleTickets.Find(x => x.TicketsID == Convert.ToInt32(id));
            var emp = worker.User.GetUserByUserName(this.Page.User.Identity.Name);

            Department dept;
            Employees head = null;
            Employees headSubmit = null;

            dept = worker.dept.GetDepartmentsByUsername(this.Page.User.Identity.Name);
            head = worker.DepartmentHeads.GetDepartmentHead(DetalleTicketes.FirstOrDefault().Department);
            headSubmit = worker.DepartmentHeads.GetDepartmentHead(DetalleTicketes.FirstOrDefault().DepartamentAdjudico);
            

            var EmpDept = worker.DepartmentHeads.GetEmployesByDeparmentNoAssing(int.Parse(id), DetalleTicketes.FirstOrDefault().IdDepartment);
            
            status = DetalleTicketes.FirstOrDefault().StatusID;
            // isMgr = head != null ? emp.EmployeeID == head.EmployeeId : false;
            // isMgrSubmit = headSubmit != null ? headSubmit.EmployeeId == emp.EmployeeID : false;

            isMgr = worker.DepartmentHeads.IsManagerByUserDepartment(emp.userName,DetalleTicketes.FirstOrDefault().IdDepartment);
            isMgrSubmit = worker.DepartmentHeads.IsManagerByUserDepartment(emp.userName, DetalleTicketes.FirstOrDefault().DeparmentIdSubmit);

            EnableOwn = DetalleTicketes.FirstOrDefault().SubmitUserName.ToLower().Trim() == User.Identity.Name.ToLower();
            Validated = (Boolean)DetalleTicketes.FirstOrDefault().validated;
            ValidatedVisibilite = ((EnableOwn) && DetalleTicketes.FirstOrDefault().PorcentProgress == 100) || DetalleTicketes.FirstOrDefault().validated == true;
            EnableLimit = worker.TSTickets.GetResponsibles(Convert.ToInt32(id)).FirstOrDefault(x => x.EmployeesIDResponsible == emp.EmployeeID) != null;

            DepartmentUsers = DetalleTicketes.FirstOrDefault().IdDepartment == dept.DepartmenId;
     
            isRequition = DetalleTicketes.FirstOrDefault().Requisition;
            Approved = DetalleTicketes.FirstOrDefault().Approved;

            if (Page.Request.Params["__EVENTTARGET"] == "GuardarComment")
            {
                string dat = Page.Request.Params["__EVENTARGUMENT"].ToString();
                guardarComment(int.Parse(lblTickets.Text), dat);
            }

            if (Page.Request.Params["__EVENTTARGET"] == "GuardarCommentHijo")
            {
                string dat = Page.Request.Params["__EVENTARGUMENT"].ToString();
                guardarTickets(dat);
            }

            CargaDatosTickest(DetalleTicketes);
            CargarRepeaters(id);
            setVisibilityOfElement(isMgr, EnableOwn, EnableLimit, status, ValidatedVisibilite,Validated, DepartmentUsers,isMgrSubmit,isRequition,Approved);

            if (!IsPostBack)
            {
                CargarDatosDropDownEstados(isMgr);
                CargarDatosDropDownListaUsuarios(EmpDept, isMgr);
                CargarDatosDropDownPriority();
                CargarDatosDropDownDeparment();
            }

            if (DetalleTicketes.FirstOrDefault().FatherTickets == null || DetalleTicketes.FirstOrDefault().FatherTickets  < 0)
            {
                IrTicketPadreLink.Visible = false;
            }
        }

        private void CargaDatosTickest(List<Tickestes> DetalleTicketes)
        {
            lblTitulo.Text = DetalleTicketes.FirstOrDefault().Title;
            lblTickets.Text = DetalleTicketes.FirstOrDefault().TicketNumber.ToString();
            lblFecha.Text = string.Format("{0:dd/MM/yyyy H:mm:ss}", DetalleTicketes.FirstOrDefault().SubmitDate);
            lblCreadoPor.Text = DetalleTicketes.FirstOrDefault().SubmitUserName;
            lblDeptAsignado.Text = DetalleTicketes.FirstOrDefault().Department;
            lblDeptAjudico.Text = DetalleTicketes.FirstOrDefault().DepartamentAdjudico;
            lblEstado.Text = DetalleTicketes.FirstOrDefault().Status;
            divProgreso.Style["width"] = DetalleTicketes.FirstOrDefault().PorcentProgress.ToString() + "%";
            divProgreso.InnerText = DetalleTicketes.FirstOrDefault().PorcentProgress.ToString() + "%";
            ValidateButton.CommandArgument = DetalleTicketes.FirstOrDefault().validated.ToString();
            PDescripcion.InnerHtml = DetalleTicketes.FirstOrDefault().Description;
            PriodidadLabel.Text = DetalleTicketes.FirstOrDefault().Priority;
            if (DetalleTicketes.FirstOrDefault().FatherTickets > 0)
            {
                PadreHidden.Value =DetalleTicketes.FirstOrDefault().FatherTickets.ToString()   ;
            }
            if (!IsPostBack)
            {
                if (DetalleTicketes.FirstOrDefault().PorcentProgress.ToString() != "0")
                {
                    PorcentajeRadioButtonList.SelectedValue = DetalleTicketes.FirstOrDefault().PorcentProgress.ToString();
                }
            }       
        }

        private void CargarRepeaters(string id)
        {
            repetar1.DataSource = worker.TSTickets.GetResponsibles(Convert.ToInt32(id));
            repetar1.DataBind();

            var comentarios = worker.TSTickets.GetCommentByTicketsID(Convert.ToInt32(id));
            CommentRepeater.DataSource = comentarios;
            CommentRepeater.DataBind();

            var Historial = worker.TSTickets.GetLogByTicketsID(Convert.ToInt32(id));
            HistorialRepeater.DataSource = Historial;
            HistorialRepeater.DataBind();
        }

        private void CargarDatosDropDownListaUsuarios(List<Employees> Emp,bool isMng)
        {
            int id = (int)worker.User.Find(x => x.UserName == Page.User.Identity.Name).FirstOrDefault().EmployeeID;
            if (isMng)
            {
               ListUsuariosDepartamento.DataSource = Emp;
            }else
            {
                ListUsuariosDepartamento.DataSource = worker.TSTickets.getLogInEmployees(Page.User.Identity.Name);
            }           
            ListUsuariosDepartamento.DataTextField = "FullName";
            ListUsuariosDepartamento.DataValueField = "EmployeeId";
            ListUsuariosDepartamento.DataBind();
        }

        private void CargarDatosDropDownEstados(bool isMgr)
        {
            List<TSTicketState> estados = new List<TSTicketState>();
            if (isMgr)
            {
                estados = worker.TicketState.GetAllTicketsState();
            }
            else if(isMgr && isRequition)
            {
                estados = worker.TicketState.GetTicketsStateToRequiquition();
            }
            else if (isRequition)
            {
                estados = worker.TicketState.GetTicketsStateToRequiquitionLimit();
            }
            else 
            {
                estados = worker.TicketState.GetTicketsStateToLimitUser();
            }
            dropEstadosTickets.DataSource = estados;
            dropEstadosTickets.DataTextField = "TicketState";
            dropEstadosTickets.DataValueField = "ID";
            dropEstadosTickets.DataBind();
        }
        private void CargarDatosDropDownPriority()
        {

            var priodidad  = worker.TSTickets.GetTicketsPriority();

            PriodidadDropDownList.DataSource = priodidad;
            PriodidadDropDownList.DataTextField = "Priority";
            PriodidadDropDownList.DataValueField = "TicketsPriorityID";
            PriodidadDropDownList.DataBind();


            PrioridadDropDownTicketHijo.DataSource = priodidad;
            PrioridadDropDownTicketHijo.DataTextField= "Priority";
            PrioridadDropDownTicketHijo.DataValueField = "TicketsPriorityID";
            PrioridadDropDownTicketHijo.DataBind();

        }

        private void CargarDatosDropDownDeparment()
        {
            var departmentService = container.GetDepartmentService();
            var departments = departmentService.GetAllDepartments();

            DeparmentDropDown.DataSource = departments;
            DeparmentDropDown.DataTextField = "Name";
            DeparmentDropDown.DataValueField = "DepartmentId";
            DeparmentDropDown.DataBind();
        }
        private void setVisibilityOfElement(Boolean isMgr, Boolean EnableOwn,Boolean LimitUser, int status, Boolean ValidatedVisibilite, Boolean Validated,Boolean DepartmentUsers,Boolean isMgrSubmit,Boolean isRequisition,Boolean Approved)
        {

            if ((status == 2 || status == 6 ) || (!DepartmentUsers && !EnableOwn && !isMgr && !isMgrSubmit && !LimitUser))
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;
                cambioUsuarioA.Visible = false;
                CancelarTicketsDiv.Visible = false;
            }
            else if (isRequisition && isMgr && Approved)
            {
                PorcentajeRadioButtonList.Enabled = true;
                dropEstadosTickets.Enabled = false;
                cambioEstadoA.Visible = false;
            }
            else if (isRequisition && Approved)
            {
                PorcentajeRadioButtonList.Enabled = true;
                dropEstadosTickets.Enabled = false;
                cambioEstadoA.Visible = false;
            }
            else if(isRequisition)
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;
                cambioUsuarioA.Visible = false;
                CancelarTicketsDiv.Visible = false;
            }
            else if (isMgr)
            {
                cambioUsuarioA.Visible = true;
            }
            else if (isMgrSubmit)
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;
                cambioUsuarioA.Visible = false;
            }
            else if (LimitUser)
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                CancelarTicketsDiv.Visible = true;
            }

            else if (EnableOwn && DepartmentUsers && !isMgr)
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;
                CancelarTicketsDiv.Visible = true;
            }
            else if(EnableOwn  && !isMgr)
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;
                cambioUsuarioA.Visible = false;
                CancelarTicketsDiv.Visible = true;
            }
           
            else if (DepartmentUsers)
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;               
                CancelarTicketsDiv.Visible = false;
                
            }else
            {
                cambioEstadoA.Visible = false;
                dropEstadosTickets.Enabled = false;
                PorcentajeRadioButtonList.Enabled = false;
                cambioUsuarioA.Visible = false;
                CancelarTicketsDiv.Visible = false;
            }

            ValidateButton.Visible = ValidatedVisibilite;

            //TODO:Codigo anterior
            //if ((!isMgr && !LimitUser) || (status == 6 || status == 2))
            //{
            //    cambioEstadoA.Visible = false;
            //    dropEstadosTickets.Enabled = false;
            //    PorcentajeRadioButtonList.Enabled = false;
            //    cambioUsuarioA.Visible = false;
            //    CancelarTicketsDiv.Visible = false;
            //}

            //ValidateButton.Visible = ValidatedVisibilite;

            //if (isMgr || isMgrSubmit || EnableOwn)
            //{
            //    if (!(status == 6 || status == 2))
            //    {
            //        CancelarTicketsDiv.Visible = true;

            //    }

            //}
            //if (!(status == 6 || status == 2))
            //{
            //    cambioUsuarioA.Visible = (DepartmentUsers || isMgr);
            //}
            if (Validated || status == 6)
            {
                ValidateButton.Text = "Validado";
                ValidateButton.CssClass = "btn btn-success btn-sm";
                ValidateButton.Enabled = false;
            }
            else
            {
                ValidateButton.Text = "Validar";
                ValidateButton.CssClass = "btn btn-primary btn-sm";
            }
         
        }
        public void EnviarEmailChanges(int IdTickets, string Subject, string body)
        {
            var Emails = worker.TSTickets.GetEmailOfTicketsByID(IdTickets);
            List<MailAddress> correo = new List<MailAddress>();
            foreach (var item in Emails)
            {
                MailAddress mail = new MailAddress(item.Email);
                correo.Add(mail);            
            }
            if (correo != null)
            {
                DRL.Utilities.Mail.Mailer.SendMail(Subject, correo, body, true);
            }
           
        }

        public void guardarComment(int IdTickets, string comentario)
        {
            if (comentario != string.Empty)
            {
                CommentTickets comment = new CommentTickets();
                comment.TicketsID = IdTickets;
                comment.UserID = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID;
                comment.Comentarios = comentario;
                comment.Create_dt = DateTime.Now;
                worker.comment.Add(comment);
                worker.Complete();
                EnviarEmailChanges(IdTickets, "Sistema de Tickets: Tickets# " + IdTickets + " Comentario:", comentario);
                Response.Redirect(Request.RawUrl, false);
            }
           
        }
        public void guardarLog(int IdTickets, string Log)
        {
            TSTicketsLog Logs = new TSTicketsLog();
            Logs.TicketsID = IdTickets;
            Logs.UserID = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID;
            Logs.TicketsLog = Log;
            Logs.create_dt = DateTime.Now;
            worker.LogTickets.Add(Logs);
            worker.Complete();

        }
        public void eliminar(int IdResponsible, int IdTickets)
        {
            TSTicketsResponsible responsible = new TSTicketsResponsible();
            responsible = worker.ResponsibleTickets.Find(x => x.Responsible == IdResponsible && x.TicketsID == IdTickets).FirstOrDefault();
            worker.ResponsibleTickets.Remove(responsible);
            var EmpDeleted = worker.Employees.Find(x => x.EmployeeId == IdResponsible).FirstOrDefault();
            string Name = EmpDeleted.FirstName + "." + EmpDeleted.LastName;
            worker.Complete();
           if (worker.ResponsibleTickets.Find(x=> x.TicketsID == IdTickets) == null)
            {
                var tickets = new TSTicket();
                tickets.TicketState = 4;
                worker.Complete();
            }
            guardarLog(IdTickets, "<b>" + Name + "</b> eliminado por el usuario " + User.Identity.Name);
            Response.Redirect(Request.RawUrl);
        }

        protected void EliminarResponsible(object sender, CommandEventArgs e)
        {
            string a = e.CommandName.ToString();
            string b = e.CommandArgument.ToString();
            eliminar(int.Parse(a), int.Parse(b));
        }

        protected void GuardarPorcentaje(object sender, EventArgs e)
        {
            int idi = Convert.ToInt32(lblTickets.Text);
            if (worker.TSTickets.GetResponsibles(idi).Count()  >= 1)
            {
                TSTicket tickest = new TSTicket();
                tickest = worker.TSTickets.Find(x => x.ID == idi).FirstOrDefault();
                tickest.PorcentProgress = Convert.ToInt32(PorcentajeRadioButtonList.SelectedItem.Value);
                if (Convert.ToInt32(PorcentajeRadioButtonList.SelectedItem.Value) == 100)
                {
                    tickest.TicketState = 5;
                    tickest.DoneDate = DateTime.Now;
                }
                else if (Convert.ToInt32(PorcentajeRadioButtonList.SelectedItem.Value) != 100 )
                {
                    tickest.TicketState = 7;
                }
                else if (Convert.ToInt32(PorcentajeRadioButtonList.SelectedItem.Value) != 100 && tickest.TicketState == 5)
                {
                    tickest.TicketState = 7;
                }
                worker.Complete();
                guardarLog(idi,"<b>"+Page.User.Identity.Name+"</b> cambio el porcentaje del progreso a "+ PorcentajeRadioButtonList.SelectedItem.Value);
                EnviarEmailChanges(idi, "Sistema de Tickets: Tickets# " + idi + " cambio de progreso", "Se cambio el progreso del ticket a " + PorcentajeRadioButtonList.SelectedItem.Value+"%");
                Response.Redirect(Request.RawUrl);
            }else
            {
                AlertHelpers.ShowAlertMessage(this.Page, "Notificación", "No Puede asignar porcentaje sin asignar a un usuario");
                Response.Redirect(Request.RawUrl);
            }
                     
        }

        protected void repeater_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                var data = e.Item.DataItem;
                var index = e.Item.ItemIndex;
                ((LinkButton)e.Item.FindControl("ResponsibleLink")).Enabled = isMgr && (status != 2 || status != 6);     
            }
        }
        protected void CancelarTickets(Object sender,EventArgs e)
        {
            int n = Convert.ToInt32(lblTickets.Text);
            TSTicket tickets = new TSTicket();
            tickets = worker.TSTickets.Find(x => x.ID == n).FirstOrDefault();
            tickets.TicketState = 2;
            tickets.CloseDate = DateTime.Now;
            worker.Complete();
            guardarLog(n,"Tickets cancelado por el usuario "+n+ User.Identity.Name);
            Response.Redirect(Request.RawUrl);

        }
        protected void ActualizarResponsable(object sender, EventArgs e)
        {
            int n = Convert.ToInt32(lblTickets.Text);

         
            TSTicketsResponsible Responsible = new TSTicketsResponsible();
            TSTicket tickets = new TSTicket();
            string user = string.Empty;
            var resp = worker.TSTickets.GetResponsibles(n);

            foreach (int item in ListUsuariosDepartamento.GetSelectedIndices())
            {               
                var HasResp = resp.Find(x => x.EmployeesIDResponsible == int.Parse(ListUsuariosDepartamento.Items[item].Value));
                if (HasResp == null)
                {
                    Responsible.TicketsID = n;
                    Responsible.Responsible = int.Parse(ListUsuariosDepartamento.Items[item].Value);
                    Responsible.Assigned = true;
                    Responsible.AssignDate = DateTime.Now;
                    worker.ResponsibleTickets.Add(Responsible);
                    worker.Complete();
                }
                
                user += ListUsuariosDepartamento.Items[item].Text + ", ";
            }
            tickets = worker.TSTickets.Find(x => x.ID == n).FirstOrDefault();
            if (resp.Count<1)
            {
                tickets.TicketState = 3;
            }
           
            worker.Complete();
            guardarLog(n, "Nueva asignación de usuario: <b>" + user + "</b> por <b>" + User.Identity.Name + "</b>");
            EnviarEmailChanges(n, " Sistema de Tickets: Actualización de Responsable  Tickets #"+n, "Nueva asignación de usuario: <b>" + user + "</b> por <b>" + User.Identity.Name + "</b>");
            Response.Redirect(Request.RawUrl);
        }

        protected void ActualizarEstado(object sender, EventArgs e)
        {
            int i = Convert.ToInt32(dropEstadosTickets.SelectedItem.Value);
            int n = Convert.ToInt32(lblTickets.Text);
            TSTicket ticket = new TSTicket();
            ticket = worker.TSTickets.Find(x => x.ID == n).FirstOrDefault();
            ticket.TicketState = i;
            if (i == 5 || i == 6)
            {
                ticket.PorcentProgress = 100;
            }
            else if (i == 7 && ticket.PorcentProgress == 100)
            {
                ticket.PorcentProgress = 50;
            }
            if(i != 5 && i!= 6)
            {
                ticket.DoneDate = null;
            }
            if (i == 5)
            {
                ticket.DoneDate = DateTime.Now;
            }
            if (i == 6)
            {
                ticket.CloseDate = DateTime.Now;
                ticket.ClosedBy = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID;
            }
            worker.Complete();
            guardarLog(n, "Actualización de estado a: <b>" + dropEstadosTickets.SelectedItem.Text + "</b>  realizado por " + User.Identity.Name);
            EnviarEmailChanges(n, "Sistema de Tickets:Actualización de estados Tickets"+n, "Actualización de estado a: <b>" + dropEstadosTickets.SelectedItem.Text + "</b>  realizado por " + User.Identity.Name);
            Response.Redirect(Request.RawUrl);
        }

        protected void ActualizarValidate(object sender, EventArgs e)
        {
            Button btn = (Button)sender;
            int n = Convert.ToInt32(lblTickets.Text);
            TSTicket ticket = new TSTicket();
            ticket = worker.TSTickets.Find(x => x.ID == n).FirstOrDefault();
            ticket.TicketState =6;
            ticket.CloseDate = DateTime.Now;
            ticket.ValidatedBy = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault().UserID;
            ticket.CloseDate = DateTime.Now;
            worker.Complete();         
            guardarLog(n,"Ticket no. "+n+" fue validado por el usuario: "+ User.Identity.Name);
            EnviarEmailChanges(n, "Sistema de Tickets:Validación de Tickets","Ticket no. "+n+" validado por el usuario " + User.Identity.Name);
            Response.Redirect(Request.RawUrl);
        }

        protected void ActualizarPriodidad(object sender,EventArgs e)
        {
            int n = Convert.ToInt32(lblTickets.Text);
            TSTicket ticket = new TSTicket();
            ticket = worker.TSTickets.Find(x => x.ID == n).FirstOrDefault();
            ticket.TicketsPriorityID = int.Parse(PriodidadDropDownList.SelectedItem.Value);
            worker.Complete();
            EnviarEmailChanges(n,"Sistema de Tickets: Validación de Tickets","Tickets no. "+n+" Priododad cambiada a "+PriodidadDropDownList.SelectedItem.Text+" por el usuario "+ User.Identity.Name);
            guardarLog(n,"Tickets no. " + n + " Priododad cambiada a " + PriodidadDropDownList.SelectedItem.Text + " por el usuario " + User.Identity.Name);
          //  EnviarEmailChanges(n,"Sistema de Tickets: Validación de Tickets","Tickets no. "+n+"Priododad cambiada a " +PriodidadDropDownList.SelectedItem.Text+" por el usuario "+ User.Identity.Name);
            Response.Redirect(Request.RawUrl);
        }

        private void guardarTickets(string Descripcion)
        {
            int oa = 0;
            TSTicket ticket = new TSTicket();
            ticket.Referral = Guid.NewGuid().ToString();
            ticket.Title = txtTitulo.Text;
            ticket.Description = Descripcion;
            ticket.DepartmentId = int.Parse(DeparmentDropDown.SelectedItem.Value);
            ticket.SubmitDate = DateTime.Now;
            var user = worker.User.Find(x => x.UserName == User.Identity.Name).FirstOrDefault();
            ticket.SubmitUser = user.UserID; //todo:reemplazar por id de usuario;
            ticket.TicketState = 4;
            ticket.PorcentProgress = 0;
            ticket.validated = false;
            ticket.TicketsPriorityID = int.Parse(PrioridadDropDownTicketHijo.SelectedItem.Value);
            ticket.FatherTickets = int.Parse(lblTickets.Text);
            if (int.TryParse(HiddenProblem.Value,out oa))
            {
                ticket.ProblemTypeID = int.Parse(HiddenProblem.Value);
            }
            ticket.DeparmentIdSubmit = (worker.dept.GetDepartmentsByUsername(this.Page.User.Identity.Name)).DepartmenId;
            ticket.Requisition = false;
            ticket.Approved = false;
            
            worker.TSTickets.Add(ticket);
            worker.Complete();
            TicketsDetalle td = new TicketsDetalle();
            guardarLog(ticket.ID, "Creación del Tickets No." + ticket.ID);


            //var Enc = worker.DepartmentHeads.GetDepartmentHead(DeparmentDropDown.SelectedItem.Text);
            //string Correo = worker.User.SingleOrDefault(x => x.EmployeeID == Enc.EmployeeId).Email;
            //string CorreoE = worker.User.SingleOrDefault(x => x.UserName == this.User.Identity.Name).Email;
            //List<MailAddress> correo = new List<MailAddress>();
            //MailAddress co = new MailAddress(Correo);
            //MailAddress coe = new MailAddress(CorreoE);
            //correo.Add(co);
            //correo.Add(coe);
            //if (correo != null)
            //{
            //    DRL.Utilities.Mail.Mailer.SendMail("Nuevo Tickets", correo, "Nuevo Tickets <br>" + txtTitulo.Text, true);
            //}

            EnviarEmailChanges(ticket.ID, "Nuevo Tickets Hijo", "Nuevo Tickets <br>" + txtTitulo.Text);
            AlertHelpers.ShowAlertMessage(this, "Ticket sometido", "Su ticket ha sido enviado. Puede revisar el estado en el tab 'Mis Tickets'");
            Response.Redirect(Request.RawUrl);
        }
        protected void IrTicketPadre(object sender ,EventArgs e)
        {
            if (PadreHidden.Value == null || PadreHidden.Value == string.Empty)
            {
                AlertHelpers.ShowAlertMessage(this, "Alerta!", "Este Tickets no tiene Tickets Padre");
            }else
            {
                Response.Redirect(String.Format("~/Tickets/TicketsDetalle.aspx?ID={0}", PadreHidden.Value));
            }
            
        }
    }
}