﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Maintenance.aspx.cs" Inherits="Optimus.Web.WebForms.Tickets.Maintenance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <script>
        $(document).ready(function () {
            $('#anchrdeptheads').click();
            document.querySelector('form').onkeypress = checkEnter;
            if (preventLettersOnKeyPress) {
                document.getElementById("txtcodEncargado").addEventListener("keydown", preventLettersOnKeyPress2);
            }
            
            getDepartmentHead();
        });

        function validateNewDeptHead() {
            if ($('#txtcodEncargado').val() == undefined || $('#txtcodEncargado').val() == "") {
                alert('Por favor digite un codigo de encargado');
                return false;
            }
        }
        function preventLettersOnKeyPress2(event) {
            if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode ==13) {
                if (event.keyCode == 13) {
                    getEmployeeDetails();
                    return false;
                }
            }
            else {
                if (event.keyCode > 95 && event.keyCode < 106) {

                    return;
                }
                if ((event.keyCode < 48 || event.keyCode > 57)) {
                    event.preventDefault();
                }
            }

        }



        function checkEnter(e) {
            e = e || event;
            var txtArea = /textarea/i.test((e.target || e.srcElement).tagName);
            return txtArea || (e.keyCode || e.which || e.charCode || 0) !== 13;
        }

        function getDepartmentHead() {
            $('#imgModal').modal();
            $.ajax({

                type: 'Post',
                url: '/WebServices/OptimusWebServices.asmx/GetDepartmentHead',
                data: '{Department:\'' + $('#MainContent_drpDepartamento option:selected').text() + '\'}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $('#empdetails').empty();
                    if (data.d) {
                       
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName + "</small> </h5>");
                        $('#empdetails').append("<img alt='Encargado' id='imgPic' />");

                        document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                    }
                    else {
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'> N/A </small> </h5>");
                    }
                    $('#imgModal').modal('hide');
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }

        function getEmployeeDetails() {
            $('#imgModal').modal();
            $.ajax({
       
                type:'Post', 
                url: '/WebServices/OptimusWebServices.asmx/GetEmployeeById',
                data: '{employeeId:' + $('#txtcodEncargado').val() + '}',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    if (data) {
                        $('#empdetails').empty();
                        $('#empdetails').append("<h5>  Encargado  <small class='text-muted'>" + data.d.FullName  + "</small> </h5>");
                        $('#empdetails').append("<img alt='Encargado' id='imgPic' />");
                       
                        document.getElementById("imgPic").src = "data:image/png;base64," + data.d.PictureBase64;
                        $('#imgModal').modal('hide');
                    }
                },
                error: function (e) {
                    $('#imgModal').modal('hide');
                    showModalMessage('Error obteniendo datos', 'Se ha producido un error indeterminado. Por favor contactar a it');
                    console.log(e);
                }

            });
        }
            
    </script>
    <style>
        @media only screen and (min-width:800px) {
            input[type=text] {
                min-width: 279px;
            }
        }

        #main {
            min-height: 300px;
            padding-top: 5%;
        }

        #departmentHeads {
            padding: 50px;
        }
    </style>
    <div class="container" id="main">


        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="collapsed"  id="anchrdeptheads" data-toggle="collapse" data-parent="#accordion" href="#departmentHeads">Encargados de departamento</a>
                </h4>
            </div>
            <div style="height: 0px;" id="departmentHeads" class="panel-collapse collapsed">
                <div class="panel-body">
                    Departamento:
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <asp:DropDownList ID="drpDepartamento" onchange="return getDepartmentHead()" runat="server"></asp:DropDownList>
                        </div>
                    </div>
                    Encargado o Coordinador:
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <asp:TextBox runat="server" ClientIDMode="Static" onChange="return getEmployeeDetails"  ID="txtcodEncargado" placeholder="Introduzca un codigo de empleado"></asp:TextBox>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <asp:CheckBox ID="EncargadoCheckBox" Text="Encargado" runat="server" />
                        </div>
                          <div class="col-sm-4 col-md-4" id="empdetails">          
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4 col-md-4">
                            <asp:Button class="btn btn-success" runat="server" Text="Guardar" OnClientClick="return validateNewDeptHead();" ID="btnGuardar" OnClick="btnGuardar_Click" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
  
</asp:Content>
