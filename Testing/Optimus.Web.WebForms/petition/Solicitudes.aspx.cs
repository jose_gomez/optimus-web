﻿using Optimus.Web.BC;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using Optimus.Web.WebForms.GuiHelpers;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.petition
{
    public partial class Solicitudes : System.Web.UI.Page
    {
        private ServiceContainer container;
        public int Identificador;
        public string Dias;

      protected void Page_Load(object sender, EventArgs e)
        {
            container = new ServiceContainer();
            if (Page.Request.Params["__EVENTTARGET"] == "DatosSubmit")
            {                                
                    try
                    {
                        Convert.ToDateTime(HiddenDate.Value);
                    }
                    catch (Exception)
                    {

                        AlertHelpers.ShowAlertMessage(this.Page, "Error", "Ingrese una fecha valida");
                    }

                guardarFile();

                Identificador = GuardarDatosPeronales();
                
                
                string dat = Page.Request.Params["__EVENTARGUMENT"];
                string[] Div = dat.Split('Æ');
                string[] her = Div[0].Split(',');
                string[] Exp = Div[1].Split('§');
                List<CandidateExperience> CandidateExpList = new List<CandidateExperience>();
                CandidateExperience CandidateExp;
                foreach (string item in Exp)
                {
                    if (item.Length > 8)
                    {
                        int @a = item.IndexOf("@") + 1;
                        int @b = item.IndexOf("#") + 1;
                        int @c = item.IndexOf("%") + 1;
                        int @d = item.IndexOf("^") + 1;
                        int @f = item.IndexOf("&") + 1;
                        int @g = item.IndexOf("$") + 1;
                        int @h = item.IndexOf("*") + 1;
                        int @i = item.IndexOf("?") + 1;

                        CandidateExp = new CandidateExperience();
                        CandidateExp.CandidateID = Identificador;
                        CandidateExp.Company = item.Substring(@a, @b - 5);
                        CandidateExp.WorkPosition = item.Substring(@b, (@c - 2) - @b);
                        CandidateExp.PerfomedTask = item.Substring(@c, (@d - 2) - @c);

                        string Ff = item.Substring(@d, (@f - 2) - @d);
                        string Ft = item.Substring(@f, (@g - 2) - @f);

                        CandidateExp.DateFrom = Convert.ToDateTime(DateTime.ParseExact(Ff, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                        CandidateExp.DateTo = Convert.ToDateTime(DateTime.ParseExact(Ft, "dd/MM/yyyy", CultureInfo.InvariantCulture).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture));
                        CandidateExp.Salary = Convert.ToDecimal(item.Substring(@g, (@h-2) - @g));
                        CandidateExp.ReasonOut = item.Substring(@h, (@i - 2) - @h);
                        CandidateExp.PhoneNumberCompany = item.Substring(@i,(item.Length -1)-@i);
                        CandidateExpList.Add(CandidateExp);
                    }
                }


                List<CandidateBrothers> CandidateBrotherList = new List<CandidateBrothers>();
                CandidateBrothers CandidatesBrother;
                foreach (string item in her)
                {
                    if (item != string.Empty)
                    {
                        CandidatesBrother = new CandidateBrothers();
                        CandidatesBrother.CandidateID = Identificador;
                        CandidatesBrother.Names = item;
                        CandidateBrotherList.Add(CandidatesBrother);
                    }

                }
                if (Identificador == 0)
                {
                    AlertHelpers.ShowAlertMessage(this.Page, "Error!", "ya se tiene una solicitud con ese número de cédula");
                }
                else
                {
                    GuardarExp(CandidateExpList);
                    GuardarHermanos(CandidateBrotherList);
                    Guardar();

                }
     
            }

            if (!IsPostBack)
            {
                cargarDropDownBox();
            }

        }
        private void cargarDropDownBox()
        {

            
            PosicionesDropDown.DataSource = container.GetPositionsService().GetPositionRequsition() ;
            PosicionesDropDown.DataTextField = "PositionName";
            PosicionesDropDown.DataValueField = "PositionID";
            PosicionesDropDown.DataBind();
            PosicionesDropDown.Items.Insert(0, new ListItem("<Seleccione una Posición>", "0"));          

            DropBoxNacionalidad.DataSource = container.GetNationalityService().GetAllNationality();
            DropBoxNacionalidad.DataTextField = "Name";
            DropBoxNacionalidad.DataValueField = "NationalityID";
            DropBoxNacionalidad.DataBind();

            DropBoxEstudios.DataSource = container.GetStudies().GetAllStudies();
            DropBoxEstudios.DataTextField = "Name";
            DropBoxEstudios.DataValueField = "StudiesID";
            DropBoxEstudios.DataBind();

            CategoriaPosicion.DataSource = container.getSolicitudesServices().GetPositionCategory();
            CategoriaPosicion.DataTextField = "PostionCategory";
            CategoriaPosicion.DataValueField = "PositionCategoryID";
            CategoriaPosicion.DataBind();


            var data = container.GetLanguagesServices().GetAllLanguages();

            List<DropDownList> dropDown = new List<DropDownList>();
            dropDown.Add(DropboxIdiomas1);
            dropDown.Add(DropboxIdiomas2);
            dropDown.Add(DropboxIdiomas3);
            dropDown.Add(DropboxIdiomas4);
            foreach (DropDownList item in dropDown)
            {
                item.DataSource = data;
                item.DataTextField = "Name";
                item.DataValueField = "LanguageID";
                item.DataBind();
                item.Items.Insert(0, new ListItem("<Seleccione un Idioma>", "0"));
            }
            var data1 = container.GetLanguagesServices().GetAllLanguagesLevel();
            List<DropDownList> dropDownLevel = new List<DropDownList>();
            dropDownLevel.Add(dropboxNivel1);
            dropDownLevel.Add(dropboxNivel2);
            dropDownLevel.Add(dropboxNivel3);
            dropDownLevel.Add(dropboxNivel4);
            foreach (DropDownList item in dropDownLevel)
            {
                item.DataSource = data1;
                item.DataTextField = "Level";
                item.DataValueField = "LevellanguageID";
                item.DataBind();
            }

        }
        private void Guardar()
        {
            if (Identificador != 0)
            {
                GuardarTitulo(Identificador);
                GuardarIdiomas(Identificador);
                GuardarReferencia(Identificador);
                guardarSkills(Identificador);
                GuardarFotos(Identificador);
                AlertHelpers.ShowAlertMessage(this.Page, "Exito!", "Su solicitud ha sido enviada");
            }
            Response.Redirect(Request.RawUrl);
        }
        private int GuardarDatosPeronales()
        {
           
            int outV = 0;
            Candidates candidato = new Candidates();

            if (int.TryParse(PosicionesDropDown.SelectedItem.Value, out outV))
            {
                candidato.PostulatePositionID = int.Parse(PosicionesDropDown.SelectedItem.Value);
            }
            candidato.FirstName = NombreTextBox.Text;
            candidato.LastName = ApellidoTextBox.Text;
            candidato.Streets = CalleTextBox.Text;
            candidato.HouseNumber = NoCasaTextBox.Text;
            candidato.Sector = SectorTextBox.Text;
            candidato.location = LocalidadTextBox.Text;
            candidato.Sex = SexoRadioButton.SelectedItem.Value.ToString();
            candidato.NationalityID = int.Parse(DropBoxNacionalidad.SelectedItem.Value);
            candidato.PhoneNumber = TelefonoTextBox.Text;
            candidato.MovilNumber = CelularTextBox.Text;
            candidato.Email = EmailTextBox.Text;
            candidato.Cedula = CedulaTextBox.Text;
            candidato.alias = ApodoTextBox.Text;
            candidato.BirthDate = DateTime.ParseExact(HiddenDate.Value, "dd/MM/yyyy", CultureInfo.InvariantCulture); //  Convert.ToDateTime(HiddenDate.Value).ToString("MM/dd/yyyy", CultureInfo.InvariantCulture);
            candidato.EmergencyNumber = TelEmergerciaTextBox.Text;
            candidato.EmergencyContactName = ContactoEmergerciaTextBox.Text;
            candidato.Size = SizeDropDown.SelectedItem.Value;
            candidato.Religion = ReligionDropDown.SelectedItem.Value;
            candidato.CivilStatus = int.Parse(EstadoCivilDropBox.SelectedItem.Value);
            candidato.Allergies = CheckboxAllergias.Checked;
            candidato.AllergiesType = AlergiasTextBox.Text;
            candidato.suffering = PadecimientoCheckBox.Checked;
            candidato.sufferingType = PadecimientoTextBox.Text;
            candidato.AvailableOverTime = checkboxExtras.Checked;
            candidato.AvailableWeekend = CheckboxFinesSemana.Checked;
            candidato.LivesWith = ViveConTextBox.Text;
            candidato.SpouseName = NombreConyugeTextBox.Text;
            candidato.SpouseActivity = ActividadConyugeTextBox.Text;
            candidato.MotherName = NombreMadreTextBox.Text;
            candidato.FatherName = NombrePadreTextBox.Text;
            candidato.DependentChildren = HijosCargoTextBox.Text == string.Empty ? 0 : int.Parse(HijosCargoTextBox.Text);
            candidato.DependentPeople = PersonasCargoTextBox.Text == string.Empty ? 0 : int.Parse(PersonasCargoTextBox.Text);
            candidato.StudiesID = int.Parse(DropBoxEstudios.SelectedItem.Value);
            candidato.ComputerStudies = CheckboxComputacion.Checked;
            candidato.IsStudent = estudiaCheckBox.Checked;
            foreach (ListItem item in CheckButtonDias.Items)
            {
                if (item.Selected)
                {
                    Dias += item.Text+",";
                }
            }
            candidato.DaysWeek = Dias;
            candidato.Schedule = HoriarioDropBox.SelectedItem.Text;
            candidato.ProgramsKnow = ProgramasTextBox.Text;
            candidato.WhosRecommended = RecomendoTextBox.Text;
            candidato.FamilyOrFriend = FamiliarEmpresa.Checked;
            candidato.FamilyOrFriendName = NombreParentescoTextBox.Text;
            candidato.FamilyOrFriendPuesto = PuestoTextBox.Text;
            candidato.FamilyOrFriendRelationship = ParentescoTextBox.Text;
            candidato.Interviewed = false;
            
                
             int CatPos =  int.TryParse(CategoriaPosicion.SelectedItem.Value, out outV) ? int.Parse(CategoriaPosicion.SelectedItem.Value) : 0;

            if (CatPos != 0)
            {
                candidato.PositionCategoryID = CatPos;
            }
            

            return container.getSolicitudesServices().Save(candidato);

        }
        private void GuardarExp(List<CandidateExperience> Exp)
        {
            container.getSolicitudesServices().Save(Exp);
        }
        private void GuardarHermanos(List<CandidateBrothers> CanBro)
        {
            container.getSolicitudesServices().Save(CanBro);
        }
        private void GuardarTitulo(int IDCand)
        {
            List<TextBox> titleBoxesList = new List<TextBox>();
            titleBoxesList.Add(TituloObtenidoTextBox1);
            titleBoxesList.Add(TituloObtenidoTextBox2);
            titleBoxesList.Add(TituloObtenidoTextBox3);
            titleBoxesList.Add(TituloObtenidoTextBox4);
            titleBoxesList.Add(TituloObtenidoTextBox5);
            List<CandidateTitle> CandidateTitleList = new List<CandidateTitle>();
            CandidateTitle CandidateTitle;

            foreach (TextBox item in titleBoxesList)
            {
                if (item.Text != string.Empty)
                {
                    CandidateTitle = new CandidateTitle();
                    CandidateTitle.CandidateID = IDCand;
                    CandidateTitle.Titles = item.Text;
                    CandidateTitleList.Add(CandidateTitle);
                }
            }
            container.getSolicitudesServices().Save(CandidateTitleList);
        }
        private void GuardarIdiomas(int IDCand)
        {
            List<CandidateLanguages> candidateLanguagesList = new List<CandidateLanguages>();
            CandidateLanguages candidateLanguages;
            List<IdiomasNiveles> IdioNivList = new List<IdiomasNiveles>();
            IdiomasNiveles IdioNiv1 = new IdiomasNiveles(int.Parse(DropboxIdiomas1.SelectedItem.Value), int.Parse(dropboxNivel1.SelectedItem.Value));
            IdiomasNiveles IdioNiv2 = new IdiomasNiveles(int.Parse(DropboxIdiomas2.SelectedItem.Value), int.Parse(dropboxNivel2.SelectedItem.Value));
            IdiomasNiveles IdioNiv3 = new IdiomasNiveles(int.Parse(DropboxIdiomas3.SelectedItem.Value), int.Parse(dropboxNivel3.SelectedItem.Value));
            IdiomasNiveles IdioNiv4 = new IdiomasNiveles(int.Parse(DropboxIdiomas4.SelectedItem.Value), int.Parse(dropboxNivel4.SelectedItem.Value));

            IdioNivList.Add(IdioNiv1);
            IdioNivList.Add(IdioNiv2);
            IdioNivList.Add(IdioNiv3);
            IdioNivList.Add(IdioNiv4);
            foreach (IdiomasNiveles item in IdioNivList)
            {
                if (item.LanguageID != 0)
                {
                    candidateLanguages = new CandidateLanguages();
                    candidateLanguages.CandidateID = IDCand;
                    candidateLanguages.LanguagesID = item.LanguageID;
                    candidateLanguages.LevelID = item.NivelID;
                    candidateLanguagesList.Add(candidateLanguages);
                }

            }
            container.getSolicitudesServices().Save(candidateLanguagesList);
        }
        private void guardarSkills(int IDCand)
        {
            List<int> SkillIDList = new List<int>();
            List<CheckBoxList> SkillCheckBoxList = new List<CheckBoxList>();
            List<CandidateSkills> CandidateSkillList = new List<CandidateSkills>();
            CandidateSkills CandidateSkill;

            SkillCheckBoxList.Add(CheckBoxList1);
            SkillCheckBoxList.Add(CheckBoxList2);
            SkillCheckBoxList.Add(CheckBoxList3);
            foreach (CheckBoxList SkillCheck in SkillCheckBoxList)
            {
                foreach (ListItem item in SkillCheck.Items)
                {
                    if (item.Selected)
                    {
                        CandidateSkill = new CandidateSkills();
                        CandidateSkill.CandidateID = IDCand;
                        CandidateSkill.SkillID = int.Parse(item.Value);
                        CandidateSkillList.Add(CandidateSkill);
                    }
                }
            }
            container.getSolicitudesServices().Save(CandidateSkillList);
        }
        private void GuardarReferencia(int IDCand)
        {
            List<TextBox> ReferenciasBoxesList = new List<TextBox>();
            List<ReferenciasCandidatos> ReferenciasCandidatosList = new List<ReferenciasCandidatos>();
            List<CandidateReferences> candidatoReferenciasList = new List<CandidateReferences>();
            CandidateReferences candidatoReferencias;

            ReferenciasCandidatosList.Add(new ReferenciasCandidatos() { Name = NombreReferenciaTextBox1.Text, PhoneNumber = TelefonoReferenciaTextBox1.Text, Profession = OcupacionReferenciaTextBox1.Text });
            ReferenciasCandidatosList.Add(new ReferenciasCandidatos() { Name = NombreReferenciaTextBox2.Text, PhoneNumber = TelefonoReferenciaTextBox2.Text, Profession = OcupacionReferenciaTextBox2.Text });
            ReferenciasCandidatosList.Add(new ReferenciasCandidatos() { Name = NombreReferenciaTextBox3.Text, PhoneNumber = TelefonoReferenciaTextBox3.Text, Profession = OcupacionReferenciaTextBox3.Text });

            foreach (ReferenciasCandidatos item in ReferenciasCandidatosList)
            {
                if (item.Name != string.Empty)
                {
                    candidatoReferencias = new CandidateReferences();
                    candidatoReferencias.CandidateID = IDCand;
                    candidatoReferencias.Name = item.Name;
                    candidatoReferencias.PhoneNumber = item.PhoneNumber;
                    candidatoReferencias.Profession = item.Profession;
                    candidatoReferenciasList.Add(candidatoReferencias);
                }

            }

            container.getSolicitudesServices().Save(candidatoReferenciasList);
        }
        protected void CheckCandidate(object sender, EventArgs e)
        {
            if (CedulaTextBox.Text != string.Empty)
            {
                GetDatosPersonales(CedulaTextBox.Text);
            }
            
        }
        private void GetDatosPersonales(string cedula)
        {
          Candidates candidato =  container.getSolicitudesServices().GetCandidates(cedula);

            if (candidato != null)
            {       
            //  candidato.PostulatePositionID = int.Parse(PosicionesDropDown.SelectedItem.Value);
            NombreTextBox.Text = candidato.FirstName;
            ApellidoTextBox.Text = candidato.LastName;
            CalleTextBox.Text = candidato.Streets;
            NoCasaTextBox.Text = candidato.HouseNumber;
            SectorTextBox.Text = candidato.Sector;
            LocalidadTextBox.Text = candidato.location;
            SexoRadioButton.SelectedValue = candidato.Sex;
            HiddenDate.Value = candidato.BirthDate.ToString();
            DropBoxNacionalidad.SelectedValue = candidato.NationalityID.ToString();
            TelefonoTextBox.Text = candidato.PhoneNumber;
            CelularTextBox.Text = candidato.MovilNumber;
            EmailTextBox.Text = candidato.Email;
            CedulaTextBox.Text = candidato.Cedula;
            ApodoTextBox.Text = candidato.alias;
            TelEmergerciaTextBox.Text = candidato.EmergencyNumber;
            ContactoEmergerciaTextBox.Text = candidato.EmergencyContactName;
            SizeDropDown.SelectedValue = candidato.Size;
            ReligionDropDown.SelectedValue = candidato.Religion;
            EstadoCivilDropBox.SelectedValue = candidato.CivilStatus.ToString();
            CheckboxAllergias.Checked = candidato.Allergies;
            AlergiasTextBox.Text = candidato.AllergiesType;
            PadecimientoCheckBox.Checked = candidato.suffering;
            PadecimientoTextBox.Text = candidato.sufferingType;
            checkboxExtras.Checked = candidato.AvailableOverTime;
            ViveConTextBox.Text = candidato.LivesWith;
            NombreConyugeTextBox.Text = candidato.SpouseName;
            ActividadConyugeTextBox.Text = candidato.SpouseActivity;
            NombreMadreTextBox.Text = candidato.MotherName;
            NombrePadreTextBox.Text = candidato.FatherName;
            HijosCargoTextBox.Text = candidato.DependentChildren.ToString();
            PersonasCargoTextBox.Text = candidato.DependentPeople.ToString();
            DropBoxEstudios.SelectedValue = candidato.StudiesID.ToString();
            CheckboxComputacion.Checked = candidato.ComputerStudies;
            estudiaCheckBox.Checked = candidato.IsStudent;
                if (candidato.DaysWeek == string.Empty)
                {
                    string[] D = candidato.DaysWeek.Split(',');
                    foreach (string dias in D)
                    {
                        foreach (ListItem item in CheckButtonDias.Items)
                        {
                            if (item.Text == dias)
                            {
                                item.Selected = true;
                            }
                        }
                    }
                }
            HoriarioDropBox.SelectedItem.Text = candidato.Schedule;
            ProgramasTextBox.Text = candidato.ProgramsKnow;
            RecomendoTextBox.Text = candidato.WhosRecommended;
            FamiliarEmpresa.Checked = candidato.FamilyOrFriend;
            NombreParentescoTextBox.Text = candidato.FamilyOrFriendName;
            PuestoTextBox.Text = candidato.FamilyOrFriendPuesto;
            ParentescoTextBox.Text = candidato.FamilyOrFriendRelationship;
            }
        }

        private void GuardarFotos(int id)
        {
            string imgStrC = strig64Candidate.Value;
            string imgStrF = strig64CedulaFront.Value;
            string imgStrB = strig64CedulaBack.Value;

            string imgDbC = string.Empty;
            string imgDbF = string.Empty;
            string imgDbB = string.Empty;

            if (HFimg.Value != string.Empty|| HFCedula.Value != string.Empty || HFCedulaTracera.Value != string.Empty)
            {
                imgDbC = HFimg.Value != string.Empty  ? HFimg.Value.Substring(22,HFimg.Value.Length-22):string.Empty;
                imgDbF = HFCedula.Value != string.Empty  ? HFCedula.Value.Substring(22, HFCedula.Value.Length - 22) : string.Empty;
                imgDbB = HFCedulaTracera.Value != string.Empty  ? HFCedulaTracera.Value.Substring(22, HFCedulaTracera.Value.Length - 22) : string.Empty;

            }
            if (imgStrC != string.Empty || imgStrF != string.Empty || imgStrB != string.Empty)
            {

               if (imgDbC == string.Empty && imgStrC != string.Empty)
                {
                    imgDbC = imgStrC.Substring(23, imgStrC.Length - 23);
                }
                if (imgDbF == string.Empty && imgStrF != string.Empty)
                {
                    imgDbF = imgStrF.Substring(23, imgStrF.Length - 23);
                }
                if (imgDbB == string.Empty && imgStrB != string.Empty)
                {
                    imgDbB = imgStrB.Substring(23, imgStrB.Length - 23);
                }
         
            }
            if (imgDbC != string.Empty || imgDbF != string.Empty || imgDbB != string.Empty)
            {
                CandidatePictures pc = new CandidatePictures();
                pc.CandidateID = Identificador;
                pc.PictureCadidate = Convert.FromBase64String(imgDbC);
                pc.PictureCadidateCedulaFrontPicture = Convert.FromBase64String(imgDbF);
                pc.PictureCadidateCedulaBackPicture = Convert.FromBase64String(imgDbB);
                container.getSolicitudesServices().SavePicture(pc);

            }
            
        }

        private void guardarFile()
        {
            if ((ArchivoFileUpload.PostedFile != null) && (ArchivoFileUpload.PostedFile.ContentLength > 0))
            {
              //  string fn = System.IO.Path.GetFileName(ArchivoFileUpload.PostedFile.FileName);

                string gui = Guid.NewGuid().ToString();
                string SaveLocation = Server.MapPath("PDFFile") + "\\" + gui+".pdf";

                CandidateGUIDCurriculum cv = new CandidateGUIDCurriculum();
                cv.Cedula = CedulaTextBox.Text;
                cv.guid = gui;
                cv.Create_dt = DateTime.Now;
                container.getSolicitudesServices().Save(cv);
                // string SaveLocation = "c:/ineput/Document/CVs/" + fn;
                // Server.MapPath("PDFFile") + "\\" + fn;
                //string a = Server.MapPath(".");
                //string b = Server.MapPath("..");
                //string c = Server.MapPath("~");
                //string d = Server.MapPath("/");
                try
                {
                    ArchivoFileUpload.PostedFile.SaveAs(SaveLocation);
                    Response.Write("El archivo se ha cargado.");
                }
                catch (Exception ex)
                {
                    Response.Write("Error : " + ex.Message);

                }
            }
            else
            {
                Response.Write("Seleccione un archivo que cargar.");
            }
        }
        
        private class IdiomasNiveles
        {
            public int LanguageID { get; set; }
            public int NivelID { get; set; }

            public IdiomasNiveles()
            {

            }
            public IdiomasNiveles(int LanguageID, int NivelID)
            {
                this.LanguageID = LanguageID;
                this.NivelID = NivelID;
            }
        }
        private class ReferenciasCandidatos
        {
            public string Name { get; set; }
            public string PhoneNumber { get; set; }
            public string Profession { get; set; }
            public ReferenciasCandidatos()
            {

            }

            public ReferenciasCandidatos(string Name, string PhoneNumber, string Profession)
            {
                this.Name = Name;
                this.PhoneNumber = PhoneNumber;
                this.Profession = Profession;
            }
        }
    }

    }