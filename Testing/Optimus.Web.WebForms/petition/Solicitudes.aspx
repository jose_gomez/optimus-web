﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Solicitudes.aspx.cs" Inherits="Optimus.Web.WebForms.petition.Solicitudes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        #DedicaExperienciaTextbox {
            width: 500px;
        }

        input, textarea {
            max-width: 800px;
        }

        .alert-info {
            color: ##000000;
            background-color: #e6f2ff;
            border-color: ##000000;
        }

        .form-control {
            width: auto;
        }
    </style>
    <style>
        .panel-heading span {
            margin-top: -26px;
            font-size: 15px;
            margin-right: -12px;
        }

        .clickable {
            background: rgba(0, 0, 0, 0.15);
            display: inline-block;
            padding: 6px 12px;
            border-radius: 4px;
            cursor: pointer;
        }
    </style>
    <style>
 .containerPic{
    margin: 0px auto;
    width: 205px;
    height: 205px;
    border: 3px #333 solid;
    float:left;
}
.containerPic2 {
    margin: 0px auto;
    width: 205px;
    height: 205px;
    border: 3px #333 solid;
    float:left;
}
.videoElement {
    width: 200px;
    height: 200px;
    background-color: #666;
}
</style>
    <script>
        $(function () {

            $('.btn').on('click', function () {
                var $this = $(this);
                $this.button('loading');
                setTimeout(function () {
                    $this.button('reset');
                }, 8000);
            });


        });
        
    </script>
    <script>
        function toggles(elemento) {
            if (elemento.value == "1") {
               
                document.getElementById("DivPosDis").style.display = "block";
                document.getElementById("DivPosCat").style.display = "none";
            } else if (elemento.value == "2") {

                document.getElementById("DivPosDis").style.display = "none";
                document.getElementById("DivPosCat").style.display = "block";
           
              }
        }
    </script>
    <script>
      $(function () {
          var video = document.querySelector("#videoElement");
          var videoCedula = document.querySelector("#videoElementCedula");
          var videoCedulaTracera = document.querySelector("#videoElementCedulaTracera");
          
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia || navigator.oGetUserMedia;

            if (navigator.getUserMedia) {
                navigator.getUserMedia({ video: true }, handleVideo, videoError);
            }

            function handleVideo(stream) {
                video.src = window.URL.createObjectURL(stream);
                videoCedula.src = window.URL.createObjectURL(stream);
                videoCedulaTracera.src = window.URL.createObjectURL(stream);
            }

            function videoError(e) {
                // do something
            }

            var canvas = document.getElementById('canvas');
            var canvasCedula = document.getElementById('canvasCedula');
            var canvasCedulaTracera = document.getElementById('canvasCedulaTracera');
            var context = canvas.getContext('2d');
            var contextCedula = canvasCedula.getContext('2d');
            var contextCedulaTracera = canvasCedulaTracera.getContext('2d');
            var myHidden = document.getElementById('MainContent_HFimg');
            var myHiddenCedula = document.getElementById('MainContent_HFCedula');
            var myHiddenCedulaTracera = document.getElementById('MainContent_HFCedulaTracera');


            document.getElementById("snap").addEventListener("click", function () {
                context.drawImage(video, 0, 0, 200, 200);
                myHidden.value = canvas.toDataURL();
            });

            document.getElementById("snapCedula").addEventListener("click", function () {
                contextCedula.drawImage(video, 0, 0, 200, 200);
                myHiddenCedula.value = canvasCedula.toDataURL();
            });
            document.getElementById("snapCedulaTracera").addEventListener("click", function () {
                contextCedulaTracera.drawImage(video, 0, 0, 200, 200);
                myHiddenCedulaTracera.value = canvasCedulaTracera.toDataURL();
            });
       });
            

            // Trigger photo take
            //document.getElementById("snap").addEventListener("click", function () {
            //    context.drawImage(video, 0, 0, 640, 480);
            //    //myHidden.value = canvas.toDataURL();
            //});
          
        
        //function take() {
        //    context.drawImage(video, 0, 0, 640, 480);
        //    myHidden.value = canvas.toDataURL();
        //}
</script>
    <script>
        function EnableDisableTextBox(chkPassport) {
            var NombreParentescoTextBox = document.getElementById("MainContent_NombreParentescoTextBox");
            var ParentescoTextBox = document.getElementById("MainContent_ParentescoTextBox");
            var PuestoTextBox = document.getElementById("MainContent_PuestoTextBox");

            NombreParentescoTextBox.disabled = chkPassport.checked ? false : true;
            ParentescoTextBox.disabled = chkPassport.checked ? false : true;
            PuestoTextBox.disabled = chkPassport.checked ? false : true;
            if (!NombreParentescoTextBox.disabled) {
                NombreParentescoTextBox.focus();
            }
            if (chkPassport.checked == false) {
                NombreParentescoTextBox.value = "";
                ParentescoTextBox.value = "";
                PuestoTextBox.value = "";
            }

        }
        function EnableDisablePadecimiento(chkPassport) {
            var PadecimientoTextBoxs = document.getElementById("MainContent_PadecimientoTextBox");

            PadecimientoTextBoxs.disabled = chkPassport.checked ? false : true;
            if (chkPassport.checked == false) {
                PadecimientoTextBoxs.value = "";
            }

        }
        function EnableDisableAlergias(chkPassport) {
            var AlergiasTextBoxs = document.getElementById("MainContent_AlergiasTextBox");
            AlergiasTextBoxs.disabled = chkPassport.checked ? false : true;
            if (chkPassport.checked == false) {
                AlergiasTextBoxs.value = "";
            }
        }
        function EnableDisableComputation(chkPassport) {
            var ProgramasTextBoxs = document.getElementById("MainContent_ProgramasTextBox");
            ProgramasTextBoxs.disabled = chkPassport.checked ? false : true;
            if (chkPassport.checked == false) {
                ProgramasTextBoxs.value = "";
            }
        }
    </script>
    <script>
        $(function () {
            var dateToday = new Date();
            $("#datepicker").datepicker({
                changeMonth: true,
                changeYear: true,
                yearRange: '1920:' + (dateToday.getFullYear() - 18),
            }).on("change", function () {
                $('#MainContent_HiddenDate').attr("value", $("#datepicker").datepicker({ dateFormat: 'dd-mm-yy' }).val());

            });
        });
        $(function () {
            if ($('#estudiaCheckBox').checked == true) {
                $('#Dias').collapse("show");
            }
        });
        $(function () {
            $("#datepicker").datepicker('setDate', $('#MainContent_HiddenDate').val());
          //$("#datepicker").datepicker({ defaultDate: $('#MainContent_HiddenDate').val() });
        });
    </script>
    <script>

        $(function () {
            $('.clickable').on('click', function () {
                var effect = $(this).data('effect');
                $(this).closest('.panel')[effect]();
            })
        });
    </script>
    <script type="text/javascript">
        $(function () {
            $("#alertaHeleido").hide();
            $.datepicker.regional['es'] = {
                closeText: 'Cerrar',
                prevText: '<Ant',
                nextText: 'Sig>',
                currentText: 'Hoy',
                monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                monthNamesShort: ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic'],
                dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
                dayNamesShort: ['Dom', 'Lun', 'Mar', 'Mié', 'Juv', 'Vie', 'Sáb'],
                dayNamesMin: ['Do', 'Lu', 'Ma', 'Mi', 'Ju', 'Vi', 'Sá'],
                weekHeader: 'Sm',
                dateFormat: 'dd/mm/yy',
                firstDay: 1,
                isRTL: false,
                showMonthAfterYear: false,
                yearSuffix: ''
            };
            $.datepicker.setDefaults($.datepicker.regional['es']);
            var dateToday = new Date();
            from = $("#from")
              .datepicker({
                  defaultDate: "+1w",
                  changeMonth: true,
                  changeYear: true,
                  yearRange: '1950:' + dateToday.getFullYear(),
                  numberOfMonths: 1
              })
              .on("change", function () {
                  to.datepicker("option", "minDate", getDate(this));
              }),
            to = $("#to").datepicker({
                defaultDate: "+1w",
                changeMonth: true,
                changeYear: true,
                yearRange: '1950:' + dateToday.getFullYear(),
                numberOfMonths: 1
            })
            .on("change", function () {
                from.datepicker("option", "maxDate", getDate(this));
            });

            function getDate(element) {
                var date;
                try {
                    date = $.datepicker.parseDate(dateFormat, element.value);
                } catch (error) {
                    date = null;
                }

                return date;
            }
        });
        $(function () {

            $('#datetimepicker2').datepicker(); ({
                showOn: "button",
                buttonImage: "../images/Icon/calendar.png",
                buttonImageOnly: true,
                buttonText: "Select date"
            });
        });
    </script>
    <script>
        function agregarExp() {
            if (!validarFormatoFecha($("#from").datepicker({ dateFormat: 'yy-mm-dd' }).val()) || !validarFormatoFecha($("#to").datepicker({ dateFormat: 'yy-mm-dd' }).val())) {
                alert("Debe ingresar  una fecha Valida");
            } else {
                if (Page_ClientValidate()) {
                    if ($("#MainContent_ExperienciaEmpresaTextBox").val() != "" && $('#MainContent_DedicaExperienciaTextbox').val() != "" &&
                        $('#MainContent_TareasRalizadasTextbox').val() != -"" && $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).val() != "" &&
                        $("#to").datepicker({ dateFormat: 'yy-mm-dd' }).val() != "" && $('#MainContent_SueldoTextBox').val() != "" && $('#MainContent_CausaRetiroTextBox').val() != ""
                                 ) {

                        $('#DivExp').append("<div class='panel panel-default'>\
                        <div class='panel-heading'>\
                                <h3 class='panel-title'>Experiencias</h3>\
                            <span class='pull-right clickable' data-effect='slideUp'><i class='fa fa-times'></i></span>\
                        </div>\
                        <div class='panel-body'>\
                            <div class='row'>\
                                <div class='col-md-6'>\
                                    <label style='font: bold'>Empresa:</label>\
                                    <Label class='Empresa' style='font-weight: normal'>" + $("#MainContent_ExperienciaEmpresaTextBox").val() + "<Label>\
                                </div>\
                            </div>\
                            <div class='row'>\
                                <div class='col-md-6'>\
                                    <label style='font: bold'>A qué se dedicá:</label>\
                                    <Label Class='SeDedica' style='font-weight: normal'>" + $('#MainContent_DedicaExperienciaTextbox').val() + "</Label>\
                                </div>\
                            </div>\
                            <div class='row'>\
                                <div class='col-md-6'>\
                                    <label style='font: bold'>Tareas Realizadas:</label>\
                                    <Label class='Trealizadas' style='font-weight: normal'>" + $('#MainContent_TareasRalizadasTextbox').val() + "</:Label>\
                                </div>\
                            </div>\
                            <div class='row'>\
                                <label style='font: bold' class='col-md-1' for='from'>Desde:</label>\
                                <Label class='col-md-2 fechaFrom' style='font-weight: normal' Class='col-md-2'>" + $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).val() + "</Label>\
                                <label style='font: bold' class='col-md-1' for='to'>Hasta:</label>\
                                <Label class='col-md-2 fechaTo' style='font-weight: normal' Class='col-md-2'>" + $("#to").datepicker({ dateFormat: 'yy-mm-dd' }).val() + "</Label>\
                                <label style='font: bold' class='col-md-2'>Sueldo:RD.$</label>\
                                <Label Class='col-md-1 sueldo' style='font-weight: normal' Class='col-md-2'>" + $('#MainContent_SueldoTextBox').val() + "</Label>\
                            </div>\
                            <div class='row'>\
                                <div class='col-md-6'>\
                                    <Label Class='form-control-label' Font-Bold='true'>Causa retiro:</Label>\
                                    <Label Class='causaExp' style='font-weight: normal'>" + $('#MainContent_CausaRetiroTextBox').val() + "<Label>\
                                </div>\
                                <div class='col-md-6'>\
                                    <Label Class='form-control-label' Font-Bold='true'>Tel.Empresa:</Label>\
                                    <Label Class='telefonoExp' style='font-weight: normal' >" + $('#MainContent_TelefonoEmpresaExp').val() + "</Label>\
                                </div>\
                            </div>\
                        </div>\
                    </div>");
                        $(function () {
                            $('.clickable').on('click', function () {
                                var effect = $(this).data('effect');
                                $(this).closest('.panel')[effect]();
                            })
                        });
                        $("#MainContent_ExperienciaEmpresaTextBox").val("");
                        $('#MainContent_DedicaExperienciaTextbox').val("");
                        $('#MainContent_TareasRalizadasTextbox').val("");
                        $("#from").datepicker({ dateFormat: 'yy-mm-dd' }).val("");
                        $("#to").datepicker({ dateFormat: 'yy-mm-dd' }).val("");
                        $('#MainContent_SueldoTextBox').val("");
                        $('#MainContent_CausaRetiroTextBox').val("");
                        $('#MainContent_TelefonoEmpresaExp').val("");
                    } else { alert("No puede dejar campos vacios") }
                }
            }
        }
    </script>
    <script>
        function agregarHerm() {
            if ($('#MainContent_hermanosTextBox').val() != "") {
                $("#Hermanos").append("<div class='col-md-2'><div class='alert alert-info'>\
    <a href='#' class='close' data-dismiss='alert' aria-label='close'>&times;</a>\
   "+ $('#MainContent_hermanosTextBox').val() + "</div></div>"
                 );

                $('#MainContent_hermanosTextBox').val("");
            } else { alert("Debe Ingresar un nombre") }

        }
    </script>
    <script>
        function Submit() {
            if ($("#datepicker").datepicker().val() == "") {
                alert("Debe ingresar  una fecha de nacimiento");
            } else if (!validarFormatoFecha($("#datepicker").datepicker().val())) {
            
                alert("Debe ingresar  una fecha de nacimiento Valida");
            
            }else {
                if (Page_ClientValidate()) {
                    var z = [];
                    var Herm = [];
                    $("#Hermanos").find(".alert").each(function () {
                        Herm.push($(this).text().trim());
                    })
                    z.push(Herm + 'Æ');

                    $("#DivExp").find(".panel").each(function (i) {
                        var Emp = $(this).find('.Empresa').text().trim();
                        var SeDedi = $(this).find('.SeDedica').text().trim();
                        var Tareas = $(this).find('.Trealizadas').text().trim();
                        var fechaFrom = $(this).find('.fechaFrom').text().trim();
                        var fechaTo = $(this).find('.fechaTo').text().trim();
                        var Sueldo = $(this).find('.sueldo').text().trim();
                        var Causa = $(this).find('.causaExp').text().trim();
                        var Telefono = $(this).find('.telefonoExp').text().trim();
                        z.push(i + '@' + Emp + '@#' + SeDedi + '#%' + Tareas + '%^' + fechaFrom + '^&' + fechaTo + '&$' + Sueldo + '$*' + Causa + '*?' + Telefono + '?§');
                    })
                    __doPostBack('DatosSubmit', z);
                }
            }
        }
                function validarFormatoFecha(campo) {
                    var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;
                    if ((campo.match(RegExPattern)) && (campo!='')) {
                        return true;
                    } else {
                        return false;
                    }
                }
    </script>
    <script>
        ///Imagen Uno
        $(function () {
            $('#FileUpload1').change(function () {
                $('#Image1').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#Image1').show();
                    $('#Image1').attr("src", e.target.result);
                    $('#MainContent_strig64Candidate').attr("value", e.target.result);
                    $('#Image1').Jcrop({
                        onChange: SetCoordinates,
                        onSelect: SetCoordinates
                    });
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
        });
    </script>
    <script>
        ///fotos cedula
        $(function () {
            $('#FileUpload2').change(function () {
                $('#MainContent_Image2').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#MainContent_Image2').show();
                    $('#MainContent_Image2').attr("src", e.target.result);
                    $('#MainContent_strig64CedulaFront').attr("value", e.target.result);
                    $('#MainContent_Image2').Jcrop({
                        onChange: SetCoordinates,
                        onSelect: SetCoordinates
                    });
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });

        });
    </script>
    <script>
        ///fotos cedula 2
        $(function () {
            $('#FileUpload3').change(function () {
                $('#Image3').hide();
                var reader = new FileReader();
                reader.onload = function (e) {
                    $('#Image3').show();
                    $('#Image3').attr("src", e.target.result);
                    $('#MainContent_strig64CedulaBack').attr("value", e.target.result);
                    $('#Image3').Jcrop({
                        onChange: SetCoordinates,
                        onSelect: SetCoordinates
                    });
                }
                reader.readAsDataURL($(this)[0].files[0]);
            });
        });
    </script>
    <script>
        jQuery(function ($) {

            $("#MainContent_CedulaTextBox").mask("999-9999999-9");

            //Mascara Telefonos
            $("#MainContent_TelefonoTextBox").mask("(999) 999-9999");
            $("#MainContent_CelularTextBox").mask("(999) 999-9999");
            $("#MainContent_TelEmergerciaTextBox").mask("(999) 999-9999");
            $("#MainContent_TelefonoEmpresaExp").mask("(999) 999-9999");
            $("#MainContent_TelefonoReferenciaTextBox1").mask("(999) 999-9999");
            $("#MainContent_TelefonoReferenciaTextBox2").mask("(999) 999-9999");
            $("#MainContent_TelefonoReferenciaTextBox3").mask("(999) 999-9999");
            //Mascara Fechas
            $("#datepicker").mask("99/99/9999", { placeholder: "dd/mm/aaaa" });
            $("#from").mask("99/99/9999", { placeholder: "dd/mm/aaaa" });
            $("#to").mask("99/99/9999", { placeholder: "dd/mm/aaaa" });


            $('#MainContent_SueldoTextBox').maskMoney({ allowNegative: false, thousands: ',', decimal: '.', affixesStay: false });


        });
  

    </script>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Vacantes y areas Disponibles</h4>
            </div>
            <div class="panel-body">
                <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                    <ContentTemplate>
                        <div class="form-group row">
                            <div class="form-group">
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" value="1" onclick="toggles(this);" checked="true">Posiciones Disponibles</label>
                                <label class="radio-inline">
                                    <input type="radio" name="optradio" value="2" onclick="toggles(this);">Categoría de a la que Aplica</label>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div id="DivPosDis" class="col-md-6">
                                <div class="form-group">
                                    <div>
                                        <asp:Label runat="server" CssClass="col-md-6 form-control-label" Font-Bold="true">Posiciones disponibles:</asp:Label>
                                        <asp:DropDownList runat="server" ID="PosicionesDropDown" CssClass="form-control" TabIndex="2">
                                        </asp:DropDownList>
                                    </div>
                                </div>
                            </div>
                            <div id="DivPosCat" style="display: none" class="col-md-6">
                                <div class="form-group">
                                    <div>
                                        <asp:Label runat="server" CssClass="col-md-6 form-control-label" Font-Bold="true">Categoría se postula:</asp:Label>
                                        <asp:DropDownList runat="server" ID="CategoriaPosicion" CssClass="form-control" TabIndex="20">
                               <%--             <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                            <asp:ListItem Value="1">Administrativo</asp:ListItem>
                                            <asp:ListItem Value="2">Técnico</asp:ListItem>
                                            <asp:ListItem Value="3">Operario</asp:ListItem>
                                            <asp:ListItem Value="4">Seguridad</asp:ListItem>--%>
                                        </asp:DropDownList>
                                      <%--  <button type="button" class="btn btn-default btn-sm" runat="server">
                                            <span class="glyphicon glyphicon-search"></span>Buscar
                                        </button>--%>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
  
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Datos Personales del solicitante</h4>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="form-group">
                        <asp:ValidationSummary
                            ID="ValidationSummary1"
                            runat="server"
                            HeaderText="Los Siguientes Errores Fueron Cometidos"
                            ShowMessageBox="false"
                            DisplayMode="BulletList"
                            ShowSummary="true"
                            BackColor="Snow"
                            Width="450"
                            ForeColor="Red"
                            Font-Size="small"
                            Font-Italic="true" />
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Cédula:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="CedulaTextBox" Width="60%" MaxLength="14" AutoPostBack="True" OnTextChanged="CheckCandidate" TabIndex="1"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Nombre:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="NombreTextBox" Width="60%" MaxLength="50" TabIndex="3"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Sexo:</asp:Label>
                            <asp:RadioButtonList ID="SexoRadioButton" runat="server" RepeatDirection="Horizontal" TabIndex="5">
                                <asp:ListItem Value="M" Selected="true">Masculino</asp:ListItem>
                                <asp:ListItem Value="F">Femenino</asp:ListItem>
                            </asp:RadioButtonList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Calle:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="CalleTextBox" Width="60%" MaxLength="50" TabIndex="8"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Localidad:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="LocalidadTextBox" Width="60%" MaxLength="50" TabIndex="10"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Teléfono:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TelefonoTextBox" Width="60%" MaxLength="20" TabIndex="12"></asp:TextBox>
                        </div>
                
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>E-Mail:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="EmailTextBox" Width="60%" MaxLength="100" TabIndex="14"></asp:TextBox>
                        </div>

                        <div class="form-group">
                            <%--  <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Fecha Nacimiento:</asp:Label>--%>

                            <label class="col-md-4 form-control-label" for="from"><span style="color:red">*</span>Fecha Nacimiento:</label>
                            <input type="text" id="datepicker" tabindex="16" class="form-control">
                            <asp:HiddenField runat="server" ID="HiddenDate" />
                            <%-- <asp:TextBox runat="server" CssClass="form-control" ID="FechaTextBox" Width="40%"></asp:TextBox>--%>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Size:</asp:Label>
                            <asp:DropDownList runat="server" ID="SizeDropDown" CssClass="form-control" TabIndex="18">
                                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                <asp:ListItem>S</asp:ListItem>
                                <asp:ListItem>M</asp:ListItem>
                                <asp:ListItem>L</asp:ListItem>
                                <asp:ListItem>XL</asp:ListItem>
                            </asp:DropDownList>
                        </div> 
                                                    <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Estado Civil:</asp:Label>
                            <asp:DropDownList runat="server" ID="EstadoCivilDropBox" CssClass="form-control" TabIndex="20">
                                <asp:ListItem Value="0">Seleccione</asp:ListItem>
                                <asp:ListItem Value="1">Casado</asp:ListItem>
                                <asp:ListItem Value="2">Soltero</asp:ListItem>
                                <asp:ListItem Value="3">Divorciado</asp:ListItem>
                                <asp:ListItem Value="4">Viudo</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-6">
                      
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Apellido:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="ApellidoTextBox" Width="60%" MaxLength="50" TabIndex="4"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true"><span style="color:red">*</span>No.casa:</asp:Label>
                            <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="NoCasaTextBox" Width="50" MaxLength="50" TabIndex="6"></asp:TextBox>
                            <asp:Label runat="server" CssClass="col-md-2  form-control-label" Font-Bold="true"><span style="color:red">*</span>Sector:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control col-md-6" Width="136" ID="SectorTextBox" MaxLength="50" TabIndex="7"></asp:TextBox>
                            <br />
                            <br />
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Nacionalidad:</asp:Label>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="DropBoxNacionalidad" TabIndex="9">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Celular:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="CelularTextBox" Width="60%" MaxLength="20" TabIndex="11"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Apodo:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="ApodoTextBox" Width="60%" MaxLength="50" TabIndex="13"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Tel.Emergencia:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TelEmergerciaTextBox" Width="60%" MaxLength="20" TabIndex="15"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" class="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Contacto Emergencia:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="ContactoEmergerciaTextBox" Width="60%" MaxLength="50" TabIndex="17"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" class="col-md-4 form-control-label" Font-Bold="true"><span style="color:red">*</span>Religión:</asp:Label>
                            <asp:DropDownList runat="server" ID="ReligionDropDown" CssClass="form-control" TabIndex="19">
                                <asp:ListItem>No Religioso</asp:ListItem>
                                <asp:ListItem>Cristiano Católico</asp:ListItem>
                                <asp:ListItem>Cristiano Advestista</asp:ListItem>
                                <asp:ListItem>Cristiano Evagenlica</asp:ListItem>
                                <asp:ListItem>Cristiano Pentescostal</asp:ListItem>
                                <asp:ListItem>Cristiano Bautista</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
                <div style="display: none">
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NombreTextBox" ErrorMessage="Tiene que Ingresar un Nombre" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CalleTextBox" ErrorMessage="Tiene que Ingresar una Calle" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="LocalidadTextBox" ErrorMessage="Tiene que Ingresar una Localidad" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TelefonoTextBox" ErrorMessage="Tiene Ingresar un Numero de télefono" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="EmailTextBox" ErrorMessage="Tiene que Ingresar un Email" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CedulaTextBox" ErrorMessage="Tiene que Ingresar una cedula" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ApellidoTextBox" ErrorMessage="Tiene que Ingresar un Apellido" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NoCasaTextBox" ErrorMessage="Tiene que Ingresar un NoCasa" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="SectorTextBox" ErrorMessage="Tiene que Ingresar un Sector" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="CelularTextBox" ErrorMessage="Tiene que Ingresar un Celular" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="TelEmergerciaTextBox" ErrorMessage="Tiene que Ingresar un telefono de Emergencia" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="ContactoEmergerciaTextBox" ErrorMessage="Tiene que Ingresar un Contacto de emergencia" Text="" ForeColor="Red">
                    </asp:RequiredFieldValidator>
                    
                </div>
                <div>
                    <%--             <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"
                        ControlToValidate="NoCasaTextBox" ErrorMessage="*Ingrese Valores Numericos"
                        ForeColor="Red"
                        ValidationExpression="^[0-9]*">
                    </asp:RegularExpressionValidator>--%>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server"
                        ControlToValidate="NombreTextBox" ErrorMessage="*Ingrese solo letras en el Campo del nombre"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server"
                        ControlToValidate="ApellidoTextBox" ErrorMessage="*Ingrese solo letras en el campo de apellido"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server"
                        ControlToValidate="ApodoTextBox" ErrorMessage="*Ingrese solo letras en el campo de apodo"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server"
                        ControlToValidate="ContactoEmergerciaTextBox" ErrorMessage="*Ingrese solo letras en el campo de Contacto Emergercia"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator8" runat="server"
                        ControlToValidate="TelefonoTextBox" ErrorMessage="*Ingrese un numero teléfonico valido"
                        ForeColor="Red"
                        ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator9" runat="server"
                        ControlToValidate="CelularTextBox" ErrorMessage="*Ingrese un numero teléfonico valido en Campo de teléfono celular"
                        ForeColor="Red"
                        ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server"
                        ControlToValidate="TelEmergerciaTextBox" ErrorMessage="*Ingrese un numero teléfonico valido en Campo teléfono de emergencia"
                        ForeColor="Red"
                        ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator10" runat="server"
                        ControlToValidate="EmailTextBox" ErrorMessage="*Ingrese un Email Valido"
                        ForeColor="Red"
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Declaración de Salud</h4>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="chkPassport">
                                <input type="checkbox" runat="server" id="PadecimientoCheckBox" onclick="EnableDisablePadecimiento(this)" />
                                Tienen algún tipo de padecimiento:?
                            </label>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"> Cual?</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="PadecimientoTextBox" Width="60%" Enabled="false" MaxLength="100"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <label for="chkPassport">
                                <input type="checkbox" runat="server" id="CheckboxAllergias" onclick="EnableDisableAlergias(this)" />
                                Tienen algún tipo de Alergias:?
                            </label>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true"> Cual?</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="AlergiasTextBox" Width="60%" Enabled="false" MaxLength="100"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="chkPassport">
                                <input type="checkbox" runat="server" id="checkboxExtras" />
                                Disponibilidad en Horas Extra:
                            </label>
                        </div>
                        <div class="form-group">
                            <label for="chkPassport">
                                <input type="checkbox" runat="server" id="CheckboxFinesSemana" />
                                Disponibilidad en Fines de Semana:
                            </label>
                        </div>
                    </div>
                </div>
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator12" runat="server"
                        ControlToValidate="PadecimientoTextBox" ErrorMessage="*Ingrese solo letras en el campo de Cual?"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator13" runat="server"
                        ControlToValidate="AlergiasTextBox" ErrorMessage="*Ingrese solo letras en el campo de Alergias?"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h4>Grupo Familiar</h4>
        </div>
        <div class="panel-body">
            <div class="form-group row">
                <div class="col-md-6">
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Vive con:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="ViveConTextBox" Width="60%" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nombre del conyugue:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="NombreConyugeTextBox" Width="60%" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Actividad del conyugue:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="ActividadConyugeTextBox" Width="60%" MaxLength="100"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nombre del Padre:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="NombrePadreTextBox" Width="60%" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Total Personas a cargo:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="PersonasCargoTextBox" Width="10%" MaxLength="2"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Hijos a Cargo:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="HijosCargoTextBox" Width="10%" MaxLength="2"></asp:TextBox>
                    </div>
                    <div class="form-group">
                        <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nombre madre:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="NombreMadreTextBox" MaxLength="100"></asp:TextBox>
                    </div>
                </div>
            </div>
            <div>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator14" runat="server"
                    ControlToValidate="ViveConTextBox" ErrorMessage="*Ingrese solo letras en el campo de Viva con ?"
                    ForeColor="Red"
                    ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                </asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator15" runat="server"
                    ControlToValidate="ActividadConyugeTextBox" ErrorMessage="*Ingrese solo letras en el campo de Actividad Conyuge?"
                    ForeColor="Red"
                    ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                </asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator16" runat="server"
                    ControlToValidate="NombrePadreTextBox" ErrorMessage="*Ingrese solo letras en el campo Nombre del Padre?"
                    ForeColor="Red"
                    ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                </asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator17" runat="server"
                    ControlToValidate="PersonasCargoTextBox" ErrorMessage="*Ingrese solo letras en el campo Nombre Personas Cargo"
                    ForeColor="Red"
                    ValidationExpression="^[0-9]*">
                </asp:RegularExpressionValidator>
                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"
                    ControlToValidate="HijosCargoTextBox" ErrorMessage="*Ingrese solo letras en el campo Nombre Personas Cargo"
                    ForeColor="Red"
                    ValidationExpression="^[0-9]*">
                </asp:RegularExpressionValidator>
            </div>
            <div class="form-group row">
                <div class="col-md-6">
                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Nombre de hermanos:</asp:Label>
                    <div class="form-group">
                        <asp:TextBox runat="server" CssClass="col-md-4 form-control" ID="hermanosTextBox"></asp:TextBox>
                        <input id="Button1" type="button" value="Agregar" class="form-control btn btn-default" onclick="agregarHerm()" />
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div id="Hermanos">
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Estudios Realizados</h4>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Estudios Realizados:</asp:Label>
                            <asp:DropDownList runat="server" CssClass="form-control" ID="DropBoxEstudios">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="chkPassport">
                                <input type="checkbox" runat="server" id="estudiaCheckBox" data-toggle="collapse" data-target="#Dias" />
                                Esta estudiando actualmente:
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <div id="Dias" class="collapse">
                                <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Día:</asp:Label>
                                <asp:CheckBoxList ID="CheckButtonDias" runat="server" RepeatDirection="Horizontal">
                                    <asp:ListItem Value="L">L</asp:ListItem>
                                    <asp:ListItem Value="M">M</asp:ListItem>
                                    <asp:ListItem Value="Mi">Mi</asp:ListItem>
                                    <asp:ListItem Value="J">J</asp:ListItem>
                                    <asp:ListItem Value="V">V</asp:ListItem>
                                    <asp:ListItem Value="S">S</asp:ListItem>
                                    <asp:ListItem Value="D">D</asp:ListItem>
                                </asp:CheckBoxList>
                                <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Horario:</asp:Label>
                                <asp:DropDownList runat="server" ID="HoriarioDropBox" CssClass="form-control">
                                    <asp:ListItem></asp:ListItem>
                                    <asp:ListItem>Ma&#241;ana</asp:ListItem>
                                    <asp:ListItem>Tarde</asp:ListItem>
                                    <asp:ListItem>Día Completo</asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                    </div>
                </div>
                <span style="font: bold; font-size: x-large">Titulos</span>
                <hr />
                <div class="Form-group row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Titulo Obtenido:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TituloObtenidoTextBox1" Width="60%" MaxLength="40"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Titulo Obtenido:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TituloObtenidoTextBox2" Width="60%" MaxLength="40"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Titulo Obtenido:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TituloObtenidoTextBox3" Width="60%" MaxLength="40"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Titulo Obtenido:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TituloObtenidoTextBox4" Width="60%" MaxLength="40"></asp:TextBox>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Titulo Obtenido:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="TituloObtenidoTextBox5" Width="60%" MaxLength="40"></asp:TextBox>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="chkPassport">
                                <input type="checkbox" runat="server" id="CheckboxComputacion" onclick="EnableDisableComputation(this)" />
                                Conocimientos en Informaticas
                            </label>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Programas:</asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control" ID="ProgramasTextBox" Width="60%" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <span style="font: bold; font-size: x-large">Idiomas</span>
                <hr />
                <div class="Form-group row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-3 form-control-label" Font-Bold="true">Idiomas 1:</asp:Label>
                            <asp:DropDownList runat="server" ID="DropboxIdiomas1" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-3 form-control-label" Font-Bold="true">Idiomas 2:</asp:Label>
                            <asp:DropDownList runat="server" ID="DropboxIdiomas2" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-3 form-control-label" Font-Bold="true">Idiomas 3:</asp:Label>
                            <asp:DropDownList runat="server" ID="DropboxIdiomas3" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-3 form-control-label" Font-Bold="true">Idiomas 4:</asp:Label>
                            <asp:DropDownList runat="server" ID="DropboxIdiomas4" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nivel:</asp:Label>
                            <asp:DropDownList runat="server" ID="dropboxNivel1" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nivel:</asp:Label>
                            <asp:DropDownList runat="server" ID="dropboxNivel2" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nivel:</asp:Label>
                            <asp:DropDownList runat="server" ID="dropboxNivel3" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                        <div class="form-group">
                            <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Nivel:</asp:Label>
                            <asp:DropDownList runat="server" ID="dropboxNivel4" CssClass="form-control">
                            </asp:DropDownList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Solo para personal Industrial/Técnico</h4>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:CheckBoxList ID="CheckBoxList1" runat="server">
                                <asp:ListItem Value="1">Joyero</asp:ListItem>
                                <asp:ListItem Value="2">Pulidor</asp:ListItem>
                                <asp:ListItem Value="3">Grabador</asp:ListItem>
                                <asp:ListItem Value="4">Montador</asp:ListItem>
                                <asp:ListItem Value="5">Banglero</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:CheckBoxList ID="CheckBoxList2" runat="server">
                                <asp:ListItem Value="6">Tornero</asp:ListItem>
                                <asp:ListItem Value="7">Fresador</asp:ListItem>
                                <asp:ListItem Value="8">Elec.Industrial</asp:ListItem>
                                <asp:ListItem Value="9">Refrigeraci&#243;n</asp:ListItem>
                                <asp:ListItem Value="10">Mecanica</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <asp:CheckBoxList ID="CheckBoxList3" runat="server">
                                <asp:ListItem Value="11">Modelista</asp:ListItem>
                                <asp:ListItem Value="12">Soldador con acetileno</asp:ListItem>
                                <asp:ListItem Value="13">Alba&#241;iler&#237;a</asp:ListItem>
                                <asp:ListItem Value="14">Electr&#243;nica</asp:ListItem>
                                <asp:ListItem Value="15">Otros</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Experiencia Laboral</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Empresa:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="ExperienciaEmpresaTextBox" Width="450" MaxLength="200"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">A qué se dedicá:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="DedicaExperienciaTextbox" Width="99%" MaxLength="100"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Tareas Realizadas:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="TareasRalizadasTextbox" TextMode="MultiLine" Width="99%"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label class="col-md-1" for="from">Desde:</label>
                    <input class="col-md-2 form-control" type="text" id="from" name="From">
                    <label class="col-md-1" for="to">Hasta:</label>
                    <input class="col-md-2 form-control"  type="text" id="to" name="to">
                    <label class="col-md-2" for="from">Sueldo:RD.$</label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="SueldoTextBox" MaxLength="18"></asp:TextBox>
                </div>
                <div class="form-group">
                    <div class="col-md-6">
                        <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Causa retiro:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="CausaRetiroTextBox" Width="450" MaxLength="50"></asp:TextBox>
                    </div>
                    <div class="col-md-6">
                        <asp:Label runat="server" CssClass="form-control-label" Font-Bold="true">Tel.Empresa:</asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="TelefonoEmpresaExp" Width="450" MaxLength="20"></asp:TextBox>
                    </div>
                </div>
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator23" runat="server"
                        ControlToValidate="TelefonoEmpresaExp" ErrorMessage="*Ingrese un numero teléfonico valido "
                        ForeColor="Red"
                        ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
                    </asp:RegularExpressionValidator>
                </div>
                <div class="form-group">
                    <br />
                    <br />
                    <input id="Button2" type="button" value="Agregar Experiencia" class="form-control btn btn-primary" onclick="agregarExp()" />
                </div>
            </div>
            <div class="panel-footer">
                <asp:UpdatePanel ID="UpdatePanelExp" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div id="DivExp">
                        </div>
                    </ContentTemplate>
                   <%-- <Triggers>
                        <asp:AsyncPostBackTrigger ControlID="BotonSolicitud" EventName="Click" />
                    </Triggers>--%>
                </asp:UpdatePanel>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Lazos con la empresa</h4>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Quien lo recomendo a nosotros?:</asp:Label>
                    <asp:TextBox runat="server" CssClass="form-control" ID="RecomendoTextBox" Width="450"></asp:TextBox>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-4 form-control-label" Font-Bold="true">Tiene algun familiar o amigo laborando en esta empresa?:</asp:Label>
                    <label for="chkPassport">
                        <input type="checkbox" runat="server" id="FamiliarEmpresa" onclick="EnableDisableTextBox(this)" />   
                        (Si/No)                    
                    </label>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" CssClass="col-md-1 form-control-label" Font-Bold="true">Nombre:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="NombreParentescoTextBox" Enabled="false" MaxLength="50"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Parentesco:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="ParentescoTextBox" Enabled="false" MaxLength="100"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-1 form-control-label" Font-Bold="true">Puesto:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="PuestoTextBox" Enabled="false" MaxLength="100"></asp:TextBox>
                </div>
                <div>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator24" runat="server"
                        ControlToValidate="RecomendoTextBox" ErrorMessage="*Ingrese solo letras en el Campo del nombre de recomendación"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator25" runat="server"
                        ControlToValidate="NombreParentescoTextBox" ErrorMessage="*Ingrese solo letras en el Campo del nombre de recomendación"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator26" runat="server"
                        ControlToValidate="ParentescoTextBox" ErrorMessage="*Ingrese solo letras en el Campo del nombre de recomendación"
                        ForeColor="Red"
                        ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
                    </asp:RegularExpressionValidator>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Referencias personales</h4>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                    <asp:Label runat="server" CssClass="col-md-1 form-control-label" Font-Bold="true">Nombre:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="NombreReferenciaTextBox1" MaxLength="50"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Télefono:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="TelefonoReferenciaTextBox1" MaxLength="20"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Profesión u Ocupación:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="OcupacionReferenciaTextBox1" MaxLength="75"></asp:TextBox>
                </div>
                <div class="form-group row">
                    <asp:Label runat="server" CssClass="col-md-1 form-control-label" Font-Bold="true">Nombre:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="NombreReferenciaTextBox2" MaxLength="50"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Télefono:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="TelefonoReferenciaTextBox2" MaxLength="20"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Profesión u Ocupación:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="OcupacionReferenciaTextBox2" MaxLength="75"></asp:TextBox>
                </div>
                <div class="form-group row">
                    <asp:Label runat="server" CssClass="col-md-1 form-control-label" Font-Bold="true">Nombre:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="NombreReferenciaTextBox3" MaxLength="50"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Télefono:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="TelefonoReferenciaTextBox3" MaxLength="20"></asp:TextBox>
                    <asp:Label runat="server" CssClass="col-md-2 form-control-label" Font-Bold="true">Profesión u Ocupación:</asp:Label>
                    <asp:TextBox runat="server" CssClass="col-md-2 form-control" ID="OcupacionReferenciaTextBox3" MaxLength="75"></asp:TextBox>
                </div>
            </div>
        </div>
    </div>
    <div>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator27" runat="server"
            ControlToValidate="NombreReferenciaTextBox1" ErrorMessage="*Ingrese solo letras en el Campo del nombre"
            ForeColor="Red"
            ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
        </asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator28" runat="server"
            ControlToValidate="NombreReferenciaTextBox2" ErrorMessage="*Ingrese solo letras en el Campo del nombre"
            ForeColor="Red"
            ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
        </asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator29" runat="server"
            ControlToValidate="NombreReferenciaTextBox3" ErrorMessage="*Ingrese solo letras en el Campo del nombre"
            ForeColor="Red"
            ValidationExpression="^[a-zA-Z''-'\s]{1,40}$">
        </asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator30" runat="server"
            ControlToValidate="TelefonoReferenciaTextBox1" ErrorMessage="*Ingrese un numero teléfonico valido"
            ForeColor="Red"
            ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
        </asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator31" runat="server"
            ControlToValidate="TelefonoReferenciaTextBox2" ErrorMessage="*Ingrese un numero teléfonico valido"
            ForeColor="Red"
            ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
        </asp:RegularExpressionValidator>
        <asp:RegularExpressionValidator ID="RegularExpressionValidator32" runat="server"
            ControlToValidate="TelefonoReferenciaTextBox3" ErrorMessage="*Ingrese un numero teléfonico valido"
            ForeColor="Red"
            ValidationExpression="^[01]?[- .]?(\([2-9]\d{2}\)|[2-9]\d{2})[- .]?\d{3}[- .]?\d{4}$">
        </asp:RegularExpressionValidator>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Fotos del solicitante</h4>
            </div>
            <div class="panel-body">
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Fotos del solicitante</h4>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">

                                <input type="file" id="FileUpload1" accept=".jpg,.png,.gif" />
                                <br />
                                <br />
                                <asp:HiddenField runat="server" ID="strig64Candidate" />
                                <table border="0" cellpadding="0" cellspacing="5">
                                    <tr>
                                        <td>
                                            <img id="Image1" src="" alt="" style="width: 100px; height: 100px; display: none" />
                                        </td>
                                        <td>
                                            <canvas id="canvas1" height="5" width="5"></canvas>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Foto de la parte frontal de la cédula</h4>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <asp:HiddenField runat="server" ID="strig64CedulaFront" />
                                <input type="file" id="FileUpload2" accept=".jpg,.png,.gif" />
                                <br />
                                <br />
                                <table border="0" cellpadding="0" cellspacing="5">
                                    <tr>
                                        <td>
                                            <img id="Image2" runat="server" src="" alt="" style="width: 100px; height: 100px; display: none" />
                                        </td>
                                        <td>
                                            <canvas id="canvas2" height="5" width="5"></canvas>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4>Foto parte trasera de la cédula</h4>
                        </div>
                        <div class="panel-body">
                            <div class="panel-body">
                                <asp:HiddenField runat="server" ID="strig64CedulaBack" />
                                <input type="file" id="FileUpload3" value="Seleccionar archivo" accept=".jpg,.png,.gif" />
                                <br />
                                <br />
                                <table border="0" cellpadding="0" cellspacing="5">
                                    <tr>
                                        <td>
                                            <img id="Image3" src="" alt="" style="width: 100px; height: 100px; display: none" />
                                        </td>
                                        <td>
                                            <canvas id="canvas3" height="5" width="5"></canvas>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Tomar Fotos</h4>
            </div>
            <div class="panel-body">
                <div class="form-group row">
                <div class="col-md-6">
                    <div class="form-group row"><label class="form-control-label">Foto candidato </label></div>
                    <div class="form-group row">                         
                        <div class="containerPic">
                            <video autoplay="true" id="videoElement" class="videoElement">
                            </video>
                        </div>
                        <div class="containerPic2">
                            <canvas id="canvas" width="200" height="200"></canvas>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:HiddenField runat="server" ID="HFimg" />
                        <input id="snap" type="button" class="form-control btn btn-primary" value="Tomar" />

                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group row"><label class="form-control-label">Foto cédula frontal </label></div>
                    <div class="form-group row">               
                        <div class="containerPic">
                            <video autoplay="true" id="videoElementCedula" class="videoElement">
                            </video>
                        </div>
                        <div class="containerPic2">
                            <canvas id="canvasCedula" width="200" height="200"></canvas>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:HiddenField runat="server" ID="HFCedula" />
                        <input id="snapCedula" class="form-control btn btn-primary"  type="button" value="Tomar" />
                    </div>
                </div>
                    </div>
                <div class="form-group row">
                    <div class="col-md-6">
                    <div class="form-group row"><label class="form-control-label">Foto cédula trasera</label></div>
                    <div class="form-group row">               
                        <div class="containerPic">
                            <video autoplay="true" id="videoElementCedulaTracera" class="videoElement">
                            </video>
                        </div>
                        <div class="containerPic2">
                            <canvas id="canvasCedulaTracera" width="200" height="200"></canvas>
                        </div>
                    </div>
                    <div class="form-group row">
                        <asp:HiddenField runat="server" ID="HFCedulaTracera" />
                        <input id="snapCedulaTracera" class="form-control btn btn-primary" type="button" value="Tomar" />
                    </div>
                </div>
                </div>
            </div>
        </div>
    </div>
        <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4>Subir Curriculum </h4>
            </div>
            <div class="panel-body">
       
                <asp:FileUpload ID="ArchivoFileUpload" runat="server" accept="application/pdf"  />         
            </div>
        </div>
    </div>
    <div>
        <p>
            <strong>Nota:</strong> Al solicitar este empleo me comprometo a aceptar las reglas y/o regulaciones presenetes y futuras que sean prescritas por la administracción de esta
            Compañia. Entiendo que dichos reglamento pueden ser modificados a mi, verbalmente o por escrito.
        </p>
        <p>
            Declaro que todas las informaciones contenidas en esta solicitud son verídicas.Entiendo que si ofresco cualquier respuesta falsa u omisión voluntaeriua en la entrevista,
            esto puede ser motivo de despido, sin tener consideracíon del momento en el que sean descubirtas dichas respuestas u omisiones.
        </p>
        <p>
            Autorizo  a la empresa a verificar esta información por las fuentes que considere necesaria, incluyendo consulta del buro de crédito antes de ser aprobado mi ingreso a la empresa
        </p>
    </div>
    <br />
    <div>
        <label for="chkPassport">
            <input type="checkbox" id="HeLeido" data-toggle="collapse" data-target="#submitButton" />
            He leído y acepto los términos y condiciones
        </label>
    </div>
    <br />
    <div id="submitButton" class="collapse">
        <input type="button" ID="BotonSolicitud" data-loading-text="Cargando..."  Class="form-control btn btn-primary" value="Enviar Solicitud" onClick="Submit()" />
     <%--   <asp:Button ID="BotonSolicitud" runat="server" Text="Enviar Solicitud" CssClass="form-control btn btn-primary" OnClientClick="Submit()" />--%>
    </div>
       
</asp:Content>