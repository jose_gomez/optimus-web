﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA.Optimus;
using Optimus.Web.WebForms.GuiHelpers;
using Optimus.Web.WebForms.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.AccessPetition
{
    public partial class AccessPetition : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        public AccessPetition()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            var departmentService = container.GetDepartmentService();
            var departments = departmentService.GetAllDepartments();
            if (!IsPostBack)
            {
                DropDownDepartamento.DataTextField = "Name";
                DropDownDepartamento.DataValueField = "DepartmentId";
                DropDownDepartamento.DataSource = departments;
                DropDownDepartamento.DataBind();
            }
            
        }
        protected void guardar(object sender,EventArgs e)
        {
            GuardarFormulario();
        }
        private void GuardarFormulario()
        {
            var user = worker.User.GetUserByUserName(User.Identity.Name);
            List<CheckBoxList> ChkBoxList = new List<CheckBoxList>();
            ChkBoxList.Add(CheckboxPermisosA);
            ChkBoxList.Add(CheckboxPermisosB);
            string Descripcion = "";
            Descripcion += "<b>Encargado: </b>"+worker.Employees.GetEmployeeById((worker.User.GetUserByUserName(Page.User.Identity.Name)).EmployeeID).FullName + "<br/>";
            Descripcion += "<b>Nombre de Usuario: </b>" + NombreTextBox.Text+"<br/>";
            Descripcion += "<b>Número de empleado: </b>" + NumeroTextBox.Text+"<br/>";
            Descripcion += "<b>Tipo de Acceso:</b> " + tipoRadioButtonList.SelectedItem.Text + "<br/>";
            Descripcion += "<b>Departamento: </b>" + DropDownDepartamento.SelectedItem.Text + "<br/>";
            Descripcion += "<b>Permisos Solicitados: </b><br/>";
            foreach (CheckBoxList CheckBox in ChkBoxList)
            {
                foreach (ListItem item in CheckBox.Items)
                {
                    if (item.Selected)
                    {
                        Descripcion += "* " + item.Text + "<br/>";
                    }
                }
            }
            Descripcion += "<b>Duplicar los permisos del usuario: </b>" + DuplicarTextBox.Text+"<br/>";
            Descripcion += "<b>Sistemas: </b><br>";
            foreach (ListItem  item in CheckboxSistema.Items)
            {
              
                if (item.Selected)
                {
                    Descripcion += item.Text+", ";
                }
            }
            Descripcion += "<br/>";
            Descripcion += "<b>Fecha requerida de creación : </b> " + fechaTextbox.Text+ "<br/>";
            Descripcion += "<b>Notas: </b><br/>";
            Descripcion += comentarioTextBox.Text;

            string guid =  worker.TSTickets.SubmitTicket("Solicitud de acceso al usuario",Descripcion,user.userID,102,2,false,false,3);
            var tickets = worker.TSTickets.Find(x => x.Referral == guid).FirstOrDefault();
            TSTicketsResponsible Responsible = new TSTicketsResponsible();
            Responsible.TicketsID = tickets.ID;
            Responsible.Responsible = 4092;
            Responsible.Assigned = true;
            Responsible.AssignDate = DateTime.Now;
            worker.ResponsibleTickets.Add(Responsible);
            worker.Complete();
            Responsible = new TSTicketsResponsible();
            Responsible.TicketsID = tickets.ID;
            Responsible.Responsible = 1253;
            Responsible.Assigned = true;
            Responsible.AssignDate = DateTime.Now;
            worker.ResponsibleTickets.Add(Responsible);
            worker.Complete();

            TicketsDetalle td = new TicketsDetalle();
            td.guardarLog(tickets.ID, "Creación del Tickets No."+ tickets.ID);
            td.EnviarEmailChanges(tickets.ID, "Nuevo Tickets No:"+ tickets.ID, "Nuevo Tickets <br/> Solicitud de Acceso");
            td.EnviarEmailChanges(tickets.ID, "Solicitud de acceso a Usuario",Descripcion);
     
            AlertHelpers.ShowAlertMessage(this, "Solicitud Enviada", "Se ha creado un tickets con su solicitud");
            Response.Redirect(Request.RawUrl);
        }
    }
}