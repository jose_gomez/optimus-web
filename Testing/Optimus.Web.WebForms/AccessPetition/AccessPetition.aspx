﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="AccessPetition.aspx.cs" Inherits="Optimus.Web.WebForms.AccessPetition.AccessPetition" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


    <script>
        jQuery(function ($) {
            $("#MainContent_NumeroTextBox").mask("9999");
        });
            </script>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Formulario de solicitud de usuario</h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label for="NombreTextBox">Nombre del empleado:</label>
                    <asp:TextBox runat="server" ID="NombreTextBox" class="form-control"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NombreTextBox" ForeColor="Red" ErrorMessage="*Ingrese un Nombre"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <label for="NumeroTextBox">Número empleado:</label>
                    <asp:TextBox runat="server" ID="NumeroTextBox" CssClass="form-control" MaxLength="4"></asp:TextBox>
                    <asp:RequiredFieldValidator runat="server" ControlToValidate="NumeroTextBox" ForeColor="Red" ErrorMessage="*Ingrese numero"></asp:RequiredFieldValidator>
                </div>
                <div class="form-group">
                    <label>Departamentos:</label>
                    <asp:DropDownList runat="server" ID="DropDownDepartamento" CssClass="form-control"></asp:DropDownList>
                </div>
                  <div class="form-group">
                    <label>Tipo de cambio:</label>
                      <asp:RadioButtonList runat="server" ID="tipoRadioButtonList">
                          <asp:ListItem>Nueva Contratación/Nuevo Usuario/ Nueva Posición</asp:ListItem>
                          <asp:ListItem>Modificación</asp:ListItem>
                          <asp:ListItem>Descontinuar Servicios</asp:ListItem>
                      </asp:RadioButtonList>
                </div>
                <hr />
                <div class="form-group row">
                    <h3>Permisos</h3>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="form-group">
                                <asp:CheckBoxList runat="server" ID="CheckboxPermisosA">
                                    <asp:ListItem>JTS</asp:ListItem>
                                    <asp:ListItem>Sistemas de Nomina</asp:ListItem>
                                    <asp:ListItem>Sistema de Almacén</asp:ListItem>
                                    <asp:ListItem>Sistema de Finanzas</asp:ListItem>
                                    <asp:ListItem>Optimus</asp:ListItem>
                                    <asp:ListItem>Requisiones de Compra</asp:ListItem>
                                    <asp:ListItem>Optimus Web</asp:ListItem>
                                    <asp:ListItem>Sistema de Tickets</asp:ListItem>
                                    <asp:ListItem>Sistemas de Pago</asp:ListItem>
                                </asp:CheckBoxList>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <asp:CheckBoxList runat="server" ID="CheckboxPermisosB">
                                <asp:ListItem>Cuanta Navision</asp:ListItem>
                                <asp:ListItem>Cuenta Syteline</asp:ListItem>
                                <asp:ListItem>Usuario PC</asp:ListItem>
                                <asp:ListItem>Correo Electrónico</asp:ListItem>
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </div>
                <hr />
                <div class="form-group">
                    <label for="DuplicarTextBox">Duplicar permisos del usuario</label>
                    <asp:TextBox runat="server" ID="DuplicarTextBox" class="form-control"></asp:TextBox>
                    <asp:CheckBoxList runat="server" RepeatDirection="Horizontal" ID="CheckboxSistema">
                        <asp:ListItem>JTS</asp:ListItem>
                        <asp:ListItem>Optimus</asp:ListItem>
                        <asp:ListItem>Optimus Web</asp:ListItem>
                    </asp:CheckBoxList>
                </div>
                <div class="form-group">
                    <label>Fecha de creación requerida</label>
                    <asp:TextBox runat="server" class="form-control datepicker" ID="fechaTextbox"></asp:TextBox>
                </div>
                <div class="form-group">
                    <label>Comentarios</label>
                    <asp:TextBox runat="server" TextMode="MultiLine" ID="comentarioTextBox" class="form-control"></asp:TextBox>
                </div>
                <hr />
                <div>
                    <asp:Button runat="server" Text="Enviar" CssClass="form-control btn btn-primary" OnClick="guardar" />
                </div>
            </div>
        </div>
    </div>
</asp:Content>
