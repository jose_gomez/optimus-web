﻿using Optimus.Web.BC.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.Security
{
    public partial class FrmResetPass : System.Web.UI.Page
    {
        BC.ServiceContainer ServCont = new BC.ServiceContainer();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["Code"] != null && Request.QueryString["UserId"] != null)
            {


                string id = Request.QueryString["Code"];
                string userName = Request.QueryString["UserId"];
                var worker = ServCont.GetUnitOfWork();
                var user = worker.User.Find(x => x.UserName == userName).FirstOrDefault();
                if (user == null)
                {
                    this.frmPass.Visible = false;
                    this.divMessage.Visible = true;
                    this.errorLabel.Text = "Este nombre de usuario no se encuentra registrado";
                }
                else
                {
                    string code = EncryptionUtility.Decrypt(id, user.UserID.ToString());
                    DA.SYResetPassHistory reset = ServCont.GetResetPassHistory().GetResetPassHistoryByID(code);
                    if (reset != null && reset.CreateDate.Value.AddMinutes(30) > DateTime.Now)
                    {
                        Session["ChangePWDUserName"] = user.UserName;
                        Session["ChangePWDCode"] = code;
                        Newpwd.Disabled = false;
                        Confirmpwd.Disabled = false;
                    }
                    else
                    {
                        Newpwd.Disabled = true;
                        Confirmpwd.Disabled = true;
                    }

                }
            }
            else
            {
                this.frmPass.Visible = false;
                this.divMessage.Visible = true;
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            if (Session["ChangePWDUserName"] != null && Session["ChangePWDCode"] != null)
            {
                string password = Newpwd.Value;
                string confirm = Confirmpwd.Value;
                if (password != confirm)
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "ErrorMessage",
                        "<script>renderErrorMessages(['El password y su confirmación deben de ser iguales'])</script>");
                }
                else if (string.IsNullOrEmpty(password) || string.IsNullOrEmpty(confirm))
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "ErrorMessage",
                         "<script>renderErrorMessages(['Por favor digite el password y su confirmación'])</script>");
                }
                else
                {
                    var worker = ServCont.GetUnitOfWork();
                    string code = Session["ChangePWDCode"].ToString();
                    DA.SYResetPassHistory reset = ServCont.GetResetPassHistory().GetResetPassHistoryByID(code);
                    if (reset.CreateDate.Value.AddMinutes(30) > DateTime.Now)
                    {
                        string username = Session["ChangePWDUserName"].ToString();
                        var user = worker.User.Find(x => x.UserName == username).FirstOrDefault();
                        if (user != null)
                        {
                            password = DRL.Utilities.Encryption.EncryptionUtility.HashPassword256(Newpwd.Value);
                            CustomMembership mem = new CustomMembership();
                            if (mem.ChangePassword(user.UserName, password, password))
                            {
                                ClientScript.RegisterStartupScript(this.GetType(), "ErrorMessage",
                                  "<script>renderValidationMessages(['Contraseña restaurada']);</script>");
                                bool access = mem.ValidateUser(user.UserName, Newpwd.Value);
                                if (access)
                                {
                                    FormsAuthentication.SetAuthCookie(user.UserName, true);
                                    FormsAuthentication.RedirectFromLoginPage(user.UserName, true);
                                }
                            }
                            else
                            {
                                this.frmPass.Visible = false;
                                this.divMessage.Visible = true;
                                this.errorLabel.Text = "El codigo de recuperación expiro. Por favor reintentar de nuevo.";
                            }

                        }
                    }
                    else
                    {
                        this.frmPass.Visible = false;
                        this.divMessage.Visible = true;
                        this.errorLabel.Text = "El codigo de recuperación expiro. Por favor reintentar de nuevo.";
                    }
                }

            }
        }
    }
}