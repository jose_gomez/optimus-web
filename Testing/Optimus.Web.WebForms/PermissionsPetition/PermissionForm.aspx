﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PermissionForm.aspx.cs" Inherits="Optimus.Web.WebForms.PermissionsPetition.PermissionForm" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <style>
        .form-control {
            max-width: none;
        }
    </style>
    <div id="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Solicitud de permisos</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" Font-Bold="true" Text="Código:" ID="Label2"></asp:Label>
                        <asp:TextBox runat="server" ID="codigoTextBox" CssClass="form-control" OnTextChanged="BuscarCodigo" AutoPostBack="true"></asp:TextBox>
                        <asp:RequiredFieldValidator runat="server" ControlToValidate="codigoTextBox" ForeColor="red" ErrorMessage="*Ingrese un codigo"></asp:RequiredFieldValidator>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <asp:Label runat="server" Font-Bold="true" Text="Nombre:" ID="Label1"></asp:Label>
                        <asp:Label runat="server" Text="Nombre:" CssClass="form-control" ID="NombreLabel"></asp:Label>
                    </div>
                    <div class="col-md-4">
                        <div>
                            <asp:Label runat="server" Font-Bold="true" Text="Fecha Efectiva:" ID="fechaLabel"></asp:Label>
                            <asp:TextBox runat="server" CssClass="form-control datepicker" ID="FechaTextBox" ClientIDMode="static"></asp:TextBox>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="FechaTextBox" ForeColor="red" ErrorMessage="*Ingrese una fecha"></asp:RequiredFieldValidator>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div>
                            <asp:Label runat="server" Font-Bold="true" Text="Rango hora:"></asp:Label>
                            <asp:DropDownList runat="server" ID="HoursStartDropdown" CssClass="form-control">
                                <asp:ListItem Value="1">1:00</asp:ListItem>
                                <asp:ListItem Value="2">2:00</asp:ListItem>
                                <asp:ListItem Value="3">3:00</asp:ListItem>
                                <asp:ListItem Value="4">4:00</asp:ListItem>
                                <asp:ListItem Value="5">5:00</asp:ListItem>
                                <asp:ListItem Value="6">6:00</asp:ListItem>
                                <asp:ListItem Value="7">7:00</asp:ListItem>
                                <asp:ListItem Value="8">8:00</asp:ListItem>
                                <asp:ListItem Value="9">9:00</asp:ListItem>
                                <asp:ListItem Value="10">10:00</asp:ListItem>
                                <asp:ListItem Value="11">11:00</asp:ListItem>
                                <asp:ListItem Value="12">12:00</asp:ListItem>
                                <asp:ListItem Value="13">13:00</asp:ListItem>
                                <asp:ListItem Value="14">14:00</asp:ListItem>
                                <asp:ListItem Value="15">15:00</asp:ListItem>
                                <asp:ListItem Value="16">16:00</asp:ListItem>
                                <asp:ListItem Value="17">17:00</asp:ListItem>
                                <asp:ListItem Value="18">18:00</asp:ListItem>
                                <asp:ListItem Value="19">19:00</asp:ListItem>
                                <asp:ListItem Value="20">20:00</asp:ListItem>
                                <asp:ListItem Value="21">21:00</asp:ListItem>
                                <asp:ListItem Value="22">22:00</asp:ListItem>
                                <asp:ListItem Value="23">23:00</asp:ListItem>
                                <asp:ListItem Value="24">24:00</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                    </div>
                    <div class="col-md-2">
                        <span style="font-weight: bold">hasta:</span>
                        <asp:DropDownList runat="server" ID="HoursEndDropdown" CssClass="form-control">
                            <asp:ListItem Value="1">1:00</asp:ListItem>
                            <asp:ListItem Value="2">2:00</asp:ListItem>
                            <asp:ListItem Value="3">3:00</asp:ListItem>
                            <asp:ListItem Value="4">4:00</asp:ListItem>
                            <asp:ListItem Value="5">5:00</asp:ListItem>
                            <asp:ListItem Value="6">6:00</asp:ListItem>
                            <asp:ListItem Value="7">7:00</asp:ListItem>
                            <asp:ListItem Value="8">8:00</asp:ListItem>
                            <asp:ListItem Value="9">9:00</asp:ListItem>
                            <asp:ListItem Value="10">10:00</asp:ListItem>
                            <asp:ListItem Value="11">11:00</asp:ListItem>
                            <asp:ListItem Value="12">12:00</asp:ListItem>
                            <asp:ListItem Value="13">13:00</asp:ListItem>
                            <asp:ListItem Value="14">14:00</asp:ListItem>
                            <asp:ListItem Value="15">15:00</asp:ListItem>
                            <asp:ListItem Value="16">16:00</asp:ListItem>
                            <asp:ListItem Value="17">17:00</asp:ListItem>
                            <asp:ListItem Value="18">18:00</asp:ListItem>
                            <asp:ListItem Value="19">19:00</asp:ListItem>
                            <asp:ListItem Value="20">20:00</asp:ListItem>
                            <asp:ListItem Value="21">21:00</asp:ListItem>
                            <asp:ListItem Value="22">22:00</asp:ListItem>
                            <asp:ListItem Value="23">23:00</asp:ListItem>
                            <asp:ListItem Value="24">24:00</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="row">
                                            <div class="col-md-4">
                                                
                        <asp:Label runat="server" Text="Razón" Font-Bold="true"></asp:Label>
                       <asp:DropDownList runat="server" ID="RazonDropDownList" CssClass="form-control"></asp:DropDownList>
                                            </div>
                                        </div>
                <div class="row">
                    <div class="col-md-12">
                        <asp:Label runat="server" Text="Comentarios" Font-Bold="true"></asp:Label>
                        <asp:TextBox runat="server" TextMode="MultiLine" ID="comentarioTextBox" CssClass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6" style="padding-top:10px">
                        <asp:Button runat="server" ID="EnviarButton" CssClass="btn btn-primary" Text="Enviar Listado" OnClick="GuardarPermisos" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
