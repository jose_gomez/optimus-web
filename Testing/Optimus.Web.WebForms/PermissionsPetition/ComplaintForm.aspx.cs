﻿using Optimus.Web.BC;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.WebForms.Tickets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Optimus.Web.WebForms.PermissionsPetition
{
    public partial class ComplaintForm : System.Web.UI.Page
    {
        private ServiceContainer container;
        private UnitOfWork worker;
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        protected void Page_Load(object sender, EventArgs e)
        {
            var TypeReclamtion = ContextSQL.HRReclamationTypes.ToList();
            ReclamacionDropDown.DataSource = TypeReclamtion;
            ReclamacionDropDown.DataTextField = "HRReclamationTypes";
            ReclamacionDropDown.DataValueField = "ReclamationTypesID";
            ReclamacionDropDown.DataBind();

        }
        protected void EnviarTicketReclamacion(object sender,EventArgs e)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();

            TicketsDetalle dt = new TicketsDetalle();       
            string Descripcion = string.Empty;
            int emplID = int.Parse(CodigoTextBox.Text);
            var Emp = worker.Employees.GetEmployeeById(emplID);
            var user = worker.User.GetUserByUserName(Page.User.Identity.Name);
            Descripcion += "<b>Empleado:</b><br/>";
            Descripcion += Emp.FullName;
            Descripcion += "<b>Tipo Reclamación:</b><br/>";
            Descripcion += ReclamacionDropDown.SelectedItem.Text + "<br/>";
            Descripcion += "<b>Reclamación:</b> <br/>";
            Descripcion += txtDescription.Text;
            string guid =  worker.TSTickets.SubmitTicket("Reclamación: " + Emp.FullName + " (" + CodigoTextBox.Text + ")",Descripcion,user.userID, 25);
            var tickets = worker.TSTickets.Find(x => x.Referral == guid).FirstOrDefault();
            dt.EnviarEmailChanges(tickets.ID, "Nuevo Tickets " + tickets.ID, "Nuevo Tickets Reclamacion");
            Response.Redirect(Request.RawUrl);
        }
    }
}