﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="PermissionsPetitionAprobation.aspx.cs" Inherits="Optimus.Web.WebForms.PermissionsPetition.PermissionsPetitionAprobation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <style>
        .bs-callout {
            padding: 20px;
            margin: 20px 0;
            border: 1px solid #eee;
            border-left-width: 5px;
            border-radius: 3px;
        }

            .bs-callout h4 {
                margin-top: 0;
                margin-bottom: 5px;
            }

            .bs-callout p:last-child {
                margin-bottom: 0;
            }

            .bs-callout code {
                border-radius: 3px;
            }

            .bs-callout + .bs-callout {
                margin-top: -5px;
            }

        .bs-callout-default {
            border-left-color: #777;
        }

            .bs-callout-default h4 {
                color: #777;
            }

        .bs-callout-primary {
            border-left-color: #428bca;
        }

            .bs-callout-primary h4 {
                color: #428bca;
            }

        .bs-callout-success {
            border-left-color: #5cb85c;
        }

            .bs-callout-success h4 {
                color: #5cb85c;
            }

        .bs-callout-danger {
            border-left-color: #d9534f;
        }

            .bs-callout-danger h4 {
                color: #d9534f;
            }

        .bs-callout-warning {
            border-left-color: #f0ad4e;
        }
         .bs-callout-warning h4 {
                color: #f0ad4e;
            }

        .bs-callout-info {
            border-left-color: #5bc0de;
        }

            .bs-callout-info h4 {
                color: #5bc0de;
            }
    </style>
    <div id="container">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3>Listado de permisos</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-md-3">
                        <asp:Label runat="server" Font-Bold="true" Text="Buscar por Número empleado:"></asp:Label>
                        <asp:TextBox runat="server" CssClass="form-control" ID="EmployeeIDTextBox" OnTextChanged="BuscarByID" AutoPostBack="true"></asp:TextBox>
                    </div>
                </div>
                <br />
                <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse2">Listado de permisos sin aprobar. <span id="CantidadPermisosPendientes" runat="server" class="badge">5</span></a>
                        </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse">
                        <div class="panel-body">
                         <%--   <asp:Repeater runat="server" ID="PermissionRepeater" OnItemDataBound="PermissionRepeater_ItemDataBound">
                                <ItemTemplate>
                                    <div class="bs-callout bs-callout-default">
                                        <h4><%#Eval("FullName")%></h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Fecha Solicitud:</b><%#Eval("DateEfective")%>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Estado:</b><asp:Label runat="server" ID="StatusLabel" Text='<%#Eval("Status")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Desde:</b> <%#Eval("HourStart")%>:00 <b>hasta</b> <%#Eval("HourEnd")%>:00
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Razón:</b> <%#Eval("PermissionReason")%>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Motivo</label>
                                                <div>
                                                    <p>
                                                        <%#Eval("Comment")%>
                                                    <p />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:LinkButton runat="server" ID="AprobarButton" Text="Aprobar" OnCommand="Aprobar" CommandName='<%#Eval("PermissionsEmployeesID")%>' CommandArgument='<%#Eval("PermissionsEmployeesID")%>'></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:Repeater>--%>
                            <asp:ListView runat="server" ID="PermissionRepeatera" OnItemDataBound="DataBound">
                                <ItemTemplate>
                                    <div class="bs-callout bs-callout-default">
                                        <h4><%#Eval("FullName")%></h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Fecha Solicitud:</b><%#Eval("DateEfective")%>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Estado:</b><asp:Label runat="server" ID="StatusLabel" Text='<%#Eval("Status")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Desde:</b> <%#Eval("HourStart")%>:00 <b>hasta</b> <%#Eval("HourEnd")%>:00
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Razón:</b> <%#Eval("PermissionReason")%>
                                            </div>
                                        </div>
                                        
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Motivo</label>
                                                <div>
                                                    <p>
                                                        <%#Eval("Comment")%>
                                                    <p />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:LinkButton runat="server" ID="AprobarButton" Text="Aprobar" OnCommand="Aprobar" CommandName='<%#Eval("PermissionsEmployeesID")%>' CommandArgument='<%#Eval("PermissionsEmployeesID")%>'></asp:LinkButton>
                                                <a  onClick="window.open(this.href, this.target, 'width=900,height=500');return false;"; target="popup" 
                                                 href='<%# string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>
                                                    <%--href='<%# string.Format("http://drl-it-testing//ReportServer/Pages/ReportViewer.aspx?/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>--%>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                    </div>
                 <div class="row">
                <div class="panel panel-default">
                    <div class="panel-heading ">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapse3">Listado de permisos aprobados. <span id="CantidadAprobados" runat="server" class="badge">5</span></a>
                        </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse">
                        <div class="panel-body">                       
                            <asp:ListView runat="server" ID="AprovadoListView" OnItemDataBound="DataBoundAProved">
                                <ItemTemplate>
                                    <div class="bs-callout bs-callout-success">
                                        <h4 style="color:black"><%#Eval("FullName")%></h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Fecha Solicitud:</b><%#Eval("DateEfective")%>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Estado:</b><asp:Label runat="server" ID="StatusLabel" Text='<%#Eval("Status")%>'></asp:Label>
                                            </div>
                                            <div class="col-md-4">
                                                <b>Desde:</b> <%#Eval("HourStart")%>:00 <b>hasta</b> <%#Eval("HourEnd")%>:00
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <b>Razón:</b> <%#Eval("PermissionReason")%>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-8">
                                                <label>Motivo</label>
                                                <div>
                                                    <p>
                                                        <%#Eval("Comment")%>
                                                    <p />
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <asp:LinkButton runat="server" ID="AprobarButton" Text="Aprobar" OnCommand="Aprobar" CommandName='<%#Eval("PermissionsEmployeesID")%>' CommandArgument='<%#Eval("PermissionsEmployeesID")%>'></asp:LinkButton>
                                                   <a  onClick="window.open(this.href, this.target, 'width=900,height=500');return false;"; target="popup" 
                                                 href='<%# string.Format("http://drl-sevr-data3//ReportServer/Pages/ReportViewer.aspx?/RRHH/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>
                                                   <%-- href='<%# string.Format("http://drl-it-testing//ReportServer/Pages/ReportViewer.aspx?/Permisos&Employees={0}&rc:Parameters=False",Eval("PermissionsEmployeesID"))%>' >Exportar para impresión</a>--%>
                                            </div>
                                        </div>
                                    </div>

                                </ItemTemplate>
                            </asp:ListView>
                        </div>
                    </div>
                </div>
                    </div>
            </div>
        </div>
    </div>
</asp:Content>
