﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Core
{
    public class OptimusConstants
    {
        public const string OPTIMUS_API_ROUTE_PREFIX_POSITION = "optimus/position";
        public const string OPTIMUS_API_ROUTE_GET_ALL_POSITIONS = OPTIMUS_API_ROUTE_PREFIX_POSITION + "/get";
        public const string OPTIMUS_API_ROUTE_GET_ACTIVE_POSITIONS = OPTIMUS_API_ROUTE_PREFIX_POSITION + "/getby/status/active";
        public const string OPTIMUS_API_ROUTE_GET_INACTIVE_POSITIONS = OPTIMUS_API_ROUTE_PREFIX_POSITION + "/getby/status/inactive";
    }
}
