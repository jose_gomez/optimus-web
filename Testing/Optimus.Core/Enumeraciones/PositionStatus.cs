﻿namespace Optimus.Core.Enumeraciones
{
    public enum PositionStatus
    {
        ACTIVO = 31,
        INACTIVO = 32
    }
}