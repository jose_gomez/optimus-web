﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;

namespace Optimus.API.Controllers
{
    public class EmployeeController : ApiController
    {
        Web.BC.Repositories.API.Employee _EmployeeRepo = new Web.BC.Repositories.API.Employee();
        // GET: optimus/Employee
        public IHttpActionResult Get()
        {
            return NotFound();
        }

        // GET: optimus/Employee/5
        public IHttpActionResult Get(int id)
        {
            var employee = _EmployeeRepo.GetEmployeeByID(id);
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        // GET: optimus/Employee/getby/position/5
        [Route("optimus/employee/getby/position/{id}")]
        public IHttpActionResult GetEmployeesByPosition(int id)
        {
            var employee = _EmployeeRepo.GetEmployeesByPosition(id);
            if (employee == null)
            {
                return NotFound();
            }
            return Ok(employee);
        }

        // POST: optimus/Employee
        public IHttpActionResult Post([FromBody]string value)
        {
            DTO.Employee employee = DTO.Factory.EmployeeFactory.Deserialize(value);
            try
            {
                if (employee != null)
                {
                    _EmployeeRepo.Save(employee);
                }
                return Ok();
            }
            catch (Core.Exceptions.ValidationException ex)
            {
                return BadRequest(ex.Message);
            }
            catch (Exception ex)
            {
                return BadRequest(Core.OptimusMessages.ERROR_MESSAGE);
            }
        }

        // PUT: optimus/Employee/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: optimus/Employee/5
        public void Delete(int id)
        {
        }
    }
}
