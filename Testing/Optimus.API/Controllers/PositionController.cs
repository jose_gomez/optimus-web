﻿using System.Web.Http;

namespace Optimus.API.Controllers
{
    public class PositionController : ApiController
    {

        #region Private Fields

        private Web.BC.Repositories.API.Position _PositionRepo = new Web.BC.Repositories.API.Position();

        #endregion Private Fields

        #region Public Methods

        // DELETE: optimus/position/delete/5
        public void Delete(int id)
        {
        }

        // GET: optimus/position/get
        public IHttpActionResult Get()
        {
            var positions = _PositionRepo.GetAllPositions();
            if (positions == null || positions.Count == 0)
            {
                return NotFound();
            }
            return Ok(positions);
        }

        // GET: optimus/position/get/5
        public IHttpActionResult Get(int id)
        {
            var position = _PositionRepo.GetPositionByID(id);
            if (position == null)
            {
                return NotFound();
            }
            return Ok(position);
        }

        //GET: optimus/position/active
        [Route("optimus/position/getby/status/active")]
        public IHttpActionResult GetActive()
        {
            var positions = _PositionRepo.GetPositionsByStatus((int)Core.Enumeraciones.PositionStatus.ACTIVO);
            if (positions == null || positions.Count == 0)
            {
                return NotFound();
            }
            return Ok(positions);
        }

        //GET: optimus/position/status/5
        [Route("optimus/position/getby/status/{id}")]
        public IHttpActionResult GetByStatus(int id)
        {
            var positions = _PositionRepo.GetPositionsByStatus(id);
            if (positions == null || positions.Count == 0)
            {
                return NotFound();
            }
            return Ok(positions);
        }

        //GET: optimus/position/inactive
        [Route("optimus/position/getby/status/inactive")]
        public IHttpActionResult GetInactive()
        {
            var positions = _PositionRepo.GetPositionsByStatus((int)Core.Enumeraciones.PositionStatus.INACTIVO);
            if (positions == null || positions.Count == 0)
            {
                return NotFound();
            }
            return Ok(positions);
        }

        // POST: optimus/position/post
        public void Post([FromBody]string value)
        {
        }

        // PUT: optimus/position/put/5
        public void Put(int id, [FromBody]string value)
        {
        }

        #endregion Public Methods

    }
}