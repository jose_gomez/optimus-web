﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class EmployeeFactory
    {

        #region Public Methods

        public static List<Employee> DeserializeList(string data)
        {
            List<Employee> employees = null;
            employees = JsonConvert.DeserializeObject<List<Employee>>(data);
            return employees;
        }

        public static Employee Deserialize(string data)
        {
            Employee employee = null;
            employee = JsonConvert.DeserializeObject<Employee>(data);
            return employee;
        }

        public static string Serialize(Employee employee)
        {
            string data = JsonConvert.SerializeObject(employee);
            return data;
        }

        #endregion Public Methods

    }
}