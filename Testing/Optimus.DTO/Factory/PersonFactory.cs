﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO.Factory
{
    public class PersonFactory
    {
        #region Public Methods

        public static List<Person> DeserializeList(string data)
        {
            List<Person> persons = null;
            persons = JsonConvert.DeserializeObject<List<Person>>(data);
            return persons;
        }

        public static Person Deserialize(string data)
        {
            Person person = null;
            person = JsonConvert.DeserializeObject<Person>(data);
            return person;
        }

        public static string Serialize(Person person)
        {
            string data = JsonConvert.SerializeObject(person);
            return data;
        }

        #endregion Public Methods
    }
}
