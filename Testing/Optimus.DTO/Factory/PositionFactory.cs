﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace Optimus.DTO.Factory
{
    public class PositionFactory
    {
        #region Public Methods

        public static List<Position> DeserializeList(string data)
        {
            List<Position> positions = null;
            positions = JsonConvert.DeserializeObject<List<Position>>(data);
            return positions;
        }

        public static Position Deserialize(string data)
        {
            Position position = null;
            position = JsonConvert.DeserializeObject<Position>(data);
            return position;
        }

        public static string Serialize(Position position)
        {
            string data = JsonConvert.SerializeObject(position);
            return data;
        }

        #endregion Public Methods
    }
}