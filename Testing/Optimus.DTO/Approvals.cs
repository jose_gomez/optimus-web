﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class Approvals
    {
        public int RequestID { get; set; }
        public int RequestTypeID { get; set; }
        public bool IsApproved { get; set; }
        public string Comment { get; set; }
        public int UserID { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? ModifyDate { get; set; }
    }
}
