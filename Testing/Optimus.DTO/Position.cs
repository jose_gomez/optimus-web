﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class Position
    {
        public int PositionID { get; set; }
        public string PositionName { get; set; }
        public string Description { get; set; }
        public int? Status { get; set; }
        public int? CategoryID { get; set; }
        public int? LevelID { get; set; }
        public int? DepartmentID { get; set; }
        public int? SuperiorPositionID { get; set; }

    }
}
