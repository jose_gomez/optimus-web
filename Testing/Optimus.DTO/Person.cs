﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.DTO
{
    public class Person
    {
        public int PersonID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PictureGUID { get; set; }
        public string Cedula { get; set; }
        public DateTime BirthDate { get; set; }
    }
}
