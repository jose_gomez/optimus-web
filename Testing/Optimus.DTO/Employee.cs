﻿using System;

namespace Optimus.DTO
{
    public class Employee
    {
        #region Public Properties

        public int CompanyID { get; set; }
        public DateTime? CreateDate { get; set; }
        public int EmployeeID { get; set; }
        public DateTime? ModifyDate { get; set; }
        public int PersonID { get; set; }
        public int PositionID { get; set; }
        public int StatusID { get; set; }
        public int UserID { get; set; }
        public Position Position { get; set; }
        public Person Person { get; set; }
        public bool? ProductionCompensation { get; set; }
        public double? BaseSalary { get; set; }
        public byte[] Picture { get; set; }

        #endregion Public Properties
    }
}