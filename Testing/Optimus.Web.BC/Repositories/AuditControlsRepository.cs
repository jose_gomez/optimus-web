﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using System.Net.Mail;

namespace Optimus.Web.BC.Repositories
{
    internal class AuditControlsRepository : IAuditControls
    {
        public List<SHAuditControl> GetAllAuditControl()
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

            var select = (from a in context.SHAuditControls select a).ToList();
            return select;
        }

        public List<SHAuditControl> GetAuditControlByID(int controlID)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

            return (from a in context.SHAuditControls where (a.ControlID == controlID) select a).ToList();
        }

        public List<SHAuditControl> GetAuditControlByName(string controlName)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.

            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
            return (from a in context.SHAuditControls where (a.NameAudit == controlName) select a).ToList();
        }

        public List<SYEmailEmployee> GetEmailsByAuditControlID(string controlName)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

            var select = (from a in context.SYEmailEmployees
                         join b in context.SYAlertsEmails on a.EmailID equals b.EmailID
                         join c in context.SYAlertsTypes on b.AlertTypesID equals c.AlertTypesID
                         where (c.AlertTypes == controlName)
                         select a).ToList();

            return select;
        }
        public void SendEmailByAlertTypeID(int TypeID,string subject,string body)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
            var select = (from a in context.SYEmailEmployees
                          join b in context.SYAlertsEmails on a.EmailID equals b.EmailID
                          join c in context.SYAlertsTypes on b.AlertTypesID equals c.AlertTypesID
                          where (c.AlertTypesID == TypeID)
                          select a).ToList();

            if (select.Count != 0)
            {
                foreach (var a in select)
                {
                    MailAddress address = new MailAddress(a.Email);
                    List<MailAddress> recipients = new List<MailAddress>() { address };

                    DRL.Utilities.Mail.Mailer.SendMail(subject, recipients, body, true);
                }
            }

        }


        public List<SHAuditControl> SaveAuditControlByID(SHAuditControl controlAudit)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

            SHAuditControl auditControl = context.SHAuditControls.Where(tab => tab.ControlID == controlAudit.ControlID).FirstOrDefault();

            if (auditControl == null)
            {
                auditControl = new SHAuditControl();
                auditControl.Create_dt = DateTime.Now;
                context.SHAuditControls.InsertOnSubmit(auditControl);
            } // // Alertas para los Artículos Nuevos
            //container.GetAlertsService().SendAlert("Modulo control de inventario -- Optimus --Nuevo artículo registrado. \n Item # = " + this.idToolsP + "\n Nombre = " + txtNameTools.Text.ToUpper().Trim() + "\n Registrado por = " + userService.GetLoggedOnUser().Username, "Nuevo artículo", "ALERTAS DE NUEVOS MATERIALES");
            //

            auditControl.NameAudit = controlAudit.NameAudit;
            auditControl.DescriptionAudit = controlAudit.DescriptionAudit;
            auditControl.Answer_type = controlAudit.Answer_type;
            auditControl.Instrucciones = controlAudit.Instrucciones;
            auditControl.valorization_from = controlAudit.valorization_from;
            auditControl.valorization_to = controlAudit.valorization_to;
            auditControl.Images_number = controlAudit.Images_number;
            auditControl.Modify_dt = DateTime.Now;
            auditControl.Users = controlAudit.Users;

            context.SubmitChanges();

            return (from a in context.SHAuditControls where (a.ControlID == auditControl.ControlID) select a).ToList();
        }

        public List<SYEmailEmployee> SaveEmailsByAuditControlID(SYEmailEmployee emailEmployee, string alertType, int controlID, int emailUserID)
        {
            DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
            ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

            SYEmailEmployee auditEmail = context.SYEmailEmployees.Where(tab => tab.EmailID == emailEmployee.EmailID && tab.EmailUserID == emailUserID).FirstOrDefault();

            if(auditEmail == null)
            {
                auditEmail = new SYEmailEmployee();
                auditEmail.create_dt = DateTime.Now;
                context.SYEmailEmployees.InsertOnSubmit(auditEmail);
            }
            auditEmail.EmailUserID = emailEmployee.EmailUserID;
            auditEmail.Email = emailEmployee.Email;
            auditEmail.UserID = emailEmployee.UserID;

            context.SubmitChanges();

            SYAlertsType alertTp = context.SYAlertsTypes.Where(tab => tab.AlertTypes == alertType).FirstOrDefault();
            if (alertTp == null)
            {
                alertTp = new SYAlertsType();
                alertTp.create_dt = DateTime.Now;
                context.SYAlertsTypes.InsertOnSubmit(alertTp);
            }
            alertTp.AlertTypes = alertType;

            context.SubmitChanges();
            ///////////////////

            SYAlertsEmail alertEmail = context.SYAlertsEmails.Where(tab => tab.AlertsEmailID == 0).FirstOrDefault();
            if (alertEmail == null)
            {
                alertEmail = new SYAlertsEmail();
                alertEmail.create_dt = DateTime.Now;
                context.SYAlertsEmails.InsertOnSubmit(alertEmail);
            }

            alertEmail.EmailID = auditEmail.EmailID;
            alertEmail.AlertTypesID = alertTp.AlertTypesID;
            alertEmail.UserID = auditEmail.UserID;
            context.SubmitChanges();
            ////////////////////////
            return (from a in context.SYEmailEmployees where(a.EmailID == auditEmail.EmailID) select a).ToList();
            
        }
    }
}
