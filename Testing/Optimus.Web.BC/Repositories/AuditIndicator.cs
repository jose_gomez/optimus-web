﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Repositories
{
    internal class AuditIndicator : IAuditIndicator
    {
        DataClassesDataContext context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public List<Models.AuditIndicator> GetAllIndicator()
        {
            var select = (from a in context.SHAuditIndicators
                          join b in context.SHAuditCategories on a.AuditCategory equals b.ID
                          join c in context.SHAuditControls on b.ControlID equals c.ControlID
                          select new Models.AuditIndicator()
                          {
                              IdIndicator = a.ID,
                              Indicator = a.AuditIndicator,
                              IdCategory = b.ID,
                              Category = b.Categoria,
                              IdControl = c.ControlID,
                              ControlName = c.NameAudit
                          }).OrderBy(x => x.ControlName).ToList();
            return select;
        }
        public List<SHAuditIndicator> GetIndicatorByID(int idIndic)
        {
            var select = (from a in context.SHAuditIndicators where (a.ID == idIndic) select a).ToList();
            return select;
        }
        public List<Models.AuditIndicator> GetAllIndicatorByControlID(int controlID)
        {
            var select = (from a in context.SHAuditIndicators
                          join b in context.SHAuditCategories on a.AuditCategory equals b.ID
                          join c in context.SHAuditControls on b.ControlID equals c.ControlID
                          where (c.ControlID == controlID)
                          select new Models.AuditIndicator()
                          {
                              IdIndicator = a.ID,
                              Indicator = a.AuditIndicator,
                              IdCategory = b.ID,
                              Category = b.Categoria
                          }).OrderBy(x => x.Category).ToList();
            return select;
        }

        public List<VwAuditIndicatorOrder> GetAllIndicatorByCategoryID(int idCat)
        {
            var select = (from a in context.VwAuditIndicatorOrders where (a.AuditCategory == idCat) select a).ToList();
            return select;
        }

        public void SaveByID(SHAuditIndicator indicator)
        {
            SHAuditIndicator audInd = context.SHAuditIndicators.Where(tab => tab.ID == indicator.ID).FirstOrDefault();

            if(audInd == null)
            {
                audInd = new SHAuditIndicator();
                audInd.CreateDate = DateTime.Now;
                context.SHAuditIndicators.InsertOnSubmit(audInd);
            }

            audInd.AuditIndicator = indicator.AuditIndicator;
            audInd.AuditCategory = indicator.AuditCategory;
            audInd.User = indicator.User;
            context.SubmitChanges();
        }
    }
}
