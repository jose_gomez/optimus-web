﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
    internal class RepoPositions : IPositions
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Delete(HRPositionCompetence relacion)
        {
            HRPositionCompetence dt = (from a in Context.HRPositionCompetences
                                       where a.PositionID == relacion.PositionID && a.CompetenceID == relacion.CompetenceID
                                       select a).FirstOrDefault();

            if (dt != null)
            {
                Context.HRPositionCompetences.DeleteOnSubmit(dt);
                Context.SubmitChanges();
            }
        }

        public void Delete(HROrganizationChart organizacion)
        {
            Context.DeleteHROrganizationChart(organizacion.SuperiorPositionID, organizacion.SubordinatePositionID);
        }

        public void Delete(HRDeparmentsPosition relacion)
        {
            Context.DeleteDepartementsPositions(relacion.DeparmentID, relacion.PositionID);
        }

        public void Delete(HRPositionAnalisysDetail detalle)
        {
            HRPositionAnalisysDetail dt = (from a in Context.HRPositionAnalisysDetails
                                           where a.DetailsID == detalle.DetailsID
                                           select a).FirstOrDefault();

            if (dt != null)
            {
                Context.HRPositionAnalisysDetails.DeleteOnSubmit(dt);
                Context.SubmitChanges();
            }
        }

        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRCompensationBenefit> GetAllCompensationBenefit()
        {
            var result = (from a in Context.HRCompensationBenefits
                          select a).OrderBy(x => x.Name).ToList();
            return result;
        }

        public List<HRCompensationBenefit> GetAllCompensationBenefitForEdit()
        {
            var result = (from a in Context.HRCompensationBenefits
                          where a.CompensationID < 2 || a.CompensationID > 4
                          select a).OrderBy(x => x.Name).ToList();
            return result;
        }

        public List<HRCompetencesCategory> GetAllCompetencesCategory()
        {
            var lista = (from a in Context.HRCompetencesCategories
                         select a).ToList();

            return lista;
        }

        public List<HRPosition> GetAllPositions()
        {
            var lista = (from a in Context.HRPositions
                         select a).OrderBy(x => x.PositionName).ToList();
            return lista;
        }

        public List<HRSalaryLevel> GetAllSalaryLevelOrderBySequence()
        {
            var result = (from a in Context.HRSalaryLevels
                          select a).OrderBy(x => x.LevelSequence).ToList();
            return result;
        }

        public List<HRSalaryLevel> GetAllSalaryLevels()
        {
            var lvls = (from a in Context.HRSalaryLevels
                        select a).ToList();
            return lvls;
        }

        public HRSalaryLevel GetBasicSalaryLevel()
        {
            var result = (from a in Context.HRSalaryLevels
                          select a).OrderBy(x => x.LevelSequence).FirstOrDefault();
            return result;
        }

        public HRCompensationBenefit GetCompensationBenefitByID(int id)
        {
            var compensation = (from a in Context.HRCompensationBenefits
                                where a.CompensationID == id
                                select a).FirstOrDefault();
            return compensation;
        }

        public Competencia GetCompetenceBy(int competenceID, int positionID)
        {
            var lista = (from a in Context.HRCompetences
                         join b in Context.HRPositionCompetences on a.CompetenceID equals b.CompetenceID
                         where b.PositionID == positionID && b.CompetenceID == competenceID
                         select new Competencia
                         {
                             competenciaID = a.CompetenceID,
                             Descripcion = a.Description,
                             Nombre = a.Name,
                             Nivel = b.Level
                         }).FirstOrDefault();
            return lista;
        }

        public List<HRCompetence> GetCompetenceByCategoryID(int categortID)
        {
            var lista = (from a in Context.HRCompetences
                         where a.CompetenceCategoryID == categortID
                         select a).ToList();
            return lista;
        }

        public HRCompetence GetCompetenceByID(int competenceID)
        {
            var comp = (from a in Context.HRCompetences
                        where a.CompetenceID == competenceID
                        select a).FirstOrDefault();
            return comp;
        }

        public HRCompetencesCategory GetCompetencesCategoryBYID(int id)
        {
            var cate = (from a in Context.HRCompetencesCategories
                        where a.CompetenceCategoryID == id
                        select a).FirstOrDefault();

            return cate;
        }

        public List<HRCompetencesCategory> GetCompetencesCategoryBYPosicionID(int id)
        {
            var lista = (from a in Context.HRCompetencesCategories
                         join b in Context.HRCompetences on a.CompetenceCategoryID equals b.CompetenceCategoryID
                         join c in Context.HRPositionCompetences on b.CompetenceID equals c.CompetenceID
                         where c.PositionID == id
                         select a).Distinct().ToList();

            return lista;
        }

        public List<Competencia> GetCompetenciaBYPositionID(int id)
        {
            var lista = (from a in Context.HRCompetences
                         join b in Context.HRPositionCompetences on a.CompetenceID equals b.CompetenceID
                         where b.PositionID == id
                         select new Competencia
                         {
                             competenciaID = a.CompetenceID,
                             Descripcion = a.Description,
                             Nombre = a.Name,
                             Nivel = b.Level
                         }).ToList();
            return lista;
        }

        public List<Competencia> GetCompetenciasByCategory(int categoryID)
        {
            var lista = (from a in Context.HRCompetences
                         where a.CompetenceCategoryID == categoryID
                         select new Competencia
                         {
                             competenciaID = a.CompetenceID,
                             Descripcion = a.Description,
                             Nombre = a.Name
                         }).ToList();
            return lista;
        }

        public List<Competencia> GetCompetenciasByCategoryByID(int categoryID, int positionID)
        {
            var lista = (from a in Context.HRCompetences
                         join b in Context.HRPositionCompetences on a.CompetenceID equals b.CompetenceID
                         where a.CompetenceCategoryID == categoryID && b.PositionID == positionID
                         select new Competencia
                         {
                             competenciaID = a.CompetenceID,
                             Descripcion = a.Description,
                             Nombre = a.Name,
                             Nivel = b.Level
                         }).ToList();
            return lista;
        }

        public HRDeparmentsPosition GetDepartmentPosition(int positionID)
        {
            var relacion = (from a in Context.HRDeparmentsPositions
                            where a.PositionID == positionID
                            select a).FirstOrDefault();
            return relacion;
        }

        public List<VWMatrizSalarial> GetMatrizSalarialByPositionID(int positionID)
        {
            var result = (from a in Context.VWMatrizSalarials
                          where a.PositionID == positionID
                          select new VWMatrizSalarial
                          {
                              MarcoGeneral = a.MarcoGeneral,
                              D = Math.Round((double)a.D, 2, MidpointRounding.ToEven),
                              C = Math.Round((double)a.C, 2, MidpointRounding.ToEven),
                              PROMEDIO = Math.Round((double)a.PROMEDIO, 2, MidpointRounding.ToEven),
                              B = Math.Round((double)a.B, 2, MidpointRounding.ToEven),
                              A = Math.Round((double)a.A, 2, MidpointRounding.ToEven),
                              CompensationID=a.CompensationID,
                              PositionID=a.PositionID
                          }).ToList();

            return result;
        }

        public HROrganizationChart GetOrganizationChart(int subordinatePositionID)
        {
            var org = (from a in Context.HROrganizationCharts
                       where a.SubordinatePositionID == subordinatePositionID
                       select a).FirstOrDefault();
            return org;
        }

        public List<HRPositionAnalisysDetail> GetPositionAnalisyDeatailByAnalisyID(int id)
        {
            var detalles = (from a in Context.HRPositionAnalisysDetails
                            where a.PositionAnalisysID == id
                            select a).ToList();
            return detalles;
        }

        public HRPosition GetPositionByName(string name)
        {
            var position = (from a in Context.HRPositions
                            where a.PositionName == name.Trim().ToUpper()
                            select a).FirstOrDefault();
            return position;
        }

        public HRPositionAnalisysDetail GetPositionDetailBy(string funtion, int positionID)
        {
            var lista = (from a in Context.HRPositionAnalisies
                         join b in Context.HRPositionAnalisysDetails on a.PositionAnalysisID equals b.PositionAnalisysID
                         where a.PositionID == positionID && b.Funtion == funtion
                         select b).FirstOrDefault();
            return lista;
        }

        public HRPositionAnalisy GetPositionsAnalisyBYPositionID(int id)
        {
            var analisis = (from a in Context.HRPositionAnalisies
                            where a.PositionID == id
                            select a).FirstOrDefault();
            return analisis;
        }

        public List<HRPosition> GetPositionsBy(string name)
        {
            var lista = (from a in Context.HRPositions
                         where a.PositionName.Contains(name)
                         select a).Take(10).ToList();
            return lista;
        }

        public List<HRPosition> GetPositionsByDeparmentID(int id)
        {
            var lista = (from a in Context.HRPositions
                         join b in Context.HRDeparmentsPositions on a.PositionID equals b.PositionID
                         where b.DeparmentID == id
                         select a).OrderBy(x => x.PositionName).ToList();

            return lista;
        }

        public HRPosition GetPositionsByID(int id)
        {
            var lista = (from a in Context.HRPositions
                         where a.PositionID == id
                         select a).FirstOrDefault();
            return lista;
        }

        public HRSalaryLevel GetSalaryLevelByID(int id)
        {
            var lvl = (from a in Context.HRSalaryLevels
                       where a.SalaryLevelID == id
                       select a).FirstOrDefault();
            return lvl;
        }

        public List<HRPosition> GetSubordinadosByPositionID(int id)
        {
            var lista = (from a in Context.HRPositions
                         join b in Context.HROrganizationCharts on a.PositionID equals b.SubordinatePositionID
                         where b.SuperiorPositionID == id
                         select a).ToList();

            return lista;
        }

        public void Save(HRCompensationBenefit compensacion)
        {
            if (string.IsNullOrWhiteSpace(compensacion.Name))
            {
                throw new Core.Exceptions.ValidationException("Ingresar el nombre.");
            }

            if ((bool)compensacion.IsIncremental && compensacion.Percentage == null)
            {
                throw new Core.Exceptions.ValidationException("Ingresar un porcentaje.");
            }

            if ((bool)compensacion.IsIncremental && (compensacion.Percentage < 1 || compensacion.Percentage > 100))
            {
                throw new Core.Exceptions.ValidationException("Ingresar un porcentaje valido entre 1% y 100%.");
            }

            HRCompensationBenefit compens = (from a in Context.HRCompensationBenefits
                                             where a.CompensationID == compensacion.CompensationID
                                             select a).FirstOrDefault();

            if (compens == null)
            {
                compens = new HRCompensationBenefit();
                Context.HRCompensationBenefits.InsertOnSubmit(compens);
            }

            compens.Name = compensacion.Name;
            compens.IsIncremental = compensacion.IsIncremental;
            compens.Percentage = compensacion.Percentage;

            Context.SubmitChanges();
        }

        public void Save(HRSalaryDetail detalleSalario)
        {
            Save1(detalleSalario);

            //Obtener el nivel minimo
            HRSalaryLevel minLvl = GetBasicSalaryLevel();
            //Verificar si el detalle pertenece al nivel mínimo entonces se realizan los cálculos de lugar
            if (minLvl.SalaryLevelID == detalleSalario.SalaryLevelsID)
            {
                //Se obtiene la compensación o beneficio al cual se le aplicaran los cálculos
                HRCompensationBenefit competence = GetCompensationBenefitByID((int)detalleSalario.CompensationID);
                //Se verifica si la compensación es para cálculos incrementar o no
                if ((bool)competence.IsIncremental)
                {
                    AplicarIncrementalMatriz(detalleSalario);
                }
                else
                {
                    AplicarEstandarMatriz(detalleSalario);
                }
            }
        }

        public void Save(HRSalaryLevel level)
        {
            if (string.IsNullOrWhiteSpace(level.Name))
            {
                throw new Core.Exceptions.ValidationException("Ingresar el nombre.");
            }
            if (string.IsNullOrWhiteSpace(level.Descripcion))
            {
                throw new Core.Exceptions.ValidationException("Ingresar la descripción.");
            }
            if (level.LevelSequence < 0)
            {
                throw new Core.Exceptions.ValidationException("La secuencia debe ser mayor.");
            }

            var lvlValid = GetSalaryLevelBySequence((int)level.LevelSequence);
            if (lvlValid != null && lvlValid.SalaryLevelID != level.SalaryLevelID)
            {
                throw new Core.Exceptions.ValidationException("ya hay otro nivel con esta secuencia.");
            }

            lvlValid = GetSalaryLevelByName(level.Name.Trim());
            if (lvlValid != null && lvlValid.SalaryLevelID != level.SalaryLevelID)
            {
                throw new Core.Exceptions.ValidationException("ya hay otro nivel con este nombre.");
            }

            HRSalaryLevel lvl = (from a in Context.HRSalaryLevels
                                 where a.SalaryLevelID == level.SalaryLevelID
                                 select a).FirstOrDefault();
            if (lvl == null)
            {
                lvl = new HRSalaryLevel();
                lvl.Create_dt = DateTime.Now;
                Context.HRSalaryLevels.InsertOnSubmit(lvl);
            }
            lvl.Name = level.Name;
            lvl.Descripcion = level.Descripcion;
            lvl.LevelSequence = level.LevelSequence;
            lvl.Modify_ft = DateTime.Now;
            lvl.UserID = level.UserID;

            Context.SubmitChanges();
        }

        public void Save(HROrganizationChart organizacion)
        {
            HROrganizationChart orga = (from a in Context.HROrganizationCharts
                                        where a.SuperiorPositionID == organizacion.SuperiorPositionID && a.SubordinatePositionID == organizacion.SubordinatePositionID
                                        select a).FirstOrDefault();
            if (orga == null) Context.InsertHROrganizationChart(organizacion.SuperiorPositionID, organizacion.SubordinatePositionID);
        }

        public void Save(HRDeparmentsPosition relacion)
        {
            HRDeparmentsPosition rela = (from a in Context.HRDeparmentsPositions
                                         where a.DeparmentID == relacion.DeparmentID && a.PositionID == relacion.PositionID
                                         select a).FirstOrDefault();
            if (rela == null)
            {
                Context.InsertDepartementsPositions(relacion.DeparmentID, relacion.PositionID);
            }
        }

        public void Save(HRPositionCompetence relacion)
        {
            int lvl = 0;

            if (!int.TryParse(relacion.Level.ToString(), out lvl)) throw new Core.Exceptions.ValidationException("El nivel debe ser un numero.");
            if (relacion.Level <= 0 || relacion.Level > 10) throw new Core.Exceptions.ValidationException("El nivel debe ser entre 1 y 10");
            HRPositionCompetence re = (from a in Context.HRPositionCompetences
                                       where a.CompetenceID == relacion.CompetenceID && a.PositionID == relacion.PositionID
                                       select a).FirstOrDefault();
            if (re == null)
            {
                Context.InsertHRPositionCompetences(relacion.CompetenceID, relacion.PositionID, relacion.Level);
            }
            else
            {
                Context.UpdateHRPositionCompetences(relacion.CompetenceID, relacion.PositionID, relacion.Level);
            }
        }

        public void Save(HRCompetencesCategory categoria)
        {
            //Verificar si el nombre no es vació
            if (string.IsNullOrWhiteSpace(categoria.Name)) throw new Core.Exceptions.ValidationException("El nombre de la categoría no puede estar vació.");

            //Verifica que si se va a crear una nueva categoría no se cree con un nombre ya existente
            HRCompetencesCategory categ = (from a in Context.HRCompetencesCategories
                                           where a.Name == categoria.Name.Trim().ToUpper()
                                           select a).FirstOrDefault();

            if (categ != null && categoria.CompetenceCategoryID != categ.CompetenceCategoryID)
            {
                throw new Core.Exceptions.ValidationException("Esta categoría ya existe.");
            }

            categ = (from a in Context.HRCompetencesCategories
                     where a.CompetenceCategoryID == categoria.CompetenceCategoryID
                     select a).FirstOrDefault();

            if (categ == null)
            {
                categ = new HRCompetencesCategory();
                Context.HRCompetencesCategories.InsertOnSubmit(categ);
            }

            categ.Name = categoria.Name.Trim().ToUpper();

            Context.SubmitChanges();
        }

        public void Save(HRCompetence competencia)
        {
            //Verificar informaciones faltante
            if (string.IsNullOrWhiteSpace(competencia.Name)) throw new Core.Exceptions.ValidationException("El nombre de la competencia no puede estar vació.");
            if (string.IsNullOrWhiteSpace(competencia.Description)) throw new Core.Exceptions.ValidationException("La descripción de la competencia no puede estar vacía.");

            //Verificar que si se va a crear una nueva competencia no se cree con un nombre ya existente
            HRCompetence comp = (from a in Context.HRCompetences
                                 where a.Name == competencia.Name.Trim().ToUpper()
                                 select a).FirstOrDefault();

            if (comp != null && competencia.CompetenceID != comp.CompetenceCategoryID)
            {
                throw new Core.Exceptions.ValidationException("Esta competencia ya existe.");
            }

            comp = (from a in Context.HRCompetences
                    where a.CompetenceID == competencia.CompetenceID
                    select a).FirstOrDefault();

            if (comp == null)
            {
                comp = new HRCompetence();
                Context.HRCompetences.InsertOnSubmit(comp);
            }
            comp.Name = competencia.Name.Trim().ToUpper();
            comp.Description = competencia.Description;
            comp.CompetenceCategoryID = competencia.CompetenceCategoryID;

            Context.SubmitChanges();
        }

        public void Save(HRPositionAnalisy analisys)
        {
            HRPositionAnalisy analis = (from a in Context.HRPositionAnalisies
                                        where a.PositionAnalysisID == analisys.PositionAnalysisID
                                        select a).FirstOrDefault();
            if (analis == null)
            {
                analis = new HRPositionAnalisy();
                analis.PositionID = analisys.PositionID;
                analis.UserID = analisys.UserID;
                analis.Modify_ft = DateTime.Now;
                analis.Create_dt = DateTime.Now;
                Context.HRPositionAnalisies.InsertOnSubmit(analis);
                Context.SubmitChanges();
            }
        }

        public void Save(HRPositionAnalisysDetail detalle)
        {
            //Verificar informaciones faltante
            if (string.IsNullOrWhiteSpace(detalle.Funtion)) throw new Core.Exceptions.ValidationException("El nombre de la función no puede ser vació.");
            if (string.IsNullOrWhiteSpace(detalle.WhatDo)) throw new Core.Exceptions.ValidationException("Debe especificar que hace la función.");
            if (string.IsNullOrWhiteSpace(detalle.HowDo)) throw new Core.Exceptions.ValidationException("Debe especificar como se hace la función.");
            if (string.IsNullOrWhiteSpace(detalle.WhyDo)) throw new Core.Exceptions.ValidationException("Debe especificar porque se hace la función.");
            if (string.IsNullOrWhiteSpace(detalle.TimeDo)) throw new Core.Exceptions.ValidationException("Debe especificar el tiempo que tomar hacer la función.");

            //Verificar que si se va a crear una nueva competencia no se cree con un nombre ya existente
            HRPositionAnalisysDetail dt = (from a in Context.HRPositionAnalisysDetails
                                           where a.Funtion == detalle.Funtion && a.PositionAnalisysID == detalle.PositionAnalisysID
                                           select a).FirstOrDefault();

            if (dt != null && detalle.DetailsID == 0)
            {
                throw new Core.Exceptions.ValidationException("Esta función ya existe ya existe.");
            }

            dt = (from a in Context.HRPositionAnalisysDetails
                  where a.DetailsID == detalle.DetailsID
                  select a).FirstOrDefault();

            if (dt == null)
            {
                dt = new HRPositionAnalisysDetail();
                dt.PositionAnalisysID = detalle.PositionAnalisysID;
                Context.HRPositionAnalisysDetails.InsertOnSubmit(dt);
            }

            dt.Funtion = detalle.Funtion;
            dt.WhatDo = detalle.WhatDo;
            dt.HowDo = detalle.HowDo;
            dt.WhyDo = detalle.WhyDo;
            dt.TimeDo = detalle.TimeDo;

            Context.SubmitChanges();
        }

        public void Save(object objeto)
        {
            HRPosition posi = (HRPosition)objeto;

            if (string.IsNullOrWhiteSpace(posi.PositionName))
            {
                throw new Core.Exceptions.ValidationException("Debe especificar el nombre.");
            }
            if (string.IsNullOrWhiteSpace(posi.Description))
            {
                throw new Core.Exceptions.ValidationException("Debe ingresar una descripción.");
            }
            var valid = (from a in Context.HRPositions
                         where a.PositionName == posi.PositionName && posi.PositionID == 0
                         select a).FirstOrDefault();
            if (valid != null)
            {
                throw new Core.Exceptions.ValidationException("Ya se encuentra registrada una posición con este nombre.");
            }

            var pos = (from a in Context.HRPositions
                       where a.PositionID == posi.PositionID
                       select a).FirstOrDefault();

            if (pos == null)
            {
                pos = new HRPosition();
                Context.HRPositions.InsertOnSubmit(pos);
            }

            pos.PositionName = posi.PositionName;
            pos.Description = posi.Description;
            pos.Status = posi.Status;

            Context.SubmitChanges();
        }

        #endregion Public Methods

        #region Private Methods

        private void AplicarEstandarMatriz(HRSalaryDetail detalle)
        {
            //Se obtienen los niveles por secuencia
            List<HRSalaryLevel> niveles = GetAllSalaryLevelOrderBySequence();

            foreach (HRSalaryLevel nivel in niveles)
            {
                HRSalaryDetail nuevoDetalle = new HRSalaryDetail();

                nuevoDetalle.PositionID = detalle.PositionID;
                nuevoDetalle.SalaryLevelsID = nivel.SalaryLevelID;
                nuevoDetalle.CompensationID = detalle.CompensationID;
                nuevoDetalle.Quantity = detalle.Quantity;
                nuevoDetalle.UserID = detalle.UserID;
                Save1(nuevoDetalle);
            }
        }

        private void AplicarIncrementalMatriz(HRSalaryDetail detalle)
        {
            //Se obtiene el porcentaje de incremento de la competencia
            double porcentajeIncremento = double.Parse(GetCompensationBenefitByID((int)detalle.CompensationID).Percentage.ToString()) / 100;
            //Se obtienen los niveles ordenados por secuencia
            List<HRSalaryLevel> niveles = GetAllSalaryLevelOrderBySequence();

            foreach (HRSalaryLevel nivel in niveles)
            {
                //Verifica si la compensación a incrementar es el sueldo semanal
                if (detalle.CompensationID == 1)
                {
                    if (detalle.SalaryLevelsID != nivel.SalaryLevelID)
                    {
                        HRSalaryDetail nuevoDetalle = new HRSalaryDetail();

                        nuevoDetalle.PositionID = detalle.PositionID;
                        nuevoDetalle.SalaryLevelsID = nivel.SalaryLevelID;
                        nuevoDetalle.CompensationID = detalle.CompensationID;
                        nuevoDetalle.Quantity = detalle.Quantity * (1 + (porcentajeIncremento));
                        nuevoDetalle.UserID = detalle.UserID;
                        Save1(nuevoDetalle);

                        //Obtener el listado de compensaciones de cálculos especiales dependientes de el sueldo semanal
                        List<HRCompensationBenefit> compensacionesCalcular = (from a in Context.HRCompensationBenefits
                                                                              where a.CompensationID > 1 && a.CompensationID < 5
                                                                              select a).ToList();

                        //Se guarda un detalle salarial de cada una de esas compensaciones especiales
                        foreach (HRCompensationBenefit compensacionSeleccionada in compensacionesCalcular)
                        {
                            //Calcular el salario mensual
                            double salarioMensual = ((double)nuevoDetalle.Quantity * 4);
                            //Obtener el porcentaje para el calculo de la compensación o beneficio definido en la configuración del sistema
                            RepoSystemConfig repoSystemConfig = new RepoSystemConfig();
                            double porc = double.Parse(repoSystemConfig.GetSysTemConfigByName(compensacionSeleccionada.Name).SysConfigValue) / 100;

                            //Calcular el pago semanal de esa compensación luego de aplicado el porcentaje
                            double pagoSemanal = (salarioMensual * porc) / 4;

                            HRSalaryDetail nuevoSubDetalle = new HRSalaryDetail();
                            nuevoSubDetalle.PositionID = nuevoDetalle.PositionID;
                            nuevoSubDetalle.SalaryLevelsID = nivel.SalaryLevelID;
                            nuevoSubDetalle.CompensationID = compensacionSeleccionada.CompensationID;
                            nuevoSubDetalle.Quantity = pagoSemanal;
                            nuevoSubDetalle.UserID = nuevoDetalle.UserID;
                            Save1(nuevoSubDetalle);
                        }
                        //Obtengo la cantidad de mi detalle ingresado para el próximo calculo
                        detalle = GetSalaryDetailBy((int)nuevoDetalle.SalaryLevelsID, (int)nuevoDetalle.CompensationID, (int)nuevoDetalle.PositionID);
                    }
                    else
                    {
                        //Obtener el listado de compensaciones de cálculos especiales dependientes de el sueldo semanal
                        List<HRCompensationBenefit> compensacionesCalcular = (from a in Context.HRCompensationBenefits
                                                                              where a.CompensationID > 1 && a.CompensationID < 5
                                                                              select a).ToList();

                        //Se guarda un detalle salarial de cada una de esas compensaciones especiales
                        foreach (HRCompensationBenefit compensacionSeleccionada in compensacionesCalcular)
                        {
                            //Calcular el salario mensual
                            double salarioMensual = ((double)detalle.Quantity * 4);
                            //Obtener el porcentaje para el calculo de la compensación o beneficio definido en la configuración del sistema
                            RepoSystemConfig repoSystemConfig = new RepoSystemConfig();
                            double porc = double.Parse(repoSystemConfig.GetSysTemConfigByName(compensacionSeleccionada.Name).SysConfigValue) / 100;

                            //Calcular el pago semanal de esa compensación luego de aplicado el porcentaje
                            double pagoSemanal = (salarioMensual * porc) / 4;

                            HRSalaryDetail nuevoSubDetalle = new HRSalaryDetail();
                            nuevoSubDetalle.PositionID = detalle.PositionID;
                            nuevoSubDetalle.SalaryLevelsID = nivel.SalaryLevelID;
                            nuevoSubDetalle.CompensationID = compensacionSeleccionada.CompensationID;
                            nuevoSubDetalle.Quantity = pagoSemanal;
                            nuevoSubDetalle.UserID = detalle.UserID;
                            Save1(nuevoSubDetalle);
                        }
                    }
                }
                else
                {
                    HRSalaryDetail nuevoDetalle = new HRSalaryDetail();

                    nuevoDetalle.PositionID = detalle.PositionID;
                    nuevoDetalle.SalaryLevelsID = nivel.SalaryLevelID;
                    nuevoDetalle.CompensationID = detalle.CompensationID;
                    nuevoDetalle.Quantity = detalle.Quantity * (1 + (porcentajeIncremento));
                    nuevoDetalle.UserID = detalle.UserID;
                    Save1(nuevoDetalle);

                    detalle.Quantity = GetSalaryDetailBy((int)nuevoDetalle.SalaryLevelsID, (int)nuevoDetalle.CompensationID, (int)nuevoDetalle.PositionID).Quantity;
                }
            }
        }

        public HRSalaryDetail GetSalaryDetailBy(int levelID, int compensationID, int positionID)
        {
            var result = (from a in Context.HRSalaryDetails
                          where a.CompensationID == compensationID && a.SalaryLevelsID == levelID && a.PositionID == positionID
                          select a).FirstOrDefault();
            return result;
        }

        private HRSalaryLevel GetSalaryLevelByName(string name)
        {
            var lvl = (from a in Context.HRSalaryLevels
                       where a.Name == name
                       select a).FirstOrDefault();
            return lvl;
        }

        private HRSalaryLevel GetSalaryLevelBySequence(int sequence)
        {
            var lvl = (from a in Context.HRSalaryLevels
                       where a.LevelSequence == sequence
                       select a).FirstOrDefault();
            return lvl;
        }

        private void Save1(HRSalaryDetail detalleSalario)
        {
            var result = (from a in Context.HRSalaryDetails
                          where a.SalaryLevelsID == detalleSalario.SalaryLevelsID
                          && a.CompensationID == detalleSalario.CompensationID
                          && a.PositionID == detalleSalario.PositionID
                          select a).FirstOrDefault();
            if (result == null)
            {
                result = new HRSalaryDetail();
                result.Create_dt = DateTime.Now;
                Context.HRSalaryDetails.InsertOnSubmit(result);
            }
            result.SalaryLevelsID = detalleSalario.SalaryLevelsID;
            result.PositionID = detalleSalario.PositionID;
            result.CompensationID = detalleSalario.CompensationID;
            result.Quantity = detalleSalario.Quantity;
            result.UserID = detalleSalario.UserID;
            result.Modify_ft = DateTime.Now;

            Context.SubmitChanges();
        }

      public List<HRPersonnelRequisition> GetPositionRequsition()
        {
            var query = (from p in Context.HRPersonnelRequisitions
                         where p.PositionID != null && p.StatusID ==27
                         select p
                         ).ToList();

            return query;

        }

        #endregion Private Methods

    }
}