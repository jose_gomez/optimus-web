﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Repositories
{
    internal class ResetPassHistory : Contracts.ResetPassHistory
    {
        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
   ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public SYResetPassHistory GetResetPassHistoryByID(string id)
        {
            var result = (from a in Context.SYResetPassHistories
                          where a.ID == id
                          select a).FirstOrDefault();
            return result;
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }

        public void Save(User user, string code)
        {
            SYResetPassHistory hist = new SYResetPassHistory();
            hist.UserID = user.userID;
            hist.ID = code;
            hist.CreateDate = DateTime.Now;
            Context.SYResetPassHistories.InsertOnSubmit(hist);
            Context.SubmitChanges();
        }
    }
}
