﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
   internal class NationalityRepository:INationality
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
      ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public List<Nationality> GetAllNationality()
        {
            var query = (from p in ContextSQL.HRNationalities
                         select new Nationality
                         {
                             NationalityID = p.NationalityID,
                             Name = p.Name
                         }).ToList();
            return query;
        }
    }
}