﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.BC.Services;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    internal class EmployeesExtraHoursListRepository:IEmployeesExtraHoursList
    {
        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
      ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        private ServiceContainer container;
        private UnitOfWork worker;

        public List<Employees> GetEmployeeNotExtraHours(int dept, DateTime fecha)
        {
            // return Employees = Context.Database.SqlQuery<Employees>("SPGetEmployeesNotHasExtraHours @departamento, @date", new SqlParameter("departamento", dept), new SqlParameter("date", fecha)).ToList();
            var b = ContextSQL.SPGetEmployeesNotHasExtraHours(dept, fecha).ToList();
            List<Employees> employees = new List<Employees>();
            foreach (SPGetEmployeesNotHasExtraHoursResult item in b)
            {
                employees.Add(new Employees
                {
                    EmployeeId = item.EmployeeId,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                   
                });
            }
                return employees;
       }
        public List<Employees> GetEmployeeHasExtraHours(int dept, DateTime fecha,int HourStar,int HourEnd)
        {
            var Emp = ContextSQL.SPGetEmployeesHasExtraHoursByDateTime(fecha, HourStar, HourEnd, dept).ToList();
            List<Employees> employees = new List<Employees>();
            foreach (SPGetEmployeesHasExtraHoursByDateTimeResult item in  Emp)
            {
                employees.Add(new Employees
                {
                    EmployeeId = item.EmployeeId,
                    FirstName = item.FirstName,
                    LastName = item.LastName,
                    comida = (bool)item.Food,
                });
            }
            return employees;
        }

        public void DeleteEmpleyeeExtraHours(int dept, DateTime fecha,int HourStar,int HourEnd,List<EmployeesExtraHoursListDetails> EmpListDetailsList)
        {

            var query = ContextSQL.HREmployeesExtraHoursLists.FirstOrDefault(x=>x.DepartmentID == dept && x.DateSubmit == fecha && x.HourStart == HourStar && x.HourEnd == HourEnd);
            if (query != null)
            {
                foreach (EmployeesExtraHoursListDetails item in EmpListDetailsList)
                {
                    HREmployeesExtraHoursListDetail empExtraHour = ContextSQL.HREmployeesExtraHoursListDetails.FirstOrDefault(x => x.EmployeesExtraHoursListID == query.EmployeesExtraHoursListID && x.EmployeeID == item.EmployeeID);
                    if (empExtraHour != null)
                    {
                        ContextSQL.HREmployeesExtraHoursListDetails.DeleteOnSubmit(empExtraHour);
                        ContextSQL.SubmitChanges();
                    }
                   
                }
            } 
        }

        public int SavesList(EmployeesExtraHoursList EmpList)
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            HREmployeesExtraHoursList EmployeeList = ContextSQL.HREmployeesExtraHoursLists.FirstOrDefault(x => x.DateSubmit == EmpList.DateSubmit && x.HourStart == EmpList.HourStart && x.HourEnd == EmpList.HourEnd && x.DepartmentID == EmpList.DepartmentID);
            if (EmployeeList == null)
            {
                EmployeeList = new HREmployeesExtraHoursList();
                EmployeeList.DepartmentID = EmpList.DepartmentID;
                EmployeeList.DateSubmit = EmpList.DateSubmit;
                EmployeeList.HourStart = EmpList.HourStart;
                EmployeeList.HourEnd = EmpList.HourEnd;
                EmployeeList.StatusID = EmpList.StatusID;
                EmployeeList.Create_dt = DateTime.Now;
                EmployeeList.UserID = EmpList.UserID;
                ContextSQL.HREmployeesExtraHoursLists.InsertOnSubmit(EmployeeList);
                ContextSQL.SubmitChanges();
                
                
            }
            return EmployeeList.EmployeesExtraHoursListID;

        }
        public void SavesListDetails(List<EmployeesExtraHoursListDetails> EmpListDetailsList)
        {
            foreach (EmployeesExtraHoursListDetails EmpListDetails in  EmpListDetailsList)
            {
                HREmployeesExtraHoursListDetail EmployeeListDetails = ContextSQL.HREmployeesExtraHoursListDetails.FirstOrDefault(x=>x.EmployeesExtraHoursListID == EmpListDetails.EmployeesExtraHoursListID && x.EmployeeID == EmpListDetails.EmployeeID);
                if (EmployeeListDetails == null)
                {
                    EmployeeListDetails =  new HREmployeesExtraHoursListDetail();
                    EmployeeListDetails.EmployeesExtraHoursListID = EmpListDetails.EmployeesExtraHoursListID;
                    EmployeeListDetails.EmployeeID = EmpListDetails.EmployeeID;
                    EmployeeListDetails.Food = EmpListDetails.Food;
                    ContextSQL.HREmployeesExtraHoursListDetails.InsertOnSubmit(EmployeeListDetails);
                    ContextSQL.SubmitChanges();
                }   
            }
        }
        public List<VWListExtraHour> GetExtraHourListAll()
        {

            var query = ContextSQL.VWListExtraHours.ToList().OrderByDescending(x=>x.Fecha).ToList();
            return query;
        }
        public List<VWListExtraHour> GetExtraHourListAll(string Date)
        {
            List<VWListExtraHour> ListExtra = new List<VWListExtraHour>();
            //var query = (from p in ContextSQL.VWListExtraHours
            //             where p.Fecha == Date
            //             select p
            //             ).ToList();

            var query = ContextSQL.SPGetListExtraHourByDate(Convert.ToDateTime(Date));
            foreach (SPGetListExtraHourByDateResult item in query)
            {
                ListExtra.Add(
                    new VWListExtraHour
                    {
                        Fecha = item.Fecha,
                        NoDepartamento = item.NoDepartamento,
                        NoEmpleado = item.NoEmpleado,
                        StatusName = item.StatusName

                    });
            }
            return ListExtra;
        }
        public int GetCountEmployeeExtraHourByDateDepartment(DateTime date,int dept)
        {
            var query = (from p in ContextSQL.HREmployeesExtraHoursLists
                         join t in ContextSQL.HREmployeesExtraHoursListDetails on p.EmployeesExtraHoursListID equals t.EmployeesExtraHoursListID
                         where p.DateSubmit == date && p.DepartmentID == dept
                         select t
                         ).ToList();

            return query.Count;
        }

        public bool GetStateExtraHourListByDate(DateTime Date)
        {
           var query = (from p in ContextSQL.HREmployeesExtraHoursLists
                        where p.DateSubmit == Date
                        select p
                        ).FirstOrDefault();
            int Status = 0;
            if (query != null)
            {
                Status =  (int)query.StatusID;
            }
            if (Status == 27  || Date.Date < DateTime.Now.Date)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool AprovedList(int EmployeeID)
        {
            var emp = ContextSQL.HREmployees.FirstOrDefault(x=>x.EmployeeId== EmployeeID);
            var query = (from p in ContextSQL.HRRequistionWorkFlows
                         where p.PositionID == emp.PositionID && p.TypeRequisitionID == 3
                         select p ).FirstOrDefault();

            if (query != null)
            {
                return true;
            }
            else
            {
                return false;
            }

        }
        public void ActualizarEstadoExtraHours(string date)
        {
            ContextSQL.SPCambiarEstadoExtraHourList(date);
        }
        
    }
}
