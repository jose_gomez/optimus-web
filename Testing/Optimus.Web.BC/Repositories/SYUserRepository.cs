﻿using DRL.Utilities.Encryption;
using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;

using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    public class SYUserRepository : Repository<Optimus.Web.DA.Optimus.SYUser>,ISYUsers
    {
        public SYUserRepository(OptimusEntities context) : base(context)
        {
           
        }
        public  User GetUserByUserName(string username)
        {
            return this.Find(x => x.UserName == username).Select(e => new User()
            {
                userID = e.UserID,
                userName = e.UserName,
                fullName = e.FullName,
                password = e.password,
                EmployeeID = Convert.ToInt32(e.EmployeeID),

            }).FirstOrDefault();
        }
    }
}

