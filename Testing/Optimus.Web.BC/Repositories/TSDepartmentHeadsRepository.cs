﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA.Optimus;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
    internal class TSDepartmentHeadsRepository : Repository<TSDepartmentHead>, ITSDepartmentHeads
    {
        private OptimusEntities context;

        public TSDepartmentHeadsRepository(OptimusEntities context) : base(context)
        {
            this.context = context;
        }

        public Employees GetDepartmentHead(string department)
        {
            HREmployeesRepository repo = new HREmployeesRepository(this.context);
            RepoDepartment departmentRepo = new RepoDepartment();
            Employees employee = null;
            var dept = departmentRepo.GetAllDepartments().Where(c => c.Name == department).FirstOrDefault();
            if (dept != null)
            {
                var tsHead = this.Find(x => x.DepartmentId == dept.DepartmentId  &&  x.Manager  == true).FirstOrDefault();
                if (tsHead != null)
                {
                    employee = repo.GetEmployeeById(tsHead.TicketManagerId);
                }
            }
            return employee;
        }

        public List<Employees> GetEmployesByDeparment(int IDdepartment )
        {        
            var query = (from p in Context.HREmployees                         
                         where p.DepartmentID == IDdepartment && p.Status == "A" 
                         select new Employees()
                         {
                             EmployeeId = p.EmployeeId,
                             FirstName = p.FirstName,
                             LastName = p.LastName,
                             Cedula = p.Cedula,
                             Picture = p.Picture,
                              

                         }).ToList();
            return query;
        }

        public List<Employees> GetEmployesByDeparmentNoAssing(int IDTickets,int dept)
        {
            var query = Context.Database.SqlQuery<Employees>("SPGetEmployesByDeparmentNoAssing  @Dept,@Tickets", new SqlParameter("@Dept", dept), new SqlParameter("Tickets", IDTickets)).ToList();
            return query;
        }

        public bool IsManagerByUser(string user)
        {

            var query = (from p in Context.TSDepartmentHeads
                         join t in Context.SYUsers on p.TicketManagerId equals t.EmployeeID
                         where t.UserName == user
                         select p
                         ).FirstOrDefault();

            if (query != null)
            {
                return true;
            }
            else
            {

                return false;
            }


        }

        public bool IsManagerByUserDepartment(string user,int dept)
        {
            var query = (from p in Context.TSDepartmentHeads
                         join t in Context.SYUsers on p.TicketManagerId equals t.EmployeeID
                         where t.UserName == user && p.DepartmentId == dept
                         select p
                       ).FirstOrDefault();
            if (query != null)
            {
                return true;
            }
            else
            {

                return false;
            }

        }

        public List<User> GetEmailsDeparmentHead(int dept)
        {
            var query = (from p in Context.TSDepartmentHeads
                         join t in Context.SYUsers on p.TicketManagerId equals t.EmployeeID
                         where p.DepartmentId == dept && t.Email != null
                         select new User()
                         {
                             userID = t.UserID,
                             Email = t.Email
                         }).Distinct().ToList();

            return query;
        }
    }
}