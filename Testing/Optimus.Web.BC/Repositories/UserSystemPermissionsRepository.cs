﻿
using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    class UserSystemPermissionsRepository : Repository<SYUserSystemPermissions>,IUserSystemPermissions
    {
        public UserSystemPermissionsRepository(OptimusEntities context) : base(context)
        {
        }

        public List<UserSystemPermissions> GetSystemPermissionByUserID(int UserID)
        {
            var query = (from a in Context.SYUserSystemPermissions
                         join b in Context.SYModules on a.ModuloID equals b.ModuleID
                         where a.UserID == UserID && a.CanRead==true && b.SystemID == 2
                         select  new UserSystemPermissions()
                         {
                             UserID = a.UserID,
                             ModuloID = a.ModuloID,
                             CanCreate = a.CanCreate,
                             CanRead = a.CanRead,
                             CanUpdate = a.CanUpdate,
                             CanDelete = a.CanDelete,
                             title = b.Title,
                             ConstantName = b.ConstantName                            
                         }
                         ).ToList();

            return query;
            
        }
    }
}
