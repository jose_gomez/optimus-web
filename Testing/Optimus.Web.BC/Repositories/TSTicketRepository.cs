﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Optimus.Web.BC.Repositories
{
    public class TSTicketRepository : Repository<TSTicket>, ITSTicket
    {
        public TSTicketRepository(OptimusEntities context) : base(context)
        {
        }

        private DataClassesDataContext ContextSQL = new DataClassesDataContext(System.Configuration.ConfigurationManager.
        ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        DataClassesDataContext linqContext = new DataClassesDataContext(System.Configuration.ConfigurationManager.
  ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);
        public List<Tickestes> GetTicketsBySubmitUser(int submittedUserId, int filtro = 1)
        {
            //Repository<VWTicket> repo = new Repository<VWTicket>(this.Context);
            //var tickets = repo.Find(x => x.SubmitUser == submittedUserId).OrderByDescending(y => y.SubmitDate).ToList();
            var tickets = Context.Database.SqlQuery<Tickestes>("getTicketBySubmitUser @UserSubmit, @Filtro", new SqlParameter("UserSubmit", submittedUserId), new SqlParameter("Filtro", filtro)).ToList();
            return tickets;
        }
        /// <summary>
        /// Somete un ticket enviado por modulos de la aplicación
        /// </summary>
        /// <param name="Title">Titulo del ticket</param>
        /// <param name="Description">Descripción</param>
        /// <param name="submitUser">Usuario enviando el ticket</param>
        /// <param name="DepartmentId">Id del departamento donde va el ticket</param>
        /// <returns>String con un GUID que identifica el ticket como tal</returns>
        public string SubmitTicket(string Title, string Description, int submitUser, int DepartmentId, int prioridad = 2, bool isRequition = false, bool Approved = false, int status = 4)
        {
            SYUserRepository userR = new SYUserRepository(Context);
            HREmployeesRepository emplR = new HREmployeesRepository(Context);

            var usuario = userR.SingleOrDefault(x => x.UserID == submitUser);
            Employees em = emplR.GetEmployeeById((int)usuario.EmployeeID);

            TSTicket ticket = new TSTicket();
            ticket.DepartmentId = DepartmentId;
            ticket.Title = Title;
            ticket.Description = Description;
            ticket.SubmitUser = submitUser;
            bool isNonExisting = false;
            while (!isNonExisting)
            {
                var exist = this.Find(x => x.Referral == ticket.Referral).FirstOrDefault();
                if (exist == null)
                {
                    isNonExisting = true;
                }
                ticket.Referral = Guid.NewGuid().ToString();
            }

            ticket.SubmitDate = DateTime.Now;
            ticket.TicketState = status;
            ticket.PorcentProgress = 0;
            ticket.validated = false;
            ticket.Requisition = isRequition;
            ticket.DeparmentIdSubmit = em.DepartmentID;
            ticket.TicketsPriorityID = prioridad;
            ticket.Approved = Approved;
            if (isRequition)
            {

                //ticket.ProblemTypeID = 17;
                ticket.ProblemTypeID = 10;
            }

            this.Context.TSTickets.Add(ticket);
            this.Context.SaveChanges();
            return ticket.Referral;
        }

        public List<Tickestes> GetTicketsPendingByDepartment(int IdDept, bool isMng, int MngID = 0)
        {

            if (isMng)
            {
                var query = (from p in Context.TSTickets
                             join o in Context.HRDepartments on p.DepartmentId equals o.DepartmentId
                             join c in Context.TSTicketStates on p.TicketState equals c.ID
                             join t in Context.TSDepartmentHeads on p.DepartmentId equals t.DepartmentId
                             join u in Context.SYUsers on p.SubmitUser equals u.UserID
                             join y in Context.TSTicketsPriority on p.TicketsPriorityID equals y.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()

                             where p.TicketState == 4 && t.TicketManagerId == MngID
                             orderby c.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 SubmitDate = p.SubmitDate,
                                 Title = p.Title,
                                 Department = o.Name,
                                 Status = c.TicketState,
                                 Priority = y.Priority,
                                 Requisition = (bool)p.Requisition,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name,
                                 SubmitUserName = u.UserName,
                                 Description = p.Description


                             }).ToList();

                return query;
            }
            else
            {
                var query = (from p in Context.TSTickets
                             join o in Context.HRDepartments on p.DepartmentId equals o.DepartmentId
                             join c in Context.TSTicketStates on p.TicketState equals c.ID
                             join u in Context.SYUsers on p.SubmitUser equals u.UserID
                             join y in Context.TSTicketsPriority on p.TicketsPriorityID equals y.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.TicketState == 4 && p.DepartmentId == IdDept
                             orderby c.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 SubmitDate = p.SubmitDate,
                                 Title = p.Title,
                                 Department = o.Name,
                                 Status = c.TicketState,
                                 Priority = y.Priority,
                                 Requisition = (bool)p.Requisition,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name,
                                 SubmitUserName = u.UserName,
                                 Description = p.Description
                             }).ToList();

                return query;
            }
        }

        public List<Tickestes> GetTicketsAssingByUser(string username)
        {
            var query = (from p in Context.TSTickets
                         join y in Context.TSTicketsResponsible on p.ID equals y.TicketsID
                         join c in Context.TSTicketStates on p.TicketState equals c.ID
                         join x in Context.SYUsers on y.Responsible equals x.EmployeeID
                         join h in Context.HREmployees on p.SubmitUser equals h.EmployeeId
                         join z in Context.HRDepartments on p.DeparmentIdSubmit equals z.DepartmentId
                         join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                         join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                         from wer in we.DefaultIfEmpty()

                         where x.UserName == username && (p.TicketState == 3 || p.TicketState == 5 || p.TicketState == 7)
                         orderby c.OrderNumber, p.SubmitDate descending

                         select new Tickestes()
                         {
                             TicketNumber = p.ID,
                             SubmitDate = p.SubmitDate,
                             Title = p.Title,
                             Department = z.Name,
                             Status = c.TicketState,
                             Priority = t.Priority,
                             TipoProblemaID = p.TicketsPriorityID,
                             TipoProblema = wer.Name

                         }).ToList();

            return query;
        }

        public List<Tickestes> GetTicketsClosedByDepartment(int IdDept, bool isMng, int MngID = 0)
        {

            if (isMng)
            {
                var query = (from p in Context.TSTickets
                             join o in Context.HRDepartments on p.DepartmentId equals o.DepartmentId
                             join c in Context.TSTicketStates on p.TicketState equals c.ID
                             join t in Context.TSDepartmentHeads on p.DepartmentId equals t.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.TicketState == 6 && t.TicketManagerId == MngID
                             orderby p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 SubmitDate = p.SubmitDate,
                                 Title = p.Title,
                                 Department = o.Name,
                                 Status = c.TicketState,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();

                return query;
            }
            else
            {
                var query = (from p in Context.TSTickets
                             join o in Context.HRDepartments on p.DepartmentId equals o.DepartmentId
                             join c in Context.TSTicketStates on p.TicketState equals c.ID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.TicketState == 6 && p.DepartmentId == IdDept
                             orderby p.SubmitDate descending

                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 SubmitDate = p.SubmitDate,
                                 Title = p.Title,
                                 Department = o.Name,
                                 Status = c.TicketState,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();

                return query;
            }
        }

        public List<Tickestes> GetTicketsDetailByIdTickets(int IdTickets)
        {
            var query = (from p in Context.TSTickets
                         join o in Context.HREmployees on p.Responsible equals o.EmployeeId into t
                         from rt in t.DefaultIfEmpty()
                         join c in Context.TSTicketStates on p.TicketState equals c.ID
                         join x in Context.SYUsers on rt.EmployeeId equals x.EmployeeID into xl
                         from xlt in xl.DefaultIfEmpty()
                         join userS in Context.SYUsers on p.SubmitUser equals userS.UserID
                         join b in Context.HRDepartments on p.DeparmentIdSubmit equals b.DepartmentId
                         join y in Context.HRDepartments on p.DepartmentId equals y.DepartmentId
                         join i in Context.TSTicketsPriority on p.TicketsPriorityID equals i.TicketsPriorityID

                         where p.ID == IdTickets

                         select new Tickestes()
                         {
                             TicketNumber = p.ID,
                             SubmitDate = p.SubmitDate,
                             Title = p.Title,
                             Department = y.Name,
                             Status = c.TicketState,
                             SubmitUserName = userS.UserName,
                             Responsible = rt.FirstName + "." + rt.LastName,
                             DepartamentAdjudico = b.Name,
                             Description = p.Description,
                             IdDepartment = (int)p.DepartmentId,
                             PorcentProgress = (int)p.PorcentProgress,
                             validated = p.validated,
                             StatusID = (int)p.TicketState,
                             Priority = i.Priority,
                             PriorityID = i.TicketsPriorityID,
                             FatherTickets = p.FatherTickets,
                             Requisition = (bool)p.Requisition,
                             Approved = (bool)p.Approved


                         }).ToList();

            return query;
        }

        public List<CommentTicket> GetCommentByTicketsID(int IDTickets)
        {
            var query = (from p in Context.CommentTickets
                         join h in Context.SYUsers on p.UserID equals h.UserID
                         where p.TicketsID == IDTickets

                         select new CommentTicket()
                         {
                             TicketsID = (int)p.TicketsID,
                             UserID = (int)p.UserID,
                             Comentarios = p.Comentarios,
                             Datetime = (DateTime)p.Create_dt,
                             UserName = h.UserName
                         }).ToList();

            return query;
        }

        public List<Tickestes> GetResponsibles(int IDTickets)
        {
            List<Tickestes> query = (from p in Context.TSTicketsResponsible
                                     join h in Context.HREmployees on p.Responsible equals h.EmployeeId
                                     let user = h.FirstName + "." + h.LastName
                                     where p.TicketsID == IDTickets
                                     select new Tickestes
                                     {
                                         Responsible = user,
                                         TicketNumber = (int)p.TicketsID,
                                         EmployeesIDResponsible = (int)p.Responsible,
                                     }).ToList();

            return query;
        }

        public List<Tickestes> GetAllTicketsByDepartment(int dept, int filtro, int IdUser, int Responsible, bool isMng)
        {
            if (isMng)
            {
                return GetAllTicketsManager(filtro, Responsible);
            }
            else
            {
                return GetAllTickets(filtro, dept, IdUser);
            }
        }

        private List<Tickestes> GetAllTickets(int filtro, int dept, int IdUser)
        {
            if (filtro == 0)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.DeparmentIdSubmit == dept || p.DepartmentId == dept
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name

                             }).ToList();

                return query;

            }
            else if (filtro == 1)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.DeparmentIdSubmit == dept
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();

                return query;
            }
            else if (filtro == 2)
            {

                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.DepartmentId == dept
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();
                return query;
            }
            else if (filtro == 3)
            {

                SYUserRepository userR = new SYUserRepository(Context);
                var emp = userR.SingleOrDefault(x => x.UserID == IdUser);
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join j in Context.TSTicketsResponsible on p.ID equals j.TicketsID into lj
                             from ljt in lj.DefaultIfEmpty()
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.SubmitUser == IdUser || ljt.Responsible == emp.EmployeeID
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();

                return query;
            }
            else if (filtro == 4)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where (p.DeparmentIdSubmit == dept || p.DepartmentId == dept) && (p.TicketState == 3 || p.TicketState == 4 || p.TicketState == 7)
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();

                return query;
            }
            else if (filtro == 5)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             where (p.DeparmentIdSubmit == dept || p.DepartmentId == dept) && p.TicketState == 5
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).ToList();

                return query;

            }
            else if (filtro == 6)
            {

                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSTicketsPriority on p.TicketsPriorityID equals t.TicketsPriorityID
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where (p.DeparmentIdSubmit == dept || p.DepartmentId == dept) && p.TicketState == 2
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 Priority = t.Priority,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name

                             }).ToList();

                return query;
            }
            return null;
        }
        private List<Tickestes> GetAllTicketsManager(int filtro, int Responsible)
        {
            if (filtro == 0)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join k in Context.TSDepartmentHeads on p.DepartmentId equals k.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where k.TicketManagerId == Responsible
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList().Union(
                    from p in Context.TSTickets
                    join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                    join i in Context.TSTicketStates on p.TicketState equals i.ID
                    join k in Context.TSDepartmentHeads on p.DeparmentIdSubmit equals k.DepartmentId
                    join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                    from wer in we.DefaultIfEmpty()
                    where k.TicketManagerId == Responsible
                    select new Tickestes()
                    {
                        TicketNumber = p.ID,
                        Title = p.Title,
                        Department = h.Name,
                        PorcentProgress = (int)p.PorcentProgress,
                        Status = i.TicketState,
                        SubmitDate = p.SubmitDate,
                        TipoProblemaID = p.ProblemTypeID,
                        TipoProblema = wer.Name
                    }).Distinct().ToList();

                return query;
            }
            else if (filtro == 1)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSDepartmentHeads on p.DeparmentIdSubmit equals t.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where t.TicketManagerId == Responsible
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList();

                return query;
            }
            else if (filtro == 2)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join t in Context.TSDepartmentHeads on p.DepartmentId equals t.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where t.TicketManagerId == Responsible
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList();
                return query;
            }
            else if (filtro == 3)
            {
                SYUserRepository userR = new SYUserRepository(Context);
                var emp = userR.SingleOrDefault(x => x.EmployeeID == Responsible);
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join j in Context.TSTicketsResponsible on p.ID equals j.TicketsID into lj
                             from ljt in lj.DefaultIfEmpty()
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where p.SubmitUser == emp.UserID || ljt.Responsible == emp.EmployeeID
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList();

                return query;
            }
            else if (filtro == 4)
            {

                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join k in Context.TSDepartmentHeads on p.DepartmentId equals k.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where k.TicketManagerId == Responsible && (p.TicketState == 3 || p.TicketState == 4 || p.TicketState == 7)
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList().Union(
                    from p in Context.TSTickets
                    join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                    join i in Context.TSTicketStates on p.TicketState equals i.ID
                    join k in Context.TSDepartmentHeads on p.DeparmentIdSubmit equals k.DepartmentId
                    join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                    from wer in we.DefaultIfEmpty()
                    where k.TicketManagerId == Responsible && (p.TicketState == 3 || p.TicketState == 4 || p.TicketState == 7)
                    orderby i.OrderNumber, p.SubmitDate descending
                    select new Tickestes()
                    {
                        TicketNumber = p.ID,
                        Title = p.Title,
                        Department = h.Name,
                        PorcentProgress = (int)p.PorcentProgress,
                        Status = i.TicketState,
                        SubmitDate = p.SubmitDate,
                        TipoProblemaID = p.ProblemTypeID,
                        TipoProblema = wer.Name
                    }).Distinct().ToList();

                return query;

            }
            else if (filtro == 5)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join k in Context.TSDepartmentHeads on p.DepartmentId equals k.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where k.TicketManagerId == Responsible && p.TicketState == 5
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList().Union(
                 from p in Context.TSTickets
                 join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                 join i in Context.TSTicketStates on p.TicketState equals i.ID
                 join k in Context.TSDepartmentHeads on p.DeparmentIdSubmit equals k.DepartmentId
                 join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                 from wer in we.DefaultIfEmpty()
                 where k.TicketManagerId == Responsible && p.TicketState == 5
                 orderby i.OrderNumber, p.SubmitDate descending
                 select new Tickestes()
                 {
                     TicketNumber = p.ID,
                     Title = p.Title,
                     Department = h.Name,
                     PorcentProgress = (int)p.PorcentProgress,
                     Status = i.TicketState,
                     SubmitDate = p.SubmitDate,
                     TipoProblemaID = p.ProblemTypeID,
                     TipoProblema = wer.Name
                 }).Distinct().ToList();

                return query;

            }
            else if (filtro == 6)
            {
                var query = (from p in Context.TSTickets
                             join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                             join i in Context.TSTicketStates on p.TicketState equals i.ID
                             join k in Context.TSDepartmentHeads on p.DepartmentId equals k.DepartmentId
                             join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                             from wer in we.DefaultIfEmpty()
                             where k.TicketManagerId == Responsible && p.TicketState == 2
                             orderby i.OrderNumber, p.SubmitDate descending
                             select new Tickestes()
                             {
                                 TicketNumber = p.ID,
                                 Title = p.Title,
                                 Department = h.Name,
                                 PorcentProgress = (int)p.PorcentProgress,
                                 Status = i.TicketState,
                                 SubmitDate = p.SubmitDate,
                                 TipoProblemaID = p.ProblemTypeID,
                                 TipoProblema = wer.Name
                             }).Distinct().ToList().Union(
                    from p in Context.TSTickets
                    join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                    join i in Context.TSTicketStates on p.TicketState equals i.ID
                    join k in Context.TSDepartmentHeads on p.DeparmentIdSubmit equals k.DepartmentId
                    join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                    from wer in we.DefaultIfEmpty()
                    where k.TicketManagerId == Responsible && p.TicketState == 2
                    select new Tickestes()
                    {
                        TicketNumber = p.ID,
                        Title = p.Title,
                        Department = h.Name,
                        PorcentProgress = (int)p.PorcentProgress,
                        Status = i.TicketState,
                        SubmitDate = p.SubmitDate,
                        TipoProblemaID = p.ProblemTypeID,
                        TipoProblema = wer.Name
                    }).Distinct().ToList();
            }

            return null;

        }
        public List<Tickestes> GetAllTicketsByUser(int IdUser, int Responsible)
        {
            var query = (from p in Context.TSTickets
                         join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                         join i in Context.TSTicketStates on p.TicketState equals i.ID
                         join w in Context.TSProblemType on p.ProblemTypeID equals w.ProblemTypeID into we
                         from wer in we.DefaultIfEmpty()
                         where p.Responsible == Responsible || p.SubmitUser == IdUser
                         orderby i.OrderNumber, p.SubmitDate descending
                         select new Tickestes()
                         {
                             TicketNumber = p.ID,
                             Title = p.Title,
                             Department = h.Name,
                             PorcentProgress = (int)p.PorcentProgress,
                             Status = i.TicketState,
                             SubmitDate = p.SubmitDate,
                             TipoProblemaID = p.ProblemTypeID,
                             TipoProblema = wer.Name
                         }).ToList();

            return query;
        }

        public List<User> GetEmailOfTicketsByID(int IDTickets)
        {
            var query = (from p in Context.TSTicketsResponsible
                         join h in Context.SYUsers on p.Responsible equals h.EmployeeID into us
                         from uss in us.DefaultIfEmpty()
                         join t in Context.HREmployees on p.Responsible equals t.EmployeeId
                         where p.TicketsID == IDTickets && uss.Email != null && t.Status == "A"

                         select new User()
                         {
                             userID = uss.UserID,
                             Email = uss.Email
                         }
                         ).Union(from p in Context.TSTickets
                                 join h in Context.SYUsers on p.SubmitUser equals h.UserID
                                 join t in Context.HREmployees on h.EmployeeID equals t.EmployeeId
                                 where p.ID == IDTickets && h.Email != null && t.Status == "A"
                                 select new User()
                                 {
                                     userID = h.UserID,
                                     Email = h.Email
                                 }).ToList();
            return query;
        }

        public int? getStateTicketsByGUID(Guid gui)
        {
            string guid = gui.ToString();

            var query = (from p in Context.TSTickets
                         where p.Referral == guid
                         select p.TicketState
                        ).FirstOrDefault();

            return query;
        }

        public List<Employees> getLogInEmployees(string user)
        {
            var emp = (from p in Context.HREmployees
                       join x in Context.SYUsers on p.EmployeeId equals x.EmployeeID
                       where x.UserName == user
                       select new Employees
                       {
                           EmployeeId = p.EmployeeId,
                           FirstName = p.FirstName,
                           LastName = p.LastName

                       }).ToList();
            return emp;
        }

        public List<CommentTicket> GetLogByTicketsID(int IDTickets)
        {
            var query = (from p in Context.TSTicketsLog
                         join h in Context.SYUsers on p.UserID equals h.UserID
                         where p.TicketsID == IDTickets

                         select new CommentTicket()
                         {
                             TicketsID = (int)p.TicketsID,
                             UserID = (int)p.UserID,
                             Comentarios = p.TicketsLog,
                             Datetime = (DateTime)p.create_dt,
                             UserName = h.UserName
                         }).ToList();

            return query;
        }

        public TicketStatistics GetPercentagesOfTicketsStatusesByDepartment(int departmentId)
        {

            if (departmentId == 41)
            {
                ServiceContainer container = new ServiceContainer();
                var dept = container.GetDepartmentService().GetAllDepartments().Where(d => d.Name.ToLower() == "Sistemas(Software)").FirstOrDefault();
                if (dept != null)
                {
                    departmentId = dept.DepartmentId;
                }

            }

            var ticketStates = Context.TSTicketStates.ToList();
            int asignedStateId = ticketStates.Where(ts => ts.TicketState == "Assigned").First().ID;
            int validatedStateId = ticketStates.Where(ts => ts.TicketState == "Validate").First().ID;
            int cancelledStateId = ticketStates.Where(ts => ts.TicketState == "Cancelled").First().ID;
            int doneStateId = ticketStates.Where(ts => ts.TicketState == "Done").First().ID;
            int submittedStateId = ticketStates.Where(ts => ts.TicketState == "Submitted").First().ID;
            int inProcessTicketState = ticketStates.Where(ts => ts.TicketState == "in process").First().ID;
            int approvedTicketState = ticketStates.Where(ts => ts.TicketState == "Approved").First().ID;

            double totalTicketsCount = Context.TSTickets.Where(d => d.DepartmentId == departmentId).Count();
            double asignedTicketsCount = Context.TSTickets.Where(d => d.TicketState == asignedStateId && d.DepartmentId == departmentId).Count();
            double validatedTicketsCount = Context.TSTickets.Where(d => d.TicketState == validatedStateId && d.DepartmentId == departmentId).Count();
            double cancelledTicketsCount = Context.TSTickets.Where(d => d.TicketState == cancelledStateId && d.DepartmentId == departmentId).Count();
            double submittedTicketsCount = Context.TSTickets.Where(d => d.TicketState == submittedStateId && d.DepartmentId == departmentId).Count();
            double doneTicketsCount = Context.TSTickets.Where(d => d.TicketState == doneStateId && d.DepartmentId == departmentId).Count();
            double inProcessTicketsCount = Context.TSTickets.Where(d => d.TicketState == inProcessTicketState && d.DepartmentId == departmentId).Count();
            double approvedTicketsCount = Context.TSTickets.Where(d => d.TicketState == approvedTicketState && d.DepartmentId == departmentId).Count();

            TicketStatistics statistics = new TicketStatistics();
            statistics.TotalTicketsCount = Math.Round((asignedTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Validated = Math.Round((validatedTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Pending = Math.Round((submittedTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Cancelled = Math.Round((cancelledTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Done = Math.Round((doneTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Approved = Math.Round((approvedTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Working = Math.Round((inProcessTicketsCount / totalTicketsCount) * 100, 2);
            statistics.Assigned = Math.Round((asignedTicketsCount / totalTicketsCount) * 100, 2);
            statistics.TotalTicketsCount = totalTicketsCount;
            return statistics;
        }

        public List<Tickestes> GetTicketsByKEyWord(string palabra)
        {

            var query = (from p in Context.TSTickets
                         join h in Context.HRDepartments on p.DepartmentId equals h.DepartmentId
                         join i in Context.TSTicketStates on p.TicketState equals i.ID
                         join j in Context.TSTicketsResponsible on p.ID equals j.TicketsID into lj
                         from ljt in lj.DefaultIfEmpty()
                         where p.Title.Contains(palabra)
                         orderby i.OrderNumber, p.SubmitDate descending
                         select new Tickestes()
                         {
                             TicketNumber = p.ID,
                             Title = p.Title,
                             Department = h.Name,
                             PorcentProgress = (int)p.PorcentProgress,
                             Status = i.TicketState,
                             SubmitDate = p.SubmitDate
                         }).Distinct().ToList();

            return query;
        }

        public List<DA.Optimus.TSTicketsPriority> GetTicketsPriority()
        {
            var query = (from p in Context.TSTicketsPriority
                         select p
                         ).ToList();
            return query;
        }



        public List<DataPoint> GetAllTimeTicketsLeaders(int employeeId)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            var employee = Context.HREmployees.Where(x => x.EmployeeId == employeeId).FirstOrDefault();
            if (employee != null)
            {
                var hierarchy = Context.SP_GetEmployeeHierarchy(0, employee.PositionID).ToList();
                var employeeLevel = hierarchy.Where(x => x.EmployeeId == employee.EmployeeId).FirstOrDefault();
                if (employeeLevel != null)
                {
                    var employeeHierarchy = hierarchy.Where(x => x.lvl >= employeeLevel.lvl).Select(x => x.EmployeeId).ToList();
                    var ticketsByCats = linqContext.VW_TicketStatistics
                        .Where(x => (x.DepartmentId == employee.DepartmentID ||
                        employeeHierarchy.Contains(x.Responsible
                        )) && x.TicketState.ToLower() == "Validate".ToLower()).ToList();

                    var results = (from r in ticketsByCats
                                   group r by new { r.FullName } into n
                                   select new
                                   {
                                       FullName = n.Key.FullName,
                                       Tickets = n.Sum(x => x.Tickets)
                                   }).OrderByDescending(x=>x.Tickets).ToList();
                    int xAxis = 0;
                    foreach (var r in results)
                    {
                        DataPoint point = new DataPoint(xAxis, (double)r.Tickets, r.FullName);
                        dataPoints.Add(point);
                        xAxis++;
                    }
                }

            }

            return dataPoints;
        }

        List<DataPoint> ITSTicket.GetAllTimeTicketsLeaders(int employeeId, string status)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();
            var employee = Context.HREmployees.Where(x => x.EmployeeId == employeeId).FirstOrDefault();
            if (employee != null)
            {
                var hierarchy = Context.SP_GetEmployeeHierarchy(0, employee.PositionID).ToList();
                var employeeLevel = hierarchy.Where(x => x.EmployeeId == employee.EmployeeId).FirstOrDefault();
                if (employeeLevel != null)
                {
                    var employeeHierarchy = hierarchy.Where(x => x.lvl <= employeeLevel.lvl).Select(x => x.EmployeeId).ToList();
                    var ticketsByCats = linqContext.VW_TicketStatistics
                        .Where(x => x.DepartmentId == employee.DepartmentID ||
                        employeeHierarchy.Contains(x.Responsible
                        ) && x.TicketState.ToLower() == status.ToLower()).ToList();

                    var results = (from r in ticketsByCats
                                   group r by new { r.FullName } into n
                                   select new
                                   {
                                       FullName  = n.Key.FullName,
                                       Tickets=n.Sum(x=>x.Tickets)
                                   }).ToList();
                    int xAxis = 0;
                    foreach (var r in results)
                    {
                        DataPoint point = new DataPoint(xAxis, (double)r.Tickets, r.FullName);
                        dataPoints.Add(point);
                        xAxis++;
                    }
                }

            }

            return dataPoints;
        }

        List<DataPoint> ITSTicket.GetAllTimeTicketsLeaders(int departmentId, string status, bool includeDeptHeadHierarchy)
        {
            List<DataPoint> dataPoints = new List<DataPoint>();

                    var ticketsByCats = linqContext.VW_TicketStatistics
                        .Where(x => x.DepartmentId == departmentId && x.TicketState.ToLower() == status.ToLower()).ToList();

                    var results = (from r in ticketsByCats
                                   group r by new { r.FullName, r.Responsible } into n
                                   select new
                                   {
                                       FullName = n.Key.FullName,
                                       Tickets = n.Sum(x => x.Tickets), 
                                       EmployeeId = n.Key.Responsible
                                   }).OrderByDescending(x=>x.Tickets).ToList();
                    int xAxis = 0;
                    foreach (var r in results)
                    {
                        DataPoint point = new DataPoint(xAxis, (double)r.Tickets, r.FullName);
                        point.EmployeeId = r.EmployeeId;
                        dataPoints.Add(point);
                        xAxis++;
                    }

            return dataPoints;
        }

        public string setApprovedTickets(string ticketID)
        {
            TSTicket tickets = new TSTicket();
            tickets = Context.TSTickets.FirstOrDefault(x => x.Referral == ticketID && x.Requisition == true);
            if (tickets != null)
            {
                tickets.Approved = true;
                Context.SaveChanges();
                return "Exito!";
            }
            return "Fails!";
        }
        public List<ProblemType> getProblemTypeByDeparment(string dept)
        {
            int deptID = Context.HRDepartments.FirstOrDefault(x => x.Name == dept).DepartmentId;
            var query = (from p in Context.TSProblemType
                         where p.DepartmentID == deptID
                         select new ProblemType
                         {
                             ProblemTypeID = p.ProblemTypeID,
                             DepartmentID = (int)p.DepartmentID,
                             Name = p.Name

                         }
                         ).ToList();
            return query;
        }

        public double GetAverageResponseTime(string dept)
        {
            double avg = 0;
            var depart = Context.HRDepartments.Where(x => x.Name.ToLower() == dept.ToLower()).FirstOrDefault();
            if(depart != null)
            {
                var avgs = Context.TSTickets.Where(x => x.DoneDate != null && x.DepartmentId == depart.DepartmentId).ToList();
                if(avgs.Count > 0)
                {
                    avg = avgs.Average(x => x.DoneDate.Value.Subtract(x.SubmitDate.Value).TotalDays);
                }
            }
           
            return avg;
        }

        private JQTreeNodeData GetJQTreeNodeData(JQTreeNodeData rootNode)
        {
            var childs = Context.TSTickets.Where(x => x.FatherTickets == rootNode.id).Select(x => new JQTreeNodeData()
            {
                id = x.ID,
                name = x.Title,
                rootId = x.FatherTickets,
                progress = x.PorcentProgress
            }).ToList();

            foreach (var child in childs)
            {
                rootNode.children.Add(child);
                GetJQTreeNodeData(child);
            }
            return rootNode;
        }

        public List<JQTreeNodeData> GetTicketsHierarchyForJQTree(int ticketId)
        {
            List<JQTreeNodeData> list = new List<JQTreeNodeData>();
            var rootNode = Context.SP_GetTicketsHierarchy(ticketId).Select(x => new JQTreeNodeData()
            {
                id = x.ID,
                name = x.Title,
                rootId = x.FatherTickets,
                progress = x.PorcentProgress
            }).FirstOrDefault();

            if (rootNode != null)
            {
                list.Add(GetJQTreeNodeData(rootNode));
                return list;
            }
            return null;
        }


       
    }
}