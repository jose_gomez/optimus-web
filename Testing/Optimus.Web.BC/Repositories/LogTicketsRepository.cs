﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA.Optimus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Repositories
{
    public class LogTicketsRepository : Repository<TSTicketsLog>, ILogTickets
    {
        public LogTicketsRepository(OptimusEntities context) : base(context)
        {
        }
    }
}
