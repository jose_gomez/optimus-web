﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Position : Contracts.API.Position
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Create(object obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<DTO.Position> GetAllPositions()
        {
            var result = (from a in Context.HRPositions
                          select new DTO.Position
                          {
                              PositionID = a.PositionID,
                              PositionName = a.PositionName,
                              Description = a.Description,
                              Status = a.Status,
                              CategoryID = a.PositionCategoryID,
                              LevelID = a.OrganizationLevelID,
                              DepartmentID = a.DepartmentID,
                              SuperiorPositionID = a.SuperiorPositionID
                          }).OrderBy(x => x.PositionName).ToList();
            return result;
        }

        public DTO.Position GetPositionByID(int positionID)
        {
            var result = (from a in Context.HRPositions
                          where a.PositionID == positionID
                          select new DTO.Position
                          {
                              PositionID = a.PositionID,
                              PositionName = a.PositionName,
                              Description = a.Description,
                              Status = a.Status,
                              CategoryID = a.PositionCategoryID,
                              LevelID = a.OrganizationLevelID,
                              DepartmentID = a.DepartmentID,
                              SuperiorPositionID = a.SuperiorPositionID
                          }).FirstOrDefault();
            return result;
        }

        public List<DTO.Position> GetPositionsByStatus(int status)
        {
            var positions = (from a in Context.HRPositions
                             where a.Status == status
                             select new DTO.Position
                             {
                                 PositionID = a.PositionID,
                                 PositionName = a.PositionName,
                                 Description = a.Description,
                                 Status = a.Status,
                                 CategoryID = a.PositionCategoryID,
                                 LevelID = a.OrganizationLevelID,
                                 DepartmentID = a.DepartmentID,
                                 SuperiorPositionID = a.SuperiorPositionID
                             }).OrderBy(x => x.PositionName).ToList();
            return positions;
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }

        public void Update(object obj)
        {
            throw new NotImplementedException();
        }

        #endregion Public Methods

    }
}