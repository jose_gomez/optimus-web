﻿using Optimus.Web.DA;
using System;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Person : Contracts.API.Person
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Create(object obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(object obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Person GetPersonByID(int id)
        {
            var person = (from a in Context.HRPeoples
                          where a.PersonID == id
                          select new DTO.Person
                          {
                              PersonID = a.PersonID,
                              FirstName = a.FirstName,
                              LastName = a.LastName,
                              PictureGUID = a.PictureGUID,
                              Cedula = a.Cedula,
                              BirthDate = a.BirthDate ?? DateTime.Now
                          }).FirstOrDefault();
            return person;
        }

        public void Save(object obj)
        {
            throw new NotImplementedException();
        }

        public void Update(object obj)
        {
            throw new NotImplementedException();
        }

        #endregion Public Methods

    }
}