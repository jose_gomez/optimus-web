﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Optimus.Web.BC.Repositories.API
{
    public class Employee : Contracts.API.Employee
    {

        #region Private Fields

        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        #endregion Private Fields

        #region Public Methods

        public void Create(object obj)
        {
            throw new NotImplementedException();
        }

        public void Delete(object obj)
        {
            throw new NotImplementedException();
        }

        public DTO.Employee GetEmployeeByID(int id)
        {
            Position positionRepo = new Position();
            Person PersonRepo = new API.Person();
            var employee = (from a in Context.HREmployees
                            where a.EmployeeId == id
                            select new DTO.Employee
                            {
                                EmployeeID = a.EmployeeId,
                                PositionID = a.PositionID ?? 0,
                                PersonID = a.PersonID ?? 0,
                                UserID = a.UserID ?? 0,
                                CreateDate = a.Create_dt,
                                ModifyDate = a.ModifyDate,
                                Position = positionRepo.GetPositionByID(a.PositionID ?? 0),
                                Person = PersonRepo.GetPersonByID(a.PersonID ?? 0),
                                Picture = GetPicture(a.PictureGUID)
                            }).FirstOrDefault();
            return employee;
        }
        private byte[] GetPicture(string guid)
        {

            var pic = Context.SYGUIDImages.Where(x => x.GUID == guid).FirstOrDefault();
            if (pic != null)
            {
                return pic.Picture.ToArray();
            }
            return null;
        }
        public List<DTO.Employee> GetEmployeesByPosition(int positionID)
        {
            Position positionRepo = new Position();
            Person PersonRepo = new API.Person();
            var employees = (from a in Context.HREmployees
                            where a.PositionID == positionID
                            select new DTO.Employee
                            {
                                EmployeeID = a.EmployeeId,
                                PositionID = a.PositionID ?? 0,
                                PersonID = a.PersonID ?? 0,
                                UserID = a.UserID ?? 0,
                                CreateDate = a.Create_dt,
                                ModifyDate = a.ModifyDate,
                                Position = positionRepo.GetPositionByID(a.PositionID ?? 0),
                                Person = PersonRepo.GetPersonByID(a.PersonID ?? 0)
                            }).OrderBy(x=> x.EmployeeID).ToList();
            return employees;
        }

        public void Save(object obj)
        {
            DTO.Employee employee = (DTO.Employee)obj;
            var employeeFinded = (from a in Context.HREmployees
                                  where a.EmployeeId == employee.EmployeeID
                                  select a).FirstOrDefault();
            if (employeeFinded == null)
            {
                Create(employee);
            }
            else
            {
                Update(employee);
            }
        }
        private void validate(DTO.Employee employee)
        {
            var employeeFinded = (from a in Context.HREmployees
                                  where a.PersonID == employee.PersonID && a.EmployeeId != employee.EmployeeID
                                  select a).FirstOrDefault();
            if (employeeFinded != null)
            {
                throw new Core.Exceptions.ValidationException(Core.OptimusMessages.PERSONA_YA_ES_EMPLEADO);
            }
        }
        public void Update(object obj)
        {
            DTO.Employee employee = (DTO.Employee)obj;
            var employeeFinded = (from a in Context.HREmployees
                                  where a.EmployeeId == employee.EmployeeID
                                  select a).FirstOrDefault();
            if (employeeFinded != null)
            {
                employeeFinded.CompanyID = employee.CompanyID;
                //employeeFinded.Status = employeeFinded.Status
                employeeFinded.ProductionCompensation = employee.ProductionCompensation;
                employeeFinded.BaseSalary = employee.BaseSalary;
                employeeFinded.ModifyDate = DateTime.Now;
                employeeFinded.UserID = employee.UserID;
            }
        }

        #endregion Public Methods

    }
}