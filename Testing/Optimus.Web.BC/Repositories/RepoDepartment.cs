﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Contracts;

namespace Optimus.Web.BC.Repositories 
{
    internal class RepoDepartment : IDepartment
    {
        private DataClassesDataContext Context = new DataClassesDataContext(System.Configuration.ConfigurationManager.
    ConnectionStrings["Optimus.Web.DA.Properties.Settings.JTS_Optimus_DR_PruebaConnectionString"].ConnectionString);

        public void Delete(object objeto)
        {
            throw new NotImplementedException();
        }

        public List<HRDepartment> GetAllDepartments()
        {
            
     
            var lista = (from a in Context.HRDepartments
                         where a.DepartmentId != 41               
                         select a).OrderBy(x=> x.Name).ToList();

            return lista;
        }

        public HRDepartment GetBy(int id)
        {
            var depart = (from a in Context.HRDepartments
                          where a.DepartmentId == id
                          select a).FirstOrDefault() ;
            return depart;
        }

        public void Save(object objeto)
        {
            throw new NotImplementedException();
        }
    }
}
