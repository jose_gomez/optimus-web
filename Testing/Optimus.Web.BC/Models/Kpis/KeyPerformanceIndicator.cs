﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models.Kpis
{
   public class KeyPerformanceIndicator
    {
        public string Department { get; set; }
        public string Category { get; set; }
        public double SetGoal { get; set; }
        public int Week { get; set; }
        public DateTime  Date { get; set; }
        public double AchievedValue { get; set; }
    }
}
