﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class EmployeesExtraHoursListDetails
    {
        public int EmployeesExtraHoursListDetailsID { get; set; }
        public int EmployeesExtraHoursListID { get; set; }
        public int EmployeeID { get; set; }
        public Boolean Food { get; set; }
    }
}
