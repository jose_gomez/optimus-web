﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class Competencia
    {
        public int competenciaID { get; set; }
        public string Nombre { get; set; }
        public string Descripcion { get; set; }
        public double Nivel { get; set; }
    }
}
