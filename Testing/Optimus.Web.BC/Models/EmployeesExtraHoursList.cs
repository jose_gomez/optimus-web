﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public class EmployeesExtraHoursList
    {
        public int EmployeesExtraHoursListID { get; set; }
        public int DepartmentID { get; set; }
        public DateTime DateSubmit  { get; set; }
        public int HourStart { get; set; }
        public int HourEnd { get; set; }
        public int StatusID { get; set; }
        public DateTime Create_dt { get; set; }
        public int UserID { get; set; }
    }
}
