﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    [DataContract]
    public class DataPoint
    {

        public DataPoint()
        {

        }
        public DataPoint(double y, string label)
        {
            this.y = y;
            this.label = label;
        }

        public DataPoint(double x, double y)
        {
            this.x = x;
            this.y = y;
        }


        public DataPoint(double x, double y, string label)
        {
            this.x = x;
            this.y = y;
            this.label = label;
        }

        public DataPoint(double x, double y, double z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public DataPoint(double x, double y, double z, string label)
        {
            this.x = x;
            this.y = y;
            this.z = z;
            this.label = label;
        }


        //Explicitly setting the name to be used while serializing to JSON. 
        [DataMember(Name = "label")]
        public string label = null;

        //Explicitly setting the name to be used while serializing to JSON.
        [DataMember(Name = "y")]
        public Nullable<double> y = null;

        //Explicitly setting the name to be used while serializing to JSON.
        [DataMember(Name = "x")]
        public Nullable<double> x = null;

        //Explicitly setting the name to be used while serializing to JSON.
        [DataMember(Name = "z")]
        public Nullable<double> z = null;

        [DataMember(Name = "employeeId")]
        public Nullable<int> EmployeeId = null;
    }
}
