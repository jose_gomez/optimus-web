﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public  class User
    {
        public int userID { get; set; }
        public string userName { get; set;}
        public string password { get; set;}
        public Int32 roleID { get; set; }
        public int EmployeeID { get; set; }
        public bool isEnable { get; set; }
        public string fullName { get; set; }
        public int JTSEmployeeID { get; set;}
        public string Email {get;set;}
    }
}

