﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class Employees
    {
        public int EmployeeId { get; set; }
        public string FullName
        {
            get
            {
                return this.FirstName + " " + this.LastName;
            }
        }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Cedula { get; set; }
        public byte[] Picture { get; set; }
        public string PictureBase64 { get; set;}
        public int DepartmentID { get; set; }
        public string Departamento { get; set; }
        public DateTime DOB { get; set; }
        public bool comida { get; set; }

    }
}
