﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
    public class OrgChartModel
    {
        public int PositionID { get; set; }
        public string PositionName { get; set; }
        public int? SuperiorPosition { get; set; }
    }
}
