﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
  public class Department
    {
        public int DepartmenId { get; set; }
        public string Name { get; set; }
        public int CompanyID { get; set; }
        public string Account { get; set; }
        public bool Status { get; set; }
        public int DivisionID { get; set; }
    }
}
