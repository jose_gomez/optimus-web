﻿using Optimus.Web.BC.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Models
{
   public class PermissionsEmployees
    {
        private ServiceContainer container;
        private UnitOfWork worker;

        public int PermissionsEmployeesID { get; set; }
        public int EmployeeID { get; set; }
        public string FullName
        {
            get
            {
                return GetFullName();
            }
        }
        public DateTime DateSubmit { get; set; }
        public DateTime DateEfective { get; set; }
        public int HourStart { get; set; }
        public int HourEnd { get; set; }
        public int PermissionReasonID { get; set; }
        public string PermissionReason { get; set; }
        public string Comment { get; set; }
        public int StatusID { get; set; }
        public string Status { get; set; }

        private string GetFullName()
        {
            container = new ServiceContainer();
            worker = container.GetUnitOfWork();
            var emp = worker.Employees.GetEmployeeById(this.EmployeeID);
            return emp.FullName;
        }

    }
}
