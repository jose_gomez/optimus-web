﻿using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface IAuditControls
    {
        List<SHAuditControl> SaveAuditControlByID(SHAuditControl controlAudit);
        List<SHAuditControl> GetAllAuditControl();
        List<SHAuditControl> GetAuditControlByID(int controlID);
        List<SHAuditControl> GetAuditControlByName(string controlName);
        //
        List<SYEmailEmployee> SaveEmailsByAuditControlID(SYEmailEmployee emailEmployee, string alertType, int controlID, int empleyeeID);
        List<SYEmailEmployee> GetEmailsByAuditControlID(string controlName);
        void SendEmailByAlertTypeID(int TypeID, string subject, string body);
        //
    }
}
