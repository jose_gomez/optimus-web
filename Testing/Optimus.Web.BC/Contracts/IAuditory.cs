﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface IAuditory
    {
        List<Audit> GetAllAudit();
        //
        List<SHAuditMaster> SaveByID(SHAuditMaster auditory);
        List<SHAuditMaster> GetAuditBy_DepartmentID_Date_IndicateID(SHAuditMaster auditory, SHAuditDetail detail);
        List<SHAuditMaster> GetAuditBy_DepartmentID_Date_ControlID(int departmentID, DateTime date, int controlID);
        //
        List<SHAuditDetail> SaveDetailByID(SHAuditDetail auditory);
        //
        List<SHAuditImagen> SaveImageByID(SHAuditImagen img);
        List<SHAuditImagen> GetImageByAuditID(int auditID);
        List<SHAuditImagen> GetImageByImgID(int imgID);
        //



    }
}
