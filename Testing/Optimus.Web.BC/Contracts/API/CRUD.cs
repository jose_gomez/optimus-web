﻿namespace Optimus.Web.BC.Contracts.API
{
    public interface CRUD
    {
        #region Public Methods

        void Create(object obj);

        void Delete(object obj);

        void Save(object obj);

        void Update(object obj);

        #endregion Public Methods
    }
}