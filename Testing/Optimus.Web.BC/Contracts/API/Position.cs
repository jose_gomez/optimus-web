﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Position : CRUD
    {

        #region Public Methods

        List<DTO.Position> GetAllPositions();

        DTO.Position GetPositionByID(int positionID);

        List<DTO.Position> GetPositionsByStatus(int status);

        #endregion Public Methods

    }
}