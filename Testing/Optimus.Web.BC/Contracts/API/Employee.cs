﻿using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts.API
{
    public interface Employee : CRUD
    {
        #region Public Methods

        DTO.Employee GetEmployeeByID(int id);

        List<DTO.Employee> GetEmployeesByPosition(int positionID);

        #endregion Public Methods
    }
}