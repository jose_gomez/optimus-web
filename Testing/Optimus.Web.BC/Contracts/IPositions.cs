﻿using Optimus.Web.DA;
using System.Collections.Generic;

namespace Optimus.Web.BC.Contracts
{
    public interface IPositions : ICRUD
    {

        #region Public Methods

        void Delete(HRDeparmentsPosition relacion);

        void Delete(HRPositionCompetence relacion);

        void Delete(HRPositionAnalisysDetail detalle);

        void Delete(HROrganizationChart organizacion);

        List<HRCompensationBenefit> GetAllCompensationBenefit();

        List<HRCompensationBenefit> GetAllCompensationBenefitForEdit();

        List<HRCompetencesCategory> GetAllCompetencesCategory();

        List<HRPosition> GetAllPositions();

        List<HRSalaryLevel> GetAllSalaryLevelOrderBySequence();

        List<HRSalaryLevel> GetAllSalaryLevels();

        HRSalaryLevel GetBasicSalaryLevel();

        HRCompensationBenefit GetCompensationBenefitByID(int id);

        Models.Competencia GetCompetenceBy(int competenceID, int positionID);

        List<HRCompetence> GetCompetenceByCategoryID(int categortID);

        HRCompetence GetCompetenceByID(int competenceID);

        HRCompetencesCategory GetCompetencesCategoryBYID(int id);

        List<HRCompetencesCategory> GetCompetencesCategoryBYPosicionID(int id);

        List<Models.Competencia> GetCompetenciaBYPositionID(int id);

        List<Models.Competencia> GetCompetenciasByCategory(int categoryID);

        List<Models.Competencia> GetCompetenciasByCategoryByID(int categoryID, int positionID);

        HRDeparmentsPosition GetDepartmentPosition(int positionID);

        List<VWMatrizSalarial> GetMatrizSalarialByPositionID(int positionID);

        HROrganizationChart GetOrganizationChart(int subordinatePositionID);

        List<HRPositionAnalisysDetail> GetPositionAnalisyDeatailByAnalisyID(int id);

        HRPosition GetPositionByName(string name);

        HRPositionAnalisysDetail GetPositionDetailBy(string funtion, int positionID);

        HRPositionAnalisy GetPositionsAnalisyBYPositionID(int id);

        List<HRPosition> GetPositionsBy(string name);

        List<HRPosition> GetPositionsByDeparmentID(int id);

        HRPosition GetPositionsByID(int id);

        HRSalaryDetail GetSalaryDetailBy(int levelID, int compensationID, int positionID);

        HRSalaryLevel GetSalaryLevelByID(int id);

        List<HRPosition> GetSubordinadosByPositionID(int id);

        void Save(HRPositionCompetence relacion);

        void Save(HRDeparmentsPosition relacion);

        void Save(HRCompetencesCategory categoria);

        void Save(HRCompetence competencia);

        void Save(HRPositionAnalisysDetail detalle);

        void Save(HRPositionAnalisy analisys);

        void Save(HROrganizationChart organizacion);

        void Save(HRSalaryLevel level);

        void Save(HRCompensationBenefit compensacion);

        void Save(HRSalaryDetail detalleSalario);

        List<HRPersonnelRequisition> GetPositionRequsition();

        #endregion Public Methods
    }
}