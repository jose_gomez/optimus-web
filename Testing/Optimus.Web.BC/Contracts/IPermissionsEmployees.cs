﻿using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
   public interface IPermissionsEmployees
    {
        void savePermission(PermissionsEmployees EmpPerm);
        List<HRPermissionReason> GetPermissionReason();
        List<PermissionsEmployees> GetPermissionPending();
        bool AprovedHHRRUser(int EmployeeID);
        void AprovedOrPreAprovedPermission(int PermissionID, bool Aproved);
        List<PermissionsEmployees> GetListPermissionPending(string user);
        List<PermissionsEmployees> GetListPermissionPendingEmployeesBeEmployeeID(int employeeID);
        List<PermissionsEmployees> GetListPermissionAproved(string user);
        List<PermissionsEmployees> GetListPermissionAprovedEmployeesBeEmployeeID(int employeeID);
    }
}
