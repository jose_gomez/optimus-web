﻿using Optimus.Web.BC.Models.SHAudit;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface IAuditoryReport
    {
        List<SHAudit> GetAuditEPPByDetDate(int deparment, DateTime date);
    }
}
