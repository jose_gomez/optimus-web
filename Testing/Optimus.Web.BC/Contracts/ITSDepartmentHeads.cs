﻿using System;
using System.Collections.Generic;
using System.Linq;
using Optimus.Web.DA.Optimus;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Contracts
{
   public interface ITSDepartmentHeads : IRepository<TSDepartmentHead>
    {
        Employees GetDepartmentHead(string department);
        List<Employees> GetEmployesByDeparment(int IDdepartment);
        List<Employees> GetEmployesByDeparmentNoAssing(int IDTickets, int dept);
        bool IsManagerByUser(string user);
        bool IsManagerByUserDepartment(string user, int dept);
        List<User> GetEmailsDeparmentHead(int dept);
      
    }
}
