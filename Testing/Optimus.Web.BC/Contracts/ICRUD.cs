﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Contracts
{
    public interface ICRUD
    {
        void Save(object objeto);
        void Delete(object objeto);
    }
}
