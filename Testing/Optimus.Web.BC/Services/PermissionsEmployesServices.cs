﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class PermissionsEmployesServices : IPermissionsEmployees
    {
        private IPermissionsEmployees _PermissionsEmployees;

        public PermissionsEmployesServices(IPermissionsEmployees repo)
        {
            this._PermissionsEmployees = repo;
        }

        public bool AprovedHHRRUser(int EmployeeID)
        {
            
            return this._PermissionsEmployees.AprovedHHRRUser(EmployeeID);
        }

        public void AprovedOrPreAprovedPermission(int PermissionID, bool Aproved)
        {
            this._PermissionsEmployees.AprovedOrPreAprovedPermission(PermissionID, Aproved);
        }

        public List<PermissionsEmployees> GetListPermissionAproved(string user)
        {
            return this._PermissionsEmployees.GetListPermissionAproved(user);
        }

        public List<PermissionsEmployees> GetListPermissionPendingEmployeesBeEmployeeID(int employeeID)
        {
            return this._PermissionsEmployees.GetListPermissionPendingEmployeesBeEmployeeID(employeeID);
        }

        public List<PermissionsEmployees> GetListPermissionPending(string user)
        {
            return this._PermissionsEmployees.GetListPermissionPending(user);
        }

        public List<PermissionsEmployees> GetPermissionPending()
        {
            return this._PermissionsEmployees.GetPermissionPending();
        }

        public List<HRPermissionReason> GetPermissionReason()
        {
            return this._PermissionsEmployees.GetPermissionReason();
        }

        public void savePermission(PermissionsEmployees EmpPerm)
        {
            this._PermissionsEmployees.savePermission(EmpPerm);
        }

        public List<PermissionsEmployees> GetListPermissionAprovedEmployeesBeEmployeeID(int employeeID)
        {
            return this._PermissionsEmployees.GetListPermissionAprovedEmployeesBeEmployeeID(employeeID);
        }
    }
}
