﻿using Optimus.Web.BC.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Models;

namespace Optimus.Web.BC.Services
{
    public class NationalityServices 
    {
        private INationality _NationalityRepo;
        public NationalityServices(INationality repo)
        {
            this._NationalityRepo = repo;
        }     
            
            public List<Nationality> GetAllNationality()
        {
            return this._NationalityRepo.GetAllNationality();
        }
    }
}
