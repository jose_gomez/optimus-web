﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Services
{
    public class AuditoryService
    {
        IAuditory _repository;
        public AuditoryService(IAuditory rep)
        {
            this._repository = rep;
        }
        public List<SHAuditMaster> SaveByID(SHAuditMaster auditory)
        {
            return this._repository.SaveByID(auditory);
        }
        public List<SHAuditMaster> GetAuditBy_DepartmentID_Date_IndicateID(SHAuditMaster auditory, SHAuditDetail detail)
        {
            return this._repository.GetAuditBy_DepartmentID_Date_IndicateID(auditory, detail);
        }
        public List<SHAuditDetail> SaveDetailByID(SHAuditDetail auditory)
        {
            return this._repository.SaveDetailByID(auditory);
        }
        public List<SHAuditImagen> SaveImageByID(SHAuditImagen img)
        {
            return this._repository.SaveImageByID(img);
        }
        public List<SHAuditImagen> GetImageByAuditID(int auditID)
        {
            return this._repository.GetImageByAuditID(auditID);
        }
        public List<SHAuditImagen> GetImageByImgID(int imgID)
        {
            return this._repository.GetImageByImgID(imgID);
        }
        public List<SHAuditMaster> GetAuditBy_DepartmentID_Date_ControlID(int departmentID, DateTime date, int controlID)
        {
            return this._repository.GetAuditBy_DepartmentID_Date_ControlID(departmentID, date, controlID);
        }
        public List<Audit> GetAllAudit()
        {
            return this._repository.GetAllAudit();
        }
    }
}
