﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.BC.Models;
using Optimus.Web.DA;
using System.Collections.Generic;
using System;

namespace Optimus.Web.BC.Services
{
    public class PositionsService : IPositions
    {

        #region Private Fields

        private IPositions _PositionsRepo;

        #endregion Private Fields

        #region Public Constructors

        public PositionsService(IPositions repo)
        {
            this._PositionsRepo = repo;
        }

        #endregion Public Constructors

        #region Public Methods

        public void Delete(HROrganizationChart organizacion)
        {
            this._PositionsRepo.Delete(organizacion);
        }

        public void Delete(HRDeparmentsPosition relacion)
        {
            this._PositionsRepo.Delete(relacion);
        }

        public void Delete(HRPositionCompetence relacion)
        {
            this._PositionsRepo.Delete(relacion);
        }

        public void Delete(HRPositionAnalisysDetail detalle)
        {
            this._PositionsRepo.Delete(detalle);
        }

        public void Delete(object objeto)
        {
            this.Delete(objeto);
        }

        public List<HRCompensationBenefit> GetAllCompensationBenefit()
        {
            return this._PositionsRepo.GetAllCompensationBenefit();
        }

        public List<HRCompensationBenefit> GetAllCompensationBenefitForEdit()
        {
            return this._PositionsRepo.GetAllCompensationBenefitForEdit();
        }

        public List<HRCompetencesCategory> GetAllCompetencesCategory()
        {
            return this._PositionsRepo.GetAllCompetencesCategory();
        }

        public List<HRPosition> GetAllPositions()
        {
            return this._PositionsRepo.GetAllPositions();
        }

        public List<HRSalaryLevel> GetAllSalaryLevelOrderBySequence()
        {
            return this._PositionsRepo.GetAllSalaryLevelOrderBySequence();
        }

        public List<HRSalaryLevel> GetAllSalaryLevels()
        {
            return this._PositionsRepo.GetAllSalaryLevels();
        }

        public HRSalaryLevel GetBasicSalaryLevel()
        {
            return this._PositionsRepo.GetBasicSalaryLevel();
        }

        public HRCompensationBenefit GetCompensationBenefitByID(int id)
        {
            return this._PositionsRepo.GetCompensationBenefitByID(id);
        }

        public Competencia GetCompetenceBy(int competenceID, int positionID)
        {
            return this._PositionsRepo.GetCompetenceBy(competenceID, positionID);
        }

        public List<HRCompetence> GetCompetenceByCategoryID(int categortID)
        {
            return this._PositionsRepo.GetCompetenceByCategoryID(categortID);
        }

        public HRCompetence GetCompetenceByID(int competenceID)
        {
            return this._PositionsRepo.GetCompetenceByID(competenceID);
        }

        public HRCompetencesCategory GetCompetencesCategoryBYID(int id)
        {
            return this._PositionsRepo.GetCompetencesCategoryBYID(id);
        }

        public List<HRCompetencesCategory> GetCompetencesCategoryBYPosicionID(int id)
        {
            return this._PositionsRepo.GetCompetencesCategoryBYPosicionID(id);
        }

        public List<Competencia> GetCompetenciaBYPositionID(int id)
        {
            return this._PositionsRepo.GetCompetenciaBYPositionID(id);
        }

        public List<Competencia> GetCompetenciasByCategory(int categoryID)
        {
            return this._PositionsRepo.GetCompetenciasByCategory(categoryID);
        }

        public List<Competencia> GetCompetenciasByCategoryByID(int categoryID, int positionID)
        {
            return this._PositionsRepo.GetCompetenciasByCategoryByID(categoryID, positionID);
        }

        public HRDeparmentsPosition GetDepartmentPosition(int positionID)
        {
            return this._PositionsRepo.GetDepartmentPosition(positionID);
        }

        public List<VWMatrizSalarial> GetMatrizSalarialByPositionID(int positionID)
        {
            return this._PositionsRepo.GetMatrizSalarialByPositionID(positionID);
        }

        public HROrganizationChart GetOrganizationChart(int subordinatePositionID)
        {
            return this._PositionsRepo.GetOrganizationChart(subordinatePositionID);
        }

        public List<HRPositionAnalisysDetail> GetPositionAnalisyDeatailByAnalisyID(int id)
        {
            return this._PositionsRepo.GetPositionAnalisyDeatailByAnalisyID(id);
        }

        public HRPosition GetPositionByName(string name)
        {
            return this._PositionsRepo.GetPositionByName(name);
        }

        public HRPositionAnalisysDetail GetPositionDetailBy(string funtion, int positionID)
        {
            return this._PositionsRepo.GetPositionDetailBy(funtion, positionID);
        }

        public List<HRPersonnelRequisition> GetPositionRequsition()
        {
            return this._PositionsRepo.GetPositionRequsition();
        }

        public HRPositionAnalisy GetPositionsAnalisyBYPositionID(int id)
        {
            return this._PositionsRepo.GetPositionsAnalisyBYPositionID(id);
        }

        public List<HRPosition> GetPositionsBy(string name)
        {
            return this._PositionsRepo.GetPositionsBy(name);
        }

        public List<HRPosition> GetPositionsByDeparmentID(int id)
        {
            return this._PositionsRepo.GetPositionsByDeparmentID(id);
        }

        public HRPosition GetPositionsByID(int id)
        {
            return this._PositionsRepo.GetPositionsByID(id);
        }

        public HRSalaryDetail GetSalaryDetailBy(int positionID, int benefitID, int levelID)
        {
            return this._PositionsRepo.GetSalaryDetailBy(levelID, benefitID, positionID );
        }

        public HRSalaryLevel GetSalaryLevelByID(int id)
        {
            return this._PositionsRepo.GetSalaryLevelByID(id);
        }

        public List<HRPosition> GetSubordinadosByPositionID(int id)
        {
            return this._PositionsRepo.GetSubordinadosByPositionID(id);
        }

        public void Save(HRSalaryLevel level)
        {
            this._PositionsRepo.Save(level);
        }

        public void Save(HROrganizationChart organizacion)
        {
            this._PositionsRepo.Save(organizacion);
        }

        public void Save(HRPositionCompetence relacion)
        {
            this._PositionsRepo.Save(relacion);
        }

        public void Save(HRCompensationBenefit compensacion)
        {
            this._PositionsRepo.Save(compensacion);
        }

        public void Save(HRSalaryDetail detalleSalario)
        {
            this._PositionsRepo.Save(detalleSalario);
        }

        public void Save(HRCompetencesCategory categoria)
        {
            this._PositionsRepo.Save(categoria);
        }

        public void Save(HRDeparmentsPosition relacion)
        {
            this._PositionsRepo.Save(relacion);
        }

        public void Save(HRCompetence competencia)
        {
            this._PositionsRepo.Save(competencia);
        }

        public void Save(HRPositionAnalisy analisys)
        {
            this._PositionsRepo.Save(analisys);
        }

        public void Save(HRPositionAnalisysDetail detalle)
        {
            this._PositionsRepo.Save(detalle);
        }

        public void Save(object objeto)
        {
            this._PositionsRepo.Save(objeto);
        }

        #endregion Public Methods

    }
}