﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class DepartmentService
    {
        private IDepartment _DepartmentRepo;

        public DepartmentService (IDepartment repo) {
            this._DepartmentRepo = repo;
        }

        public List<HRDepartment> GetAllDepartments() {
            return this._DepartmentRepo.GetAllDepartments();
        }
        public HRDepartment GetBy(int id) {
            return this._DepartmentRepo.GetBy(id);
        }
    }
}
