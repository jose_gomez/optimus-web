﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Services
{
    public class AuditIndicatorService
    {
        IAuditIndicator _repository;
        public AuditIndicatorService(IAuditIndicator rep)
        {
            this._repository = rep;
        }

        public List<Models.AuditIndicator> GetAllIndicator()
        {
            return this._repository.GetAllIndicator();
        }

        public void SaveByID(SHAuditIndicator indicator)
        {
            this._repository.SaveByID(indicator);
        }
        public List<VwAuditIndicatorOrder> GetAllIndicatorByCategoryID(int idCat)
        {
            return this._repository.GetAllIndicatorByCategoryID(idCat);
        }
        public List<SHAuditIndicator> GetIndicatorByID(int idIndic)
        {
            return this._repository.GetIndicatorByID(idIndic);
        }
        public List<Models.AuditIndicator> GetAllIndicatorByControlID(int controlID)
        {
            return this._repository.GetAllIndicatorByControlID(controlID);
        }
    }
}
