﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Optimus.Web.DA;

namespace Optimus.Web.BC.Services
{
    public class OrganizationLevelService : Contracts.OrganizationLevel
    {
        private Contracts.OrganizationLevel _OrganizationLevelRepo;
        public OrganizationLevelService(Contracts.OrganizationLevel organizationLevelRepo)
        {
            this._OrganizationLevelRepo = organizationLevelRepo;
        }
        public void Delete(object objeto)
        {
            this._OrganizationLevelRepo.Delete(objeto);
        }

        public List<HROrganizationLevel> GetAllOrganizationLevels()
        {
            return this._OrganizationLevelRepo.GetAllOrganizationLevels();
        }

        public List<HROrganizationLevel> GetSuperiorOrganizationLevels(int organizationLevelID)
        {
            return this._OrganizationLevelRepo.GetSuperiorOrganizationLevels(organizationLevelID);
        }

        public void Save(object objeto)
        {
            this._OrganizationLevelRepo.Save(objeto);
        }
    }
}
