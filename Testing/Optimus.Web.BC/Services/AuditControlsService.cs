﻿using Optimus.Web.BC.Contracts;
using Optimus.Web.DA;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Optimus.Web.BC.Services
{
    public class AuditControlsService
    {
        IAuditControls _repository;
        public AuditControlsService(IAuditControls rep)
        {
            this._repository = rep;
        }
        public List<SHAuditControl> SaveAuditControlByID(SHAuditControl controlAudit)
        {
            return this._repository.SaveAuditControlByID(controlAudit);
        }
        public List<SHAuditControl> GetAllAuditControl()
        {
            return this._repository.GetAllAuditControl();
        }
        public List<SHAuditControl> GetAuditControlByID(int controlID)
        {
            return this._repository.GetAuditControlByID(controlID);
        }
        public List<SYEmailEmployee> GetEmailsByAuditControlID(string controlName)
        {
            return this._repository.GetEmailsByAuditControlID(controlName);
        }
        public List<SYEmailEmployee> SaveEmailsByAuditControlID(SYEmailEmployee emailEmployee, string alertType, int controlID, int empleyeeID)
        {
            return this._repository.SaveEmailsByAuditControlID(emailEmployee, alertType, controlID, empleyeeID);
        }
        public List<SHAuditControl> GetAuditControlByName(string controlName)
        {
            return this._repository.GetAuditControlByName(controlName);
        }

        public void SendEmailByAlertTypeID(int TypeID, string subject, string body)
        {
             this._repository.SendEmailByAlertTypeID(TypeID, subject, body);
        }

    }
}
