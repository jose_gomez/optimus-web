//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Optimus.Web.DA.Optimus
{
    using System;
    using System.Collections.Generic;
    
    public partial class HRDepartment
    {
        public HRDepartment()
        {
            this.HRDeparment_Position = new HashSet<HRDeparment_Position>();
            this.TSTickets = new HashSet<TSTicket>();
            this.HREmployees = new HashSet<HREmployee>();
            this.TSDepartmentHeads = new HashSet<TSDepartmentHead>();
            this.TSTickets1 = new HashSet<TSTicket>();
            this.TSProblemType = new HashSet<TSProblemType>();
        }
    
        public int DepartmentId { get; set; }
        public string Name { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public string Account { get; set; }
        public Nullable<bool> Status { get; set; }
        public Nullable<int> DivisionID { get; set; }
        public Nullable<System.DateTime> Create_dt { get; set; }
        public Nullable<System.DateTime> Modify_dt { get; set; }
        public string Users { get; set; }
        public string Comments { get; set; }
        public Nullable<bool> IsProduction { get; set; }
        public Nullable<bool> ValidationRequired { get; set; }
    
        public virtual ICollection<HRDeparment_Position> HRDeparment_Position { get; set; }
        public virtual ICollection<TSTicket> TSTickets { get; set; }
        public virtual ICollection<HREmployee> HREmployees { get; set; }
        public virtual ICollection<TSDepartmentHead> TSDepartmentHeads { get; set; }
        public virtual ICollection<TSTicket> TSTickets1 { get; set; }
        public virtual ICollection<TSProblemType> TSProblemType { get; set; }
    }
}
