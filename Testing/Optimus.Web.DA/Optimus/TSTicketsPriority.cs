//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Optimus.Web.DA.Optimus
{
    using System;
    using System.Collections.Generic;
    
    public partial class TSTicketsPriority
    {
        public TSTicketsPriority()
        {
            this.TSTickets = new HashSet<TSTicket>();
        }
    
        public int TicketsPriorityID { get; set; }
        public string Priority { get; set; }
    
        public virtual ICollection<TSTicket> TSTickets { get; set; }
    }
}
